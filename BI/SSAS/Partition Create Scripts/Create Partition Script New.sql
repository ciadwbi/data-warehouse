-- Transaction

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =0

WHILE @Batch<=200
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Transaction_fb191a35-189a-4cb4-b0b1-7bf18f877c63</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT T.TransactionId,T.TransactionIdSource,TransactionDetailKey,BalanceTypeKey,T.ContractKey,AccountKey,AccountStatusKey,WalletKey,PromotionKey,MarketKey,EventKey,
									DayKey,DarwinTransactionTimestamp,DarwinTimeKey,SydneyTransactionTimestamp,SydneyTimeKey,CampaignKey,ChannelKey,LedgerID,TransactedAmount,BetTypeKey,LegTypeKey,
									PriceTypeKey,ClassKey,UserKey,InstrumentKey,NoteKey,InPlayKey,C.BetId
									FROM [Fact].[Transaction] T WITH(NOLOCK)
									INNER JOIN Dimension.[Contract] C WITH(NOLOCK) ON T.ContractKey=C.ContractKey
									WHERE T.ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END

-- Account

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =0

WHILE @Batch<=500
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Account_3af3badf-e214-407a-9a7a-87ffc0f7fccd</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT AccountKey,AccountID,PIN,CustomerKey,CustomerID,LedgerID,LegacyLedgerID,LedgerTypeKey,LedgerTypeID,LedgerTypeName,LedgerGroupingName,LedgerSequenceNumber,SourceTitle,SourceSurname,
									SourceMiddleName,SourceFirstName,SourceGender,Title,Surname,MiddleName,FirstName,DOB,Gender,CompanyKey,AccountOpenedDayKey,AccountOpenedDate,AccountOpenedChannel,AddressType,SourceAddress1,
									SourceAddress2,SourceSuburb,SourceStateCode,SourceStateName,SourcePostCode,SourceCountryCode,SourceCountryName,Address1,Address2,Suburb,StateCode,StateName,PostCode,CountryCode,CountryName,
									GeographicalLocation,AustralianClient,SourcePhone,SourceMobile,SourceFax,SourceEmail,SourceAlternateEmail,Phone,Mobile,Fax,Email,AlternateEmail,StatementMethod,StatementFrequency,BTag2,
									AffiliateID,AffiliateName,SiteID,TrafficSource,RefURL,CampaignID,Keywords,AccountSource,CustomerSource,LedgerSource,LedgerTypeSource
									FROM [Dimension].[Account] WITH(NOLOCK)
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END


-- AccountStatus

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =0

WHILE @Batch<=200
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Query_1f31febe-de52-463f-8844-08c21db73fd1</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT AccountStatusKey,AccountKey,LedgerID,LedgerSource,LedgerEnabled,LedgerStatusCode,AccountType,InternetProfile,VIP,CompanyKey,CreditLimit,MinBetAmount,MaxBetAmount,Introducer,
									Handled,PhoneColourDesc,DisableDeposit,DisableWithdrawal,AccountClosedByUserID,AccountClosedByUserName,AccountClosedDate,ReIntroducedBy,ReIntroducedDate,AccountClosureReason,DepositLimit,
									LossLimit,LimitPeriod,LimitTimeStamp,ReceiveMarketingEmail,ReceiveCompetitionEmail,ReceiveSMS,ReceivePostalMail,ReceiveFax,CurrentVersion
									FROM [Dimension].[AccountStatus] WITH(NOLOCK)
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END

     
-- Balance

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT		=	0

WHILE @Batch<=9
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Balance_ba2d260c-7335-4e73-b009-878905d37655</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT AccountKey,AccountStatusKey,LedgerID,LedgerSource,DayKey,BalanceTypeKey,
									WalletKey,OpeningBalance,TotalTransactedAmount,ClosingBalance
									FROM [Fact].[Balance] WITH(NOLOCK)							
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END


-- BetType

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =1

WHILE @Batch<=500
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>BetType_21a474aa-971b-45db-ba1d-c3cdf39e5e57</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT BetTypeKey,BetGroupType,BetType,BetSubType,GroupingType,PredictionType 
									FROM [Dimension].BetType WITH(NOLOCK)
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END

-- Position

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =1

WHILE @Batch<=500
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Position_08f75688-32a2-4527-9225-d064a9b0b384</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT BalanceTypeKey,AccountKey,AccountStatusKey,LedgerID,LedgerSource,DayKey,ActivityKey,LifecycleKey,FirstDepositDayKey,FirstBetDayKey,FirstBetTypeKey,FirstBetClassKey,
									FirstBetChannelKey,LastBetDayKey,LastBetTypeKey,LastBetClassKey,LastBetChannelKey,OpeningBalance,ClosingBalance,TransactedAmount,LifetimeTurnover,LifetimeGrossWin,LifetimeBetCount,
									LifetimeBonus,LifetimeDeposits,LifetimeWithdrawals,TurnoverMTDCalender,TurnoverMTDFiscal,TierKey,TierKeyFiscal
									FROM [Fact].[Position] WITH(NOLOCK)
									WHERE DAYKEY/100 >=20150101 AND DAYKEY/100 &lt;20151201
									WHERE DayKey%100='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        PRINT @SQL
        --EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END


-- Contract (BatchKey Partition)

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch BIGINT      =-1000000

WHILE @Batch<=300000000
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Contract_49a6a2b8-6c4e-43ae-97af-827460f1f174</MeasureGroupID>
			<PartitionID>Range_'+CONVERT(CHAR(4),@Batch/1000000)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>Range_'+CONVERT(CHAR(100),@Batch/1000000)+'</ID>
			  <Name>Range_'+CONVERT(CHAR(100),@Batch/1000000)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT ContractKey,BetGroupID,BetId,LegId,TransactionID,FreeBetID,ExternalID,External1,External2,RequestID,WithdrawID,
									BetGroupIDSource,LegIdSource,BetIdSource,TransactionIDSource
									FROM [Dimension].Contract WITH(NOLOCK)
									WHERE ContractKey>='+CONVERT(CHAR(100),@Batch)+' AND ContractKey&lt;'+CONVERT(CHAR(100),@Batch+1000000)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1000000
END



-- Contract (Range Partition)

DECLARE @SQL VARCHAR(MAX)
DECLARE @Partition SMALLINT      =2
DECLARE @MinRange BIGINT
DECLARE @MaxRange BIGINT

WHILE @Partition<=500
BEGIN
		SELECT @MinRange = ISNULL(FromKey,-1000000) FROM Warehouse.[SSAS].[ContractPartitions] WHERE PartitionNo=@Partition
		SELECT @MaxRange = ISNULL(ToKey,1000000000) FROM Warehouse.[SSAS].[ContractPartitions] WHERE PartitionNo=@Partition

        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Contract_49a6a2b8-6c4e-43ae-97af-827460f1f174</MeasureGroupID>
			<PartitionID>Range_'+CONVERT(CHAR(4),@Partition)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>Range_'+CONVERT(CHAR(4),@Partition)+'</ID>
			  <Name>Range_'+CONVERT(CHAR(4),@Partition)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT ContractKey,BetGroupID,BetId,LegId,TransactionID,FreeBetID,ExternalID,External1,External2
									FROM [Dimension].Contract WITH(NOLOCK)
									WHERE ContractKey>='+CONVERT(CHAR(20),@MinRange)+' AND ContractKey &lt; '+CONVERT(CHAR(20),@MaxRange)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>
		
		'        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Partition=@Partition+1
END

-- Instrument

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =1

WHILE @Batch<=500
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Instrument_51f6abc3-7492-4d19-bdc5-4a9821b9acfc</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT InstrumentKey,PaymentMethodCode,PaymentMethod 
									FROM [Dimension].Instrument WITH(NOLOCK) 
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END



-- InstrumentSecure

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =1

WHILE @Batch<=500
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Instrument Secure_2f9d714e-1dcc-488e-9951-9286f014830b</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT InstrumentKey,InstrumentID,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,ProviderName 
									FROM [Dimension].Instrument WITH(NOLOCK)
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END


-- User

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =0

WHILE @Batch<=200
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>User_5db18643-a5d3-444f-a881-72cc8c00e893</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT UserKey,UserId,SystemName,EmployeeKey,Forename,Surname,Source
									FROM [Dimension].[User] WITH(NOLOCK)
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END


-- KPI

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT =0

WHILE @Batch<=200
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>KPI_b9163b76-0007-43d4-9e11-e50a3bc78073</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT ContractKey,DayKey,AccountKey,AccountStatusKey,BetTypeKey,LegTypeKey,ChannelKey,CampaignKey,MarketKey,EventKey,WalletKey,ClassKey,UserKey,
									InstrumentKey,InPlayKey,AccountsOpened,FirstTimeDepositors,FirstTimeBettors,DepositsRequested,DepositsCompleted,Promotions,Transfers,BetsPlaced,ContractsPlaced,
									BettorsPlaced,PlayDaysPlaced,PlayFiscalWeeksPlaced,PlayCalenderWeeksPlaced,PlayFiscalMonthsPlaced,PlayCalenderMonthsPlaced,TurnoverPlaced,Payout,WithdrawalsRequested,WithdrawalsCompleted,
									Adjustments,BetsSettled,ContractsResulted,BettorsSettled,PlayDaysResulted,PlayFiscalWeeksResulted,PlayCalenderWeeksResulted,PlayFiscalMonthsResulted,PlayCalenderMonthsResulted,
									TurnoverSettled,GrossWin,NetRevenue,NetRevenueAdjustment,BetbackGrosswin,GrosswinResettledAdjustment,ModifiedBatchKey
									FROM [Fact].[KPI] WITH(NOLOCK)
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END


    
-- Market

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =0

WHILE @Batch<=200
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Market_d48e5cd7-f8fd-4f2a-9bc7-758fdadf7121</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT MarketKey,Source,MarketId,MarketName,MarketType,MarketGroup,LevyRate,Sport 
									FROM [Dimension].[Market] WITH(NOLOCK)
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END


-- Event


DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =0

WHILE @Batch<=200
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Event_e7bac926-9a35-40cd-a6c2-c84ae875c720</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT EventKey,EventId,ClassID,MarketName,OfferedDateTime,OfferedDayKey,OfferedLocalDateTime,SuspendedDateTime,SuspendedDayKey,SuspendedLocalDateTime,SettledDateTime,SettledDayKey,
									SettledLocalDateTime,EventName,EventDescription,EventType,Grade,Distance,PrizePool,Track,Weather,ScheduledDateTime,ScheduledDayKey,ScheduledLocalDateTime,ResultedDateTime,ResultedDayKey,
									ResultedLocalDateTime,WinningTime,RoundName,RoundSequence,CompetitionName,CompetitionSeason,CompetitionStartDate,CompetitionEndDate,ClassKey,VenueName,VenueDescription,StateCode,StateName,
									CountryCode,CountryName,Source,SourceMarketName,SourceEventId,SourceEventName,SourceParentEventId,SourceParentEventName,SourceGrandparentEventId,SourceGrandparentEventName,SourceGreatGrandparentEventId,
									SourceGreatGrandparentEventName,SourceEventType,SourceEventDate,SourceVenueId,SourceVenueName,SourceSubSportType 
									FROM [Dimension].[Event] WITH(NOLOCK)
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END


-- Wallet

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =0

WHILE @Batch<=200
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Wallet_ae8773c0-e9d8-49d8-9219-2fa03023d387</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT WalletKey,WalletId,WalletName
									FROM [Dimension].[Wallet] WITH(NOLOCK)
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END



-- Promotion

DECLARE @SQL VARCHAR(MAX)
DECLARE @Batch SMALLINT      =0

WHILE @Batch<=200
BEGIN
        SELECT @SQL = 
        '<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
		  <Object>
			<DatabaseID>Bonza</DatabaseID>
			<CubeID>Model</CubeID>
			<MeasureGroupID>Promotion_eccf2f78-9d19-4633-8a08-487e41dda0f0</MeasureGroupID>
			<PartitionID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</PartitionID>
		  </Object>
		  <ObjectDefinition>
			<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
			  <ID>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</ID>
			  <Name>ModifiedBatch_'+CONVERT(CHAR(4),@Batch)+'</Name>
			  <Source xsi:type="QueryBinding">
				<DataSourceID>40ee7a12-5191-4965-ab8d-a1d226eca553</DataSourceID>
				<QueryDefinition>	SELECT PromotionKey,PromotionId,Source,PromotionType,PromotionName,OfferDate,ExpiryDate 
									FROM [Dimension].[Promotion] WITH(NOLOCK)
									WHERE ModifiedBatchKey='+CONVERT(CHAR(4),@Batch)+'
				</QueryDefinition>
								 
			  </Source>
			  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
			  <ProcessingMode>Regular</ProcessingMode>
			  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
			</Partition>
		  </ObjectDefinition>
		</Alter>'
        
        --PRINT @SQL
        EXEC(@SQL) AT [DW04_SSAS_TABULAR]

        SELECT @Batch=@Batch+1
END