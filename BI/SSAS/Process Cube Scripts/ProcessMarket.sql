USE [Warehouse]
GO
/****** Object:  StoredProcedure [SSAS].[SP_ProcessMarket]    Script Date: 18/05/2016 1:47:59 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [SSAS].[SP_ProcessMarket] 
(
	@BatchKey		INT,
	@ProcessType	CHAR(4) -- 'Inc' or 'Full'
)
AS
BEGIN
BEGIN TRY
	DECLARE @Process NVARCHAR(MAX)

	EXEC Warehouse.[Control].Sp_Log	@BatchKey,'SP_ProcessMarket','ProcessTables','Start'

	IF ISNULL(@BatchKey,-1) >=0 AND @ProcessType='Inc'
	BEGIN
		SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessData</Type>
						  <Object>
							<DatabaseID>Bonza</DatabaseID>
							<CubeID>Model</CubeID>
							<MeasureGroupID>Market_d48e5cd7-f8fd-4f2a-9bc7-758fdadf7121</MeasureGroupID>
							<PartitionID>ModifiedBatch_'+CONVERT(VARCHAR(10),@BatchKey)+'</PartitionID>
						  </Object>
						</Process>
						'
	END

	IF ISNULL(@BatchKey,-1) >=0 AND @ProcessType='Full'
	BEGIN
		SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessData</Type>
						  <Object>
							<DatabaseID>Bonza</DatabaseID>
							<DimensionID>Market_d48e5cd7-f8fd-4f2a-9bc7-758fdadf7121</DimensionID>
						  </Object>
						</Process>
						'
	END

	EXEC(@Process) AT [WHTABULARMODEL]

	EXEC Warehouse.[Control].Sp_Log @BatchKey,'SP_ProcessMarket',NULL,'Success'

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE() + ' ' + suser_sname();

	EXEC Warehouse.[Control].Sp_Log @BatchKey, 'SP_ProcessMarket', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END