USE [Warehouse]
GO
/****** Object:  StoredProcedure [SSAS].[SP_ProcessRecalc]    Script Date: 18/05/2016 1:48:20 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROC [SSAS].[SP_ProcessRecalc] 
(
	@BatchKey		INT
)
AS
BEGIN
BEGIN TRY
	DECLARE @Process NVARCHAR(MAX)

	EXEC Warehouse.[Control].Sp_Log	@BatchKey,'SP_ProcessRecalc','ProcessTables','Start'

	SET @Process=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
					  <Type>ProcessRecalc</Type>
					  <Object>
						<DatabaseID>Bonza</DatabaseID>
					  </Object>
					</Process>'

	EXEC(@Process) AT [WHTABULARMODEL]

	EXEC Warehouse.[Control].Sp_Log @BatchKey,'SP_ProcessRecalc',NULL,'Success'

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE() + ' ' + suser_sname();

	EXEC Warehouse.[Control].Sp_Log @BatchKey, 'SP_ProcessRecalc', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END