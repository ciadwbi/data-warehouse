﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VIPDashboard
{
    public partial class GiftingHospitality : Global
    {
        private int iRow;

        public GiftingHospitality()
        {
            InitializeComponent();

            lblLoggedIn.Text = "Logged in as : " + NameoftheUser;
        }

        private void GiftingHospitality_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDDataSet1.DIM_VIP_Dashboard_Reasons' table. You can move, or remove it, as needed.
            this.dIM_VIP_Dashboard_ReasonsTableAdapter.FillGifting(this.bIDDataSet.DIM_VIP_Dashboard_Reasons);
            // TODO: This line of code loads data into the 'bIDDataSet1.DIM_VIP_Dashboard_User' table. You can move, or remove it, as needed.
            this.dIM_VIP_Dashboard_UserTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_User);
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard' table. You can move, or remove it, as needed.
            this.dIM_VIP_Dashboard_GiftingTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_Gifting, UserName);

            cbCategory.SelectedIndex = -1;
            cbVIPManager.SelectedValue = UserName;

            tsCopy.Click += tsCopy_Click;
            tsDelete.Click += btnDelete_Click;
        }

        private void dgvVIP_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvVIP.CurrentCell != null)
                iRow = dgvVIP.CurrentCell.RowIndex;
        }

        private void txtPIN_TextChanged(object sender, EventArgs e)
        {
            int PIN;
            if (!int.TryParse(txtPIN.Text, out PIN) && txtPIN.Text != "")
            {
                MessageBox.Show("Entered PIN is not valid.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            double Value;
            if (!double.TryParse(txtValue.Text, out Value) && txtValue.Text != "")
            {
                MessageBox.Show("Entered Value is not valid.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (IsValidated())
                {

                    DataRow drRow = this.bIDDataSet.DIM_VIP_Dashboard_Gifting.NewRow();

                    drRow["Date"] = dtpDate.Value.ToShortDateString();
                    drRow["PIN"] = txtPIN.Text.ToString();
                    drRow["Category"] = cbCategory.SelectedValue.ToString();
                    drRow["Gift"] = txtGift.Text.Trim();
                    drRow["Comments"] = txtComments.Text.ToString();
                    drRow["EnteredDate"] = DateTime.Now;
                    drRow["EnteredBy"] = UserName;
                    drRow["VIPManager"] = cbVIPManager.SelectedValue.ToString();
                    drRow["Value"] = txtValue.Text.Trim();

                    bIDDataSet.DIM_VIP_Dashboard_Gifting.Rows.Add(drRow);

                    this.dIM_VIP_Dashboard_GiftingTableAdapter.Update(bIDDataSet);

                    ClearFields();

                    btnClear.Visible = true;
                    btnDelete.Visible = true;
                    btnEdit.Visible = true;

                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowDelete = this.bIDDataSet.DIM_VIP_Dashboard_Gifting.Rows[iRow];

                dtpDate.Value = (DateTime)drRowDelete["Date"];
                txtPIN.Text = drRowDelete["PIN"].ToString();
                txtGift.Text = drRowDelete["Gift"].ToString();
                txtValue.Text = drRowDelete["Value"].ToString();
                cbCategory.SelectedValue = drRowDelete["Category"].ToString();
                txtComments.Text = drRowDelete["Comments"].ToString();
                cbVIPManager.SelectedValue = drRowDelete["VIPManager"].ToString();



                this.dIM_VIP_Dashboard_GiftingTableAdapter.DeleteGifting((int)(drRowDelete["PIN"]), (DateTime)drRowDelete["Date"]);

                this.bIDDataSet.DIM_VIP_Dashboard_Gifting.Rows.Remove(drRowDelete);

                this.dIM_VIP_Dashboard_GiftingTableAdapter.Update(bIDDataSet);

                btnClear.Visible = false;
                btnDelete.Visible = false;
                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private bool IsValidated()
        {
            if (txtPIN.Text.Trim() == "")
            {
                MessageBox.Show("Entered PIN is not valid.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtGift.Text.Trim() == "")
            {
                MessageBox.Show("Please enter the gift.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtValue.Text.Trim() == "")
            {
                MessageBox.Show("Please enter the Value.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbCategory.SelectedValue == null)
            {
                MessageBox.Show("Please select a category.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbVIPManager.SelectedValue == null)
            {
                MessageBox.Show("Please select a VIP Manager.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }

            else return true;
        }


        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        private void ClearFields()
        {
            dtpDate.Value = DateTime.Today;
            txtPIN.Text = "";
            txtGift.Text = "";
            txtPIN.Text = "";
            cbCategory.SelectedIndex = -1;
            txtValue.Text = "";
            txtComments.Text = "";
           
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "VIP Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDDataSet.DIM_VIP_Dashboard_Gifting.Rows[iRow];

                    this.dIM_VIP_Dashboard_GiftingTableAdapter.DeleteGifting((int)(drRowDelete["PIN"]), (DateTime)drRowDelete["Date"]);

                    this.bIDDataSet.DIM_VIP_Dashboard_Gifting.Rows.Remove(drRowDelete);

                    this.dIM_VIP_Dashboard_GiftingTableAdapter.Update(bIDDataSet);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void tsCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(dgvVIP.GetClipboardContent());
        }

        private void linkLabel1_Click(object sender, EventArgs e)
        {
            Process.Start("mailto:BI.Service.Reports@williamhill.com.au");
        }

    }
}
