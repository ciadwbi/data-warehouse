﻿namespace VIPDashboard
{
    partial class GiftingHospitality
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(GiftingHospitality));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.lblLoggedIn = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dgvVIP = new System.Windows.Forms.DataGridView();
            this.dateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pINDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categoryDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.giftDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valueDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.commentsDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enteredDateDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.enteredByDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vIPManagerDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cmpGifting = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsCopy = new System.Windows.Forms.ToolStripMenuItem();
            this.tsDelete = new System.Windows.Forms.ToolStripMenuItem();
            this.dIMVIPDashboardGiftingBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bIDDataSet = new VIPDashboard.BIDDataSet();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cbVIPManager = new System.Windows.Forms.ComboBox();
            this.dIMVIPDashboardUserBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.txtValue = new System.Windows.Forms.TextBox();
            this.txtGift = new System.Windows.Forms.TextBox();
            this.cbCategory = new System.Windows.Forms.ComboBox();
            this.dIMVIPDashboardReasonsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtComments = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnEdit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.dtpDate = new System.Windows.Forms.DateTimePicker();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPIN = new System.Windows.Forms.TextBox();
            this.dIM_VIP_Dashboard_UserTableAdapter = new VIPDashboard.BIDDataSetTableAdapters.DIM_VIP_Dashboard_UserTableAdapter();
            this.dIM_VIP_Dashboard_ReasonsTableAdapter = new VIPDashboard.BIDDataSetTableAdapters.DIM_VIP_Dashboard_ReasonsTableAdapter();
            this.dIM_VIP_Dashboard_GiftingTableAdapter = new VIPDashboard.BIDDataSetTableAdapters.DIM_VIP_Dashboard_GiftingTableAdapter();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVIP)).BeginInit();
            this.cmpGifting.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dIMVIPDashboardGiftingBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bIDDataSet)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dIMVIPDashboardUserBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dIMVIPDashboardReasonsBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // lblLoggedIn
            // 
            this.lblLoggedIn.AutoSize = true;
            this.lblLoggedIn.Location = new System.Drawing.Point(26, 114);
            this.lblLoggedIn.Name = "lblLoggedIn";
            this.lblLoggedIn.Size = new System.Drawing.Size(77, 13);
            this.lblLoggedIn.TabIndex = 22;
            this.lblLoggedIn.Text = "Logged in as : ";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.ForeColor = System.Drawing.Color.DimGray;
            this.label8.Location = new System.Drawing.Point(21, 651);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 21;
            this.label8.Text = "Provided By";
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.linkLabel1.AutoSize = true;
            this.linkLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.linkLabel1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(155)))), ((int)(((byte)(211)))));
            this.linkLabel1.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(34)))), ((int)(((byte)(155)))), ((int)(((byte)(211)))));
            this.linkLabel1.Location = new System.Drawing.Point(81, 651);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(66, 13);
            this.linkLabel1.TabIndex = 20;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "BI Service";
            this.linkLabel1.Click += new System.EventHandler(this.linkLabel1_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(48)))), ((int)(((byte)(86)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Location = new System.Drawing.Point(24, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(947, 82);
            this.panel1.TabIndex = 19;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(250, 82);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 28F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.Color.White;
            this.label7.Location = new System.Drawing.Point(282, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(341, 43);
            this.label7.TabIndex = 0;
            this.label7.Text = "Gifting / Hospitality";
            // 
            // dgvVIP
            // 
            this.dgvVIP.AllowUserToAddRows = false;
            this.dgvVIP.AllowUserToDeleteRows = false;
            this.dgvVIP.AllowUserToOrderColumns = true;
            this.dgvVIP.AllowUserToResizeRows = false;
            this.dgvVIP.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvVIP.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVIP.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvVIP.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvVIP.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dateDataGridViewTextBoxColumn,
            this.pINDataGridViewTextBoxColumn,
            this.categoryDataGridViewTextBoxColumn,
            this.giftDataGridViewTextBoxColumn,
            this.valueDataGridViewTextBoxColumn,
            this.commentsDataGridViewTextBoxColumn,
            this.enteredDateDataGridViewTextBoxColumn,
            this.enteredByDataGridViewTextBoxColumn,
            this.vIPManagerDataGridViewTextBoxColumn});
            this.dgvVIP.ContextMenuStrip = this.cmpGifting;
            this.dgvVIP.DataSource = this.dIMVIPDashboardGiftingBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgvVIP.DefaultCellStyle = dataGridViewCellStyle2;
            this.dgvVIP.Location = new System.Drawing.Point(24, 329);
            this.dgvVIP.MultiSelect = false;
            this.dgvVIP.Name = "dgvVIP";
            this.dgvVIP.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvVIP.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgvVIP.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvVIP.Size = new System.Drawing.Size(947, 319);
            this.dgvVIP.TabIndex = 18;
            this.dgvVIP.SelectionChanged += new System.EventHandler(this.dgvVIP_SelectionChanged);
            // 
            // dateDataGridViewTextBoxColumn
            // 
            this.dateDataGridViewTextBoxColumn.DataPropertyName = "Date";
            this.dateDataGridViewTextBoxColumn.HeaderText = "Date";
            this.dateDataGridViewTextBoxColumn.Name = "dateDataGridViewTextBoxColumn";
            this.dateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // pINDataGridViewTextBoxColumn
            // 
            this.pINDataGridViewTextBoxColumn.DataPropertyName = "PIN";
            this.pINDataGridViewTextBoxColumn.HeaderText = "PIN";
            this.pINDataGridViewTextBoxColumn.Name = "pINDataGridViewTextBoxColumn";
            this.pINDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // categoryDataGridViewTextBoxColumn
            // 
            this.categoryDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.categoryDataGridViewTextBoxColumn.DataPropertyName = "Category";
            this.categoryDataGridViewTextBoxColumn.HeaderText = "Category";
            this.categoryDataGridViewTextBoxColumn.Name = "categoryDataGridViewTextBoxColumn";
            this.categoryDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // giftDataGridViewTextBoxColumn
            // 
            this.giftDataGridViewTextBoxColumn.DataPropertyName = "Gift";
            this.giftDataGridViewTextBoxColumn.HeaderText = "Gift";
            this.giftDataGridViewTextBoxColumn.Name = "giftDataGridViewTextBoxColumn";
            this.giftDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // valueDataGridViewTextBoxColumn
            // 
            this.valueDataGridViewTextBoxColumn.DataPropertyName = "Value";
            this.valueDataGridViewTextBoxColumn.HeaderText = "Value";
            this.valueDataGridViewTextBoxColumn.Name = "valueDataGridViewTextBoxColumn";
            this.valueDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // commentsDataGridViewTextBoxColumn
            // 
            this.commentsDataGridViewTextBoxColumn.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.commentsDataGridViewTextBoxColumn.DataPropertyName = "Comments";
            this.commentsDataGridViewTextBoxColumn.HeaderText = "Comments";
            this.commentsDataGridViewTextBoxColumn.Name = "commentsDataGridViewTextBoxColumn";
            this.commentsDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // enteredDateDataGridViewTextBoxColumn
            // 
            this.enteredDateDataGridViewTextBoxColumn.DataPropertyName = "EnteredDate";
            this.enteredDateDataGridViewTextBoxColumn.HeaderText = "EnteredDate";
            this.enteredDateDataGridViewTextBoxColumn.Name = "enteredDateDataGridViewTextBoxColumn";
            this.enteredDateDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // enteredByDataGridViewTextBoxColumn
            // 
            this.enteredByDataGridViewTextBoxColumn.DataPropertyName = "EnteredBy";
            this.enteredByDataGridViewTextBoxColumn.HeaderText = "EnteredBy";
            this.enteredByDataGridViewTextBoxColumn.Name = "enteredByDataGridViewTextBoxColumn";
            this.enteredByDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // vIPManagerDataGridViewTextBoxColumn
            // 
            this.vIPManagerDataGridViewTextBoxColumn.DataPropertyName = "VIPManager";
            this.vIPManagerDataGridViewTextBoxColumn.HeaderText = "VIPManager";
            this.vIPManagerDataGridViewTextBoxColumn.Name = "vIPManagerDataGridViewTextBoxColumn";
            this.vIPManagerDataGridViewTextBoxColumn.ReadOnly = true;
            // 
            // cmpGifting
            // 
            this.cmpGifting.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsCopy,
            this.tsDelete});
            this.cmpGifting.Name = "cmpComms";
            this.cmpGifting.Size = new System.Drawing.Size(108, 48);
            // 
            // tsCopy
            // 
            this.tsCopy.Image = ((System.Drawing.Image)(resources.GetObject("tsCopy.Image")));
            this.tsCopy.Name = "tsCopy";
            this.tsCopy.Size = new System.Drawing.Size(107, 22);
            this.tsCopy.Text = "&Copy";
            // 
            // tsDelete
            // 
            this.tsDelete.Image = ((System.Drawing.Image)(resources.GetObject("tsDelete.Image")));
            this.tsDelete.Name = "tsDelete";
            this.tsDelete.Size = new System.Drawing.Size(107, 22);
            this.tsDelete.Text = "&Delete";
            // 
            // dIMVIPDashboardGiftingBindingSource
            // 
            this.dIMVIPDashboardGiftingBindingSource.DataMember = "DIM_VIP_Dashboard_Gifting";
            this.dIMVIPDashboardGiftingBindingSource.DataSource = this.bIDDataSet;
            // 
            // bIDDataSet
            // 
            this.bIDDataSet.DataSetName = "BIDDataSet";
            this.bIDDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.cbVIPManager);
            this.groupBox1.Controls.Add(this.txtValue);
            this.groupBox1.Controls.Add(this.txtGift);
            this.groupBox1.Controls.Add(this.cbCategory);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.btnDelete);
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.txtComments);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.btnEdit);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.btnSave);
            this.groupBox1.Controls.Add(this.dtpDate);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.txtPIN);
            this.groupBox1.Location = new System.Drawing.Point(24, 128);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(947, 195);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(20, 45);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(69, 13);
            this.label3.TabIndex = 24;
            this.label3.Text = "VIP Manager";
            // 
            // cbVIPManager
            // 
            this.cbVIPManager.DataSource = this.dIMVIPDashboardUserBindingSource;
            this.cbVIPManager.DisplayMember = "Name";
            this.cbVIPManager.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbVIPManager.FormattingEnabled = true;
            this.cbVIPManager.Location = new System.Drawing.Point(94, 42);
            this.cbVIPManager.Name = "cbVIPManager";
            this.cbVIPManager.Size = new System.Drawing.Size(156, 21);
            this.cbVIPManager.TabIndex = 23;
            this.cbVIPManager.ValueMember = "UserName";
            // 
            // dIMVIPDashboardUserBindingSource
            // 
            this.dIMVIPDashboardUserBindingSource.DataMember = "DIM_VIP_Dashboard_User";
            this.dIMVIPDashboardUserBindingSource.DataSource = this.bIDDataSet;
            // 
            // txtValue
            // 
            this.txtValue.Location = new System.Drawing.Point(388, 67);
            this.txtValue.Name = "txtValue";
            this.txtValue.Size = new System.Drawing.Size(156, 20);
            this.txtValue.TabIndex = 22;
            this.txtValue.TextChanged += new System.EventHandler(this.txtValue_TextChanged);
            // 
            // txtGift
            // 
            this.txtGift.Location = new System.Drawing.Point(388, 41);
            this.txtGift.Name = "txtGift";
            this.txtGift.Size = new System.Drawing.Size(156, 20);
            this.txtGift.TabIndex = 21;
            // 
            // cbCategory
            // 
            this.cbCategory.DataSource = this.dIMVIPDashboardReasonsBindingSource;
            this.cbCategory.DisplayMember = "ReasonName";
            this.cbCategory.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategory.FormattingEnabled = true;
            this.cbCategory.Location = new System.Drawing.Point(94, 66);
            this.cbCategory.Name = "cbCategory";
            this.cbCategory.Size = new System.Drawing.Size(156, 21);
            this.cbCategory.TabIndex = 20;
            this.cbCategory.ValueMember = "ReasonName";
            // 
            // dIMVIPDashboardReasonsBindingSource
            // 
            this.dIMVIPDashboardReasonsBindingSource.DataMember = "DIM_VIP_Dashboard_Reasons";
            this.dIMVIPDashboardReasonsBindingSource.DataSource = this.bIDDataSet;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(326, 45);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(23, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Gift";
            // 
            // btnDelete
            // 
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Location = new System.Drawing.Point(185, 162);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 23);
            this.btnDelete.TabIndex = 11;
            this.btnDelete.Text = "DELETE";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnClear
            // 
            this.btnClear.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnClear.Location = new System.Drawing.Point(266, 162);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 12;
            this.btnClear.Text = "CLEAR";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(20, 93);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(56, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Comments";
            // 
            // txtComments
            // 
            this.txtComments.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtComments.Location = new System.Drawing.Point(94, 90);
            this.txtComments.MaxLength = 900;
            this.txtComments.Name = "txtComments";
            this.txtComments.Size = new System.Drawing.Size(847, 63);
            this.txtComments.TabIndex = 8;
            this.txtComments.Text = "";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(326, 70);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Value";
            // 
            // btnEdit
            // 
            this.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEdit.Location = new System.Drawing.Point(104, 162);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(75, 23);
            this.btnEdit.TabIndex = 10;
            this.btnEdit.Text = "EDIT";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(20, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Date";
            // 
            // btnSave
            // 
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Location = new System.Drawing.Point(23, 162);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 23);
            this.btnSave.TabIndex = 9;
            this.btnSave.Text = "SAVE";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // dtpDate
            // 
            this.dtpDate.CustomFormat = "dd/MM/yyyy";
            this.dtpDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDate.Location = new System.Drawing.Point(94, 16);
            this.dtpDate.Name = "dtpDate";
            this.dtpDate.Size = new System.Drawing.Size(156, 20);
            this.dtpDate.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(20, 70);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Category";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(326, 19);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(54, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Client PIN";
            // 
            // txtPIN
            // 
            this.txtPIN.Location = new System.Drawing.Point(388, 16);
            this.txtPIN.Name = "txtPIN";
            this.txtPIN.Size = new System.Drawing.Size(156, 20);
            this.txtPIN.TabIndex = 2;
            this.txtPIN.TextChanged += new System.EventHandler(this.txtPIN_TextChanged);
            // 
            // dIM_VIP_Dashboard_UserTableAdapter
            // 
            this.dIM_VIP_Dashboard_UserTableAdapter.ClearBeforeFill = true;
            // 
            // dIM_VIP_Dashboard_ReasonsTableAdapter
            // 
            this.dIM_VIP_Dashboard_ReasonsTableAdapter.ClearBeforeFill = true;
            // 
            // dIM_VIP_Dashboard_GiftingTableAdapter
            // 
            this.dIM_VIP_Dashboard_GiftingTableAdapter.ClearBeforeFill = true;
            // 
            // GiftingHospitality
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(993, 671);
            this.Controls.Add(this.lblLoggedIn);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.linkLabel1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.dgvVIP);
            this.Controls.Add(this.groupBox1);
            this.Name = "GiftingHospitality";
            this.Text = "GiftingHospitality";
            this.Load += new System.EventHandler(this.GiftingHospitality_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvVIP)).EndInit();
            this.cmpGifting.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dIMVIPDashboardGiftingBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bIDDataSet)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dIMVIPDashboardUserBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dIMVIPDashboardReasonsBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLoggedIn;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.LinkLabel linkLabel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dgvVIP;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ComboBox cbCategory;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.RichTextBox txtComments;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.DateTimePicker dtpDate;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtPIN;
        private System.Windows.Forms.TextBox txtValue;
        private System.Windows.Forms.TextBox txtGift;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cbVIPManager;
        private BIDDataSet bIDDataSet;
        private System.Windows.Forms.BindingSource dIMVIPDashboardUserBindingSource;
        private BIDDataSetTableAdapters.DIM_VIP_Dashboard_UserTableAdapter dIM_VIP_Dashboard_UserTableAdapter;
        private System.Windows.Forms.BindingSource dIMVIPDashboardReasonsBindingSource;
        private BIDDataSetTableAdapters.DIM_VIP_Dashboard_ReasonsTableAdapter dIM_VIP_Dashboard_ReasonsTableAdapter;
        private System.Windows.Forms.ContextMenuStrip cmpGifting;
        private System.Windows.Forms.ToolStripMenuItem tsCopy;
        private System.Windows.Forms.ToolStripMenuItem tsDelete;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn pINDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn categoryDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn giftDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn valueDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn commentsDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn enteredDateDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn enteredByDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn vIPManagerDataGridViewTextBoxColumn;
        private System.Windows.Forms.BindingSource dIMVIPDashboardGiftingBindingSource;
        private BIDDataSetTableAdapters.DIM_VIP_Dashboard_GiftingTableAdapter dIM_VIP_Dashboard_GiftingTableAdapter;

    }
}