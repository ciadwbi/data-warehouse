﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace VIPDashboard
{
    public partial class Communications : Global
    {
        public int iRow;
        public string user;

        public Communications()
        {
            InitializeComponent();

            lblLoggedIn.Text = "Logged in as : " + NameoftheUser;


        } 

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard_Reasons' table. You can move, or remove it, as needed.
            this.dIM_VIP_Dashboard_ReasonsTableAdapter.FillComms(this.bIDDataSet.DIM_VIP_Dashboard_Reasons);
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard_User' table. You can move, or remove it, as needed.
            this.dIM_VIP_Dashboard_UserTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_User);
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard_Channels' table. You can move, or remove it, as needed.
            this.dIM_VIP_Dashboard_ChannelsTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_Channels);
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard' table. You can move, or remove it, as needed.
            this.dIM_VIP_DashboardTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard, UserName);

            cbChannel.SelectedIndex = -1;
            cbCategory.SelectedIndex = -1;
            cbVIPManager.SelectedValue = UserName;

            tsCopy.Click += tsCopy_Click;
            tsDelete.Click += btnDelete_Click;

        }


        private void Save_Click(object sender, EventArgs e)
        {

            try
            {
                if (IsValidated())
                {

                    DataRow drRow = this.bIDDataSet.DIM_VIP_Dashboard.NewRow();

                    drRow["Date"] = dtpDate.Value.ToShortDateString();
                    drRow["PIN"] = txtPIN.Text.ToString();
                    drRow["Channel"] = cbChannel.SelectedValue.ToString();
                    drRow["Category"] = cbCategory.SelectedValue.ToString();
                    drRow["Success"] = cbSuccess.SelectedItem.ToString();
                    drRow["Comments"] = txtComments.Text.ToString();
                    drRow["EnteredDate"] = DateTime.Now;
                    drRow["EnteredBy"] = UserName;
                    drRow["VIPManager"] = cbVIPManager.SelectedValue.ToString();
                    drRow["Direction"] = cbDirection.SelectedItem.ToString();

                    bIDDataSet.DIM_VIP_Dashboard.Rows.Add(drRow);

                    this.dIM_VIP_DashboardTableAdapter.Update(bIDDataSet);

                    ClearFields();

                    btnClear.Visible = true;
                    btnDelete.Visible = true;
                    btnEdit.Visible = true;

                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvVIP_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvVIP.CurrentCell != null)
                iRow = dgvVIP.CurrentCell.RowIndex;
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowDelete = this.bIDDataSet.DIM_VIP_Dashboard.Rows[iRow];

                dtpDate.Value = (DateTime)drRowDelete["Date"];
                txtPIN.Text = drRowDelete["PIN"].ToString();
                cbChannel.SelectedValue = drRowDelete["Channel"].ToString();
                cbCategory.SelectedValue = drRowDelete["Category"].ToString();
                cbSuccess.SelectedItem = drRowDelete["Success"].ToString();
                txtComments.Text = drRowDelete["Comments"].ToString();
                cbVIPManager.SelectedValue = drRowDelete["VIPManager"].ToString();
                cbDirection.SelectedItem = drRowDelete["Direction"].ToString();

                this.dIM_VIP_DashboardTableAdapter.DeleteVIPDashboard((int)(drRowDelete["PIN"]), (DateTime)drRowDelete["Date"]);

                this.bIDDataSet.DIM_VIP_Dashboard.Rows.Remove(drRowDelete);

                this.dIM_VIP_DashboardTableAdapter.Update(bIDDataSet);

                btnClear.Visible = false;
                btnDelete.Visible = false;
                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Process.Start("mailto:BI.Service.Reports@williamhill.com.au");
        }

        private void txtPIN_TextChanged(object sender, EventArgs e)
        {
            int PIN;
            if (!int.TryParse(txtPIN.Text, out PIN) && txtPIN.Text != "")
            {
                MessageBox.Show("Entered PIN is not valid.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearFields();
        }

        private void ClearFields()
        {
            dtpDate.Value = DateTime.Today;
            txtPIN.Text = "";
            cbChannel.SelectedIndex = -1;
            cbCategory.SelectedIndex = -1;
            cbSuccess.SelectedIndex = -1;
            txtComments.Text = "";
            cbDirection.SelectedIndex = -1;
        }

        private bool IsValidated()
        {
            if (txtPIN.Text.Trim() == "")
            {
                MessageBox.Show("Entered PIN is not valid.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (txtComments.Text.Trim() == "")
            {
                MessageBox.Show("Please enter some comments.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbChannel.SelectedValue == null)
            {
                MessageBox.Show("Please select a channel.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbCategory.SelectedValue == null)
            {
                MessageBox.Show("Please select a category.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbVIPManager.SelectedValue == null)
            {
                MessageBox.Show("Please select a VIP Manager.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbSuccess.SelectedItem == null)
            {
                MessageBox.Show("Please select a success.", "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                return false;
            }
            else if (cbDirection.SelectedItem == null)
            {
                MessageBox.Show("Please select a direction.","VIP Manager",MessageBoxButtons.OK,MessageBoxIcon.Warning);
                return false;
            }
            else return true;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "VIP Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information) 
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDDataSet.DIM_VIP_Dashboard.Rows[iRow];

                    this.dIM_VIP_DashboardTableAdapter.DeleteVIPDashboard((int)(drRowDelete["PIN"]), (DateTime)drRowDelete["Date"]);

                    this.bIDDataSet.DIM_VIP_Dashboard.Rows.Remove(drRowDelete);

                    this.dIM_VIP_DashboardTableAdapter.Update(bIDDataSet);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "VIP Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void tsCopy_Click(object sender, EventArgs e)
        {
            Clipboard.SetDataObject(dgvVIP.GetClipboardContent());
        }
    }
}
