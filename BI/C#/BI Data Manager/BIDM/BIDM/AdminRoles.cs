﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class AdminRoles : Form
    {

        private int iRow;
        private int roleId;

        public AdminRoles()
        {
            InitializeComponent();
        }

        private void AdminRoles_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDMDataSet.Role' table. You can move, or remove it, as needed.
            this.roleTableAdapter.Fill(this.bIDMDataSet.Role, Global.Team);

            this.tsDelete.Click += tsDelete_Click;

        }

        private void tsDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "BI Data Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDMDataSet.Role.Rows[iRow];

                    this.roleTableAdapter.DeleteRole((int)(drRowDelete["RoleId"]));

                    this.roleTableAdapter.Update(bIDMDataSet);

                    this.roleTableAdapter.Fill(this.bIDMDataSet.Role, Global.Team);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtRoleName.Text.Trim() == "")
                {
                    MessageBox.Show("Entered Role Name is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (txtTeam.Text.Trim() == "")
                {
                    MessageBox.Show("Entered Team is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                if (roleId != 0)
                {

                    this.roleTableAdapter.UpdateRole( txtRoleName.Text.Trim(),txtTeam.Text.Trim(), roleId);

                    this.roleTableAdapter.Update(bIDMDataSet);

                }
                else
                {
                    DataRow drRow = this.bIDMDataSet.Role.NewRow();

                    drRow["RoleName"] = txtRoleName.Text.Trim();
                    drRow["Team"] = txtTeam.Text.Trim();

                    bIDMDataSet.Role.Rows.Add(drRow);

                    this.roleTableAdapter.Update(bIDMDataSet);
                }

                ClearFields();
                this.roleTableAdapter.Fill(this.bIDMDataSet.Role, Global.Team);

                btnEdit.Visible = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowEdit = this.bIDMDataSet.Role.Rows[iRow];

                txtRoleName.Text = drRowEdit["RoleName"].ToString();
                txtTeam.Text = drRowEdit["Team"].ToString();

                roleId = (int)drRowEdit["RoleId"];

                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void ClearFields()
        {
            txtRoleName.Text = "";
            txtTeam.Text = "";
            roleId = 0;
        }

        
        private void dgvChannels_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvRole.CurrentCell != null)
                iRow = dgvRole.CurrentCell.RowIndex;
        }
    }
}
