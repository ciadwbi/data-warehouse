﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class AdminChannels : Form
    {

        private int iRow;
        private int updateChannel;

        public AdminChannels()
        {
            InitializeComponent();
        }

        private void AdminChannels_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDMDataSet.Channel' table. You can move, or remove it, as needed.
            this.channelTableAdapter.Fill(this.bIDMDataSet.Channel);

            this.tsDelete.Click += tsDelete_Click;

        }

        private void tsDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "BI Data Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDMDataSet.Channel.Rows[iRow];

                    this.channelTableAdapter.DeleteChannel((int)(drRowDelete["ChannelId"]));

                    this.channelTableAdapter.Update(bIDMDataSet);

                    this.channelTableAdapter.Fill(this.bIDMDataSet.Channel);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtChannelName.Text.Trim() == "")
                {
                    MessageBox.Show("Entered Channel Name is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                if (updateChannel != 0)
                {

                    this.channelTableAdapter.UpdateChannel( txtChannelName.Text.Trim(), updateChannel);

                    this.channelTableAdapter.Update(bIDMDataSet);
                }
                else
                {
                    DataRow drRow = this.bIDMDataSet.Channel.NewRow();

                    drRow["ChannelName"] = txtChannelName.Text.Trim();

                    bIDMDataSet.Channel.Rows.Add(drRow);

                    this.channelTableAdapter.Update(bIDMDataSet);
                }

                ClearFields();
                this.channelTableAdapter.Fill(this.bIDMDataSet.Channel);

                btnEdit.Visible = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowEdit = this.bIDMDataSet.Channel.Rows[iRow];

                txtChannelName.Text = drRowEdit["ChannelName"].ToString();

                updateChannel = (int)drRowEdit["ChannelId"];

                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void ClearFields()
        {
            txtChannelName.Text = "";
            updateChannel = 0;
        }

        
        private void dgvChannels_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvChannels.CurrentCell != null)
                iRow = dgvChannels.CurrentCell.RowIndex;
        }
    }
}
