﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                BIDMDataSet.UserDataTable dtTable = this.userTableAdapter.GetUser(txtUserName.Text.Trim(), txtPassword.Text.Trim(),0);

                if (dtTable.Rows.Count == 0)
                {
                    MessageBox.Show("Login Failed", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    DialogResult = System.Windows.Forms.DialogResult.None;

                }
                else
                {                    
                    Global.UserName = txtUserName.Text.Trim();
                    Global.NameoftheUser = dtTable.Rows[0]["Name"].ToString();
                    Global.Team = dtTable.Rows[0]["Team"].ToString();
                    Global.RoleId = (int)dtTable.Rows[0]["RoleId"];
                    Global.UserId = (int)dtTable.Rows[0]["UserId"];

                    DialogResult = System.Windows.Forms.DialogResult.OK;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Login_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard_User' table. You can move, or remove it, as needed.
           // this.dIM_VIP_Dashboard_UserTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_User);

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
        }

        private void Login_Leave(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
