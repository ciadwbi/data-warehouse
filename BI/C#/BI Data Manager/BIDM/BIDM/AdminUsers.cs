﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class AdminUsers : Form
    {

        private int iRow;
        private int userId = 0;

        public AdminUsers()
        {
            InitializeComponent();
        }

        private void AdminUsers_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDMDataSet.Role' table. You can move, or remove it, as needed.
            this.roleTableAdapter.Fill(this.bIDMDataSet.Role, Global.Team);
            // TODO: This line of code loads data into the 'bIDMDataSet.User' table. You can move, or remove it, as needed.
            this.userTableAdapter.Fill(this.bIDMDataSet.User, Global.Team);
            // TODO: This line of code loads data into the 'bIDDataSet.DIM_VIP_Dashboard_User' table. You can move, or remove it, as needed.
           // this.dIM_VIP_Dashboard_UserTableAdapter.Fill(this.bIDDataSet.DIM_VIP_Dashboard_User);

            this.tsDelete.Click += tsDelete_Click;

        }

        private void tsDelete_Click(object sender, EventArgs e)
        {
            try
            {

                if (MessageBox.Show("Do you want to delete the highlighted record ?", "BI Data Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    DataRow drRowDelete = this.bIDMDataSet.User.Rows[iRow];

                    this.userTableAdapter.DeleteUser((int)drRowDelete["UserId"]);

                    this.userTableAdapter.Fill(this.bIDMDataSet.User, Global.Team);
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {

                if (txtUserName.Text.Trim() == "")
                {
                    MessageBox.Show("Entered User Name is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (txtPassword.Text.Trim() == "")
                {
                    MessageBox.Show("Password cannot be blank.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (txtReType.Text.Trim() == "")
                {
                    MessageBox.Show("Retype Password cannot be blank.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (txtPassword.Text.Trim() != txtReType.Text.Trim())
                {
                    MessageBox.Show("Entered passwords are not matched.Please Re-Enter the passwords again.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPassword.Text = "";
                    txtReType.Text = "";

                    return;
                }
                else if (txtName.Text.Trim() == "")
                {
                    MessageBox.Show("Name cannot be blank.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                else if (cmbRole.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a Role.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }

                if (userId != 0 )
                {

                    this.userTableAdapter.UpdateUser(txtUserName.Text.Trim(), txtPassword.Text.Trim(), txtName.Text.Trim(), (int)cmbRole.SelectedValue, userId);
                }
                else
                {
                    this.userTableAdapter.InsertUser(txtUserName.Text.Trim(), txtPassword.Text.Trim(), txtName.Text.Trim(),(int)cmbRole.SelectedValue);
                }

                ClearFields();
                this.userTableAdapter.Fill(this.bIDMDataSet.User, Global.Team);

                btnEdit.Visible = true;
            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                DataRow drRowEdit = this.bIDMDataSet.User.Rows[iRow];

                txtUserName.Text = drRowEdit["UserName"].ToString();
                txtPassword.Text = drRowEdit["Pword"].ToString();
                txtReType.Text = drRowEdit["Pword"].ToString();
                txtName.Text = drRowEdit["Name"].ToString();
                cmbRole.SelectedValue = (int)drRowEdit["RoleId"];

                userId = (int)drRowEdit["UserId"];

                btnEdit.Visible = false;
            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }

        private void ClearFields()
        {
            txtUserName.Text = "";
            txtPassword.Text = "";
            txtReType.Text = "";
            txtName.Text = "";
            cmbRole.SelectedIndex = -1;
            userId = 0;
        }

        private void dgvUsers_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvUsers.CurrentCell != null)
                iRow = dgvUsers.CurrentCell.RowIndex;
        }
    }
}
