﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BIDM
{
    public partial class VIPProfiler : Form
    {

        #region #### Initialization ####


        private int iProfileId, iClientId, iPIN;
        private string currentPortfolio;
        private bool bEnterPressed = false;

        public VIPProfiler()
        {
            InitializeComponent();

            lblLoggedIn.Text = "Logged in as : " + Global.NameoftheUser;

        }

        private void VIPProfiler_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bIDMDataSet.ClientProfiler' table. You can move, or remove it, as needed.
            this.clientProfilerTableAdapter.Fill(this.bIDMDataSet.ClientProfiler, Global.UserId, Global.RoleId.ToString());
            this.wealthIndexTableAdapter.Fill(this.bIDMDataSet.WealthIndex);
            this.contactFrequencyIndexTableAdapter.Fill(this.bIDMDataSet.ContactFrequencyIndex);
            this.vIPStatusTableAdapter.Fill(this.bIDMDataSet.VIPStatus);
            this.relationshipIndexTableAdapter.Fill(this.bIDMDataSet.RelationshipIndex);
            this.portfolioTableAdapter.Fill(this.bIDMDataSet.Portfolio);

            tsCopyComms.Click += tsCopyComms_Click;
            tsCopyVIP.Click += tsCopyVIP_Click;
            tsDeleteVIP.Click += btnDelete_Click;

            cbContact.Enabled = false;
            cbPortfolio.Enabled = false;
            cbRelationship.Enabled = false;
            cbStatus.Enabled = false;
            cbWealth.Enabled = false;

            ClearFields();

        }

        #endregion

        #region #### Button Clicks ####
        private void Save_Click(object sender, EventArgs e)
        {

            try
            {
                if (IsValidated())
                {
                    if (iProfileId != 0)
                    {
                        clientProfilerTableAdapter.UpdateProfile(Convert.ToInt32(txtPIN.Text.Trim()), iClientId, (int)cbContact.SelectedValue, (int)cbRelationship.SelectedValue, (int)cbWealth.SelectedValue,
                            (int)cbStatus.SelectedValue, (int)cbPortfolio.SelectedValue, DateTime.Now, Global.UserId, txtComments.Text.Trim(), iProfileId);

                        btnClear.Visible = true;
                        btnDelete.Visible = true;
                        btnEdit.Visible = true;

                    }
                    else
                    {
                        clientProfilerTableAdapter.InsertProfile(iClientId, Convert.ToInt32(txtPIN.Text.Trim()), (int)cbContact.SelectedValue, (int)cbRelationship.SelectedValue, (int)cbWealth.SelectedValue,
                           (int)cbStatus.SelectedValue, (int)cbPortfolio.SelectedValue, DateTime.Now, Global.UserId, DateTime.Now, Global.UserId, txtComments.Text.Trim());
                    }

                    ClearFields();

                    this.clientProfilerTableAdapter.Fill(this.bIDMDataSet.ClientProfiler, Global.UserId, Global.RoleId.ToString());
                }

            }


            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void btnEdit_Click(object sender, EventArgs e)
        {
            try
            {

                if (iProfileId != 0)
                {
                    if (dgvVIPProfiler.CurrentCell == null)
                    {
                        MessageBox.Show("Please select a record to edit.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }


                    DataRow drRowEdit = this.bIDMDataSet.ClientProfiler.FindByProfileId(iProfileId);

                    RetreiveClient((int)drRowEdit["PIN"]);

                    txtPIN.Text = drRowEdit["PIN"].ToString();
                }

                cbContact.Enabled = true;
                cbRelationship.Enabled = true;
                cbStatus.Enabled = true;
                cbWealth.Enabled = true;

                txtComments.Enabled = true;

                bEnterPressed = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }


        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            ClearFields();
        }
        private void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvVIPProfiler.CurrentCell == null)
                {
                    MessageBox.Show("Please select a record to delete.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }


                if (MessageBox.Show("Do you want to delete the highlighted record ?", "BI Data Manager", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
                    == System.Windows.Forms.DialogResult.Yes)
                {
                    this.clientProfilerTableAdapter.DeleteProfile(iProfileId);

                    this.clientProfilerTableAdapter.Fill(this.bIDMDataSet.ClientProfiler, Global.UserId, Global.RoleId.ToString());
                }

            }

            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void BIServiceLink_Click(object sender, EventArgs e)
        {
            Process.Start("mailto:BI.Service@williamhill.com.au");
        }
        private void tsCopyComms_Click(object sender, EventArgs e)
        {
            if (dgvCommunication.Rows.Count > 0)
                Clipboard.SetDataObject(dgvCommunication.GetClipboardContent());
        }
        private void tsCopyVIP_Click(object sender, EventArgs e)
        {

            Clipboard.SetDataObject(dgvVIPProfiler.GetClipboardContent());
        }
        #endregion

        #region #### Events ####
        private void txtPIN_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == (char)Keys.Return)
                {
                    if (txtPIN.Text.Trim() == "")
                    {
                        MessageBox.Show("Entered PIN is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    else if (Int32.Parse(txtPIN.Text.Trim()) == 0 && txtPIN.Text.Trim() != "")
                    {
                        MessageBox.Show("Entered PIN is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        txtPIN.Text = "";
                        return;
                    }

                    iProfileId = 0;

                    RetreiveClient(Convert.ToInt32(txtPIN.Text.Trim()));

                    bEnterPressed = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void txtLastContactDate_TextChanged(object sender, EventArgs e)
        {
            if (txtLastContactDate.Text.Trim() == "01-01-1900")
                txtLastContactDate.Text = "";
        }
        private void dgvVIPProfiler_SelectionChanged(object sender, EventArgs e)
        {
            if (dgvVIPProfiler.CurrentCell != null)
            {
                iProfileId = (int)dgvVIPProfiler.Rows[dgvVIPProfiler.CurrentCell.RowIndex].Cells["ProfileId"].Value;
            }
        }
        private void txtPIN_TextChanged(object sender, EventArgs e)
        {
            bEnterPressed = false;
        }

        #endregion

        #region #### Methods ####
        private void ClearFields()
        {
            cbPortfolio.SelectedIndex = -1;
            cbRelationship.SelectedIndex = -1;
            cbContact.SelectedIndex = -1;
            cbStatus.SelectedIndex = -1;
            cbWealth.SelectedIndex = -1;

            txtComments.Text = "";
            txtDOB.Text = "";
            txtDP.Text = "";
            txtFacility.Text = "";
            txtFactor.Text = "";
            txtFirstName.Text = "";
            txtInternetProfile.Text = "";
            txtLastContactDate.Text = "";
            txtOptInOut.Text = "";
            txtPIN.Text = "";
            txtSignupDate.Text = "";
            txtState.Text = "";
            txtSurname.Text = "";
            txtManagedBy.Text = "";

            iProfileId = 0;
            dgvVIPProfiler.CurrentCell = null;

            this.clientCommunicationTableAdapter.Fill(this.bIDMDataSet.ClientCommunication, 0, 0);

            cbContact.Enabled = false;
            cbRelationship.Enabled = false;
            cbStatus.Enabled = false;
            cbWealth.Enabled = false;
            cbPortfolio.Enabled = false;
            txtComments.Enabled = false;
        }
        private bool IsValidated()
        {

            try
            {
                if (txtPIN.Text.Trim() == "")
                {
                    MessageBox.Show("Entered PIN is not valid.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else if (cbPortfolio.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a portfolio.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else if (cbRelationship.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a relationship index.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else if (cbStatus.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a status.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else if (cbWealth.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a wealth index.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else if (cbContact.SelectedIndex == -1)
                {
                    MessageBox.Show("Please select a contact frequency.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else if (txtComments.Text.Trim() == "")
                {
                    MessageBox.Show("Please enter some comments.", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }


                var selectedPortfolio = (((DataRowView)cbPortfolio.SelectedItem)).Row.ItemArray[1].ToString();

                if (!bEnterPressed)
                {
                    MessageBox.Show("Please press Enter key before saving!", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    txtPIN.Focus();
                    return false;
                }
                else if ((currentPortfolio == "VIP1" || currentPortfolio == "VIP2") && selectedPortfolio.Contains("POTVIP"))
                {
                    MessageBox.Show("This client is already a VIP. Cannot change the portfolio to a potential VIP!", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else if (currentPortfolio == "BDM" && selectedPortfolio.Contains("POTVIP"))
                {
                    MessageBox.Show("This client is a BDM. Please change the managed by field in Intrabet!", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else if (currentPortfolio == "INTCLT" && selectedPortfolio.Contains("POTVIP"))
                {
                    MessageBox.Show("This client is an Internet Client. Please change the managed by field in Intrabet!", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }
                else if (currentPortfolio == "POTVIP" && (selectedPortfolio == "INTCLT" || selectedPortfolio == "BDM" || selectedPortfolio == "VIP1" || selectedPortfolio == "VIP2"))
                {
                    MessageBox.Show("This client is a Potential VIP client. Please change the managed by field in Intrabet!", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return false;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return true;
        }
        private void RetreiveClient(int PIN)
        {
            try
            {
                var dtClient = this.clientsTableAdapter.GetClientByPIN(PIN);

                if (dtClient.Rows.Count == 0)
                {
                    MessageBox.Show("Client not found! Please change the Client PIN ", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                iClientId = Convert.ToInt32(dtClient.Rows[0]["ClientId"]);

                if (dtClient.Rows.Count > 0)
                {
                    var dtClientInfo = this.clientsTableAdapter.GetData(Convert.ToInt32(dtClient.Rows[0]["AccountKey"]),
                                                                         Convert.ToInt32(dtClient.Rows[0]["PIN"]));

                    if (dtClientInfo.Rows.Count > 0)
                    {
                        txtFirstName.Text = dtClientInfo.Rows[0]["FirstName"].ToString();
                        txtSurname.Text = dtClientInfo.Rows[0]["Surname"].ToString();
                        txtDOB.Text = dtClientInfo.Rows[0]["DOB"] != DBNull.Value ? ((DateTime)dtClientInfo.Rows[0]["DOB"]).ToString("dd-MM-yyyy") : "";
                        txtSignupDate.Text = dtClientInfo.Rows[0]["SignupDate"] != DBNull.Value ? ((DateTime)dtClientInfo.Rows[0]["SignupDate"]).ToString("dd-MM-yyyy") : "";
                        txtFacility.Text = dtClientInfo.Rows[0]["Facility"] != DBNull.Value ? Convert.ToDouble(dtClientInfo.Rows[0]["Facility"]).ToString("C2") : "$0.00";
                        txtState.Text = dtClientInfo.Rows[0]["State"].ToString();
                        txtInternetProfile.Text = dtClientInfo.Rows[0]["InternetProfile"].ToString();
                        txtLastContactDate.Text = ((DateTime)dtClientInfo.Rows[0]["LastContactDate"]).ToString("dd-MM-yyyy") != "01-01-1900" ?
                                                                    ((DateTime)dtClientInfo.Rows[0]["LastContactDate"]).ToString("dd-MM-yyyy") : "N/A";
                    }
                    else
                        return;

                    var dtDPInfo = this.differentialPricingTableAdapter.GetDPData(iClientId);

                    if (dtDPInfo.Rows.Count > 0)
                        txtDP.Text = dtDPInfo.Rows[0]["DPProfile"].ToString();
                    else
                        txtDP.Text = "N/A";

                    var dtFactors = this.factorsTableAdapter.GetFactors(iClientId);

                    if (dtFactors.Rows.Count > 0)
                        txtFactor.Text = dtFactors.Rows[0]["Factored"].ToString();
                    else
                        txtFactor.Text = "N/A";

                    var dtOptIn = this.clientSubscriptionsTableAdapter.GetClientSubscriptions(iClientId);

                    if (dtOptIn.Rows.Count > 0)
                        txtOptInOut.Text = dtOptIn.Rows[0]["OptInOut"].ToString();
                    else
                        txtOptInOut.Text = "N/A";

                    var dtManagedBy = this.managedByTableAdapter.GetManagedBy(iClientId);

                    if (dtManagedBy.Rows.Count > 0)
                    {
                        txtManagedBy.Text = dtManagedBy.Rows[0]["ManagedBy"].ToString();

                        currentPortfolio = dtManagedBy.Rows[0]["Portfolio"].ToString();

                        if (currentPortfolio != "POTVIP")
                        {
                            cbPortfolio.Text = currentPortfolio;
                            cbPortfolio.Enabled = false;
                        }
                        else
                        {
                            cbPortfolio.Enabled = true;
                            cbPortfolio.SelectedIndex = -1;
                        }

                        var profileId = this.clientProfilerTableAdapter.FillProfileByPIN(PIN);

                        if (profileId != null)
                            iProfileId = (int)profileId;


                        if (iProfileId != 0)
                        {
                            DataRow drRow = this.bIDMDataSet.ClientProfiler.FindByProfileId(iProfileId);
                            if (drRow != null)
                            {
                                cbWealth.SelectedValue = (int)drRow["WealthIndexId"];
                                cbRelationship.SelectedValue = (int)drRow["RelIndexId"];
                                cbStatus.SelectedValue = (int)drRow["VIPStatusId"];
                                cbContact.SelectedValue = (int)drRow["ContFreqIndexId"];
                                txtComments.Text = drRow["Comments"].ToString();
                                cbPortfolio.SelectedValue = (int)drRow["PortfolioId"];
                            }
                        }
                        else
                        {

                            cbRelationship.SelectedIndex = -1;
                            cbContact.SelectedIndex = -1;
                            cbStatus.SelectedIndex = -1;
                            cbWealth.SelectedIndex = -1;

                            txtComments.Text = "";
                        }



                        this.clientCommunicationTableAdapter.Fill(this.bIDMDataSet.ClientCommunication, 0, PIN);
                    

                        foreach (DataGridViewRow row in dgvVIPProfiler.Rows)
                        {
                            if (row.Cells["ProfileId"].Value.Equals(iProfileId))
                            {
   
                                dgvVIPProfiler.CurrentCell = dgvVIPProfiler.Rows[row.Index].Cells[1];
                                break;
                            }
                        }

                    }
                    else
                        MessageBox.Show("Client not found! Please change the Client PIN ", "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetBaseException().Message, "BI Data Manager", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        #endregion
        
    }
}
