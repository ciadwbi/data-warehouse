### README ###

# This is the CIA Data Warehouse Development team repo

### How do I get set up? ###

This repo uses the following software:

* Git source control 
* Atlassian SourceTree (GUI over Git) 
* Visual Studio 2013 
* SQL Server Data Tools 2014 
* SQL Server Data Tools - Business Intelligence 2014
* SQL Server Management Studio Enterprise Edition 2014 

### Contribution guidelines ###

* Development Process - [https://wiki.sbet.com.au/display/CIA/Development+Process+Guidelines](https://wiki.sbet.com.au/display/CIA/Development+Process+Guidelines)
* Writing tests - Devs write unit tests before handing over to QA
* Branching - Master is for production code only, use branches for new contributions
* Merging - Code must pass QA review before merging back into Master
* JIRA - [https://jira.sbet.com.au/jira/browse/DW/](https://jira.sbet.com.au/jira/browse/DW/)
* Confluence - [https://wiki.sbet.com.au/display/CIA/Data+Warehouse](https://wiki.sbet.com.au/display/CIA/Data+Warehouse)

### Who do I talk to? ###

* Repo owner: Richard Hudson, Aaron Jackson or Edward Chen