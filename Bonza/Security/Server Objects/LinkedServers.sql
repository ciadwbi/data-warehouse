﻿EXECUTE sp_addlinkedserver @server = N'WHTABULARMODEL', @srvproduct = N'', @provider = N'MSOLAP', @datasrc = N'EQ3DBS05\TABULAR';


GO
EXECUTE sp_serveroption @server = N'WHTABULARMODEL', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'WHTABULARMODEL', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'WEBAPP270.TOMWATERHOUSE.COM', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'TW_DW_SSAS_CUBE', @srvproduct = N'', @provider = N'MSOLAP', @datasrc = N'10.2.3.33';


GO
EXECUTE sp_serveroption @server = N'TW_DW_SSAS_CUBE', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'TW_DW_SSAS_CUBE', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_addlinkedserver @server = N'SBAREP01.SBADARWIN.COM.AU', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'FBRWNPRDBCDC01', @srvproduct = N' ', @provider = N'SQLNCLI', @datasrc = N'FBRWNPRDBCDC01';


GO
EXECUTE sp_serveroption @server = N'FBRWNPRDBCDC01', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'FBRWNPRDBCDC01', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'FBRWNPRDBCDC01', @optname = N'remote proc transaction promotion', @optvalue = N'FALSE';


GO
EXECUTE sp_addlinkedserver @server = N'EQ3WNPRDBDW01', @srvproduct = N' ', @provider = N'SQLNCLI', @datasrc = N'EQ3WNPRDBDW01';


GO
EXECUTE sp_serveroption @server = N'EQ3WNPRDBDW01', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'EQ3WNPRDBDW01', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'EQ3WNPRDBDW01', @optname = N'remote proc transaction promotion', @optvalue = N'FALSE';


GO
EXECUTE sp_addlinkedserver @server = N'EQ3REP01', @srvproduct = N'SQL Server';


GO
EXECUTE sp_addlinkedserver @server = N'EQ3DEVDW01', @srvproduct = N'', @provider = N'SQLNCLI', @datasrc = N'EQ3DEVDW01.dev.sbet.com.au';


GO
EXECUTE sp_addlinkedserver @server = N'EQ3DBS05_Autonomous', @srvproduct = N' ', @provider = N'SQLNCLI', @datasrc = N'EQ3DBS05';


GO
EXECUTE sp_serveroption @server = N'EQ3DBS05_Autonomous', @optname = N'rpc', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'EQ3DBS05_Autonomous', @optname = N'rpc out', @optvalue = N'TRUE';


GO
EXECUTE sp_serveroption @server = N'EQ3DBS05_Autonomous', @optname = N'remote proc transaction promotion', @optvalue = N'FALSE';


GO
EXECUTE sp_addlinkedserver @server = N'EQ3DBS05\TABULAR', @srvproduct = N'', @provider = N'MSOLAP', @datasrc = N'EQ3DBS05\TABULAR', @catalog = N'DataWareHouse_WH_PreProd';


GO
EXECUTE sp_addlinkedserver @server = N'EQ3BIDA01', @srvproduct = N'SQL Server';

