﻿CREATE TABLE [Intrabet].[tblClientInfo_NonVolatile]
(
    [FromDate]							DATETIME2(3)	NOT NULL,
    [ToDate]							DATETIME2(3)	NOT NULL,
    [CorrectedDate]						DATETIME2(3)	NOT NULL,
    [VersionNumber]						INT				NOT NULL,
	[ClientID]							INT				NOT NULL,
	[Reference]							VARCHAR(200)	NULL,
	[VIP]								INT				NULL,
	[Country]							INT				NULL,
	[CreditLimit]						MONEY			NULL,
	[MinBet]							MONEY			NULL,
	[MaxBet]							MONEY			NULL,
	[AgentID]							INT				NULL,
	[Comments]							VARCHAR(255)	NULL,
	[StatementFrequency]				INT				NULL,
	[PhoneRebate]						BIT				NULL,
	[StartDate]							DATETIME2(3)	NULL,
	[StatementMethod]					INT				NULL,
	[ReceiveMailouts]					BIT				NULL,
	[CommissionType]					INT				NULL,
	[CommissionPercent]					MONEY			NULL,
	[ClientCommissionType]				INT				NULL,
	[ClientCommissionPercent]			MONEY			NULL,
	[IntroducedBy]						INT				NULL,
	[Handled]							INT				NULL,
	[SignupInfo]						VARCHAR(200)	NULL,
	[InternetCreditLimit]				MONEY			NULL,
	[HeardAboutUsID]					INT				NULL,
	[AvgDays]							MONEY			NULL,
	[PhoneColourID]						INT				NULL,
	[BPayNo]							INT				NULL,
	[MarketingProfile]					INT				NULL,
	[SameAddress]						BIT				NULL,
	[AllupBetLimit]						MONEY			NULL,
	[OccupationID]						INT				NULL,
	[Site]								INT				NULL,
	[ManagedBy]							INT				NULL,
	[SiteType]							INT				NULL,
	[CompetitionsOnly]					BIT				NULL,
	[DisableDeposit]					INT				NULL,
	[Channel]							INT				NULL,
	[DriversLicense]					VARCHAR(25)		NULL,
	[DisableWithdrawal]					INT				NULL,
	[DepositLimit]						MONEY			NULL,
	[DepositLimitPeriod]				INT				NULL,
	[PartialMobileSignup]				INT				NULL,
	[BonusBetBalance]					MONEY			NULL,
	[RequiredTurnoverBalance]			MONEY			NULL,
	[CB_BPAY_NO]						VARCHAR(20)		NULL,
	[CB_Client_key]						INT				NULL,
	[IsClickToCallDisabled]				BIT				NULL,
	[IsPoliWhitelisted]					BIT				NULL,
	[InPlayVIP]							BIT				NULL
)
GO

ALTER TABLE [IntraBet].[tblClientInfo_NonVolatile] SET (LOCK_ESCALATION = AUTO)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_tblClientInfo_NonVolatile] ON [Intrabet].[tblClientInfo_NonVolatile]
([ClientID] ASC, [ToDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO


CREATE NONCLUSTERED INDEX [NI_tblClientInfo_NonVolatile_FD] ON [Intrabet].[tblClientInfo_NonVolatile]
([FromDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO

CREATE NONCLUSTERED INDEX [NI_tblClientInfo_NonVolatile_TD] ON [Intrabet].[tblClientInfo_NonVolatile]
([ToDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO



