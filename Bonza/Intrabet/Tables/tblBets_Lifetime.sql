﻿CREATE TABLE [Intrabet].[tblBets_Lifetime] (
-- Metadata
	[FromDate]							DATETIME2(3)	NOT NULL,
	[ToDate]							DATETIME2(3)	NOT NULL,
	[CorrectedDate]						DATETIME2(3)	NOT NULL,
	[isNegated]							BIT				NOT NULL,	-- Bet pysically deleted, which effectively negates the effect of the previous accruals, so dummy bet version created to assert a net negative value
-- Delta Columns - primarily used to flag material changes as triggers to ETL processing
	[DeltaLeg]							SMALLINT		NOT NULL, -- impact of this version of tblBets record on overall leg count (i.e. -1, 0 or +1) - intended ad a trigger for bet request
	[DeltaValid]						SMALLINT		NOT NULL, -- impact of this version of tblBets record on valid leg count (i.e. -1, 0 or +1) - intended as a trigger for bet accept
	[DeltaSettled]						SMALLINT		NOT NULL, -- impact of this version of tblBets record on settled leg count (i.e. -1, 0 or +1) - intended as a trigger for bet win/lose/unsettled
	[DeltaAmountToWin]					MONEY			NOT NULL, -- impact of this version of tblBets record on amount placed to win
	[DeltaAmountToPlace]				MONEY			NOT NULL, -- impact of this version of tblBets record on amount placed to place
	[DeltaPayoutWin]					MONEY			NOT NULL, -- impact of this version of tblBets record on amount paid out on win
	[DeltaPayoutPlace]					MONEY			NOT NULL, -- impact of this version of tblBets record on amount paid out on place
-- Allup Lifetime (single bets populated as 1 leg Allup)
	[AllUpToDate]						DATETIME2(3)	NOT NULL, -- ToDate for the validity of this version of the tblBets record in the overall bet lifetime, as distinct from the main ToDate above which is really the Todate of applicability in the bet leg lifetime (one and the same for single bets, but different lifecycles for multis)
	[AllUpLeg]							TINYINT			NOT NULL, -- leg count for the bet as of this version of tblBets record
	[AllUpValid]						TINYINT			NOT NULL, -- valid leg count for the bet as of this version of tblBets record
	[AllUpSettled]						TINYINT			NOT NULL, -- settled leg count for the bet as of this version of tblBets record
	[AllUpAmountToWin]					MONEY			NOT NULL, -- win stake amount for the bet as of this version of tblBets record
	[AllUpAmountToPlace]				MONEY			NOT NULL, -- place stake amount for the bet as of this version of tblBets record
	[AllUpPayoutWin]					MONEY			NOT NULL, -- win paid amount for the bet as of this version of tblBets record
	[AllUpPayoutPlace]					MONEY			NOT NULL, -- place paid amount for the bet as of this version of tblBets record
-- Client Lifetime
	[ClientToDate]						DATETIME2(3)	NOT NULL, -- ToDate for the validity of this version of the tblBets record in the overall client lifetime
	[ClientPlaced]						INT				NOT NULL, -- placed count (i.e. 1 or 0) for the bet as of this version of tblBets record
	[ClientPlacedStake]					MONEY			NOT NULL, -- impact of this version of the Bet on placed stake amounts
	[ClientCancelled]					INT				NOT NULL, -- placed count (i.e. 1 or 0) for the bet as of this version of tblBets record
	[ClientCancelledStake]				MONEY			NOT NULL, -- impact of this version of the Bet on placed stake amounts
	[ClientLosing]						INT				NOT NULL, -- impact of this version of the Bet on losing counts
	[ClientLosingStake]					MONEY			NOT NULL, -- impact of this version of the Bet on losing stake amounts
	[ClientWinning]						INT				NOT NULL, -- impact of this version of the Bet on winning counts
	[ClientWinningStake]				MONEY			NOT NULL, -- impact of this version of the Bet on winning stake amounts
	[ClientPayout]						MONEY			NOT NULL,  -- impact of this version of the Bet on payout amounts
-- Derived Column from source payload to facilitate appropriate groupings for lifetime values
	[isBonusBet]						BIT				NOT NULL, -- yes/no indicator as to bonus bet or not based on whether or not FreeBetId is NULL. Used as an additional grouping to maintain independent lifetime values for bonus bets and cash bets
-- Source tblBets payload 
	[BetID]								BIGINT			NOT NULL,
	[ClientID]							INT				NOT NULL,
	[BetDate]							DATETIME2(3)	NOT NULL,
	[UserCode]							INT				NULL,
	[EventID]							INT				NULL,
	[CompetitorID]						INT				NULL,
	[AmountToWin]						MONEY			NOT NULL,
	[AmountToPlace]						MONEY			NOT NULL,
	[BetType]							INT				NULL,
	[PriceToWin]						MONEY			NULL,
	[PriceToPlace]						MONEY			NULL,
	[PayoutWin]							MONEY			NOT NULL,
	[PayoutPlace]						MONEY			NOT NULL,
	[WinOverride]						MONEY			NULL,
	[PlaceOverride]						MONEY			NULL,
	[AllUpID]							BIGINT			NULL,
	[FreeBetID]							INT				NULL,
	[Points]							MONEY			NULL,
	[Valid]								BIT				NOT NULL,
	[Settled]							BIT				NOT NULL,
	[Legacy]							BIT				NOT NULL,
	[Internet]							BIT				NOT NULL,
	[Channel]							INT				NULL,
	[IPAddress]							VARCHAR(15)		NULL,
	[ExternalBetID]						BIGINT			NULL,
	[ExternalClientID]					VARCHAR(50)		NULL,
	[ExternalChannelID]					VARCHAR(20)		NULL,
	[NPlaceGetters]						INT				NULL,
	[CB_Client_Key]						INT				NULL,
	[CB_EVENT_SK]						INT				NULL,
	[CB_Bet_ID_Transformed]				BIGINT			NULL,
	[IsLiveEvent]						BIT				NULL,
	[BestTotePct]						INT				NULL,
	[InterimWinPayout]					MONEY			NULL,
	[InterimPlacePayout]				MONEY			NULL
)
GO

ALTER TABLE [IntraBet].[tblBets_Lifetime] SET (LOCK_ESCALATION = AUTO)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_tblBets_Lifetime(A)] ON [IntraBet].[tblBets_Lifetime]
	([BetId] ASC, [ToDate] ASC, [isNegated] ASC) WITH (DATA_COMPRESSION = ROW) 
	ON Lifetime(ToDate)
GO

CREATE NONCLUSTERED INDEX [NI_tblBets_Lifetime_AllUpId] ON [IntraBet].[tblBets_Lifetime]
	([AllUpId] ASC, [AllUpToDate] ASC) INCLUDE(FromDate) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO

CREATE NONCLUSTERED INDEX [NI_tblBets_Lifetime_ClientId_isFreeBet] ON [IntraBet].[tblBets_Lifetime]
	([ClientID] ASC, [isBonusBet] ASC, [ClientToDate] ASC) INCLUDE(FromDate) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO

CREATE NONCLUSTERED INDEX [NI_tblBets_Lifetime_FD(A)] ON [IntraBet].[tblBets_Lifetime]
	([FromDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO

CREATE NONCLUSTERED INDEX [NI_tblBets_Lifetime_TD(A)] ON [IntraBet].[tblBets_Lifetime]
	([ToDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO


