﻿CREATE TABLE [Intrabet].[tblBets_First] (
    [FromDate]							DATETIME2(3)	NOT NULL,
    [ToDate]							DATETIME2(3)	NOT NULL,
    [CorrectedDate]						DATETIME2(3)	NOT NULL,
-- Derived Column from source payload to facilitate appropriate groupings for lifetime values
	[isBonusBet]						BIT				NOT NULL, -- yes/no indicator as to bonus bet or not based on whether or not FreeBetId is NULL. Used as an additional grouping to maintain independent lifetime values for bonus bets and cash bets
-- Source tblBets payload 
	[BetID]								BIGINT			NOT NULL,
	[ClientID]							INT				NOT NULL,
	[BetDate]							DATETIME2(3)	NOT NULL,
	[UserCode]							INT				NULL,
	[EventID]							INT				NULL,
	[CompetitorID]						INT				NULL,
	[AmountToWin]						MONEY			NOT NULL,
	[AmountToPlace]						MONEY			NOT NULL,
	[BetType]							INT				NULL,
	[PriceToWin]						MONEY			NULL,
	[PriceToPlace]						MONEY			NULL,
	[PayoutWin]							MONEY			NOT NULL,
	[PayoutPlace]						MONEY			NOT NULL,
	[WinOverride]						MONEY			NULL,
	[PlaceOverride]						MONEY			NULL,
	[AllUpID]							BIGINT			NULL,
	[FreeBetID]							INT				NULL,
	[Points]							MONEY			NULL,
	[Valid]								BIT				NOT NULL,
	[Settled]							BIT				NOT NULL,
	[Legacy]							BIT				NOT NULL,
	[Internet]							BIT				NOT NULL,
	[Channel]							INT				NULL,
	[IPAddress]							VARCHAR(15)		NULL,
	[ExternalBetID]						BIGINT			NULL,
	[ExternalClientID]					VARCHAR(50)		NULL,
	[ExternalChannelID]					VARCHAR(20)		NULL,
	[NPlaceGetters]						INT				NULL,
	[CB_Client_Key]						INT				NULL,
	[CB_EVENT_SK]						INT				NULL,
	[CB_Bet_ID_Transformed]				BIGINT			NULL,
	[IsLiveEvent]						BIT				NULL,
	[BestTotePct]						INT				NULL,
	[InterimWinPayout]					MONEY			NULL,
	[InterimPlacePayout]				MONEY			NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_tblBets_First(A)] ON [IntraBet].[tblBets_First]
	([ClientId] ASC, [isBonusBet] ASC) WITH (DATA_COMPRESSION = ROW) 
GO

CREATE NONCLUSTERED INDEX [NI_tblBets_First_FD(A)] ON [IntraBet].[tblBets_First]
	([FromDate] ASC) WITH (DATA_COMPRESSION = ROW)
GO


