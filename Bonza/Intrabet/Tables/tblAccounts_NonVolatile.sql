﻿CREATE TABLE [Intrabet].[tblAccounts_NonVolatile] (
    [FromDate]							DATETIME2(3)	NOT NULL,
    [ToDate]							DATETIME2(3)	NOT NULL,
    [CorrectedDate]						DATETIME2(3)	NOT NULL,
    [VersionNumber]						INT				NOT NULL,
    [AccountId]							INT				NOT NULL,
    [ClientId]							INT				NULL,
    [Ledger]							INT				NULL,
    [AccountNumber]						INT				NULL,
	[Balance]							MONEY			NULL,
	[Currency]							INT				NULL,
    [AccountEnabled]					BIT				NULL,
    [HoldingAmount]						MONEY			NULL,
    [VendorSplit]						FLOAT			NULL,
    [Status]							CHAR(1)			NULL,
    [CB_Client_Key]						INT				NULL
)
GO

ALTER TABLE [IntraBet].[tblAccounts_NonVolatile] SET (LOCK_ESCALATION = AUTO)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_tblAccounts_NonVolatile] ON [Intrabet].[tblAccounts_NonVolatile]
([AccountID] ASC, [ToDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO


CREATE NONCLUSTERED INDEX [NI_tblAccounts_NonVolatile_FD] ON [Intrabet].[tblAccounts_NonVolatile]
([FromDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO

CREATE NONCLUSTERED INDEX [NI_tblAccounts_NonVolatile_TD] ON [Intrabet].[tblAccounts_NonVolatile]
([ToDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO



