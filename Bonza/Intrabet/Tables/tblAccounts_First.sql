﻿CREATE TABLE [Intrabet].[tblAccounts_First] (
    [FromDate]							DATETIME2(3)	NOT NULL,
    [ToDate]							DATETIME2(3)	NOT NULL,
    [CorrectedDate]						DATETIME2(3)	NOT NULL,
    [AccountId]							INT				NOT NULL,
    [ClientId]							INT				NULL,
    [Ledger]							INT				NULL,
    [AccountNumber]						INT				NULL,
	[Balance]							MONEY			NULL,
	[Currency]							INT				NULL,
    [AccountEnabled]					BIT				NULL,
    [HoldingAmount]						MONEY			NULL,
    [VendorSplit]						FLOAT			NULL,
    [Status]							CHAR(1)			NULL,
    [CB_Client_Key]						INT				NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_tblAccounts_First] ON [Intrabet].[tblAccounts_First]
([AccountID] ASC) WITH (DATA_COMPRESSION = ROW)
GO


CREATE NONCLUSTERED INDEX [NI_tblAccounts_First_FD] ON [Intrabet].[tblAccounts_First]
([FromDate] ASC) WITH (DATA_COMPRESSION = ROW)
GO
