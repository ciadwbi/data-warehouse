﻿CREATE TABLE [Intrabet].[tblTransactions_Lifetime] (
-- Metadata
	[FromDate]							DATETIME2(3)	NOT NULL,
	[ToDate]							DATETIME2(3)	NOT NULL,
	[CorrectedDate]						DATETIME2(3)	NOT NULL,
	[isNegated]							BIT				NOT NULL,	-- Transaction pysically deleted or transaction code changed, which effectively negates the effect of the previous transaction code, so dummy transaction version created to asset a net negative value under the old transaction code
-- Source tblTransactions payload columns
	[BetID]								BIGINT			NULL,
	[ClientID]							INT				NULL,
	[AccountNumber]						INT				NULL,
	[TransactionDate]					DATETIME2(3)	NULL,
	[Amount]							MONEY			NOT NULL,
	[TransactionCode]					INT				NULL,
	[Notes]								VARCHAR(250)	NULL,
	[Legacy]							BIT				NULL,
	[SettlingCode]						INT				NULL,
	[TransactionID]						BIGINT			NOT NULL,
	[Channel]							INT				NULL,
	[CB_Client_key]						INT				NULL,
	[CB_Bet_ID_Transformed]				BIGINT			NULL,
-- Lifetime Columns
	[AmountDelta]						MONEY			NOT NULL, -- the actual financial impact of this version of the transaction - Equal to the transaction amount for creation or change of transaction code, negative transaction amount for any type of negation, zero for any other change
	-- The following are running totals to date, all by TransactionCode by Account; where if a transaction code is changed, the transaction is netted off the totals for the old transaction code and added to the totals for the new
	-- They allow any required complex balance to be constructed from any combination of transaction codes for each Account at any point in time
	[NumberOfTransactions]				INT				NOT NULL, -- the number of transactions without deducting negations
	[CurrentTransactions]				INT				NOT NULL, -- the net number of transactions less number that have been negated.
	[Value]								MONEY			NOT NULL, -- the value of transactions without deducting negations
	[Balance]							MONEY			NOT NULL -- the net value of all transactions less the value of negations
)
GO

ALTER TABLE [IntraBet].[tblTransactions_Lifetime] SET (LOCK_ESCALATION = AUTO)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_tblTransactions_Lifetime(A)] ON [IntraBet].[tblTransactions_Lifetime]
	([TransactionId] ASC, [TransactionCode], [isNegated] ASC, [ClientId] ASC, [AccountNumber] ASC, [ToDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO

CREATE NONCLUSTERED INDEX [NI_tblTransactions_Lifetime_ClientId_AccountNumber_TransactionCode] ON [IntraBet].[tblTransactions_Lifetime]
	([ClientID] ASC, [AccountNumber] ASC, [TransactionCode] ASC, [ToDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO

CREATE NONCLUSTERED INDEX [NI_tblTransactions_Lifetime_FD(A)] ON [IntraBet].[tblTransactions_Lifetime]
	([FromDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO

CREATE NONCLUSTERED INDEX [NI_tblTransactions_Lifetime_TD(A)] ON [IntraBet].[tblTransactions_Lifetime]
	([ToDate] ASC) WITH (DATA_COMPRESSION = ROW)
	ON Lifetime(ToDate)
GO

