﻿CREATE TABLE [Intrabet].[CompleteDate]
(
	[ToDate] [datetime2](3) NOT NULL,
	[CorrectedDate] [datetime2](3) NULL
)
