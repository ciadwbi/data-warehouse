﻿CREATE TABLE [Intrabet].[tblTransactions_First] (
    [FromDate]							DATETIME2(3)	NOT NULL,
    [ToDate]							DATETIME2(3)	NOT NULL,
    [CorrectedDate]						DATETIME2(3)	NOT NULL,
	[BetID]								BIGINT			NULL,
	[ClientID]							INT				NULL,
	[AccountNumber]						INT				NULL,
	[TransactionDate]					DATETIME2(3)	NULL,
	[Amount]							MONEY			NOT NULL,
	[TransactionCode]					INT				NULL,
	[Notes]								VARCHAR(250)	NULL,
	[Legacy]							BIT				NULL,
	[SettlingCode]						INT				NULL,
	[TransactionID]						BIGINT			NOT NULL,
	[Channel]							INT				NULL,
	[CB_Client_key]						INT				NULL,
	[CB_Bet_ID_Transformed]				BIGINT			NULL
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_tblTransactions_First(A)] ON [IntraBet].[tblTransactions_First]
	([ClientId] ASC, [AccountNumber] ASC, [TransactionCode] ASC) WITH (DATA_COMPRESSION = ROW)
GO

CREATE NONCLUSTERED INDEX [NI_tblTransactions_First_FD(A)] ON [IntraBet].[tblTransactions_First]
	([FromDate] ASC) WITH (DATA_COMPRESSION = ROW)
GO

