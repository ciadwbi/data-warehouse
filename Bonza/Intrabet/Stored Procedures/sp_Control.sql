﻿CREATE PROCEDURE [Intrabet].[sp_Control]
(
	@FromDate		datetime2(3) = NULL,
	@ToDate			datetime2(3) = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	SELECT @ToDate = CASE WHEN @ToDate > ToDate THEN @ToDate ELSE ToDate END FROM [$(Acquisition)].Intrabet.CompleteDate

	EXEC Intrabet.sp_tblClientInfo @Fromdate, @ToDate, @RowsProcessed
	EXEC Intrabet.sp_tblAccounts @Fromdate, @ToDate, @RowsProcessed
	EXEC Intrabet.sp_tblTransactions @Fromdate, @ToDate, @RowsProcessed

	MERGE Intrabet.CompleteDate t USING (SELECT @ToDate ToDate) s ON 1=1 
		WHEN MATCHED THEN UPDATE SET ToDate = s.ToDate , CorrectedDate = s.Todate
		WHEN NOT MATCHED THEN INSERT (ToDate, CorrectedDate) VALUES (s.ToDate, s.ToDate);

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

--	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
