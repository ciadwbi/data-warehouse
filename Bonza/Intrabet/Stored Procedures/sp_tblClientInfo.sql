﻿CREATE PROCEDURE [Intrabet].[sp_tblClientInfo]
(
	@FromDate		datetime2(3) = NULL,
	@ToDate			datetime2(3) = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

	BEGIN TRANSACTION CacheClientInfo

BEGIN TRY

	SELECT @FromDate = COALESCE(@FromDate, MAX(FromDate), CONVERT(datetime2(3),'1900-01-01')) FROM Intrabet.tblClientInfo_NonVolatile WHERE ToDate = CONVERT(datetime2(3),'9999-12-31')
	SELECT @ToDate = COALESCE(@ToDate, ToDate) FROM [$(Acquisition)].Intrabet.CompleteDate

	--EXEC [Control].Sp_Log	@BatchKey,@Me,'Latency','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	*
	INTO	#Latency
	FROM	[$(Acquisition)].Intrabet.Latency
	WHERE	OriginalDate > @FromDate AND OriginalDate <= @ToDate
			AND OriginalDate <> CorrectedDate
	OPTION (RECOMPILE)

	--EXEC [Control].Sp_Log	@BatchKey,@Me,'Changes','Start', @RowsProcessed = @@ROWCOUNT

	;WITH 
		Diff AS 
			(	SELECT FromDate, ClientID, Reference, VIP, Country, CreditLimit, MinBet, MaxBet, AgentID, Comments, StatementFrequency, 
									PhoneRebate, StartDate, StatementMethod, ReceiveMailouts, CommissionType, CommissionPercent, ClientCommissionType,
									ClientCommissionPercent, IntroducedBy, Handled, SignupInfo, InternetCreditLimit, HeardAboutUsID, MAX(AvgDays) AvgDays, PhoneColourID, 
									BPayNo, MarketingProfile, SameAddress, AllupBetLimit, OccupationID, Site, ManagedBy, SiteType, CompetitionsOnly, 
									DisableDeposit, Channel, DriversLicense, DisableWithdrawal, DepositLimit, DepositLimitPeriod, PartialMobileSignup,
									MAX(BonusBetBalance) BonusBetBalance, MAX(RequiredTurnOverBalance) RequiredTurnoverBalance, CB_BPAY_NO, CB_Client_key, IsClickToCallDisabled, IsPoliWhitelisted, InPlayVIP, MAX(src) src
				FROM	(	SELECT	FromDate, ClientID, Reference, VIP, Country, CreditLimit, MinBet, MaxBet, AgentID, Comments, StatementFrequency, 
									PhoneRebate, StartDate, StatementMethod, ReceiveMailouts, CommissionType, CommissionPercent, ClientCommissionType,
									ClientCommissionPercent, IntroducedBy, Handled, SignupInfo, InternetCreditLimit, HeardAboutUsID, AvgDays, PhoneColourID, 
									BPayNo, MarketingProfile, SameAddress, AllupBetLimit, OccupationID, Site, ManagedBy, SiteType, CompetitionsOnly, 
									DisableDeposit, Channel, DriversLicense, DisableWithdrawal, DepositLimit, DepositLimitPeriod, PartialMobileSignup,
									BonusBetBalance, RequiredTurnOverBalance, CB_BPAY_NO, CB_Client_key, IsClickToCallDisabled, IsPoliWhitelisted, InPlayVIP, '+' src -- NOTE: even though we are not recording every change to the volatile column (Balance) we still preserve its current value each time we record a non-volatile change as it may be useful for context - e.g. current baalnce at time account status changed 
							FROM [$(Acquisition)].IntraBet.tblClientInfo 
							WHERE FromDate > @FromDate AND FromDate <= @ToDate AND  ToDate > @FromDate
							UNION ALL
							SELECT ToDate, ClientID, Reference, VIP, Country, CreditLimit, MinBet, MaxBet, AgentID, Comments, StatementFrequency, 
									PhoneRebate, StartDate, StatementMethod, ReceiveMailouts, CommissionType, CommissionPercent, ClientCommissionType,
									ClientCommissionPercent, IntroducedBy, Handled, SignupInfo, InternetCreditLimit, HeardAboutUsID, AvgDays, PhoneColourID, 
									BPayNo, MarketingProfile, SameAddress, AllupBetLimit, OccupationID, Site, ManagedBy, SiteType, CompetitionsOnly, 
									DisableDeposit, Channel, DriversLicense, DisableWithdrawal, DepositLimit, DepositLimitPeriod, PartialMobileSignup,
									BonusBetBalance, RequiredTurnOverBalance, CB_BPAY_NO, CB_Client_key, IsClickToCallDisabled, IsPoliWhitelisted, InPlayVIP, '-' src 
							FROM [$(Acquisition)].IntraBet.tblClientinfo
							WHERE ToDate > @FromDate AND ToDate <= @ToDate
						) x
				GROUP BY FromDate, ClientID, Reference, VIP, Country, CreditLimit, MinBet, MaxBet, AgentID, Comments, StatementFrequency, 
									PhoneRebate, StartDate, StatementMethod, ReceiveMailouts, CommissionType, CommissionPercent, ClientCommissionType,
									ClientCommissionPercent, IntroducedBy, Handled, SignupInfo, InternetCreditLimit, HeardAboutUsID, PhoneColourID, 
									BPayNo, MarketingProfile, SameAddress, AllupBetLimit, OccupationID, Site, ManagedBy, SiteType, CompetitionsOnly, 
									DisableDeposit, Channel, DriversLicense, DisableWithdrawal, DepositLimit, DepositLimitPeriod, PartialMobileSignup,
									CB_BPAY_NO, CB_Client_key, IsClickToCallDisabled, IsPoliWhitelisted, InPlayVIP
				HAVING COUNT(*) <> 2
			)
	SELECT	FromDate, ISNULL(CorrectedDate,FromDate) CorrectedDate, 1 VersionNumber, 
			ClientID, Reference, VIP, Country, CreditLimit, MinBet, MaxBet, AgentID, Comments, StatementFrequency, 
			PhoneRebate, StartDate, StatementMethod, ReceiveMailouts, CommissionType, CommissionPercent, ClientCommissionType,
			ClientCommissionPercent, IntroducedBy, Handled, SignupInfo, InternetCreditLimit, HeardAboutUsID, AvgDays, PhoneColourID, 
			BPayNo, MarketingProfile, SameAddress, AllupBetLimit, OccupationID, Site, ManagedBy, SiteType, CompetitionsOnly, 
			DisableDeposit, Channel, DriversLicense, DisableWithdrawal, DepositLimit, DepositLimitPeriod, PartialMobileSignup,
			BonusBetBalance, RequiredTurnOverBalance, CB_BPAY_NO, CB_Client_key, IsClickToCallDisabled, IsPoliWhitelisted, InPlayVIP, 'I' Upsert 
	INTO	#ClientInfo
	FROM	( SELECT *, CASE COUNT(*) OVER (PARTITION BY ClientId, FromDate) WHEN 2 THEN CASE src WHEN '+' THEN 'U' ELSE NULL END ELSE CASE src WHEN '+' THEN 'I' ELSE 'D' END END act FROM Diff ) x
			LEFT JOIN #Latency l ON l.OriginalDate = x.FromDate
	WHERE	act IS NOT NULL

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Previous','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	DISTINCT ClientId
	INTO	#ClientIds
	FROM	#ClientInfo

	INSERT	#ClientInfo
	SELECT	FromDate, CorrectedDate, VersionNumber, 
			ClientID, Reference, VIP, Country, CreditLimit, MinBet, MaxBet, AgentID, Comments, StatementFrequency, 
			PhoneRebate, StartDate, StatementMethod, ReceiveMailouts, CommissionType, CommissionPercent, ClientCommissionType,
			ClientCommissionPercent, IntroducedBy, Handled, SignupInfo, InternetCreditLimit, HeardAboutUsID, AvgDays, PhoneColourID, 
			BPayNo, MarketingProfile, SameAddress, AllupBetLimit, OccupationID, Site, ManagedBy, SiteType, CompetitionsOnly, 
			DisableDeposit, Channel, DriversLicense, DisableWithdrawal, DepositLimit, DepositLimitPeriod, PartialMobileSignup,
			BonusBetBalance, RequiredTurnOverBalance, CB_BPAY_NO, CB_Client_key, IsClickToCallDisabled, IsPoliWhitelisted, InPlayVIP, 'U' Upsert
	FROM	Intrabet.tblClientInfo_NonVolatile
	WHERE	ClientId IN (SELECT ClientId FROM #ClientIds)
			AND FromDate <= @FromDate AND ToDate > @FromDate -- Want the record immediately preceding the earliest we have in the new batch collected above to act as the baseline for the timeline using the new batch - need to update the ToDate on this record to match FromDate on the new one
	OPTION(RECOMPILE)

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Timeline','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	FromDate, 
			LEAD(FromDate, 1, CONVERT(datetime2(3),'9999-12-31')) OVER (PARTITION BY ClientId ORDER BY FromDate) ToDate,
			CorrectedDate,
			SUM(VersionNumber) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) VersionNumber,
			ClientID, Reference, VIP, Country, CreditLimit, MinBet, MaxBet, AgentID, Comments, StatementFrequency, 
			PhoneRebate, StartDate, StatementMethod, ReceiveMailouts, CommissionType, CommissionPercent, ClientCommissionType,
			ClientCommissionPercent, IntroducedBy, Handled, SignupInfo, InternetCreditLimit, HeardAboutUsID, AvgDays, PhoneColourID, 
			BPayNo, MarketingProfile, SameAddress, AllupBetLimit, OccupationID, Site, ManagedBy, SiteType, CompetitionsOnly, 
			DisableDeposit, Channel, DriversLicense, DisableWithdrawal, DepositLimit, DepositLimitPeriod, PartialMobileSignup,
			BonusBetBalance, RequiredTurnOverBalance, CB_BPAY_NO, CB_Client_key, IsClickToCallDisabled, IsPoliWhitelisted, InPlayVIP, Upsert
	INTO	#Stage
	FROM	#ClientInfo

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE u 
		SET	ToDate = r.ToDate
	FROM	Intrabet.tblClientInfo_NonVolatile u
			INNER JOIN #Stage r ON r.ClientID = u.ClientId AND r.FromDate = u.FromDate AND r.Upsert = 'U'

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @RowsProcessed = @RowCount

	INSERT Intrabet.tblClientInfo_NonVolatile
		(	FromDate, ToDate, CorrectedDate, VersionNumber, 
			ClientID, Reference, VIP, Country, CreditLimit, MinBet, MaxBet, AgentID, Comments, StatementFrequency, 
			PhoneRebate, StartDate, StatementMethod, ReceiveMailouts, CommissionType, CommissionPercent, ClientCommissionType,
			ClientCommissionPercent, IntroducedBy, Handled, SignupInfo, InternetCreditLimit, HeardAboutUsID, AvgDays, PhoneColourID, 
			BPayNo, MarketingProfile, SameAddress, AllupBetLimit, OccupationID, Site, ManagedBy, SiteType, CompetitionsOnly, 
			DisableDeposit, Channel, DriversLicense, DisableWithdrawal, DepositLimit, DepositLimitPeriod, PartialMobileSignup,
			BonusBetBalance, RequiredTurnOverBalance, CB_BPAY_NO, CB_Client_key, IsClickToCallDisabled, IsPoliWhitelisted, InPlayVIP	) 
	SELECT	FromDate, ToDate, CorrectedDate, VersionNumber, 
			ClientID, Reference, VIP, Country, CreditLimit, MinBet, MaxBet, AgentID, Comments, StatementFrequency, 
			PhoneRebate, StartDate, StatementMethod, ReceiveMailouts, CommissionType, CommissionPercent, ClientCommissionType,
			ClientCommissionPercent, IntroducedBy, Handled, SignupInfo, InternetCreditLimit, HeardAboutUsID, AvgDays, PhoneColourID, 
			BPayNo, MarketingProfile, SameAddress, AllupBetLimit, OccupationID, Site, ManagedBy, SiteType, CompetitionsOnly, 
			DisableDeposit, Channel, DriversLicense, DisableWithdrawal, DepositLimit, DepositLimitPeriod, PartialMobileSignup,
			BonusBetBalance, RequiredTurnOverBalance, CB_BPAY_NO, CB_Client_key, IsClickToCallDisabled, IsPoliWhitelisted, InPlayVIP
	FROM	#Stage
	WHERE	Upsert = 'I' 
		
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'First','Start', @RowsProcessed = @RowCount

	INSERT Intrabet.tblClientInfo_First
		(	FromDate, ToDate, CorrectedDate, 
			ClientID, Reference, VIP, Country, CreditLimit, MinBet, MaxBet, AgentID, Comments, StatementFrequency, 
			PhoneRebate, StartDate, StatementMethod, ReceiveMailouts, CommissionType, CommissionPercent, ClientCommissionType,
			ClientCommissionPercent, IntroducedBy, Handled, SignupInfo, InternetCreditLimit, HeardAboutUsID, AvgDays, PhoneColourID, 
			BPayNo, MarketingProfile, SameAddress, AllupBetLimit, OccupationID, Site, ManagedBy, SiteType, CompetitionsOnly, 
			DisableDeposit, Channel, DriversLicense, DisableWithdrawal, DepositLimit, DepositLimitPeriod, PartialMobileSignup,
			BonusBetBalance, RequiredTurnOverBalance, CB_BPAY_NO, CB_Client_key, IsClickToCallDisabled, IsPoliWhitelisted, InPlayVIP	) 
	SELECT	FromDate, '9999-12-31' ToDate, CorrectedDate, 
			ClientID, Reference, VIP, Country, CreditLimit, MinBet, MaxBet, AgentID, Comments, StatementFrequency, 
			PhoneRebate, StartDate, StatementMethod, ReceiveMailouts, CommissionType, CommissionPercent, ClientCommissionType,
			ClientCommissionPercent, IntroducedBy, Handled, SignupInfo, InternetCreditLimit, HeardAboutUsID, AvgDays, PhoneColourID, 
			BPayNo, MarketingProfile, SameAddress, AllupBetLimit, OccupationID, Site, ManagedBy, SiteType, CompetitionsOnly, 
			DisableDeposit, Channel, DriversLicense, DisableWithdrawal, DepositLimit, DepositLimitPeriod, PartialMobileSignup,
			BonusBetBalance, RequiredTurnOverBalance, CB_BPAY_NO, CB_Client_key, IsClickToCallDisabled, IsPoliWhitelisted, InPlayVIP
	FROM	#Stage
	WHERE	Upsert = 'I' AND VersionNumber = 1
		
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	COMMIT TRANSACTION CacheClientInfo

--	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	ROLLBACK TRANSACTION CacheClientInfo;

--	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
