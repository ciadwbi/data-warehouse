﻿CREATE PROCEDURE [Intrabet].[sp_tblTransactions]
(
	@FromDate		datetime2(3) = NULL,
	@ToDate			datetime2(3) = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

	BEGIN TRANSACTION CacheTransaction

BEGIN TRY

	SELECT @FromDate = COALESCE(@FromDate, MAX(FromDate), CONVERT(datetime2(3),'1900-01-01')) FROM Intrabet.tblTransactions_Lifetime WHERE ToDate = CONVERT(datetime2(3),'9999-12-31')
	SELECT @ToDate = COALESCE(@ToDate, ToDate) FROM [$(Acquisition)].Intrabet.CompleteDate

	--EXEC [Control].Sp_Log	@BatchKey,@Me,'Latency','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	*
	INTO	#Latency
	FROM	[$(Acquisition)].Intrabet.Latency
	WHERE	OriginalDate > @FromDate AND OriginalDate <= @ToDate
			AND OriginalDate <> CorrectedDate
	OPTION (RECOMPILE)

	--EXEC [Control].Sp_Log	@BatchKey,@Me,'Changes','Start', @RowsProcessed = @@ROWCOUNT

	;WITH 
		Diff AS 
			(	SELECT	FromDate, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, 
						MAX(src) src,
						SUM(SUM(AmountDelta)) OVER (PARTITION BY TransactionId, TransactionCode, ClientId, AccountNumber, FromDate) AmountDelta,
						CASE COUNT(*) OVER (PARTITION BY TransactionId, TransactionCode, ClientId, AccountNumber, FromDate) WHEN 1 THEN 1 ELSE 0 END LifetimeChange
				FROM	(	SELECT	FromDate, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, Amount AmountDelta, +1 src 
							FROM	[$(Acquisition)].IntraBet.tblTransactions 
							WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND  ToDate > @FromDate
							UNION ALL
							SELECT	ToDate, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, - Amount AmountDelta, -1 src 
							FROM	[$(Acquisition)].IntraBet.tblTransactions 
							WHERE	ToDate > @FromDate AND ToDate <= @ToDate
						) x
				GROUP BY FromDate, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed
				HAVING COUNT(*) <> 2
			)
	SELECT	FromDate, ISNULL(CorrectedDate,FromDate) CorrectedDate, 
			CASE src WHEN -1 THEN 1 ELSE 0 END isNegated,										-- transactions pysically deleted or transaction code changed, which effectively negates the effect of the previous transaction code, so dummy transaction version created to asset a net negative value under the old transaction code
			BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, 
			AmountDelta,																		-- the actual financial impact of this version of the transaction - Equal to the transaction amount for creation or change of transaction code, negative transaction amount for any type of negation, zero for any other change
			CASE WHEN LifetimeChange = 1 AND src = 1 THEN 1 ELSE 0 END NumberOfTransactions,	-- the number of transactions without deducting negations
			CASE LifetimeChange WHEN 1 THEN src ELSE 0 END CurrentTransactions,					-- the net number of transactions less number that have been negated.
			CASE src WHEN +1 THEN AmountDelta ELSE 0 END Value,									-- the value of transactions without deducting negations
			CASE LifetimeChange WHEN 1 THEN AmountDelta ELSE 0 END Balance,						-- the net value of all transactions less the value of negations
			'I' Upsert
	INTO	#Transactions
	FROM	Diff x
			LEFT JOIN #Latency l ON l.OriginalDate = x.FromDate
	WHERE	src = +1 OR LifetimeChange = 1 -- only want the new version of each change unless the change involves an negative material lifetime change (which would be a negation)

	--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Previous','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	DISTINCT ClientId, AccountNumber, TransactionCode
	INTO	#Ids
	FROM	#Transactions

	INSERT	#Transactions
	SELECT	FromDate, CorrectedDate, isNegated, 
			BetID, t.ClientID, t.AccountNumber, TransactionDate, Amount, t.TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, 
			AmountDelta, NumberOfTransactions, CurrentTransactions, Value, Balance, 'U'	Upsert
	FROM	Intrabet.tblTransactions_Lifetime t
			INNER JOIN #Ids i ON i.ClientId = t.ClientId AND i.AccountNumber = t.AccountNumber AND i.TransactionCode = t.TransactionCode
	WHERE	FromDate <= @FromDate AND ToDate > @FromDate -- Want the record immediately preceding the earliest we have in the new batch collected above to act as the baseline for the timeline using the new batch - need to update the ToDate on this record to match FromDate on the new one
	OPTION(RECOMPILE)

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Timeline','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	FromDate, 
			LEAD(FromDate, 1, CONVERT(datetime2(3),'9999-12-31')) OVER (PARTITION BY ClientId, AccountNumber, TransactionCode ORDER BY FromDate, TransactionDate, TransactionId) ToDate,
			CorrectedDate, isNegated, 
			BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, 
			AmountDelta,
			SUM(NumberOfTransactions) OVER (PARTITION BY ClientId, AccountNumber, TransactionCode ORDER BY FromDate, TransactionDate, TransactionId ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) NumberOfTransactions,
			SUM(CurrentTransactions) OVER (PARTITION BY ClientId, AccountNumber, TransactionCode ORDER BY FromDate, TransactionDate, TransactionId ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) CurrentTransactions,
			SUM(Value) OVER (PARTITION BY ClientId, AccountNumber, TransactionCode ORDER BY FromDate, TransactionDate, TransactionId ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) Value,
			SUM(Balance) OVER (PARTITION BY ClientId, AccountNumber, TransactionCode ORDER BY FromDate, TransactionDate, TransactionId ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) Balance,
			Upsert
	INTO	#Stage
	FROM	#Transactions

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE u 
		SET	ToDate = r.ToDate
	FROM	Intrabet.tblTransactions_Lifetime u
			INNER JOIN #Stage r ON r.ClientId = u.ClientId AND r.AccountNumber = u.AccountNumber AND r.TransactionCode = u.TransactionCode AND r.FromDate = u.FromDate AND r.Upsert = 'U'
	WHERE	u.ToDate > @ToDate

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @RowsProcessed = @RowCount

	INSERT Intrabet.tblTransactions_Lifetime
		(	FromDate, ToDate, CorrectedDate, isNegated, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, AmountDelta, NumberOfTransactions, CurrentTransactions, Value, Balance ) 
	SELECT	FromDate, ToDate, CorrectedDate, isNegated, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, AmountDelta, NumberOfTransactions, CurrentTransactions, Value, Balance
	FROM	#Stage
	WHERE	Upsert = 'I' 
		
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'First','Start', @RowsProcessed = @RowCount

	INSERT Intrabet.tblTransactions_First
		(	FromDate, ToDate, CorrectedDate, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed	) 
	SELECT	FromDate, '9999-12-31' ToDate, CorrectedDate, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed
	FROM	#Stage
	WHERE	Upsert = 'I' AND NumberOfTransactions = 1 AND CurrentTransactions = 1 AND AmountDelta <> 0
		
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	COMMIT TRANSACTION CacheTransaction

--	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	ROLLBACK TRANSACTION CacheTransaction;

--	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
