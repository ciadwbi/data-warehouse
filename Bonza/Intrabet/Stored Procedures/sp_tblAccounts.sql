﻿CREATE PROCEDURE [Intrabet].[sp_tblAccounts]
(
	@FromDate		datetime2(3) = NULL,
	@ToDate			datetime2(3) = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

	BEGIN TRANSACTION CacheAccounts

BEGIN TRY

	SELECT @FromDate = COALESCE(@FromDate, MAX(FromDate), CONVERT(datetime2(3),'1900-01-01')) FROM Intrabet.tblAccounts_NonVolatile WHERE ToDate = CONVERT(datetime2(3),'9999-12-31')
	SELECT @ToDate = COALESCE(@ToDate, ToDate) FROM [$(Acquisition)].Intrabet.CompleteDate

	--EXEC [Control].Sp_Log	@BatchKey,@Me,'Latency','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	*
	INTO	#Latency
	FROM	[$(Acquisition)].Intrabet.Latency
	WHERE	OriginalDate > @FromDate AND OriginalDate <= @ToDate
			AND OriginalDate <> CorrectedDate
	OPTION (RECOMPILE)

	--EXEC [Control].Sp_Log	@BatchKey,@Me,'Changes','Start', @RowsProcessed = @@ROWCOUNT

	;WITH 
		Diff AS 
			(	SELECT FromDate, AccountID, ClientID, Ledger, AccountNumber, MAX(Balance) Balance, Currency, AccountEnabled, HoldingAmount, VendorSplit, Status, CB_Client_key, MAX(src) src -- NOTE: even though we are not recording every change to the volatile column (Balance) we still preserve its current value each time we record a non-volatile change as it may be useful for context - e.g. current baalnce at time account status changed
				FROM	(	SELECT FromDate, AccountID, ClientID, Ledger, AccountNumber, Balance, Currency, AccountEnabled, HoldingAmount, VendorSplit, Status, CB_Client_key, '+' src 
							FROM [$(Acquisition)].IntraBet.tblAccounts 
							WHERE FromDate > @FromDate AND FromDate <= @ToDate AND  ToDate > @FromDate
							UNION ALL
							SELECT ToDate, AccountID, ClientID, Ledger, AccountNumber, Balance, Currency, AccountEnabled, HoldingAmount, VendorSplit, Status, CB_Client_key, '-' src 
							FROM [$(Acquisition)].IntraBet.tblAccounts 
							WHERE ToDate > @FromDate AND ToDate <= @ToDate
						) x
				GROUP BY FromDate, AccountID, ClientID, Ledger, AccountNumber, Currency, AccountEnabled, HoldingAmount, VendorSplit, Status, CB_Client_key
				HAVING COUNT(*) <> 2
			)
	SELECT	FromDate, ISNULL(CorrectedDate,FromDate) CorrectedDate, 1 VersionNumber, AccountID, ClientID, Ledger, AccountNumber, Balance, Currency, AccountEnabled, HoldingAmount, VendorSplit, Status, CB_Client_key, 'I' Upsert 
	INTO	#Accounts
	FROM	( SELECT *, CASE COUNT(*) OVER (PARTITION BY AccountId, FromDate) WHEN 2 THEN CASE src WHEN '+' THEN 'U' ELSE NULL END ELSE CASE src WHEN '+' THEN 'I' ELSE 'D' END END act FROM Diff ) x
			LEFT JOIN #Latency l ON l.OriginalDate = x.FromDate
	WHERE	act IS NOT NULL

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Previous','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	DISTINCT AccountId
	INTO	#AccountIds
	FROM	#Accounts

	INSERT	#Accounts
	SELECT	FromDate, CorrectedDate, VersionNumber, AccountID, ClientID, Ledger, AccountNumber, Balance, Currency, AccountEnabled, HoldingAmount, VendorSplit, Status, CB_Client_key, 'U' Upsert
	FROM	Intrabet.tblAccounts_NonVolatile
	WHERE	AccountId IN (SELECT AccountId FROM #AccountIds)
			AND FromDate <= @FromDate AND ToDate > @FromDate -- Want the record immediately preceding the earliest we have in the new batch collected above to act as the baseline for the timeline using the new batch - need to update the ToDate on this record to match FromDate on the new one
	OPTION(RECOMPILE)

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Timeline','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	FromDate, 
			LEAD(FromDate, 1, CONVERT(datetime2(3),'9999-12-31')) OVER (PARTITION BY AccountId ORDER BY FromDate) ToDate,
			CorrectedDate,
			SUM(VersionNumber) OVER (PARTITION BY AccountId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) VersionNumber,
			AccountID, ClientID, Ledger, AccountNumber, Balance, Currency, AccountEnabled, HoldingAmount, VendorSplit, Status, CB_Client_key, Upsert
	INTO	#Stage
	FROM	#Accounts

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE u 
		SET	ToDate = r.ToDate
	FROM	Intrabet.tblAccounts_NonVolatile u
			INNER JOIN #Stage r ON r.AccountId = u.AccountId AND r.FromDate = u.FromDate AND r.Upsert = 'U'

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @RowsProcessed = @RowCount

	INSERT Intrabet.tblAccounts_NonVolatile
		(	FromDate, ToDate, CorrectedDate, VersionNumber, AccountID, ClientID, Ledger, AccountNumber, Balance, Currency, AccountEnabled, HoldingAmount, VendorSplit, Status, CB_Client_key	) 
	SELECT	FromDate, ToDate, CorrectedDate, VersionNumber, AccountID, ClientID, Ledger, AccountNumber, Balance, Currency, AccountEnabled, HoldingAmount, VendorSplit, Status, CB_Client_key
	FROM	#Stage
	WHERE	Upsert = 'I' 
		
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

--	EXEC [Control].Sp_Log	@BatchKey,@Me,'First','Start', @RowsProcessed = @RowCount

	INSERT Intrabet.tblAccounts_First
		(	FromDate, ToDate, CorrectedDate, AccountID, ClientID, Ledger, AccountNumber, Balance, Currency, AccountEnabled, HoldingAmount, VendorSplit, Status, CB_Client_key	) 
	SELECT	FromDate, '9999-12-31' ToDate, CorrectedDate, AccountID, ClientID, Ledger, AccountNumber, Balance, Currency, AccountEnabled, HoldingAmount, VendorSplit, Status, CB_Client_key
	FROM	#Stage
	WHERE	Upsert = 'I' AND VersionNumber = 1
		
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	COMMIT TRANSACTION CacheAccounts

--	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	ROLLBACK TRANSACTION CacheAccounts;

--	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
