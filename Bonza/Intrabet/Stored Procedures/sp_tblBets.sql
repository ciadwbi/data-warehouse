﻿CREATE PROCEDURE [Intrabet].[sp_tblBets]
(
	@FromDate		datetime2(3) = NULL,
	@ToDate			datetime2(3) = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

	BEGIN TRANSACTION CacheBets

BEGIN TRY

	SELECT @FromDate = COALESCE(@FromDate, MAX(FromDate), CONVERT(datetime2(3),'1900-01-01')) FROM Intrabet.tblBets_Lifetime WHERE ToDate = CONVERT(datetime2(3),'9999-12-31')
	SELECT @ToDate = COALESCE(@ToDate, ToDate) FROM [$(Acquisition)].Intrabet.CompleteDate

	--EXEC [Control].Sp_Log	@BatchKey,@Me,'Latency','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	*
	INTO	#Latency
	FROM	[$(Acquisition)].Intrabet.Latency
	WHERE	OriginalDate > @FromDate AND OriginalDate <= @ToDate
			AND OriginalDate <> CorrectedDate
	OPTION (RECOMPILE)

	--EXEC [Control].Sp_Log	@BatchKey,@Me,'Changes','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	FromDate, ToDate, BetID, ClientID, BetDate, UserCode, AmountToWin, AmountToPlace, PayoutWin, PayoutPlace, 
			AllUpID, FreeBetID, Valid, Settled, Legacy, Internet, IsLiveEvent, Channel, IPAddress,
			CASE WHEN FromDate > @FromDate THEN 1 ELSE 0 END + CASE WHEN ToDate <= @ToDate THEN 2 ELSE 0 END Scope
	INTO	#Changes
	FROM	[$(Acquisition)].IntraBet.tblBets 
	WHERE	FromDate <= @ToDate AND  ToDate > @FromDate
			AND (FromDate > @FromDate OR ToDate <= @ToDate)
			AND (AmountToWin <> 0 OR AmountToPlace <> 0)

	--EXEC [Control].Sp_Log	@BatchKey,@Me,'Unsettled AllUps','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	a.BetId, a.ChangeDate,
			CASE WHEN MAX(b.ToDate) > a.ChangeDate THEN 1 ELSE 0 END + CASE WHEN MIN(b.FromDate) < a.ChangeDate THEN 2 ELSE 0 END Scope
	INTO	#UnsettledAllUps
	FROM	(	SELECT	BetId, AllUpId, FromDate ChangeDate FROM #Changes WHERE AllUpId IS NOT NULL AND Settled = 1 AND Scope & 1 = 1
				UNION
				SELECT	BetId, AllUpId, ToDate ChangeDate FROM #Changes WHERE AllUpId IS NOT NULL AND Settled = 1 AND Scope & 2 = 2
			) a 
			INNER JOIN [$(Acquisition)].IntraBet.tblBets b WITH(FORCESEEK) 
						ON b.AllUpId IS NOT NULL AND b.Settled = 0 AND b.AllUpID = a.AllUpID 
							AND b.FromDate <= a.ChangeDate AND b.ToDate >= a.ChangeDate 
	GROUP BY a.BetId, a.ChangeDate

	--EXEC [Control].Sp_Log	@BatchKey,@Me,'Bets','Start', @RowsProcessed = @@ROWCOUNT

	;WITH 
		Diff AS 
			(	SELECT	FromDate, BetID, ClientID, BetDate, UserCode, AmountToWin, AmountToPlace, PayoutWin, PayoutPlace, AllUpID, FreeBetID, Valid, Settled, Legacy, Internet, IsLiveEvent, Channel, IPAddress, 
						MAX(src) src,
						SUM(SUM(CASE Valid WHEN 1 THEN Src ELSE 0 END)) OVER (PARTITION BY BetId, ClientId, FromDate) PlacedCountDelta,
						SUM(SUM(CASE WHEN Settled = 1 AND PayoutWin + PayoutPlace = 0 THEN Src ELSE 0 END)) OVER (PARTITION BY BetId, ClientId, FromDate) LosingCountDelta,
						SUM(SUM(CASE WHEN Settled = 1 AND PayoutWin + PayoutPlace <> 0 THEN Src ELSE 0 END)) OVER (PARTITION BY BetId, ClientId, FromDate) WinningCountDelta,
						SUM(SUM(CASE Valid WHEN 1 THEN Src * (AmountToWin + AmountToPlace) ELSE 0.00 END)) OVER (PARTITION BY BetId, ClientId, FromDate) PlacedDelta,
						SUM(SUM(CASE WHEN Settled = 1 AND PayoutWin + PayoutPlace = 0 THEN Src * (AmountToWin + AmountToPlace) ELSE 0 END)) OVER (PARTITION BY BetId, ClientId, FromDate) LosingDelta,
						SUM(SUM(CASE WHEN Settled = 1 AND PayoutWin + PayoutPlace <> 0 THEN Src * (AmountToWin + AmountToPlace) ELSE 0 END)) OVER (PARTITION BY BetId, ClientId, FromDate) WinningDelta,
						SUM(SUM(CASE WHEN Settled = 1 THEN Src * (PayoutWin + PayoutPlace) ELSE 0 END)) OVER (PARTITION BY BetId, ClientId, FromDate) PayoutDelta,
						CASE COUNT(*) OVER (PARTITION BY BetId, ClientId, FromDate) WHEN 1 THEN 1 ELSE 0 END LifetimeChange
				FROM	(	SELECT	FromDate, c.BetID, ClientID, BetDate, UserCode, AmountToWin, AmountToPlace, PayoutWin, PayoutPlace, AllUpID, FreeBetID, 
									Valid, CASE WHEN u.BetID IS NOT NULL THEN 0 ELSE Settled END Settled, Legacy, Internet, IsLiveEvent, Channel, IPAddress, +1 src 
							FROM	#Changes c
									LEFT JOIN #UnsettledAllUps u ON u.BetID = c.BetID AND u.ChangeDate = c.FromDate AND u.Scope & 1 = 1
							WHERE	c.Scope & 1 = 1
							UNION ALL
							SELECT	ToDate, c.BetID, ClientID, BetDate, UserCode, AmountToWin, AmountToPlace, PayoutWin, PayoutPlace, AllUpID, FreeBetID, 
									Valid, CASE WHEN u.BetID IS NOT NULL THEN 0 ELSE Settled END Settled, Legacy, Internet, IsLiveEvent, Channel, IPAddress, -1 src 
							FROM	#Changes c
									LEFT JOIN #UnsettledAllUps u ON u.BetID = c.BetID AND u.ChangeDate = c.ToDate AND u.Scope & 2 = 2
							WHERE	c.Scope & 2 = 2
						) x
				GROUP BY FromDate, BetID, ClientID, BetDate, UserCode, AmountToWin, AmountToPlace, PayoutWin, PayoutPlace, AllUpID, FreeBetID, Valid, Settled, Legacy, Internet, IsLiveEvent, Channel, IPAddress
				HAVING COUNT(*) <> 2
			)
	SELECT	FromDate, ISNULL(CorrectedDate,FromDate) CorrectedDate, 
			CASE src WHEN -1 THEN 1 ELSE 0 END isNegated,										-- bets pysically deleted or client changed, which effectively negates the effect of the previous records, so dummy bet version created to asset a net negative value under the old client
			BetID, ClientID, BetDate, UserCode, AmountToWin, AmountToPlace, PayoutWin, PayoutPlace, AllUpID, FreeBetID, Valid, Settled, Legacy, Internet, IsLiveEvent, Channel, IPAddress, 
			PlacedCountDelta, LosingCountDelta,	WinningCountDelta, 
			PlacedDelta, LosingDelta, WinningDelta, PayoutDelta,
			PlacedCountDelta LifetimePlacedCount, LosingCountDelta LifetimeLosingCount,	WinningCountDelta LifetimeWinningCount, 
			PlacedDelta LifetimePlaced, LosingDelta LifetimeLosing, WinningDelta LifetimeWinning, PayoutDelta LifetimePayout,
			'I' Upsert
	INTO	#Bets
	FROM	Diff x
			LEFT JOIN #Latency l ON l.OriginalDate = x.FromDate
	WHERE	src = +1 OR LifetimeChange = 1 -- only want the new version of each change unless the change involves an negative material lifetime change (which would be a negation)

	--	EXEC [Control].Sp_Log	@BatchKey,@Me,'Previous','Start', @RowsProcessed = @@ROWCOUNT

--	SELECT	DISTINCT ClientId, AccountNumber, TransactionCode
--	INTO	#Ids
--	FROM	#Transactions

--	INSERT	#Transactions
--	SELECT	FromDate, LoadDate, isNegated, 
--			BetID, t.ClientID, t.AccountNumber, TransactionDate, Amount, t.TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, 
--			AmountDelta, NumberOfTransactions, CurrentTransactions, Value, Balance, 'U'	Upsert
--	FROM	Intrabet.tblTransactions_Lifetime t
--			INNER JOIN #Ids i ON i.ClientId = t.ClientId AND i.AccountNumber = t.AccountNumber AND i.TransactionCode = t.TransactionCode
--	WHERE	FromDate <= @FromDate AND ToDate > @FromDate -- Want the record immediately preceding the earliest we have in the new batch collected above to act as the baseline for the timeline using the new batch - need to update the ToDate on this record to match FromDate on the new one
--	OPTION(RECOMPILE)

----	EXEC [Control].Sp_Log	@BatchKey,@Me,'Timeline','Start', @RowsProcessed = @@ROWCOUNT

--	SELECT	FromDate, 
--			LEAD(FromDate, 1, CONVERT(datetime2(3),'9999-12-31')) OVER (PARTITION BY ClientId, AccountNumber, TransactionCode ORDER BY FromDate, TransactionDate, TransactionId) ToDate,
--			LoadDate, isNegated, 
--			BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, 
--			AmountDelta,
--			SUM(NumberOfTransactions) OVER (PARTITION BY ClientId, AccountNumber, TransactionCode ORDER BY FromDate, TransactionDate, TransactionId ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) NumberOfTransactions,
--			SUM(CurrentTransactions) OVER (PARTITION BY ClientId, AccountNumber, TransactionCode ORDER BY FromDate, TransactionDate, TransactionId ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) CurrentTransactions,
--			SUM(Value) OVER (PARTITION BY ClientId, AccountNumber, TransactionCode ORDER BY FromDate, TransactionDate, TransactionId ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) Value,
--			SUM(Balance) OVER (PARTITION BY ClientId, AccountNumber, TransactionCode ORDER BY FromDate, TransactionDate, TransactionId ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) Balance,
--			Upsert
--	INTO	#Stage
--	FROM	#Transactions

----	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @RowsProcessed = @@ROWCOUNT

--	UPDATE u 
--		SET	ToDate = r.ToDate
--	FROM	Intrabet.tblTransactions_Lifetime u
--			INNER JOIN #Stage r ON r.ClientId = u.ClientId AND r.AccountNumber = u.AccountNumber AND r.TransactionCode = u.TransactionCode AND r.FromDate = u.FromDate AND r.Upsert = 'U'
--	WHERE	u.ToDate > @ToDate

--	SET @RowCount = @@ROWCOUNT
--	SET @RowsProcessed = @RowsProcessed + @RowCount

----	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @RowsProcessed = @RowCount

--	INSERT Intrabet.tblTransactions_Lifetime
--		(	FromDate, ToDate, LoadDate, isNegated, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, AmountDelta, NumberOfTransactions, CurrentTransactions, Value, Balance ) 
--	SELECT	FromDate, ToDate, LoadDate, isNegated, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed, AmountDelta, NumberOfTransactions, CurrentTransactions, Value, Balance
--	FROM	#Stage
--	WHERE	Upsert = 'I' 
		
--	SET @RowCount = @@ROWCOUNT
--	SET @RowsProcessed = @RowsProcessed + @RowCount

----	EXEC [Control].Sp_Log	@BatchKey,@Me,'First','Start', @RowsProcessed = @RowCount

--	INSERT Intrabet.tblTransactions_First
--		(	FromDate, ToDate, LoadDate, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed	) 
--	SELECT	FromDate, '9999-12-31' ToDate, LoadDate, BetID, ClientID, AccountNumber, TransactionDate, Amount, TransactionCode, Notes, Legacy, SettlingCode, TransactionID, Channel, CB_Client_key, CB_Bet_ID_Transformed
--	FROM	#Stage
--	WHERE	Upsert = 'I' AND NumberOfTransactions = 1 AND CurrentTransactions = 1 AND AmountDelta <> 0
		
--	SET @RowCount = @@ROWCOUNT
--	SET @RowsProcessed = @RowsProcessed + @RowCount

	COMMIT TRANSACTION CacheBets

----	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	ROLLBACK TRANSACTION CacheBets;

--	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
