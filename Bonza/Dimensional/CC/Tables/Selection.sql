﻿CREATE TABLE [CC].[Selection] (
    [SelectionKey]					INT				IDENTITY (1, 1) NOT NULL,
    [ScheduledDayKey]				INT				NOT NULL,
    [ResultedDayKey]				INT				NOT NULL,
	[AbandonedDayKey]				INT				NOT NULL,
	[ScratchedDayKey]				INT				NOT NULL,
-- Source
    [Source]						CHAR (3)		NOT NULL,
    [SourceCompetitor]				VARCHAR (100)	NOT NULL,
	[SourceJockey]					VARCHAR (50)	NOT NULL,
	[SourceForm]					VARCHAR (20)	NOT NULL,
	[SourceHistory]					VARCHAR (10)	NOT NULL,
	[SourceTrainer]					VARCHAR (50)	NULL,

	[SourceMarketID]				INT				NOT NULL, -- Natural Key

	[EventId0]						INT				NOT NULL,
    [Event0]						VARCHAR (100)	NULL,
	[EventTypeId0]					INT				NULL,
    [EventType0]					VARCHAR (50)	NULL,
	[EventSubTypeId0]				INT				NULL,
    [EventSubType0]					VARCHAR (50)	NULL,
	[SportSubTypeId0]				INT				NULL,
	[SportSubType0]					VARCHAR (50)	NULL,
	[IsLiveEvent0]					BIT				NULL,
	[VenueId0]						INT				NULL,
    [Venue0]						VARCHAR (50)	NULL,	
	[VenueType0]					VARCHAR (60)	NULL,
	[VenueEventTypeId0]				INT				NULL,
	[VenueEventType0]				VARCHAR (50)	NULL,
    [VenueStateCode0]				CHAR (3)		NULL,
    [VenueState0]					VARCHAR (50)	NULL,
    [VenueCountryCode0]				CHAR (3)		NULL,
    [VenueCountry0]					VARCHAR (50)	NULL,
    [EventId1]						INT				NULL,
    [Event1]						VARCHAR (100)	NULL,
	[EventTypeId1]					INT				NULL,
    [EventType1]					VARCHAR (50)	NULL,
	[EventSubTypeId1]				INT				NULL,
    [EventSubType1]					VARCHAR (50)	NULL,
	[SportSubTypeId1]				INT				NULL,
	[SportSubType1]					VARCHAR (50)	NULL,
	[IsLiveEvent1]					BIT				NULL,
	[VenueId1]						INT				NULL,
    [Venue1]						VARCHAR (50)	NULL,	
	[VenueType1]					VARCHAR (60)	NULL,
	[VenueEventTypeId1]				INT				NULL,
	[VenueEventType1]				VARCHAR (50)	NULL,
    [VenueStateCode1]				CHAR (3)		NULL,
    [VenueState1]					VARCHAR (50)	NULL,
    [VenueCountryCode1]				CHAR (3)		NULL,
    [VenueCountry1]					VARCHAR (50)	NULL,
    [EventId2]						INT				NULL,
    [Event2]						VARCHAR (100)	NULL,
	[EventTypeId2]					INT				NULL,
    [EventType2]					VARCHAR (50)	NULL,
	[EventSubTypeId2]				INT				NULL,
    [EventSubType2]					VARCHAR (50)	NULL,
	[SportSubTypeId2]				INT				NULL,
	[SportSubType2]					VARCHAR (50)	NULL,
	[IsLiveEvent2]					BIT				NULL,
	[VenueId2]						INT				NULL,
    [Venue2]						VARCHAR (50)	NULL,	
	[VenueType2]					VARCHAR (60)	NULL,
	[VenueEventTypeId2]				INT				NULL,
	[VenueEventType2]				VARCHAR (50)	NULL,
    [VenueStateCode2]				CHAR (3)		NULL,
    [VenueState2]					VARCHAR (50)	NULL,
    [VenueCountryCode2]				CHAR (3)		NULL,
    [VenueCountry2]					VARCHAR (50)	NULL,
    [EventId3]						INT				NULL,
    [Event3]						VARCHAR (100)	NULL,
	[EventTypeId3]					INT				NULL,
    [EventType3]					VARCHAR (50)	NULL,
	[EventSubTypeId3]				INT				NULL,
    [EventSubType3]					VARCHAR (50)	NULL,
	[SportSubTypeId3]				INT				NULL,
	[SportSubType3]					VARCHAR (50)	NULL,
	[IsLiveEvent3]					BIT				NULL,
	[VenueId3]						INT				NULL,
    [Venue3]						VARCHAR (50)	NULL,	
	[VenueType3]					VARCHAR (60)	NULL,
	[VenueEventTypeId3]				INT				NULL,
	[VenueEventType3]				VARCHAR (50)	NULL,
    [VenueStateCode3]				CHAR (3)		NULL,
    [VenueState3]					VARCHAR (50)	NULL,
    [VenueCountryCode3]				CHAR (3)		NULL,
    [VenueCountry3]					VARCHAR (50)	NULL,
    [SourceEventDate]				DATETIME		NULL,
	[SourcePrizePool]				MONEY			NULL,
	[SourceTrackCondition]			VARCHAR (20)	NULL,
    [SourceEventWeather]			VARCHAR (20)	NULL,
	[SourceDistance]				INT				NULL,
	[SourceRaceGroup]				VARCHAR (20)	NULL,
	[SourceRaceClass]				VARCHAR (150)	NULL,
	[SourceLiveStreamPerformId]		VARCHAR (50)	NULL,
	[SourceHybridPricingTemplate]	VARCHAR (50)	NULL,
	[SourceWeightType]				VARCHAR (50)	NULL,
	[SourceRaceNumber]				INT				NULL,
-- Selection (predicted outcome e.g. )
	[SelectionID]					INT				NOT NULL,
    [Selection]						VARCHAR (100)	NOT NULL,
-- Selection Type (nature of the prediction e.g. Win, Place, Each-Way, Forecast)
	[Selection_SelectionTypeKey]	INT				NOT NULL,
    [SelectionType]					VARCHAR (100)	NOT NULL,
-- Competitor (individual/team favoured by the selection e.g. )
	[Selection_CompetitorKey]		INT				NOT NULL,
	[CompetitorID]					INT				NOT NULL, -- Natural Key
	[Competitor]					VARCHAR (100)	NOT NULL,
	[SaddleNumber]					INT				NOT NULL,
	[Jockey]						VARCHAR (50)	NOT NULL,
	[Draw]							INT				NOT NULL,
	[Form]							VARCHAR (20)	NOT NULL,
	[History]						VARCHAR (10)	NOT NULL,
	[Weight]						DECIMAL (4,1)	NULL,
	[Trainer]						VARCHAR (50)	NOT NULL,
	[Scratched]						CHAR(3)			NOT NULL,
    [ScratchedDateTime]				DATETIME2 (3)	NULL,
-- Individual (a person or animal which can be a competitor in multiple events e.g. )
	[Selection_IndividualKey]		INT				NOT NULL,
	[Individual]					VARCHAR (100)	NOT NULL,
-- Team (a team or grouping which can be a competitor or contain an individual who is competitor across multiple events e.g. )
	[Selection_TeamKey]				INT				NOT NULL,
    [Team]							VARCHAR (100)	NOT NULL,
-- Market(a set of all possible outcomes on any attribute of an event which can be bet on e.g. )
	[Selection_MarketKey]			INT				NOT NULL,
	[MarketID]						INT				NOT NULL, -- Natural Key
    [Market]						VARCHAR (100)	NOT NULL,
-- Market Type(a classification of the nature of a betting market irrespective of specific event descriptions e.g. Winner, Head-to-Head, Highest Scorer, Highest first Innings Score, Winning Margin, Total Number Of Sets, Placed Finisher)
	[Selection_MarketTypeKey]		INT				NOT NULL,
    [MarketType]					VARCHAR (100)	NOT NULL,
-- Market Category(a higher level summary classification of generic market types e.g. Win, Place, Line, etc)
	[Market_MarketCategoryKey]		INT				NOT NULL,
    [MarketCategory]				VARCHAR (100)	NOT NULL,
-- Event (e.g. Australia vs England or Nadal vs Murray or Rip Curl Pro Bells Beach or Men's Long Jump or Emirates Melbourne Cup)
	[Selection_EventKey]			INT				NOT NULL,
    [Event]							VARCHAR (100)	NOT NULL,
    [ScheduledDateTime]				DATETIME2 (3)	NULL,
    [ScheduledVenueDateTime]		DATETIME2 (3)	NULL,
    [ResultedDateTime]				DATETIME2 (3)	NULL,
    [ResultedVenueDateTime]			DATETIME2 (3)	NULL,
	[AbandonedDateTime]				DATETIME2 (3)	NULL,
    [Distance]						VARCHAR (10)	NOT NULL,
    [PrizePool]						INT				NULL,
	[Grade]							VARCHAR (10)	NOT NULL, -- Listed, Group 1, Group 2, Group 3, N/A
	[RaceClass]						VARCHAR (10)	NOT NULL, -- maiden and Class 1 thru Class 6 or N/A
	[Age]							VARCHAR (10)	NOT NULL, -- 2yo thru 5yo+
	[Gender]						VARCHAR (50)	NOT NULL, -- fillies, mares, colts and gueldings
	[WeightType]					VARCHAR (100)	NOT NULL, -- handicap, open, weight for age, set weights and penalties, ratings
	[Conditions]					VARCHAR (150)	NOT NULL,
    [TrackCondition]				VARCHAR (20)	NOT NULL,
    [EventWeather]					VARCHAR (20)	NOT NULL,
	[HybridPricingTemplate]			VARCHAR (100)	NOT NULL,
	[LiveVideoStreamed]				CHAR (3)		NOT NULL,
	[LiveScoreBoard]				CHAR (3)		NOT NULL,
-- Round (e.g. 3rd Test or Semi-final or Round 3 or Final or Race 7)
	[Selection_RoundKey]			INT				NOT NULL,
	[Round]							VARCHAR (100)	NOT NULL,
    [RoundSequence]					SMALLINT		NOT NULL,
-- Competition (e.g. Ashes 2017/8 or Australian Open 2017 or WSL Mens Championship Tour 2017 or Olympics 2016 or Melbourne Cup Day 2017)
	[Selection_CompetitionKey]		INT				NOT NULL,
    [Competition]					VARCHAR (100)	NOT NULL,
    [CompetitionSeason]				VARCHAR (10)	NOT NULL,
    [CompetitionStartDate]			DATE			NULL,
    [CompetitionEndDate]			DATE			NULL,
-- Competition Class (e.g. Ashes or Australian Open or WSL Mens Championship Tour or Olympics or Melbourne Cup Day)
	[Selection_CompetitionClassKey]	INT				NOT NULL,
    [CompetitionClass]				VARCHAR (100)	NOT NULL,
-- Competition Type (e.g. International or Grand Slam or WSL Championship Tour or World Games or Spring Carnival)
	[Selection_CompetitionTypeKey]	INT				NOT NULL,
    [CompetitionType]				VARCHAR (100)	NOT NULL,
-- Venue (e.g. SCG or Melbourne Park or Bells Beach or Rio de Janeiro or Flemington)
	[Selection_VenueKey]			INT				NOT NULL,
    [Venue]							VARCHAR (50)	NOT NULL,
    [VenueType]						VARCHAR (60)	NOT NULL,
-- Venue State (e.g. NSW or VIC or N/A)
	[Selection_VenueStateKey]		INT				NOT NULL,
    [VenueStateCode]				VARCHAR (3)		NOT NULL,
    [VenueState]					VARCHAR (30)	NOT NULL,
-- Venue Country (e.g. Australia or Brazil)
	[Selection_VenueCountryKey]		INT				NOT NULL,
    [VenueCountryCode]				VARCHAR (3)		NOT NULL,
    [VenueCountry]					VARCHAR (40)	NOT NULL,
-- SubClass (e.g. Test Cricket or Mens or Long Jump or Flat Race)
    [Selection_SubClassKey]			SMALLINT		NOT NULL,
    [SubClassCode]					VARCHAR (10)	NOT NULL,
    [SubClass]						VARCHAR (32)	NOT NULL,
-- Class (e.g. Cricket or Tennis or Surfing or Track & Field or Horse)
    [Selection_ClassKey]			SMALLINT		NOT NULL,
    [ClassCode]						VARCHAR (4)		NOT NULL,
    [Class]							VARCHAR (32)	NOT NULL,
-- SuperClass (e.g. Sports or Racing)
    [Selection_SuperClassKey]		SMALLINT		NOT NULL,
    [SuperClassCode]				VARCHAR (4)		NOT NULL,
    [SuperClass]					VARCHAR (32)	NOT NULL,
-- Metadata
    [FirstDate]						DATETIME2 (3)	NOT NULL,
    [FromDate]						DATETIME2 (3)	NOT NULL,
    [ToDate]						DATETIME2 (3)	NOT NULL,
    [CreatedDate]					DATETIME2 (7)	NOT NULL,
    [CreatedBatchKey]				INT				NOT NULL,
    [CreatedBy]						VARCHAR (100)	NOT NULL,
    [ModifiedDate]					DATETIME2 (7)	NOT NULL,
    [ModifiedBatchKey]				INT				NOT NULL,
    [ModifiedBy]					VARCHAR (100)	NOT NULL
);
GO

CREATE CLUSTERED COLUMNSTORE INDEX [CC_Selection] ON [CC].[Selection] 
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Selection_PK]
ON [CC].[Selection]([SelectionKey])
GO


--CREATE NONCLUSTERED INDEX [NI_Selection_ModifiedBatchKey]
--    ON [CC].[Event]([ModifiedBatchKey] ASC) 
--	INCLUDE (EventId, [Competition]) WITH (DATA_COMPRESSION = ROW);
--GO
--CREATE UNIQUE NONCLUSTERED INDEX [NI-NK-Event]
--    ON [CC].[Event]([EventId] ASC, [Source] ASC)
--    INCLUDE([EventKey]) WITH (DATA_COMPRESSION = ROW);
--GO
--CREATE NONCLUSTERED INDEX [NI_Selection_ScheduledDayKey] 
--	ON [CC].[Event] ([ScheduledDayKey] ASC) 
--	INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW)
--GO
--CREATE NONCLUSTERED INDEX [NI_Selection_ResultedDayKey] 
--	ON [CC].[Event] ([ResultedDayKey] ASC) 
--	INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW)
--GO
--CREATE NONCLUSTERED INDEX [NI_Selection_AbandonedDayKey] 
--	ON [CC].[Event] ([EventAbandonedDayKey] ASC)
--	INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW)
--GO
--CREATE NONCLUSTERED INDEX [NI_Selection_ClassKey] 
--	ON [CC].[Event] ([ClassKey] ASC) 
--	INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW)
--GO
