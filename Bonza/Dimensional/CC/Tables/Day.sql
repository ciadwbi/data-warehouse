﻿CREATE TABLE CC.[Day]
(	[DayKey] [int] IDENTITY(1,1) NOT NULL,
	[MasterDayKey] [int] NOT NULL,
	[AnalysisDayKey] [int] NOT NULL,
	[OpeningMasterDayKey] [int] NOT NULL DEFAULT - 1,
	[OpeningAnalysisDayKey] [int] NOT NULL DEFAULT - 1,
	[ClosingMasterDayKey] [int] NOT NULL DEFAULT - 1,
	[ClosingAnalysisDayKey] [int] NOT NULL DEFAULT - 1,
	[MasterDayText] [char](10) NOT NULL,
	[AnalysisDayText] [char](10) NOT NULL,
	[MasterDayDate] [date] NULL,
	[AnalysisDayDate] [date] NULL,
	[FromDate] [datetime2](3) NOT NULL,
	[ToDate] [datetime2](3) NOT NULL,
	[DayToDate] [datetime2](3) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NOT NULL,
 CONSTRAINT [CI_Day] PRIMARY KEY CLUSTERED ([DayKey] ASC)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Day_FromDateToDate] 
ON [CC].[Day] ([FromDate] ASC, [ToDate] ASC) INCLUDE ([DayKey])
GO

