﻿CREATE TABLE [CC].[Class] (
    [ClassKey]				SMALLINT      IDENTITY (1, 1) NOT NULL,
    [HashKey]				BINARY(16)	   NOT NULL,
    [Source]				CHAR (3)      NOT NULL,
	[Keyword]				VARCHAR(255)  NULL,
    [Class_SubClassKey]		SMALLINT      NOT NULL,
    [SubClassId]			VARCHAR (30)  NULL,
    [SourceSubClass]		VARCHAR (50)  NULL,
    [SubClassCode]			VARCHAR (3)   NOT NULL,
    [SubClass]				VARCHAR (32)  NOT NULL,
    [Class_ClassKey]		SMALLINT      NOT NULL,
    [ClassId]				VARCHAR (30)  NOT NULL,
    [SourceClass]			VARCHAR (50)  NULL,
    [ClassCode]				VARCHAR (4)   NOT NULL,
    [Class]					VARCHAR (32)  NOT NULL,
    [ClassIcon]				IMAGE         NULL,
    [Class_SuperClassKey]	SMALLINT      NOT NULL,
    [SuperClass]			VARCHAR (32)  NOT NULL,
    [FirstDate]				DATETIME2 (3) NOT NULL,
    [FromDate]				DATETIME2 (3) NOT NULL,
    [ToDate]				DATETIME2 (3) NOT NULL,
	[CreatedDate]			DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]		INT           NOT NULL,
    [CreatedBy]				VARCHAR (32)  NOT NULL,
    [ModifiedDate]			DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey]		INT           NOT NULL,
    [ModifiedBy]			VARCHAR (32)  NOT NULL,
	[Increment] [int] NULL,
    CONSTRAINT [CI_Class_PK] PRIMARY KEY CLUSTERED ([ClassKey] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Class_HK]
    ON [CC].[Class]([HashKey] ASC)
    INCLUDE([ClassKey]);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Class_NK]
    ON [CC].[Class]([ClassId] ASC, [SubClassId] ASC, [Keyword] ASC, [Source] ASC)
    INCLUDE([ClassKey]);
GO

