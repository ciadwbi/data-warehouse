CREATE TABLE [CC].[KPI](
-- Primary Keys
	[ContractKey] [int] NOT NULL,
	[AccountKey] [int] NOT NULL, -- Has to be part of primary key for account transfers - same contractkey, same timestamp, different accounts
	[DayKey] [int] NOT NULL,
	[MasterTimestamp] datetime2(3) NOT NULL,
-- Timestamps
	[AnalysisTimestamp] datetime2(3) NULL, -- Placeholder - column value not currently available in KPI Staging
-- Dimension Foreign Keys
	[MasterTimeKey] [int] NOT NULL,
	[AnalysisTimeKey] [int] NOT NULL,
	[AccountStatusKey] [int] NOT NULL,
	[StructureKey] [int] NOT NULL,
	[BetTypeKey] [smallint] NOT NULL,
	[LegTypeKey] [smallint] NOT NULL,
	[ChannelKey] [smallint] NOT NULL,
	[ActionChannelKey] [smallint] NOT NULL,
	[CampaignKey] [smallint] NOT NULL,
	[MarketKey] [int] NOT NULL,
	[EventKey] [int] NOT NULL,
	[ClassKey] [smallint] NOT NULL,
	[UserKey] [smallint] NOT NULL,
	[InstrumentKey] [int] NOT NULL,
	[InPlayKey] [smallint] NOT NULL,
-- Registrations
	[AccountsOpened] [int] NULL,
-- Deposits
	[FirstTimeDepositors] [int] NULL,
	[DepositsRequestedCount] [bigint] NULL,
	[DepositsRequested] [money] NOT NULL,
	[DepositsCompletedCount] [bigint] NULL,
	[DepositsCompleted] [money] NOT NULL,
-- Withdrawals
	[WithdrawalsRequestedCount] [bigint] NULL,
	[WithdrawalsRequested] [money] NOT NULL,
	[WithdrawalsCompletedCount] [bigint] NULL,
	[WithdrawalsCompleted] [money] NOT NULL,
-- Placement
	[FirstTimeBettors] [int] NULL,
	[BettorsPlaced] [int] NULL,
	[BonusBettorsPlaced] [int] NULL,
	[BetsPlaced] [int] NULL,
	[BonusBetsPlaced] [int] NULL,
	[ContractsPlaced] [int] NULL,
	[BonusContractsPlaced] [int] NULL,
	[PlayDaysPlaced] [bigint] NULL,
	[BonusPlayDaysPlaced] [bigint] NULL,
	[PlayFiscalWeeksPlaced] [bigint] NULL,
	[BonusPlayFiscalWeeksPlaced] [bigint] NULL,
	[PlayWeeksPlaced] [bigint] NULL,
	[BonusPlayWeeksPlaced] [bigint] NULL,
	[PlayFiscalMonthsPlaced] [bigint] NULL,
	[BonusPlayFiscalMonthsPlaced] [bigint] NULL,
	[PlayMonthsPlaced] [bigint] NULL,
	[BonusPlayMonthsPlaced] [bigint] NULL,
	[TurnoverPlaced] [money] NOT NULL,
	[BonusTurnoverPlaced] [money] NULL,
-- Double Down
	[Fees] [money] NOT NULL,
-- Settlements
	[BettorsSettled] [int] NULL,
	[BonusBettorsSettled] [int] NULL,
	[BetsSettled] [int] NULL,
	[BonusBetsSettled] [int] NULL,
	[ContractsSettled] [int] NULL,
	[BonusContractsSettled] [int] NULL,
	[PlayDaysSettled] [bigint] NULL,
	[BonusPlayDaysSettled] [bigint] NULL,
	[PlayFiscalWeeksSettled] [bigint] NULL,
	[BonusPlayFiscalWeeksSettled] [bigint] NULL,
	[PlayWeeksSettled] [bigint] NULL,
	[BonusPlayWeeksSettled] [bigint] NULL,
	[PlayFiscalMonthsSettled] [bigint] NULL,
	[BonusPlayFiscalMonthsSettled] [bigint] NULL,
	[PlayMonthsSettled] [bigint] NULL,
	[BonusPlayMonthsSettled] [bigint] NULL,
	[TurnoverSettled] [money] NOT NULL,
	[BonusTurnoverSettled] [money] NULL,
	[Payout] [money] NOT NULL,
	[BonusPayout] [money] NOT NULL,
	[GrossWin] [money] NOT NULL,
	[GrossWinResettledAdjustment] [money] NOT NULL,
	[BonusGrossWin] [money] NOT NULL,
-- Cashout
	[BettorsCashedOut] [int] NULL,
	[BetsCashedOut] [int] NULL,
	[Cashout] [money] NOT NULL,
	[CashoutDifferential] [money] NOT NULL,
-- Others
	[Promotions] [money] NOT NULL,
	[Transfers] [money] NOT NULL,
	[Adjustments] [money] NOT NULL,
	[NetRevenue] [money] NOT NULL,
	[NetRevenueAdjustment] [money] NULL,
-- Rewards
	[FirstTimeAccruers] [int] NULL,
	[RewardAccruals] [int] NULL,
	[RewardAccruers] [int] NULL,
	[RewardAccrualPoints] [money] NOT NULL,
	[FirstTimeBonusRedeemers] [int] NULL,
	[RewardBonusRedemptions] [int] NULL,
	[RewardBonusRedeemers] [int] NULL,
	[RewardBonusRedeemedPoints] [money] NOT NULL,
	[RewardBonusRedeemed] [money] NOT NULL,
	[FirstTimeVelocityRedeemers] [int] NULL,
	[RewardVelocityRedemptions] [int] NULL,
	[RewardVelocityRedeemers] [int] NULL,
	[RewardVelocityRedeemedPoints] [money] NOT NULL,
	[RewardVelocityRedeemed] [money] NOT NULL,
	[RewardVelocityCost] [money] NOT NULL,
	[FirstTimeRedeemers] [int] NULL,
	[RewardRedeemers] [int] NULL,
	[RewardAdjustmentPoints] [money] NOT NULL,
-- Betbacks
	[BetbackGrossWin] [money] NOT NULL,
-- Metadata
	[FromDate] [datetime2](3) NOT NULL,
	[MappingId] [int] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL
)
GO

CREATE CLUSTERED COLUMNSTORE INDEX [CC_KPI] ON [CC].[KPI]  ON [DayKey_Yearly] ([DayKey])
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_KPI_PK]
ON [CC].[KPI](
	[ContractKey] ASC,
	[AccountKey] ASC,
	[MasterTimestamp] ASC,
	[DayKey] ASC,
	[CreatedBatchKey] ASC
)
GO

--CREATE NONCLUSTERED INDEX [NI_KPI_MarketKey]
--ON [CC].[KPI] ([MarketKey] ASC)
--GO

--CREATE NONCLUSTERED INDEX [NI_KPI_DayKey] 
--ON [CC].[KPI] ([DayKey] ASC)
--GO

--CREATE NONCLUSTERED INDEX [NI_KPI_StructureKey] 
--ON [CC].[KPI] ([StructureKey] ASC)
--GO

--CREATE NONCLUSTERED INDEX [NI_KPI_AccountKey] 
--ON [CC].[KPI] ([AccountKey] ASC)
--GO

--CREATE NONCLUSTERED INDEX [NI_KPI_ContractKey] 
--ON [CC].[KPI] ([ContractKey] ASC)
--GO

