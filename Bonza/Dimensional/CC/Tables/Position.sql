﻿CREATE TABLE [CC].[Position] (
	[AccountKey] [int] NOT NULL,
	[AccountStatusKey] [int] NOT NULL,
	[StructureKey] [int] NOT NULL,
	[DayKey] [int] NOT NULL,
	[FirstAccrualDayKey] [int] NOT NULL,
	[FirstAccrualBetId] [bigint] NULL,
	[FirstBonusRedemptionDayKey] [int] NOT NULL,
	[FirstVelocityRedemptionDayKey] [int] NOT NULL,
	[LastAccrualDayKey] [int] NOT NULL,
	[LastAccrualBetId] [bigint] NULL,
	[LastBonusRedemptionDayKey] [int] NOT NULL,
	[LastVelocityRedemptionDayKey] [int] NOT NULL,
	[PointsBalance] [money] NOT NULL,
	[LifetimePoints] [money] NOT NULL,
	[AccrualBalance] [money] NOT NULL,
	[NumberOfAccruals] [int] NOT NULL,
	[LifetimeAccrual] [money] NOT NULL,
	[NumberOfBonusRedemptions] [int] NOT NULL,
	[LifetimeBonusRedemption] [money] NOT NULL,
	[NumberOfVelocityRedemptions] [int] NOT NULL,
	[LifetimeVelocityRedemption] [money] NOT NULL,
	[LifetimeRewardCredit] [money] NOT NULL,
	[LifetimeRewardDebit] [money] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NULL
)
GO

CREATE CLUSTERED COLUMNSTORE INDEX [CC_Position] ON [CC].[Position]  ON [DayKey_Yearly] ([DayKey])
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Position_PK]
ON [CC].[Position]
(
	[DayKey] ASC, 
	[AccountKey] ASC
)
GO

	