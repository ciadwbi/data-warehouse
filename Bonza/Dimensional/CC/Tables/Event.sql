﻿CREATE TABLE [CC].[Event] (
    [EventKey]                        INT                IDENTITY (1, 1) NOT NULL,
    [ClassKey]                        SMALLINT           NULL,
    [ScheduledDayKey]                 INT                NULL,
    [ResultedDayKey]                  INT                NULL,
	[EventAbandonedDayKey]            INT                NULL,
-- Source Event
	[SourceEventId]                   INT                NOT NULL,
    [Source]                          CHAR (3)           NOT NULL,
    [SourceEvent]					  VARCHAR (255)      NULL,
    [ParentEventId]					  INT                NULL,
    [ParentEvent]					  VARCHAR (255)      NULL,
    [GrandparentEventId]			  INT                NULL,
    [GrandparentEvent]				  VARCHAR (255)      NULL,
    [GreatGrandparentEventId]		  INT                NULL,
    [GreatGrandparentEvent]			  VARCHAR (255)      NULL,
    [SourceEventDate]                 DATETIME           NULL,
	[SourcePrizePool]                 INT                NULL,
	[SourceTrackCondition]            VARCHAR (32)       NULL,
    [SourceEventWeather]              VARCHAR (32)       NULL,
	[SourceSubSportType]			  VARCHAR (50)		 NULL,
	[SourceDistance]				  INT				 NULL,
	[SourceRaceGroup]				  VARCHAR (50)		 NULL,
	[SourceRaceClass]				  VARCHAR (200)		 NULL,
	[SourceEventAbandoned]			  INT			     NULL,	
	[SourceLiveStreamPerformId]		  VARCHAR (50)		 NULL,
	[SourceHybridPricingTemplate]	  VARCHAR (100)		 NULL,
	[SourceWeightType]				  VARCHAR (100)		 NULL,
	[SourceRaceNumber]				  INT				 NULL,
	[SourceVenueId]		              INT                NULL,
    [SourceVenue]			          VARCHAR (255)      NULL,	
	[SourceVenueType]                 VARCHAR (10)       NULL,
    [SourceVenueStateCode]            CHAR (3)           NULL,
    [SourceVenueState]                VARCHAR (32)       NULL,
    [SourceVenueCountryCode]          CHAR (3)           NULL,
    [SourceVenueCountry]              VARCHAR (32)       NULL,
	[EventTypeId]					  INT				 NULL,
	[EventType]						  VARCHAR (50)		 NULL,
	[SportSubTypeId]				  INT				 NULL,
	[SportSubType]					  VARCHAR (50)		 NULL,
	[VenueEventTypeId]				  INT				 NULL,
	[VenueEventType]				  VARCHAR (50)		 NULL,
-- Event
	[Event_EventKey]				  INT				 NOT NULL,
    [EventId]                         INT                NOT NULL,
    [Event]                           VARCHAR (255)      NULL,
    [ScheduledDateTime]               DATETIME           NULL,
    [ScheduledVenueDateTime]          DATETIME           NULL,
    [ResultedDateTime]                DATETIME           NULL,
    [ResultedVenueDateTime]           DATETIME           NULL,
    [Grade]                           VARCHAR (255)      NULL,
    [Distance]                        VARCHAR (10)       NULL,
    [PrizePool]                       INT                NULL,
	[RaceGroup]						  VARCHAR (50)		 NULL,
	[RaceClass]						  VARCHAR (200)		 NULL,
	[HybridPricingTemplate]			  VARCHAR (100)		 NULL,
	[WeightType]					  VARCHAR (100)		 NULL,
    [TrackCondition]                  VARCHAR (32)       NULL,
    [EventWeather]                    VARCHAR (32)       NULL,
	[EventAbandoned]				  VARCHAR (10)	     NULL,
	[EventAbandonedDateTime]		  DATETIME2 (3)		 NULL,
	[LiveVideoStreamed]			      VARCHAR (10)	     NULL,
	[LiveScoreBoard]				  VARCHAR (10)		 NULL,
-- Round
	[Event_RoundKey]				  INT				 NOT NULL,
	[Round]                           VARCHAR (255)      NULL,
    [RoundSequence]                   SMALLINT           NULL,
-- Competition
	[Event_CompetitionKey]			  INT				 NOT NULL,
    [Competition]                     VARCHAR (255)      NULL,
    [CompetitionSeason]               VARCHAR (32)       NULL,
    [CompetitionStartDate]            DATE               NULL,
    [CompetitionEndDate]              DATE               NULL,
-- Venue
	[Event_VenueKey]				  INT				 NOT NULL,
    [VenueId]		                  INT                NULL,
    [Venue]                           VARCHAR (100)      NULL,
    [VenueType]                       VARCHAR (100)      NULL,
-- Venue State
	[Event_VenueStateKey]			  INT				 NOT NULL,
    [VenueStateCode]                  VARCHAR (3)        NULL,
    [VenueState]                      VARCHAR (32)       NULL,
-- Venue Country
	[Event_VenueCountryKey]			  INT				 NOT NULL,
    [VenueCountryCode]                VARCHAR (3)        NULL,
    [VenueCountry]                    VARCHAR (32)       NULL,
-- SubClass
    [Event_SubClassKey]				  SMALLINT			 NOT NULL,
    [SubClassId]					  VARCHAR (30)		 NULL,
    [SubClassCode]					  VARCHAR (3)		 NOT NULL,
    [SubClass]						  VARCHAR (32)		 NOT NULL,
-- Class
    [Event_ClassKey]				  SMALLINT			 NOT NULL,
    [ClassId]						  VARCHAR (30)		 NOT NULL,
    [ClassCode]						  VARCHAR (4)		 NOT NULL,
    [Class]							  VARCHAR (32)		 NOT NULL,
-- SuperClass
    [Event_SuperClassKey]			  SMALLINT			 NOT NULL,
    [SuperClass]					  VARCHAR (32)		 NOT NULL,
-- Metadata
    [FirstDate]                       DATETIME2 (3)      NOT NULL,
    [FromDate]                        DATETIME2 (3)      NOT NULL,
    [ToDate]		                  DATETIME2 (3)      NOT NULL,
    [CreatedDate]                     DATETIME2 (7)      NOT NULL,
    [CreatedBatchKey]                 INT                NOT NULL,
    [CreatedBy]                       VARCHAR (100)      NOT NULL,
    [ModifiedDate]                    DATETIME2 (7)      NOT NULL,
    [ModifiedBatchKey]                INT                NOT NULL,
    [ModifiedBy]                      VARCHAR (100)      NOT NULL
);
GO

CREATE CLUSTERED COLUMNSTORE INDEX [CC_Event] ON [CC].[Event] 
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Event_PK]
ON [CC].[Event]([EventKey])
GO


--CREATE NONCLUSTERED INDEX [NI_Event_ModifiedBatchKey]
--    ON [CC].[Event]([ModifiedBatchKey] ASC) 
--	INCLUDE (EventId, [Competition]) WITH (DATA_COMPRESSION = ROW);
--GO
--CREATE UNIQUE NONCLUSTERED INDEX [NI-NK-Event]
--    ON [CC].[Event]([EventId] ASC, [Source] ASC)
--    INCLUDE([EventKey]) WITH (DATA_COMPRESSION = ROW);
--GO
--CREATE NONCLUSTERED INDEX [NI_Event_ScheduledDayKey] 
--	ON [CC].[Event] ([ScheduledDayKey] ASC) 
--	INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW)
--GO
--CREATE NONCLUSTERED INDEX [NI_Event_ResultedDayKey] 
--	ON [CC].[Event] ([ResultedDayKey] ASC) 
--	INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW)
--GO
--CREATE NONCLUSTERED INDEX [NI_Event_AbandonedDayKey] 
--	ON [CC].[Event] ([EventAbandonedDayKey] ASC)
--	INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW)
--GO
--CREATE NONCLUSTERED INDEX [NI_Event_ClassKey] 
--	ON [CC].[Event] ([ClassKey] ASC) 
--	INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW)
--GO
