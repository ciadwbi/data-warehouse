﻿CREATE TABLE [CC].[Market] (
    [MarketKey]						INT				NOT NULL, --IDENTITY (1, 1) NOT NULL, -- Will initially copy EventKey until cut loose to go its own way and take the lead
    [OpenedDayKey]					INT				NOT NULL,
    [ClosedDayKey]					INT				NOT NULL,
    [SettledDayKey]					INT				NOT NULL,
    [ScheduledDayKey]				INT				NOT NULL,
    [ResultedDayKey]				INT				NOT NULL,
	[AbandonedDayKey]				INT				NOT NULL,
-- Market(a set of all possible outcomes on any attribute of an event which can be bet on e.g. )
	[MarketID]						INT				NOT NULL,			-- Source Key - Intrabet: EventId
    [Source]						CHAR (3)		NOT NULL,
    [Market]						VARCHAR (100)	NOT NULL,			-- Derived
    [Opened]						CHAR (3)		NOT NULL,			--Intrabet.tblEvents
    [FirstOpenedDateTime]			DATETIME2 (3)	NULL,				--Intrabet.tblEvents
    [FirstOpenedVenueDateTime]		DATETIME2 (3)	NULL,				--Derived
	[Closed]						CHAR (3)		NOT NULL,			--Intrabet.tblEvents
    [LastClosedDateTime]			DATETIME2 (3)	NULL,				--Intrabet.tblEvents
    [LastClosedVenueDateTime]		DATETIME2 (3)	NULL,				--Derived
	[Settled]						CHAR (3)		NOT NULL,			--Intrabet.tblEvents
    [LastSettledDateTime]			DATETIME2 (3)	NULL,				--Intrabet.tblEvents
    [LastSettledVenueDateTime]		DATETIME2 (3)	NULL,				--Derived
	[MaxWinnings]					MONEY			NULL,				--Intrabet.tblEvents
	[MaxStake]						MONEY			NULL,				--Intrabet.tblEvents
	[LevyRate]						VARCHAR (30)	NOT NULL,			-- Derived
-- Market Type(a classification of the nature of a betting market irrespective of specific market/event descriptions e.g. Winner, Head-to-Head, Highest Scorer, Highest first Innings Score, Winning Margin, Total Number Of Sets, Placed Finisher)
	[Market_MarketTypeKey]			INT				NOT NULL,
    [MarketType]					VARCHAR (100)	NOT NULL,			-- Derived
	[LiveMarket]					CHAR (3)		NOT NULL,			--Derived
	[FutureMarket]					CHAR (3)		NOT NULL,			--Intrabet.tblEvents
-- Market Category(a higher level summary classification of generic market types e.g. Win, Place, Line, etc)
	[Market_MarketCategoryKey]		INT				NOT NULL,
    [MarketCategory]				VARCHAR (100)	NOT NULL,			-- Derived
-- Event (e.g. Australia vs England or Nadal vs Murray or Rip Curl Pro Bells Beach or Men's Long Jump or Emirates Melbourne Cup)
	[Market_EventKey]				INT				NOT NULL,
    [Event]							VARCHAR (100)	NOT NULL,			-- Derived
    [ScheduledDateTime]				DATETIME2 (3)	NULL,	 			--Intrabet.tblEvents
    [ScheduledVenueDateTime]		DATETIME2 (3)	NULL,	 			--Derived
	[Resulted]						CHAR (3)		NOT NULL,			--Intrabet.tblEvents
    [LastResultedDateTime]			DATETIME2 (3)	NULL,	 			--Intrabet.tblEvents
    [LastResultedVenueDateTime]		DATETIME2 (3)	NULL,	 			--Derived
	[Abandoned]						CHAR (3)		NOT NULL,			--Intrabet.tblEvents
	[LastAbandonedDateTime]			DATETIME2 (3)	NULL,	 			--Intrabet.tblEvents
    [LastAbandonedVenueDateTime]	DATETIME2 (3)	NULL,	 			--Derived
    [Distance]						VARCHAR (10)	NOT NULL,			-- Derived
    [PrizePool]						INT				NULL,				-- Derived
	[Grade]							VARCHAR (10)	NOT NULL,			-- Derived: Listed, Group 1, Group 2, Group 3, N/A
	[RaceClass]						VARCHAR (10)	NOT NULL,			-- Derived: maiden and Class 1 thru Class 6 or N/A
	[Age]							VARCHAR (10)	NOT NULL,			-- Derived: 2yo thru 5yo+
	[Gender]						VARCHAR (50)	NOT NULL,			-- Derived: fillies, mares, colts and gueldings
	[WeightType]					VARCHAR (100)	NOT NULL,			-- Derived: handicap, open, weight for age, set weights and penalties, ratings
    [TrackCondition]				VARCHAR (20)	NOT NULL,			-- Derived
    [EventWeather]					VARCHAR (20)	NOT NULL,			-- Derived
	[HybridPricingTemplate]			VARCHAR (100)	NOT NULL,			-- Derived
	[LiveEvent]						CHAR (3)		NOT NULL,			-- Derived
	[LiveVideoStreamed]				CHAR (3)		NOT NULL,			-- Derived
	[LiveScoreBoard]				CHAR (3)		NOT NULL,			-- Derived
	[NSWTAB]						CHAR (3)		NOT NULL,			-- Derived
	[VICTAB]						CHAR (3)		NOT NULL,			-- Derived
	[UNITAB]						CHAR (3)		NOT NULL,			-- Derived
-- Round (e.g. 3rd Test or Semi-final or Round 3 or Final or Race 7)
	[Market_RoundKey]				INT				NOT NULL,
	[Round]							VARCHAR (100)	NOT NULL,			-- Derived
    [RoundSequence]					SMALLINT		NULL,				-- Derived
-- Competition (e.g. Ashes 2017/8 or Australian Open 2017 or WSL Mens Championship Tour 2017 or Olympics 2016 or Melbourne Cup Day 2017)
	[Market_CompetitionKey]			INT				NOT NULL,
    [Competition]					VARCHAR (100)	NOT NULL,			-- Derived
    [CompetitionSeason]				VARCHAR (10)	NOT NULL,			-- Derived
    [CompetitionStartDate]			DATE			NULL,	 			-- Derived
    [CompetitionEndDate]			DATE			NULL,	 			-- Derived
-- Competition Class (e.g. Ashes or Australian Open or WSL Mens Championship Tour or Olympics or Melbourne Cup Day)
	[Market_CompetitionClassKey]	INT				NOT NULL,
    [CompetitionClass]				VARCHAR (100)	NOT NULL,			-- Derived
-- Competition Type (e.g. International or Grand Slam or WSL Championship Tour or World Games or Spring Carnival)
	[Market_CompetitionTypeKey]		INT				NOT NULL,
    [CompetitionType]				VARCHAR (100)	NOT NULL,			-- Derived
-- Venue (e.g. SCG or Melbourne Park or Bells Beach or Rio de Janeiro or Flemington)
	[Market_VenueKey]				INT				NOT NULL,
    [Venue]							VARCHAR (50)	NOT NULL,			-- Derived
	[VenueLocation] 				VARCHAR (50)	NOT NULL,			-- Derived
    [VenueType]						VARCHAR (60)	NOT NULL,			-- Derived
-- Venue State (e.g. NSW or VIC or N/A)
	[Market_VenueStateKey]			INT				NOT NULL,
    [VenueStateCode]				VARCHAR (3)		NOT NULL,			-- Derived
    [VenueState]					VARCHAR (30)	NOT NULL,			-- Derived
-- Venue Country (e.g. Australia or Brazil)
	[Market_VenueCountryKey]		INT				NOT NULL,
    [VenueCountryCode]				VARCHAR (3)		NOT NULL,			-- Derived
    [VenueCountry]					VARCHAR (40)	NOT NULL,			-- Derived
-- SubClass (e.g. Test Cricket or Mens or Long Jump or Flat Race)
    [Market_SubClassKey]			SMALLINT		NOT NULL,
    [SubClassCode]					VARCHAR (10)	NOT NULL,			-- Derived
    [SubClass]						VARCHAR (32)	NOT NULL,			-- Derived
-- Class (e.g. Cricket or Tennis or Surfing or Track & Field or Horse)
    [Market_ClassKey]				SMALLINT		NOT NULL,
    [ClassCode]						VARCHAR (4)		NOT NULL,			-- Derived
    [Class]							VARCHAR (32)	NOT NULL,			-- Derived
-- SuperClass (e.g. Sports or Racing)
    [Market_SuperClassKey]			SMALLINT		NOT NULL,
    [SuperClassCode]				VARCHAR (4)		NOT NULL,			-- Derived
    [SuperClass]					VARCHAR (32)	NOT NULL,			-- Derived
-- Source (Intrabet.tblEvents/tblLUVenues/tblLUStates/tblLUCountry)
	[EventId0]						INT				NOT NULL,		--Intrabet.tblEvents
    [Event0]						VARCHAR (100)	NULL,			--Intrabet.tblEvents
	[EventTypeId0]					INT				NULL,			--Intrabet.tblEvents
    [EventType0]					VARCHAR (50)	NULL,			--Intrabet.tblLUEventTypes
	[EventSubTypeId0]				INT				NULL,			--Intrabet.tblEvents
    [EventSubType0]					VARCHAR (50)	NULL,			--Intrabet.tblLUEventSubTypes
	[SportSubTypeId0]				INT				NULL,			--Intrabet.tblEvents
	[SportSubType0]					VARCHAR (50)	NULL,			--Intrabet.tblLUSportSubTypes
	[IsLiveEvent0]					BIT				NULL,			--Intrabet.tblEvents
	[LiveStreamPerformId0]			VARCHAR (50)	NULL,			--Intrabet.tblEvents
	[VenueId0]						INT				NULL,			--Intrabet.tblEvents
    [Venue0]						VARCHAR (50)	NULL,			--Intrabet.tblLUVenues
	[VenueType0]					VARCHAR (60)	NULL,			--Intrabet.tblLUVenues
	[VenueEventTypeId0]				INT				NULL,			--Intrabet.tblLUVenues
	[VenueEventType0]				VARCHAR (50)	NULL,			--Intrabet.tblLUEventTypes
    [VenueStateCode0]				CHAR (3)		NULL,			--Intrabet.tblLUVenues
    [VenueState0]					VARCHAR (50)	NULL,			--Intrabet.tblLUStates
    [VenueCountryCode0]				CHAR (3)		NULL,			--Intrabet.tblLUVenues
    [VenueCountry0]					VARCHAR (50)	NULL,			--Intrabet.tblLUCountry
    [EventId1]						INT				NULL,			--Intrabet.tblEvents
    [Event1]						VARCHAR (100)	NULL,			--Intrabet.tblEvents
	[EventTypeId1]					INT				NULL,			--Intrabet.tblEvents
    [EventType1]					VARCHAR (50)	NULL,			--Intrabet.tblLUEventTypes
	[EventSubTypeId1]				INT				NULL,			--Intrabet.tblEvents
    [EventSubType1]					VARCHAR (50)	NULL,			--Intrabet.tblLUEventSubTypes
	[SportSubTypeId1]				INT				NULL,			--Intrabet.tblEvents
	[SportSubType1]					VARCHAR (50)	NULL,			--Intrabet.tblLUSportSubTypes
	[IsLiveEvent1]					BIT				NULL,			--Intrabet.tblEvents
	[LiveStreamPerformId1]			VARCHAR (50)	NULL,			--Intrabet.tblEvents
	[VenueId1]						INT				NULL,			--Intrabet.tblEvents
    [Venue1]						VARCHAR (50)	NULL,			--Intrabet.tblLUVenues	
	[VenueType1]					VARCHAR (60)	NULL,			--Intrabet.tblLUVenues
	[VenueEventTypeId1]				INT				NULL,			--Intrabet.tblLUVenues
	[VenueEventType1]				VARCHAR (50)	NULL,			--Intrabet.tblLUEventTypes
    [VenueStateCode1]				CHAR (3)		NULL,			--Intrabet.tblLUVenues
    [VenueState1]					VARCHAR (50)	NULL,			--Intrabet.tblLUStates
    [VenueCountryCode1]				CHAR (3)		NULL,			--Intrabet.tblLUVenues
    [VenueCountry1]					VARCHAR (50)	NULL,			--Intrabet.tblLUCountry
    [EventId2]						INT				NULL,			--Intrabet.tblEvents
    [Event2]						VARCHAR (100)	NULL,			--Intrabet.tblEvents
	[EventTypeId2]					INT				NULL,			--Intrabet.tblEvents
    [EventType2]					VARCHAR (50)	NULL,			--Intrabet.tblLUEventTypes
	[EventSubTypeId2]				INT				NULL,			--Intrabet.tblEvents
    [EventSubType2]					VARCHAR (50)	NULL,			--Intrabet.tblLUEventSubTypes
	[SportSubTypeId2]				INT				NULL,			--Intrabet.tblEvents
	[SportSubType2]					VARCHAR (50)	NULL,			--Intrabet.tblLUSportSubTypes
	[IsLiveEvent2]					BIT				NULL,			--Intrabet.tblEvents
	[LiveStreamPerformId2]			VARCHAR (50)	NULL,			--Intrabet.tblEvents
	[VenueId2]						INT				NULL,			--Intrabet.tblEvents
    [Venue2]						VARCHAR (50)	NULL,			--Intrabet.tblLUVenues		
	[VenueType2]					VARCHAR (60)	NULL,			--Intrabet.tblLUVenues
	[VenueEventTypeId2]				INT				NULL,			--Intrabet.tblLUVenues
	[VenueEventType2]				VARCHAR (50)	NULL,			--Intrabet.tblLUEventTypes
    [VenueStateCode2]				CHAR (3)		NULL,			--Intrabet.tblLUVenues
    [VenueState2]					VARCHAR (50)	NULL,			--Intrabet.tblLUStates
    [VenueCountryCode2]				CHAR (3)		NULL,			--Intrabet.tblLUVenues
    [VenueCountry2]					VARCHAR (50)	NULL,			--Intrabet.tblLUCountry
    [EventId3]						INT				NULL,			--Intrabet.tblEvents
    [Event3]						VARCHAR (100)	NULL,			--Intrabet.tblEvents
	[EventTypeId3]					INT				NULL,			--Intrabet.tblEvents
    [EventType3]					VARCHAR (50)	NULL,			--Intrabet.tblLUEventTypes
	[EventSubTypeId3]				INT				NULL,			--Intrabet.tblEvents
    [EventSubType3]					VARCHAR (50)	NULL,			--Intrabet.tblLUEventSubTypes
	[SportSubTypeId3]				INT				NULL,			--Intrabet.tblEvents
	[SportSubType3]					VARCHAR (50)	NULL,			--Intrabet.tblLUSportSubTypes
	[IsLiveEvent3]					BIT				NULL,			--Intrabet.tblEvents
	[LiveStreamPerformId3]			VARCHAR (50)	NULL,			--Intrabet.tblEvents
	[VenueId3]						INT				NULL,			--Intrabet.tblEvents
    [Venue3]						VARCHAR (50)	NULL,			--Intrabet.tblLUVenues		
	[VenueType3]					VARCHAR (60)	NULL,			--Intrabet.tblLUVenues
	[VenueEventTypeId3]				INT				NULL,			--Intrabet.tblLUVenues
	[VenueEventType3]				VARCHAR (50)	NULL,			--Intrabet.tblLUEventTypes
    [VenueStateCode3]				CHAR (3)		NULL,			--Intrabet.tblLUVenues
    [VenueState3]					VARCHAR (50)	NULL,			--Intrabet.tblLUStates
    [VenueCountryCode3]				CHAR (3)		NULL,			--Intrabet.tblLUVenues
    [VenueCountry3]					VARCHAR (50)	NULL,			--Intrabet.tblLUCountry
	[TopLevel]						TINYINT			NOT NULL,		--Intrabet.tblEvents
	[RaceNumber]					INT				NULL,			--Intrabet.tblEvents
	[Track]							VARCHAR (20)	NULL,			--Intrabet.tblTrackConditions
    [Weather]						VARCHAR (20)	NULL,			--Intrabet.tblTrackConditions
	[TotalPrizeMoney]				MONEY			NULL,			--PricingData.Race
	[SourceDistance]				INT				NULL,			--PricingData.Race
	[RaceGroup]						VARCHAR (20)	NULL,			--PricingData.Race
	[SourceRaceClass]				VARCHAR (150)	NULL,			--PricingData.Race
	[SourceHybridPricingTemplate]	VARCHAR (50)	NULL,			--PricingData.Race
	[SourceWeightType]				VARCHAR (50)	NULL,			--PricingData.Race
	[TABListing]					INT				NULL,			--PricingData.Race
-- Metadata
    [FirstDate]						DATETIME2 (3)	NOT NULL,
    [FromDate]						DATETIME2 (3)	NOT NULL,
    [ToDate]						DATETIME2 (3)	NOT NULL,
    [CreatedDate]					DATETIME2 (7)	NOT NULL,
    [CreatedBatchKey]				INT				NOT NULL,
    [CreatedBy]						VARCHAR (100)	NOT NULL,
    [ModifiedDate]					DATETIME2 (7)	NOT NULL,
    [ModifiedBatchKey]				INT				NOT NULL,
    [ModifiedBy]					VARCHAR (100)	NOT NULL,
    [CleansedDate]					DATETIME2 (7)	NULL,
    [CleansedBatchKey]				INT				NULL,
    [CleansedBy]					VARCHAR (100)	NULL,
 CONSTRAINT [CI_Market_PK] PRIMARY KEY CLUSTERED 
(
	[MarketKey] ASC
) WITH (DATA_COMPRESSION = ROW)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Market_NK]
	ON [CC].[Market] ([MarketID] ASC, [Source] ASC)
	INCLUDE ([MarketKey]) WITH (DATA_COMPRESSION = ROW)
GO

CREATE NONCLUSTERED INDEX [NI_Market_ModifiedBatchKey] 
	ON [CC].[Market] ([ModifiedBatchKey] ASC)
	INCLUDE ([MarketID]) WITH (DATA_COMPRESSION = ROW)
GO

