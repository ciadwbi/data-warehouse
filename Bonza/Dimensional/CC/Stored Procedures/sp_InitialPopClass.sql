CREATE PROCEDURE [CC].sp_InitialPopClass
AS
	SET XACT_ABORT ON;

	SET IDENTITY_INSERT CC.Class ON 

	;WITH x (	ClassKey,	Source,	Keyword,	Class_SubClassKey,	SubClassId,	SourceSubClass,	SubClassCode,	SubClass,	Class_ClassKey,	ClassId,	SourceClass,	ClassCode,	Class,		Class_SuperClassKey,	SuperClass,	FromDate,		ToDate			) AS
	(	SELECT	-1,			'IBT',	NULL,		-1,					NULL,		NULL,			'UNK',			'Unknown',	-1,				-1,			'Unknown',		'UNKN',		'Unknown',	-1,						'Unknown',	'1900-01-01',	'9999-12-31'	UNION ALL
		SELECT	0,			'IBT',	NULL,		0,					NULL,		NULL,			'N/A',			'N/A',		0,				0,			'Legacy',			'NOTA',		'N/A',		0,						'N/A',		'1900-01-01',	'9999-12-31'				
		EXCEPT
		SELECT	ClassKey,	Source,	Keyword,	Class_SubClassKey,	SubClassId,	SourceSubClass,	SubClassCode,	SubClass,	Class_ClassKey,	ClassId,	SourceClass,	ClassCode,	Class,		Class_SuperClassKey,	SuperClass, FromDate,		ToDate			FROM CC.Class
	)
	MERGE CC.Class m
	USING (SELECT *, HASHBYTES('MD5','|'+CONVERT(varchar(20),ClassId)+'|'+ISNULL(CONVERT(varchar(20),SubClassId),'')+'|'+ISNULL(Keyword,'')+'|') HashKey FROM x) u ON u.ClassKey = m.ClassKey
	WHEN NOT MATCHED THEN 
		INSERT (ClassKey, HashKey, Source, Keyword, Class_SubClassKey, SubClassId, SourceSubClass, SubClassCode, SubClass, Class_ClassKey, ClassId, SourceClass, ClassCode, Class, Class_SuperClassKey, SuperClass, FirstDate, FromDate, ToDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy)
		VALUES (u.ClassKey, u.HashKey, u.Source, u.Keyword, u.Class_SubClassKey, u.SubClassId, u.SourceSubClass, u.SubClassCode, u.SubClass, u.Class_ClassKey, u.ClassId, u.SourceClass, u.ClassCode, u.Class, u.Class_SuperClassKey, u.SuperClass, u.FromDate, u.FromDate, u.ToDate, GETDATE(), 0, 'sp_InitialPopClass', GETDATE(), 0, 'sp_InitialPopClass')
	WHEN MATCHED THEN 
		UPDATE SET		HashKey 			= u.HashKey 			,	
						Source 				= u.Source 				,
						Keyword				= u.Keyword				,	
						Class_SubClassKey	= u.Class_SubClassKey	,
						SubClassId 			= u.SubClassId 			,	
						SourceSubClass		= u.SourceSubClass		,
						SubClassCode 		= u.SubClassCode 		,	
						SubClass	 		= u.SubClass	 		,	
						Class_ClassKey		= u.Class_ClassKey		,
						ClassId 			= u.ClassId 			,	
						SourceClass			= u.SourceClass			,
						ClassCode 			= u.ClassCode 			,	
						Class	 			= u.Class				,	
						Class_SuperClassKey = u.Class_SuperClassKey ,
						SuperClass	 		= u.SuperClass	 		,
						FirstDate 			= u.FromDate 			,	
						FromDate 			= u.FromDate 			,	
						ToDate 				= u.ToDate 				,	
						ModifiedDate 		= GETDATE()				,
						ModifiedBatchKey	= 0						,
						ModifiedBy			= 'sp_InitialPopClass';

	SET IDENTITY_INSERT CC.Class OFF;
