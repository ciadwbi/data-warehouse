﻿CREATE TABLE [Fact].[Cashflow] (
	[Id] [binary](16) NOT NULL,
	[IdSource] [char](3) NOT NULL,
	[TransactionDetailKey] [smallint] NOT NULL,
	[BalanceTypeKey] [smallint] NOT NULL,
	[LegTypeKey] [smallint] NOT NULL,
	[DayZoneKey] [int] NOT NULL,
	[MasterTimestamp] [datetime2](3) NOT NULL,
	[MasterTimeKey] [smallint] NOT NULL,
	[AnalysisTimestamp] [datetime2](3) NOT NULL,
	[AnalysisTimeKey] [smallint] NOT NULL,
	[ContractKey] [int] NOT NULL,
	[AccountKey] [int] NOT NULL,
	[AccountStatusKey] [int] NOT NULL,
	[StructureKey] [int] NOT NULL,
	[WalletKey] [smallint] NOT NULL,
	[PriceTypeKey] [int] NOT NULL,
	[BetTypeKey] [smallint] NOT NULL,
	[ClassKey] [smallint] NOT NULL,
	[EventKey] [int] NOT NULL,
	[MarketKey] [int] NOT NULL,
	[ChannelKey] [smallint] NOT NULL,
	[ActionChannelKey] [smallint] NOT NULL,
	[UserKey] [smallint] NOT NULL,
	[InstrumentKey] [int] NOT NULL,
	[NoteKey] [int] NOT NULL,
	[InPlayKey] [int] NOT NULL,
	[PromotionKey] [int] NOT NULL,
	[Amount] [money] NOT NULL,
	[TransactionId] [bigint] NULL,
	[TransactionSource] [char](3) NOT NULL,
	[GroupID] [bigint] NOT NULL,
	[GroupSource] [char](3) NOT NULL,
	[BetID] [bigint] NULL,
	[BetSource] [char](3) NOT NULL,
	[LegID] [bigint] NULL,
	[LegSource] [char](3) NOT NULL,
	[FreeBetID] [int] NULL,
	[ExternalReference] [varchar](32) NULL,
	[ExternalSource] [char](3) NOT NULL,
	[MappingId] [int] NOT NULL,-- Lineage passed on fromt the Atomic processes
	[FunctionalArea] [char](1) NOT NULL, -- Functional segregation to allow partial reloads on specific functional area, e.g. Withdrawals (W), Deposits(D), Bets(B), etc
	[FromDate] [datetime2](3) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL
)
GO

CREATE CLUSTERED COLUMNSTORE INDEX [CC_Cashflow] ON [Fact].[CashFlow]  ON [DayKey_Yearly] ([DayZoneKey])
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Cashflow_PK]
ON [Fact].[Cashflow](
	[Id] ASC,
	[IdSource] ASC,
	[TransactionDetailKey] ASC,
	[BalanceTypeKey] ASC,
	[MasterTimestamp] ASC,
	[DayZoneKey] ASC,
	[TransactionId] ASC,-- Included as a temporary measure to cope with a small number of duplicate transactions which will ultimately be removed by bet bugfixes and reload
	[TransactionSource] ASC) 
GO
