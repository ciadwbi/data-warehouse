﻿CREATE TABLE [Fact].[Transaction] (
	[TransactionId] [bigint] NOT NULL,
	[TransactionIdSource] [char](3) NOT NULL,
	[TransactionDetailKey] [smallint] NOT NULL,
	[BalanceTypeKey] [smallint] NOT NULL,
	[ContractKey] [int] NOT NULL,
	[AccountKey] [int] NOT NULL,
	[AccountStatusKey] [int] NOT NULL,
	[WalletKey] [smallint] NOT NULL,
	[PromotionKey] [int] NOT NULL,
	[MarketKey] [int] NOT NULL,
	[EventKey] [int] NOT NULL,
	[DayKey] [int] NOT NULL,
	[MasterTransactionTimestamp] [datetime2](7) NOT NULL,
	[MasterTimeKey] [smallint] NOT NULL,
	[AnalysisTransactionTimestamp] [datetime2](7) NOT NULL,
	[AnalysisTimeKey] [smallint] NOT NULL,
	[CampaignKey] [smallint] NOT NULL,
	[ChannelKey] [smallint] NOT NULL,
	[ActionChannelKey] [smallint] NOT NULL,
	[LedgerID] [bigint] NOT NULL,
	[TransactedAmount] [money] NOT NULL,
	[PriceTypeKey] [int] NOT NULL,
	[BetTypeKey] [smallint] NOT NULL,
	[LegTypeKey] [smallint] NOT NULL,
	[ClassKey] [smallint] NOT NULL,
	[UserKey] [smallint] NOT NULL,
	[InstrumentKey] [int] NOT NULL,
	[NoteKey] [int] NOT NULL,
	[InPlayKey] [int] NOT NULL,
	[FromDate] [datetime2](3) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[StructureKey] int NOT NULL,
	[StructureKeyx]  AS ([accountkey]*	case [InPlayKey] 
											when (2) then (-1) when (4) then (-1) when (7) then (-1) when (9) then (-1) 
											when (12) then (-1) when (14) then (-1) when (17) then (-1) when (19) then (-1) 
											when (22) then (-1) when (24) then (-1) when (27) then (-1) when (29) then (-1) 
											else (1) 
										end)
) ON [par_sch_int_Dimensional] ([DayKey])
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Transaction_PK]
ON [Fact].[Transaction](
	[DayKey] ASC,
	[TransactionId] ASC,
	[TransactionIdSource] ASC,
	[TransactionDetailKey] ASC,
	[BalanceTypeKey] ASC,
	[MasterTransactionTimestamp] ASC,
	[LegTypeKey] ASC) 
GO

CREATE NONCLUSTERED INDEX [NI_Transaction_TransactionId]
    ON [Fact].[Transaction]([TransactionId] ASC, [TransactionIdSource] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Transaction_TransactionDetailKey]
    ON [Fact].[Transaction]([TransactionDetailKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Transaction_BalanceKey]
    ON [Fact].[Transaction]([BalanceTypeKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Transaction_AccountKey]
    ON [Fact].[Transaction]([AccountKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Transaction_MasterTransactionTimestamp]
    ON [Fact].[Transaction]([MasterTransactionTimestamp] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Transaction_ContractKey]
    ON [Fact].[Transaction]([ContractKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Transaction_CreatedBatchKey]
    ON [Fact].[Transaction]([CreatedBatchKey] ASC)
GO
