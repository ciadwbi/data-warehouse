﻿CREATE TABLE [Fact].[Contact](
	[AccountKey] [int] NULL,
	[AccountStatusKey] [int] NULL,
	[CommunicationId] [varchar](200) NULL,
	[CommunicationKey] [int] NULL,
	[StoryKey] [int] NULL,
	[ContactDate] [datetime] NULL,
	[ChannelKey] [int] NULL,
	[PromotionKey] [int] NULL,
	[BetTypeKey] [smallint] NULL,
	[ClassKey] [smallint] NULL,
	[CommunicationCompanyKey] [int] NULL,
	[EventKey] [int] NULL,
	[InteractionTypeKey] [int] NULL,
	[InteractionDayKey] [int] NULL,
	[InteractionTimeKeyMaster] [int] NULL,
	[InteractionTimeKeyAnalysis] [int] NULL,
	[Details] [varchar](500) NULL,
	[CreatedDate] [datetime2](3) NULL,
	[CreatedBatchKey] [int] NULL,
	[ModifiedDate] [datetime2](3) NULL,
	[ModifiedBatchKey] [int] NULL
)

GO