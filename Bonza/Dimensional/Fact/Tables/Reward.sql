CREATE TABLE [Fact].[Reward](
	[ContractKey] [int] NOT NULL,
	[MasterTransactionTimestamp] [datetime2](3) NOT NULL,
	[AnalysisTransactionTimestamp] [datetime2](3) NOT NULL,
	[DayKey] [int] NOT NULL,
	[MasterTimeKey] [int] NOT NULL,
	[AnalysisTimeKey] [int] NOT NULL,
	[AccountKey] [int] NOT NULL,
	[AccountStatusKey] [int] NOT NULL,
	[StructureKey]  [int] NOT NULL,
	[BetTypeKey] [smallint] NOT NULL,
	[LegTypeKey] [smallint] NOT NULL,
	[PriceTypeKey] [int] NOT NULL,
	[RewardDetailKey] [int] NOT NULL,
	[ChannelKey] [smallint] NOT NULL,
	[InPlayKey] [int] NOT NULL,
	[PromotionKey] [int] NOT NULL,
	[MarketKey] [int] NOT NULL,
	[EventKey] [int] NOT NULL,
	[ClassKey] [smallint] NOT NULL,
	[Points] [money] NOT NULL,
	[VelocityPoints] [int] NOT NULL,
	[BonusCredit] [money] NOT NULL,
	[FromDate] [datetime2](3) NOT NULL,
	[MappingId] [smallint] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL
)
GO

CREATE CLUSTERED COLUMNSTORE INDEX [CC_Reward]
ON [Fact].[Reward]
ON [par_sch_int_Dimensional] ([DayKey])
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Reward_PK]
ON [Fact].[Reward]([ContractKey] ASC, [DayKey] ASC, [RewardDetailKey] ASC, [MasterTransactionTimestamp] ASC, FromDate ASC) 
GO

CREATE NONCLUSTERED INDEX [NI_KPI_AccountKey] 
ON [Fact].[Reward] ([AccountKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_KPI_MarketKey]
ON [Fact].[Reward] ([MarketKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_KPI_EventKey]
ON [Fact].[Reward] ([EventKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_KPI_DayKey] 
ON [Fact].[Reward] ([DayKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_KPI_StructureKey] 
ON [Fact].[Reward] ([StructureKey] ASC)
GO



