﻿CREATE TABLE [Fact].[Risk](
	[SelectionKey] [int] NOT NULL,
	[LegTypeKey] [int] NOT NULL,
	[WalletKey] [smallint] NOT NULL,
	[DayKey] [int] NOT NULL,
	[TimeKey] [int] NOT NULL,
	[CompanyKey] [int] NOT NULL,
	[PriceKey] [int] NOT NULL,
	[LocalDayKey] [int] NOT NULL,
	[StatusKey] [int] NOT NULL,
	[RequestedStake] [smallmoney] NOT NULL,
	[Stake] [smallmoney] NOT NULL,
	[NumberOfBets] [int] NOT NULL,
	[Return] [money] NOT NULL,
	[MinimumReturn] [money] NOT NULL,
	[MaximumReturn] [money] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NOT NULL,
 CONSTRAINT [Risk_PK] PRIMARY KEY NONCLUSTERED 
(
	[SelectionKey] ASC,
	[WalletKey] ASC,
	[DayKey] ASC,
	[TimeKey] ASC,
	[LegTypeKey] ASC,
	[CompanyKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
