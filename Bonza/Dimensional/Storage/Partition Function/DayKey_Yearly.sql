﻿CREATE PARTITION FUNCTION [DayKey_Yearly](INT)
    AS RANGE RIGHT
    FOR VALUES (27, 757, 1487, 2219, 2949); -- yearly right bound values based on 2 day keys per day (Master and Analysis) starting on 19-DEC-2013