﻿CREATE ROLE [Reporting]
go
Grant Select on schema :: [Control] to [Reporting]
Go
Grant Select on schema :: [Fact] to [Reporting]
Go
Grant Select on schema :: [Dimension] to [Reporting]
Go
