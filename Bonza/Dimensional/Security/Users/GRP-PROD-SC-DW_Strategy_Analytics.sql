﻿CREATE LOGIN [PROD\GRP-PROD-SC-DW_Strategy_Analytics] FROM WINDOWS WITH DEFAULT_DATABASE=[$(DatabaseName)], DEFAULT_LANGUAGE=[us_english]
Go
Create User [PROD\GRP-PROD-SC-DW_Strategy_Analytics] For Login [PROD\GRP-PROD-SC-DW_Strategy_Analytics]
GO
Grant CONNECT TO [PROD\GRP-PROD-SC-DW_Strategy_Analytics]
Go
GRANT SHOWPLAN TO [PROD\GRP-PROD-SC-DW_Strategy_Analytics]
Go