﻿CREATE VIEW [SnowFlake].[Account_LedgerType1] WITH SCHEMABINDING
AS
SELECT	--DISTINCT
		LedgerTypeID, 
		CASE LedgerType 
			WHEN 'Unk' THEN 'Unknown' 
			WHEN 'N/A' THEN 'No Ledger' 
			ELSE ISNULL(LedgerType,'Unknown') 
		END LedgerType, 
		CASE LedgerType
			WHEN 'N/A' THEN 'Financial' 
			WHEN 'No Ledger' THEN 'Financial' 
			WHEN 'Unknown' THEN 'Financial' 
			ELSE ISNULL(LedgerGrouping,'Financial') 
		END LedgerGrouping, 
		COUNT_BIG(*) z -- Compulsory analytical column to be allowed to create clustered index
FROM	Dimension.Account
GROUP BY
		LedgerTypeID, 
		CASE LedgerType 
			WHEN 'Unk' THEN 'Unknown' 
			WHEN 'N/A' THEN 'No Ledger' 
			ELSE ISNULL(LedgerType,'Unknown') 
		END, 
		CASE LedgerType
			WHEN 'N/A' THEN 'Financial' 
			WHEN 'No Ledger' THEN 'Financial' 
			WHEN 'Unknown' THEN 'Financial' 
			ELSE ISNULL(LedgerGrouping,'Financial') 
		END

GO

CREATE UNIQUE CLUSTERED INDEX [CI_LedgerTypeId(A)] ON [SnowFlake].[Account_LedgerType1]
(
	[LedgerTypeID] ASC,
	[LedgerGrouping] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

