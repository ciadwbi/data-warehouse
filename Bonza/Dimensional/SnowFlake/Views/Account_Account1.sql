﻿CREATE VIEW [SnowFlake].[Account_Account1] WITH SCHEMABINDING
AS
SELECT	AccountId,
		PIN,
		ExactTargetID,
		CustomerId,
		AccountTypeCode AccountTypeId,
		z.MasterDayKey AccountOpenedDayKey,
		BrandId,
		BusinessUnitId,
		COALESCE(NULLIF(BDM,'N/A'),NULLIF(VIP,'N/A'),'N/A') ManagedById,
		BTag2+'!'+TrafficSource+'!'+RefURL+'!'+CampaignID+'!'+Keywords TrafficSourceId,
		CASE WHEN StatementFrequency = '-1' THEN 'Unknown' WHEN StatementMethod = 'Never' THEN 'Never' ELSE StatementFrequency END
		+CASE WHEN StatementMethod = 'Unk' THEN 'Unknown' WHEN StatementFrequency = 'Never' THEN 'Never' ELSE StatementMethod END StatementId,
		CardAccountId,
		CardFirstIssueDate,
		CardIssueDate,
		CardStatusId,
		CardStatusCode,
		CardStatus,
		EachwayAccountId,
		VelocityNumber,
		VelocityLinkedDate,
		PasswordLastChanged,
		COUNT_BIG(*) x
FROM	Dimension.Account a
		INNER JOIN Dimension.Company c ON c.CompanyKey = a.CompanyKey
		INNER JOIN Dimension.DayZone z ON z.DayKey = a.AccountOpenedDayKey
		Where AccountKey <> -1
		Group By
		AccountId,
		PIN,
		ExactTargetID,
		CustomerId,
		AccountTypeCode,
		z.MasterDayKey,
		BrandId,
		BusinessUnitId,
		COALESCE(NULLIF(BDM,'N/A'),NULLIF(VIP,'N/A'),'N/A'),
		BTag2+'!'+TrafficSource+'!'+RefURL+'!'+CampaignID+'!'+Keywords,
		CASE WHEN StatementFrequency = '-1' THEN 'Unknown' WHEN StatementMethod = 'Never' THEN 'Never' ELSE StatementFrequency END
		+CASE WHEN StatementMethod = 'Unk' THEN 'Unknown' WHEN StatementFrequency = 'Never' THEN 'Never' ELSE StatementMethod END,
		CardAccountId,
		CardFirstIssueDate,
		CardIssueDate,
		CardStatusId,
		CardStatusCode,
		CardStatus,
		EachwayAccountId,
		VelocityNumber,
		VelocityLinkedDate,
		PasswordLastChanged

GO

CREATE UNIQUE CLUSTERED INDEX [CI_AccountId(A)] ON [SnowFlake].[Account_Account1]
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

