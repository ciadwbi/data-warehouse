﻿CREATE TABLE [SnowFlake].[Account_LedgerType](
	[Account_LedgerTypeKey] [int] NOT NULL,
	[LedgerTypeID] [int] NOT NULL,
	[LedgerType] [varchar](50) NOT NULL,
	[LedgerGrouping] [varchar](30) NOT NULL,
 CONSTRAINT [CI_Account_LedgerType_PK] PRIMARY KEY CLUSTERED 
(
	[Account_LedgerTypeKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
