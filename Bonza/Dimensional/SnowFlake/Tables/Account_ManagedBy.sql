﻿CREATE TABLE [SnowFlake].[Account_ManagedBy](
	[Account_ManagedByKey] [int] NOT NULL,
	[ManagedById] [char](32) NOT NULL,
	[ManagedBy] [varchar](50) NOT NULL,
	[BDM] [varchar](50) NOT NULL,
	[VIPCode] [int] NOT NULL,
	[VIP] [varchar](50) NOT NULL,
 CONSTRAINT [CI_Account_ManagedBy_PK] PRIMARY KEY CLUSTERED 
(
	[Account_ManagedByKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
