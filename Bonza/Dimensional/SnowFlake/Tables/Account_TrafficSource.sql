﻿CREATE TABLE [SnowFlake].[Account_TrafficSource](
	[Account_TrafficSourceKey] [int] NOT NULL,
	[TrafficSourceId] [char](32) NOT NULL,
	[BTag2] [varchar](100) NOT NULL,
	[AffiliateID] [varchar](30) NOT NULL,
	[AffiliateName] [varchar](100) NOT NULL,
	[SiteID] [varchar](10) NOT NULL,
	[TrafficSource] [varchar](50) NOT NULL,
	[RefURL] [varchar](500) NOT NULL,
	[CampaignID] [varchar](50) NOT NULL,
	[Keywords] [varchar](500) NOT NULL,
 CONSTRAINT [CI_Account_TrafficSource_PK] PRIMARY KEY CLUSTERED 
(
	[Account_TrafficSourceKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
