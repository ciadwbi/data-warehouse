﻿CREATE TABLE [SnowFlake].[Account_Country](
	[Account_CountryKey] [int] NOT NULL,
	[CountryId] [char](32) NOT NULL,
	[CountryCode] [varchar](3) NOT NULL,
	[Country] [varchar](40) NOT NULL,
 CONSTRAINT [CI_Account_Country_PK] PRIMARY KEY CLUSTERED 
(
	[Account_CountryKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
