﻿CREATE TABLE [SnowFlake].[Event_Round](
	[RoundID] [varchar](600) NULL,
	[Round] [varchar](255) NULL,
	[RoundSequence] [smallint] NOT NULL,
	[CompetitionID] [varchar](345) NULL
) ON [PRIMARY]

GO

CREATE CLUSTERED COLUMNSTORE INDEX [CC_Round] ON [SnowFlake].[Event_Round] WITH (DROP_EXISTING = OFF) ON [PRIMARY]
GO
