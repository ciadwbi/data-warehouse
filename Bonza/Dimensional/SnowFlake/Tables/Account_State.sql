﻿CREATE TABLE [SnowFlake].[Account_State](
	[Account_StateKey] [int] NOT NULL,
	[StateId] [char](32) NOT NULL,
	[StateCode] [varchar](3) NOT NULL,
	[State] [varchar](30) NOT NULL,
 CONSTRAINT [CI_Account_State_PK] PRIMARY KEY CLUSTERED 
(
	[Account_StateKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
