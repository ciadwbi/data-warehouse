﻿CREATE TABLE [SnowFlake].[Event_Venue](
	[VenueId] [varchar](136) NULL,
	[Venue] [varchar](100) NULL,
	[VenueStateId] [varchar](6) NULL,
	[VenueTypeId] [varchar](100) NULL
) ON [PRIMARY]

GO

CREATE CLUSTERED COLUMNSTORE INDEX [CC_Venue] ON [SnowFlake].[Event_Venue] WITH (DROP_EXISTING = OFF) ON [PRIMARY]