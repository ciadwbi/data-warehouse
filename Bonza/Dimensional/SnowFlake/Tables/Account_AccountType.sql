﻿CREATE TABLE [SnowFlake].[Account_AccountType](
	[Account_AccountTypeKey] [int] NOT NULL,
	[AccountTypeId] [char](1) NOT NULL,
	[AccountTypeCode] [char](1) NOT NULL,
	[AccountType] [varchar](10) NOT NULL,
 CONSTRAINT [CI_Account_AccountType_PK] PRIMARY KEY CLUSTERED 
(
	[Account_AccountTypeKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
