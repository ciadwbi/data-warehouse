﻿CREATE TABLE [SnowFlake].[Account_Statement](
	[Account_StatementKey] [int] NOT NULL,
	[StatementId] [char](32) NOT NULL,
	[StatementMethod] [varchar](20) NOT NULL,
	[StatementFrequency] [varchar](20) NOT NULL,
 CONSTRAINT [CI_Account_Statement_PK] PRIMARY KEY CLUSTERED 
(
	[Account_StatementKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
