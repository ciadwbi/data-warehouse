﻿CREATE TABLE [SnowFlake].[Account_Customer](
	[Account_CustomerKey] [int] NOT NULL,
	[CustomerID] [int] NOT NULL,
	[CustomerSource] [char](3) NOT NULL,
	[Title] [varchar](10) NULL,
	[Surname] [varchar](60) NULL,
	[FirstName] [varchar](60) NULL,
	[MiddleName] [varchar](60) NULL,
	[Gender] [char](1) NOT NULL,
	[DOB] [date] NULL,
	[Age]  AS (datediff(year,[DOB],getdate())),
	[Phone] [varchar](20) NULL,
	[Mobile] [varchar](20) NULL,
	[Fax] [varchar](20) NULL,
	[PhoneLastChanged] [datetime2](3) NULL,
	[Email] [varchar](100) NULL,
	[EmailLastChanged] [datetime2](3) NULL,
	[Address1] [varchar](60) NULL,
	[Address2] [varchar](60) NULL,
	[Suburb] [varchar](50) NULL,
	[PostCode] [varchar](7) NULL,
	[GeographicalLocation] [geography] NULL,
	[AddressLastChanged] [datetime2](3) NULL,
	[Account_StateKey] [int] NOT NULL,
	[StateId] [char](32) NOT NULL,
	[StateCode] [varchar](3) NOT NULL,
	[State] [varchar](30) NOT NULL,
	[Account_CountryKey] [int] NOT NULL,
	[CountryId] [char](32) NOT NULL,
	[CountryCode] [varchar](3) NOT NULL,
	[Country] [varchar](40) NOT NULL,
	[AustralianClient] [varchar](3) NOT NULL,
 CONSTRAINT [CI_Account_Customer_PK] PRIMARY KEY CLUSTERED 
(
	[Account_CustomerKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
