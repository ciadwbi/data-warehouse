﻿CREATE PROC dbo.[SP_FullProcessCube]
--WITH EXECUTE AS 'PROD\Joseph.George'
AS
BEGIN
/*
	DECLARE @ProcessFull NVARCHAR(MAX)

	SET @ProcessFull=N'<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessClear</Type>
						  <Object>
							<DatabaseID>DataWareHouseWH</DatabaseID>
							<DimensionID>Query_b987a4c1-5192-4214-b6b7-225fed63d9c4</DimensionID>
						  </Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessClear</Type>
						  <Object>
							<DatabaseID>DataWareHouseWH</DatabaseID>
							<DimensionID>Wallet_a0ec0154-248e-41c0-b4ec-cf8bb4a300f7</DimensionID>
						  </Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessClear</Type>
						  <Object>
							<DatabaseID>DataWareHouseWH</DatabaseID>
							<DimensionID>Transaction_ef4ab73f-686c-4f5b-b13f-1c8d87a763f9</DimensionID>
						  </Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessClear</Type>
						  <Object>
							<DatabaseID>DataWareHouseWH</DatabaseID>
							<DimensionID>Bet_a1110428-ff63-4703-9ab5-c542dfaa5c6e</DimensionID>
						  </Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessClear</Type>
						  <Object>
							<DatabaseID>DataWareHouseWH</DatabaseID>
							<DimensionID>BetType_032ecf96-0aa2-401b-ac7e-d4a7748e24aa</DimensionID>
						  </Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessClear</Type>
						  <Object>
							<DatabaseID>DataWareHouseWH</DatabaseID>
							<DimensionID>KPI_79e0de34-c7ce-4744-8f19-4249e0c4c7b5</DimensionID>
						  </Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessClear</Type>
						  <Object>
							<DatabaseID>DataWareHouseWH</DatabaseID>
							<DimensionID>Position_0f67ea54-27c6-4e4f-8a38-698905f2c48b</DimensionID>
						  </Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessClear</Type>
						  <Object>
							<DatabaseID>DataWareHouseWH</DatabaseID>
							<DimensionID>FirstBetType_eb6b5379-14f9-4d8b-9e5c-ef447e4a3d15</DimensionID>
						  </Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessClear</Type>
						  <Object>
							<DatabaseID>DataWareHouseWH</DatabaseID>
							<DimensionID>LastBetType_169fa75e-f04f-445e-85bf-6fb0c28ebae1</DimensionID>
						  </Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
						  <Type>ProcessClear</Type>
						  <Object>
							<DatabaseID>DataWareHouseWH</DatabaseID>
							<DimensionID>Market_432a74ac-7865-49ef-a685-5ec72d399e6f</DimensionID>
						  </Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
							<Type>ProcessFull</Type>
							<Object>
								<DatabaseID>DataWareHouseWH</DatabaseID>
								<DimensionID>Query_b987a4c1-5192-4214-b6b7-225fed63d9c4</DimensionID>
							</Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
							<Type>ProcessFull</Type>
							<Object>
								<DatabaseID>DataWareHouseWH</DatabaseID>
								<DimensionID>Wallet_a0ec0154-248e-41c0-b4ec-cf8bb4a300f7</DimensionID>
							</Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
							<Type>ProcessFull</Type>
							<Object>
								<DatabaseID>DataWareHouseWH</DatabaseID>
								<DimensionID>Transaction_ef4ab73f-686c-4f5b-b13f-1c8d87a763f9</DimensionID>
							</Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
							<Type>ProcessFull</Type>
							<Object>
								<DatabaseID>DataWareHouseWH</DatabaseID>
								<DimensionID>Bet_a1110428-ff63-4703-9ab5-c542dfaa5c6e</DimensionID>
							</Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
							<Type>ProcessFull</Type>
							<Object>
								<DatabaseID>DataWareHouseWH</DatabaseID>
								<DimensionID>BetType_032ecf96-0aa2-401b-ac7e-d4a7748e24aa</DimensionID>
							</Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
							<Type>ProcessFull</Type>
							<Object>
								<DatabaseID>DataWareHouseWH</DatabaseID>
								<DimensionID>KPI_79e0de34-c7ce-4744-8f19-4249e0c4c7b5</DimensionID>
							</Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
							<Type>ProcessFull</Type>
							<Object>
								<DatabaseID>DataWareHouseWH</DatabaseID>
								<DimensionID>LegType_b5eabb5c-1aff-474d-91ea-07e25f093fad</DimensionID>
							</Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
							<Type>ProcessFull</Type>
							<Object>
								<DatabaseID>DataWareHouseWH</DatabaseID>
								<DimensionID>Position_0f67ea54-27c6-4e4f-8a38-698905f2c48b</DimensionID>
							</Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
							<Type>ProcessFull</Type>
							<Object>
								<DatabaseID>DataWareHouseWH</DatabaseID>
								<DimensionID>FirstBetType_eb6b5379-14f9-4d8b-9e5c-ef447e4a3d15</DimensionID>
							</Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
							<Type>ProcessFull</Type>
							<Object>
								<DatabaseID>DataWareHouseWH</DatabaseID>
								<DimensionID>LastBetType_169fa75e-f04f-445e-85bf-6fb0c28ebae1</DimensionID>
							</Object>
						</Process>
						<Process xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
							<Type>ProcessFull</Type>
							<Object>
								<DatabaseID>DataWareHouseWH</DatabaseID>
								<DimensionID>Market_432a74ac-7865-49ef-a685-5ec72d399e6f</DimensionID>
							</Object>
						</Process>'

	EXEC(@ProcessFull) AT [WHTABULARMODEL]
*/
PRINT ('Disabled');
END
