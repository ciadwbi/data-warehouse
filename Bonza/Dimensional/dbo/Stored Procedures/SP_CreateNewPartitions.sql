CREATE PROC dbo.SP_CreateNewPartitions
AS BEGIN
/*
DECLARE @SQL VARCHAR(4000)
DECLARE @DATE  DATE			='2014-12-02'

WHILE @DATE<'2015-02-01'

BEGIN

SELECT @SQL =
				'<Alter AllowCreate="true" ObjectExpansion="ExpandFull" xmlns="http://schemas.microsoft.com/analysisservices/2003/engine">
				  <Object>
					<DatabaseID>DataWareHouseWH</DatabaseID>
					<CubeID>Model</CubeID>
					<MeasureGroupID>Position_0f67ea54-27c6-4e4f-8a38-698905f2c48b</MeasureGroupID>
					<PartitionID>Position_' + CONVERT(VARCHAR(11),@DATE) + '</PartitionID>
				  </Object>
				  <ObjectDefinition>
					<Partition xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:ddl2="http://schemas.microsoft.com/analysisservices/2003/engine/2" xmlns:ddl2_2="http://schemas.microsoft.com/analysisservices/2003/engine/2/2" xmlns:ddl100_100="http://schemas.microsoft.com/analysisservices/2008/engine/100/100" xmlns:ddl200="http://schemas.microsoft.com/analysisservices/2010/engine/200" xmlns:ddl200_200="http://schemas.microsoft.com/analysisservices/2010/engine/200/200" xmlns:ddl300="http://schemas.microsoft.com/analysisservices/2011/engine/300" xmlns:ddl300_300="http://schemas.microsoft.com/analysisservices/2011/engine/300/300" xmlns:ddl400="http://schemas.microsoft.com/analysisservices/2012/engine/400" xmlns:ddl400_400="http://schemas.microsoft.com/analysisservices/2012/engine/400/400">
					  <ID>Position_' + CONVERT(VARCHAR(11),@DATE) + '</ID>
					  <Name>Position_' + CONVERT(VARCHAR(11),@DATE) + '</Name>
					  <Source xsi:type="QueryBinding">
						<DataSourceID>1a159e14-b540-4f22-99a2-0aa8834e22e7</DataSourceID>
						<QueryDefinition>
								SELECT BalanceKey,AccountKey,P.DayKey,ActivityKey,LifecycleKey,FirstDepositDayKey,FirstBetPlacedDayKey,FirstBetSettledDayKey,FirstBetTypeKey,FirstClassKey,FirstChannelKey,
								LastBetPlacedDayKey,LastBetSettledDayKey,LastBetTypeKey,LastClassKey,LastChannelKey,SourceOpeningBalance,OpeningBalance,ClosingBalance,LifetimeTurnover,LifetimeGrossWin,0 AS WalletKey
								FROMFact.POSITION P WITH(NOLOCK)
								INNER JOIN Dimension.Day D WITH(NOLOCK) ON P.DayKey=D.DayKey
								WHERE D.DayDate	=''' + CONVERT(VARCHAR(11),@DATE) + '''
						</QueryDefinition>
					  </Source>
					  <StorageMode valuens="ddl200_200">InMemory</StorageMode>
					  <ProcessingMode>Regular</ProcessingMode>
					  <ddl300_300:DirectQueryUsage>InMemoryOnly</ddl300_300:DirectQueryUsage>
					</Partition>
				  </ObjectDefinition>
				</Alter>'

--SELECT @SQL 

EXEC(@SQL) AT [WHTABULARMODEL]

SELECT @DATE= MIN(DAYDATE) FROM Dimension.DAY WHERE DayDate>@DATE AND DayDate=AnalysisDayDate

END
*/
PRINT ('Disabled');
END
