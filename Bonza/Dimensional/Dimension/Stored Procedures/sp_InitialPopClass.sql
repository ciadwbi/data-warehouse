CREATE PROCEDURE [Dimension].sp_InitialPopClass
AS
	SET XACT_ABORT ON;

	SET IDENTITY_INSERT Dimension.Class ON 

	;WITH x (	ClassKey,	Source,		SubClassId,	SourceSubClassName,		SubClassCode,	SubClassName,	ClassId,	SourceClassName,				ClassCode,	ClassName,						SuperclassKey,	SuperclassName, FromDate,					ToDate						) AS
	(	SELECT	-1,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		-1,			'Unknown',						'UNKN',		'Unknown',						-1,				'Unknown',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	0,			'IBT',		NULL,		NULL,					'N/A',			'N/A',			0,			'N/A',							'NOTA',		'N/A',							0,				'N/A',			'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	-2,			'PDG',		NULL,		NULL,					'PDG',			'Pending',		-2,			'Pending',						'PDG',		'Pending',						-2,				'Pending',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	-10,		'LOT',		NULL,		NULL,					'UNK',			'Unknown',		-10,		'Lottery',						'LOTT',		'Lottery',						4,				'Gaming',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	2,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		1,			'Racing',						'HORS',		'Horse',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	4,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		2,			'Trots',						'HARN',		'Harness',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	5,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		3,			'Greyhounds',					'GREY',		'Greyhound',					1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	8,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		4,			'Rugby Union',					'RGUN',		'Rugby Union',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	9,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		5,			'Australian Rules',				'AFLR',		'Australian Rules (AFL)',		2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	12,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		6,			'Golf',							'GOLF',		'Golf',							2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	14,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		7,			'Soccer',						'SOCC',		'Soccer',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	15,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		8,			'Basketball - US',				'BASK',		'Basketball',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	17,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		9,			'American Football',			'NHLF',		'American Football (NFL)',		2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	20,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		10,			'Tennis',						'TENN',		'Tennis',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	21,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		11,			'Cricket',						'CRIC',		'Cricket',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	24,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		12,			'Rugby League',					'RGLE',		'Rugby League (NRL)',			2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	25,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		13,			'Boxing',						'BOXI',		'Boxing',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	27,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		14,			'Goalkicking Head To Heads',	'AFLR',		'Australian Rules (AFL)',		2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	29,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		15,			'Possession Head To Heads',		'AFLR',		'Australian Rules (AFL)',		2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	31,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		16,			'Brownlow Head To Heads',		'AFLR',		'Australian Rules (AFL)',		2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	33,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		17,			'Brownlow Club Total H-H',		'AFLR',		'Australian Rules (AFL)',		2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	35,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		18,			'Brownlow-Flag Double',			'AFLR',		'Australian Rules (AFL)',		2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	37,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		19,			'Ice Hockey - NHL',				'ICEH',		'Ice Hockey',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	39,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		20,			'Baseball',						'BASE',		'Baseball',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	41,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		21,			'Basketball - Aus',				'BASK',		'Basketball',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	44,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		22,			'Soccer - A League',			'SOCC',		'Soccer',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	46,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		23,			'Jockey Challenge',				'HORS',		'Horse',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	47,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		24,			'Handball',						'HAND',		'Handball',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	50,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		25,			'Feature Races',				'HORS',		'Horse',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	51,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		26,			'Entertainment',				'ENTT',		'Entertainment',				3,				'Novelty',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	53,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		27,			'Basketball - Europe',			'BASK',		'Basketball',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	56,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		28,			'SydneyGolf',					'GOLF',		'Golf',							2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	58,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		29,			'Surfing',						'SURF',		'Surfing',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	59,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		30,			'Cycling',						'CYCL',		'Cycling',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	61,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		31,			'Soccer - Euro 2012',			'SOCC',		'Soccer',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	64,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		33,			'Darts',						'DART',		'Darts',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	66,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		34,			'Elections',					'ELEC',		'Elections',					3,				'Novelty',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	68,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		35,			'Favourites Challenge',			'UNKN',		'Unknown',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	71,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		36,			'Premiership Double',			'UNKN',		'Unknown',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	72,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		37,			'Alpine Sports',				'ALPI',		'Alpine Sports',				2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	74,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		38,			'Snooker / Pool',				'SNOO',		'Snooker/Pool',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	77,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		39,			'Yachting',						'YACH',		'Yachting',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	78,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		40,			'Netball',						'NETB',		'Netball',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	79,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		41,			'Motor Sport',					'MOTR',		'Motor Racing',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	80,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		42,			'Insurance',					'UNKN',		'Unknown',						-1,				'Unknown',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	81,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		43,			'Ice Hockey - Europe',			'ICEH',		'Ice Hockey',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	82,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		44,			'Soccer - World Cup',			'SOCC',		'Soccer',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	83,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		46,			'2012  London Games',			'WRGM',		'World Games',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	84,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		47,			'Rowing',						'ROWG',		'Rowing',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	85,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		48,			'Field Hockey',					'HOCK',		'Field Hockey',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	86,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		50,			'Greys Box Challenge',			'GREY',		'Greyhound',					1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	87,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		51,			'Swimming',						'SWIM',		'Swimming',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	88,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		52,			'Hong Kong Racing',				'HORS',		'Horse',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	89,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		53,			'Basketball - World',			'BASK',		'Basketball',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	90,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		100,		'Special Events',				'UNKN',		'Unknown',						-1,				'Unknown',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	91,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		104,		'Promotions',					'UNKN',		'Unknown',						-1,				'Unknown',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	92,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		105,		'Content',						'UNKN',		'Unknown',						-1,				'Unknown',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	93,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		107,		'Athletics',					'ATHL',		'Athletics',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	94,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		108,		'Martial Arts',					'MART',		'Martial Arts',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	95,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		109,		'Soccer - African Cup',			'SOCC',		'Soccer',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	96,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		110,		'Lacrosse',						'LACR',		'Lacrosse',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	97,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		111,		'Canadian Football',			'CFLF',		'Canadian Football (CFL)',		2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	98,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		112,		'Volleyball',					'VOLB',		'Volleyball',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	99,			'IBT',		NULL,		NULL,					'UNK',			'Unknown',		114,		'Lawn Bowls',					'LAWB',		'Lawn Bowls',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	100,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		115,		'Happy Hour!',					'UNKN',		'Unknown',						-1,				'Unknown',		'1900-01-01',				'2014-03-26 14:03:40.543'	UNION ALL
		SELECT	101,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		115,		'AFL Specials',					'AFLR',		'Australian Rules (AFL)',		2,				'Sports',		'2014-03-26 14:03:40.543',	'2014-04-11 14:17:44.130'	UNION ALL
		SELECT	102,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		115,		'Friday Favourites',			'UNKN',		'Unknown',						-1,				'Unknown',		'2014-04-11 14:17:44.130',	'2014-04-17 09:52:15.860'	UNION ALL
		SELECT	103,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		115,		'AFL Specials',					'AFLR',		'Australian Rules (AFL)',		2,				'Sports',		'2014-04-17 09:52:15.860',	'2014-04-23 09:39:35.560'	UNION ALL
		SELECT	104,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		115,		'$2 Fav',						'UNKN',		'Unknown',						-1,				'Unknown',		'2014-04-23 09:39:35.560',	'2014-04-23 15:57:44.973'	UNION ALL
		SELECT	107,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		115,		'Friday Favourites',			'UNKN',		'Unknown',						-1,				'Unknown',		'2014-04-23 15:57:44.973',	'9999-12-31'				UNION ALL
		SELECT	108,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		116,		'Fantasy AFL',					'AFLR',		'Australian Rules (AFL)',		2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	109,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		117,		'Fantasy NRL',					'RGLE',		'Rugby League (NRL)',			2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	110,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		118,		'Financials',					'FINA',		'Financials',					3,				'Novelty',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	111,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		119,		'AFL & NRL Specials',			'UNKN',		'Unknown',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	112,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		120,		'Racing & Sport Doubles',		'UNKN',		'Unknown',						-1,				'Unknown',		'1900-01-01',				'2013-12-23 11:29:29.120'	UNION ALL
		SELECT	113,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		120,		'Sport Doubles',				'UNKN',		'Unknown',						2,				'Sports',		'2013-12-23 11:29:29.120',	'9999-12-31'				UNION ALL
		SELECT	114,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		121,		'2010 Delhi Games',				'WRGM',		'World Games',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	115,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		122,		'2010 Vancouver Games',			'WRGM',		'World Games',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	116,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		123,		'Water Polo',					'WAPO',		'Water Polo',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	117,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		124,		'Multi Specials',				'UNKN',		'Unknown',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	118,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		125,		'Fantasy - EPL',				'SOCC',		'Soccer',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	119,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		126,		'Fantasy - ALEAGUE',			'SOCC',		'Soccer',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	120,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		130,		'PTI Specials',					'UNKN',		'Unknown',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	121,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		131,		'Surf Ironman',					'SURI',		'Surf Ironman',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	122,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		132,		'GAA Sports',					'GAAS',		'Gaelic games (GAA)',			2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	123,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		133,		'First to Happen',				'UNKN',		'Unknown',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	124,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		134,		'Other Events',					'UNKN',		'Unknown',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	125,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		200,		'Live Telecast',				'UNKN',		'Unknown',						-1,				'Unknown',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	126,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		201,		'LIVE! Betting',				'UNKN',		'Unknown',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	127,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		202,		'Gaelic Football',				'GAAS',		'Gaelic games (GAA)',			2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	128,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		205,		'Kiosk Specials',				'UNKN',		'Unknown',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	129,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		206,		'Ice Hockey - Aust',			'ICEH',		'Ice Hockey',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	130,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		207,		'Soccer - Asian Cup',			'SOCC',		'Soccer',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	131,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		208,		'Rugby World Cup 2011',			'RGUN',		'Rugby Union',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	132,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		209,		'Pesapallo',					'PESA',		'Pesapallo',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	133,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		210,		'Badminton',					'BADM',		'Badminton',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	134,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		211,		'Bandy',						'BAND',		'Bandy',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	135,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		212,		'Commonwealth Games',			'WRGM',		'World Games',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	136,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		213,		'Floorball',					'FLOR',		'Floorball',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	137,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		214,		'Indoor Cricket',				'CRIC',		'Cricket',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	138,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		215,		'Skateboard',					'SKAT',		'Skateboard',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	139,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		216,		'Snowboarding',					'SNOW',		'Snowboarding',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	140,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		217,		'Table Tennis',					'TABT',		'Table Tennis',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	141,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		218,		'Winter Sports',				'WRGM',		'World Games',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	142,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		219,		'2008 Beijing Games',			'WRGM',		'World Games',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	143,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		220,		'Hour of Power',				'UNKN',		'Unknown',						-1,				'Unknown',		'1900-01-01',				'2014-05-06 17:13:13.690'	UNION ALL
		SELECT	144,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		220,		'Power Bet',					'UNKN',		'Unknown',						-1,				'Unknown',		'2014-05-06 17:13:13.690',	'9999-12-31'				UNION ALL
		SELECT	145,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		221,		'Inside Or Outside',			'HORS',		'Horse',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	146,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		222,		'Odd Or Even',					'HORS',		'Horse',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	147,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		223,		'Body Boarding',				'BODB',		'Body Boarding',				2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	148,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		224,		'Futsal',						'FUTS',		'Futsal',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	149,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		225,		'2014 Sochi Games',				'WRGM',		'World Games',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	150,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		226,		'Racing Specials',				'UNKN',		'Unknown',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	151,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		227,		'Quick Multis',					'UNKN',		'Unknown',						-1,				'Unknown',		'1900-01-01',				'2014-04-03 14:20:45.720'	UNION ALL
		SELECT	152,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		227,		'Take On Tom',					'UNKN',		'Unknown',						-1,				'Unknown',		'2014-04-03 14:20:45.720',	'9999-12-31'				UNION ALL
		SELECT	154,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		229,		'Quick Multis',					'UNKN',		'Unknown',						-1,				'Unknown',		'2014-04-04 12:11:03.240',	'9999-12-31'				UNION ALL
		SELECT	155,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		230,		'TAB Special',					'UNKN',		'Unknown',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	156,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		231,		'$2 Fav',						'UNKN',		'Unknown',						2,				'Sports',		'1900-01-01',				'2014-04-30 15:50:18.880'	UNION ALL
		SELECT	157,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		231,		'Big Bet',						'UNKN',		'Unknown',						2,				'Sports',		'2014-04-30 15:50:18.880',	'9999-12-31'				UNION ALL
		SELECT	158,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		232,		'Competitions',					'COMP',		'Competitions',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	159,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		233,		'Triathlon',					'TRIA',		'Triathlon',					2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	160,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		237,		'E-Sport',						'ESPO',		'E-Sport',						2,				'Sports',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	161,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		238,		'Head to Head',					'HORS',		'Horse',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	162,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		240,		'Fav vs Field',					'HORS',		'Horse',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	163,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		242,		'Half vs Half',					'HORS',		'Horse',						1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	164,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		243,		'Greys Box Challenge - Live',	'GREY',		'Greyhound',					1,				'Racing',		'1900-01-01',				'9999-12-31'				UNION ALL
		SELECT	165,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		244,		'Olympics 2016',				'WRGM',		'World Games',					2,				'Sports',		'2016-01-05 13:12:55.423',	'9999-12-31'				UNION ALL
		SELECT	166,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		239,		'Concession',					'HORS',		'Horse',						1,				'Racing',		'2016-01-20 11:34:48.357',	'9999-12-31'				UNION ALL
		SELECT	167,		'IBT',		NULL,		NULL,					'UNK',			'Unknown',		241,		'Fav Out',						'HORS',		'Horse',						1,				'Racing',		'2016-01-20 11:34:48.357',	'9999-12-31'
		EXCEPT
		SELECT	ClassKey,	Source,		SubClassId,	SourceSubClassName,		SubClassCode,	SubClassName,	ClassId,	SourceClassName,				ClassCode,	ClassName,						SuperclassKey,	SuperclassName, FromDate,						ToDate							FROM Dimension.Class
	)
	MERGE Dimension.Class m
	USING (SELECT *, HASHBYTES('MD5','|'+CONVERT(varchar(20),ClassId)+'|'+ISNULL(CONVERT(varchar(20),SubClassId)+'|','')) HashKey FROM x) u ON u.ClassKey = m.ClassKey
	WHEN NOT MATCHED THEN 
		INSERT (ClassKey, HashKey, Source, SubClassId, SourceSubClassName, SubClassCode, SubClassName, ClassId, SourceClassName, ClassCode, ClassName, SuperclassKey, SuperclassName, FromDate, ToDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy)
		VALUES (u.ClassKey, u.HashKey, u.Source, u.SubClassId, u.SourceSubClassName, u.SubClassCode, u.SubClassName, u.ClassId, u.SourceClassName, u.ClassCode, u.ClassName, u.SuperclassKey, u.SuperclassName, u.FromDate, u.ToDate, GETDATE(), 0, 'sp_InitialPopClass', GETDATE(), 0, 'sp_InitialPopClass')
	WHEN MATCHED THEN 
		UPDATE SET		Source 				= u.Source 				,	
						HashKey 			= u.HashKey 			,	
						SubClassId 			= u.SubClassId 			,	
						SourceSubClassName	= u.SourceSubClassName	,
						SubClassCode 		= u.SubClassCode 		,	
						SubClassName 		= u.SubClassName 		,	
						ClassId 			= u.ClassId 			,	
						SourceClassName		= u.SourceClassName		,
						ClassCode 			= u.ClassCode 			,	
						ClassName 			= u.ClassName 			,	
						SuperclassKey 		= u.SuperclassKey 		,
						SuperclassName 		= u.SuperclassName 		,
						FromDate 			= u.FromDate 			,	
						ToDate 				= u.ToDate 				,	
						ModifiedDate 		= GETDATE()				,
						ModifiedBatchKey	= 0						,
						ModifiedBy			= 'sp_InitialPopClass';

	SET IDENTITY_INSERT Dimension.Class OFF;
