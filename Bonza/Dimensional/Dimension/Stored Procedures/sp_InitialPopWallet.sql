CREATE PROCEDURE [Dimension].[sp_InitialPopWallet]
AS
	SET XACT_ABORT ON;

	BEGIN TRANSACTION Wallet
	SET IDENTITY_INSERT Dimension.Wallet ON 

	;WITH u (	WalletKey,	WalletId,	WalletName,		FromDate		) AS
	(	SELECT	-1,			'U',		'Unknown',		'1900-01-01'	UNION ALL 
		SELECT	0,			'N',		'N/A',			'1900-01-01'	UNION ALL
		SELECT	1,			'C',		'Cash',			'1900-01-01'	UNION ALL
		SELECT	2,			'B',		'Bonus',		'1900-01-01'	UNION ALL
		SELECT	3,			'R',		'Reward',		'2017-03-01'	UNION ALL
		SELECT	4,			'V',		'Velocity',		'2017-03-01'
	)
	MERGE Dimension.Wallet m
	USING u ON u.WalletKey = m.WalletKey
	WHEN MATCHED AND (m.WalletId <> u.WalletId OR m.WalletName <> u.WalletName) THEN 
		UPDATE SET WalletId = u.WalletId, WalletName = u.WalletName, FromDate = u.FromDate, ModifiedDate = GETDATE(), ModifiedBatchKey = 0, ModifiedBy = 'sp_InitialPopWallet'  
	WHEN NOT MATCHED THEN 
		INSERT (WalletKey, WalletId, WalletName, FromDate, ToDate, FirstDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy)
		VALUES (u.WalletKey, u.WalletId, u.WalletName, u.FromDate, '9999-12-31', u.FromDate, GETDATE(), 0, 'sp_InitialPopWallet', GETDATE(), 0, 'sp_InitialPopWallet');

	SET IDENTITY_INSERT Dimension.Wallet OFF;
	COMMIT TRANSACTION Wallet;
