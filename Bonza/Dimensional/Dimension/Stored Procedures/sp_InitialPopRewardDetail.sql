CREATE PROCEDURE [Dimension].sp_InitialPopRewardDetail
AS
	SET XACT_ABORT ON;

	SET IDENTITY_INSERT Dimension.RewardDetail ON 

	;WITH x (	RewardDetailKey,	RewardTransactionTypeKey,	TransactionCode,				RewardTransactionTypeCode,	RewardTransactionType, RewardTransactionClassKey,	RewardTransactionClassCode,	RewardTransactionClass,	RewardBetTypeKey,	RewardBetTypeId,	RewardBetTypeCode,	RewardBetType,		RewardReasonKey,	RewardReason,	RewardReasonTypeKey,	RewardReasonTypeCode,	RewardReasonType,	DirectionCode,	FromDate,		ToDate			) AS
	(	SELECT	-1,					-1,							'Unknown',						'U',						'Unknown',				-1,							'U',						'Unknown',				-1,					-2,					'U',				'Unknown',			-1,					'Unknown',		-1,						'U',					'Unknown',			'U',			'2017-03-01',	'9999-12-31'	UNION ALL
		SELECT	0,					0,							'N/A',							'X',						'N/A',					0,							'X',						'N/A',					0,					-1,					'X',				'N/A',				0,					'N/A',			0,						'X',					 'N/A',				'X',			'2017-03-01',	'9999-12-31'	UNION ALL
		SELECT	1,					1,							'Rewards.PointsAccrued',		'B',						'Bet',					1,							'A',						'Accrual',				1,					0,					'F',				'RacingFixedPrice',	0,					'N/A',			0,						'X',					 'N/A',				'+',			'2017-03-01',	'9999-12-31'	UNION ALL
		SELECT	2,					1,							'Rewards.PointsAccrued',		'B',						'Bet',					1,							'A',						'Accrual',				2,					1,					'E',				'ExoticBet',		0,					'N/A',			0,						'X',					 'N/A',				'+',			'2017-03-01',	'9999-12-31'	UNION ALL
		SELECT	3,					1,							'Rewards.PointsAccrued',		'B',						'Bet',					1,							'A',						'Accrual',				3,					2,					'T',				'ToteWinOrPlace',	0,					'N/A',			0,						'X',					 'N/A',				'+',			'2017-03-01',	'9999-12-31'	UNION ALL
		SELECT	4,					1,							'Rewards.PointsAccrued',		'B',						'Bet',					1,							'A',						'Accrual',				4,					3,					'S',				'Sport',			0,					'N/A',			0,						'X',					 'N/A',				'+',			'2017-03-01',	'9999-12-31'	UNION ALL
		SELECT	5,					1,							'Rewards.PointsAccrued',		'B',						'Bet',					1,							'A',						'Accrual',				5,					4,					'P',				'PhoneBet',			0,					'N/A',			0,						'X',					 'N/A',				'+',			'2017-03-01',	'9999-12-31'	UNION ALL
		SELECT	6,					1,							'Rewards.PointsAccrued',		'B',						'Bet',					1,							'A',						'Accrual',				6,					5,					'M',				'Multi',			0,					'N/A',			0,						'X',					 'N/A',				'+',			'2017-03-01',	'9999-12-31'	UNION ALL
		SELECT	7,					2,							'Rewards.PointsAdjusted',		'A',						'Adjustment',			2,							'D',						'Adjustment',			0,					-1,					'X',				'N/A',				-1,					'Unknown',		-1,						'U',					 'Unknown',			'+',			'2017-03-01',	'9999-12-31'	UNION ALL
		SELECT	8,					2,							'Rewards.PointsAdjusted',		'A',						'Adjustment',			2,							'D',						'Adjustment',			0,					-1,					'X',				'N/A',				-1,					'Unknown',		-1,						'U',					 'Unknown',			'-',			'2017-03-01',	'9999-12-31'	UNION ALL
		SELECT	9,					3,							'Rewards.BonusRedemption',		'O',						'Bonus',				3,							'R',						'Redemption',			0,					-1,					'X',				'N/A',				0,					'N/A',			 0,						'X',					 'N/A',				'-',			'2017-03-01',	'9999-12-31'	UNION ALL
		SELECT	10,					4,							'Rewards.VelocityRedemption',	'V',						'Velocity',				3,							'R',						'Redemption',			0,					-1,					'X',				'N/A',				0,					'N/A',			 0,						'X',					 'N/A',				'-',			'2017-03-01',	'9999-12-31'
		EXCEPT
		SELECT	RewardDetailKey,	RewardTransactionTypeKey,	TransactionCode,				RewardTransactionTypeCode,	RewardTransactionType, RewardTransactionClassKey,	RewardTransactionClassCode,	RewardTransactionClass,	RewardBetTypeKey,	RewardBetTypeId,	RewardBetTypeCode,	RewardBetType,		RewardReasonKey,	RewardReason,	RewardReasonTypeKey,	RewardReasonTypeCode,	RewardReasonType,	DirectionCode,	FromDate,		ToDate			FROM dIMENSION.RewardDetail
	)
	MERGE Dimension.RewardDetail m
	USING (SELECT *, CONVERT(char(32),HASHBYTES('MD5',upper(RewardReason)),2) RewardReasonId FROM x) u ON u.RewardDetailKey = m.RewardDetailKey
	WHEN NOT MATCHED THEN 
		INSERT (RewardDetailKey, RewardTransactionTypeKey, TransactionCode, RewardTransactionTypeCode, RewardTransactionType, RewardTransactionClassKey, RewardTransactionClassCode, RewardTransactionClass, RewardBetTypeKey, RewardBetTypeId, RewardBetTypeCode, RewardBetType, RewardReasonKey, RewardReasonId, RewardReason, RewardReasonTypeKey, RewardReasonTypeCode, RewardReasonType, DirectionCode, FirstDate, FromDate, ToDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy)
		VALUES (u.RewardDetailKey, u.RewardTransactionTypeKey, u.TransactionCode, u.RewardTransactionTypeCode, u.RewardTransactionType, u.RewardTransactionClassKey, u.RewardTransactionClassCode, u.RewardTransactionClass, u.RewardBetTypeKey, u.RewardBetTypeId, u.RewardBetTypeCode, u.RewardBetType, u.RewardReasonKey, u.RewardReasonId, u.RewardReason, u.RewardReasonTypeKey, u.RewardReasonTypeCode, u.RewardReasonType, u.DirectionCode, u.FromDate, u.FromDate, u.ToDate, GETDATE(), 0, 'sp_InitialPopRewardDetail', GETDATE(), 0, 'sp_InitialPopRewardDetail')
	WHEN MATCHED THEN 
		UPDATE SET		RewardTransactionTypeKey	= u.RewardTransactionTypeKey	,
						TransactionCode				= u.TransactionCode				,
						RewardTransactionTypeCode	= u.RewardTransactionTypeCode	,
						RewardTransactionType		= u.RewardTransactionType		,
						RewardTransactionClassKey	= u.RewardTransactionClassKey	,
						RewardTransactionClassCode	= u.RewardTransactionClassCode	,
						RewardTransactionClass		= u.RewardTransactionClass		,
						RewardBetTypeKey			= u.RewardBetTypeKey			,
						RewardBetTypeId				= u.RewardBetTypeId				,
						RewardBetTypeCode			= u.RewardBetTypeCode			,
						RewardBetType				= u.RewardBetType				,
						RewardReasonKey				= u.RewardReasonKey				,
						RewardReasonId				= u.RewardReasonId				,
						RewardReason				= u.RewardReason				,
						RewardReasonTypeKey			= u.RewardReasonTypeKey			,
						RewardReasonTypeCode		= u.RewardReasonTypeCode		,
						RewardReasonType			= u.RewardReasonType			,
						DirectionCode				= u.DirectionCode				,
						FirstDate 					= u.FromDate 					,	
						FromDate 					= u.FromDate 					,	
						ToDate 						= u.ToDate 						,	
						ModifiedDate 				= GETDATE()						,
						ModifiedBatchKey			= 0								,
						ModifiedBy					= 'sp_InitialPopRewardDetail';



	SET IDENTITY_INSERT Dimension.RewardDetail OFF;

