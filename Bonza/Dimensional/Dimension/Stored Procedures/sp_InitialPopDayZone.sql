Create Procedure [Dimension].sp_InitialPopDayZone

as

IF not exists (Select	[DayKey] from [Dimension].[DayZone])
Begin
Begin Transaction

INSERT [Dimension].[DayZone] ([DayKey], [MasterDayKey], [AnalysisDayKey], [MasterDayText], [AnalysisDayText], [FromDate], [ToDate],[CreatedDate],[CreatedBatchKey],[CreatedBy],[ModifiedDate],[ModifiedBatchKey],[ModifiedBy]) VALUES (-1, -1, -1, N'UNK', N'UNK', NULL, NULL, getdate(), 0, 'sp_InitialPopDayZone', getdate(), 0, 'sp_InitialPopDayZone')
INSERT [Dimension].[DayZone] ([DayKey], [MasterDayKey], [AnalysisDayKey], [MasterDayText], [AnalysisDayText], [FromDate], [ToDate],[CreatedDate],[CreatedBatchKey],[CreatedBy],[ModifiedDate],[ModifiedBatchKey],[ModifiedBy]) VALUES (0, 0, 0, N'N/A', N'N/A', NULL, NULL, getdate(), 0, 'sp_InitialPopDayZone', getdate(), 0, 'sp_InitialPopDayZone')
INSERT [Dimension].[DayZone] ([DayKey], [MasterDayKey], [AnalysisDayKey], [MasterDayText], [AnalysisDayText], [FromDate], [ToDate],[CreatedDate],[CreatedBatchKey],[CreatedBy],[ModifiedDate],[ModifiedBatchKey],[ModifiedBy]) VALUES (1990010100, 19900101, 19900101, N'1990-01-01', N'1990-01-01', '1990-01-01 00:00:00.000', '1990-01-01 22:30:00.000', getdate(), 0, 'sp_InitialPopDayZone', getdate(), 0, 'sp_InitialPopDayZone')
INSERT [Dimension].[DayZone] ([DayKey], [MasterDayKey], [AnalysisDayKey], [MasterDayText], [AnalysisDayText], [FromDate], [ToDate],[CreatedDate],[CreatedBatchKey],[CreatedBy],[ModifiedDate],[ModifiedBatchKey],[ModifiedBy]) VALUES (1990010101, 19900101, 19900102, N'1990-01-01', N'1990-01-02', '1990-01-01 22:30:00.000', '1990-01-02 00:00:00.000', getdate(), 0, 'sp_InitialPopDayZone', getdate(), 0, 'sp_InitialPopDayZone')
INSERT [Dimension].[DayZone] ([DayKey], [MasterDayKey], [AnalysisDayKey], [MasterDayText], [AnalysisDayText], [FromDate], [ToDate],[CreatedDate],[CreatedBatchKey],[CreatedBy],[ModifiedDate],[ModifiedBatchKey],[ModifiedBy]) VALUES (1990010102, 19900101, -1, N'1990-01-01', N'N/A', NULL, NULL, getdate(), 0, 'sp_InitialPopDayZone', getdate(), 0, 'sp_InitialPopDayZone')
INSERT [Dimension].[DayZone] ([DayKey], [MasterDayKey], [AnalysisDayKey], [MasterDayText], [AnalysisDayText], [FromDate], [ToDate],[CreatedDate],[CreatedBatchKey],[CreatedBy],[ModifiedDate],[ModifiedBatchKey],[ModifiedBy]) VALUES (1990010103, -1, 19900101, N'N/A', N'1990-01-01', NULL, NULL, getdate(), 0, 'sp_InitialPopDayZone', getdate(), 0, 'sp_InitialPopDayZone')

 
IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End