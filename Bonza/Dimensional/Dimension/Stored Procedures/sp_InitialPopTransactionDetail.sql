CREATE PROCEDURE [Dimension].sp_InitialPopTransactionDetail
AS

	SET XACT_ABORT ON;

	BEGIN TRANSACTION TransactionDetail
	SET IDENTITY_INSERT Dimension.TransactionDetail ON 

	;WITH u (TransactionDetailKey, TransactionDetailId, TransactionTypeID, TransactionType, TransactionStatusID, TransactionStatus, TransactionMethodID, TransactionMethod, TransactionDirection, TransactionSign, FromDate) AS
	(	SELECT -1, N'Unk ', N'UK', N'Unknown', N'UK', N'Unknown', N'UK', N'Unknown', N'UN', -9, '1900-01-01' UNION ALL
		SELECT 0, N'N/A ', N'NA', N'N/A', N'NA', N'N/A', N'NA', N'N/A', N'NA', -8, '1900-01-01' UNION ALL
		SELECT 3, N'BAI+', N'BT', N'Bet', N'AC', N'Accept', N'SY', N'System', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 4, N'BAI-', N'BT', N'Bet', N'AC', N'Accept', N'SY', N'System', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 5, N'BCI+', N'BT', N'Bet', N'CN', N'Cancel', N'VO', N'Void', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 6, N'BCI-', N'BT', N'Bet', N'CN', N'Cancel', N'VO', N'Void', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 7, N'BD&+', N'BT', N'Bet', N'DE', N'Decline', N'TC', N'Terms & Conditions', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 8, N'BD&-', N'BT', N'Bet', N'DE', N'Decline', N'TC', N'Terms & Conditions', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 9, N'BDA+', N'BT', N'Bet', N'DE', N'Decline', N'CT', N'Client', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 10, N'BDA-', N'BT', N'Bet', N'DE', N'Decline', N'CT', N'Client', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 11, N'BDC+', N'BT', N'Bet', N'DE', N'Decline', N'CO', N'Combination', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 12, N'BDC-', N'BT', N'Bet', N'DE', N'Decline', N'CO', N'Combination', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 13, N'BDD+', N'BT', N'Bet', N'DE', N'Decline', N'DU', N'Duplicate', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 14, N'BDD-', N'BT', N'Bet', N'DE', N'Decline', N'DU', N'Duplicate', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 15, N'BDE+', N'BT', N'Bet', N'DE', N'Decline', N'ER', N'Error', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 16, N'BDE-', N'BT', N'Bet', N'DE', N'Decline', N'ER', N'Error', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 17, N'BDI+', N'BT', N'Bet', N'DE', N'Decline', N'IP', N'In Play', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 18, N'BDI-', N'BT', N'Bet', N'DE', N'Decline', N'IP', N'In Play', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 19, N'BDL+', N'BT', N'Bet', N'DE', N'Decline', N'LI', N'Limit', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 20, N'BDL-', N'BT', N'Bet', N'DE', N'Decline', N'LI', N'Limit', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 21, N'BDN+', N'BT', N'Bet', N'DE', N'Decline', N'NO', N'Not Offered', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 22, N'BDN-', N'BT', N'Bet', N'DE', N'Decline', N'NO', N'Not Offered', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 23, N'BDO+', N'BT', N'Bet', N'DE', N'Decline', N'OF', N'Offer', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 24, N'BDO-', N'BT', N'Bet', N'DE', N'Decline', N'OF', N'Offer', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 25, N'BDP+', N'BT', N'Bet', N'DE', N'Decline', N'PC', N'Price Change', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 26, N'BDP-', N'BT', N'Bet', N'DE', N'Decline', N'PC', N'Price Change', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 27, N'BDR+', N'BT', N'Bet', N'DE', N'Decline', N'RE', N'Request', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 28, N'BDR-', N'BT', N'Bet', N'DE', N'Decline', N'RE', N'Request', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 29, N'BDS+', N'BT', N'Bet', N'DE', N'Decline', N'SU', N'Suspended', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 30, N'BDS-', N'BT', N'Bet', N'DE', N'Decline', N'SU', N'Suspended', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 31, N'BDT+', N'BT', N'Bet', N'DE', N'Decline', N'TE', N'Test', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 32, N'BDT-', N'BT', N'Bet', N'DE', N'Decline', N'TE', N'Test', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 33, N'BDU+', N'BT', N'Bet', N'DE', N'Decline', N'UK', N'Unknown', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 34, N'BDU-', N'BT', N'Bet', N'DE', N'Decline', N'UK', N'Unknown', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 35, N'BDW+', N'BT', N'Bet', N'DE', N'Decline', N'WI', N'Withdrawn', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 36, N'BDW-', N'BT', N'Bet', N'DE', N'Decline', N'WI', N'Withdrawn', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 39, N'BLA+', N'BT', N'Bet', N'LO', N'Lose', N'AD', N'Adjustment', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 40, N'BLA-', N'BT', N'Bet', N'LO', N'Lose', N'AD', N'Adjustment', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 41, N'BLI+', N'BT', N'Bet', N'LO', N'Lose', N'SE', N'Settlement', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 42, N'BLI-', N'BT', N'Bet', N'LO', N'Lose', N'SE', N'Settlement', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 43, N'BRI+', N'BT', N'Bet', N'RE', N'Request', N'SY', N'System', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 44, N'BRI-', N'BT', N'Bet', N'RE', N'Request', N'SY', N'System', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 45, N'BUI+', N'BT', N'Bet', N'UN', N'Unsettle', N'AD', N'Adjustment', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 46, N'BUI-', N'BT', N'Bet', N'UN', N'Unsettle', N'AD', N'Adjustment', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 47, N'BWA+', N'BT', N'Bet', N'WN', N'Win', N'AD', N'Adjustment', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 48, N'BWA-', N'BT', N'Bet', N'WN', N'Win', N'AD', N'Adjustment', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 51, N'BWI+', N'BT', N'Bet', N'WN', N'Win', N'SE', N'Settlement', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 52, N'BWI-', N'BT', N'Bet', N'WN', N'Win', N'SE', N'Settlement', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 53, N'DC$+', N'DP', N'Deposit', N'CO', N'Complete', N'CA', N'Cash', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 54, N'DC$-', N'DP', N'Deposit', N'CO', N'Complete', N'CA', N'Cash', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 55, N'DC2+', N'DP', N'Deposit', N'CO', N'Complete', N'B2', N'Bet 247', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 56, N'DC2-', N'DP', N'Deposit', N'CO', N'Complete', N'B2', N'Bet 247', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 57, N'DCA+', N'DP', N'Deposit', N'CO', N'Complete', N'SA', N'Sports Alive', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 58, N'DCA-', N'DP', N'Deposit', N'CO', N'Complete', N'SA', N'Sports Alive', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 59, N'DCB+', N'DP', N'Deposit', N'CO', N'Complete', N'BY', N'BPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 60, N'DCB-', N'DP', N'Deposit', N'CO', N'Complete', N'BY', N'BPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 61, N'DCC+', N'DP', N'Deposit', N'CO', N'Complete', N'CC', N'Credit Card', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 62, N'DCC-', N'DP', N'Deposit', N'CO', N'Complete', N'CC', N'Credit Card', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 63, N'DCE+', N'DP', N'Deposit', N'CO', N'Complete', N'EF', N'EFT', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 64, N'DCE-', N'DP', N'Deposit', N'CO', N'Complete', N'EF', N'EFT', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 65, N'DCF+', N'DP', N'Deposit', N'CO', N'Complete', N'EO', N'EFTPOS', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 66, N'DCF-', N'DP', N'Deposit', N'CO', N'Complete', N'EO', N'EFTPOS', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 67, N'DCL+', N'DP', N'Deposit', N'CO', N'Complete', N'PP', N'PayPal', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 68, N'DCL-', N'DP', N'Deposit', N'CO', N'Complete', N'PP', N'PayPal', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 69, N'DCM+', N'DP', N'Deposit', N'CO', N'Complete', N'MP', N'MasterPass', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 70, N'DCM-', N'DP', N'Deposit', N'CO', N'Complete', N'MP', N'MasterPass', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 71, N'DCN+', N'DP', N'Deposit', N'CO', N'Complete', N'NE', N'Neteller', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 72, N'DCN-', N'DP', N'Deposit', N'CO', N'Complete', N'NE', N'Neteller', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 73, N'DCP+', N'DP', N'Deposit', N'CO', N'Complete', N'PO', N'Poli', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 74, N'DCP-', N'DP', N'Deposit', N'CO', N'Complete', N'PO', N'Poli', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 75, N'DCQ+', N'DP', N'Deposit', N'CO', N'Complete', N'CH', N'Cheque', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 76, N'DCQ-', N'DP', N'Deposit', N'CO', N'Complete', N'CH', N'Cheque', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 77, N'DCS+', N'DP', N'Deposit', N'CO', N'Complete', N'SK', N'Skrill', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 78, N'DCS-', N'DP', N'Deposit', N'CO', N'Complete', N'SK', N'Skrill', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 79, N'DCU+', N'DP', N'Deposit', N'CO', N'Complete', N'UK', N'Unknown', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 80, N'DCU-', N'DP', N'Deposit', N'CO', N'Complete', N'UK', N'Unknown', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 81, N'DCZ+', N'DP', N'Deposit', N'CO', N'Complete', N'EP', N'EzyPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 82, N'DCZ-', N'DP', N'Deposit', N'CO', N'Complete', N'EP', N'EzyPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 83, N'DD$+', N'DP', N'Deposit', N'DE', N'Decline', N'CA', N'Cash', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 84, N'DD$-', N'DP', N'Deposit', N'DE', N'Decline', N'CA', N'Cash', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 85, N'DDB+', N'DP', N'Deposit', N'DE', N'Decline', N'BY', N'BPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 86, N'DDB-', N'DP', N'Deposit', N'DE', N'Decline', N'BY', N'BPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 87, N'DDC+', N'DP', N'Deposit', N'DE', N'Decline', N'CC', N'Credit Card', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 88, N'DDC-', N'DP', N'Deposit', N'DE', N'Decline', N'CC', N'Credit Card', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 89, N'DDE+', N'DP', N'Deposit', N'DE', N'Decline', N'EF', N'EFT', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 90, N'DDE-', N'DP', N'Deposit', N'DE', N'Decline', N'EF', N'EFT', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 91, N'DDF+', N'DP', N'Deposit', N'DE', N'Decline', N'EO', N'EFTPOS', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 92, N'DDF-', N'DP', N'Deposit', N'DE', N'Decline', N'EO', N'EFTPOS', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 93, N'DDL+', N'DP', N'Deposit', N'DE', N'Decline', N'PP', N'PayPal', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 94, N'DDL-', N'DP', N'Deposit', N'DE', N'Decline', N'PP', N'PayPal', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 95, N'DDM+', N'DP', N'Deposit', N'DE', N'Decline', N'MP', N'MasterPass', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 96, N'DDM-', N'DP', N'Deposit', N'DE', N'Decline', N'MP', N'MasterPass', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 97, N'DDN+', N'DP', N'Deposit', N'DE', N'Decline', N'NE', N'Neteller', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 98, N'DDN-', N'DP', N'Deposit', N'DE', N'Decline', N'NE', N'Neteller', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 99, N'DDP+', N'DP', N'Deposit', N'DE', N'Decline', N'PO', N'Poli', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 100, N'DDP-', N'DP', N'Deposit', N'DE', N'Decline', N'PO', N'Poli', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 101, N'DDQ+', N'DP', N'Deposit', N'DE', N'Decline', N'CH', N'Cheque', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 102, N'DDQ-', N'DP', N'Deposit', N'DE', N'Decline', N'CH', N'Cheque', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 103, N'DDS+', N'DP', N'Deposit', N'DE', N'Decline', N'MB', N'Moneybookers', N'CR', 1, '1900-01-01' UNION ALL

		SELECT 104, N'DDS-', N'DP', N'Deposit', N'DE', N'Decline', N'SK', N'Skrill', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 105, N'DDU+', N'DP', N'Deposit', N'DE', N'Decline', N'UK', N'Unknown', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 106, N'DDU-', N'DP', N'Deposit', N'DE', N'Decline', N'UK', N'Unknown', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 107, N'DDZ+', N'DP', N'Deposit', N'DE', N'Decline', N'EP', N'EzyPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 108, N'DDZ-', N'DP', N'Deposit', N'DE', N'Decline', N'EP', N'EzyPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 109, N'DR$+', N'DP', N'Deposit', N'RE', N'Request', N'CA', N'Cash', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 110, N'DRB+', N'DP', N'Deposit', N'RE', N'Request', N'BY', N'BPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 111, N'DRC+', N'DP', N'Deposit', N'RE', N'Request', N'CC', N'Credit Card', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 112, N'DRE+', N'DP', N'Deposit', N'RE', N'Request', N'EF', N'EFT', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 113, N'DRF+', N'DP', N'Deposit', N'RE', N'Request', N'EO', N'EFTPOS', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 114, N'DRL+', N'DP', N'Deposit', N'RE', N'Request', N'PP', N'PayPal', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 115, N'DRM+', N'DP', N'Deposit', N'RE', N'Request', N'MP', N'MasterPass', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 116, N'DRN+', N'DP', N'Deposit', N'RE', N'Request', N'NE', N'Neteller', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 117, N'DRP+', N'DP', N'Deposit', N'RE', N'Request', N'PO', N'Poli', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 118, N'DRQ+', N'DP', N'Deposit', N'RE', N'Request', N'CH', N'Cheque', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 119, N'DRS+', N'DP', N'Deposit', N'RE', N'Request', N'SK', N'Skrill', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 120, N'DRU+', N'DP', N'Deposit', N'RE', N'Request', N'UK', N'Unknown', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 121, N'DRZ+', N'DP', N'Deposit', N'RE', N'Request', N'EP', N'EzyPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 122, N'DV$+', N'DP', N'Deposit', N'RJ', N'Reject', N'CA', N'Cash', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 123, N'DV$-', N'DP', N'Deposit', N'RJ', N'Reject', N'CA', N'Cash', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 124, N'DVB+', N'DP', N'Deposit', N'RJ', N'Reject', N'BY', N'BPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 125, N'DVB-', N'DP', N'Deposit', N'RJ', N'Reject', N'BY', N'BPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 126, N'DVC+', N'DP', N'Deposit', N'RJ', N'Reject', N'CC', N'Credit Card', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 127, N'DVC-', N'DP', N'Deposit', N'RJ', N'Reject', N'CC', N'Credit Card', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 128, N'DVE+', N'DP', N'Deposit', N'RJ', N'Reject', N'EF', N'EFT', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 129, N'DVE-', N'DP', N'Deposit', N'RJ', N'Reject', N'EF', N'EFT', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 130, N'DVF+', N'DP', N'Deposit', N'RJ', N'Reject', N'EO', N'EFTPOS', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 131, N'DVF-', N'DP', N'Deposit', N'RJ', N'Reject', N'EO', N'EFTPOS', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 132, N'DVL+', N'DP', N'Deposit', N'RJ', N'Reject', N'PP', N'PayPal', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 133, N'DVL-', N'DP', N'Deposit', N'RJ', N'Reject', N'PP', N'PayPal', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 134, N'DVM+', N'DP', N'Deposit', N'RJ', N'Reject', N'MP', N'MasterPass', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 135, N'DVM-', N'DP', N'Deposit', N'RJ', N'Reject', N'MP', N'MasterPass', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 136, N'DVN+', N'DP', N'Deposit', N'RJ', N'Reject', N'NE', N'Neteller', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 137, N'DVN-', N'DP', N'Deposit', N'RJ', N'Reject', N'NE', N'Neteller', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 138, N'DVP+', N'DP', N'Deposit', N'RJ', N'Reject', N'PO', N'Poli', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 139, N'DVP-', N'DP', N'Deposit', N'RJ', N'Reject', N'PO', N'Poli', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 140, N'DVQ+', N'DP', N'Deposit', N'RJ', N'Reject', N'CH', N'Cheque', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 141, N'DVQ-', N'DP', N'Deposit', N'RJ', N'Reject', N'CH', N'Cheque', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 142, N'DVS+', N'DP', N'Deposit', N'RJ', N'Reject', N'MB', N'Moneybookers', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 143, N'DVS-', N'DP', N'Deposit', N'RJ', N'Reject', N'MB', N'Moneybookers', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 144, N'DVU+', N'DP', N'Deposit', N'RJ', N'Reject', N'UK', N'Unknown', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 145, N'DVU-', N'DP', N'Deposit', N'RJ', N'Reject', N'UK', N'Unknown', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 146, N'DVZ+', N'DP', N'Deposit', N'RJ', N'Reject', N'EP', N'EzyPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 147, N'DVZ-', N'DP', N'Deposit', N'RJ', N'Reject', N'EP', N'EzyPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 148, N'DXB-', N'DP', N'Deposit', N'CN', N'Cancel', N'BY', N'BPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 149, N'DXE-', N'DP', N'Deposit', N'CN', N'Cancel', N'EF', N'EFT', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 150, N'DXP-', N'DP', N'Deposit', N'CN', N'Cancel', N'PD', N'POLI Deposit', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 151, N'KAI+', N'BB', N'Betback', N'AC', N'Accept', N'SY', N'System', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 152, N'KAI-', N'BB', N'Betback', N'AC', N'Accept', N'SY', N'System', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 153, N'KCI+', N'BB', N'Betback', N'CN', N'Cancel', N'VO', N'Void', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 154, N'KCI-', N'BB', N'Betback', N'CN', N'Cancel', N'VO', N'Void', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 155, N'KLI+', N'BB', N'Betback', N'LO', N'Lose', N'SE', N'Settlement', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 156, N'KLI-', N'BB', N'Betback', N'LO', N'Lose', N'SE', N'Settlement', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 157, N'KWI+', N'BB', N'Betback', N'WN', N'Win', N'SE', N'Settlement', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 158, N'KWI-', N'BB', N'Betback', N'WN', N'Win', N'SE', N'Settlement', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 159, N'TCA+', N'TR', N'Transfer', N'CO', N'Complete', N'AT', N'Account Transfer', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 160, N'TCA-', N'TR', N'Transfer', N'CO', N'Complete', N'AT', N'Account Transfer', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 161, N'TCV+', N'TR', N'Transfer', N'CO', N'Complete', N'VI', N'VIP', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 162, N'TCV-', N'TR', N'Transfer', N'CO', N'Complete', N'VI', N'VIP', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 163, N'VCI+', N'BO', N'Bonus', N'CO', N'Complete', N'BO', N'Bonus', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 164, N'VCI-', N'BO', N'Bonus', N'CO', N'Complete', N'BO', N'Bonus', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 165, N'VEI+', N'BO', N'Bonus', N'EX', N'Expire', N'BO', N'Bonus', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 166, N'VEI-', N'BO', N'Bonus', N'EX', N'Expire', N'BO', N'Bonus', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 167, N'WC$-', N'WI', N'Withdrawal', N'CO', N'Complete', N'CA', N'Cash', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 168, N'WCB-', N'WI', N'Withdrawal', N'CO', N'Complete', N'BY', N'BPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 169, N'WCC-', N'WI', N'Withdrawal', N'CO', N'Complete', N'CC', N'Credit Card', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 170, N'WCE+', N'WI', N'Withdrawal', N'CO', N'Complete', N'EF', N'EFT', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 171, N'WCE-', N'WI', N'Withdrawal', N'CO', N'Complete', N'EF', N'EFT', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 172, N'WCL-', N'WI', N'Withdrawal', N'CO', N'Complete', N'PP', N'PayPal', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 173, N'WCN-', N'WI', N'Withdrawal', N'CO', N'Complete', N'NE', N'Neteller', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 174, N'WCQ-', N'WI', N'Withdrawal', N'CO', N'Complete', N'CH', N'Cheque', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 175, N'WCS-', N'WI', N'Withdrawal', N'CO', N'Complete', N'MB', N'Moneybookers', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 176, N'WCU-', N'WI', N'Withdrawal', N'CO', N'Complete', N'UK', N'Unknown', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 177, N'WD$+', N'WI', N'Withdrawal', N'DE', N'Decline', N'CA', N'Cash', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 178, N'WD$-', N'WI', N'Withdrawal', N'DE', N'Decline', N'CA', N'Cash', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 179, N'WDB+', N'WI', N'Withdrawal', N'DE', N'Decline', N'BY', N'BPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 180, N'WDB-', N'WI', N'Withdrawal', N'DE', N'Decline', N'BY', N'BPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 181, N'WDC+', N'WI', N'Withdrawal', N'DE', N'Decline', N'CC', N'Credit Card', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 182, N'WDC-', N'WI', N'Withdrawal', N'DE', N'Decline', N'CC', N'Credit Card', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 183, N'WDE+', N'WI', N'Withdrawal', N'DE', N'Decline', N'EF', N'EFT', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 185, N'WDE-', N'WI', N'Withdrawal', N'DE', N'Decline', N'EF', N'EFT', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 187, N'WDL+', N'WI', N'Withdrawal', N'DE', N'Decline', N'PP', N'PayPal', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 188, N'WDL-', N'WI', N'Withdrawal', N'DE', N'Decline', N'PP', N'PayPal', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 189, N'WDN+', N'WI', N'Withdrawal', N'DE', N'Decline', N'NE', N'Neteller', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 190, N'WDN-', N'WI', N'Withdrawal', N'DE', N'Decline', N'NE', N'Neteller', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 191, N'WDQ+', N'WI', N'Withdrawal', N'DE', N'Decline', N'CH', N'Cheque', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 193, N'WDQ-', N'WI', N'Withdrawal', N'DE', N'Decline', N'CH', N'Cheque', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 195, N'WDS+', N'WI', N'Withdrawal', N'DE', N'Decline', N'MB', N'Moneybookers', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 196, N'WDS-', N'WI', N'Withdrawal', N'DE', N'Decline', N'MB', N'Moneybookers', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 197, N'WDU+', N'WI', N'Withdrawal', N'DE', N'Decline', N'UK', N'Unknown', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 198, N'WDU-', N'WI', N'Withdrawal', N'DE', N'Decline', N'UK', N'Unknown', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 199, N'WR$+', N'WI', N'Withdrawal', N'RE', N'Request', N'CA', N'Cash', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 200, N'WR$-', N'WI', N'Withdrawal', N'RE', N'Request', N'CA', N'Cash', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 201, N'WRB+', N'WI', N'Withdrawal', N'RE', N'Request', N'BY', N'BPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 202, N'WRB-', N'WI', N'Withdrawal', N'RE', N'Request', N'BY', N'BPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 203, N'WRC+', N'WI', N'Withdrawal', N'RE', N'Request', N'CC', N'Credit Card', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 204, N'WRC-', N'WI', N'Withdrawal', N'RE', N'Request', N'CC', N'Credit Card', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 205, N'WRE+', N'WI', N'Withdrawal', N'RE', N'Request', N'EF', N'EFT', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 206, N'WRE-', N'WI', N'Withdrawal', N'RE', N'Request', N'EF', N'EFT', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 207, N'WRL+', N'WI', N'Withdrawal', N'RE', N'Request', N'PP', N'PayPal', N'CR', 1, '1900-01-01' UNION ALL

		SELECT 208, N'WRL-', N'WI', N'Withdrawal', N'RE', N'Request', N'PP', N'PayPal', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 209, N'WRN+', N'WI', N'Withdrawal', N'RE', N'Request', N'NE', N'Neteller', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 210, N'WRN-', N'WI', N'Withdrawal', N'RE', N'Request', N'NE', N'Neteller', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 211, N'WRQ+', N'WI', N'Withdrawal', N'RE', N'Request', N'CH', N'Cheque', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 212, N'WRQ-', N'WI', N'Withdrawal', N'RE', N'Request', N'CH', N'Cheque', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 213, N'WRS+', N'WI', N'Withdrawal', N'RE', N'Request', N'MB', N'Moneybookers', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 214, N'WRS-', N'WI', N'Withdrawal', N'RE', N'Request', N'MB', N'Moneybookers', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 215, N'WRU+', N'WI', N'Withdrawal', N'RE', N'Request', N'UK', N'Unknown', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 216, N'WRU-', N'WI', N'Withdrawal', N'RE', N'Request', N'UK', N'Unknown', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 217, N'WVB+', N'WI', N'Withdrawal', N'RJ', N'Reject', N'BY', N'BPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 218, N'WVB-', N'WI', N'Withdrawal', N'RJ', N'Reject', N'BY', N'BPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 219, N'WVE+', N'WI', N'Withdrawal', N'RJ', N'Reject', N'EF', N'EFT', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 220, N'WVE-', N'WI', N'Withdrawal', N'RJ', N'Reject', N'EF', N'EFT', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 221, N'WVL+', N'WI', N'Withdrawal', N'RJ', N'Reject', N'PP', N'PayPal', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 222, N'WVL-', N'WI', N'Withdrawal', N'RJ', N'Reject', N'PP', N'PayPal', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 223, N'WVN+', N'WI', N'Withdrawal', N'RJ', N'Reject', N'NE', N'Neteller', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 224, N'WVN-', N'WI', N'Withdrawal', N'RJ', N'Reject', N'NE', N'Neteller', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 225, N'WVQ+', N'WI', N'Withdrawal', N'RJ', N'Reject', N'CH', N'Cheque', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 226, N'WVQ-', N'WI', N'Withdrawal', N'RJ', N'Reject', N'CH', N'Cheque', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 227, N'WVS+', N'WI', N'Withdrawal', N'RJ', N'Reject', N'MB', N'Moneybookers', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 228, N'WVS-', N'WI', N'Withdrawal', N'RJ', N'Reject', N'MB', N'Moneybookers', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 229, N'WVU+', N'WI', N'Withdrawal', N'RJ', N'Reject', N'UK', N'Unknown', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 230, N'WVU-', N'WI', N'Withdrawal', N'RJ', N'Reject', N'UK', N'Unknown', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 231, N'WX$+', N'WI', N'Withdrawal', N'CN', N'Cancel', N'CA', N'Cash', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 232, N'WX$-', N'WI', N'Withdrawal', N'CN', N'Cancel', N'CA', N'Cash', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 233, N'WXC+', N'WI', N'Withdrawal', N'CN', N'Cancel', N'CC', N'Credit Card', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 234, N'WXC-', N'WI', N'Withdrawal', N'CN', N'Cancel', N'CC', N'Credit Card', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 235, N'WXE+', N'WI', N'Withdrawal', N'CN', N'Cancel', N'EF', N'EFT', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 236, N'WXE-', N'WI', N'Withdrawal', N'CN', N'Cancel', N'EF', N'EFT', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 237, N'WXQ+', N'WI', N'Withdrawal', N'CN', N'Cancel', N'CH', N'Cheque', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 238, N'WXQ-', N'WI', N'Withdrawal', N'CN', N'Cancel', N'CH', N'Cheque', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 261, N'ACA+', N'AD', N'Adjustment', N'CO', N'Complete', N'AA', N'Account Adjustments', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 262, N'ACA-', N'AD', N'Adjustment', N'CO', N'Complete', N'AA', N'Account Adjustments', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 263, N'ACB+', N'AD', N'Adjustment', N'CO', N'Complete', N'BA', N'Bet Adjustment', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 264, N'ACB-', N'AD', N'Adjustment', N'CO', N'Complete', N'BA', N'Bet Adjustment', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 265, N'ACC+', N'AD', N'Adjustment', N'CO', N'Complete', N'CB', N'Chargeback', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 266, N'ACC-', N'AD', N'Adjustment', N'CO', N'Complete', N'CB', N'Chargeback', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 267, N'ACO+', N'AD', N'Adjustment', N'CO', N'Complete', N'OA', N'Other Adjustments', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 268, N'ACO-', N'AD', N'Adjustment', N'CO', N'Complete', N'OA', N'Other Adjustments', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 269, N'ACU+', N'AD', N'Adjustment', N'CO', N'Complete', N'UK', N'Unknown', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 270, N'ACU-', N'AD', N'Adjustment', N'CO', N'Complete', N'UK', N'Unknown', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 271, N'ACW+', N'AD', N'Adjustment', N'CO', N'Complete', N'WO', N'Write Off', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 272, N'ACW-', N'AD', N'Adjustment', N'CO', N'Complete', N'WO', N'Write Off', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 273, N'PAB+', N'BO', N'Bonus', N'AW', N'Award', N'BP', N'BetPack', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 274, N'PAB-', N'BO', N'Bonus', N'AW', N'Award', N'BP', N'BetPack', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 275, N'PAC+', N'BO', N'Bonus', N'AW', N'Award', N'CR', N'Credits', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 276, N'PAC-', N'BO', N'Bonus', N'AW', N'Award', N'CR', N'Credits', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 277, N'PAR+', N'BO', N'Bonus', N'AW', N'Award', N'RB', N'Rebate', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 278, N'PAR-', N'BO', N'Bonus', N'AW', N'Award', N'RB', N'Rebate', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 279, N'PCB+', N'BO', N'Bonus', N'CN', N'Cancel', N'BP', N'BetPack', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 280, N'PCB-', N'BO', N'Bonus', N'CN', N'Cancel', N'BP', N'BetPack', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 281, N'PCC+', N'BO', N'Bonus', N'CN', N'Cancel', N'CR', N'Credits', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 282, N'PCC-', N'BO', N'Bonus', N'CN', N'Cancel', N'CR', N'Credits', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 283, N'PCR+', N'BO', N'Bonus', N'CN', N'Cancel', N'RB', N'Rebate', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 284, N'PCR-', N'BO', N'Bonus', N'CN', N'Cancel', N'RB', N'Rebate', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 285, N'ACY+', N'AD', N'Adjustment', N'CO', N'Complete', N'CL', N'CB Legacy', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 286, N'ACY-', N'AD', N'Adjustment', N'CO', N'Complete', N'CL', N'CB Legacy', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 289, N'WCY-', N'WI', N'Withdrawal', N'CO', N'Complete', N'CL', N'CB Legacy', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 290, N'WDY+', N'WI', N'Withdrawal', N'DE', N'Decline', N'CL', N'CB Legacy', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 291, N'WDY-', N'WI', N'Withdrawal', N'DE', N'Decline', N'CL', N'CB Legacy', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 292, N'WRY+', N'WI', N'Withdrawal', N'RE', N'Request', N'CL', N'CB Legacy', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 293, N'WRY-', N'WI', N'Withdrawal', N'RE', N'Request', N'CL', N'CB Legacy', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 294, N'WVY+', N'WI', N'Withdrawal', N'RJ', N'Reject', N'CL', N'CB Legacy', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 295, N'WVY-', N'WI', N'Withdrawal', N'RJ', N'Reject', N'CL', N'CB Legacy', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 296, N'BCS+', N'BT', N'Bet', N'CN', N'Cancel', N'SC', N'Scratched', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 297, N'BCS-', N'BT', N'Bet', N'CN', N'Cancel', N'SC', N'Scratched', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 298, N'TDA+', N'TR', N'Transfer', N'DE', N'Decline', N'AT', N'Account Transfer', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 299, N'TDA-', N'TR', N'Transfer', N'DE', N'Decline', N'AT', N'Account Transfer', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 302, N'WU$+', N'WI', N'Withdrawal', N'RV', N'Reverse', N'CA', N'Cash', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 303, N'WU$-', N'WI', N'Withdrawal', N'RV', N'Reverse', N'CA', N'Cash', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 304, N'WUB+', N'WI', N'Withdrawal', N'RV', N'Reverse', N'BY', N'BPay', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 305, N'WUB-', N'WI', N'Withdrawal', N'RV', N'Reverse', N'BY', N'BPay', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 306, N'WUC+', N'WI', N'Withdrawal', N'RV', N'Reverse', N'CC', N'Credit Card', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 307, N'WUC-', N'WI', N'Withdrawal', N'RV', N'Reverse', N'CC', N'Credit Card', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 308, N'WUE+', N'WI', N'Withdrawal', N'RV', N'Reverse', N'EF', N'EFT', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 309, N'WUE-', N'WI', N'Withdrawal', N'RV', N'Reverse', N'EF', N'EFT', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 310, N'WUL+', N'WI', N'Withdrawal', N'RV', N'Reverse', N'PP', N'PayPal', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 311, N'WUL-', N'WI', N'Withdrawal', N'RV', N'Reverse', N'PP', N'PayPal', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 312, N'WUN+', N'WI', N'Withdrawal', N'RV', N'Reverse', N'NE', N'Neteller', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 313, N'WUN-', N'WI', N'Withdrawal', N'RV', N'Reverse', N'NE', N'Neteller', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 314, N'WUQ+', N'WI', N'Withdrawal', N'RV', N'Reverse', N'CH', N'Cheque', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 315, N'WUQ-', N'WI', N'Withdrawal', N'RV', N'Reverse', N'CH', N'Cheque', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 316, N'WUS+', N'WI', N'Withdrawal', N'RV', N'Reverse', N'MB', N'Moneybookers', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 317, N'WUS-', N'WI', N'Withdrawal', N'RV', N'Reverse', N'MB', N'Moneybookers', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 318, N'WUU+', N'WI', N'Withdrawal', N'RV', N'Reverse', N'UK', N'Unknown', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 319, N'WUU-', N'WI', N'Withdrawal', N'RV', N'Reverse', N'UK', N'Unknown', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 320, N'KCS+', N'BB', N'Betback', N'CN', N'Cancel', N'SC', N'Scratched', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 321, N'KCS-', N'BB', N'Betback', N'CN', N'Cancel', N'SC', N'Scratched', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 322, N'KLA+', N'BB', N'Betback', N'LO', N'Lose', N'AD', N'Adjustment', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 323, N'KLA-', N'BB', N'Betback', N'LO', N'Lose', N'AD', N'Adjustment', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 324, N'KUP+', N'BB', N'Betback', N'PY', N'Payment', N'UK', N'Unknown', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 325, N'KUP-', N'BB', N'Betback', N'PY', N'Payment', N'UK', N'Unknown', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 326, N'KUA+', N'BB', N'Betback', N'UN', N'Unsettle', N'AD', N'Adjustment', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 327, N'KUA-', N'BB', N'Betback', N'UN', N'Unsettle', N'AD', N'Adjustment', N'DR', -1, '1900-01-01' UNION ALL
		SELECT 328, N'KWA+', N'BB', N'Betback', N'WN', N'Win', N'AD', N'Adjustment', N'CR', 1, '1900-01-01' UNION ALL
		SELECT 329, N'KWA-', N'BB', N'Betback', N'WN', N'Win', N'AD', N'Adjustment', N'DR', -1, '1900-01-01' UNION ALL

		SELECT 330, N'DVR-', N'DP', N'Deposit', N'RJ', N'Reject', N'CD', N'CashCard', N'DR', -1, '2016-03-16' UNION ALL
		SELECT 331, N'DRR+', N'DP', N'Deposit', N'RE', N'Request', N'CD', N'CashCard', N'CR', 1, '2016-03-16' UNION ALL
		SELECT 332, N'DCR+', N'DP', N'Deposit', N'CO', N'Complete', N'CD', N'CashCard', N'CR', 1, '2016-03-16' UNION ALL
		SELECT 333, N'DCR-', N'DP', N'Deposit', N'CO', N'Complete', N'CD', N'CashCard', N'DR', -1, '2016-03-16' UNION ALL
		SELECT 334, N'DVR+', N'DP', N'Deposit', N'RJ', N'Reject', N'CD', N'CashCard', N'CR', 1, '2016-03-16' UNION ALL
		SELECT 335, N'WCR-', N'WI', N'Withdrawal', N'CO', N'Complete', N'CD', N'CashCard', N'DR', -1, '2016-03-16' UNION ALL
		SELECT 336, N'WRR+', N'WI', N'Withdrawal', N'RE', N'Request', N'CD', N'CashCard', N'CR', 1, '2016-03-16' UNION ALL
		SELECT 337, N'WRR-', N'WI', N'Withdrawal', N'RE', N'Request', N'CD', N'CashCard', N'DR', -1, '2016-03-16' UNION ALL
		SELECT 338, N'WVR+', N'WI', N'Withdrawal', N'RJ', N'Reject', N'CD', N'CashCard', N'CR', 1, '2016-03-16' UNION ALL
		SELECT 339, N'WVR-', N'WI', N'Withdrawal', N'RJ', N'Reject', N'CD', N'CashCard', N'DR', -1, '2016-03-16' UNION ALL
		SELECT 340, N'DVW-', N'DP', N'Deposit', N'RJ', N'Reject', N'CW', N'CashCard - EachWay', N'DR', -1, '2016-03-16' UNION ALL
		SELECT 341, N'DRW+', N'DP', N'Deposit', N'RE', N'Request', N'CW', N'CashCard - EachWay', N'CR', 1, '2016-03-16' UNION ALL
		SELECT 342, N'DCW+', N'DP', N'Deposit', N'CO', N'Complete', N'CW', N'CashCard - EachWay', N'CR', 1, '2016-03-16' UNION ALL
		SELECT 343, N'DCW-', N'DP', N'Deposit', N'CO', N'Complete', N'CW', N'CashCard - EachWay', N'DR', -1, '2016-03-16' UNION ALL
		SELECT 344, N'DVW+', N'DP', N'Deposit', N'RJ', N'Reject', N'CW', N'CashCard - EachWay', N'CR', 1, '2016-03-16' UNION ALL

		SELECT 345, N'BOA+', N'BT', N'Bet', N'CO', N'Cashout', N'AD', N'Adjustment', N'CR', 1, '2016-05-24' UNION ALL
		SELECT 346, N'BOA-', N'BT', N'Bet', N'CO', N'Cashout', N'AD', N'Adjustment', N'DR', -1, '2016-05-24' UNION ALL
		SELECT 347, N'BOI+', N'BT', N'Bet', N'CO', N'Cashout', N'SE', N'Settlement', N'CR', 1, '2016-05-24' UNION ALL
		SELECT 348, N'BOI-', N'BT', N'Bet', N'CO', N'Cashout', N'SE', N'Settlement', N'DR', -1, '2016-05-24' UNION ALL

		SELECT 349, N'TCM+', N'TR', N'Transfer', N'CO', N'Complete', N'MI', N'Migration', N'CR', 1, '2016-06-07' UNION ALL
		SELECT 350, N'TCM-', N'TR', N'Transfer', N'CO', N'Complete', N'MI', N'Migration', N'DR', -1, '2016-06-07' UNION ALL
		SELECT 351, N'TCW+', N'TR', N'Transfer', N'CO', N'Complete', N'WO', N'Write Off', N'CR', 1, '2016-06-07' UNION ALL
		SELECT 352, N'TCW-', N'TR', N'Transfer', N'CO', N'Complete', N'WO', N'Write Off', N'DR', -1, '2016-06-07' UNION ALL

		SELECT 353, N'FPD+', N'FE', N'Fee', N'PY', N'Payment', N'DD', N'Double Down', N'CR', 1, '2016-08-30' UNION ALL
		SELECT 354, N'FPD-', N'FE', N'Fee', N'PY', N'Payment', N'DD', N'Double Down', N'DR', -1, '2016-08-30' UNION ALL
		SELECT 355, N'FRD+', N'FE', N'Fee', N'RF', N'Refund', N'DD', N'Double Down', N'CR', 1, '2016-08-30' UNION ALL
		SELECT 356, N'FRD-', N'FE', N'Fee', N'RF', N'Refund', N'DD', N'Double Down', N'DR', -1, '2016-08-30' UNION ALL

		SELECT 357, N'PAA+', N'BO', N'Bonus', N'AW', N'Award', N'BP', N'Accrual', N'CR', 1, '2017-03-01' UNION ALL
		SELECT 358, N'PAA-', N'BO', N'Bonus', N'AW', N'Award', N'BP', N'Accrual', N'DR', -1, '2017-03-01' UNION ALL
		SELECT 359, N'PRB+', N'BO', N'Bonus', N'RE', N'Redemption', N'BO', N'Bonus', N'CR', 1, '2017-03-01' UNION ALL
		SELECT 360, N'PRB-', N'BO', N'Bonus', N'RE', N'Redemption', N'BO', N'Bonus', N'DR', -1, '2017-03-01' UNION ALL
		SELECT 361, N'PRP+', N'BO', N'Bonus', N'RE', N'Redemption', N'PA', N'Partner', N'CR', 1, '2017-03-01' UNION ALL
		SELECT 362, N'PRP-', N'BO', N'Bonus', N'RE', N'Redemption', N'PA', N'Partner', N'DR', -1, '2017-03-01'
	)
	MERGE Dimension.TransactionDetail m
	USING u ON u.TransactionDetailKey = m.TransactionDetailKey
	WHEN MATCHED AND (m.TransactionDetailId <> u.TransactionDetailId OR m.TransactionTypeID <> u.TransactionTypeID OR m.TransactionType <> u.TransactionType OR m.TransactionStatusID <> u.TransactionStatusID OR m.TransactionStatus <> u.TransactionStatus OR m.TransactionMethodID <> u.TransactionMethodID OR m.TransactionMethod <> u.TransactionMethod OR m.TransactionDirection <> u.TransactionDirection OR m.TransactionSign <> u.TransactionSign) THEN 
		UPDATE SET TransactionDetailId = u.TransactionDetailId, TransactionTypeID = u.TransactionTypeID, TransactionType = u.TransactionType, TransactionStatusID = u.TransactionStatusID, TransactionStatus = u.TransactionStatus, TransactionMethodID = u.TransactionMethodID, TransactionMethod = u.TransactionMethod, TransactionDirection = u.TransactionDirection, TransactionSign = u.TransactionSign, FromDate = u.FromDate, ModifiedDate = GETDATE(), ModifiedBatchKey = 0, ModifiedBy = 'sp_InitialPopWallet' 
	WHEN NOT MATCHED THEN 
		INSERT (TransactionDetailKey, TransactionDetailId, TransactionTypeID, TransactionType, TransactionStatusID, TransactionStatus, TransactionMethodID, TransactionMethod, TransactionDirection, TransactionSign, FromDate, ToDate, FirstDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy)
		VALUES (u.TransactionDetailKey, u.TransactionDetailId, u.TransactionTypeID, u.TransactionType, u.TransactionStatusID, u.TransactionStatus, u.TransactionMethodID, u.TransactionMethod, u.TransactionDirection, u.TransactionSign, u.FromDate, '9999-12-31', u.FromDate, GETDATE(), 0, 'sp_InitialPopWallet', GETDATE(), 0, 'sp_InitialPopWallet');

	SET IDENTITY_INSERT Dimension.TransactionDetail OFF;
	COMMIT TRANSACTION TransactionDetail

