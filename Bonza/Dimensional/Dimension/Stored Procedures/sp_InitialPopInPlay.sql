﻿CREATE PROCEDURE [Dimension].sp_InitialPopInPlay
AS
	SET XACT_ABORT ON;

	SET IDENTITY_INSERT Dimension.InPlay ON 

	;WITH x (	InPlayKey,	CashoutType,	InPlay,		ClickToCall,	CashedOut,	StatusAtCashout,	IsDoubleDown,	IsDoubledDown,	DoubleDown,		IsChaseTheAce,	FeatureFlags	) AS
	(	SELECT	-1,			-1,				'Unknown',	'Unknown',		'UNK',		'Unknown',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	0,			0,				'N/A',		'N/A',			'No',		'N/A',				0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	1,			0,				'N',		'N',			'No',		'N/A',				0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	2,			0,				'Y',		'N',			'No',		'N/A',				0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	3,			0,				'N',		'Y',			'No',		'N/A',				0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	4,			0,				'Y',		'Y',			'No',		'N/A',				0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	5,			1,				'N/A',		'N/A',			'Yes',		'PreGame',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	6,			1,				'N',		'N',			'Yes',		'PreGame',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	7,			1,				'Y',		'N',			'Yes',		'PreGame',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	8,			1,				'N',		'Y',			'Yes',		'PreGame',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	9,			1,				'Y',		'Y',			'Yes',		'PreGame',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	10,			2,				'N/A',		'N/A',			'Yes',		'InPlay',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	11,			2,				'N',		'N',			'Yes',		'InPlay',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	12,			2,				'Y',		'N',			'Yes',		'InPlay',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	13,			2,				'N',		'Y',			'Yes',		'InPlay',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	14,			2,				'Y',		'Y',			'Yes',		'InPlay',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	15,			3,				'N/A',		'N/A',			'Yes',		'InPlay',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	16,			3,				'N',		'N',			'Yes',		'InPlay',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	17,			3,				'Y',		'N',			'Yes',		'InPlay',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	18,			3,				'N',		'Y',			'Yes',		'InPlay',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	19,			3,				'Y',		'Y',			'Yes',		'InPlay',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	20,			4,				'N/A',		'N/A',			'Yes',		'Settled',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	21,			4,				'N',		'N',			'Yes',		'Settled',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	22,			4,				'Y',		'N',			'Yes',		'Settled',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	23,			4,				'N',		'Y',			'Yes',		'Settled',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	24,			4,				'Y',		'Y',			'Yes',		'Settled',			0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	25,			5,				'N/A',		'N/A',			'Yes',		'Multi Max',		0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	26,			5,				'N',		'N',			'Yes',		'Multi Max',		0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	27,			5,				'Y',		'N',			'Yes',		'Multi Max',		0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	28,			5,				'N',		'Y',			'Yes',		'Multi Max',		0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	29,			5,				'Y',		'Y',			'Yes',		'Multi Max',		0,				0,				'N/A',			0,				0				UNION ALL
		SELECT	30,			0,				'N/A',		'N/A',			'No',		'N/A',				1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	31,			0,				'N',		'N',			'No',		'N/A',				1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	32,			0,				'Y',		'N',			'No',		'N/A',				1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	33,			0,				'N',		'Y',			'No',		'N/A',				1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	34,			0,				'Y',		'Y',			'No',		'N/A',				1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	35,			1,				'N/A',		'N/A',			'Yes',		'PreGame',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	36,			1,				'N',		'N',			'Yes',		'PreGame',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	37,			1,				'Y',		'N',			'Yes',		'PreGame',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	38,			1,				'N',		'Y',			'Yes',		'PreGame',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	39,			1,				'Y',		'Y',			'Yes',		'PreGame',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	40,			2,				'N/A',		'N/A',			'Yes',		'InPlay',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	41,			2,				'N',		'N',			'Yes',		'InPlay',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	42,			2,				'Y',		'N',			'Yes',		'InPlay',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	43,			2,				'N',		'Y',			'Yes',		'InPlay',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	44,			2,				'Y',		'Y',			'Yes',		'InPlay',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	45,			3,				'N/A',		'N/A',			'Yes',		'InPlay',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	46,			3,				'N',		'N',			'Yes',		'InPlay',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	47,			3,				'Y',		'N',			'Yes',		'InPlay',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	48,			3,				'N',		'Y',			'Yes',		'InPlay',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	49,			3,				'Y',		'Y',			'Yes',		'InPlay',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	50,			4,				'N/A',		'N/A',			'Yes',		'Settled',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	51,			4,				'N',		'N',			'Yes',		'Settled',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	52,			4,				'Y',		'N',			'Yes',		'Settled',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	53,			4,				'N',		'Y',			'Yes',		'Settled',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	54,			4,				'Y',		'Y',			'Yes',		'Settled',			1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	55,			5,				'N/A',		'N/A',			'Yes',		'Multi Max',		1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	56,			5,				'N',		'N',			'Yes',		'Multi Max',		1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	57,			5,				'Y',		'N',			'Yes',		'Multi Max',		1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	58,			5,				'N',		'Y',			'Yes',		'Multi Max',		1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	59,			5,				'Y',		'Y',			'Yes',		'Multi Max',		1,				0,				'Initial',		0,				0				UNION ALL
		SELECT	60,			0,				'N/A',		'N/A',			'No',		'N/A',				0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	61,			0,				'N',		'N',			'No',		'N/A',				0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	62,			0,				'Y',		'N',			'No',		'N/A',				0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	63,			0,				'N',		'Y',			'No',		'N/A',				0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	64,			0,				'Y',		'Y',			'No',		'N/A',				0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	65,			1,				'N/A',		'N/A',			'Yes',		'PreGame',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	66,			1,				'N',		'N',			'Yes',		'PreGame',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	67,			1,				'Y',		'N',			'Yes',		'PreGame',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	68,			1,				'N',		'Y',			'Yes',		'PreGame',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	69,			1,				'Y',		'Y',			'Yes',		'PreGame',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	70,			2,				'N/A',		'N/A',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	71,			2,				'N',		'N',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	72,			2,				'Y',		'N',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	73,			2,				'N',		'Y',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	74,			2,				'Y',		'Y',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	75,			3,				'N/A',		'N/A',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	76,			3,				'N',		'N',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	77,			3,				'Y',		'N',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	78,			3,				'N',		'Y',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	79,			3,				'Y',		'Y',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	80,			4,				'N/A',		'N/A',			'Yes',		'Settled',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	81,			4,				'N',		'N',			'Yes',		'Settled',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	82,			4,				'Y',		'N',			'Yes',		'Settled',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	83,			4,				'N',		'Y',			'Yes',		'Settled',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	84,			4,				'Y',		'Y',			'Yes',		'Settled',			0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	85,			5,				'N/A',		'N/A',			'Yes',		'Multi Max',		0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	86,			5,				'N',		'N',			'Yes',		'Multi Max',		0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	87,			5,				'Y',		'N',			'Yes',		'Multi Max',		0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	88,			5,				'N',		'Y',			'Yes',		'Multi Max',		0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	89,			5,				'Y',		'Y',			'Yes',		'Multi Max',		0,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	90,			0,				'N/A',		'N/A',			'No',		'N/A',				1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	91,			0,				'N',		'N',			'No',		'N/A',				1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	92,			0,				'Y',		'N',			'No',		'N/A',				1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	93,			0,				'N',		'Y',			'No',		'N/A',				1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	94,			0,				'Y',		'Y',			'No',		'N/A',				1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	95,			1,				'N/A',		'N/A',			'Yes',		'PreGame',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	96,			1,				'N',		'N',			'Yes',		'PreGame',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	97,			1,				'Y',		'N',			'Yes',		'PreGame',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	98,			1,				'N',		'Y',			'Yes',		'PreGame',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	99,			1,				'Y',		'Y',			'Yes',		'PreGame',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	100,		2,				'N/A',		'N/A',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	101,		2,				'N',		'N',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	102,		2,				'Y',		'N',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	103,		2,				'N',		'Y',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	104,		2,				'Y',		'Y',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	105,		3,				'N/A',		'N/A',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	106,		3,				'N',		'N',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	107,		3,				'Y',		'N',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	108,		3,				'N',		'Y',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	109,		3,				'Y',		'Y',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	110,		4,				'N/A',		'N/A',			'Yes',		'Settled',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	111,		4,				'N',		'N',			'Yes',		'Settled',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	112,		4,				'Y',		'N',			'Yes',		'Settled',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	113,		4,				'N',		'Y',			'Yes',		'Settled',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	114,		4,				'Y',		'Y',			'Yes',		'Settled',			1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	115,		5,				'N/A',		'N/A',			'Yes',		'Multi Max',		1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	116,		5,				'N',		'N',			'Yes',		'Multi Max',		1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	117,		5,				'Y',		'N',			'Yes',		'Multi Max',		1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	118,		5,				'N',		'Y',			'Yes',		'Multi Max',		1,				1,				'DoubledDown',	0,				0				UNION ALL
		SELECT	119,		5,				'Y',		'Y',			'Yes',		'Multi Max',		1,				1,				'DoubledDown',	0,				0				UNION ALL		
		SELECT	120,		0,				'N/A',		'N/A',			'No',		'N/A',				0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	121,		0,				'N',		'N',			'No',		'N/A',				0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	122,		0,				'Y',		'N',			'No',		'N/A',				0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	123,		0,				'N',		'Y',			'No',		'N/A',				0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	124,		0,				'Y',		'Y',			'No',		'N/A',				0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	125,		1,				'N/A',		'N/A',			'Yes',		'PreGame',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	126,		1,				'N',		'N',			'Yes',		'PreGame',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	127,		1,				'Y',		'N',			'Yes',		'PreGame',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	128,		1,				'N',		'Y',			'Yes',		'PreGame',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	129,		1,				'Y',		'Y',			'Yes',		'PreGame',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	130,		2,				'N/A',		'N/A',			'Yes',		'InPlay',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	131,		2,				'N',		'N',			'Yes',		'InPlay',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	132,		2,				'Y',		'N',			'Yes',		'InPlay',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	133,		2,				'N',		'Y',			'Yes',		'InPlay',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	134,		2,				'Y',		'Y',			'Yes',		'InPlay',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	135,		3,				'N/A',		'N/A',			'Yes',		'InPlay',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	136,		3,				'N',		'N',			'Yes',		'InPlay',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	137,		3,				'Y',		'N',			'Yes',		'InPlay',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	138,		3,				'N',		'Y',			'Yes',		'InPlay',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	139,		3,				'Y',		'Y',			'Yes',		'InPlay',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	140,		4,				'N/A',		'N/A',			'Yes',		'Settled',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	141,		4,				'N',		'N',			'Yes',		'Settled',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	142,		4,				'Y',		'N',			'Yes',		'Settled',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	143,		4,				'N',		'Y',			'Yes',		'Settled',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	144,		4,				'Y',		'Y',			'Yes',		'Settled',			0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	145,		5,				'N/A',		'N/A',			'Yes',		'Multi Max',		0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	146,		5,				'N',		'N',			'Yes',		'Multi Max',		0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	147,		5,				'Y',		'N',			'Yes',		'Multi Max',		0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	148,		5,				'N',		'Y',			'Yes',		'Multi Max',		0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	149,		5,				'Y',		'Y',			'Yes',		'Multi Max',		0,				0,				'N/A',			1,				0				UNION ALL
		SELECT	150,		0,				'N/A',		'N/A',			'No',		'N/A',				1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	151,		0,				'N',		'N',			'No',		'N/A',				1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	152,		0,				'Y',		'N',			'No',		'N/A',				1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	153,		0,				'N',		'Y',			'No',		'N/A',				1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	154,		0,				'Y',		'Y',			'No',		'N/A',				1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	155,		1,				'N/A',		'N/A',			'Yes',		'PreGame',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	156,		1,				'N',		'N',			'Yes',		'PreGame',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	157,		1,				'Y',		'N',			'Yes',		'PreGame',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	158,		1,				'N',		'Y',			'Yes',		'PreGame',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	159,		1,				'Y',		'Y',			'Yes',		'PreGame',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	160,		2,				'N/A',		'N/A',			'Yes',		'InPlay',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	161,		2,				'N',		'N',			'Yes',		'InPlay',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	162,		2,				'Y',		'N',			'Yes',		'InPlay',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	163,		2,				'N',		'Y',			'Yes',		'InPlay',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	164,		2,				'Y',		'Y',			'Yes',		'InPlay',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	165,		3,				'N/A',		'N/A',			'Yes',		'InPlay',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	166,		3,				'N',		'N',			'Yes',		'InPlay',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	167,		3,				'Y',		'N',			'Yes',		'InPlay',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	168,		3,				'N',		'Y',			'Yes',		'InPlay',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	169,		3,				'Y',		'Y',			'Yes',		'InPlay',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	170,		4,				'N/A',		'N/A',			'Yes',		'Settled',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	171,		4,				'N',		'N',			'Yes',		'Settled',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	172,		4,				'Y',		'N',			'Yes',		'Settled',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	173,		4,				'N',		'Y',			'Yes',		'Settled',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	174,		4,				'Y',		'Y',			'Yes',		'Settled',			1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	175,		5,				'N/A',		'N/A',			'Yes',		'Multi Max',		1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	176,		5,				'N',		'N',			'Yes',		'Multi Max',		1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	177,		5,				'Y',		'N',			'Yes',		'Multi Max',		1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	178,		5,				'N',		'Y',			'Yes',		'Multi Max',		1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	179,		5,				'Y',		'Y',			'Yes',		'Multi Max',		1,				0,				'Initial',		1,				0				UNION ALL
		SELECT	180,		0,				'N/A',		'N/A',			'No',		'N/A',				0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	181,		0,				'N',		'N',			'No',		'N/A',				0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	182,		0,				'Y',		'N',			'No',		'N/A',				0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	183,		0,				'N',		'Y',			'No',		'N/A',				0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	184,		0,				'Y',		'Y',			'No',		'N/A',				0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	185,		1,				'N/A',		'N/A',			'Yes',		'PreGame',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	186,		1,				'N',		'N',			'Yes',		'PreGame',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	187,		1,				'Y',		'N',			'Yes',		'PreGame',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	188,		1,				'N',		'Y',			'Yes',		'PreGame',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	189,		1,				'Y',		'Y',			'Yes',		'PreGame',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	190,		2,				'N/A',		'N/A',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	191,		2,				'N',		'N',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	192,		2,				'Y',		'N',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	193,		2,				'N',		'Y',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	194,		2,				'Y',		'Y',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	195,		3,				'N/A',		'N/A',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	196,		3,				'N',		'N',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	197,		3,				'Y',		'N',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	198,		3,				'N',		'Y',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	199,		3,				'Y',		'Y',			'Yes',		'InPlay',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	200,		4,				'N/A',		'N/A',			'Yes',		'Settled',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	201,		4,				'N',		'N',			'Yes',		'Settled',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	202,		4,				'Y',		'N',			'Yes',		'Settled',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	203,		4,				'N',		'Y',			'Yes',		'Settled',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	204,		4,				'Y',		'Y',			'Yes',		'Settled',			0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	205,		5,				'N/A',		'N/A',			'Yes',		'Multi Max',		0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	206,		5,				'N',		'N',			'Yes',		'Multi Max',		0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	207,		5,				'Y',		'N',			'Yes',		'Multi Max',		0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	208,		5,				'N',		'Y',			'Yes',		'Multi Max',		0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	209,		5,				'Y',		'Y',			'Yes',		'Multi Max',		0,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	210,		0,				'N/A',		'N/A',			'No',		'N/A',				1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	211,		0,				'N',		'N',			'No',		'N/A',				1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	212,		0,				'Y',		'N',			'No',		'N/A',				1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	213,		0,				'N',		'Y',			'No',		'N/A',				1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	214,		0,				'Y',		'Y',			'No',		'N/A',				1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	215,		1,				'N/A',		'N/A',			'Yes',		'PreGame',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	216,		1,				'N',		'N',			'Yes',		'PreGame',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	217,		1,				'Y',		'N',			'Yes',		'PreGame',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	218,		1,				'N',		'Y',			'Yes',		'PreGame',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	219,		1,				'Y',		'Y',			'Yes',		'PreGame',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	220,		2,				'N/A',		'N/A',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	221,		2,				'N',		'N',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	222,		2,				'Y',		'N',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	223,		2,				'N',		'Y',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	224,		2,				'Y',		'Y',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	225,		3,				'N/A',		'N/A',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	226,		3,				'N',		'N',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	227,		3,				'Y',		'N',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	228,		3,				'N',		'Y',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	229,		3,				'Y',		'Y',			'Yes',		'InPlay',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	230,		4,				'N/A',		'N/A',			'Yes',		'Settled',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	231,		4,				'N',		'N',			'Yes',		'Settled',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	232,		4,				'Y',		'N',			'Yes',		'Settled',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	233,		4,				'N',		'Y',			'Yes',		'Settled',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	234,		4,				'Y',		'Y',			'Yes',		'Settled',			1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	235,		5,				'N/A',		'N/A',			'Yes',		'Multi Max',		1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	236,		5,				'N',		'N',			'Yes',		'Multi Max',		1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	237,		5,				'Y',		'N',			'Yes',		'Multi Max',		1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	238,		5,				'N',		'Y',			'Yes',		'Multi Max',		1,				1,				'DoubledDown',	1,				0				UNION ALL
		SELECT	239,		5,				'Y',		'Y',			'Yes',		'Multi Max',		1,				1,				'DoubledDown',	1,				0
		EXCEPT
		SELECT	InPlayKey,	CashoutType,	InPlay,		ClickToCall,	CashedOut,	StatusAtCashout,	IsDoubleDown,	IsDoubledDown,	DoubleDown,		IsChaseTheAce,	FeatureFlags	FROM Dimension.InPlay
	)
	MERGE Dimension.InPlay m
	USING x u ON u.InPlayKey = m.InPlayKey
	WHEN NOT MATCHED THEN 
		INSERT (InPlayKey, CashoutType, InPlay, ClickToCall, CashedOut, StatusAtCashout, IsDoubleDown, IsDoubledDown, DoubleDown, IsChaseTheAce, FeatureFlags, FirstDate, FromDate, ToDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy)
		VALUES (u.InPlayKey, u.CashoutType, u.InPlay, u.ClickToCall, u.CashedOut, u.StatusAtCashout, u.IsDoubleDown, u.IsDoubledDown, u.DoubleDown, u.IsChaseTheAce, u.FeatureFlags, '1900-01-01-01', '1900-01-01-01', '9999-12-31', GETDATE(), 0, 'sp_InitialPopInPlay', GETDATE(), 0, 'sp_InitialPopInPlay')
	WHEN MATCHED THEN 
		UPDATE SET	CashoutType = u.CashoutType, 
					InPlay = u.InPlay, 
					ClickToCall = u.ClickToCall, 
					CashedOut = u.CashedOut, 
					StatusAtCashout = u.StatusAtCashout, 
					IsDoubleDown = u.IsDoubleDown, 
					IsDoubledDown = u.IsDoubledDown, 
					DoubleDown = u.DoubleDown, 
					IsChaseTheAce = u.IsChaseTheAce, 
					FeatureFlags =u.FeatureFlags,
					FirstDate = '1900-01-01',	
					FromDate = '1900-01-01',	
					ToDate = '9999-12-31',	
					ModifiedDate = GETDATE(),
					ModifiedBatchKey = 0,
					ModifiedBy = 'sp_InitialPopInPlay';

	SET IDENTITY_INSERT Dimension.Class OFF;
