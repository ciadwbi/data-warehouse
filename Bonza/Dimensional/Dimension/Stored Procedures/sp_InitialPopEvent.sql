Create Procedure [Dimension].sp_InitialPopEvent

as

IF not exists (Select	[EventKey] from [Dimension].[Event])
Begin
Begin Transaction
SET IDENTITY_INSERT [Dimension].[Event] ON 

INSERT into [Dimension].[Event] 
([EventKey], [Source], [SourceEventId], [EventId], [SourceEvent], [SourceEventType], [ParentEventId], [ParentEvent], [GrandparentEventId], 
[GrandparentEvent], [GreatGrandparentEventId], [GreatGrandparentEvent], [SourceEventDate], [SourceVenueId], [VenueId], [SourceVenue], [SourceVenueType], 
[SourceVenueStateCode],[SourceVenueState],[SourceVenueCountryCode],[SourceVenueCountry],[SourceVenueEventType], [SourceVenueEventTypeName], [ClassID], [Event], 
[Grade], [Distance], [SourcePrizePool], [PrizePool], [SourceTrackCondition], [SourceEventWeather], [TrackCondition], [EventWeather], [ScheduledDateTime], [ScheduledDayKey], [ScheduledVenueDateTime], [ResultedDateTime], 
[ResultedDayKey], [ResultedVenueDateTime], [Round], [RoundSequence], [Competition], [CompetitionSeason], [CompetitionStartDate], [CompetitionEndDate], 
[ClassKey], [SourceRaceNumber], [Venue], [VenueType], [VenueStateCode], [VenueState], [VenueCountryCode], [VenueCountry],[SourceRaceGroup], [SourceRaceClass],[SourceHybridPricingTemplate],[RaceGroup],[RaceClass],[HybridPricingTemplate],
[SourceWeightType], [WeightType],[SourceDistance], [SourceEventAbandoned], [EventAbandoned], [EventAbandonedDateTime], [EventAbandonedDayKey], [SourceLiveStreamPerformId], [LiveVideoStreamed], [LiveScoreBoard], [SubSportType], [SourceSubSportType], [ColumnLock], [FromDate], [FirstDate], 
[CreatedDate], [CreatedBatchKey], [CreatedBy], 
[ModifiedDate], [ModifiedBatchKey], [ModifiedBy]) 
select 
-1  [EventKey], N'UNK' [Source], -1 [SourceEventId], -1 [EventId], N'UNK' [SourceEvent], N'UNK' [SourceEventType], -1 [ParentEventId], N'UNK' [ParentEvent], -1 [GrandparentEventId], 
N'UNK' [GrandparentEvent], -1 [GreatGrandparentEventId], N'UNK' [GreatGrandparentEvent], CAST(N'1900-01-01 00:00:00.000' AS DateTime) [SourceEventDate], -1 [SourceVenueId], -1 [VenueId], N'UNK' [SourceVenue], N'UNK' [SourceVenueType], 
N'UNK' [SourceVenueStateCode], N'UNK' [SourceVenueState], N'XX' [SourceVenueCountryCode], N'UNK' [SourceVenueCountry], -1 [SourceVenueEventType], N'UNK' [SourceVenueEventTypeName], -1 [ClassID], N'UNK' [Event], 
N'UNK' [Grade], N'UNK' [Distance], -1 [SourcePrizePool], -1 [PrizePool], N'UNK' [SourceTrackCondition], N'UNK' [SourceEventWeather], N'UNK' [TrackCondition], N'UNK' [EventWeather], CAST(N'1900-01-01 00:00:00.000' AS DateTime) [ScheduledDateTime], -1 [ScheduledDayKey], CAST(N'1900-01-01 00:00:00.000' AS DateTime) [ScheduledVenueDateTime], CAST(N'1900-01-01 00:00:00.000' AS DateTime) [ResultedDateTime],
-1 [ResultedDayKey], CAST(N'1900-01-01 00:00:00.000' AS DateTime) [ResultedVenueDateTime], N'UNK' [Round], 100 [RoundSequence], N'UNK' [Competition], N'UNK' [CompetitionSeason], CAST(N'1900-01-01' AS Date) [CompetitionStartDate], CAST(N'1900-01-01' AS Date) [CompetitionEndDate], 
-1 [ClassKey], -1 [SourceRaceNumber], N'UNK' [Venue], N'UNK' [VenueType], N'UNK' [VenueStateCode], N'UNK' [VenueState], N'XX' [VenueCountryCode], N'UNK' [VenueCountry], -1 [SourceRaceGroup],N'UNK' [SourceRaceClass],N'UNK' [SourceHybridPricingTemplate], -1 [RaceGroup],N'UNK' [RaceClass],N'UNK' [HybridPricingTemplate],
N'UNK' [SourceWeightType], N'UNK' [WeightType],-1 [SourceDistance], -1 [SourceEventAbandoned], N'UNK' [EventAbandoned], CAST(N'1900-01-01 00:00:00.000' AS DateTime) [EventAbandonedDateTime], -1 [EventAbandonedDayKey], N'UNK' [SourceLiveStreamPerformId], N'UNK' [LiveVideoStreamed], N'UNK' [LiveScoreBoard], N'UNK' [SubSportType], N'UNK' [SourceSubSportType],-1 [ColumnLock], CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2) [FromDate], CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2) [FirstDate], 
CAST(N'2014-11-07 18:25:30.7430000' AS DateTime2) [CreatedDate], 0 [CreatedBatchKey], N'sp_InitialPopEvent' [CreatedBy], 
CAST(N'2014-11-07 18:25:30.7430000' AS DateTime2) [ModifiedDate], 0 [ModifiedBatchKey], N'sp_InitialPopEvent' [ModifiedBy]
union all
select
 0 [EventKey], N'N/A' [Source], 0 [SourceEventId], 0 [EventId], N'N/A' [SourceEvent], N'N/A' [SourceEventType], 0 [ParentEventId], N'N/A' [ParentEvent], 0 [GrandparentEventId],
  N'N/A' [GrandparentEvent], 0 [GreatGrandparentEventId], N'N/A' [GreatGrandparentEvent], CAST(N'1900-01-01 00:00:00.000' AS DateTime) [SourceEventDate], 0 [SourceVenueId], 0 [VenueId], N'N/A' [SourceVenue], N'N/A' [SourceVenueType], 
  N'N/A' [SourceVenueStateCode], N'N/A' [SourceVenueState], N'N/A' [SourceVenueCountryCode], N'N/A' [SourceVenueCountry], 0 [SourceVenueEventType], N'N/A' [SourceVenueEventTypeName], 0 [ClassID], N'N/A' [Event],
  N'N/A' [Grade], N'N/A' [Distance], 0 [SourcePrizePool], 0 [PrizePool], N'N/A' [SourceTrackCondition], N'N/A' [SourceEventWeather], N'N/A' [TrackCondition], N'N/A' [EventWeather], CAST(N'1900-01-01 00:00:00.000' AS DateTime) [ScheduledDateTime], 0 [ScheduledDayKey], CAST(N'1900-01-01 00:00:00.000' AS DateTime) [ScheduledVenueDateTime], CAST(N'1900-01-01 00:00:00.000' AS DateTime)[ResultedDateTime],
  0 [ResultedDayKey], CAST(N'1900-01-01 00:00:00.000' AS DateTime) [ResultedVenueDateTime], N'N/A' [Round], 99 [RoundSequence], N'N/A' [Competition], N'N/A' [CompetitionSeason], CAST(N'1900-01-01' AS Date) [CompetitionStartDate], CAST(N'1900-01-01' AS Date) [CompetitionEndDate], 
  0 [ClassKey], 0 [SourceRaceNumber], N'N/A' [Venue], N'N/A' [VenueType], N'N/A' [VenueStateCode], N'N/A' [VenueState], N'N/A' [VenueCountryCode], N'N/A' [VenueCountry], 0 [SourceRaceGroup],N'N/A' [SourceRaceClass],N'N/A' [SourceHybridPricingTemplate],  0 [RaceGroup],N'N/A' [RaceClass],N'N/A' [HybridPricingTemplate],
  N'N/A' [SourceWeightType], N'N/A' [WeightType],0 [SourceDistance], 0 [SourceEventAbandoned], N'N/A' [EventAbandoned], CAST(N'1900-01-01 00:00:00.000' AS DateTime) [EventAbandonedDateTime], 0 [EventAbandonedDayKey], N'N/A' [SourceLiveStreamPerformId], N'N/A' [LiveVideoStreamed], N'N/A' [LiveScoreBoard], N'N/A' [SubSportType], N'N/A' [SourceSubSportType], 0 [ColumnLock], CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2) [FromDate], CAST(N'1900-01-01 00:00:00.0000000' AS DateTime2) [FirstDate], 
  CAST(N'2014-11-07 18:24:08.5300000' AS DateTime2) [CreatedDate], 0 [CreatedBatchKey], N'sp_InitialPopEvent' [CreatedBy],  
  CAST(N'2014-11-07 18:24:08.5300000' AS DateTime2) [ModifiedDate], 0 [ModifiedBatchKey], N'sp_InitialPopEvent' [ModifiedBy]

SET IDENTITY_INSERT [Dimension].[Event] OFF
IF @@ERROR = 0
Begin
	Commit Transaction
End
Else
Begin
	Rollback Transaction
End
End