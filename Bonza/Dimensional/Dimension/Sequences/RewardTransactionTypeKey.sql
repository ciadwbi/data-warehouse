﻿CREATE SEQUENCE [Dimension].[RewardTransactionTypeKey]
		AS Smallint
		START WITH 1
		INCREMENT BY 1
		NO MAXVALUE
		NO CYCLE
		CACHE 1
