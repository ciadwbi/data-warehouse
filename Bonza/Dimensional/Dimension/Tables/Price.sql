﻿CREATE TABLE [Dimension].[Price] (
    
	[PriceKey]               INT           IDENTITY (1, 1) NOT NULL,
    [PriceText]              VARCHAR (10)  NOT NULL,
    [Price]                  FLOAT (53)    NOT NULL,
    [Probability]            FLOAT (53)    NOT NULL,
    
	[PriceBandKey]           INT           NOT NULL,
    [PriceBandName]          VARCHAR (32)  NOT NULL,
    [PriceBandLowerBound]    FLOAT (53)    NOT NULL,
    [PriceBandUpperBound]    FLOAT (53)    NOT NULL,
    
	/*[BetPriceKey]            INT           NOT NULL,
    [BetPriceText]           VARCHAR (10)  NOT NULL,
    [BetPrice]               FLOAT (53)    NOT NULL,
    [BetProbability]         FLOAT (53)    NOT NULL,
    [BetPriceBandKey]        INT           NOT NULL,
    [BetPriceBandName]       VARCHAR (32)  NOT NULL,
    [BetPriceBandLowerBound] FLOAT (53)    NOT NULL,
    [BetPriceBandUpperBound] FLOAT (53)    NOT NULL,*/ -- removed for now. Add back in later and populate as ELT
    
	[FromDate]               DATETIME2 (0) NOT NULL,
    [ToDate]                 DATETIME2 (0) NOT NULL,
    [FirstDate]              DATETIME2 (0) NOT NULL,
    [CreatedDate]            DATETIME2 (7) DEFAULT (getdate()) NOT NULL,
    [CreatedBatchKey]        INT           NOT NULL,
    [CreatedBy]              VARCHAR (32)  DEFAULT (suser_sname()) NOT NULL,
    [ModifiedDate]           DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey]       INT           NOT NULL,
    [ModifiedBy]             VARCHAR (32)  NOT NULL,
    CONSTRAINT [CI-PK-PriceKey] PRIMARY KEY NONCLUSTERED ([PriceKey] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user or porcess which last modified the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'ModifiedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The key of the ETL batch which last modified the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'ModifiedBatchKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date the record was last modified', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'ModifiedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user or process which first created the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'CreatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The key of the ETL batch which first created the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'CreatedBatchKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date the record was first created', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'CreatedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which the natural key of this record was first encountered.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'FirstDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which this version of data for the natural key was deprecated.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'ToDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which this version of data for the natural key was first encountered.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'FromDate';


GO
/*
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The highest price included in the price band', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'BetPriceBandUpperBound';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The lowest price included in the price band', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'BetPriceBandLowerBound';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name given to the band of prices.

e.g. Short or Long', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'BetPriceBandName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard internal data warehouse surrogate key', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'BetPriceBandKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The equivalent mathematical probability for the price

e.g. a $6 price has a potential return of $5 winnings plus the $1 stake, indicating a 1 in 5 or 0.20 chance of the winning outcome occurring', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'BetProbability';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The factor by which the stake is multiplied to derive the return. The Australian norm is to express the price as a dollar value, indicating the total return for a $1 bet.

e.g. price of $1.40 indicates a successful $1 bet will return $1.40, comprised of $0.40 winnings and the $1 stake 
likewise, a $50 bet at the same price would return $70, comprised of $20 winnings and $50 stake.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'BetPrice';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The preferred textual representation of the price, including punctuation and trailing zeroes etc.

e.g. $1.40', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'BetPriceText';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard internal data warehouse surrogate key', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'BetPriceKey';
*/


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The highest price included in the price band', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'PriceBandUpperBound';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The lowest price included in the price band', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'PriceBandLowerBound';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Name given to the band of prices.

e.g. Short or Long', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'PriceBandName';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard internal data warehouse surrogate key', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'PriceBandKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The equivalent mathematical probability for the price

e.g. a $6 price has a potential return of $5 winnings plus the $1 stake, indicating a 1 in 5 or 0.20 chance of the winning outcome occurring', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'Probability';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The factor by which the stake is multiplied to derive the return. The Australian norm is to express the price as a dollar value, indicating the total return for a $1 bet.

e.g. price of $1.40 indicates a successful $1 bet will return $1.40, comprised of $0.40 winnings and the $1 stake 
likewise, a $50 bet at the same price would return $70, comprised of $20 winnings and $50 stake.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'Price';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The preferred textual representation of the price, including punctuation and trailing zeroes etc.

e.g. $1.40', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'PriceText';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard internal data warehouse surrogate key', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price', @level2type = N'COLUMN', @level2name = N'PriceKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The price or odds offered.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Price';

