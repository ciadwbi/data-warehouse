﻿CREATE TABLE [Dimension].[BetType] (
	[BetTypeKey] [smallint] IDENTITY(1,1) NOT NULL,
	[BetGroupType] [varchar](20) NOT NULL,
	[BetType] [varchar](20) NOT NULL,
	[BetSubType] [varchar](20) NOT NULL,
	[GroupingType] [varchar](20) NOT NULL,
	[LegType] [varchar](20) NOT NULL,
	[FromDate] [datetime2](3) NOT NULL,
	[ToDate] [datetime2](3) NOT NULL,
	[FirstDate] [datetime2](3) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NOT NULL,
    CONSTRAINT [CI-PK-BetTypeKey] PRIMARY KEY CLUSTERED ([BetTypeKey] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[BetType]([BetGroupType] ASC, [BetType] ASC, [BetSubType] ASC, [GroupingType] ASC, [LegType] ASC)
    INCLUDE([BetTypeKey]);
GO

