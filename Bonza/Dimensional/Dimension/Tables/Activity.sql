﻿CREATE TABLE [Dimension].[Activity] (
    [ActivityKey]      INT           IDENTITY (1, 1) NOT NULL,
    [HasDeposited]     VARCHAR (3)   NOT NULL,
    [HasWithdrawn]     VARCHAR (3)   NOT NULL,
    [HasBet]           VARCHAR (3)   NOT NULL,
    [HasWon]           VARCHAR (3)   NOT NULL,
    [HasLost]          VARCHAR (3)   NOT NULL,
    [FromDate]         DATETIME2 (0) NOT NULL,
    [ToDate]           DATETIME2 (0) NOT NULL,
    [FirstDate]        DATETIME2 (0) NOT NULL,
    [CreatedDate]      DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]  INT           NOT NULL,
    [CreatedBy]        VARCHAR (32)  NOT NULL,
    [ModifiedDate]     DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey] INT           NOT NULL,
    [ModifiedBy]       VARCHAR (32)  NOT NULL,
    CONSTRAINT [Activity_PK] PRIMARY KEY NONCLUSTERED ([ActivityKey] ASC)
);


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Defines all possible permutations of activities an account may have been involved in to allow pattern matched subsets of accounts to be readily selected.

e.g. Has Bet (Y/N), Has Deposited (Y/N), Has Withdrawn (Y/N), Has Won (Y/N)', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Standard internal data warehouse surrogate key', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'ActivityKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Explicit YES and NO values to implement a Boolean data type in a user friendly form.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'HasDeposited';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Explicit YES and NO values to implement a Boolean data type in a user friendly form.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'HasWithdrawn';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Explicit YES and NO values to implement a Boolean data type in a user friendly form.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'HasBet';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'Explicit YES and NO values to implement a Boolean data type in a user friendly form.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'HasWon';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which this version of data for the natural key was first encountered.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'FromDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which this version of data for the natural key was deprecated.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'ToDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The source system date from which the natural key of this record was first encountered.', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'FirstDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date the record was first created', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'CreatedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The key of the ETL batch which first created the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'CreatedBatchKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user or process which first created the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'CreatedBy';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The date the record was last modified', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'ModifiedDate';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The key of the ETL batch which last modified the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'ModifiedBatchKey';


GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'The user or porcess which last modified the record', @level0type = N'SCHEMA', @level0name = N'Dimension', @level1type = N'TABLE', @level1name = N'Activity', @level2type = N'COLUMN', @level2name = N'ModifiedBy';

