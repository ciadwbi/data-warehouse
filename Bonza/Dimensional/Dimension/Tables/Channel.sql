﻿CREATE TABLE [Dimension].[Channel](
	[ChannelKey] [int] IDENTITY(1,1) NOT NULL,
	[ChannelId] [smallint] NULL,
	[SourceChannelDescription] [varchar](200) NULL,
	[SourceChannelName] [varchar](50) NULL,

-- Transformed Columns
	[ChannelDescription] [varchar](200) NOT NULL DEFAULT 'Pending',
	[ChannelName] [varchar](50) NOT NULL DEFAULT 'Pending',
	[ChannelTypeName] [varchar](50) NOT NULL DEFAULT 'Pending',

-- Metadata --
	[FirstDate] [datetime2](3) NULL,
	[FromDate] [datetime2](3) NULL,
	[ToDate] [datetime2](3) NULL,
	[CreatedDate] [datetime2](7) NULL,
	[CreatedBatchKey] [int] NULL,
	[CreatedBy] [varchar](32) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[ModifiedBatchKey] [int] NULL,
	[ModifiedBy] [varchar](32) NULL,
	[CleansedDate] [datetime2](7) NULL,
	[CleansedBatchKey] [int] NULL,
	[CleansedBy] [varchar](32) NULL,
    CONSTRAINT [CI-PK-ChannelKey] PRIMARY KEY CLUSTERED ([ChannelKey] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[Channel]([ChannelId] ASC)
    INCLUDE([ChannelKey]);
GO
