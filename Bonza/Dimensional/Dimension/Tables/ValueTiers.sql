﻿CREATE TABLE [Dimension].[ValueTiers] (
    [TierKey]    INT           NOT NULL,
    [TierNumber] SMALLINT      NOT NULL,
    [TierName]   VARCHAR (30)  NULL,
    [FromDate]   DATETIME2 (7) NULL,
    [ToDate]     DATETIME2 (7) NULL,
    CONSTRAINT [PK_ValueTiers] PRIMARY KEY CLUSTERED ([TierKey] ASC) ON [PRIMARY]
) ON [PRIMARY];
GO

CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[ValueTiers]([TierNumber] ASC)
    INCLUDE([TierKey]);
GO

