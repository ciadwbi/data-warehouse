CREATE TABLE [Dimension].[Account](
	[AccountKey] [int] IDENTITY (1,1) NOT NULL,

-- Ledger --
	[Account_LedgerKey] [int] NOT NULL DEFAULT -2,
	[LedgerID] [int] NOT NULL,
	[LedgerSource] [char](3) NOT NULL,
	[LegacyLedgerID] [int] NULL,
	[LedgerSequenceNumber] [int] NOT NULL,
	[LedgerOpenedDate] [datetime2](3) NULL,
	[LedgerOpenedDateOrigin] [varchar](20) NOT NULL,
	[LedgerOpenedDayKey] [int] NOT NULL,

-- Ledger Type --
	[Account_LedgerTypeKey] [int] NOT NULL DEFAULT -2,
	[LedgerTypeID] [int] NOT NULL,
	[LedgerType] [varchar](50) NOT NULL,
	[LedgerGroup] [varchar](10) NOT NULL DEFAULT 'Pending',
	[LedgerGrouping] [varchar](30) NOT NULL DEFAULT 'Pending',

-- Account --
	[Account_AccountKey] [int] NOT NULL DEFAULT -2,
	[AccountID] [int] NOT NULL,
	[AccountSource] [char](3) NOT NULL,
	[PIN] [int] NULL,
	[UserName] [varchar](64) NULL,
	[PasswordHash] [nvarchar](200) NULL,
	[PasswordLastChanged] [datetime2](3) NULL,
	[ExactTargetID] [varchar](40) NULL,
	[AccountFlags] [int] NULL,

	[AccountOpenedDate] [datetime2](3) NULL,
	[AccountOpenedDayKey] [int] NOT NULL DEFAULT -2,
	[AccountOpenedChannelKey] [int] NOT NULL,

	[ReferralBurnCode] [varchar](50) NOT NULL,
	[ReferralBurntDate] [datetime2](3) NULL,
	[ReferralAccountKey] [int] NOT NULL DEFAULT - 2,

	[EachwayAccountId] [int] NULL,
	[CardAccountId] [nvarchar](18) NULL,
	[CardFirstIssueDate] [datetime2](3) NULL,
	[CardIssueDate] [datetime2](3) NULL,

	[VelocityNumber] [bigint] NULL,
	[VelocityFirstName] [varchar](50) NULL,
	[VelocitySurname] [varchar](50) NULL,
	[VelocityLinkedDate] [datetime2](3) NULL,
	[RewardStatusDate] [datetime2](3) NULL,

	[CompanyKey] [int] NOT NULL DEFAULT -2,

-- CardStatus --
	[Account_CardStatusKey] [int] NOT NULL DEFAULT -2,
	[CardStatusId] [nvarchar](4) NULL,
	[CardStatusCode] [char](1) NOT NULL DEFAULT 'P',
	[CardStatus] [varchar](15) NOT NULL DEFAULT 'Pending',

-- Reward Status --
	[Account_RewardStatusKey] [int] NOT NULL DEFAULT -2,
	[RewardStatusId] [int] NOT NULL,
	[RewardStatusCode] [char](1) NOT NULL DEFAULT 'P',
	[RewardStatus] [varchar](50) NOT NULL DEFAULT 'Pending',

-- Account Type
	[Account_AccountTypeKey] [int] NOT NULL DEFAULT -2,
	[AccountTypeId] [char] (1) NOT NULL DEFAULT 'P',
	[AccountTypeCode] [char] (1) NOT NULL DEFAULT 'P',
	[AccountType] [varchar] (10) NOT NULL DEFAULT 'Pending',

-- Managed By
	[Account_ManagedByKey] [int] NOT NULL DEFAULT -2,
	[ManagedById] [char](32) NOT NULL DEFAULT 'Pending',
	[ManagedBy] [varchar](50) NOT NULL DEFAULT 'Pending',
	[BDM] [varchar](50) NOT NULL DEFAULT 'Pending',
	[VIPCode] [int] NOT NULL DEFAULT -2,
	[VIP] [varchar](50) NOT NULL DEFAULT 'Pending',

-- Traffic Source
	[Account_TrafficSourceKey] [int] NOT NULL DEFAULT -2,
	[TrafficSourceId] [char](32) NOT NULL DEFAULT 'Pending',
	[BTag2] [varchar](100) NOT NULL DEFAULT 'Pending',
	[AffiliateID] [varchar](30) NOT NULL DEFAULT 'Pending',
	[AffiliateName] [varchar](100) NOT NULL DEFAULT 'Pending',
	[SiteID] [varchar](10) NOT NULL DEFAULT 'Pending',
	[TrafficSource] [varchar](50) NOT NULL DEFAULT 'Pending',
	[RefURL] [varchar](500) NOT NULL DEFAULT 'Pending',
	[CampaignID] [varchar](50) NOT NULL DEFAULT 'Pending',
	[Keywords] [varchar](500) NOT NULL DEFAULT 'Pending',
	[Promo] [varchar](100) NOT NULL DEFAULT 'Pending',

-- Tracking Heirarchy
	[ReferralDevice] [varchar](100) NOT NULL DEFAULT 'Pending',
	[ReferralChannel] [varchar](100) NOT NULL DEFAULT 'Pending',
	[ReferralPublisher] [varchar](100) NOT NULL DEFAULT 'Pending',
	[ReferralTargeting] [varchar](100) NOT NULL DEFAULT 'Pending',
	[ReferralSpecifics] [varchar](100) NOT NULL DEFAULT 'Pending',

--Statement
	[Account_StatementKey] [int] NOT NULL DEFAULT -2,
	[StatementId] [char](32) NOT NULL,
	[StatementMethod] [varchar](20) NOT NULL,
	[StatementFrequency] [varchar](20) NOT NULL,

-- Customer --
	[Account_CustomerKey] [int] NOT NULL DEFAULT -2,
	[CustomerID] [int] NOT NULL DEFAULT -2,
	[CustomerSource] [char](3) NOT NULL DEFAULT 'PDG',
	[Title] [varchar](10) NULL DEFAULT 'Pending',
	[Surname] [varchar](60) NULL DEFAULT 'Pending',
	[FirstName] [varchar](60) NULL DEFAULT 'Pending',
	[MiddleName] [varchar](60) NULL DEFAULT 'Pending',
	[Gender] [char](1) NOT NULL DEFAULT 'P',
	[DOB] [date] NULL,
	[Age]  AS (DATEDIFF(year,[DOB],GETDATE())),
	[Phone] [varchar](20) NULL DEFAULT 'Pending',
	[Mobile] [varchar](20) NULL DEFAULT 'Pending',
	[Fax] [varchar](20) NULL DEFAULT 'Pending',
	[PhoneLastChanged] [datetime2](3) NULL,
	[Email] [varchar](100) NOT NULL DEFAULT 'Pending',
	[EmailLastChanged] [datetime2](3) NULL,
	[Address1] [varchar](60) NOT NULL DEFAULT 'Pending',
	[Address2] [varchar](60) NOT NULL DEFAULT 'Pending',
	[Suburb] [varchar](50) NOT NULL DEFAULT 'Pending',
	[PostCode] [varchar](7) NOT NULL DEFAULT 'Pending',
	[GeographicalLocation] [geography] NULL,
	[AddressLastChanged] [datetime2](3) NULL,

-- State
	[Account_StateKey] [int] NOT NULL DEFAULT -2,
	[StateId] [char](32) NOT NULL DEFAULT 'Pending',
	[StateCode] [varchar](3) NOT NULL DEFAULT 'PDG',
	[State] [varchar](30) NOT NULL DEFAULT 'Pending',

-- Country
	[Account_CountryKey] [int] NOT NULL DEFAULT -2,
	[CountryId] [char](32) NOT NULL DEFAULT 'Pending',
	[CountryCode] [varchar](3) NOT NULL DEFAULT 'PDG',
	[Country] [varchar](40)  NOT NULL DEFAULT 'Pending',
	[AustralianClient] [varchar](3) NOT NULL DEFAULT 'PDG',

-- Advanced
	[ReferralAccountID] [int] NOT NULL,
	[ReferralAccountSource] [char](3) NOT NULL,

	[SourceBTag2] [varchar](100) NULL,
	[SourceTrafficSource] [varchar](50) NULL,
	[SourceRefURL] [varchar](500) NULL,
	[SourceCampaignID] [varchar](50) NULL,
	[SourceKeywords] [varchar](500) NULL,
	[SourceAffiliateName] [varchar](32) NULL,
	[SourceSitePromoName] [varchar](100) NULL,

	-- Tracking Heirarchy
	[TrackingChannel] [varchar](20) NULL,
    [TrackingAppId] [varchar](20) NULL,
    [TrackingAppName] [varchar](50) NULL,
    [TrackingAppVersion] [varchar](20) NULL,
    [TrackingEventName] [varchar](255) NULL,
    [TrackingTracker] [varchar](50) NULL,
    [TrackingTrackerName] [varchar](MAX) NULL,
    [TrackingAdwordsCampaignType] [varchar](255) NULL,
    [TrackingAdwordsCampaignName] [varchar](255) NULL,
    [TrackingFbCampaignGroupName] [varchar](255) NULL,
    [TrackingFbCampaignName] [varchar](255) NULL,
    [TrackingIpAddress] [varchar](255) NULL,
    [TrackingPartnerParameters] [varchar](MAX) NULL,
	[TrackingDcpBtag] [varchar](30) NULL,
    [TrackingDcpCampaign] [varchar](30) NULL,
    [TrackingDcpKeyword] [varchar](30) NULL,
    [TrackingDcpPublisher] [varchar](30) NULL,
    [TrackingDcpSource] [varchar](30) NULL,
    [TrackingLabel] [varchar](30) NULL,
    [TrackingPublisher] [varchar](30) NULL,
    [TrackingKeyword] [varchar](30) NULL,
    [TrackingCampaign] [varchar](30) NULL,
    [TrackingBtag] [varchar](30) NULL,
    [TrackingSource] [varchar](100) NULL,
	
	[AccountTitle] [varchar](10) NULL,
	[AccountSurname] [varchar](60) NULL,
	[AccountFirstName] [varchar](60) NULL,
	[AccountMiddleName] [varchar](60) NULL,
	[AccountGender] [char](1) NULL,
	[AccountDOB] [date] NULL,
	[HasHomeAddress] [tinyint] NOT NULL,
	[HomeAddressLastChanged] [datetime2](3) NULL,
	[HomePhoneLastChanged] [datetime2](3) NULL,
	[HomeEmailLastChanged] [datetime2](3) NULL,
	[HomeAddress1] [varchar](60) NULL,
	[HomeAddress2] [varchar](60) NULL,
	[HomeSuburb] [varchar](50) NULL,
	[HomeStateCode] [varchar](3) NULL,
	[HomeState] [varchar](30) NULL,
	[HomePostCode] [varchar](7) NULL,
	[HomeCountryCode] [varchar](3) NULL,
	[HomeCountry] [varchar](40) NULL,
	[HomePhone] [varchar](20) NULL,
	[HomeMobile] [varchar](20) NULL,
	[HomeFax] [varchar](20) NULL,
	[HomeEmail] [varchar](100) NULL,
	[HomeAlternateEmail] [varchar](100) NULL,
	[HasPostalAddress] [tinyint] NOT NULL,
	[PostalAddressLastChanged] [datetime2](3) NULL,
	[PostalPhoneLastChanged] [datetime2](3) NULL,
	[PostalEmailLastChanged] [datetime2](3) NULL,
	[PostalAddress1] [varchar](60) NULL,
	[PostalAddress2] [varchar](60) NULL,
	[PostalSuburb] [varchar](50) NULL,
	[PostalStateCode] [varchar](3) NULL,
	[PostalState] [varchar](30) NULL,
	[PostalPostCode] [varchar](7) NULL,
	[PostalCountryCode] [varchar](3) NULL,
	[PostalCountry] [varchar](40) NULL,
	[PostalPhone] [varchar](20) NULL,
	[PostalMobile] [varchar](20) NULL,
	[PostalFax] [varchar](20) NULL,
	[PostalEmail] [varchar](100) NULL,
	[PostalAlternateEmail] [varchar](100) NULL,
	[HasBusinessAddress] [tinyint] NOT NULL,
	[BusinessAddressLastChanged] [datetime2](3) NULL,
	[BusinessPhoneLastChanged] [datetime2](3) NULL,
	[BusinessEmailLastChanged] [datetime2](3) NULL,
	[BusinessAddress1] [varchar](60) NULL,
	[BusinessAddress2] [varchar](60) NULL,
	[BusinessSuburb] [varchar](50) NULL,
	[BusinessStateCode] [varchar](3) NULL,
	[BusinessState] [varchar](30) NULL,
	[BusinessPostCode] [varchar](7) NULL,
	[BusinessCountryCode] [varchar](3) NULL,
	[BusinessCountry] [varchar](40) NULL,
	[BusinessPhone] [varchar](20) NULL,
	[BusinessMobile] [varchar](20) NULL,
	[BusinessFax] [varchar](20) NULL,
	[BusinessEmail] [varchar](100) NULL,
	[BusinessAlternateEmail] [varchar](100) NULL,
	[HasOtherAddress] [tinyint] NOT NULL,
	[OtherAddressLastChanged] [datetime2](3) NULL,
	[OtherPhoneLastChanged] [datetime2](3) NULL,
	[OtherEmailLastChanged] [datetime2](3) NULL,
	[OtherAddress1] [varchar](60) NULL,
	[OtherAddress2] [varchar](60) NULL,
	[OtherSuburb] [varchar](50) NULL,
	[OtherStateCode] [varchar](3) NULL,
	[OtherState] [varchar](30) NULL,
	[OtherPostCode] [varchar](7) NULL,
	[OtherCountryCode] [varchar](3) NULL,
	[OtherCountry] [varchar](40) NULL,
	[OtherPhone] [varchar](20) NULL,
	[OtherMobile] [varchar](20) NULL,
	[OtherFax] [varchar](20) NULL,
	[OtherEmail] [varchar](100) NULL,
	[OtherAlternateEmail] [varchar](100) NULL,

-- Metadata --
	[FirstDate] [datetime2](3) NULL,
	[FromDate] [datetime2](3) NULL,
	[ToDate] [datetime2](3) NULL,
	[CreatedDate] [datetime2](7) NULL,
	[CreatedBatchKey] [int] NULL,
	[CreatedBy] [varchar](32) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[ModifiedBatchKey] [int] NULL,
	[ModifiedBy] [varchar](32) NULL,
	[CleansedDate] [datetime2](7) NULL,
	[CleansedBatchKey] [int] NULL,
	[CleansedBy] [varchar](32) NULL,
	[Increment] [int] NULL,
	CONSTRAINT [CI_Account_PK] PRIMARY KEY CLUSTERED ([AccountKey] ASC) WITH (DATA_COMPRESSION = ROW)
)
GO

CREATE NONCLUSTERED INDEX [NI_Account_LedgerKey]
    ON [Dimension].[Account]([Account_LedgerKey] ASC);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Account_NK]
    ON [Dimension].[Account]([LedgerId] ASC, [LedgerSource] ASC)
    INCLUDE([Account_LedgerKey]);
GO

CREATE NONCLUSTERED INDEX [NI_Account_LedgerTypeKey]
    ON [Dimension].[Account]([Account_LedgerTypeKey] ASC);
GO

CREATE NONCLUSTERED INDEX [NI_Account_LedgerType_NK] 
	ON [Dimension].[Account] ([LedgerTypeID] ASC, [LedgerSource] ASC)
	INCLUDE ([Account_LedgerTypeKey])
GO

CREATE NONCLUSTERED INDEX [NI_Account_AccountKey]
    ON [Dimension].[Account]([Account_AccountKey] ASC);
GO

CREATE NONCLUSTERED INDEX [NI_Account_Account_NK] 
	ON [Dimension].[Account] ([AccountID] ASC, [AccountSource] ASC)
	INCLUDE ([Account_AccountKey] ,[LedgerSequenceNumber])
GO

CREATE NONCLUSTERED INDEX [NI_Account_AccountTypeKey]
    ON [Dimension].[Account]([Account_AccountTypeKey] ASC);
GO

CREATE NONCLUSTERED INDEX [NI_Account_AccountType_NK] 
	ON [Dimension].[Account] ([AccountTypeID] ASC, [AccountSource] ASC)
	INCLUDE ([Account_AccountTypeKey])
GO

CREATE NONCLUSTERED INDEX [NI_Account_ManagedByKey]
    ON [Dimension].[Account]([Account_ManagedByKey] ASC);
GO

CREATE NONCLUSTERED INDEX [NI_Account_ManagedBy_NK] 
	ON [Dimension].[Account] ([ManagedById] ASC)
	INCLUDE ([Account_ManagedByKey])
GO

CREATE NONCLUSTERED INDEX [NI_Account_TrafficSourceKey]
    ON [Dimension].[Account]([Account_TrafficSourceKey] ASC);
GO

CREATE NONCLUSTERED INDEX [NI_Account_TrafficSource_NK] 
	ON [Dimension].[Account] ([TrafficSourceId] ASC)
	INCLUDE ([Account_TrafficSourceKey])
GO

CREATE NONCLUSTERED INDEX [NI_Account_StatementKey]
    ON [Dimension].[Account]([Account_StatementKey] ASC);
GO

CREATE NONCLUSTERED INDEX [NI_Account_Statement_NK] 
	ON [Dimension].[Account] ([StatementID] ASC)
	INCLUDE ([Account_StatementKey])
GO

CREATE NONCLUSTERED INDEX [NI_Account_CustomerKey]
    ON [Dimension].[Account]([Account_CustomerKey] ASC);
GO

CREATE NONCLUSTERED INDEX [NI_Account_Customer_NK] 
	ON [Dimension].[Account] ([CustomerID] ASC,	[CustomerSource] ASC)
	INCLUDE ([Account_CustomerKey])
GO

CREATE NONCLUSTERED INDEX [NI_Account_StateKey]
    ON [Dimension].[Account]([Account_StateKey] ASC);
GO

CREATE NONCLUSTERED INDEX [NI_Account_State_NK] 
	ON [Dimension].[Account] ([StateID] ASC)
	INCLUDE ([Account_StateKey])
GO

CREATE NONCLUSTERED INDEX [NI_Account_CountryKey]
    ON [Dimension].[Account]([Account_CountryKey] ASC);
GO

CREATE NONCLUSTERED INDEX [NI_Account_Country_NK] 
	ON [Dimension].[Account] ([CountryID] ASC)
	INCLUDE ([Account_CountryKey])
GO

CREATE NONCLUSTERED INDEX [NI_Account_CompanyKey]
	ON [Dimension].[Account] ([CompanyKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Account_LedgerOpenedDayKey]
	ON [Dimension].[Account] ([LedgerOpenedDayKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Account_AccountOpenedDayKey]
	ON [Dimension].[Account] ([AccountOpenedDayKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Account_AccountOpenedChannelKey]
	ON [Dimension].[Account] ([AccountOpenedChannelKey] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_Account_ModifiedBatchKey] 
	ON [Dimension].[Account] ([ModifiedBatchKey] ASC)
	INCLUDE ([Increment])
GO

CREATE NONCLUSTERED COLUMNSTORE INDEX [NC_Account] ON [Dimension].[Account]
(
	[AccountKey],
	[CompanyKey],
	[LedgerOpenedDayKey],
	[AccountOpenedDayKey],
	[AccountOpenedChannelKey],
	[LedgerID],
	[LedgerSource],
	[LegacyLedgerID],
	[LedgerSequenceNumber],
	[LedgerTypeID],
	[LedgerType],
	[LedgerGroup],
	[LedgerGrouping],
	[LedgerOpenedDate],
	[LedgerOpenedDateOrigin],
	[AccountID],
	[AccountSource],
	[PIN],
	[UserName],
	[PasswordHash],
	[PasswordLastChanged],
	[ExactTargetID],
	[AccountFlags],
	[AccountTypeCode],
	[AccountType],
	[AccountOpenedDate],
	[BDM],
	[VIPCode],
	[VIP],
	[BTag2],
	[AffiliateID],
	[AffiliateName],
	[SiteID],
	[TrafficSource],
	[RefURL],
	[CampaignID],
	[Keywords],
	[StatementMethod],
	[StatementFrequency],
	[VelocityNumber],
	[VelocityFirstName],
	[VelocitySurname],
	[VelocityLinkedDate],
	[CustomerID],
	[CustomerSource],
	[Title],
	[Surname],
	[FirstName],
	[MiddleName],
	[Gender],
	[DOB],
	[Address1],
	[Address2],
	[Suburb],
	[StateCode],
	[State],
	[PostCode],
	[CountryCode],
	[Country],
	[AustralianClient],
	[Phone],
	[Mobile],
	[Fax],
	[Email],
	[AccountTitle],
	[AccountSurname],
	[AccountFirstName],
	[AccountMiddleName],
	[AccountGender],
	[AccountDOB],
	[HasHomeAddress],
	[HomeAddress1],
	[HomeAddress2],
	[HomeSuburb],
	[HomeStateCode],
	[HomeState],
	[HomePostCode],
	[HomeCountryCode],
	[HomeCountry],
	[HomePhone],
	[HomeMobile],
	[HomeFax],
	[HomeEmail],
	[HomeAlternateEmail],
	[HasPostalAddress],
	[PostalAddress1],
	[PostalAddress2],
	[PostalSuburb],
	[PostalStateCode],
	[PostalState],
	[PostalPostCode],
	[PostalCountryCode],
	[PostalCountry],
	[PostalPhone],
	[PostalMobile],
	[PostalFax],
	[PostalEmail],
	[PostalAlternateEmail],
	[HasBusinessAddress],
	[BusinessAddress1],
	[BusinessAddress2],
	[BusinessSuburb],
	[BusinessStateCode],
	[BusinessState],
	[BusinessPostCode],
	[BusinessCountryCode],
	[BusinessCountry],
	[BusinessPhone],
	[BusinessMobile],
	[BusinessFax],
	[BusinessEmail],
	[BusinessAlternateEmail],
	[HasOtherAddress],
	[OtherAddress1],
	[OtherAddress2],
	[OtherSuburb],
	[OtherStateCode],
	[OtherState],
	[OtherPostCode],
	[OtherCountryCode],
	[OtherCountry],
	[OtherPhone],
	[OtherMobile],
	[OtherFax],
	[OtherEmail],
	[OtherAlternateEmail],
	[SourceBTag2],
	[SourceTrafficSource],
	[SourceRefURL],
	[SourceCampaignID],
	[SourceKeywords],
	[FirstDate],
	[FromDate],
	[ToDate],
	[CreatedDate],
	[CreatedBatchKey],
	[CreatedBy],
	[ModifiedDate],
	[ModifiedBatchKey],
	[ModifiedBy],
	[Increment]
)
GO

