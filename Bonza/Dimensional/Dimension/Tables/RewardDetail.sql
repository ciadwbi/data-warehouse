﻿CREATE TABLE [Dimension].[RewardDetail](
	[RewardDetailKey] [int] IDENTITY(1,1) NOT NULL,
	[RewardTransactionTypeKey] [smallint] NOT NULL,
	[TransactionCode] [varchar](50) NOT NULL,
	[RewardTransactionTypeCode] [char](1) NOT NULL,
	[RewardTransactionType] [varchar](50) NOT NULL,
	[RewardTransactionClassKey] [smallint] NOT NULL,
	[RewardTransactionClassCode] [char](1) NOT NULL,
	[RewardTransactionClass] [varchar](50) NOT NULL,
	[RewardBetTypeKey] [smallint] NOT NULL,
	[RewardBetTypeId] [smallint] NOT NULL,
	[RewardBetTypeCode] [char](1) NOT NULL,
	[RewardBetType] [varchar](50) NOT NULL,
	[RewardReasonKey] [int] NOT NULL,
	[RewardReasonId] [char](32) NOT NULL,
	[RewardReason] [varchar](255) NOT NULL,
	[RewardReasonTypeKey] [int] NOT NULL,
	[RewardReasonTypeCode] [char](1) NOT NULL,
	[RewardReasonType] [varchar](50) NOT NULL,
	[DirectionCode] [char](1) NOT NULL,
	[FirstDate] [datetime2](3) NOT NULL,
	[FromDate] [datetime2](3) NOT NULL,
	[ToDate] [datetime2](3) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](30) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](30) NOT NULL,
	[CleansedDate] [datetime2](7) NULL,
	[CleansedBatchKey] [int] NULL,
	[CleansedBy] [varchar](32) NULL,
    CONSTRAINT [CI_RewardDetail] PRIMARY KEY CLUSTERED ([RewardDetailKey] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_RewardDetail_NK]
    ON [Dimension].[RewardDetail]([RewardTransactionTypeCode] ASC, [RewardBetTypeId] ASC, [RewardReasonId] ASC, [DirectionCode] ASC)
    INCLUDE([RewardDetailKey]);
GO
