﻿CREATE TABLE [Dimension].[DayZone](
	[DayKey] [int] NOT NULL,
	[MasterDayKey] [int] NULL,
	[AnalysisDayKey] [int] NULL,
	[MasterDayText] [char](11) NULL,
	[AnalysisDayText] [char](11) NULL,
	[FromDate] [datetime2](3) NULL,
	[ToDate] [datetime2](3) NULL,
	[CreatedDate] [datetime2](3) NULL,
	[CreatedBatchKey] [int] NULL,
	[CreatedBy] [varchar](32) NULL,
	[ModifiedDate] [datetime2](3) NULL,
	[ModifiedBatchKey] [int] NULL,
	[ModifiedBy] [varchar](32) NULL,
 CONSTRAINT [CI-DayKey] PRIMARY KEY CLUSTERED ([DayKey] ASC)
)  ON par_sch_int_Dimensional(DayKey)
GO

CREATE NONCLUSTERED INDEX [NI_DayZone_MasterDayText]
	ON [Dimension].[DayZone] ([MasterDayText] ASC)
	INCLUDE ([FromDate],[ToDate], [DayKey])
GO

CREATE NONCLUSTERED INDEX [NI_DayZone_MasterDayKey] 
	ON [Dimension].[DayZone] ([MasterDayKey] ASC)
	INCLUDE ([DayKey])
GO

