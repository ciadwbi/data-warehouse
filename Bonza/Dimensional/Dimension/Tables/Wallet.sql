﻿CREATE TABLE [Dimension].[Wallet] (
    [WalletKey]        SMALLINT      IDENTITY (1, 1) NOT NULL,
    [WalletId]         CHAR (1)      NOT NULL,
    [WalletName]       VARCHAR (32)  NOT NULL,
    [FromDate]         DATETIME2 (0) NOT NULL,
    [ToDate]           DATETIME2 (0) NOT NULL,
    [FirstDate]        DATETIME2 (0) NOT NULL,
    [CreatedDate]      DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]  INT           NOT NULL,
    [CreatedBy]        VARCHAR (32)  NOT NULL,
    [ModifiedDate]     DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey] INT           NOT NULL,
    [ModifiedBy]       VARCHAR (32)  NOT NULL,
    CONSTRAINT [CI-PK-WalletKey] PRIMARY KEY CLUSTERED ([WalletKey] ASC)
);




GO


