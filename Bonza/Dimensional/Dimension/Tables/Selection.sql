﻿CREATE TABLE [Dimension].[Selection] (
    [SelectionKey]					INT				IDENTITY (1, 1) NOT NULL,
    [SelectionStatusDayKey]			INT				NOT NULL DEFAULT -2,
    [MarketKey]						INT				NOT NULL,
-- Selection(a set of all predictions for every market.g. )
	[MarketID]						INT				NOT NULL,					-- Source Key - Intrabet: EventId
	[CompetitorID]					INT				NOT NULL,					-- Source Key - Intrabet: CompetitorId
    [Source]						CHAR (3)		NOT NULL,
    [CompetitorNumber]				SMALLINT		NULL,						-- The number on the competitors back
    [DrawNumber]					VARCHAR (50)	NOT NULL DEFAULT 'Pending',	-- The gate or sequence number in which the competitors compete
    [Weight]						DECIMAL (4,1)	NULL,						-- Derived
    [Form]							VARCHAR	(50)	NOT NULL DEFAULT 'Pending',	-- Derived
	[Jockey]						VARCHAR (50)	NOT NULL DEFAULT 'Pending',	-- Derived
    [Trainer]						VARCHAR (50)	NOT NULL DEFAULT 'Pending',	-- Derived
-- Prediction(a predicted outcome for a competitor which might repeat across more than one market e.g. Cleveland Cavaliers to Win, LeBron James highest scorer, etc (can be reused whoever Cleveland are playing)
	[Selection_PredictionKey]		INT				NOT NULL DEFAULT -2,
    [Prediction]					VARCHAR (100)	NOT NULL DEFAULT 'Pending',	-- Distinct list of source system predictions (competitor names)
-- Status (current status of any prediction within a market e.g. Scratched, Withdrawn, etc)
	[Selection_StatusKey]			INT				NOT NULL DEFAULT -2,
    [SelectionStatus]				VARCHAR (20)	NOT NULL DEFAULT 'Pending',	-- Derived
    [SelectionStatusDateTime]		DATETIME2 (3)	NULL,						-- Derived
-- Result (the eventual success or failure of the prediction on the specific market e.g. win/lose for the predicition or the actual final score)
	[Selection_ResultKey]			INT				NOT NULL DEFAULT -2,
    [SelectionResult]				VARCHAR (20)	NOT NULL DEFAULT 'Pending',	-- Derived
    [Score]							INT				NULL,	 					-- Derived
    [Placing]						INT				NULL,	 					-- Derived
-- Competitor (the team/participant/sport that is the focus of the prediction e.g. Cleveland Cavaliers or LeBron James playing for Cleveland Cavaliers)
	[Selection_CompetitorKey]		INT				NOT NULL DEFAULT -2,
	[RelationshipStartDate]			DATETIME2 (3)	NULL,						-- Derived
    [RelationshipEndDate]			DATETIME2 (3)	NULL,						-- Derived
-- Outcome (The predicted result e.g. Horse to win, Cavaliers to score over 100, etc)
	[Selection_OutcomeKey]			INT				NOT NULL DEFAULT -2,
    [Outcome]						VARCHAR (100)	NOT NULL DEFAULT 'Pending',	-- Derived
    [PredictedScore]				INT				NULL,	 					-- Derived
    [PredictedPlacing]				INT				NULL,	 					-- Derived
-- Outcome Type (Logical grouping of predicted outcomes e.g. Head to head, highest score etc....)
	[Selection_OutcometypeKey]		SMALLINT		NOT NULL DEFAULT -2,
    [OutcomeType]					VARCHAR (30)	NOT NULL DEFAULT 'Pending',	-- Derived
-- Team (Team name irrespective of sport class e.g. Australia, Cleveland Cavaliers, N/A (for horses))
	[Selection_TeamKey]				INT				NOT NULL DEFAULT -2,
    [Team]							VARCHAR (50)	NOT NULL DEFAULT 'Pending',	-- Derived
    [TeamCountry]					VARCHAR (30)	NOT NULL DEFAULT 'Pending',	-- Derived
-- Participant (Person or horse or dog etc)
	[Selection_ParticipantKey]		INT				NOT NULL DEFAULT -2,
    [Participant]					VARCHAR (50)	NOT NULL DEFAULT 'Pending',	-- Derived
    [ParticipantCountry]			VARCHAR (30)	NOT NULL DEFAULT 'Pending',	-- Derived
-- Source (Intrabet.tblCompetitors)
	[SourceSaddleNumber]			INT				NULL,			--Intrabet.tblCompetitors
    [SourceCompetitorName]			VARCHAR (100)	NULL,			--Intrabet.tblCompetitors
	[SourceScratched]				BIT				NULL,			--Intrabet.tblCompetitors
	[SourceScratchedLate]			BIT				NULL,			--Intrabet.tblCompetitors
	[SourceScratchedTime]			DATETIME2 (3)	NULL,			--Intrabet.tblScratchingTimes
	[SourceScratchedFieldSize]		INT				NULL,			--Intrabet.tblScratchingTimes
	[SourceKnockedOut]				BIT				NULL,			--Intrabet.tblCompetitors
    [SourceJockey]					VARCHAR (50)	NULL,			--Intrabet.tblCompetitors
	[SourceDraw]					VARCHAR (50)	NULL,			--Intrabet.tblCompetitors
    [SourceForm]					VARCHAR (20)	NULL,			--Intrabet.tblCompetitors
	[SourceHistory]					VARCHAR (10)	NULL,			--Intrabet.tblCompetitors
	[SourceWeight]					DECIMAL (4,1)	NULL,			--Intrabet.tblCompetitors
	[SourceTrainer]					VARCHAR (50)	NULL,			--Intrabet.tblCompetitors
	[SourceBestTime]				VARCHAR (10)	NULL,			--Intrabet.tblCompetitors
	[SourceMargin]					INT				NULL,			--Intrabet.tblSportsResults
    [SourceFinalPlacing]			INT				NULL,			--Intrabet.tblSportsResults/tblRacingResults
    [SourceTotalScore]				INT				NULL,			--Intrabet.tblSportsResults
	[SourceExtraTime]				BIT				NULL,			--Intrabet.tblSportsResults
	[SourceSpecialCondition]		INT				NULL,			--Intrabet.tblSportsResults
-- Metadata
    [FirstDate]						DATETIME2 (3)	NOT NULL,
    [FromDate]						DATETIME2 (3)	NOT NULL,
    [ToDate]						DATETIME2 (3)	NOT NULL,
    [CreatedDate]					DATETIME2 (7)	NOT NULL,
    [CreatedBatchKey]				INT				NOT NULL,
    [CreatedBy]						VARCHAR (100)	NOT NULL,
    [ModifiedDate]					DATETIME2 (7)	NOT NULL,
    [ModifiedBatchKey]				INT				NOT NULL,
    [ModifiedBy]					VARCHAR (100)	NOT NULL,
    [CleansedDate]					DATETIME2 (7)	NULL,
    [CleansedBatchKey]				INT				NULL,
    [CleansedBy]					VARCHAR (100)	NULL,
 CONSTRAINT [CI_Selection] PRIMARY KEY CLUSTERED 
(
	[SelectionKey] ASC
) WITH (DATA_COMPRESSION = ROW)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Selection_NK]
	ON [Dimension].[Selection] ([MarketID] ASC, [CompetitorID] ASC, [Source] ASC)
	WITH (DATA_COMPRESSION = ROW)
GO

CREATE NONCLUSTERED INDEX [NI_Selection_ModifiedBatchKey] 
	ON [Dimension].[Selection] ([ModifiedBatchKey] ASC)
	WITH (DATA_COMPRESSION = ROW)
GO

