﻿CREATE TABLE [Dimension].[Class] (
    [ClassKey]			 SMALLINT      IDENTITY (1, 1) NOT NULL,
    [HashKey]			 BINARY(16)	   NOT NULL DEFAULT 0x,
    [Source]			 CHAR (3)      NOT NULL,
    [SubClassId]         SMALLINT      NULL,
    [SourceSubClassName] VARCHAR (32)  NULL,
    [SubClassCode]       VARCHAR (3)   NOT NULL DEFAULT 'PDG',
    [SubClassName]       VARCHAR (32)  NOT NULL DEFAULT 'Pending',
    [ClassId]			 SMALLINT      NOT NULL,
    [SourceClassName]	 VARCHAR (32)  NOT NULL,
    [ClassCode]			 VARCHAR (4)   NOT NULL,
    [ClassName]			 VARCHAR (32)  NOT NULL,
    [ClassIcon]			 IMAGE         NULL,
    [SuperclassKey]		 INT           NOT NULL,
    [SuperclassName]	 VARCHAR (32)  NOT NULL,
    [FromDate]			 DATETIME2 (3) NOT NULL,
    [ToDate]			 DATETIME2 (3) NOT NULL,
	[CreatedDate]		 DATETIME2 (7) NOT NULL,
    [CreatedBatchKey]	 INT           NOT NULL,
    [CreatedBy]			 VARCHAR (32)  NOT NULL,
    [ModifiedDate]		 DATETIME2 (7) NOT NULL,
    [ModifiedBatchKey]	 INT           NOT NULL,
    [ModifiedBy]		 VARCHAR (32)  NOT NULL,
    CONSTRAINT [CI_Class_PK] PRIMARY KEY CLUSTERED ([ClassKey] ASC)
);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Class_HK]
    ON [Dimension].[Class]([HashKey] ASC, [FromDate] ASC)
    INCLUDE([ClassKey]);
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_Class_NK]
    ON [Dimension].[Class]([ClassId] ASC, [SubClassId] ASC, [Source] ASC, [FromDate] ASC)
    INCLUDE([ClassKey]);
GO

