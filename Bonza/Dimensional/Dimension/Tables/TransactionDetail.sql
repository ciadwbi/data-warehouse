﻿CREATE TABLE [Dimension].[TransactionDetail] (
	[TransactionDetailKey] [smallint] IDENTITY(1,1) NOT NULL,
	[TransactionDetailId] [char](4) NOT NULL,
	[TransactionTypeID] [char](2) NOT NULL,
	[TransactionType] [varchar](32) NOT NULL,
	[TransactionStatusID] [char](2) NOT NULL,
	[TransactionStatus] [varchar](32) NOT NULL,
	[TransactionMethodID] [char](2) NOT NULL,
	[TransactionMethod] [varchar](32) NOT NULL,
	[TransactionDirection] [varchar](3) NOT NULL,
	[TransactionSign] [smallint] NOT NULL,
	[FromDate] [datetime2](0) NOT NULL,
	[ToDate] [datetime2](0) NOT NULL,
	[FirstDate] [datetime2](0) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NOT NULL,
    CONSTRAINT [CI-PK-TDKey] PRIMARY KEY CLUSTERED ([TransactionDetailKey] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [NCI-NaturalKeys]
    ON [Dimension].[TransactionDetail]([TransactionDetailId] ASC)
    INCLUDE([TransactionDetailKey]);

