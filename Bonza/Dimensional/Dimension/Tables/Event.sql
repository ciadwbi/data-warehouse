﻿CREATE TABLE [Dimension].[Event] (
    [EventKey]                        INT                IDENTITY (1, 1) NOT NULL,
    [ClassKey]                        SMALLINT           NULL,
    [ScheduledDayKey]                 INT                NULL,
    [ResultedDayKey]                  INT                NULL,
	[EventAbandonedDayKey]            INT                NULL,
-- Source Event
	[SourceEventId]                   INT                NOT NULL,
    [Source]                          CHAR (3)           NOT NULL,
    [SourceEvent]					  VARCHAR (255)      NULL,
	[SourceEventType]				  VARCHAR (32)		 NULL,
    [ParentEventId]					  INT                NULL,
    [ParentEvent]					  VARCHAR (255)      NULL,
    [GrandparentEventId]			  INT                NULL,
    [GrandparentEvent]				  VARCHAR (255)      NULL,
    [GreatGrandparentEventId]		  INT                NULL,
    [GreatGrandparentEvent]			  VARCHAR (255)      NULL,
    [SourceEventDate]                 DATETIME           NULL,
	[SourcePrizePool]                 BIGINT             NULL,
	[SourceTrackCondition]            VARCHAR (32)       NULL,
    [SourceEventWeather]              VARCHAR (32)       NULL,
	[SourceSubSportType]			  VARCHAR (50)		 NULL,
	[SourceDistance]				  INT				 NULL,
	[SourceRaceGroup]				  VARCHAR (50)		 NULL,
	[SourceRaceClass]				  VARCHAR (200)		 NULL,
	[SourceEventAbandoned]			  INT			     NULL,	
	[SourceLiveStreamPerformId]		  VARCHAR (50)		 NULL,
	[SourceHybridPricingTemplate]	  VARCHAR (100)		 NULL,
	[SourceWeightType]				  VARCHAR (100)		 NULL,
	[SourceRaceNumber]				  INT				 NULL,
	[SourceVenueId]		              INT                NULL,
    [SourceVenue]			          VARCHAR (255)      NULL,	
	[SourceVenueType]                 VARCHAR (10)       NULL,
	[SourceVenueEventType]			  INT				 NULL,
	[SourceVenueEventTypeName]		  VARCHAR (255)		 NULL,
    [SourceVenueStateCode]            CHAR (3)           NULL,
    [SourceVenueState]                VARCHAR (32)       NULL,
    [SourceVenueCountryCode]          CHAR (3)           NULL,
    [SourceVenueCountry]              VARCHAR (32)       NULL,
	[SubSportType]					  VARCHAR (50)		 NULL,
-- Event
	[Event_EventKey]				  INT				 NOT NULL DEFAULT -2,
    [EventId]                         INT                NOT NULL,
    [Event]                           VARCHAR (255)      NULL,
    [ScheduledDateTime]               DATETIME           NULL,
    [ScheduledVenueDateTime]          DATETIME           NULL,
    [ResultedDateTime]                DATETIME           NULL,
    [ResultedVenueDateTime]           DATETIME           NULL,
    [Grade]                           VARCHAR (255)      NULL,
    [Distance]                        VARCHAR (10)       NULL,
    [PrizePool]                       BIGINT             NULL,
	[RaceGroup]						  VARCHAR (50)		 NULL,
	[RaceClass]						  VARCHAR (200)		 NULL,
	[HybridPricingTemplate]			  VARCHAR (100)		 NULL,
	[WeightType]					  VARCHAR (100)		 NULL,
    [TrackCondition]                  VARCHAR (32)       NULL,
    [EventWeather]                    VARCHAR (32)       NULL,
	[EventAbandoned]				  VARCHAR (10)	     NULL,
	[EventAbandonedDateTime]		  DATETIME2 (3)		 NULL,
	[LiveVideoStreamed]			      VARCHAR (10)	     NULL,
	[LiveScoreBoard]				  VARCHAR (10)		 NULL,
    [ClassID]                         INT                NOT NULL,
-- Round
	[Event_RoundKey]				  INT				 NOT NULL DEFAULT -2,
	[Round]                           VARCHAR (255)      NULL,
    [RoundSequence]                   SMALLINT           NULL,
-- Competition
	[Event_CompetitionKey]			  INT				 NOT NULL DEFAULT -2,
    [Competition]                     VARCHAR (255)      NULL,
    [CompetitionSeason]               VARCHAR (32)       NULL,
    [CompetitionStartDate]            DATE               NULL,
    [CompetitionEndDate]              DATE               NULL,
-- Venue
	[Event_VenueKey]				  INT				 NOT NULL DEFAULT -2,
    [VenueId]		                  INT                NULL,
    [Venue]                           VARCHAR (100)      NULL,
    [VenueType]                       VARCHAR (100)      NULL,
-- Venue State
	[Event_VenueStateKey]			  INT				 NOT NULL DEFAULT -2,
    [VenueStateCode]                  VARCHAR (3)        NULL,
    [VenueState]                      VARCHAR (32)       NULL,
-- Venue Country
	[Event_VenueCountryKey]			  INT				 NOT NULL DEFAULT -2,
    [VenueCountryCode]                VARCHAR (3)        NULL,
    [VenueCountry]                    VARCHAR (32)       NULL,
-- Metadata
    [ColumnLock]                      BIGINT             NOT NULL,
    [FromDate]                        DATETIME2 (3)      NOT NULL,
    [FirstDate]                       DATETIME2 (3)      NOT NULL,
    [CreatedDate]                     DATETIME2 (7)      NOT NULL,
    [CreatedBatchKey]                 INT                NOT NULL,
    [CreatedBy]                       VARCHAR (100)      NOT NULL,
    [ModifiedDate]                    DATETIME2 (7)      NOT NULL,
    [ModifiedBatchKey]                INT                NOT NULL,
    [ModifiedBy]                      VARCHAR (100)      NOT NULL,
 	[CleansedDate]					  [datetime2](7)	 NULL,
	[CleansedBatchKey]				  [int]				 NULL,
	[CleansedBy]					  [varchar](50)		 NULL,
    [NewClassKey]                     SMALLINT           NULL,
	[SourceSubSportTypeId]			  INT				 NULL,
    CONSTRAINT [CI-PK-EventKey] PRIMARY KEY CLUSTERED ([EventKey] ASC) WITH (DATA_COMPRESSION = ROW)
);
GO
CREATE NONCLUSTERED INDEX [NI_Event_ModifiedBatchKey]
    ON [Dimension].[Event]([ModifiedBatchKey] ASC) 
	INCLUDE ([SourceEventType], [ClassKey],
	[Competition], [ColumnLock]) WITH (DATA_COMPRESSION = ROW);
GO
CREATE NONCLUSTERED INDEX [NI_Event_NK]
    ON [Dimension].[Event]([SourceEventId] ASC, [Source] ASC) WITH (DATA_COMPRESSION = ROW);
GO
CREATE NONCLUSTERED INDEX [NI_Event_BK]
    ON [Dimension].[Event]([EventId] ASC) WITH (DATA_COMPRESSION = ROW);
GO
CREATE NONCLUSTERED INDEX [NI_Event_SourceEvent_SourceEventDate]
    ON [Dimension].[Event]([SourceEvent] ASC, [SourceEventDate] ASC) 
	INCLUDE ([EventKey]) WITH (DATA_COMPRESSION = ROW);
GO
CREATE NONCLUSTERED INDEX [NI_Event_ScheduledDayKey] ON [Dimension].[Event]
(
	[ScheduledDayKey] ASC
) INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW);
GO
CREATE NONCLUSTERED INDEX [NI_Event_ResultedDayKey] ON [Dimension].[Event]
(
	[ResultedDayKey] ASC
) INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW);
GO
CREATE NONCLUSTERED INDEX [NI_Event_AbandonedDayKey] ON [Dimension].[Event]
(
	[EventAbandonedDayKey] ASC
) INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW);
GO
CREATE NONCLUSTERED INDEX [NI_Event_ClassKey] ON [Dimension].[Event]
(
	[ClassKey] ASC
) INCLUDE (EventKey) WITH (DATA_COMPRESSION = ROW);
GO
CREATE NONCLUSTERED COLUMNSTORE INDEX [NC_Event] ON [Dimension].[Event]
(
	[EventKey],
	[Source],
	[SourceEventId],
	[EventId],
	[SourceEvent],
	[SourceEventType],
	[ParentEventId],
	[ParentEvent],
	[GrandparentEventId],
	[GrandparentEvent],
	[GreatGrandparentEventId],
	[GreatGrandparentEvent],
	[SourceEventDate],
	[ClassID],
	[ClassKey],
	[SourceRaceNumber],
	[Event],
	[Round],
	[RoundSequence],
	[Competition],
	[CompetitionSeason],
	[CompetitionStartDate],
	[CompetitionEndDate],
	[Grade],
	[Distance],
	[SourcePrizePool],
	[PrizePool],
	[SourceTrackCondition],
	[SourceEventWeather],
	[TrackCondition],
	[EventWeather],
	[EventAbandoned],
	[EventAbandonedDateTime],
	[EventAbandonedDayKey],
	[LiveVideoStreamed],
	[LiveScoreBoard],
	[ScheduledDateTime],
	[ScheduledDayKey],
	[ScheduledVenueDateTime],
	[ResultedDateTime],
	[ResultedDayKey],
	[ResultedVenueDateTime],
	[SourceVenueId],
	[SourceVenue],
	[SourceVenueType],
	[SourceVenueEventType],
	[SourceVenueEventTypeName],
	[SourceVenueStateCode],
	[SourceVenueState],
	[SourceVenueCountryCode],
	[SourceVenueCountry],
	[VenueId],
	[Venue],
	[VenueType],
	[VenueStateCode],
	[VenueState],
	[VenueCountryCode],
	[VenueCountry],
	[SourceSubSportType],
	[SourceRaceGroup],
	[SourceRaceClass],
	[SourceHybridPricingTemplate],
	[SourceWeightType],
	[SubSportType],
	[RaceGroup],
	[RaceClass],
	[HybridPricingTemplate],
	[WeightType],
	[SourceDistance],
	[SourceEventAbandoned],
	[SourceLiveStreamPerformId],
	[ColumnLock],
	[FromDate],
	[FirstDate],
	[CreatedDate],
	[CreatedBatchKey],
	[CreatedBy],
	[ModifiedDate],
	[ModifiedBatchKey],
	[ModifiedBy]
)
GO

