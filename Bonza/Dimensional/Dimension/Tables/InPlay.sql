﻿CREATE TABLE [Dimension].[InPlay](
	[InPlayKey] [int] IDENTITY(1,1) NOT NULL,
	[CashoutType] [int] NOT NULL,
	[InPlay] [varchar](20) NOT NULL,
	[ClickToCall] [varchar](20) NOT NULL,
	[CashedOut] [char](3) NOT NULL,
	[StatusAtCashout] [varchar](20) NOT NULL,
	[IsDoubleDown] [bit] NOT NULL,
	[IsDoubledDown] [bit] NOT NULL,
	[DoubleDown] [varchar](20) NOT NULL, 
    [IsChaseTheAce] [bit] NOT NULL,
    [FeatureFlags] [int] NOT NULL,
	[ChaseTheAce] [char](3) NOT NULL DEFAULT 'PDG',			-- 1
	[PricePump] [char](3) NOT NULL DEFAULT 'PDG',			-- 2, 4194304
	[QuickBet] [char](3) NOT NULL DEFAULT 'PDG',			-- 4
	[ReBet] [char](3) NOT NULL DEFAULT 'PDG',				-- 8
	[NextToJump] [char](3) NOT NULL DEFAULT 'PDG',			-- 16
	[NextToGo] [char](3) NOT NULL DEFAULT 'PDG',			-- 32
	[HeroWidget] [char](3) NOT NULL DEFAULT 'PDG',			-- 64
	[FeatureTiles] [char](3) NOT NULL DEFAULT 'PDG',		-- 128
	[TrendingBet] [char](3) NOT NULL DEFAULT 'PDG',			-- 256
	[HiLoBet] [char](3) NOT NULL DEFAULT 'PDG',				-- 512
	[NextToJumpTicker] [char](3) NOT NULL DEFAULT 'PDG',	-- 1024
	[SportsHomeGrid] [char](3) NOT NULL DEFAULT 'PDG',		-- 2048
	[SportsHomeList] [char](3) NOT NULL DEFAULT 'PDG',		-- 4096
	[OddEvenBet] [char](3) NOT NULL DEFAULT 'PDG',			-- 8192
	[MysteryBet] [char](3) NOT NULL DEFAULT 'PDG',			-- 16384
	[FeaturedRaceVenue] [char](3) NOT NULL DEFAULT 'PDG',	-- 32768
	[MysteryBetPokies] [char](3) NOT NULL DEFAULT 'PDG',	-- 65536
	[FastMulti] [char](3) NOT NULL DEFAULT 'PDG',			-- 131072
	[SpinAndWin] [char](3) NOT NULL DEFAULT 'PDG',			-- 262144
	[MultiPricePump] [char](3) NOT NULL DEFAULT 'PDG',		-- 524288
	[HotStreak] [char](3) NOT NULL DEFAULT 'PDG',			-- 2097152
	[WolfsTip] [char](3) NOT NULL DEFAULT 'PDG',			-- 8388608
	[TurboTopup] [char](3) NOT NULL DEFAULT 'PDG',			-- 16777216

-- Metadata --
	[FirstDate] [datetime2](3) NOT NULL,
	[FromDate] [datetime2](3) NOT NULL,
	[ToDate] [datetime2](3) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[CreatedBy] [varchar](32) NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[ModifiedBy] [varchar](32) NOT NULL,
	[CleansedDate] [datetime2](7) NULL,
	[CleansedBatchKey] [int] NULL,
	[CleansedBy] [varchar](32) NULL,
	CONSTRAINT [CI_InPlay] PRIMARY KEY CLUSTERED ([InPlayKey] ASC) WITH (DATA_COMPRESSION = ROW)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_InPLay_NK]
    ON [Dimension].[InPlay]([InPlay] ASC, [ClickToCall] ASC, [CashoutType] ASC, [IsDoubleDown] ASC, [IsDoubledDown] ASC, [IsChaseTheAce] ASC, [FeatureFlags] ASC)
    INCLUDE([InPlayKey]);
GO
