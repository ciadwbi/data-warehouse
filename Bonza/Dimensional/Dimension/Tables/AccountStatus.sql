﻿CREATE TABLE [Dimension].[AccountStatus](
	[AccountStatusKey] [int] NOT NULL,
	[AccountKey] [int] NOT NULL,

-- Ledger --
	[Account_LedgerKey] [int] NULL, -- NEW: Temporarily allow null for rebuild
	[LedgerID] [int] NOT NULL,
	[LedgerSource] [char](3) NOT NULL,

-- Ledger Type --
	[Account_LedgerTypeKey] [int] NULL, -- NEW: Temporarily allow null for rebuild
	[LedgerTypeID] [int] NULL, -- NEW: Temporarily allow null for rebuild
	[LedgerType] [varchar](50) NULL, -- NEW: Temporarily allow null for rebuild
	[LedgerGrouping] [varchar](30) NULL, -- NEW: Temporarily allow null for rebuild
	[AccountType] [varchar](30) NULL,		-- Migrate to using Ledger Type in Warehouse view and ETL, copy data into LedgerType before removing this column

-- Ledger Status --
	[Account_LedgerStatusKey] [int] NULL, -- NEW: Temporarily allow null for rebuild
	[LedgerEnabled] [char](3) NOT NULL,
	[LedgerStatusCode] [char](1) NULL, -- NEW: Temporarily allow null for rebuild
	[LedgerStatus] [varchar](8) NOT NULL, --Migrate current single char contents to LedgerStatusCode and then replace with full text decode here
	[LedgerStatusDate] [datetime2](3) NULL, -- NEW
	[LedgerStatusReason] [varchar](50) NULL, -- NEW: Temporarily allow null for rebuild
	[LedgerStatusDescription] [varchar](1000) NULL, -- NEW
	[LedgerStatusUserKey] [int] NULL, -- NEW: Temporarily allow null for rebuild

-- Managed By
	[Account_ManagedByKey] [int] NULL, -- NEW: Temporarily allow null for rebuild
	[ManagedById] [char](32) NULL, -- NEW: Temporarily allow null for rebuild
	[ManagedBy] [varchar](50) NULL, -- NEW: Temporarily allow null for rebuild
	[BDM] [varchar](50) NULL, -- NEW: Temporarily allow null for rebuild
	[VIPCode] [int] NULL, -- NEW: Temporarily allow null for rebuild
	[VIP] [varchar](10) NOT NULL,

	[CompanyKey] [int] NOT NULL,

-- Limits
	[InternetProfile] [varchar](30) NULL,
	[CreditLimit] [money] NULL,
	[MinBetAmount] [money] NULL,
	[MaxBetAmount] [money] NULL,
	[DisableDeposit] [char](3) NOT NULL,
	[DisableWithdrawal] [char](3) NOT NULL,
	[LimitSetDate] [datetime2](3) NULL, -- NEW: Cleanse from limit Period/Timestamp
	[LimitCycleDays] [int] NULL, -- NEW: Cleanse from limit Period/Timestamp
	[DepositLimit] [money] NULL,
	[LossLimit] [money] NULL,

-- Consents
	[Account_ConsentsKey] [int] NULL, -- NEW: Temporarily allow null for rebuild
	[ReceiveMarketingEmail] [char](3) NOT NULL,
	[ReceiveCompetitionEmail] [char](3) NOT NULL,
	[ReceiveSMS] [char](3) NOT NULL,
	[ReceivePostalMail] [char](3) NOT NULL,
	[ReceiveFax] [char](3) NOT NULL,
	[PhoneColourDesc] [varchar](20) NOT NULL, -- RENAME to PhoneMoodColour

-- Reward Exclusion
	[RewardExclusion] [char](3) NOT NULL,
	[RewardExclusionDate] [datetime2](3) NULL,
	[RewardExclusionReasonId] [int] NOT NULL,
	[RewardExclusionReasonCode] [char](1) NOT NULL,
	[RewardExclusionReason] [varchar](50) NOT NULL,
	[RewardExclusionDayKey] [int] NOT NULL,

-- Advanced
	[AccountClosedByUserID] [VARCHAR](32) NOT NULL,
	[AccountClosedByUserSource] [char](3) NOT NULL,
	[AccountClosedByUserName] [varchar](100) NOT NULL,
	[AccountClosedDate] [smalldatetime] NULL,
	[ReIntroducedBy] [int] NULL,
	[ReIntroducedDate] [smalldatetime] NULL,
	[AccountClosureReason] [varchar](1000) NULL,
	[Introducer] [varchar](50) NULL,
	[Handled] [varchar](10) NULL,
	[SourceDepositLimit] [money] NULL,
	[SourceLossLimit] [money] NULL,
	[LimitPeriod] [int] NULL,
	[LimitTimeStamp] [datetime] NULL,

-- Metadata --
	[FirstDate] [datetime2](3) NULL,
	[FromDate] [datetime2](3) NOT NULL,
	[ToDate] [datetime2](3) NULL,
	[CurrentVersion] [varchar](3) NOT NULL,
	[CreatedDate] [datetime2](7) NULL,
	[CreatedBatchKey] [int] NULL,
	[CreatedBy] [varchar](32) NULL,
	[ModifiedDate] [datetime2](7) NULL,
	[ModifiedBatchKey] [int] NULL,
	[ModifiedBy] [varchar](32) NULL,
	[CleansedDate] [datetime2](7) NULL,
	[CleansedBatchKey] [int] NULL,
	[CleansedBy] [varchar](32) NULL,

	CONSTRAINT [CI_AccountStatus_PK] PRIMARY KEY CLUSTERED ([AccountStatusKey] ASC) WITH (DATA_COMPRESSION = ROW)
)
GO

CREATE UNIQUE NONCLUSTERED INDEX [NI_AccountStatus_NK]
    ON [Dimension].[AccountStatus]([LedgerId] ASC, [LedgerSource] ASC, [FromDate] ASC, [ToDate] ASC)
    INCLUDE([AccountStatusKey]);
GO

