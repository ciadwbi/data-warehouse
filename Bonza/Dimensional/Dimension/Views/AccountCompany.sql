CREATE VIEW [Dimension].[AccountCompany] WITH SCHEMABINDING AS
SELECT 
	[AccountKey],

-- Ledger --
	[Account_LedgerKey],
	[LedgerID],
	[LedgerSource],
	[LegacyLedgerID],
	[LedgerSequenceNumber],
	[LedgerOpenedDate],
	[LedgerOpenedDateOrigin],
	[LedgerOpenedDayKey],

-- Ledger Type --
	[Account_LedgerTypeKey],
	[LedgerTypeID],
	[LedgerType],
	[LedgerGroup],
	[LedgerGrouping],

-- Account --
	[Account_AccountKey],
	[AccountID],
	[AccountSource],
	[PIN],
	[UserName],
	[PasswordHash],
	[PasswordLastChanged],
	[ExactTargetID],
	[AccountFlags],

	[AccountOpenedDate],
	z.[MasterDayKey] [AccountOpenedDayKey],
	[AccountOpenedChannelKey],

	[ReferralBurnCode],
	[ReferralBurntDate],
	[ReferralAccountKey],

	[EachwayAccountId],
	[CardAccountId],
	[CardFirstIssueDate],
	[CardIssueDate],

	[VelocityNumber],
	[VelocityFirstName],
	[VelocitySurname],
	[VelocityLinkedDate],

	[RewardStatusDate],

	a.[CompanyKey],

-- Card Status
	[Account_CardStatusKey],
	[CardStatusId],
	[CardStatusCode],
	[CardStatus],

-- Card Status
	[Account_RewardStatusKey],
	[RewardStatusId],
	[RewardStatusCode],
	[RewardStatus],

-- Account Type
	[Account_AccountTypeKey],
	[AccountTypeId],
	[AccountTypeCode],
	[AccountType],

-- Managed By
	[Account_ManagedByKey],
	[ManagedById],
	[ManagedBy],
	[BDM],
	[VIPCode],
	[VIP],

-- Traffic Source
	[Account_TrafficSourceKey],
	[TrafficSourceId],
	[BTag2],
	[AffiliateID],
	[AffiliateName],
	[SiteID],
	[TrafficSource],
	[RefURL],
	[CampaignID],
	[Keywords],

--Statement
	[Account_StatementKey],
	[StatementId],
	[StatementMethod],
	[StatementFrequency],

-- Customer --
	[Account_CustomerKey],
	[CustomerID],
	[CustomerSource],
	[Title],
	[Surname],
	[FirstName],
	[MiddleName],
	[Gender],
	[DOB],
	[Phone],
	[Mobile],
	[Fax],
	[PhoneLastChanged],
	[Email],
	[EmailLastChanged],
	[Address1],
	[Address2],
	[Suburb],
	[PostCode],
	[GeographicalLocation],
	[AddressLastChanged],

-- State
	[Account_StateKey],
	[StateId],
	[StateCode],
	[State],

-- Country
	[Account_CountryKey],
	[CountryId],
	[CountryCode],
	[Country],
	[AustralianClient]

-- Company Details
		,[OrgId]
		,[DivisionId]
		,[DivisionName]
		,[BrandId]
		,[BrandCode]
		,[BrandName]
		,[BusinessUnitId]
		,[BusinessUnitCode]
		,[BusinessUnitName]
		,[CompanyID]
		,[CompanyCode]
		,[CompanyName]

-- Advanced
		,[AccountTitle]
		,[AccountSurname]
		,[AccountFirstName]
		,[AccountMiddleName]
		,[AccountGender]
		,[AccountDOB]
		,[HasHomeAddress]
		,[HomeAddressLastChanged]
		,[HomePhoneLastChanged]
		,[HomeEmailLastChanged]
		,[HomeAddress1]
		,[HomeAddress2]
		,[HomeSuburb]
		,[HomeStateCode]
		,[HomeState]
		,[HomePostCode]
		,[HomeCountryCode]
		,[HomeCountry]
		,[HomePhone]
		,[HomeMobile]
		,[HomeFax]
		,[HomeEmail]
		,[HomeAlternateEmail]
		,[HasPostalAddress]
		,[PostalAddressLastChanged]
		,[PostalPhoneLastChanged]
		,[PostalEmailLastChanged]
		,[PostalAddress1]
		,[PostalAddress2]
		,[PostalSuburb]
		,[PostalStateCode]
		,[PostalState]
		,[PostalPostCode]
		,[PostalCountryCode]
		,[PostalCountry]
		,[PostalPhone]
		,[PostalMobile]
		,[PostalFax]
		,[PostalEmail]
		,[PostalAlternateEmail]
		,[HasBusinessAddress]
		,[BusinessAddressLastChanged]
		,[BusinessPhoneLastChanged]
		,[BusinessEmailLastChanged]
		,[BusinessAddress1]
		,[BusinessAddress2]
		,[BusinessSuburb]
		,[BusinessStateCode]
		,[BusinessState]
		,[BusinessPostCode]
		,[BusinessCountryCode]
		,[BusinessCountry]
		,[BusinessPhone]
		,[BusinessMobile]
		,[BusinessFax]
		,[BusinessEmail]
		,[BusinessAlternateEmail]
		,[HasOtherAddress]
		,[OtherAddressLastChanged]
		,[OtherPhoneLastChanged]
		,[OtherEmailLastChanged]
		,[OtherAddress1]
		,[OtherAddress2]
		,[OtherSuburb]
		,[OtherStateCode]
		,[OtherState]
		,[OtherPostCode]
		,[OtherCountryCode]
		,[OtherCountry]
		,[OtherPhone]
		,[OtherMobile]
		,[OtherFax]
		,[OtherEmail]
		,[OtherAlternateEmail]

		,[SourceBTag2]
		,[SourceTrafficSource]
		,[SourceRefURL]
		,[SourceCampaignID]
		,[SourceKeywords]

-- Metadata --
		,a.[FirstDate]
		,a.[FromDate]
		,a.[ToDate]
		,a.[CreatedDate]
		,a.[CreatedBatchKey]
		,a.[CreatedBy]
		,a.[ModifiedDate]
		,a.[ModifiedBatchKey]
		,a.[ModifiedBy]
		,a.[CleansedDate]
		,a.[CleansedBatchKey]
		,a.[CleansedBy]
		,a.[Increment]
  FROM [Dimension].[Account] a
		INNER JOIN [Dimension].[Company] c ON (c.CompanyKey = a.CompanyKey)
		INNER JOIN [Dimension].[DayZone] z ON (z.DayKey = a.AccountOpenedDayKey)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_AccountCompany] ON [Dimension].[AccountCompany]
(
	[AccountKey] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [NI_AccountCompany_Company] ON [Dimension].[AccountCompany]
(
	[CompanyCode] ASC
) INCLUDE ([AccountKey], [AccountTypeCode], [LedgerGrouping]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [NI_AccountCompany_AccountId] ON [Dimension].[AccountCompany]
(
	[AccountId] ASC
) INCLUDE ([AccountKey], [LedgerId]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

CREATE NONCLUSTERED INDEX [NI_AccountCompany_AccountOpenedDayKey] ON [Dimension].[AccountCompany]
(
	[AccountOpenedDayKey] ASC
) INCLUDE ([AccountKey]) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO

