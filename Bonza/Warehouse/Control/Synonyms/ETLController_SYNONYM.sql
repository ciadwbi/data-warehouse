﻿--this is a bit of a fudge to get Warehouse to be able to use data in Staging without linking the Visual Studio projects
CREATE SYNONYM [Control].[ETLController_SYNONYM]

FOR [$(Dimensional)].[Control].[ETLController]
