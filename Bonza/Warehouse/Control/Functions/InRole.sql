﻿Create Function [Control].InRole (@RoleName varchar(100))
Returns int
as
Begin
	Declare @Return int
	Set @Return = (
					select  1
					from	sys.database_principals as Users
							left join (sys.database_role_members as Members join sys.database_principals as Roles on Members.role_principal_id = Roles.principal_id) 
										on Members.member_principal_id = Users.principal_id
					where	Users.type in ('U', 'S')
							and Roles.principal_id is not null
							and Roles.is_fixed_role = 0
							and Roles.principal_id != 0
							and Roles.name = @RoleName
							and Users.name = SUSER_NAME()
					)
	Return ISNULL(@Return,0)
End
go