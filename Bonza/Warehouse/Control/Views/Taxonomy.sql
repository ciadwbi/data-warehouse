﻿Create View [Control].Taxonomy 

as

SELECT		CASE
				When c.[Type] = 'View' Then v.Name + ' (table)'
				Else  c.Name
				End as [DisplayName]
			, e.value as [Description]
FROM		sys.schemas s
            INNER JOIN sys.views v ON (v.schema_id = s.schema_id)
            INNER JOIN	(SELECT		object_id
									, column_id
									, name
									, 'COLUMN' [Type] 
						 FROM		sys.columns
                         UNION ALL
                         SELECT		object_id
									, 0
									, ''
									, 'VIEW' 
						 FROM sys.views
                        ) c ON (c.object_id = v.object_Id)
             LEFT JOIN sys.extended_properties e ON (e.major_id = v.object_id AND e.minor_id = c.column_id)
WHERE		s.name IN ('Dimension','Fact')
			and c.Name not in ('ModifiedBatchKey')
