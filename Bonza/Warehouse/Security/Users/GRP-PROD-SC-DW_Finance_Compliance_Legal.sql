﻿CREATE LOGIN [PROD\GRP-PROD-SC-DW_Finance_Compliance_Legal] FROM WINDOWS WITH DEFAULT_DATABASE=[$(DatabaseName)], DEFAULT_LANGUAGE=[us_english]
Go
Create User [PROD\GRP-PROD-SC-DW_Finance_Compliance_Legal] For Login [PROD\GRP-PROD-SC-DW_Finance_Compliance_Legal]
GO
Grant CONNECT TO [PROD\GRP-PROD-SC-DW_Finance_Compliance_Legal]
Go
EXEC sp_addrolemember 'Finance_Compliance_Legal', [PROD\GRP-PROD-SC-DW_Finance_Compliance_Legal]
Go

