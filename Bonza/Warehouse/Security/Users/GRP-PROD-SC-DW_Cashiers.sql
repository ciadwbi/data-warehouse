﻿CREATE LOGIN [PROD\GRP-PROD-SC-DW_Cashiers] FROM WINDOWS WITH DEFAULT_DATABASE=[$(DatabaseName)], DEFAULT_LANGUAGE=[us_english]
Go
Create User [PROD\GRP-PROD-SC-DW_Cashiers] For Login [PROD\GRP-PROD-SC-DW_Cashiers]
GO
Grant CONNECT TO [PROD\GRP-PROD-SC-DW_Cashiers]
Go
EXEC sp_addrolemember 'Cashiers', [PROD\GRP-PROD-SC-DW_Cashiers]
Go

