﻿CREATE LOGIN [PROD\GRP-PROD-SC-DW_Trading_Operations] FROM WINDOWS WITH DEFAULT_DATABASE=[$(DatabaseName)], DEFAULT_LANGUAGE=[us_english]
Go
Create User [PROD\GRP-PROD-SC-DW_Trading_Operations] For Login [PROD\GRP-PROD-SC-DW_Trading_Operations]
GO
Grant CONNECT TO [PROD\GRP-PROD-SC-DW_Trading_Operations]
Go
EXEC sp_addrolemember 'Trading_Ops', [PROD\GRP-PROD-SC-DW_Trading_Operations]
Go

