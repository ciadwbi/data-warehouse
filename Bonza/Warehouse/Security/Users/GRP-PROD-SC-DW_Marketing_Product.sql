﻿CREATE LOGIN [PROD\GRP-PROD-SC-DW_Marketing_Product] FROM WINDOWS WITH DEFAULT_DATABASE=[$(DatabaseName)], DEFAULT_LANGUAGE=[us_english]
Go
Create User [PROD\GRP-PROD-SC-DW_Marketing_Product] For Login [PROD\GRP-PROD-SC-DW_Marketing_Product]
GO
Grant CONNECT TO [PROD\GRP-PROD-SC-DW_Marketing_Product]
Go
EXEC sp_addrolemember 'Marketing_Product', [PROD\GRP-PROD-SC-DW_Marketing_Product]
Go

