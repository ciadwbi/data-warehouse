﻿CREATE ROLE [Trading_Ops]
go
Grant Select on schema :: Fact to [Trading_Ops]
Go
Grant Select on schema :: Dimension to [Trading_Ops]
Go