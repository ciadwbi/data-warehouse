﻿CREATE ROLE [Cashiers]
go
Grant Select on schema :: Fact to [Cashiers]
Go
Grant Select on schema :: Dimension to [Cashiers]
Go
