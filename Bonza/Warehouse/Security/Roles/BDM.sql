﻿CREATE ROLE [BDM]
go
Grant Select on schema :: Fact to [BDM]
Go
Grant Select on schema :: Dimension to [BDM]
Go
