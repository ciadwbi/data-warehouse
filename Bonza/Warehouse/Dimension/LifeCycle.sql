﻿CREATE VIEW Dimension.Lifecycle
AS

SELECT		[LifeStageKey] AS [LifecycleKey],
			[LifeStage] AS [Lifecycle]
FROM		[$(Dimensional)].[Dimension].LifeStage with (nolock)
go

EXEC sys.sp_addextendedproperty @level1name=N'LifeCycle', @value=N'Dimension cataloguing the pre-defined stages a client can go through from sign-up to closure.' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'LifeCycleKey', @value=N'Unique internal data warehouse key for Lifecycle dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'LifeCycle', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LifeCycle', @value=N'The name of the stage in the Lifecycle as defined by marketing processes', @level2type=N'COLUMN' ,@level1name=N'LifeCycle', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
