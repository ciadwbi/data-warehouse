﻿CREATE VIEW [Dimension].[AccountStatus]
	AS SELECT 
	[AccountStatusKey]
-- Surrogate Keys --
      ,[AccountKey]
--	  ,[CompanyKey]
-- Ledger Status --
      --,[LedgerID] -- CCIA-7125 (Hide)
      --,[LedgerSource] -- CCIA-7125 (Hide)
      ,[LedgerEnabled]
      ,[LedgerStatus] AS [LedgerStatusCode] --temp rename till v15.12
      ,CASE [LedgerStatus] WHEN 'A' THEN 'Active' WHEN 'D' THEN 'Disabled' WHEN 'C' THEN 'Closed' ELSE 'Unknown' END AS [LedgerStatus] -- CCIA-7125
      ,CASE [LedgerStatus] WHEN 'C' THEN [AccountClosedDate] WHEN 'D' THEN [AccountClosedDate] WHEN 'A' THEN NULL ELSE NULL END AS [LedgerStatusDate] -- CCIA-7125
      ,CASE [LedgerStatus] WHEN 'C' THEN [AccountClosureReason] WHEN 'D' THEN [AccountClosureReason] WHEN 'A' THEN 'N/A' ELSE 'Unknown' END AS [LedgerStatusReason] -- CCIA-7125
      --,[AccountClosedByUserID] AS [LedgerStatusChangeUserCode] -- CCIA-7125 (Hide)
      --,[AccountClosedByUserSource] --CCIA-5786
      --,[AccountClosedByUserName] AS [LedgerStatusChangeUserName] -- CCIA-7125 (Hide)
      --,[ReIntroducedBy] -- CCIA-7125 (Hide)
      --,[ReIntroducedDate] -- CCIA-7125 (Hide)
-- Account Status --
      --,[AccountType] -- CCIA-7125 (Hide)
      ,[Introducer]
      ,[Handled]
      ,[InternetProfile]
      --,[VIP] -- CCIA-7125 (Hide)
      ,[PhoneColourDesc] AS [PhoneMoodColour]
      ,[CreditLimit]
      ,[MinBetAmount]
      ,[MaxBetAmount]
      ,[DisableWithdrawal]
      ,[DisableDeposit]
	  ,CASE WHEN [LimitPeriod] IS NULL THEN NULL WHEN [LimitPeriod] = -1 THEN NULL WHEN [LimitPeriod] = 0 THEN NULL ELSE [LimitTimestamp] END [LimitSetDateTime] 
	  ,CASE WHEN [LimitPeriod] IS NULL THEN NULL WHEN [LimitPeriod] = -1 THEN NULL WHEN [LimitPeriod] = 0 THEN NULL ELSE [LimitPeriod] END [LimitCycleDays]
	  ,CASE WHEN [LimitPeriod] IS NULL THEN NULL WHEN [LimitPeriod] = -1 THEN NULL WHEN [LimitPeriod] = 0 THEN NULL WHEN [DepositLimit] <= 0 THEN NULL ELSE [DepositLimit] END [DepositLimit]
	  ,CASE WHEN [LimitPeriod] IS NULL THEN NULL WHEN [LimitPeriod] = -1 THEN NULL WHEN [LimitPeriod] = 0 THEN NULL WHEN [LossLimit] <= 0 THEN NULL ELSE [LossLimit] END [LossLimit]
      ,[ReceiveMarketingEmail]
      ,[ReceiveCompetitionEmail]
      ,[ReceiveSMS]
      ,[ReceivePostalMail]
      ,[ReceiveFax]
	  ,[RewardExclusion]
	  ,[RewardExclusionDate]
	  --,[RewardExclusionReasonId] -- Internal id, hide
	  ,[RewardExclusionReasonCode]
	  ,[RewardExclusionReason]
-- Division Details --
      --,[DivisionId] -- CCIA-7148 (Hide)
      --,[DivisionName] -- Hidden until the Division/BDM can reliably be derived ETL using the new reporting structures - see Structure dimension for current implementation
-- Brand Details --
      ,[OrgId]
      --,[BrandId] -- CCIA-7148 (Hide)
      ,[BrandCode]
      ,[BrandName] AS [Brand]
-- Business Unit Details --
      --,[BusinessUnitId] -- CCIA-7148 (Hide)
      --,[BusinessUnitCode] -- Hidden until the Division/BDM can reliably be derived ETL using the new reporting structures - see Structure dimension for current implementation
      --,[BusinessUnitName] -- Hidden until the Division/BDM can reliably be derived ETL using the new reporting structures - see Structure dimension for current implementation
-- Company Details --
      --,[CompanyID] -- CCIA-7148 (Hide)
      ,[CompanyCode]
      ,[CompanyName] AS [Company]
-- Advanced Users Only --
      --,[FirstDate]
      ,[FromDate] -- Revealed to allow selection of specific type 2 version
      ,[ToDate] -- Revealed to allow selection of specific type 2 version
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
      ,[CurrentVersion]
  FROM [$(Dimensional)].[Dimension].[AccountStatusCompany] a with (nolock)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'AccountStatus', @value=N'Account dimension containing Ledger -> Account -> Customer hierarchy. Contains all versions of key time-based attributes of this hierarchy where we are interested in the value of the attributes contemporary with the fact record, as opposed to Account dimension which maintains all static or near static attributes against this same hierarchy. e.g. Name, address, account type' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'AccountStatusKey', @value=N'Unique internal data warehouse key for AccountStatus dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountKey', @value=N'Unique internal data warehouse key for Account dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'CreditLimit', @value=N'Maximum Limit on the credit allowed for a client', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'CurrentVersion', @value=N'Indicates whether the data warehouse record is the current version of the client Account or not. Ex: Yes/No. A "No" in this field indicates that the record is outdated version of the client Account and is only applicable to the timeframe selected.', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'DepositLimit', @value=N'Limit on the deposits a client can make', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'DisableDeposit', @value=N'Indicates if deposit is disabled for a client. Ex: Yes/No', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'DisableWithdrawal', @value=N'Indicates if withdrawal is disabled for a client. Ex: Yes/No', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Handled', @value=N'An indicator used by Finance to indicate if the client is being Handled. Ex: Yes/No', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'InternetProfile', @value=N'Internet Profile name which indicates the Internet factoring applicable to a client', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Introducer', @value=N'Person who introduced the client to WHA', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LedgerEnabled', @value=N'Yes/No indication of whether the ledger is currently enaled to transact or not', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
--EXEC sys.sp_addextendedproperty @level2name=N'LedgerID', @value=N'Unique identifier of each client account', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
EXEC sys.sp_addextendedproperty @level2name=N'LedgerStatus', @value=N'Indicates the Ledger Status as Closed, Disabled, Active - NOTE, this is the presentation column for this attribute and subject to change; conditions should be coded against LedgerStatusCode', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LedgerStatusCode', @value=N'Single letter representation of ledger status - C = Closed, D = Disabled or A = Active - NOTE, this is the immutable column and conditions should be coded against this rather than the LedgerStatus column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LedgerStatusDate', @value=N'Date the current LedgerStatus value came into effect', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LedgerStatusReason', @value=N'Reason for the current LedgerStatus value', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LimitCycleDays', @value=N'Number of days in each limit cycle before the deposit and loss limits are reset again, typically daily (1 day, weekly 7 days or monthly 30 days values)', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LimitSetDateTime', @value=N'When the current limits were applied', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LossLimit', @value=N'Limit on the loss that a client can accrue and continue betting', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MaxBetAmount', @value=N'Maximum Bet amount applicable to a client', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MinBetAmount', @value=N'Minimum Bet amount applicable to a client', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PhoneMoodColour', @value=N'Unique identifier to indicate the type of the client when receiving the phone calls', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ReceiveCompetitionEmail', @value=N'Indicates whether the client intends to receive CompetitionEmail', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ReceiveFax', @value=N'Indicates whether the client intends to receive Fax', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ReceiveMarketingEmail', @value=N'Indicates whether the client intends to receive MarketingEmail', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ReceivePostalMail', @value=N'Indicates whether the client intends to receive PostalMail', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ReceiveSMS', @value=N'Indicates whether the client intends to receive SMS', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'RewardExclusion', @value=N'Yes/No indicator as to whether account is currently excluded from Loyalty rewards', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'RewardExclusionDate', @value=N'Date/time when any exclusion came into force. Defaults to NULL where there is no current exclusion', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'RewardExclusionReasonCode', @value=N'Code letter indicating the reason for any exclusion - NOTE, this is the immutable column and conditions should be coded against this rather than the full description in RewardExclusionReason column which might change for presentation purposes', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'RewardExclusionReason', @value=N'Further detail regarding the reason for any exclusion, otherwise defaults to N/A if no exclusion', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FromDate', @value=N'The AccountStatus dimension holds multiple versions of data for each ledger/account as the details change over time. This FromDate identifies the date/time from which the version of the data came into being for the ledger/account - can be used in conjunction with Todate to identify the version of the AccountStatus which was current as of any point in time', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ToDate', @value=N'The AccountStatus dimension holds multiple versions of data for each ledger/account as the details change over time. This ToDate identifies the date/time up to which the version of the data was current for the ledger/account - can be used in conjunction with Fromdate to identify the version of the AccountStatus which was current as of any point in time', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'BrandCode', @value=N'Short code of brand name. Ex: TWH,CBT,WHA,RWWA', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
--EXEC sys.sp_addextendedproperty @level2name=N'BrandId', @value=N'Unique ID for Brand name', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
EXEC sys.sp_addextendedproperty @level2name=N'Brand', @value=N'TomWaterhouse,Centerbet,SportingBet,RWWA', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'CompanyCode', @value=N'Short code for company name. Ex: RWWA,WHA', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
--EXEC sys.sp_addextendedproperty @level2name=N'CompanyID', @value=N'Unique ID for company name', @level0type=N'SCHEMA',@level0name=N'Dimension', @level1type=N'VIEW',@level1name=N'Company'
--Go
EXEC sys.sp_addextendedproperty @level2name=N'Company', @value=N'Full company names like William Hill Australia for all Company Codes', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'OrgId', @value=N'Unique identifier of Brand name as in Intrabet', @level2type=N'COLUMN' ,@level1name=N'AccountStatus', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
