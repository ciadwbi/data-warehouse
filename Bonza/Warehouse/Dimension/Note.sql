﻿Create View [Dimension].[Note]

as


SELECT [NoteId] as [NoteKey] -- CCIA-7063
--	  ,[NoteSource]
--    ,[NoteId]
      ,[Notes]
--Advanced Users Only
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Note] with (nolock)
GO

EXEC sys.sp_addextendedproperty @level1name=N'Note', @value=N'Satellite Dimension used to hold ancilliary descriptive information about a fact record without bloating the fact record itself with all the additional text.' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'NoteKey', @value=N'Unique internal data warehouse key for Note dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Note', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Notes', @value=N'The body of the additional information', @level2type=N'COLUMN' ,@level1name=N'Note', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
