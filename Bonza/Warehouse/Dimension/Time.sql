﻿
Create View Dimension.[Time]

as 

SELECT [TimeKey]
-- Minute Level --
      ,TimeText AS [Time]
      ,[Time] AS [TimeValue]
      ,RIGHT(FORMAT([TimeOfHourNumber] - 1,'d10'),2) AS [Minute]
      ,[TimeOfHourNumber] AS [MinuteOfHour]
      ,[TimeOfDayNumber] AS [MinuteOfDay]
-- Hour Level --
      ,HourText AS [Hour]
      ,[HourOfDayNumber] + ISNUMERIC([HourText]) AS [HourOfDay]
-- Period Level --
	  --,[PeriodKey]
      ,[PeriodText] AS [Period]
--Advanced Users Only
      ,[TimeStartTime]
      ,[TimeEndTime]
      ,[TimeStartTimestamp]
      ,[TimeEndTimestamp]
	  ,[HourStartTime]
      ,[HourEndTime]
	  --,TimeID
	  --,[HourKey]
	  --,HourID
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Time] with (nolock)
Go

EXEC sys.sp_addextendedproperty @level1name=N'Time', @value=N'Dimension defining individual minutes of the day in the context of the position of the minute within the day and within pre-defined periods which sub-divide the day, such as peak, off-peak, etc.' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'TimeKey', @value=N'Unique internal data warehouse key for Time dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'HourEndTime', @value=N'The end time of a particular hour Ex: 14:59:59', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'HourOfDay', @value=N'Hour in a day stored in numeric format (24hr clock). Ex: 1,2', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'HourStartTime', @value=N'The start time of a particular hour Ex: 14:00:00', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Hour', @value=N'All hours from 0 to 23 in a day. Ex: 14', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TimeValue', @value=N'The time portion of a day as an offset from midnight. Ex: 14:00:00', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TimeEndTime', @value=N'The end time of the value in Time field. Ex: 14:00:59', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TimeEndTimestamp', @value=N'The complete timestamp in 24hrs including the date for the value in TimeEndTime field. Ex: 2013-10-04 14:00:59.0000000', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Minute', @value=N'The minute component of the time in text format with force leading zero etc', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MinuteOfDay', @value=N'Number of the minute from 1 to 1440 in the day in Time field. Ex: 841', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'MinuteOfHour', @value=N'Number of the minute in Time field from 1 to 60 in an hour. Ex: 1', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TimeStartTime', @value=N'The start time of the value in Time field. Ex: 14:00:00', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TimeStartTimestamp', @value=N'The complete timestamp in 24hrs including the date for the value in TimeStartTime field. Ex: 2013-10-04 14:00:00.0000000', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Time', @value=N'Time in 24hr notation. Ex:17:23', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Period', @value=N'The period of the day split into: Morning 06-12, Afternoon 12-18, Peak 18-23 and Midnight 23-06', @level2type=N'COLUMN' ,@level1name=N'Time', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
