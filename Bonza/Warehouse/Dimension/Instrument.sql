﻿CREATE View [Dimension].[Instrument]

as

SELECT	[InstrumentKey]
		--,[InstrumentID]
		--,[Source]
		,CASE 1 WHEN [FinanceComplianceLegal] THEN [AccountNumber] ELSE [SafeAccountNumber] END AS [AccountNumber]
		,CASE 1 WHEN [FinanceComplianceLegal] THEN [AccountName] ELSE '***' END AS [AccountName]
		,CASE 1 WHEN [FinanceComplianceLegal] THEN [BSB] ELSE '' END [BSB]
		,CASE 1 WHEN [FinanceComplianceLegal] THEN [Verified] ELSE '***' END [Verified]
		--,[InstrumentTypeKey] as [PaymentMethodKey]
		,[InstrumentType] as [PaymentMethod]
		--,[ProviderKey]
		,CASE 1 WHEN [FinanceComplianceLegal] THEN [ProviderName] ELSE '***' END [Provider]
--Advanced Users Only
		--,[FromDate]
		--,[ToDate]
		--,[FirstDate]
		--,[CreatedDate]
		--,[CreatedBatchKey]
		--,[CreatedBy]
		--,[ModifiedDate]
		,[ModifiedBatchKey]
		--,[ModifiedBy]
		,CASE 1 WHEN [FinanceComplianceLegal] THEN ExpiryDate ELSE null END [ExpiryDate]
		--,CASE 1 WHEN [FinanceComplianceLegal] THEN VerifyAmount ELSE null END [VerifyAmount]
FROM	[$(Dimensional)].[Dimension].[Instrument] with (nolock),
		(SELECT [control].InRole ('Finance_Compliance_Legal') AS [FinanceComplianceLegal]) x
GO

EXEC sys.sp_addextendedproperty @level1name=N'Instrument', @value=N'Dimension providing additional information on financial instruments such as credit cards and BPay which will typically consist of external account information not included as part of core Transaction or Contract information. Note that because most of the columns for this dimension contain high sensitivity data such as credit card and account numbers they are heavily masked except for users with the highest security level' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'InstrumentKey', @value=N'Unique internal data warehouse key for Instrument dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Instrument', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'AccountNumber', @value=N'Third-party account or card number to identify the financial intrument', @level2type=N'COLUMN' ,@level1name=N'Instrument', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'AccountName', @value=N'Account holder name associated with the external financial intrument', @level2type=N'COLUMN' ,@level1name=N'Instrument', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'BSB', @value=N'The BSB number identifying the holding branch of the external financial instrument, if applicable', @level2type=N'COLUMN' ,@level1name=N'Instrument', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ExpiryDate', @value=N'Any expiry date applicable to the external financial instrument, typically associated with credit cards', @level2type=N'COLUMN' ,@level1name=N'Instrument', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PaymentMethod', @value=N'The payment method used to exchange funds with the external financial instrument. Correlates with the TransactionMethod of any actual transaction with the instrument. e.g. EFT, Credit Card', @level2type=N'COLUMN' ,@level1name=N'Instrument', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Provider', @value=N'The name of the institution providing the external financial instrument - typically bank or credit card provider', @level2type=N'COLUMN' ,@level1name=N'Instrument', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Verified', @value=N'Yes/No flag indicating whether the external financial instrument details have ever been verifed', @level2type=N'COLUMN' ,@level1name=N'Instrument', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
