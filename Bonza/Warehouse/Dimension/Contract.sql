﻿create view [Dimension].[Contract] as
select [ContractKey]
-- Leg Level --
      ,[LegId]      
	  --,[LegIdSource] -- CCIA-7149 (Hide)
-- Bet Level --
      ,[BetId]      
	  --,[BetIdSource] -- CCIA-7149 (Hide)
      ,[FreeBetID]
-- Bet Group Level --
      ,[BetGroupID]   
	  --,[BetGroupIDSource] -- CCIA-7149 (Hide)
-- Transaction Level --
      ,[TransactionID]
	  --,[TransactionIDSource] -- CCIA-7149 (Hide)
      ,[RequestID]
      ,[WithdrawID]
      ,[ExternalID]
      ,[External1]
      ,[External2]
--Advanced Users Only
      --,[FirstDate]
      --,[FromDate]
      --,[ToDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].Dimension.[Contract] with (nolock)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'Contract', @value=N'Dimension containing all external bet/transaction identifiers mapped to the leg, bet, bet group (full cover bet) and grouping (permutations) hierarchy levels' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'ContractKey', @value=N'Unique internal data warehouse key for Contract dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Contract', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BetGroupID', @value=N'Unique Identifier for each BetGroupType', @level2type=N'COLUMN' ,@level1name=N'Contract', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'BetId', @value=N'Unique ID of each bet', @level2type=N'COLUMN' ,@level1name=N'Contract', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'External1', @value=N'Any additional information that is related to a contract. For Example, BankBSB number for EFT transactions.', @level2type=N'COLUMN' ,@level1name=N'Contract', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'External2', @value=N'Any additional information that is related to a contract. For Example, Account Number for EFT transactions.', @level2type=N'COLUMN' ,@level1name=N'Contract', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ExternalID', @value=N'Identifiers for any external references. Ex: Email for transactions using MoneyBooker accounts', @level2type=N'COLUMN' ,@level1name=N'Contract', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FreeBetID', @value=N'Unique value for each free bet, Null if cash bet', @level2type=N'COLUMN' ,@level1name=N'Contract', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'LegId', @value=N'Unique ID of each leg in a bet', @level2type=N'COLUMN' ,@level1name=N'Contract', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'RequestID', @value=N'Unique identifier for each Withdrawal request transaction created in Intrabet', @level2type=N'COLUMN' ,@level1name=N'Contract', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TransactionID', @value=N'Uniques identifier of each transaction', @level2type=N'COLUMN' ,@level1name=N'Contract', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'WithdrawID', @value=N'Unique identifier for each Withdrawal transaction created in Intrabet', @level2type=N'COLUMN' ,@level1name=N'Contract', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go

