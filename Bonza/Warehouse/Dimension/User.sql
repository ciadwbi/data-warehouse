﻿CREATE View [Dimension].[User]

as

SELECT [UserKey]
      ,[UserId]
	  --,[Source]
      --,[SystemName]
      --,[EmployeeKey]
      ,[Forename] AS UserForename
      ,[Surname] AS UserSurname
      --,[PayrollNumber] CCIA-4477
--For Advanced Users
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[User] with (nolock)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'User', @value=N'Dimension holding all WHA employees who might be linked to facts for audit and accountability purposes.' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'UserKey', @value=N'Unique internal data warehouse key for User dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'User', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'UserId', @value=N'Unique external identifer for the employee', @level2type=N'COLUMN' ,@level1name=N'User', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'UserSurname', @value=N'Surname of the employee', @level2type=N'COLUMN' ,@level1name=N'User', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'UserForename', @value=N'Forename of the employee', @level2type=N'COLUMN' ,@level1name=N'User', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
