﻿Create view Dimension.Communication AS
SELECT [CommunicationKey]
      ,[CommunicationId]
      --,[CommunicationSource] -- CCIA-7155 (Hide)
      ,[Title]
      ,[Details]
      ,[SubDetails]
--Advanced Users Only
      --,[CreatedDate] -- CCIA-7155 (Hide)
      --,[CreatedBatchKey] -- CCIA-7155 (Hide)
      --,[CreatedBy] -- CCIA-7155 (Hide)
      --,[ModifiedDate] -- CCIA-7155 (Hide)
      ,[ModifiedBatchKey]
      --,[ModifiedBy] -- CCIA-7155 (Hide)
  FROM [$(Dimensional)].[Dimension].[Communication] WITH (NOLOCK)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'Communication', @value=N'Dimension defining particular communications such as a mass email or SMS send' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'CommunicationKey', @value=N'Unique internal data warehouse key for Communication dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Communication', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'CommunicationId', @value=N'Communication Id as specified from source. for ET it is Job Id', @level2type=N'COLUMN' ,@level1name=N'Communication', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
--EXEC sys.sp_addextendedproperty @level2name=N'CommunicationSource', @value=N'Source of Communication', @level2type=N'COLUMN' ,@level1name=N'Communication', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
EXEC sys.sp_addextendedproperty @level2name=N'Title', @value=N'Title/Name of the Communication', @level2type=N'COLUMN' ,@level1name=N'Communication', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Details', @value=N'Details/content of the Communication', @level2type=N'COLUMN' ,@level1name=N'Communication', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'SubDetails', @value=N'Level down details of the Communication if any', @level2type=N'COLUMN' ,@level1name=N'Communication', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'