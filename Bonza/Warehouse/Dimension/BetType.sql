﻿CREATE VIEW [Dimension].[BetType]

AS 

SELECT [BetTypeKey]
-- Leg Level --
      ,[LegType] as PredictionType --CCIA-5789
-- Bet Level --
      ,[BetType]
      ,[BetSubType]
-- Bet Group Level (i.e. permutations of multi-bets such as Goliath) --
      ,[BetGroupType]
-- Group Bet Level (i.e. permutations of selections) --
      ,[GroupingType]
--Advanced Users
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[BetType] with (nolock)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'BetType', @value=N'Dimension containing a logical classification of the type of bet mapped to its implications at the leg, bet, bet group (full cover bet) and grouping (permutations) levels' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'BetTypeKey', @value=N'Unique internal data warehouse key for BetType dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'BetType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BetGroupType', @value=N'Indicates if a bet is a full cover bet or not. This is the highest level in the hierarchy of various bet type classifications. Ex: Straight, Goliath, yankee,trixee etc.', @level2type=N'COLUMN' ,@level1name=N'BetType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'BetSubType', @value=N'Indicates the number of legs or type of bet as in double, trble, trifecta, quinella etc. This is the third level in the hierarchy of various bet type classifications.', @level2type=N'COLUMN' ,@level1name=N'BetType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'BetType', @value=N'The type of the bet indicating whether the bet is a Single,exotic or multi. This is the second level in the hierarchy of various bet type classifications.', @level2type=N'COLUMN' ,@level1name=N'BetType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'GroupingType', @value=N'Indicates how the bets are grouped. Ex: Boxed, multiple,straight etc.', @level2type=N'COLUMN' ,@level1name=N'BetType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PredictionType', @value=N'Indicates how the outcome of an event is predicted. Ex: Each way,forecast,win,place', @level2type=N'COLUMN' ,@level1name=N'BetType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go