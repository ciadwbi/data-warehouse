﻿CREATE VIEW [Dimension].[RevenueLifecycle]

AS

SELECT		[RevenueLifecycleKey]
			,[RevenueLifecycle]
FROM		[$(Dimensional)].[Dimension].[RevenueLifecycle] with (nolock)
Go

EXEC sys.sp_addextendedproperty @level1name=N'RevenueLifeCycle', @value=N'Dimension covering an older form of client Lifecycle modelling based on monitoring time since last bet was settled.' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'RevenueLifeCycleKey', @value=N'Unique internal data warehouse key for RevenueLifecycle dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'RevenueLifeCycle', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'RevenueLifeCycle', @value=N'The name of the stage in the RevenueLifecycle as defined by commercial management processes', @level2type=N'COLUMN' ,@level1name=N'RevenueLifeCycle', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
