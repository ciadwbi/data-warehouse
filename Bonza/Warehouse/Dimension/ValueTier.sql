﻿CREATE VIEW [Dimension].[ValueTier]

as

Select		[TierKey] AS [ValueTierKey]
			,[TierNumber]
			,[TierName] AS [Tier]
			--,[FromDate]
			--,[ToDate]
From		[$(Dimensional)].Dimension.ValueTiers with (nolock)
GO

EXEC sys.sp_addextendedproperty @level1name=N'ValueTier', @value=N'Dimension defining the value tier levels that clients can be assigned to depending upon their value/profitability.' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'ValueTierKey', @value=N'Unique internal data warehouse key for ValueTier dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'ValueTier', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TierNumber', @value=N'The relative level of the value tier', @level2type=N'COLUMN' ,@level1name=N'ValueTier', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Tier', @value=N'The name of the value tier level', @level2type=N'COLUMN' ,@level1name=N'ValueTier', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
