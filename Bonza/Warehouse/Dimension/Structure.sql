﻿CREATE VIEW Dimension.Structure

AS

SELECT  [StructureKey]
		, [Level0]
		, [Level1]
		, [Level2]
		, [BDM]
		, [VIPCode]
		, [VIP]
		, [PotentialVIP]
		,c.[DivisionName] AS [Division]
		,c.[BrandCode]
		,c.[BrandName] AS [Brand]
		,c.[BusinessUnitCode]
		,c.[BusinessUnitName] AS [BusinessUnit]
		,c.[CompanyCode]
		,c.[CompanyName] AS [Company]
		, [PreviousLevel0] as [PreviousYearLevel0]
		, [PreviousLevel1] as [PreviousYearLevel1]
		, [PreviousLevel2] as [PreviousYearLevel2]
		, [PreviousBDM]
		, [PreviousVIPCode]
		, [PreviousVIP]
		,p.[DivisionName] AS [PreviousDivision]
		,p.[BrandCode] AS [PreviousBrandCode]
		,p.[BrandName] AS [PreviousBrand]
		,p.[BusinessUnitCode] AS [PreviousBusinessUnitCode]
		,p.[BusinessUnitName] AS [PreviousBusinessUnit]
		,p.[CompanyCode] AS [PreviousCompanyCode]
		,p.[CompanyName] AS [PreviousCompany]
		, [FY2017Level0]
		, [FY2017Level1]
		, [FY2017Level2]
		, [FY2017BDM]
		, [FY2017VIPCode]
		, [FY2017VIP]
		,f2017.[DivisionName] AS [FY2017Division]
		,f2017.[BrandCode] AS [FY2017BrandCode]
		,f2017.[BrandName] AS [FY2017Brand]
		,f2017.[BusinessUnitCode] AS [FY2017BusinessUnitCode]
		,f2017.[BusinessUnitName] AS [FY2017BusinessUnit]
		,f2017.[CompanyCode] AS [FY2017CompanyCode]
		,f2017.[CompanyName] AS [FY2017Company]
		, [FY2016Level0]
		, [FY2016Level1]
		, [FY2016Level2]
		, [FY2016BDM]
		, [FY2016VIPCode]
		, [FY2016VIP]
		,f2016.[DivisionName] AS [FY2016Division]
		,f2016.[BrandCode] AS [FY2016BrandCode]
		,f2016.[BrandName] AS [FY2016Brand]
		,f2016.[BusinessUnitCode] AS [FY2016BusinessUnitCode]
		,f2016.[BusinessUnitName] AS [FY2016BusinessUnit]
		,f2016.[CompanyCode] AS [FY2016CompanyCode]
		,f2016.[CompanyName] AS [FY2016Company]
		, [FY2015Level0]
		, [FY2015Level1]
		, [FY2015Level2]
		, [FY2015BDM]
		, [FY2015VIPCode]
		, [FY2015VIP]
		,f2015.[DivisionName] AS [FY2015Division]
		,f2015.[BrandCode] AS [FY2015BrandCode]
		,f2015.[BrandName] AS [FY2015Brand]
		,f2015.[BusinessUnitCode] AS [FY2015BusinessUnitCode]
		,f2015.[BusinessUnitName] AS [FY2015BusinessUnit]
		,f2015.[CompanyCode] AS [FY2015CompanyCode]
		,f2015.[CompanyName] AS [FY2015Company]
		, [FY2014Level0]
		, [FY2014Level1]
		, [FY2014Level2]
		, [FY2014BDM]
		, [FY2014VIPCode]
		, [FY2014VIP]
		,f2014.[DivisionName] AS [FY2014Division]
		,f2014.[BrandCode] AS [FY2014BrandCode]
		,f2014.[BrandName] AS [FY2014Brand]
		,f2014.[BusinessUnitCode] AS [FY2014BusinessUnitCode]
		,f2014.[BusinessUnitName] AS [FY2014BusinessUnit]
		,f2014.[CompanyCode] AS [FY2014CompanyCode]
		,f2014.[CompanyName] AS [FY2014Company]
      --,[FirstDate]
      --,[FromDate]
      --,[ToDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
        , s.[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[Structure] s WITH (NOLOCK)
		INNER JOIN [$(Dimensional)].[Dimension].[Company] c ON (c.CompanyKey = s.CompanyKey)
		INNER JOIN [$(Dimensional)].[Dimension].[Company] p ON (p.CompanyKey = s.PreviousCompanyKey)
		INNER JOIN [$(Dimensional)].[Dimension].[Company] f2017 ON (f2017.CompanyKey = s.FY2017CompanyKey)
		INNER JOIN [$(Dimensional)].[Dimension].[Company] f2016 ON (f2016.CompanyKey = s.FY2016CompanyKey)
		INNER JOIN [$(Dimensional)].[Dimension].[Company] f2015 ON (f2015.CompanyKey = s.FY2015CompanyKey)
		INNER JOIN [$(Dimensional)].[Dimension].[Company] f2014 ON (f2014.CompanyKey = s.FY2014CompanyKey)
  GO

  

EXEC sys.sp_addextendedproperty @level1name=N'Structure', @value=N'Dimension dedicated to classifying facts according to the current and historic reporting structures. Now that reporting structure is not just mapped to accounts (because of distinct In-Play bet metrics in Level2) a dedicated structure dimension is required attached directly to the fact records rather than a satellite grouping of the Account dimension.' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'StructureKey', @value=N'Unique internal data warehouse key for Structure dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Level0', @value=N'Level 0 allocation in current reporting structure at this point YTD for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Level1', @value=N'Level 1 allocation in current reporting structure at this point YTD for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Level2', @value=N'Level 2 allocation in current reporting structure at this point YTD for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BDM', @value=N'Name of the current BDM responsible for client otherwise shows N/A.', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'VIPCode', @value=N'Number of the current VIP group to which client belongs. Zero indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'VIP', @value=N'Name of the current VIP group to which client belongs. N/A indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PotentialVIP', @value=N'Yes / No flag to indicate whether the client has been marked as a potential VIP client in Intrabet', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Division', @value=N'Name of the current division in the company. Ex: Digital,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'BrandCode', @value=N'Short code of current brand. Ex: TWH,CBT,WHA,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Brand', @value=N'Full name of current brand. Ex: TomWaterhouse,Centerbet,SportingBet,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'BusinessUnitCode', @value=N'Short code of current business unit. Ex: TWH,SBA,CBT,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'BusinessUnit', @value=N'Full Name of the current business unit. Ex: Tom Waterhouse', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'CompanyCode', @value=N'Short code for current company name. Ex: RWWA,WHA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'Company', @value=N'Full current company names like William Hill Australia for all Company Codes', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go

EXEC sys.sp_addextendedproperty @level2name=N'PreviousYearLevel0', @value=N'Level 0 allocation in reporting structure as it stood at close of previous fiscal year for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PreviousYearLevel1', @value=N'Level 1 allocation in reporting structure as it stood at close of previous fiscal year for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PreviousYearLevel2', @value=N'Level 2 allocation in reporting structure as it stood at close of previous fiscal year for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PreviousBDM', @value=N'Name of the BDM responsible for client at the end of the previous fiscal year otherwise shows N/A.', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PreviousVIPCode', @value=N'Number of the VIP group to which client belonged at the end of the previous fiscal year. Zero indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PreviousVIP', @value=N'Name of the VIP group to which client belonged at the end of the previous fiscal year. N/A indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PreviousDivision', @value=N'Name of the previous division in the company. Ex: Digital,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PreviousBrandCode', @value=N'Short code of previous brand. Ex: TWH,CBT,WHA,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PreviousBrand', @value=N'Full name of previous brand. Ex: TomWaterhouse,Centerbet,SportingBet,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PreviousBusinessUnitCode', @value=N'Short code of previous business unit. Ex: TWH,SBA,CBT,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PreviousBusinessUnit', @value=N'Full Name of the previous business unit. Ex: Tom Waterhouse', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PreviousCompanyCode', @value=N'Short code for previous company name. Ex: RWWA,WHA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PreviousCompany', @value=N'Full previous company names like William Hill Australia for all Company Codes', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go

EXEC sys.sp_addextendedproperty @level2name=N'FY2017BDM', @value=N'Name of the BDM responsible for client at the end of the 2017 fiscal year otherwise shows N/A.', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2017VIPCode', @value=N'Number of the VIP group to which client belonged at the end of the 2017 fiscal year. Zero indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2017VIP', @value=N'Name of the VIP group to which client belonged at the end of the 2017 fiscal year. N/A indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2017Division', @value=N'Name of the FY2017 division in the company. Ex: Digital,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2017BrandCode', @value=N'Short code of FY2017 brand. Ex: TWH,CBT,WHA,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2017Brand', @value=N'Full name of FY2017 brand. Ex: TomWaterhouse,Centerbet,SportingBet,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2017BusinessUnitCode', @value=N'Short code of FY2017 business unit. Ex: TWH,SBA,CBT,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2017BusinessUnit', @value=N'Full Name of the FY2017 business unit. Ex: Tom Waterhouse', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2017CompanyCode', @value=N'Short code for FY2017 company name. Ex: RWWA,WHA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2017Company', @value=N'Full FY2017 company names like William Hill Australia for all Company Codes', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go

EXEC sys.sp_addextendedproperty @level2name=N'FY2016Level0', @value=N'Level 0 allocation in reporting structure at this point YTD up to the close of 2016 fiscal year for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'FY2016Level1', @value=N'Level 1 allocation in reporting structure at this point YTD up to the close of 2016 fiscal year for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'FY2016Level2', @value=N'Level 2 allocation in reporting structure at this point YTD up to the close of 2016 fiscal year for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'FY2016BDM', @value=N'Name of the BDM responsible for client at the end of the 2016 fiscal year otherwise shows N/A.', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2016VIPCode', @value=N'Number of the VIP group to which client belonged at the end of the 2016 fiscal year. Zero indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2016VIP', @value=N'Name of the VIP group to which client belonged at the end of the 2016 fiscal year. N/A indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2016Division', @value=N'Name of the FY2016 division in the company. Ex: Digital,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2016BrandCode', @value=N'Short code of FY2016 brand. Ex: TWH,CBT,WHA,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2016Brand', @value=N'Full name of FY2016 brand. Ex: TomWaterhouse,Centerbet,SportingBet,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2016BusinessUnitCode', @value=N'Short code of FY2016 business unit. Ex: TWH,SBA,CBT,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2016BusinessUnit', @value=N'Full Name of the FY2016 business unit. Ex: Tom Waterhouse', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2016CompanyCode', @value=N'Short code for FY2016 company name. Ex: RWWA,WHA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2016Company', @value=N'Full FY2016 company names like William Hill Australia for all Company Codes', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go

EXEC sys.sp_addextendedproperty @level2name=N'FY2015Level0', @value=N'Level 0 allocation in reporting structure as it stood at close of 2015 fiscal year with all TWH accounts in their pre-migrated state for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'FY2015Level1', @value=N'Level 1 allocation in reporting structure as it stood at close of 2015 fiscal year with all TWH accounts in their pre-migrated state for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'FY2015Level2', @value=N'Level 2 allocation in reporting structure as it stood at close of 2015 fiscal year with all TWH accounts in their pre-migrated state for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'FY2015BDM', @value=N'Name of the BDM responsible for client at the end of the 2015 fiscal year otherwise shows N/A.', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2015VIPCode', @value=N'Number of the VIP group to which client belonged at the end of the 2015 fiscal year. Zero indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2015VIP', @value=N'Name of the VIP group to which client belonged at the end of the 2015 fiscal year. N/A indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2015Division', @value=N'Name of the FY2015 division in the company. Ex: Digital,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2015BrandCode', @value=N'Short code of FY2015 brand. Ex: TWH,CBT,WHA,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2015Brand', @value=N'Full name of FY2015 brand. Ex: TomWaterhouse,Centerbet,SportingBet,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2015BusinessUnitCode', @value=N'Short code of FY2015 business unit. Ex: TWH,SBA,CBT,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2015BusinessUnit', @value=N'Full Name of the FY2015 business unit. Ex: Tom Waterhouse', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2015CompanyCode', @value=N'Short code for FY2015 company name. Ex: RWWA,WHA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2015Company', @value=N'Full FY2015 company names like William Hill Australia for all Company Codes', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go

EXEC sys.sp_addextendedproperty @level2name=N'FY2014Level0', @value=N'Level 0 allocation in reporting structure as it stood at close of 2014 fiscal year for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'FY2014Level1', @value=N'Level 1 allocation in reporting structure as it stood at close of 2014 fiscal year for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'FY2014Level2', @value=N'Level 2 allocation in reporting structure as it stood at close of 2014 fiscal year for all facts linked to this dimension', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'FY2014BDM', @value=N'Name of the BDM responsible for client at the end of the 2014 fiscal year otherwise shows N/A.', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2014VIPCode', @value=N'Number of the VIP group to which client belonged at the end of the 2014 fiscal year. Zero indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2014VIP', @value=N'Name of the VIP group to which client belonged at the end of the 2014 fiscal year. N/A indicates not VIP', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2014Division', @value=N'Name of the FY2014 division in the company. Ex: Digital,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2014BrandCode', @value=N'Short code of FY2014 brand. Ex: TWH,CBT,WHA,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2014Brand', @value=N'Full name of FY2014 brand. Ex: TomWaterhouse,Centerbet,SportingBet,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2014BusinessUnitCode', @value=N'Short code of FY2014 business unit. Ex: TWH,SBA,CBT,BDM,RWWA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2014BusinessUnit', @value=N'Full Name of the FY2014 business unit. Ex: Tom Waterhouse', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2014CompanyCode', @value=N'Short code for FY2014 company name. Ex: RWWA,WHA', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FY2014Company', @value=N'Full FY2014 company names like William Hill Australia for all Company Codes', @level2type=N'COLUMN' ,@level1name=N'Structure', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go


