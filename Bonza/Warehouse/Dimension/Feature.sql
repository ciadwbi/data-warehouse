﻿CREATE VIEW Dimension.Feature
AS

SELECT [InPlayKey] [FeatureKey]
      ,CASE [InPlay] WHEN 'Y' THEN 'Yes' WHEN 'N' THEN 'No' ELSE [InPlay] END AS [InPlay]
      ,CASE [ClickToCall] WHEN 'Y' THEN 'Yes' WHEN 'N' THEN 'No' ELSE [ClickToCall] END AS [ClickToCall]
      ,[CashedOut]
      ,[StatusAtCashout]
	  ,[DoubleDown]
	  ,[ChaseTheAce]
	  ,[PricePump]
	  ,[QuickBet]
	  ,[ReBet]
	  ,[NextToJump]
	  ,[NextToGo]
	  ,[HeroWidget]
	  ,[FeatureTiles]
	  ,[TrendingBet]
	  ,[HiLoBet]
	  ,[NextToJumpTicker]
	  ,[SportsHomeGrid]
	  ,[SportsHomeList]
	  ,[OddEvenBet]
  	  ,[MysteryBet]
	  ,[FeaturedRaceVenue]
	  ,[MysteryBetPokies]
	  ,[FastMulti]
	  ,[SpinAndWin]
	  ,[MultiPricePump]
	  ,[HotStreak]
	  ,[WolfsTip]
	  ,[TurboTopup]

  FROM [$(Dimensional)].[Dimension].[InPlay] with (nolock)
GO

EXEC sys.sp_addextendedproperty @level1name=N'Feature', @value=N'Dimension defining bets as In-Play or not and if In-Play whether Click-to-call was used as the mechanism for placing the bet' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'FeatureKey', @value=N'Unique internal data warehouse key for In-Play dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'InPlay', @value=N'Yes/No flag to indicate In-Play or not', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ClickToCall', @value=N'Yes/No flag to indicate Click-To-Call or not', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'CashedOut', @value=N'Yes/No flag to indicate whether or not the bet was cashed out', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'StatusAtCashout', @value=N'Status of the bet or individual leg at the time the bet was cashed out, i.e. Pre-game, In-Play or Settled (the latter only applicable to multis where some legs had already settled at the time of cashout', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'DoubleDown', @value=N'Documents any DoubleDown feature linked to bet, i.e. ''Initial'' for an iniital bet placed with a double down option, ''DoubledDown'' for an additional bet placed exploiting a double down option or ''N/A'' indicating no links to double down', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ChaseTheAce', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'PricePump', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'QuickBet', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ReBet', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'NextToJump', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'NextToGo', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'HeroWidget', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'FeatureTiles', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'TrendingBet', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'HiLoBet', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'NextToJumpTicker', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'SportsHomeGrid', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'SportsHomeList', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'OddEvenBet', @value=N'Yes/No flag to indicate whether this feature was present', @level2type=N'COLUMN' ,@level1name=N'Feature', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
