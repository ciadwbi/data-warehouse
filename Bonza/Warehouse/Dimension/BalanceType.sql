﻿Create view [Dimension].[BalanceType]
AS
SELECT [BalanceTypeKey]
      ,[BalanceTypeID] AS [BalanceTypeCode] -- CCIA-7138 -- Note, we may ultimately want to instantiate 2 versions of this columns on the record BalanceTypeId as the natural key and BalanceTypeCode to standardise with other columns on other records as the Imuttable for conditional logic
      ,Case	
		When [BalanceTypeName] = 'Hold' then 'Unsettled'
		Else [BalanceTypeName]
		End AS [BalanceType] -- CCIA-7138 & CCIA-7320
--Advanced Users
      --,[FromDate]
      --,[ToDate]
      --,[FirstDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[Dimension].[BalanceType] with (nolock)
  go

EXEC sys.sp_addextendedproperty @level1name=N'BalanceType', @value=N'Catalogue of financial balance points along the cashflow model at which the flow of cash into and out of the balance can be analysed. e.g. Client, Unsettled, Pending Withdrawal, etc.' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BalanceTypeCode', @value=N'Natural key of Balance Type Dimension - pre-defined character short code. NOTE this is the imuttable value which should be used for and BalanceType specific conditions in preference to BalanceType which can change for presentation purposes and BalanceTypeKey which is not guaranteed consistent between reloads', @level2type=N'COLUMN' ,@level1name=N'BalanceType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BalanceType', @value=N'Name of the balance on the cashflow model. Ex: Hold, Intercept, Profit & Loss - NOTE, this is the presentation column for this attribute and subject to change; conditions should be coded against BalanceTypeCode', @level2type=N'COLUMN' ,@level1name=N'BalanceType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'BalanceTypeKey', @value=N'Unique internal data warehouse key for BalanceType dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'BalanceType', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

