﻿CREATE VIEW [Dimension].[Event]
AS

SELECT [EventKey] 
-- Surrogate Keys --
	  ,[ClassKey]    
      ,s.[MasterDayKey] [ScheduledDayKey]
     --,s.[AnalysisDayKey] [SydneyScheduledDayKey]
	  ,r.[MasterDayKey] [ResultedDayKey]
      --,r.[AnalysisDayKey] [SydneyResultedDayKey]
-- Event Level --
      ,[EventId]
	  --,[SourceEventID]
	  --,[Source]
	  --,[ClassID]
      ,[Event]
      ,[ScheduledDateTime]
      --,[ScheduledVenueDateTime]
      ,[ResultedDateTime]
      --,[ResultedVenueDateTime]
      ,[Grade]
      ,[Distance]
      ,[PrizePool]
	  ,[EventAbandoned]
	  ,[EventAbandonedDateTime]
	  ,[EventAbandonedDayKey]
      ,[TrackCondition]
      ,[EventWeather]
	  ,[LiveVideoStreamed]
	  ,[LiveScoreBoard]
-- Round Level --
      ,[Round]
      ,[RoundSequence]
-- Competition Level --
      ,[Competition]
      ,[CompetitionSeason]
      ,[CompetitionStartDate]
      ,[CompetitionEndDate]
-- Venue Level --
      ,[Venue]
      ,[VenueType]
-- Venue State Level --
      ,[VenueStateCode]
      ,[VenueState]
-- Venue Country Level --
      ,[VenueCountryCode]
      ,[VenueCountry]
--Advanced Users
      --,[ColumnLock] this field is only used to identify manual amendments have taken place
      --,[FromDate]
	  --,[FirstDate]
      --,[ToDate]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      ,e.[ModifiedBatchKey]
      --,[ModifiedBy]
      ,[SourceEvent]
      ,[ParentEventId]
      ,[ParentEvent]
      ,[GrandparentEventId]
      ,[GrandparentEvent]
      ,[GreatGrandparentEventId]
      ,[GreatGrandparentEvent]
      ,[SourceEventDate]
      ,[VenueId]
	  --,[SourceVenueId]
      ,[SourceVenue]
	  ,[SourceVenueType]
	  ,[SourceVenueEventType]	
	  ,[SourceVenueEventTypeName]	
	  ,[SourceVenueStateCode]
	  ,[SourceVenueState]
	  ,[SourceVenueCountryCode]
	  ,[SourceVenueCountry]
      ,[SubSportType]
	  ,[RaceGroup]
	  ,[RaceClass]
	  ,[HybridPricingTemplate]
	  ,[WeightType]
	  ,[SourceDistance]
	  --,[SourceEventAbandoned]
	  --,[SourceRaceNumber]
	  --,[SourceLiveStreamPerformId]
	  --,[SourcePrizePool]
	  --,[SourceSubSportType]
	  --,[SourceRaceGroup]
	  --,[SourceRaceClass]
	  --,[SourceHybridPricingTemplate]
	  --,[SourceWeightType]
	  --,[SourceTrackCondition] Can be enabled if there is a business need
      --,[SourceEventWeather] Can be enabled if there is a business need
	  --,[SourceEventType] Can be enabled if there is a business need

FROM  [$(Dimensional)].[Dimension].[Event] e  WITH (NOLOCK)
		LEFT JOIN [$(Dimensional)].[Dimension].[DayZone] s WITH (NOLOCK) ON (s.DayKey = e.ScheduledDayKey)
		LEFT JOIN [$(Dimensional)].[Dimension].[DayZone] r WITH (NOLOCK) ON (r.DayKey = e.ResultedDayKey)
GO

EXEC sys.sp_addextendedproperty @level1name=N'Event', @value=N'Dimension documenting an Event to be bet on. This dimension purely covers details of the sporting event devoid of any betting information, which is all contained in the Market dimension' ,@level1type=N'VIEW' ,@level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'EventKey', @value=N'Unique internal data warehouse key for Event dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
EXEC sys.sp_addextendedproperty @level2name=N'ClassKey', @value=N'Link to the Class dimension which categorises the event by sport or racing type', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
Go
--EXEC sys.sp_addextendedproperty @level2name=N'ClassID', @value=N'Unique identifier for the class of the event', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--Go
EXEC sys.sp_addextendedproperty @level2name=N'ScheduledDayKey', @value=N'Link to the Day dimension for the date on which the event is/was scheduled to occur on Darwin time', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
--EXEC sys.sp_addextendedproperty @level2name=N'SydneyScheduledDayKey', @value=N'Link to the Day dimension for the date on which the event is/was scheduled to occur in Sydney time', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
EXEC sys.sp_addextendedproperty @level2name=N'ResultedDayKey', @value=N'Link to the Day dimension for the date on which the event was resulted on Darwin time', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
--EXEC sys.sp_addextendedproperty @level2name=N'SydneyResultedDayKey', @value=N'Link to the Day dimension for the date on which the event was resulted on Sydney time', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
EXEC sys.sp_addextendedproperty @level2name=N'EventId', @value=N'Unique numeric Event record identifer - provided for linkage back to originating source system', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Competition', @value=N'Name/description of any overall competition the vent is part of, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'CompetitionEndDate', @value=N'Where a competition takes place over a period of one or more days, the last day of the competition, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'CompetitionSeason', @value=N'Where a competition repeats seasonally, the specific season of the competition the event is part of, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'CompetitionStartDate', @value=N'Where a competition takes place over a period of one or more days, the first day of the competition, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Distance', @value=N'The distance over which the event is competed, more generally relevant to racing, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Event', @value=N'Name/description of the event, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Grade', @value=N'The grade or level of the event, more generally relevant to racing, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'PrizePool', @value=N'The total pool of prize money to be paid out to winners and runners-up as an indication of the importance of the event, more generally relevant to racing, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ResultedDateTime', @value=N'Date/time at which the results of the event were announced (or last announced in the case of changed results)', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Round', @value=N'Where a competition is broken down into a series of rounds, the round the event belongs to, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'RoundSequence', @value=N'A logical sequence number used to indicate the chronological order of distinct rounds if it is not readily ascertained from the Round for sorting purposes, e.g. Round 1, Round 2, Semi-Final and Final would be given RoundSequences 1, 2, 3 and 4 respectively. Extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ScheduledDateTime', @value=N'Date/time at which the event is scheduled to commence', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SourceEvent', @value=N'Verbatim copy of the Event data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SourceEventDate', @value=N'Verbatim copy of the EventDate data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'GrandparentEvent', @value=N'Verbatim copy of the Grandparent Event data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'GrandparentEventId', @value=N'Unique external identifier of the Grandparent Event data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'GreatGrandparentEvent', @value=N'Verbatim copy of the Great Grandparent Event data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'GreatGrandparentEventId', @value=N'Unique external identifier of the Great Grandparent Event data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ParentEvent', @value=N'Verbatim copy of the Parent Event data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ParentEventId', @value=N'Unique external identifier of the Parent Event data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SubSportType', @value=N'Verbatim copy of the SubSportType data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SourceVenue', @value=N'Verbatim copy of the Venue data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SourceVenueCountry', @value=N'Verbatim copy of the VenueCountry data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SourceVenueCountryCode', @value=N'Verbatim copy of the VenueCountryCode data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'VenueId', @value=N'Unique external identifier of the Venue, data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SourceVenueEventType', @value=N'The event type intended for the Venue, data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SourceVenueEventTypeName', @value=N'The name of the event type intended for the Venue, data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SourceVenueState', @value=N'Verbatim copy of the VenueState data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SourceVenueStateCode', @value=N'Verbatim copy of the VenueStateCode data obtained from source system; to facilitate comparison and validation of the extracted and cleansed data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'Venue', @value=N'The venue name/description at which the event is held, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'VenueCountry', @value=N'The country of the venue at which the event is held, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'VenueCountryCode', @value=N'Three character ISO code for the country of the venue at which the event is held, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'VenueState', @value=N'The australian state of the venue at which the event is held, extracted and cleansed from available source data. If the venue is outside Australia the value will be Outside Australia', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'VenueStateCode', @value=N'The standard Australian state code for the venue at which the event is held, extracted and cleansed from available source data. If the venue is outside Australia the value will be N/A', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'VenueType', @value=N'Classifies the venue as Metro, Provincial or Country for racing events only, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SourceVenueType', @value=N'Classifies the venue as Metro, Provincial or Country for racing events only, extracted and cleansed from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
--EXEC sys.sp_addextendedproperty @level2name=N'SourceEventAbandoned', @value=N'Verbatim copy of the flag (1/ 0/ NULL) to indicate whether the event is abandoned or not', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
EXEC sys.sp_addextendedproperty @level2name=N'RaceGroup', @value=N'Verbatim copy of the Race grouping of an event from source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'EventAbandoned', @value=N'Yes /No flag to indicate whether the event is abandoned or not', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'RaceClass', @value=N'Verbatim copy of the Race class/ grade of an event from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'HybridPricingTemplate', @value=N'Verbatim copy of the pricing template of an event from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'WeightType', @value=N'Verbatim copy of the weight type of a racing event from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SourceDistance', @value=N'Verbatim copy of the track distance for a racing event from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
--EXEC sys.sp_addextendedproperty @level2name=N'SourceVenueTrack', @value=N'Verbatim copy of the track condition of an event venue from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
--EXEC sys.sp_addextendedproperty @level2name=N'SourceVenueWeather', @value=N'Verbatim copy of the weather condition at the event venue from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
EXEC sys.sp_addextendedproperty @level2name=N'TrackCondition', @value=N'Cleaned up verison of the track condition of an event venue', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'EventWeather', @value=N'Cleaned up verison of the weather condition at the event venue', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
--EXEC sys.sp_addextendedproperty @level2name=N'SourceRaceNumber', @value=N'Verbatim copy of the Race number of an event from available source data', @level2type=N'COLUMN' ,@level1name=N'Event', @level1type=N'VIEW', @level0name=N'Dimension', @level0type=N'SCHEMA', @name=N'MS_Description'
--GO
