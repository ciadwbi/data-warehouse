﻿CREATE VIEW [SnowFlake].[Channel_Channel]
AS
SELECT	DISTINCT 
		ChannelId,
		ChannelId Channel
FROM	SnowFlake.Channel_ChannelPlatform