﻿CREATE VIEW [SnowFlake].[Channel_ChannelPlatform]
AS
SELECT	DISTINCT 
		s.ChannelPlatformId,
		s.ChannelPlatformId ChannelPlatform,
		CASE  
			WHEN ChannelName = 'Exact Target' THEN ChannelTypeName 
			WHEN ChannelName = 'Web' THEN 'Internet' 
			WHEN ChannelName = 'APIWS' THEN 'API' 
			WHEN ChannelName = 'CMS' THEN 'Phone' 
			WHEN ChannelName = 'Telebet' THEN 'Phone' 
			WHEN ChannelName = 'Betback' THEN 'N/A' 
			WHEN ChannelName = 'TAB' THEN 'N/A' 
			WHEN ChannelName = 'TAB_Hedging' THEN 'N/A' 
			WHEN ChannelName = 'Brokering' THEN 'N/A' 
			WHEN ChannelName = 'Concierge' THEN 'Phone' 
			WHEN ChannelName = 'Admin' THEN 'N/A' 
			WHEN ChannelName = 'QuickSlip' THEN 'Internet' 
			ELSE ChannelName 
		END ChannelId
FROM	[$(Dimensional)].Dimension.Channel d INNER JOIN SnowFlake.Channel_ChannelDetail s ON s.ChannelDetailId = d.ChannelDescription