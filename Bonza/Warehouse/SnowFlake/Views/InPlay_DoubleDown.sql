﻿CREATE VIEW SnowFlake.InPlay_DoubleDown
AS
Select	Distinct
		IsDoubleDown IsDoubleDownId,
		CASE WHEN IsDoubleDown = 0 THEN 'No' ELSE 'Yes' END IsDoubleDown
From [$(Dimensional)].Dimension.InPlay