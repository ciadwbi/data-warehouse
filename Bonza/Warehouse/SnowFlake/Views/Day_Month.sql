﻿



CREATE VIEW [SnowFlake].[Day_Month]
AS
SELECT DISTINCT case when MonthOfYearNumber<10 then cast(concat(yearnumber,'0',MonthOfYearNumber)as int)
 else cast(concat(yearnumber,MonthOfYearNumber)as int) end MonthId
, MonthName MonthDesc
, MonthStartDate
, MonthEndDate
, MonthOfYearText MonthOfYear
, MonthOfYearNumber
, MonthDays
, cast(concat(yearnumber,QuarterOfYearNumber)as int) QuarterId
, YearNumber YearId 
, cast(convert(varchar(10),dateadd(mm,-1,daydate),112) as int)/100 as LastMonthId
, cast(convert(varchar(10),dateadd(mm,-1,daydate),112) as int)/100 as LastYearSameMonthId
FROM [$(Dimensional)].Dimension.Day
  where daykey>=20000101