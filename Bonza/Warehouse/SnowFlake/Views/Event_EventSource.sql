﻿





CREATE VIEW [SnowFlake].[Event_EventSource]
AS

Select	EventKey,
		EventId SourceEventId,
		CONVERT(Varchar(20),EventId) + '-' + CONVERT(Varchar(20),ClassId) as EventId
From [$(Dimensional)].[Dimension].[Event]