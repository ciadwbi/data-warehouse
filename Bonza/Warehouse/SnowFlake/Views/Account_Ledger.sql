﻿CREATE VIEW [SnowFlake].[Account_Ledger]
AS
--SELECT	AccountKey,
--		LedgerID,
--		LegacyLedgerID,
--		LedgerSequenceNumber,
--		AccountId,
--		LedgerTypeID,
--		CASE WHEN LedgerSequenceNumber = 1 AND AccountTypeCode = 'C' AND	LedgerGrouping = 'Transactional' AND CompanyKey <> 100 THEN 'Y' ELSE 'N' END ClientAccount
--FROM	Dimensional.Dimension.Account

--SELECT [AccountKey]
--      ,[LedgerID]
--      ,[LegacyLedgerID]
--      ,[LedgerSequenceNumber]
--      ,[AccountId]
--      ,[LedgerTypeID]
--      ,[ClientAccount]
--  FROM [$(Dimensional)].[SnowFlake].[Account_Ledger]

SELECT	a.[AccountKey],
		l.[Account_LedgerKey],
		l.[LedgerID],
		l.[LegacyLedgerID],
		l.[LedgerSequenceNumber],
		l.[AccountId],
		l.[LedgerTypeID],
		CASE 
			WHEN l.LedgerSequenceNumber = 1 AND l.AccountTypeCode = 'C' AND	l.LedgerGrouping = 'Transactional' AND l.CompanyKey <> 100 THEN 'Y' 
			ELSE 'N' 
		END ClientAccount
  FROM	[$(Dimensional)].[SnowFlake].[Account_Ledger] l
		JOIN [$(Dimensional)].[Dimension].[Account] a ON a.Account_LedgerKey = l.Account_LedgerKey -- join to Account for now to prevent Microstrategy breaking on AccountKey