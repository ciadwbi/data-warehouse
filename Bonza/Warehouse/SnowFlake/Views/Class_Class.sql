﻿
CREATE VIEW [SnowFlake].[Class_Class]
AS
SELECT DISTINCT ClassCode + CONVERT(Varchar,SuperclassKey) ClassId, ClassCode, ClassName Class, SuperclassKey SuperclassId FROM [$(Dimensional)].Dimension.Class