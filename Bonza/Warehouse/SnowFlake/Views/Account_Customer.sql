﻿

CREATE VIEW [SnowFlake].[Account_Customer]
AS
SELECT	DISTINCT
		CustomerID,
		Title,
		FirstName,
		MiddleName,
		Surname,
		DOB,
		Age,
		Gender,
		Address1,
		Address2,
		Suburb,
		PostCode,
		Phone,
		Mobile,
		Fax,
		Email,
		CASE WHEN Country = 'CHANNEL ISLANDS' THEN 'CI2' ELSE CountryCode END + StateCode StateId,
		AddressLastChanged,
		EmailLastChanged,
		PhoneLastChanged
FROM	[$(Dimensional)].Dimension.Account