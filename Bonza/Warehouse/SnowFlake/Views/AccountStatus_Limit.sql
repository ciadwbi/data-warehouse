﻿CREATE VIEW [Snowflake].[AccountStatus_Limit]
AS 
SELECT	[AccountStatusKey] LimitId,
		[CreditLimit],
		[MinBetAmount],
		[MaxBetAmount],
		[DisableWithdrawal],
		[DisableDeposit],
		CASE WHEN [LimitPeriod] IS NULL THEN NULL WHEN [LimitPeriod] = -1 THEN NULL WHEN [LimitPeriod] = 0 THEN NULL ELSE [LimitTimestamp] END [LimitSetDateTime],
		CASE WHEN [LimitPeriod] IS NULL THEN NULL WHEN [LimitPeriod] = -1 THEN NULL WHEN [LimitPeriod] = 0 THEN NULL ELSE [LimitPeriod] END [LimitCycleDays],
		CASE WHEN [LimitPeriod] IS NULL THEN NULL WHEN [LimitPeriod] = -1 THEN NULL WHEN [LimitPeriod] = 0 THEN NULL WHEN [DepositLimit] <= 0 THEN NULL ELSE [DepositLimit] END [DepositLimit],
		CASE WHEN [LimitPeriod] IS NULL THEN NULL WHEN [LimitPeriod] = -1 THEN NULL WHEN [LimitPeriod] = 0 THEN NULL WHEN [LossLimit] <= 0 THEN NULL ELSE [LossLimit] END [LossLimit]
FROM	[$(Dimensional)].[Dimension].[AccountStatus]