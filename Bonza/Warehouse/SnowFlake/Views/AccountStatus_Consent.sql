﻿CREATE VIEW [Snowflake].[AccountStatus_Consent]
AS 
SELECT	DISTINCT
		[ReceiveMarketingEmail]+[ReceiveCompetitionEmail]+[ReceiveSMS]+[ReceivePostalMail]+[ReceiveFax] ConsentId,
		[ReceiveMarketingEmail],
		[ReceiveCompetitionEmail],
		[ReceiveSMS],
		[ReceivePostalMail],
		[ReceiveFax]
FROM	[$(Dimensional)].[Dimension].[AccountStatus]