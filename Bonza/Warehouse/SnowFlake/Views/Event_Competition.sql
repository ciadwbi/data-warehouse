﻿

CREATE VIEW [SnowFlake].[Event_Competition]
AS
--SELECT	DISTINCT
--		CASE
--			WHEN CompetitionName IS NULL or CompetitionName = '' THEN 'Unknown' + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,c.ClassId)
--			ELSE CompetitionName + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,c.ClassId)
--		END AS CompetitionID,
--		CASE
--			WHEN CompetitionName IS NULL or CompetitionName = '' THEN 'Unknown'
--			ELSE CompetitionName
--		END AS Competition,
--		CASE 
--			WHEN CompetitionSeason IS NULL THEN 'Unknown'
--			ELSE CompetitionSeason
--		END AS CompetitionSeason,
--		CASE
--			WHEN CompetitionStartDate IS NULL THEN '1900-01-01'
--			ELSE CompetitionStartDate
--		END AS CompetitionStartDate,
--		CASE
--			WHEN CompetitionEndDate IS NULL THEN '1900-01-01'
--			ELSE CompetitionEndDate
--		END AS CompetitionEndDate,
--		c.ClassId SubClassId
--FROM Dimensional.[Dimension].[Event] e
--		INNER JOIN Dimensional.[Dimension].[Class] c ON c.ClassKey = ISNULL(e.ClassKey, -1)

Select * From [$(Dimensional)].[SnowFlake].[Event_Competition]