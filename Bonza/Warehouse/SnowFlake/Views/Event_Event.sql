﻿






CREATE VIEW [SnowFlake].[Event_Event]
AS
--Select	EventId,
--		IsNull([EventName], SourceEventName) as [Event],
--		ScheduledDateTime as EventDateTime,
--		Grade,
--		Distance,
--		PrizePool,
--		ScheduledDayKey/100 as EventDayKey,
--		CASE
--			WHEN CompetitionName IS NULL or CompetitionName = '' THEN 'Unknown' + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,c.ClassId) + (CASE WHEN RoundName IS NULL OR RoundName = 'Unknown' THEN 'UNK' ELSE RoundName END)
--			ELSE CompetitionName + CONVERT(Varchar,IsNull(CompetitionStartDate,'1900-01-01')) + CONVERT(Varchar,IsNull(CompetitionEndDate,'1900-01-01')) + CONVERT(Varchar,c.ClassId) + (CASE WHEN RoundName IS NULL OR RoundName = 'Unknown' THEN 'UNK' ELSE RoundName END)
--		END AS RoundID,
--		CASE 
--			WHEN CountryCode IS NULL THEN CONVERT(Varchar,IsNull(SourceVenueId,-1)) +  StateCode + 'XX' + (CASE WHEN VenueDescription IS NULL THEN 'UNK' ELSE VenueDescription END)
--			ELSE CONVERT(Varchar,IsNull(SourceVenueId,-1)) + StateCode + CountryCode + (CASE WHEN VenueDescription IS NULL THEN 'UNK' ELSE VenueDescription END)
--		END	AS VenueId,
--		c.ClassId SubClassId
--FROM Dimensional.[Dimension].[Event] e
--		INNER JOIN Dimensional.[Dimension].[Class] c ON c.ClassKey = ISNULL(e.ClassKey, -1)

Select * From [$(Dimensional)].[SnowFlake].[Event_Event]