﻿CREATE VIEW [SnowFlake].[TransactionDetail_TransactionStatus]
AS
SELECT	DISTINCT
		[TransactionStatusCode]+[TransactionTypeCode] TransactionStatusId,
		[TransactionStatusCode],
		[TransactionStatus],
		[TransactionTypeCode] TransactionTypeId
FROM	[Dimension].[TransactionDetail]