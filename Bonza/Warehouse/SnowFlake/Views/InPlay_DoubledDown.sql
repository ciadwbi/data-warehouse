﻿CREATE VIEW SnowFlake.InPlay_DoubledDown
AS
Select	Distinct
		IsDoubledDown IsDoubledDownId,
		CASE WHEN IsDoubledDown = 0 THEN 'No' ELSE 'Yes' END IsDoubledDown
From [$(Dimensional)].Dimension.InPlay