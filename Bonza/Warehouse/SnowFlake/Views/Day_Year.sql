﻿


CREATE VIEW [SnowFlake].[Day_Year]
AS
SELECT DISTINCT YearNumber YearId, YearNumber, YearStartDate, YearEndDate, YearDays
, YearNumber-1 as LastYearId FROM [$(Dimensional)].Dimension.Day
  where daykey>=20000101