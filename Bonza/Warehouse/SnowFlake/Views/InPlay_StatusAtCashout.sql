﻿CREATE VIEW [SnowFlake].[InPlay_StatusAtCashout]
AS
SELECT DISTINCT CashoutType StatusAtCashoutId, CashoutType, StatusAtCashout, CashedOut CashedOutId  FROM [$(Dimensional)].Dimension.InPlay