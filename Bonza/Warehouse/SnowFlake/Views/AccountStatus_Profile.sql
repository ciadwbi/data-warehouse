﻿CREATE VIEW [Snowflake].[AccountStatus_Profile]
AS 
SELECT	DISTINCT
		[InternetProfile]+[PhoneColourDesc] ProfileId,
		[InternetProfile],
		[PhoneColourDesc] AS [PhoneMoodColour]
FROM	[$(Dimensional)].[Dimension].[AccountStatus]