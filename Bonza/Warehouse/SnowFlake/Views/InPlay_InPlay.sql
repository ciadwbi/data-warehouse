﻿
CREATE VIEW [SnowFlake].[InPlay_InPlay]
AS
SELECT	DISTINCT
		InPlayId, 
		CASE InPLayId WHEN 'Y' THEN 'Yes' WHEN 'N' THEN 'No' ELSE InPLayId END InPlay
FROM Snowflake.InPlay_ClickToCall