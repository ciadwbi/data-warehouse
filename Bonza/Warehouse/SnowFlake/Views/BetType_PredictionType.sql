﻿CREATE VIEW [SnowFlake].[BetType_PredictionType]
AS
SELECT DISTINCT LegType PredictionTypeId, LegType PredictionType FROM [$(Dimensional)].Dimension.BetType