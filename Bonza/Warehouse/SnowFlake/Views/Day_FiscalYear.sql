﻿

CREATE VIEW [SnowFlake].[Day_FiscalYear]
AS
SELECT DISTINCT FiscalYearNumber FiscalYearId
, FiscalYearName FiscalYear
, FiscalYearNumber
, FiscalYearStartDate
, FiscalYearEndDate
, FiscalYearDays 
, FiscalYearNumber-1 LastFiscalYearId
FROM [$(Dimensional)].Dimension.Day
  where daykey>=20000101