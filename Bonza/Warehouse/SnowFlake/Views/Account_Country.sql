﻿
CREATE VIEW [SnowFlake].[Account_Country]
AS
SELECT	DISTINCT
		CASE WHEN Country = 'CHANNEL ISLANDS' THEN 'CI2' ELSE CountryCode END AS CountryId,
		CountryCode, 
		Country
FROM	[$(Dimensional)].Dimension.account