﻿CREATE VIEW [SnowFlake].[TransactionDetail_TransactionType]
AS
SELECT	DISTINCT
		[TransactionTypeCode] TransactionTypeId,
		[TransactionTypeCode],
		[TransactionType]
FROM	[Dimension].[TransactionDetail]