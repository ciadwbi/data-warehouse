﻿CREATE view [UAT].[KPI] as
SELECT [AccountKey]
      ,[AccountStatusKey]
      ,[ContractKey]
      ,[InstrumentKey]
      ,[StructureKey]
      ,[BetTypeKey]
      ,[LegTypeKey]
      ,[ChannelKey]
	  ,[ActionChannelKey]
	  ,[InPlayKey]
	  ,[InPlayKey] [FeatureKey]
      ,[ClassKey]
      ,[EventKey]
      ,[MarketKey]
      ,[CampaignKey]
      ,[UserKey]
      ,z.[MasterDayKey] [DayKey]
      ,z.[AnalysisDayKey] [SydneyDayKey]
      ,[MasterTimeKey] [TimeKey]
--      ,[AnalysisTimeKey] [SydneyTimeKey]
	  ,[MasterTimestamp] [Timestamp]
--	  ,[AnalysisTimestamp]
      ,[AccountsOpened] --CCIA=5219
      ,[FirstTimeDepositors] --CCIA=5219
	  ,[FirstTimeBettors] --CCIA=5219
	  ,[DepositsRequestedCount]
      ,[DepositsCompletedCount]
      ,[DepositsRequested]
      ,[DepositsCompleted]
      ,[WithdrawalsRequestedCount]
      ,[WithdrawalsCompletedCount]
      ,[WithdrawalsRequested]
      ,[WithdrawalsCompleted]
      ,[Transfers]
      ,[Promotions]
      ,[BetsPlaced]
      ,[BonusBetsPlaced]
      ,[ContractsPlaced]
      ,[BonusContractsPlaced]
      ,[BettorsPlaced]
      ,[BonusBettorsPlaced]
      ,[PlayDaysPlaced]
      ,[BonusPlayDaysPlaced]
      ,[PlayFiscalWeeksPlaced]
      ,[BonusPlayFiscalWeeksPlaced]
      ,[PlayWeeksPlaced]
      ,[BonusPlayWeeksPlaced]
      ,[PlayFiscalMonthsPlaced]
      ,[BonusPlayFiscalMonthsPlaced]
      ,[PlayMonthsPlaced]
      ,[BonusPlayMonthsPlaced]
      ,-[TurnoverPlaced] [TurnoverPlaced]--CCIA=5219
      ,[BonusTurnoverPlaced]
      ,[Payout]--CCIA=5219
      ,[BettorsCashedOut]
	  ,[BetsCashedOut]
      ,[Cashout]
	  ,[CashoutDifferential]
	  ,[Fees]
      ,[BetsSettled] --CCIA=5219
      ,[BonusBetsSettled]
      ,[ContractsSettled]
      ,[BonusContractsSettled]
      ,[BettorsSettled] --CCIA=5219
      ,[BonusBettorsSettled]
      ,[PlayDaysSettled]
      ,[BonusPlayDaysSettled]
      ,[PlayFiscalWeeksSettled]
      ,[BonusPlayFiscalWeeksSettled]
      ,[PlayWeeksSettled]
      ,[BonusPlayWeeksSettled]
      ,[PlayFiscalMonthsSettled]
      ,[BonusPlayFiscalMonthsSettled]
      ,[PlayMonthsSettled]
      ,[BonusPlayMonthsSettled]
      ,[TurnoverSettled]--CCIA=5219
      ,[BonusTurnoverSettled]
      ,[GrossWin]
      ,[GrosswinResettledAdjustment] --CCIA=5219
      ,[BonusGrosswin] --CCIA=5219
      ,[Adjustments]
      ,[NetRevenue]
      ,[NetRevenueAdjustment]
      ,[BetbackGrosswin]--CCIA=5219
      --,[CreatedDate]
      ,k.[CreatedBatchKey]
      --,[CreatedBy]
      --,[ModifiedDate]
      --,[ModifiedBatchKey]
      --,[ModifiedBy]
  FROM [$(Dimensional)].[CC].[KPI] k WITH (NOLOCK)
		LEFT JOIN [$(Dimensional)].[CC].[Day] z WITH (NOLOCK) ON (z.DayKey = k.DayKey)



GO

