﻿CREATE VIEW [Fact].[Reward]
AS
SELECT [ContractKey]
      ,[MasterTransactionTimestamp]
      ,[AnalysisTransactionTimestamp]
      ,z.MasterDayKey [DayKey]
      ,[MasterTimeKey]
      ,[AnalysisTimeKey]
      ,[AccountKey]
      ,[AccountStatusKey]
      ,[StructureKey]
      ,[BetTypeKey]
      ,[LegTypeKey]
      ,[PriceTypeKey]
      ,[RewardDetailKey]
      ,[ChannelKey]
      ,[InPlayKey]
      ,[PromotionKey]
      ,[MarketKey]
      ,[EventKey]
      ,[ClassKey]
      ,[Points]
      ,[VelocityPoints]
      ,[BonusCredit]
      --,[FromDate]
      --,[MappingId]
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[CreatedBy]
  FROM [$(Dimensional)].[Fact].[Reward] a
  LEFT JOIN [$(Dimensional)].[CC].[Day] z WITH (NOLOCK) ON (z.DayKey = a.DayKey)
GO

