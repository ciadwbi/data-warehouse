﻿CREATE VIEW [Fact].[ClosingPosition] AS
SELECT [AccountKey]
      ,[AccountStatusKey]
      ,[StructureKey]
      ,d.[ClosingMasterDayKey] [DayKey]
      ,d.[ClosingAnalysisDayKey] [SydneyDayKey]
      ,ISNULL(fa.[MasterDayKey],0) [FirstAccrualDayKey]
      ,ISNULL(fa.[AnalysisDayKey],0) [FirstAccrualSydneyDayKey]
      ,[FirstAccrualBetId]
      ,ISNULL(fb.[MasterDayKey],0) [FirstBonusRedemptionDayKey]
      ,ISNULL(fb.[AnalysisDayKey],0) [FirstBonusRedemptionSydneyDayKey]
      ,ISNULL(fv.[MasterDayKey],0) [FirstVelocityRedemptionDayKey]
      ,ISNULL(fv.[AnalysisDayKey],0) [FirstVelocityRedemptionSydneyDayKey]
      ,ISNULL(la.[MasterDayKey],0) [LastAccrualDayKey]
      ,ISNULL(la.[AnalysisDayKey],0) [LastAccrualSydneyDayKey]
      ,[LastAccrualBetId]
      ,ISNULL(lb.[MasterDayKey],0) [LastBonusRedemptionDayKey]
      ,ISNULL(lb.[AnalysisDayKey],0) [LastBonusRedemptionSydneyDayKey]
      ,ISNULL(lv.[MasterDayKey],0) [LastVelocityRedemptionDayKey]
      ,ISNULL(lv.[AnalysisDayKey],0) [LastVelocityRedemptionSydneyDayKey]
      ,[PointsBalance]
      ,[LifetimePoints]
      ,[AccrualBalance]
      ,[NumberOfAccruals]
      ,[LifetimeAccrual]
      ,[NumberOfBonusRedemptions]
      ,[LifetimeBonusRedemption]
      ,[NumberOfVelocityRedemptions]
      ,[LifetimeVelocityRedemption]
      ,[LifetimeRewardCredit]
      ,[LifetimeRewardDebit]
      ,p.[CreatedDate]
      ,p.[CreatedBatchKey]
      ,p.[CreatedBy]
      ,p.[ModifiedDate]
      ,p.[ModifiedBatchKey]
      ,p.[ModifiedBy]
  FROM [$(Dimensional)].cc.[Position] p WITH (NOLOCK)
		LEFT JOIN [$(Dimensional)].[CC].[Day] d WITH (NOLOCK) ON d.DayKey = p.DayKey
		LEFT JOIN [$(Dimensional)].[CC].[Day] fa WITH (NOLOCK) ON fa.DayKey = p.FirstAccrualDayKey
		LEFT JOIN [$(Dimensional)].[CC].[Day] fb WITH (NOLOCK) ON fb.DayKey = p.FirstBonusRedemptionDayKey
		LEFT JOIN [$(Dimensional)].[CC].[Day] fv WITH (NOLOCK) ON fv.DayKey = p.FirstVelocityRedemptionDayKey
		LEFT JOIN [$(Dimensional)].[CC].[Day] la WITH (NOLOCK) ON la.DayKey = p.LastAccrualDayKey
		LEFT JOIN [$(Dimensional)].[CC].[Day] lb WITH (NOLOCK) ON lb.DayKey = p.LastBonusRedemptionDayKey
		LEFT JOIN [$(Dimensional)].[CC].[Day] lv WITH (NOLOCK) ON lv.DayKey = p.LastVelocityRedemptionDayKey
