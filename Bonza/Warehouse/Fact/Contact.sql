﻿Create view Fact.Contact AS
SELECT [AccountKey]
      ,[AccountStatusKey]
      --,[CommunicationId] --CCIA-7266
      ,[CommunicationKey]
      --,[StoryKey] --CCIA-7264
	  ,[ContactDate]
      ,[ChannelKey]
      --,[PromotionKey] --CCIA-7264
      --,[BetTypeKey] --CCIA-7264
      --,[ClassKey] --CCIA-7264
      --,[CommunicationCompanyKey] --CCIA-7263
      --,[EventKey] --CCIA-7264
      ,[InteractionTypeKey]
      ,i.[MasterDayKey] as DayKey
      ,i.[AnalysisDayKey] as SydneyDayKey
      , InteractionTimeKeyMaster as TimeKey --CCIA-6170
	  , InteractionTimeKeyAnalysis as SydneyTimeKey
      --,[Details]
-- Advanced Users Only --
      --,[CreatedDate]
      --,[CreatedBatchKey]
      --,[ModifiedDate]
      ,c.[ModifiedBatchKey]
  FROM [$(Dimensional)].[Fact].[Contact] c WITH (NOLOCK)
		INNER JOIN [$(Dimensional)].[Dimension].[DayZone] i WITH (NOLOCK) ON (i.DayKey = c.InteractionDayKey)
  Go

EXEC sys.sp_addextendedproperty @level1name=N'Contact', @value=N'Fact table defining all interactions or attempted interactions with the client, such as sent email/sms, bounced email, click-through, etc. Grain = 1 record per interaction' ,@level1type=N'VIEW' ,@level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO

EXEC sys.sp_addextendedproperty @level2name=N'AccountKey', @value=N'Unique internal data warehouse key for Account dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Contact', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'AccountStatusKey', @value=N'Unique internal data warehouse key for AccountStatus dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Contact', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'CommunicationKey', @value=N'Unique internal data warehouse key for Communication dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Contact', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ChannelKey', @value=N'Unique internal data warehouse key for Channel dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Contact', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'InteractionTypeKey', @value=N'Unique internal data warehouse key for InteractionType dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Contact', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'ContactDate', @value=N'The timestamp of when the contact was made in Darwin timezone', @level2type=N'COLUMN' ,@level1name=N'Contact', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'DayKey', @value=N'Unique internal data warehouse key for Day dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Contact', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SydneyDayKey', @value=N'Unique internal data warehouse key aliasing Day dimension to provide metrics based on days defined by Sydney time as opposed to the default of Darwin time - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Contact', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'TimeKey', @value=N'Unique internal data warehouse key for Time dimension - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Contact', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
EXEC sys.sp_addextendedproperty @level2name=N'SydneyTimeKey', @value=N'Unique internal data warehouse key aliasing Time dimension to provide metrics based on Sydney time as opposed to the default of Darwin time - only to be used for joining fact and dimension tables', @level2type=N'COLUMN' ,@level1name=N'Contact', @level1type=N'VIEW', @level0name=N'Fact', @level0type=N'SCHEMA', @name=N'MS_Description'
GO
