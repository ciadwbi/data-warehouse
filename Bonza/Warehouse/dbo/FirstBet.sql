﻿CREATE TABLE [dbo].[FirstBet](
	[AccountKey] [int] NOT NULL,
	[BetID] [bigint] NULL,
	[FirstBetDayKey] [int] NOT NULL,
	[FirstBetTypeKey] [smallint] NULL,
	[FirstBetClassKey] [smallint] NULL,
	[FirstBetChannelKey] [int] NULL,
 CONSTRAINT [PK_FirstBet] PRIMARY KEY CLUSTERED 
(
	[AccountKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


