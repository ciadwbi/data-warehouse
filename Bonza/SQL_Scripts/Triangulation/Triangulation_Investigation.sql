DECLARE @FromDate DATETIME2(3) = '2015-04-06';
DECLARE @ToDate DATETIME2(3) = '2015-04-07';
DECLARE @AccountID VARCHAR(10) = '300174';

SELECT 
	CASE 
		WHEN BrandCode = 'Unk' AND SourceAccountId NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'Tote' 
		ELSE BrandCode 
	END BrandCode, 
    BalanceTypeName Balance,
    SourceAccountId AccountId,
    ClientId, 
    DayDate,
    NULL SourceOpeningBalance, 
    SUM(TransactedAmount) as TransactedAmount,
	W.WalletName
FROM Dimension.day d with (nolock)
	inner join dimension.dayzone dz with (nolock) on d.DayKey = dz.MasterDayKey
	inner join Fact.[Transaction] t with (nolock) on (t.DayKey = dz.DayKey)
    inner join Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = t.BalanceTypeKey)
    inner join Dimension.wallet w with (nolock) on (w.WalletKey = t.WalletKey)
    inner join Dimension.account a with (nolock) on (a.AccountKey = t.AccountKey)
    inner join Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey)
where DayDate BETWEEN @FromDate AND @FromDate
    and BalanceTypeName IN ('Client')--,'Hold')
    --and w.WalletName IN ('Cash', 'N/A')
    and a.LedgerName NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
    and a.AccountTypeName = 'Client'
	and t.AccountID = @AccountID
GROUP BY
	CASE 
		WHEN BrandCode = 'Unk' AND SourceAccountId NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'Tote' 
		ELSE BrandCode 
	END, 
    BalanceTypeName,
    SourceAccountId,
    ClientId, 
    DayDate,
	W.WalletName;

SELECT 
	CASE 
		WHEN BrandCode = 'Unk' AND SourceAccountId NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'Tote' 
		ELSE BrandCode 
	END BrandCode, 
    BalanceTypeName Balance,
    SourceAccountId AccountId,
    ClientId, 
    DayDate,
    OpeningBalance as [SourceOpeningBalance], 
    NULL TransactedAmount
FROM Dimension.day d with (nolock)
	inner join dimension.dayzone dz with (nolock) on d.DayKey = dz.MasterDayKey
	inner join Fact.[Position] t with (nolock) on (t.DayKey = dz.DayKey)
    inner join Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = t.BalanceTypeKey)
    inner join Dimension.account a with (nolock) on (a.AccountKey = t.AccountKey)
    inner join Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey)
where DayDate BETWEEN @FromDate AND @ToDate
    and BalanceTypeName IN ('Client')--,'Hold')
    and a.LedgerName NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
    and a.AccountTypeName = 'Client'
	and t.AccountID = @AccountID;

SELECT 
	CASE 
		WHEN (BrandCode = 'Unk' OR BrandCode IS NULL) AND SourceAccountId NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'Tote' 
		ELSE BrandCode 
	END as BrandCode, 
	BalanceTypeName as Balance,
	A.SourceAccountId as AccountId,
	a.ClientId as ClientID, 
	t.DayDate,
	ClientBalance as SourceOpeningBalance, 
	NULL as TransactedAmount,
	t.FreeBet
FROM [AJ_Staging].[Atomic].[OpeningBalance] t with (nolock)
	inner join [AJ_Staging].[Atomic].[Balance] as ab with (nolock) on t.AccountId = ab.AccountID and t.BatchKey = ab.BatchKey
	inner join Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = 2)
	inner join (SELECT
					AccountID,
					SourceAccountId,
					ClientID,
					CompanyKey,
					LedgerName,
					AccountTypeName,
					ToDate
				FROM Dimension.Account with (nolock)
				WHERE ToDate > @FromDate
				AND FromDate <= @FromDate
				) as a on (a.SourceAccountID = t.AccountID)
	left join Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey and c.orgid = ab.OrgID)
WHERE T.DayDate BETWEEN @FromDate AND @ToDate
--AND B.BalanceName IN ('Client')--,'Hold')
and a.LedgerName NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
AND A.AccountTypeName = 'Client'
--and t.FreeBet = 0
and t.AccountID = @AccountID;