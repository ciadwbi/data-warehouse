﻿CREATE FUNCTION dbo.SplitWithRowNum
(
	@origString varchar(max),
	@Delimiter char(1),
	@CountEmpty bit
)
returns @temptable TABLE
(
	rowNum int,
	items varchar(max)
)
as
begin
	declare @rowNum int = 0
    declare @idx int
    declare @split varchar(max)

    select @idx = 1
        if len(@origString) < 1 or @origString is null return

    while @idx != 0
    begin
        set @idx = charindex(@Delimiter, @origString)
        if @idx != 0
            set @split = left(@origString, @idx - 1)
        else
            set @split = @origString

        if (len(@split) > 0 or isnull(@CountEmpty,0) = 1)
			begin
	            insert into @temptable(rowNum, Items) values(@rowNum, @split)
				set @rowNum = @rowNum + 1
			END

        set @origString = right(@origString, len(@origString) - @idx)
        if len(@origString) = 0 break
    end
return
end
