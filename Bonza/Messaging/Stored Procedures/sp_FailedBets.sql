﻿CREATE PROCEDURE [Messaging].[sp_FailedBets]
AS

BEGIN

/* **********************
 *
 *		Failed Bets
 *
 * *********************/
declare @FromDate datetime2(3) = coalesce((select max(FromDate) from [Messaging].[BetFailed] with(nolock)), '1900-01-01')

/* ******************
 * analyse new entries
 ********************/
select
	[FromDate],
	[ToDate],
	[SentAt],
	[MessageID],
	[MessageType],
	[BetFailedKey] = NEXT VALUE FOR MessagingSequence,
	CASE WHEN ISNULL(JSON_QUERY(MessagePayload, '$.payload."response"'),'{}') = '{}' THEN 0 ELSE 1 END [HasResponse],
	JSON_QUERY(MessagePayload, '$.payload."response"') [ResponseJSON],
	JSON_VALUE(MessagePayload, '$.payload.clientId') [ClientID],
	JSON_VALUE(MessagePayload, '$.payload.channel') [Channel],
	JSON_VALUE(MessagePayload, '$.payload.ipAddress') [IPAddress],
	JSON_VALUE(MessagePayload, '$.payload.errorCode') [ErrorCode],
	JSON_VALUE(MessagePayload, '$.payload.errorMessage') [ErrorMessage],
	CASE WHEN JSON_QUERY(MessagePayload, '$.payload."validationErrors"') IS NULL THEN 0 ELSE 1 END [HasValidationErrors],
	JSON_QUERY(MessagePayload, '$.payload."validationErrors"') [ValidationErrorJSON],
	JSON_QUERY(MessagePayload, '$.payload.request') [RequestJSON]
into #tmpFail
from [$(Acquisition)].[Messaging].[betfailedv1] bf with(nolock)
where ToDate = '9999-12-31'
	and FromDate >= @fromDate
	and not exists (select 1 from [Messaging].[BetFailed] k with(nolock) where k.SentAt = bf.SentAt and k.MessageID = bf.MessageID)

--select * from #tmpFail order by BetFailedKey

insert into [Messaging].[BetFailed]
select 	[FromDate],
	[ToDate],
	[BetFailedKey],
	[SentAt],
	[MessageID],
	[MessageType],
	[HasResponse],
	[ClientID],
	[Channel],
	[IPAddress],
	[ErrorCode],
	[ErrorMessage],
	[HasValidationErrors]
from #tmpFail n


/********************************
 *  Validation Errors
 *******************************/
select
	hve.FromDate,
	hve.ToDate,
	hve.BetFailedKey,
	[BetFailedValidationErrorKey] = NEXT VALUE FOR MessagingSequence,
	JSON_VALUE(value, '$.feature') [Feature],
	JSON_VALUE(value, '$.code') [Code],
	JSON_VALUE(value, '$.title') [Title],
	JSON_VALUE(value, '$.detail') [Detail]
into #tmpVal
from #tmpFail hve with(nolock)
CROSS APPLY
openjson(ValidationErrorJSON, '$')
where [HasValidationErrors] = 1
-- ~12s

INSERT INTO [Messaging].[BetFailedValidationErrors]
select
	t.[FromDate],
	t.[ToDate],
	t.[BetFailedKey],
	t.[BetFailedValidationErrorKey],
	t.[Feature],
	t.[Code],
	t.[Title],
	t.[Detail]
from #tmpVal t
-- ~3s


/********************************
 *  Response
 *******************************/
insert into [Messaging].[BetFailedResponses]
select
	hve.[FromDate],
	hve.[ToDate],
	hve.BetFailedKey,
	[BetFailedResponseKey] = NEXT VALUE FOR MessagingSequence,
	JSON_VALUE(ResponseJSON, '$.allowedUnitAmount') [AllowedUnitAmount],
	JSON_VALUE(ResponseJSON, '$.allowedTotalAmount') [AllowedTotalAmount],
	JSON_VALUE(ResponseJSON, '$.maxAllowedStake') [MaxAllowedStake],
	JSON_VALUE(ResponseJSON, '$.currentPriceToPlace') [CurrentPriceToPlace],
	JSON_VALUE(ResponseJSON, '$.currentPriceToWin') [CurrentPriceToWin]
from #tmpFail hve
where [HasResponse] = 1
-- ~1m


/********************************
 *  Request
 *******************************/
select
	[FromDate],
	[ToDate],
	[BetFailedKey],
	RequestJSON,
	[BetFailedRequestKey] = NEXT VALUE FOR MessagingSequence,
	JSON_VALUE(RequestJSON, '$.eventId') [EventID],
	JSON_VALUE(RequestJSON, '$.eventDomain') [EventDomain],
	JSON_VALUE(RequestJSON, '$.competitorId') [CompetitorID],
	JSON_VALUE(RequestJSON, '$.amountToWin') [AmountToWin],
	JSON_VALUE(RequestJSON, '$.amountToPlace') [AmountToPlace],
	JSON_VALUE(RequestJSON, '$.priceToWin') [PriceToWin],
	JSON_VALUE(RequestJSON, '$.priceToPlace') [PriceToPlace],
	JSON_VALUE(RequestJSON, '$.points') [Points],
	JSON_VALUE(RequestJSON, '$.betType') [BetType],
	JSON_VALUE(RequestJSON, '$.stakePerUnit') [StakePerUnit],
	JSON_VALUE(RequestJSON, '$.multiType') [MultiType],
	JSON_VALUE(RequestJSON, '$.isBonusBet') [IsBonusBet],
	JSON_VALUE(RequestJSON, '$.requestFromTwilio') [RequestFromTwilio],
	CASE WHEN JSON_QUERY(RequestJSON, '$."features"') IS NULL THEN 0 ELSE 1 END [HasFeatures],
	JSON_QUERY(RequestJSON, '$."features"') [FeaturesJSON],
	JSON_QUERY(RequestJSON, '$.legs') [MultiLegsJSON]
into #tmpreq
from #tmpFail aq
-- ~2m


insert into [Messaging].[BetFailedRequests]
select
	[FromDate],
	[ToDate],
	[BetFailedKey],
	[BetFailedRequestKey],
	[EventID],
	[EventDomain],
	[CompetitorID],
	[AmountToWin],
	[AmountToPlace],
	[PriceToWin],
	[PriceToPlace],
	[Points],
	[BetType],
	[StakePerUnit],
	[MultiType],
	[IsBonusBet],
	[RequestFromTwilio],
	[HasFeatures]
from #tmpreq


/* *******
 *  Get leg data
 *********/
insert into [Messaging].[BetFailedMultiLegs]
select
	hve.[FromDate],
	hve.[ToDate],
	hve.BetFailedRequestKey,
--	hve.MultiLegsJSON,
	[BetFailedMultiLegKey] = NEXT VALUE FOR MessagingSequence,
	JSON_VALUE(value, '$.eventId') [EventID],
	JSON_VALUE(value, '$.eventDomain') [EventDomain],
	JSON_VALUE(value, '$.competitorId') [CompetitorID],
	JSON_VALUE(value, '$.betType') [BetType],
	JSON_VALUE(value, '$.isEachWay') [IsEachWay],
	JSON_VALUE(value, '$.requestedPriceToWin') [RequestedPriceToWin],
	JSON_VALUE(value, '$.requestedPriceToPlace') [RequestedPriceToPlace],
	JSON_VALUE(value, '$.requestedPoints') [RequestedPoints]
from #tmpreq hve with(nolock)
CROSS APPLY
OPENJSON(MultiLegsJSON, '$')
where MultiLegsJSON is not null
-- 40s


/* **********
 *  Get request feature data
 *************/
insert into [Messaging].[BetFailedFeatures]
select
	[FromDate],
	[ToDate],
	[BetFailedRequestKey],
	[BetFailedFeatureKey] = NEXT VALUE FOR MessagingSequence,
	JSON_VALUE(value, '$.feature') [Feature],
	JSON_VALUE(value, '$."params".originalPrice') [OriginalPrice],
	JSON_VALUE(value, '$."params".boostedPrice') [BoostedPrice],
	JSON_VALUE(value, '$."params".tokenId') [TokenID]
from #tmpreq
cross apply
OPENJSON(RequestJSON, '$.features')
Where [HasFeatures] = 1
-- 30s


drop table #tmpreq
drop table #tmpFail
drop table #tmpVal

END
