﻿CREATE PROCEDURE [Messaging].[sp_PricePump]
AS

BEGIN

/* **********************
 *
 *		Price Pump
 *
 * *********************/
declare @fromDate datetime2(3) = coalesce((select max(FromDate) from [Messaging].[PricePumpBetCancelled] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.[ToDate],
		cast(json_value(MessagePayload, '$.payload.betId') as bigint) [BetID],
		cast(json_value(MessagePayload, '$.payload.reason') as nvarchar(255)) [Reason],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[pricepumpbetcancelledv1] rep with(nolock)
	WHERE rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by BetID, Reason order by FromDate) rn
	into #newData
from nd


INSERT INTO [Messaging].[PricePumpBetCancelled](FromDate, ToDate, BetID, Reason, MessageID, MessagePublisher, SentAt)
SELECT
	nd.[FromDate],
	nd.[ToDate],
	nd.[BetID],
	nd.[Reason],
	nd.[MessageID],
	nd.[MessagePublisher],
	nd.[SentAt]
from #newData nd
WHERE nd.rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[PricePumpBetCancelled] k with(nolock) WHERE nd.BetID = k.BetID AND nd.Reason = k.Reason)



set @fromDate = coalesce((select max(FromDate) from [Messaging].[PricePumpProfileUpdated] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.[FromDate],
		rep.[ToDate],
		cast(json_value(MessagePayload, '$.payload."x-WHA-System-ID"') as nvarchar(255)) [XWhaSystemID],
		cast(json_value(MessagePayload, '$.payload."x-WHA-Channel-ID"') as nvarchar(255)) [XWhaChannelID],
		cast(json_value(MessagePayload, '$.payload."x-WHA-User-ID"') as nvarchar(255)) [XWhaUserID],
		cast(json_value(MessagePayload, '$.payload.clientId') as int) [ClientID],
		cast(json_value(MessagePayload, '$.payload.profileName') as nvarchar(255)) [ProfileName],
		cast(json_value(MessagePayload, '$.payload.profileId') as int) [ProfileID],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[pricepumpprofileupdatedv1] rep with(nolock)
	WHERE rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by ClientID, ProfileID order by FromDate) rn
	into #newDataC
from nd

INSERT INTO [Messaging].[PricePumpProfileUpdated](FromDate, ToDate, XWhaSystemID, XWhaChannelID, XWhaUserID, ClientID, ProfileName, ProfileID, MessageID, MessagePublisher, SentAt)
SELECT
	FromDate,ToDate,XWhaSystemID, XWhaChannelID, XWhaUserID, ClientID, ProfileName, ProfileID, MessageID, MessagePublisher, SentAt
FROM #newDataC nd
WHERE rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[PricePumpProfileUpdated] k with(nolock) WHERE k.ClientID = nd.ClientID and k.ProfileID = nd.ProfileID)



set @fromDate = coalesce((select max(FromDate) from [Messaging].[PricePumpTokenCreated] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		[FromDate],
		[ToDate],
		cast(json_value(MessagePayload, '$.payload."x-WHA-System-ID"') as nvarchar(255)) [XWhaSystemID],
		cast(json_value(MessagePayload, '$.payload."x-WHA-Channel-ID"') as nvarchar(255)) [XWhaChannelID],
		cast(json_value(MessagePayload, '$.payload."x-WHA-User-ID"') as nvarchar(255)) [XWhaUserID],
		cast(json_value(MessagePayload, '$.payload.clientId') as int) [ClientID],
		cast(json_value(MessagePayload, '$.payload.tokenType') as nvarchar(255)) [TokenType],
		cast(json_value(MessagePayload, '$.payload.clientProfile') as nvarchar(255)) [ClientProfile],
		cast(json_value(MessagePayload, '$.payload.maxStake') as decimal(10,2)) [MaxStake],
   		left(replace(replace(json_value(MessagePayload, '$.payload.expiry'),'T',' '),'Z',''), case when charindex('+', json_value(MessagePayload, '$.payload.expiry')) > 24 then 23 else charindex('+', json_value(MessagePayload, '$.payload.expiry'))-1 end) [Expiry],
		cast(cast(json_value(MessagePayload, '$.payload.tokenId') as uniqueidentifier) as binary(16)) [TokenID],
		MessageID,
		MessagePublisher,
		SentAt
	from [$(Acquisition)].[Messaging].[pricepumptokencreatedv1] rep with(nolock)
	where rep.FromDate >= @fromDate
)
select FromDate, ToDate, XWhaSystemID, XWhaChannelID, CAST(TRY_CAST(XWhaUserID as int) as nvarchar(255)) [XWhaUserID], ClientID, TokenType, ClientProfile, MaxStake, Expiry, TokenID, MessageID, MessagePublisher, SentAt,
	ROW_NUMBER() over (partition by TokenID order by FromDate) rn
	into #newDataT
from nd

INSERT INTO [Messaging].[PricePumpTokenCreated](FromDate, ToDate, XWhaSystemID, XWhaChannelID, XWhaUserID, ClientID, TokenType, ClientProfile, MaxStake, Expiry, TokenID, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, XWhaSystemID, XWhaChannelID, XWhaUserID, ClientID, TokenType, ClientProfile, MaxStake, Expiry, TokenID, MessageID, MessagePublisher, SentAt
from #newDataT nd
WHERE rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[PricePumpTokenCreated] k with(nolock) WHERE k.TokenID = nd.TokenID)

DROP TABLE #newData
DROP TABLE #newDataC
DROP TABLE #newDataT

END

