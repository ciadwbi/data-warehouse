﻿CREATE PROCEDURE [Messaging].[sp_AcquisitionTracking]
AS

BEGIN

/* *****************
 *
 * ETL
 *
 *******************/
declare @meh nvarchar(100)
declare @rowz int

declare my_cursor CURSOR for
select sr.[FileName]
from [$(Acquisition)].[AWS].[AcquisitionTracking] sr
where Processed is null


open my_cursor
fetch NEXT from my_cursor into @meh

while @@fetch_status = 0
begin

/* *****************
 * Column headers
 *******************/
select rowNum ColumnID, items, replace(replace(replace(replace(replace(replace(items, '"""',''), '[', ''), ']', ''), '{', ''), '}', ''), ' ', '_') [ColumnHeaders]
into #columnHeaders
from [dbo].[SplitWithRowNum](
	(select items
		from [dbo].[SplitWithRowNum](
			(select substring(payload, 0, charindex(CHAR(10), payload, 0))
			from [$(Acquisition)].[AWS].[AcquisitionTracking]
			where [FileName] = @meh),CHAR(10),1
		)
	where rowNum = 0
	),',',1
)
-- select * from #columnHeaders


declare @qw nvarchar(max) = '
create table #tmpTable (
	PKID int identity,' + stuff((
	select ', [' + ColumnHeaders + '] nvarchar(1000)'
	from #columnHeaders
	order by ColumnID
	FOR XML PATH('')), 1, 1, '') + '
)

declare @jsonline nvarchar(max) = (
	select concat(''[{"entry": ["'', replace(replace(replace(replace(replace(payload, ''"""'',''''),''"'',''\"''),'','',''","''), CHAR(10), ''"]},{"entry":["''), ''&'', ''&amp;''), ''"]}]'') e
	from [$(Acquisition)].[AWS].[AcquisitionTracking] sr
	where [FileName] = ''' + @meh + '''
)

insert into #tmpTable
select *
from openjson(@jsonline)
WITH (' + stuff((
	select ', [' + ColumnHeaders + '] varchar(1000) ''$.entry[' + cast(ColumnID as nvarchar(20)) + ']'''
	from #columnHeaders
	order by ColumnID
	FOR XML PATH('')), 1, 1, '') + '
)

declare @FromDate datetime2(3) = (select FromDate from [$(Acquisition)].[AWS].[AcquisitionTracking] where [FileName] = ''' + @meh + ''')

insert into [Messaging].[TrackingAdjustData] ([FromDate], [ToDate], [FileName], [rowNum], ' + stuff((
	select ', [' + ColumnHeaders + ']'
	from #columnHeaders
	order by ColumnID
	FOR XML PATH('')), 1, 1, '') + ')
select @FromDate, ''9999-12-31'', ''' + @meh + ''', PKID - 1 [PKID],' + stuff((
	select ', [' + ColumnHeaders + ']'
	from #columnHeaders
	order by ColumnID
	FOR XML PATH('')), 1, 1, '') + '
from #tmpTable where PKID > 1 and isnull([' + (select ColumnHeaders from #columnHeaders where ColumnID = 0) + '],'''') <> '''''

set @qw = replace(@qw, (select ColumnHeaders from #columnHeaders where ColumnID = 0), 'Channel')

--select @qw

execute(@qw)
set @rowz = @@ROWCOUNT;

-- update Acquisition
--select @meh, @rowz
update sr
set Processed = @rowz
from [$(Acquisition)].[AWS].[AcquisitionTracking] sr
where sr.[FileName] = @meh

-- cleanup
execute('IF OBJECT_ID(''tempdb..#tmpTable'') IS NOT NULL DROP TABLE #tmpTable');
drop table #columnHeaders

	fetch next from my_cursor into @meh
end

close my_cursor
deallocate my_cursor

END
