﻿CREATE PROCEDURE [Messaging].[sp_SpinAndWin]
AS

BEGIN

/* **********************
 *
 *		Spin & Win +
 *
 * *********************/
declare @fromDate datetime2(3) = coalesce((select max(FromDate) from [Messaging].[SpinAndWinPrize] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.betId') as bigint) BetID,
		cast(json_value(MessagePayload, '$.payload.prizeId') as int) PrizeID,
		cast(json_value(MessagePayload, '$.payload.pricePumpMaxStake') as decimal(18,2)) PricePumpMaxStake,
		cast(json_value(MessagePayload, '$.payload.desc') as nvarchar(255)) [Desc],
		rep.MessageType,
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[spinandwinprizev1] rep with(nolock)
	WHERE rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by BetID, PrizeID order by FromDate) rn
	into #newData
from nd

INSERT INTO [Messaging].[SpinAndWinPrize] (FromDate,ToDate,BetID,PrizeID,PricePumpMaxStake,[Desc],MessageType,MessageID,MessagePublisher,SentAt)
SELECT
	FromDate,
	ToDate,
	BetID,
	PrizeID,
	PricePumpMaxStake,
	[Desc],
	MessageType,
	MessageID,
	MessagePublisher,
	SentAt
from #newData nd
where rn = 1 and not exists (select 1 from [Messaging].[SpinAndWinPrize] k with(nolock) where k.BetID = nd.BetID and k.PrizeID = nd.PrizeID)



set @fromDate = coalesce((select max(FromDate) from [Messaging].[SpinAndWinRedemption] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.clientId') as int) ClientID,
		cast(json_value(MessagePayload, '$.payload.prizeId') as int) PrizeID,
		cast(json_value(MessagePayload, '$.payload.awardedPoints') as int) AwardedPoints,
		cast(json_value(MessagePayload, '$.payload.betId') as bigint) BetID,
		cast(json_value(MessagePayload, '$.payload.acesServed') as int) AcesServed,
		cast(json_value(MessagePayload, '$.payload.desc') as nvarchar(255)) [Desc],
		rep.MessageType,
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[spinandwinredemptionactivityv1] rep with(nolock)
	WHERE rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by BetID, PrizeID order by FromDate) rn
	into #newData2
from nd

INSERT INTO [Messaging].[SpinAndWinRedemption] (FromDate,ToDate,ClientID,PrizeID,AwardedPoints,BetID,AcesServed,[Desc],MessageType,MessageID,MessagePublisher,SentAt)
SELECT
	FromDate,
	ToDate,
	ClientID,
	PrizeID,
	AwardedPoints,
	BetID,
	AcesServed,
	[Desc],
	MessageType,
	MessageID,
	MessagePublisher,
	SentAt
from #newData2 nd
where rn = 1 and not exists (select 1 from [Messaging].[SpinAndWinRedemption] k with(nolock) where k.BetID = nd.BetID and k.PrizeID = nd.PrizeID)


set @fromDate = coalesce((select max(FromDate) from [Messaging].[ChaseTheAceSettlement] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.eventId') as int) EventID,
		JSON_QUERY(MessagePayload, '$.payload."competitors"') [CompetitorsJSON],
		rep.MessageType,
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[chasetheacesettlementv1] rep with(nolock)
	WHERE rep.FromDate >= @fromDate
)
select
	FromDate,
	ToDate,
	EventID,
	JSON_VALUE(value, '$.competitorId') [CompetitorID],
	JSON_VALUE(value, '$.name') [CompetitorName],
	JSON_VALUE(value, '$.acesServed') [AcesServed],
	MessageType,
	MessageID,
	MessagePublisher,
	SentAt,
	ROW_NUMBER() over (partition by EventID, JSON_VALUE(value, '$.competitorId'), JSON_VALUE(value, '$.acesServed') order by FromDate) rn
into #newData3
from nd
CROSS APPLY
openjson(CompetitorsJSON, '$')

INSERT INTO [Messaging].[ChaseTheAceSettlement] (FromDate,ToDate,EventID,CompetitorID,CompetitorName,AcesServed,MessageType,MessageID,MessagePublisher,SentAt)
SELECT
	FromDate,
	ToDate,
	EventID,
	CompetitorID,
	CompetitorName,
	AcesServed,
	MessageType,
	MessageID,
	MessagePublisher,
	SentAt
from #newData3 nd
where rn = 1 and not exists (select 1 from [Messaging].[ChaseTheAceSettlement] k with(nolock) where k.EventID = nd.EventID and k.CompetitorID = nd.CompetitorID and k.AcesServed = nd.AcesServed)

drop table #newData
drop table #newData2
drop table #newData3

END
