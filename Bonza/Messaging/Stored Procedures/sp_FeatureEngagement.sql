﻿CREATE PROCEDURE [Messaging].[sp_FeatureEngagement]
AS

BEGIN

/* ***************************
 *
 *   Feature.Engagement.Event
 *
 ****************************/
declare @fromDate datetime2(3) = dateadd(MINUTE, -1, coalesce((select max(FromDate) from [Messaging].[FeatureEngagement] with(nolock)), '1900-01-01'))

;with nd as (
	SELECT DISTINCT
		rep.FromDate,
		rep.[ToDate],
		cast(json_value(MessagePayload, '$.payload.betID') as bigint) [BetID],
		cast(value as int) [WebFeatureKey],
		cast(json_value(MessagePayload, '$.payload.channel') as nvarchar(255)) [Channel],
		cast(value as int) [FeatureID],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[featureengagementevent] rep with(nolock)
	CROSS APPLY
	OPENJSON(MessagePayload, '$.payload.webFeatureID')
	WHERE rep.MessageType = 'FEATUREENGAGEMENT'
		and rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by BetID, WebFeatureKey order by FromDate) rn
	into #newData
from nd


INSERT INTO [Messaging].[FeatureEngagement](FromDate, ToDate, BetID, WebFeatureKey, Channel, FeatureID, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, BetID, WebFeatureKey, Channel, FeatureID, MessageID, MessagePublisher, SentAt
FROM #newData nd
WHERE rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[FeatureEngagement] k WITH(NOLOCK) WHERE k.BetID = nd.BetID and k.FeatureID = nd.FeatureID)

DROP TABLE #newData

END
