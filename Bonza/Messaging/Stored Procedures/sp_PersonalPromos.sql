﻿CREATE PROCEDURE [Messaging].[sp_PersonalPromos]
AS

BEGIN

/* ***************************
 *
 *   PersonalPromo.Claim_Promo
 *
 ****************************/
declare @fromDate datetime2(3) = coalesce((select max(FromDate) from [Messaging].[PersonalPromoClaim] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.Payload.ExactTargetId') as nvarchar(255)) [ExactTargetID],
		cast(json_value(MessagePayload, '$.Payload.PromoId') as nvarchar(255)) [PromoID],
		left(replace(replace(json_value(MessagePayload, '$.Payload.ClaimDate'),'T',' '),'Z',''),23) [ClaimDate],
		cast(json_value(MessagePayload, '$.Payload.Pin') as int) [PIN],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[personalpromoclaimv1] rep with(nolock)
	WHERE MessageType = 'CLAIM_PROMO'
		and rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by PIN, PromoID order by SentAt, FromDate) rn
into #newData
from nd

INSERT INTO [Messaging].[PersonalPromoClaim](FromDate, ToDate, ExactTargetId, PromoId, ClaimDate, Pin, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, ExactTargetId, PromoId, ClaimDate, Pin, MessageID, MessagePublisher, SentAt
FROM #newData nd
WHERE rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[PersonalPromoClaim] k with(nolock) WHERE k.Pin = nd.PIN and k.PromoId = nd.PromoID)

DROP TABLE #newData

END
