﻿CREATE PROCEDURE [Messaging].[sp_Rewards]
AS

BEGIN

/* ***************************
 *
 *   Rewards.Points.Activity.PointsAdjusted
 *
 ****************************/
declare @fromDate datetime2(3) = coalesce((select max(FromDate) from [Messaging].[RewardsPointsAdjusted] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.clientId') as int) [ClientID],
		cast(json_value(MessagePayload, '$.payload.points') as decimal(18,2)) [Points],
		cast(json_value(MessagePayload, '$.payload.current') as decimal(18,2)) [Current],
		cast(json_value(MessagePayload, '$.payload.lifetime') as decimal(18,2)) [Lifetime],
		cast(json_value(MessagePayload, '$.payload.reason') as nvarchar(255)) [Reason],
		left(replace(replace(json_value(MessagePayload, '$.payload.executedAt'),'T',' '),'Z',''),23) [ExecutedAt],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[rewardspointsactivityv1] rep with(nolock)
	WHERE MessageType = 'POINTS_ADJUSTED'
		and rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by ClientID, ExecutedAt, MessageID order by FromDate) rn
	into #newDataAdjusted
from nd

INSERT INTO [Messaging].[RewardsPointsAdjusted](FromDate, ToDate, ClientID, Points, [Current], [Lifetime], Reason, ExecutedAt, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, ClientID, Points, [Current], [Lifetime], Reason, ExecutedAt, MessageID, MessagePublisher, SentAt
FROM #newDataAdjusted nd
WHERE NOT EXISTS (SELECT 1 FROM [Messaging].[RewardsPointsAdjusted] k with(nolock) WHERE k.ClientID = nd.ClientID and k.ExecutedAt = nd.ExecutedAt and k.MessageID = nd.MessageID)


/* ***************************
 *
 *   Rewards.Points.Activity.PointsAccrued
 *
 ****************************/
set @fromDate = coalesce((select max(FromDate) from [Messaging].[RewardsPointsAccrued] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.clientId') as int) [ClientID],
		cast(json_value(MessagePayload, '$.payload.points') as decimal(18,2)) [Points],
		cast(json_value(MessagePayload, '$.payload.current') as decimal(18,2)) [Current],
		cast(json_value(MessagePayload, '$.payload.lifetime') as decimal(18,2)) [Lifetime],
		cast(json_value(MessagePayload, '$.payload.betId') as bigint) [BetID],
		cast(json_value(MessagePayload, '$.payload.typeOfBet') as int) [TypeOfBet],
		left(replace(replace(json_value(MessagePayload, '$.payload.executedAt'),'T',' '),'Z',''),23) [ExecutedAt],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[rewardspointsactivityv1] rep with(nolock)
	WHERE MessageType = 'POINTS_ACCRUED'
		and rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by BetID, ClientID, ExecutedAt order by FromDate) rn
	into #newDataAccrued
from nd

INSERT INTO [Messaging].[RewardsPointsAccrued](FromDate, ToDate, ClientID, Points, [Current], [Lifetime], BetID, TypeOfBet, ExecutedAt, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, ClientID, Points, [Current], [Lifetime], BetID, TypeOfBet, ExecutedAt, MessageID, MessagePublisher, SentAt
FROM #newDataAccrued nd
WHERE rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[RewardsPointsAccrued] k WITH(NOLOCK) WHERE k.ClientID = nd.ClientID and k.BetID = nd.BetID and k.MessageID = nd.MessageID)


/* ***************************
 *
 *   Rewards.Client.Activity.UpdateAddress
 *
 ****************************/
set @fromDate = coalesce((select max(FromDate) from [Messaging].[RewardsUpdateAddress] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.clientId') as int) [ClientID],
		cast(json_value(MessagePayload, '$.payload.state') as int) [State],
		left(replace(replace(json_value(MessagePayload, '$.payload.executedAt'),'T',' '),'Z',''),23) [ExecutedAt],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[rewardsclientactivityv1] rep with(nolock)
	WHERE MessageType = 'UPDATE_ADDRESS'
		and rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by ClientID order by FromDate) rn
	into #newDataUpdAdd
from nd

INSERT INTO [Messaging].[RewardsUpdateAddress](FromDate, ToDate, ClientID, [State], ExecutedAt, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, ClientID, [State], ExecutedAt, MessageID, MessagePublisher, SentAt
FROM #newDataUpdAdd nd
WHERE rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[RewardsUpdateAddress] k with(nolock) WHERE k.ClientID = nd.ClientID and k.ToDate = nd.ToDate)


/* ***************************
 *
 *   Rewards.Client.Activity.CreateVelocityLink
 *
 ****************************/
set @fromDate = coalesce((select max(FromDate) from [Messaging].[RewardsCreateVelocityLink] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.clientId') as int) [ClientID],
		cast(json_value(MessagePayload, '$.payload.velocityNumber') as bigint) [VelocityNumber],
		cast(json_value(MessagePayload, '$.payload.firstName') as nvarchar(255)) [FirstName],
		cast(json_value(MessagePayload, '$.payload.lastName') as nvarchar(255)) [LastName],
		left(replace(replace(json_value(MessagePayload, '$.payload.executedAt'),'T',' '),'Z',''),23) [ExecutedAt],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[rewardsclientactivityv1] rep with(nolock)
	WHERE MessageType = 'CREATE_VELOCITY_LINK'
		and rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by ClientID, VelocityNumber order by FromDate) rn
	into #newDataCVelo
from nd


INSERT INTO [Messaging].[RewardsCreateVelocityLink](FromDate, ToDate, ClientID, VelocityNumber, FirstName, LastName,  ExecutedAt, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, ClientID, VelocityNumber, FirstName, LastName,  ExecutedAt, MessageID, MessagePublisher, SentAt
FROM #newDataCVelo nd
WHERE rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[RewardsCreateVelocityLink] k with(nolock) WHERE k.ClientID = nd.ClientID and k.VelocityNumber = nd.VelocityNumber and k.ToDate = nd.ToDate)


/* ***************************
 *
 *   Rewards.Client.Activity.UpdateVelocityLink
 *
 ****************************/
set @fromDate = coalesce((select max(FromDate) from [Messaging].[RewardsUpdateVelocityLink] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.clientId') as int) [ClientID],
		cast(json_value(MessagePayload, '$.payload.velocityNumber') as bigint) [VelocityNumber],
		cast(json_value(MessagePayload, '$.payload.firstName') as nvarchar(255)) [FirstName],
		cast(json_value(MessagePayload, '$.payload.lastName') as nvarchar(255)) [LastName],
		left(replace(replace(json_value(MessagePayload, '$.payload.executedAt'),'T',' '),'Z',''),23) [ExecutedAt],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[rewardsclientactivityv1] rep with(nolock)
	WHERE MessageType = 'UPDATE_VELOCITY_LINK'
		and rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by ClientID, VelocityNumber order by FromDate) rn
	into #newDataUpdVelo
from nd

INSERT INTO [Messaging].[RewardsUpdateVelocityLink](FromDate, ToDate, ClientID, VelocityNumber, FirstName, LastName, ExecutedAt, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, ClientID, VelocityNumber, FirstName, LastName, ExecutedAt, MessageID, MessagePublisher, SentAt
FROM #newDataUpdVelo nd
WHERE rn=1 and NOT EXISTS (SELECT 1 FROM [Messaging].[RewardsUpdateVelocityLink] k with(nolock) WHERE k.ClientID = nd.ClientID and k.VelocityNumber = nd.VelocityNumber and k.ToDate = nd.ToDate)


/* ***************************
 *
 *   Rewards.Client.Activity.UpdateEligibility
 *
 ****************************/
set @fromDate = coalesce((select max(FromDate) from [Messaging].[RewardsUpdateEligibility] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.clientId') as int) [ClientID],
		case when cast(json_value(MessagePayload, '$.payload.excluded') as nvarchar(255)) = 'true' then 1 else 0 end [Excluded],
		cast(json_value(MessagePayload, '$.payload.exclusionReason') as int) [ExclusionReason],
		left(replace(replace(json_value(MessagePayload, '$.payload.executedAt'),'T',' '),'Z',''),23) [ExecutedAt],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[rewardsclientactivityv1] rep with(nolock)
	WHERE MessageType = 'UPDATE_ELIGIBILITY'
		and rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by ClientID, Excluded, ExclusionReason order by FromDate) rn
	into #newDataUpdElig
from nd

INSERT INTO [Messaging].[RewardsUpdateEligibility](FromDate, ToDate, ClientID, Excluded, ExclusionReason, ExecutedAt, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, ClientID, Excluded, ExclusionReason, ExecutedAt, MessageID, MessagePublisher, SentAt
FROM #newDataUpdElig nd
WHERE rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[RewardsUpdateEligibility] k WITH(NOLOCK) WHERE k.ClientID = nd.ClientID and k.Excluded = nd.Excluded and k.ExclusionReason = nd.ExclusionReason and k.ToDate = nd.ToDate)


/* ***************************
 *
 *   Rewards.Redemption.BonusRedemption
 *
 ****************************/
set @fromDate = coalesce((select max(FromDate) from [Messaging].[RewardsBonusRedemption] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.clientId') as int) [ClientID],
		cast(json_value(MessagePayload, '$.payload.channelId') as int) [ChannelID],
		cast(json_value(MessagePayload, '$.payload.points') as bigint) [Points],
		cast(json_value(MessagePayload, '$.payload.redemptionValue') as bigint) [RedemptionValue],
		cast(json_value(MessagePayload, '$.payload.current') as decimal(18,2)) [Current],
		cast(json_value(MessagePayload, '$.payload.lifetime') as decimal(18,2)) [Lifetime],
		left(replace(replace(json_value(MessagePayload, '$.payload.executedAt'),'T',' '),'Z',''),23) [ExecutedAt],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[rewardsredemptionactivityv1] rep with(nolock)
	WHERE MessageType = 'BONUS_REDEMPTION'
		and rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by ClientID, ExecutedAt order by FromDate) rn
	into #newDataRedmBonus
from nd


INSERT INTO [Messaging].[RewardsBonusRedemption](FromDate, ToDate, ClientID, ChannelID, Points, RedemptionValue, [Current], [Lifetime], ExecutedAt, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, ClientID, ChannelID, Points, RedemptionValue, [Current], [Lifetime], ExecutedAt, MessageID, MessagePublisher, SentAt
FROM #newDataRedmBonus nd
WHERE rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[RewardsBonusRedemption] k WITH(NOLOCK) WHERE k.ClientID = nd.ClientID and k.ExecutedAt = nd.ExecutedAt and k.ToDate = nd.ToDate)


/* ***************************
 *
 *   Rewards.Redemption.VelocityRedemption
 *
 ****************************/
set @fromDate = coalesce((select max(FromDate) from [Messaging].[RewardsVelocityRedemption] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.clientId') as int) [ClientID],
		cast(json_value(MessagePayload, '$.payload.channelId') as int) [ChannelID],
		cast(json_value(MessagePayload, '$.payload.points') as bigint) [Points],
		cast(json_value(MessagePayload, '$.payload.redemptionValue') as bigint) [RedemptionValue],
		cast(json_value(MessagePayload, '$.payload.current') as decimal(18,2)) [Current],
		cast(json_value(MessagePayload, '$.payload.lifetime') as decimal(18,2)) [Lifetime],
		left(replace(replace(json_value(MessagePayload, '$.payload.executedAt'),'T',' '),'Z',''),23) [ExecutedAt],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[rewardsredemptionactivityv1] rep with(nolock)
	WHERE MessageType = 'VELOCITY_REDEMPTION'
		and rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by ClientID, ExecutedAt order by FromDate) rn
	into #newDataRedmVelo
from nd

INSERT INTO [Messaging].[RewardsVelocityRedemption](FromDate, ToDate, ClientID, ChannelID, Points, RedemptionValue, [Current], [Lifetime], ExecutedAt, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, ClientID, ChannelID, Points, RedemptionValue, [Current], [Lifetime], ExecutedAt, MessageID, MessagePublisher, SentAt
FROM #newDataRedmVelo nd
WHERE rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[RewardsVelocityRedemption] k WITH(NOLOCK) WHERE k.ClientID = nd.ClientID and k.ExecutedAt = nd.ExecutedAt and k.ToDate = nd.ToDate)


/* ***************************
 *
 *   Rewards.Redemption.PricePumpRedemption
 *
 ****************************/
set @fromDate = coalesce((select max(FromDate) from [Messaging].[RewardsPricePumpRedemption] with(nolock)), '1900-01-01')

;with nd as (
	SELECT
		rep.FromDate,
		rep.ToDate,
		cast(json_value(MessagePayload, '$.payload.clientId') as int) [ClientID],
		cast(json_value(MessagePayload, '$.payload.channelId') as int) [ChannelID],
		cast(json_value(MessagePayload, '$.payload.points') as bigint) [Points],
		cast(json_value(MessagePayload, '$.payload.maxStake') as decimal(18,2)) [MaxStake],
		cast(json_value(MessagePayload, '$.payload.current') as decimal(18,2)) [Current],
		cast(json_value(MessagePayload, '$.payload.lifetime') as decimal(18,2)) [Lifetime],
		left(replace(replace(json_value(MessagePayload, '$.payload.executedAt'),'T',' '),'Z',''),23) [ExecutedAt],
		rep.MessageID,
		rep.MessagePublisher,
		rep.SentAt
	from [$(Acquisition)].[Messaging].[rewardsredemptionactivityv1] rep with(nolock)
	WHERE MessageType = 'PRICE_PUMP_REDEMPTION'
		and rep.FromDate >= @fromDate
)
select *,
	ROW_NUMBER() over (partition by ClientID, ExecutedAt order by FromDate) rn
	into #newDataRedmPP
from nd

INSERT INTO [Messaging].[RewardsPricePumpRedemption](FromDate, ToDate, ClientID, ChannelID, Points, MaxStake, [Current], [Lifetime], ExecutedAt, MessageID, MessagePublisher, SentAt)
SELECT FromDate, ToDate, ClientID, ChannelID, Points, MaxStake, [Current], [Lifetime], ExecutedAt, MessageID, MessagePublisher, SentAt
FROM #newDataRedmPP nd
WHERE rn = 1 and NOT EXISTS (SELECT 1 FROM [Messaging].[RewardsPricePumpRedemption] k WITH(NOLOCK) WHERE k.ClientID = nd.ClientID and k.ExecutedAt = nd.ExecutedAt and k.ToDate = nd.ToDate)

drop table #newDataAccrued
drop table #newDataAdjusted
drop table #newDataCVelo
drop table #newDataRedmBonus
drop table #newDataRedmPP
drop table #newDataRedmVelo
drop table #newDataUpdAdd
drop table #newDataUpdElig
drop table #newDataUpdVelo

END
