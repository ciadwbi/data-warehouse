﻿CREATE PROCEDURE [Messaging].[sp_RawExtract]
AS

BEGIN

IF DATEPART(HOUR, GETDATE()) > 8 -- let DWH/BID finish in peace :)
BEGIN

--	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

	declare @startTime datetime2(3) = getdate();

	EXEC Messaging.sp_SpinAndWin
	EXEC Messaging.sp_PricePump
	EXEC Messaging.sp_FeatureEngagement
	EXEC Messaging.sp_Rewards
	EXEC Messaging.sp_FailedBets
	EXEC Messaging.sp_PersonalPromos

END
END
