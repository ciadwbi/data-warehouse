﻿CREATE TABLE [Messaging].[BetFailedRequests] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[BetFailedKey] bigint,
	[BetFailedRequestKey] BIGINT,-- DEFAULT NEXT VALUE FOR MessagingSequence,
	[EventID] int,
	[EventDomain] int,
	[CompetitorID] int,
	[AmountToWin] float,
	[AmountToPlace] float,
	[PriceToWin] float,
	[PriceToPlace] float,
	[Points] float,
	[BetType] int,
	[StakePerUnit] float,
	[MultiType] int,
	[IsBonusBet] bit,
	[RequestFromTwilio] bit,
	[HasFeatures] bit,
	CONSTRAINT PK_BetFailedRequests_Key PRIMARY KEY NONCLUSTERED ([BetFailedRequestKey],[ToDate])
)
GO
CREATE UNIQUE CLUSTERED INDEX [CI_BetFailedRequests(A)] ON [Messaging].[BetFailedRequests]
(
	[BetFailedKey] ASC,
	[BetFailedRequestKey] ASC,
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailedRequests_FD(A)] ON [Messaging].[BetFailedRequests]
(
    [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailedRequests_TD(A)] ON [Messaging].[BetFailedRequests]
(
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)