﻿create table [Messaging].[RewardsPointsAccrued] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[RewardsPointsAccruedKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[ClientID] int,
	[Points] decimal(18,2),
	[Current] decimal(18,2),
	[Lifetime] decimal(18,2),
	[BetID] bigint,
	[TypeOfBet] nvarchar(255),
	[ExecutedAt] datetime,
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_RewardsPointsAccrued_Key PRIMARY KEY NONCLUSTERED ([RewardsPointsAccruedKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_RewardsPointsAccrued(A)] ON [Messaging].[RewardsPointsAccrued]
(
	   [BetID] ASC,
       [ClientID] ASC,
	   [ExecutedAt] ASC,
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsPointsAccrued_FD(A)] ON [Messaging].[RewardsPointsAccrued]
(
       [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsPointsAccrued_TD(A)] ON [Messaging].[RewardsPointsAccrued]
(
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
