﻿CREATE TABLE [Messaging].[FeatureEngagement](
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[FeatureEngagementKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[BetID] [bigint] NULL,
	[WebFeatureKey] [int] NULL,
	[Channel] [int] NULL,
	[FeatureID] [int] NULL,
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_FeatureEngagement_Key PRIMARY KEY NONCLUSTERED ([FeatureEngagementKey],[ToDate])
)
GO
CREATE UNIQUE CLUSTERED INDEX [CI_FeatureEngagement(A)] ON [Messaging].[FeatureEngagement]
(
	[BetID] ASC,
	[WebFeatureKey] ASC,
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_FeatureEngagement_FD(A)] ON [Messaging].[FeatureEngagement]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_FeatureEngagement_TD(A)] ON [Messaging].[FeatureEngagement]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
