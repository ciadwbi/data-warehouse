﻿create table [Messaging].[ChaseTheAceSettlement] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[ChaseTheAceSettlementKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[EventID] bigint,
	[CompetitorID] int,
	[CompetitorName] nvarchar(255),
	[AcesServed] int,
	[MessageType] nvarchar(255),
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_ChaseTheAceSettlement_Key PRIMARY KEY NONCLUSTERED ([ChaseTheAceSettlementKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_ChaseTheAceSettlement(A)] ON [Messaging].[ChaseTheAceSettlement]
(
    [EventID] ASC,
	[CompetitorID] ASC,
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_ChaseTheAceSettlement_FD(A)] ON [Messaging].[ChaseTheAceSettlement]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_ChaseTheAceSettlement_TD(A)] ON [Messaging].[ChaseTheAceSettlement]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
