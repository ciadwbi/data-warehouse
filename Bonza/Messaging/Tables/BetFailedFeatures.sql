﻿CREATE TABLE [Messaging].[BetFailedFeatures] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[BetFailedRequestKey] bigint,
	[BetFailedFeatureKey] BIGINT,-- DEFAULT NEXT VALUE FOR MessagingSequence,
	[Feature] nvarchar(100),
	[OriginalPrice] float,
	[BoostedPrice] float,
	[TokenID] nvarchar(100),
	CONSTRAINT PK_BetFailedFeatures_Key PRIMARY KEY NONCLUSTERED ([BetFailedFeatureKey],[ToDate])
)
GO
CREATE UNIQUE CLUSTERED INDEX [CI_BetFailedFeatures(A)] ON [Messaging].[BetFailedFeatures]
(
	[BetFailedRequestKey] ASC,
    [BetFailedFeatureKey] ASC,
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailedFeatures_FD(A)] ON [Messaging].[BetFailedFeatures]
(
    [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailedFeatures_TD(A)] ON [Messaging].[BetFailedFeatures]
(
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)