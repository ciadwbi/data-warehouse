﻿create table [Messaging].[RewardsUpdateEligibility] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[RewardsUpdateEligibilityKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[ClientID] int,
	[Excluded] bit,
	[ExclusionReason] int,
	[ExecutedAt] datetime,
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_RewardsUpdateEligibility_Key PRIMARY KEY NONCLUSTERED ([RewardsUpdateEligibilityKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_RewardsUpdateEligibility(A)] ON [Messaging].[RewardsUpdateEligibility]
(
	[ClientID] ASC,
	[Excluded] ASC,
	[ExclusionReason] ASC,
	[SentAt] ASC,
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsUpdateEligibility_FD(A)] ON [Messaging].[RewardsUpdateEligibility]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsUpdateEligibility_TD(A)] ON [Messaging].[RewardsUpdateEligibility]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
