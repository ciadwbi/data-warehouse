﻿create table [Messaging].[RewardsUpdateVelocityLink] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[RewardsUpdateVelocityLinkKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[ClientID] int,
	[VelocityNumber] bigint,
	[FirstName] nvarchar(255),
	[LastName] nvarchar(255),
	[ExecutedAt] datetime,
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_RewardsUpdateVelocityLink_Key PRIMARY KEY NONCLUSTERED ([RewardsUpdateVelocityLinkKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_RewardsUpdateVelocityLink(A)] ON [Messaging].[RewardsUpdateVelocityLink]
(
	[ClientID] ASC,
	[VelocityNumber] ASC,
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsUpdateVelocityLink_FD(A)] ON [Messaging].[RewardsUpdateVelocityLink]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsUpdateVelocityLink_TD(A)] ON [Messaging].[RewardsUpdateVelocityLink]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
