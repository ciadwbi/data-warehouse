﻿CREATE TABLE [Messaging].[BetFailedMultiLegs] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[BetFailedRequestKey] bigint,
	[BetFailedMultiLegKey] BIGINT,-- DEFAULT NEXT VALUE FOR MessagingSequence,
	[EventID] int,
	[EventDomain] int,
	[CompetitorID] int,
	[BetType] int,
	[IsEachWay] bit,
	[RequestedPriceToWin] float,
	[RequestedPriceToPlace] float,
	[RequestedPoints] float,
	CONSTRAINT PK_BetFailedMultiLegs_Key PRIMARY KEY NONCLUSTERED ([BetFailedMultiLegKey],[ToDate])
)
GO
CREATE UNIQUE CLUSTERED INDEX [CI_BetFailedMultiLegs(A)] ON [Messaging].[BetFailedMultiLegs]
(
	[BetFailedRequestKey] ASC,
    [BetFailedMultiLegKey] ASC,
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailedMultiLegs_FD(A)] ON [Messaging].[BetFailedMultiLegs]
(
    [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailedMultiLegs_TD(A)] ON [Messaging].[BetFailedMultiLegs]
(
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailedRequest_FD(A)] ON [Messaging].[BetFailedMultiLegs]
(
	[BetFailedRequestKey] ASC,
	[BetFailedMultiLegKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)