﻿create table [Messaging].[RewardsCreateVelocityLink] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[RewardsCreateVelocityLinkKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[ClientID] int,
	[VelocityNumber] bigint,
	[FirstName] nvarchar(255),
	[LastName] nvarchar(255),
	[ExecutedAt] datetime,
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_RewardsCreateVelocityLink_Key PRIMARY KEY NONCLUSTERED ([RewardsCreateVelocityLinkKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_RewardsCreateVelocityLink(A)] ON [Messaging].[RewardsCreateVelocityLink]
(
	[ClientID] ASC,
	[VelocityNumber] ASC,       
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsCreateVelocityLink_FD(A)] ON [Messaging].[RewardsCreateVelocityLink]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsCreateVelocityLink_TD(A)] ON [Messaging].[RewardsCreateVelocityLink]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
