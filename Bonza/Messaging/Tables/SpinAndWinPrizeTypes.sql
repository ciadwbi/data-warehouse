﻿create table [Messaging].[SpinAndWinPrizeTypes] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[SpinAndWinPrizeTypeKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[PrizeType] int,
	[Description] nvarchar(255),
	[RewardsPoints] int,
	[VelocityPoints] int,
	CONSTRAINT PK_SpinAndWinPrizeTypes_Key PRIMARY KEY NONCLUSTERED ([SpinAndWinPrizeTypeKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_SpinAndWinPrizeTypes(A)] ON [Messaging].[SpinAndWinPrizeTypes]
(
    [PrizeType] ASC,
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_SpinAndWinPrizeTypes_FD(A)] ON [Messaging].[SpinAndWinPrizeTypes]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_SpinAndWinPrizeTypes_TD(A)] ON [Messaging].[SpinAndWinPrizeTypes]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
