﻿create table [Messaging].[PricePumpProfileUpdated] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),

	[PricePumpProfileUpdatedKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
    [XWhaSystemID] nvarchar(255),
    [XWhaChannelID] nvarchar(255),
    [XWhaUserID] nvarchar(255),
    [ClientID] int,
    [ProfileName] nvarchar(255),
    [ProfileID] int,

	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_PricePumpProfileUpdated_Key PRIMARY KEY NONCLUSTERED ([PricePumpProfileUpdatedKey],[ToDate])
)
go

CREATE UNIQUE CLUSTERED INDEX [CI_PricePumpProfileUpdated(A)] ON [Messaging].[PricePumpProfileUpdated]
(
       [ClientID] ASC,
	   [ProfileID] ASC,
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_PricePumpProfileUpdated_FD(A)] ON [Messaging].[PricePumpProfileUpdated]
(
       [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_PricePumpProfileUpdated_TD(A)] ON [Messaging].[PricePumpProfileUpdated]
(
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
