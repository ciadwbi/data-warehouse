﻿create table [Messaging].[PersonalPromoClaim] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[PersonalPromoClaimKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[ExactTargetID] nvarchar(255),
	[PromoID] nvarchar(255),
	[ClaimDate] datetime,
	[PIN] bigint,
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_PersonalPromoClaim_Key PRIMARY KEY NONCLUSTERED ([PersonalPromoClaimKey],[ToDate])
)
go

CREATE UNIQUE CLUSTERED INDEX [CI_PersonalPromoClaim(A)] ON [Messaging].[PersonalPromoClaim]
(
       [PIN] ASC,
	   [PromoID] ASC,
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_PersonalPromoClaim_FD(A)] ON [Messaging].[PersonalPromoClaim]
(
       [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_PersonalPromoClaim_TD(A)] ON [Messaging].[PersonalPromoClaim]
(
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO