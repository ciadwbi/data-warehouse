﻿create table [Messaging].[TrackingAdjustData] (
	[FromDate] Datetime2(3) NULL,
	[ToDate] datetime2(3) NULL,
	[FileName] nvarchar(100) NULL,
	[RowNum] int,
	[Channel] nvarchar(20),
	[clientID] bigint,
	[app_id] nvarchar(20),
	[app_name] nvarchar(50),
	[app_version] nvarchar(20),
	[event_name] nvarchar(255),
	[tracker] nvarchar(50),
	[tracker_name] nvarchar(max),
	[adwords_campaign_type] nvarchar(255),
	[adwords_campaign_name] nvarchar(255),
	[fb_campaign_group_name] nvarchar(255),
	[fb_campaign_name] nvarchar(255),
	[ip_address] nvarchar(255),
	[partner_parameters] nvarchar(max),
	[dcp_btag] nvarchar(255),
	[dcp_campaign] nvarchar(255),
	[dcp_keyword] nvarchar(255),
	[dcp_publisher] nvarchar(255),
	[dcp_source] nvarchar(255),
	[label] nvarchar(255),
	[publisher] nvarchar(255),
	[keyword] nvarchar(1000),
	[campaign] nvarchar(255),
	[btag] nvarchar(255),
	[source] nvarchar(255)
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_TrackingAdjustData(A)] ON [Messaging].[TrackingAdjustData]
(
    [FileName] ASC,
	[RowNum] ASC,
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_TrackingAdjustData_FD(A)] ON [Messaging].[TrackingAdjustData]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_TrackingAdjustData_TD(A)] ON [Messaging].[TrackingAdjustData]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_TrackingAdjustData_Channel(A)] ON [Messaging].[TrackingAdjustData]
(
	[Channel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_TrackingAdjustData_ClientId_Channel] ON [Messaging].[TrackingAdjustData]
	([ClientID] ASC, [Channel] ASC) INCLUDE ( [FromDate], [ToDate])
GO

