﻿create table [Messaging].[BetFailedValidationErrors] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[BetFailedKey] bigint,
	[BetFailedValidationErrorKey] BIGINT, -- DEFAULT NEXT VALUE FOR MessagingSequence,
	[Feature] nvarchar(100),
	[Code] nvarchar(100),
	[Title] nvarchar(1000),
	[Detail] nvarchar(max),
	CONSTRAINT PK_BetFailedValidationErrors_Key PRIMARY KEY NONCLUSTERED ([BetFailedValidationErrorKey],[ToDate])
)
GO
CREATE UNIQUE CLUSTERED INDEX [CI_BetFailedValidationErrors(A)] ON [Messaging].[BetFailedValidationErrors]
(
    [BetFailedKey] ASC,
	[BetFailedValidationErrorKey] ASC,
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailedValidationErrors_FD(A)] ON [Messaging].[BetFailedValidationErrors]
(
    [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailedValidationErrors_TD(A)] ON [Messaging].[BetFailedValidationErrors]
(
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)