﻿create table [Messaging].[RewardsBonusRedemption] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[RewardsBonusRedemptionKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[ClientID] int,
	[ChannelID] int,
	[Points] bigint,
	[RedemptionValue] bigint,
	[Current] decimal(18,2),
	[Lifetime] decimal(18,2),
	[ExecutedAt] datetime,
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_RewardsBonusRedemption_Key PRIMARY KEY NONCLUSTERED ([RewardsBonusRedemptionKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_RewardsBonusRedemption(A)] ON [Messaging].[RewardsBonusRedemption]
(
	[ClientID] ASC,
	[ExecutedAt] ASC,
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsBonusRedemption_FD(A)] ON [Messaging].[RewardsBonusRedemption]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsBonusRedemption_TD(A)] ON [Messaging].[RewardsBonusRedemption]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
