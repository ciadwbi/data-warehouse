﻿create table [Messaging].[RewardsPricePumpRedemption] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[RewardsPricePumpRedemptionKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[ClientID] int,
	[ChannelID] int,
	[Points] bigint,
	[MaxStake] decimal(18,2),
	[Current] decimal(18,2),
	[Lifetime] decimal(18,2),
	[ExecutedAt] datetime,
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_RewardsPricePumpRedemption_Key PRIMARY KEY NONCLUSTERED ([RewardsPricePumpRedemptionKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_RewardsPricePumpRedemption(A)] ON [Messaging].[RewardsPricePumpRedemption]
(
       [ClientID] ASC,
	   [ExecutedAt] ASC,
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsPricePumpRedemption_FD(A)] ON [Messaging].[RewardsPricePumpRedemption]
(
       [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsPricePumpRedemption_TD(A)] ON [Messaging].[RewardsPricePumpRedemption]
(
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
