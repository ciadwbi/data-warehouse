﻿create table [Messaging].[PricePumpBetCancelled] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[PricePumpBetCancelledKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
    [BetID] bigint,
    [Reason] nvarchar(255),
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_PricePumpBetCancelledKey_Key PRIMARY KEY NONCLUSTERED ([PricePumpBetCancelledKey],[ToDate])
)
go

CREATE UNIQUE CLUSTERED INDEX [CI_PricePumpBetCancelled(A)] ON [Messaging].[PricePumpBetCancelled]
(
       [BetID] ASC,
	   [Reason] ASC,      
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_PricePumpBetCancelled_FD(A)] ON [Messaging].[PricePumpBetCancelled]
(
       [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_PricePumpBetCancelled_TD(A)] ON [Messaging].[PricePumpBetCancelled]
(
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
