﻿create table [Messaging].[SpinAndWinRedemption] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[SpinAndWinRedemptionKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[ClientID] int,
	[PrizeID] int,
	[AwardedPoints] decimal(18,2),
	[BetID] bigint,
	[AcesServed] int,
	[Desc] nvarchar(255),
	[MessageType] nvarchar(255),
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_SpinAndWinRedemption_Key PRIMARY KEY NONCLUSTERED ([SpinAndWinRedemptionKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_SpinAndWinRedemption(A)] ON [Messaging].[SpinAndWinRedemption]
(
    [BetID] ASC,
	[PrizeID] ASC,
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_SpinAndWinRedemption_FD(A)] ON [Messaging].[SpinAndWinRedemption]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_SpinAndWinRedemption_TD(A)] ON [Messaging].[SpinAndWinRedemption]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
