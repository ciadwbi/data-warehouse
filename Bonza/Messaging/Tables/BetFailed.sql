﻿create table [Messaging].[BetFailed] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[BetFailedKey] BIGINT, -- DEFAULT NEXT VALUE FOR MessagingSequence,
	[SentAt] datetime,
	[MessageID] binary(16),
	[MessageType] nvarchar(50),
	[HasResponse] bit,
	[ClientID] int,
	[Channel] int,
	[IPAddress] varchar(100),
	[ErrorCode] int,
	[ErrorMessage] nvarchar(max),
	[HasValidationErrors] bit,
	CONSTRAINT PK_BetFailed_Key PRIMARY KEY NONCLUSTERED ([BetFailedKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_BetFailed(A)] ON [Messaging].[BetFailed]
(
    [ClientID] ASC,
	[SentAt] ASC,
	[BetFailedKey] ASC,
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailed_FD(A)] ON [Messaging].[BetFailed]
(
    [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailed_TD(A)] ON [Messaging].[BetFailed]
(
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailed_Resp(A)] ON [Messaging].[BetFailed]
(
	[HasResponse] ASC
) INCLUDE (BetFailedKey, SentAt, MessageID)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)