﻿create table [Messaging].[RewardsVelocityRedemption] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[RewardsVelocityRedemptionKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[ClientID] int,
	[ChannelID] int,
	[Points] bigint,
	[RedemptionValue] bigint,
	[Current] decimal(18,2),
	[Lifetime] decimal(18,2),
	[ExecutedAt] datetime,
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_RewardsVelocityRedemption_Key PRIMARY KEY NONCLUSTERED ([RewardsVelocityRedemptionKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_RewardsVelocityRedemption(A)] ON [Messaging].[RewardsVelocityRedemption]
(
	[ClientID] ASC,
	[ExecutedAt] ASC,
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsVelocityRedemption_FD(A)] ON [Messaging].[RewardsVelocityRedemption]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsVelocityRedemption_TD(A)] ON [Messaging].[RewardsVelocityRedemption]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
