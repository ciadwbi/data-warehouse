﻿create table [Messaging].[RewardsUpdateAddress] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[RewardsUpdateAddressKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[ClientID] int,
	[State] tinyint,
	[ExecutedAt] datetime,
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_RewardsUpdateAddress_Key PRIMARY KEY NONCLUSTERED ([RewardsUpdateAddressKey],[ToDate])
)
go

CREATE UNIQUE CLUSTERED INDEX [CI_RewardsUpdateAddress(A)] ON [Messaging].[RewardsUpdateAddress]
(
	[ClientID] ASC,
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsUpdateAddress_FD(A)] ON [Messaging].[RewardsUpdateAddress]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsUpdateAddress_TD(A)] ON [Messaging].[RewardsUpdateAddress]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
