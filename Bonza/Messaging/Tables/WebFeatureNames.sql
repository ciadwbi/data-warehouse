﻿create table [Messaging].[WebFeatureNames] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[WebFeatureNameKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[WebFeatureKey] int,
	[Name] nvarchar(100),
	CONSTRAINT PK_WebFeatureNames_Key PRIMARY KEY NONCLUSTERED ([WebFeatureNameKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_WebFeatureNames(A)] ON [Messaging].[WebFeatureNames]
(
	[WebFeatureKey] ASC,
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = ON, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_WebFeatureNames_FD(A)] ON [Messaging].[WebFeatureNames]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_WebFeatureNames_TD(A)] ON [Messaging].[WebFeatureNames]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
