﻿create table [Messaging].[RewardsPointsAdjusted] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[RewardsPointsAdjustedKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[ClientID] int,
	[Points] decimal(18,2),
	[Current] decimal(18,2),
	[Lifetime] decimal(18,2),
	[Reason] nvarchar(255),
	[ExecutedAt] datetime,
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_RewardsPointsAdjusted_Key PRIMARY KEY NONCLUSTERED ([RewardsPointsAdjustedKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_RewardsPointsAdjusted(A)] ON [Messaging].[RewardsPointsAdjusted]
(
       [ClientID] ASC,
       [ExecutedAt] ASC,
	   [MessageID] ASC,
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsPointsAdjusted_FD(A)] ON [Messaging].[RewardsPointsAdjusted]
(
       [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_RewardsPointsAdjusted_TD(A)] ON [Messaging].[RewardsPointsAdjusted]
(
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
