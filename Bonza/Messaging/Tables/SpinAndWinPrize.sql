﻿create table [Messaging].[SpinAndWinPrize] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[SpinAndWinPrizeKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
	[BetID] bigint,
	[PrizeID] int,
	[PricePumpMaxStake] decimal(18,2),
	[Desc] nvarchar(255),
	[MessageType] nvarchar(255),
	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_SpinAndWinPrize_Key PRIMARY KEY NONCLUSTERED ([SpinAndWinPrizeKey],[ToDate])
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_SpinAndWinPrize(A)] ON [Messaging].[SpinAndWinPrize]
(
    [BetID] ASC,
	[PrizeID] ASC,
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_SpinAndWinPrize_FD(A)] ON [Messaging].[SpinAndWinPrize]
(
	[FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_SpinAndWinPrize_TD(A)] ON [Messaging].[SpinAndWinPrize]
(
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
