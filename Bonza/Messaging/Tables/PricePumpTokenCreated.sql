﻿create table [Messaging].[PricePumpTokenCreated] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),

	[PricePumpTokenCreatedKey] bigint DEFAULT NEXT VALUE FOR MessagingSequence,
    [XWhaSystemID] nvarchar(255),
    [XWhaChannelID] nvarchar(255),
    [XWhaUserID] nvarchar(255),
    [ClientID] int,
    [TokenType] nvarchar(255),
    [ClientProfile] nvarchar(255),
    [MaxStake] decimal(10,2),
    [Expiry] datetime,
    [TokenID] binary(16),

	[MessageID] binary(16),
	[MessagePublisher] nvarchar(255),
	[SentAt] datetime,
	CONSTRAINT PK_PricePumpTokenCreated_Key PRIMARY KEY NONCLUSTERED ([PricePumpTokenCreatedKey],[ToDate])
)
go

CREATE UNIQUE CLUSTERED INDEX [CI_PricePumpTokenCreated(A)] ON [Messaging].[PricePumpTokenCreated]
(
       [TokenID] ASC,
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_PricePumpTokenCreated_FD(A)] ON [Messaging].[PricePumpTokenCreated]
(
       [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO

CREATE NONCLUSTERED INDEX [NI_PricePumpTokenCreated_TD(A)] ON [Messaging].[PricePumpTokenCreated]
(
       [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
