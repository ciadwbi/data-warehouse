﻿CREATE TABLE [Messaging].[BetFailedResponses] (
	[FromDate] datetime2(3),
	[ToDate] datetime2(3),
	[BetFailedKey] bigint,
	[BetFailedResponseKey] BIGINT, -- DEFAULT NEXT VALUE FOR MessagingSequence,
	[AllowedUnitAmount] float,
	[AllowedTotalAmount] float,
	[MaxAllowedStake] float,
	[CurrentPriceToPlace] float,
	[CurrentPriceToWin] float,
	CONSTRAINT PK_BetFailedResponses_Key PRIMARY KEY NONCLUSTERED ([BetFailedResponseKey],[ToDate])
)
GO
CREATE UNIQUE CLUSTERED INDEX [CI_BetFailedResponses(A)] ON [Messaging].[BetFailedResponses]
(
	[BetFailedKey] ASC,
	[BetFailedResponseKey] ASC,
	[ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailedResponses_FD(A)] ON [Messaging].[BetFailedResponses]
(
    [FromDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
CREATE NONCLUSTERED INDEX [NI_BetFailedResponses_TD(A)] ON [Messaging].[BetFailedResponses]
(
    [ToDate] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)