﻿CREATE TABLE [Atomic].[Position] (
	[BatchKey] [int] NOT NULL,
	[LedgerID] [int] NOT NULL,
	[LedgerSource] [varchar](3) NOT NULL,
	[FromDate] [datetime2](3) NOT NULL,
	[FirstAccrualDate] [datetime2](3) NULL,
	[FirstAccrualBetId] [bigint] NULL,
	[FirstBonusRedemptionDate] [datetime2](3) NULL,
	[FirstVelocityRedemptionDate] [datetime2](3) NULL,
	[LastAccrualDate] [datetime2](3) NULL,
	[LastAccrualBetId] [bigint] NULL,
	[LastBonusRedemptionDate] [datetime2](3) NULL,
	[LastVelocityRedemptionDate] [datetime2](3) NULL,
	[PointsBalance] [money] NOT NULL,
	[AccrualBalance] [money] NOT NULL,
	[NumberOfAccruals] [int] NOT NULL,
	[LifetimeAccrual] [money] NOT NULL,
	[NumberOfBonusRedemptions] [int] NOT NULL,
	[LifetimeBonusRedemption] [money] NOT NULL,
	[NumberOfVelocityRedemptions] [int] NOT NULL,
	[LifetimeVelocityRedemption] [money] NOT NULL,
	[LifetimeRewardCredit] [money] NOT NULL,
	[LifetimeRewardDebit] [money] NOT NULL,
) ON [Staging] ([BatchKey])
GO

ALTER TABLE [Atomic].[Position] SET (LOCK_ESCALATION = AUTO)
GO



