﻿
CREATE TABLE [Atomic].[Transaction] (
    [BatchKey]                   INT			NOT NULL,
    [TransactionId]              BIGINT			NOT NULL,
    [TransactionSource]          CHAR (3)		NOT NULL,
    [MasterTransactionTimestamp] DATETIME2 (7)	NOT NULL,
    [TransactionCode]			 VARCHAR (50)	NOT NULL,
    [UserID]                     VARCHAR (200)  NOT NULL,
    [UserSource]                 CHAR (3)       NOT NULL,
    [InstrumentID]               VARCHAR (50)   NOT NULL,
    [InstrumentSource]           CHAR (3)       NOT NULL,
    [LedgerId]                   INT			NOT NULL,
    [LedgerSource]               CHAR (3)		NOT NULL,
    [PromotionId]                INT			NOT NULL,
    [PromotionSource]            CHAR (3)		NOT NULL,
    [ChannelId]					 SMALLINT		NOT NULL,
    [ChannelSource]	    		 CHAR (3)		NOT NULL,
    [NotesID]					 VARCHAR(255)	NOT NULL,
    [NotesSource]				 CHAR(3)		NOT NULL,
    [Cash]						 MONEY			NOT NULL,
    [Bonus]						 MONEY			NOT NULL,
    [Points]					 MONEY			NOT NULL,
    [VelocityPoints]			 MONEY			NOT NULL,
    [MappingId]                  SMALLINT		NOT NULL,
    [FromDate]                   DATETIME2(3)	NOT NULL,
) ON [Staging] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Atomic].[Transaction] SET (LOCK_ESCALATION = AUTO)
GO



