﻿CREATE TABLE [Atomic].[Withdrawal] (
    [BatchKey]                   INT				NOT NULL,
    [TransactionId]              BIGINT				NOT NULL,
    [TransactionSource]		     CHAR (3)			NOT NULL,
    [GroupId]					 BIGINT				NOT NULL,
    [GroupSource]			     CHAR (3)			NOT NULL,
    [RequestId]                  INT				NULL,
    [WithdrawId]                 INT				NULL,
    [ExternalId]                 VARCHAR (200)		NOT NULL,
    [External1]                  VARCHAR (200)		NOT NULL,
    [External2]                  VARCHAR (200)		NOT NULL,
    [MasterTimestamp]			 DATETIME2 (3)		NOT NULL,
    [TransactionCode]            VARCHAR (50)		NOT NULL,
    [LedgerId]                   INT				NOT NULL,
    [LedgerSource]               CHAR (3)			NOT NULL,
    [UserId]                     VARCHAR (10)		NOT NULL,
    [UserSource]				 CHAR (3)			NOT NULL,
    [InstrumentID]               VARCHAR (20)		NOT NULL,
    [InstrumentSource]			 CHAR (3)			NOT NULL,
    [ChannelId]					 SMALLINT			NOT NULL,
    [ChannelSource]				 CHAR (3)			NOT NULL,
    [NoteId]			         VARCHAR (20)		NOT NULL,
    [NoteSource]				 CHAR (3)			NOT NULL,
    [Amount]	                 MONEY				NOT NULL,
    [MappingId]                  INT				NOT NULL,
    [SourceTransactionCode]      INT				NOT NULL,
    [SourceTransactionDate]      DATETIME2 (3)		NULL,
    [CaptureDate]                DATETIME2 (3)		NOT NULL,
    [FromDate]                   DATETIME2 (3)		NOT NULL
) ON [Staging] ([BatchKey]);
GO

ALTER TABLE [Atomic].[Withdrawal] SET (LOCK_ESCALATION = AUTO)
GO








