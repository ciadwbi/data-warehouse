﻿CREATE TABLE [Atomic].[Lottery] (
    [BatchKey]                   INT				NOT NULL,
    [TransactionId]              BIGINT				NOT NULL,
    [TransactionSource]		     CHAR (3)			NOT NULL,
    [MasterTransactionTimestamp] DATETIME2 (3)		NOT NULL,
    [TransactionCode]            VARCHAR (50)		NOT NULL,
    [TicketId]			         BIGINT				NOT NULL,
    [TicketSource]				 CHAR (3)			NOT NULL,
    [LedgerId]                   BIGINT				NOT NULL,
    [LedgerSource]               CHAR (3)			NOT NULL,
	[Product]					 VARCHAR (255)		NOT NULL,
	[DrawDate]					 DATETIME2(3)		NULL,
    [Stake]		                 MONEY				NOT NULL,
    [Payout]	                 MONEY				NOT NULL,
	[StakeDelta]			     MONEY				NOT NULL,
    [PayoutDelta]				 MONEY				NOT NULL,
    [ExternalId]                 UNIQUEIDENTIFIER	NULL,
    [MappingId]                  INT				NOT NULL,
    [FromDate]                   DATETIME2 (3)		NOT NULL
) ON [Staging] ([BatchKey]);
GO

ALTER TABLE [Atomic].[Lottery] SET (LOCK_ESCALATION = AUTO)
GO








