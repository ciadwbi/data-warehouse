﻿EXEC sp_create_plan_guide
@name = N'BULK:Intrabet-sp_Event:InsertIntoStaging(ThresholdDays=10)',
@stmt = N'
	INSERT	into Stage.Event (BatchKey, Source, SourceEventId, SourceEvent, SourceEventType, ParentEventId, ParentEvent, 
							GrandparentEventId, GrandparentEvent, GreatGrandparentEventId, GreatGrandparentEvent,
							SourceEventDate, ResultedDateTime, SourceRaceNumber, SourceVenueId, SourceVenue, SourceVenueType,SourceVenueEventType, SourceVenueEventTypeName, SourceVenueStateCode,
							SourceVenueState, SourceVenueCountryCode, SourceVenueCountry, ClassID, ClassSource, SourceSubSportType
							, SourceRaceGroup, SourceRaceClass, SourceHybridPricingTemplate, SourcePrizePool, SourceWeightType, SourceDistance, SourceEventAbandoned, EventAbandonedDateTime, SourceLiveStreamPerformId, SourceTrackCondition, SourceEventWeather, FromDate, SourceSubSportTypeId)
	SELECT	@BatchKey BatchKey, 
			''IEI'' Source, 
			e.EventId SourceEventId, 
			e.EventName SourceEvent, 
			et.description SourceEventType,
			ep.EventId SourceParentEventId, 
			ep.EventName SourceParentEvent, 
			egp.EventId SourceGrandparentEventId, 
			egp.EventName SourceGrandparentEvent, 
			eggp.EventId SourceGreatGrandparentEventId, 
			eggp.EventName SourceGreatGrandparentEvent,
			e.EventDate SourceEventDate, 
			R.ResultedDate ResultedDateTime,
			e.RaceNumber SourceRaceNumber,
			v.VenueID SourceVenueId,
			v.VenueName SourceVenue,
			v.VenueType SourceVenueType, 
			v.EventType SourceVenueEventType,
			et2.Description SourceVenueEventTypeName,
			v.stateCode as SourceVenuestateCode, 
			s.StateName as SourceVenueState,
			v.CountryCode as SourceVenueCountryCode, 
			ct.CountryName as SourceVenueCountry,
			e.EventType ClassID, 
			''IBT'' ClassSource,
			sst.[Description] as SourceSubSportType,
			ri.RaceGroup as SourceRaceGroup,
			ri.RaceClass as SourceRaceClass,
			ri.HybridPricingTemplate as SourceHybridPricingTemplate,
			ri.TotalPrizeMoney as SourcePrizePool,
			ri.WeightType as SourceWeightType,
			ri.Distance as SourceDistance,
			e.IsAbandonedEvent SourceEventAbandoned,
			A.AbandonedDate EventAbandonedDateTime,
			e.LiveStreamPerformId,
			ISNULL(tc.Track,tc2.Track) SourceTrackCondition,
			ISNULL(tc.Weather,tc2.Weather) SourceEventWeather,
			c.FromDate,
			e.SportSubType
	FROM	#Candidate c 
			INNER JOIN [$(Acquisition)].IntraBet.tblEvents e  ON (e.EventID = c.EventId AND e.FromDate <= c.FromDate AND e.ToDate > c.FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents ep  ON (ep.EventID = e.MasterEventId AND ep.FromDate <= c.FromDate AND ep.ToDate > c.FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents egp  ON (egp.EventID = ep.MasterEventId AND egp.FromDate <= c.FromDate AND egp.ToDate > c.FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents eggp  ON (eggp.EventID = egp.MasterEventId AND eggp.FromDate <= c.FromDate AND eggp.ToDate > c.FromDate)
			left join [$(Acquisition)].[Intrabet].[tblLUEventTypes] et 
			on et.eventtype = e.eventtype and (et.FromDate<=c.FromDate and et.ToDate>c.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUVenues] v 
			on v.venueid = e.venueid and (v.FromDate<=c.FromDate and v.ToDate>c.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUEventTypes] et2 
			on v.EventType = et2.EventType and (et2.FromDate<=v.FromDate and et2.ToDate>v.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUStates] s 
			on v.StateCode = s.Code and (s.FromDate<=c.FromDate and s.ToDate>c.FromDate)
			left outer join [$(Acquisition)].[IntraBet].[tblLUCountry] ct 
			on v.CountryCode = ct.Country and (ct.FromDate<=c.FromDate and ct.ToDate>c.FromDate)
			LEFT OUTER JOIN [$(Acquisition)].[IntraBet].[tblLUSubSportTypes] sst on e.SportSubType = sst.SportSubType and e.EventType = sst.EventType and sst.ToDate = ''9999-12-31 00:00:00.000''
			LEFT OUTER JOIN #RaceInfo ri on c.EventId = ri.IntrabetID
			LEFT OUTER JOIN #Resulted R on R.EventID=C.EventId
			LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTrackConditions tc on e.VenueID=tc.VenueID and ISNULL(v.EventType,e.EventType) = tc.EventType and c.FromDate>=tc.FromDate and c.FromDate<tc.ToDate
			LEFT OUTER JOIN [$(Acquisition)].IntraBet.tblTrackConditions tc2 on e.VenueID=tc2.VenueID and c.FromDate>=tc2.FromDate and c.FromDate<tc2.ToDate and tc.VenueID is null
			LEFT JOIN #Abandoned A on c.EventId=A.EventID
where isnull(e.IsMasterEvent,0)<>1
OPTION(RECOMPILE, TABLE HINT(e, FORCESEEK), TABLE HINT(ep, FORCESEEK), TABLE HINT(egp, FORCESEEK), TABLE HINT(eggp, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Intrabet.sp_Event',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'
