﻿IF '$(ServerName)' <> '$(LiveServerName)' 
BEGIN
	DROP SYNONYM [Control].[LiveETLController]
	CREATE SYNONYM [Control].[LiveETLController] FOR [$(LiveServerName)].[$(DatabaseName)].[Control].[ETLController]
END
GO