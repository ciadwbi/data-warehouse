﻿EXEC sp_create_plan_guide
@name = N'BULK:Dimension-sp_Account:CombinedChanges(ThresholdDays=10)',
@stmt = N'
	SELECT	AccountId, ClientId, AccountNumber, MAX(FirstDate) FirstDate, MAX(FromDate) FromDate, MAX(NewLedger) NewLedger
	INTO	#Candidates
	FROM	(	SELECT	AccountId, ClientId, AccountNumber, FirstDate, FromDate, NewLedger
				FROM	#Accounts
				UNION ALL
				SELECT	a.AccountId, a.ClientId, a.AccountNumber, c.FirstDate, c.FromDate, 0 NewLedger
				FROM	#Clients c
						INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON (a.ClientID = c.ClientID AND a.FromDate <= c.FromDate AND a.ToDate > c.Fromdate)
			) x
	GROUP BY AccountId, ClientId, AccountNumber
	OPTION (RECOMPILE, TABLE HINT(a, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Dimension.sp_Account',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'
EXEC sp_control_plan_guide 
@operation = N'DISABLE',  
@name = N'BULK:Dimension-sp_Account:CombinedChanges(ThresholdDays=10)'

EXEC sp_create_plan_guide
@name = N'BULK:Dimension-sp_Account:MissingOpenDate(ThresholdDays=10)',
@stmt = N'
	SELECT	c.AccountId, c.ClientID, c.AccountNumber, c.FromDate
	INTO	#MissingOpenDate
	FROM	#Candidates c
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientInfo i ON i.ClientID = c.ClientID AND i.FromDate <= c.FromDate AND i.ToDate > c.FromDate
	WHERE	c.AccountNumber > 1 OR i.StartDate = CONVERT(datetime,''1900-01-01'') OR i.StartDate IS NULL
	OPTION (RECOMPILE, TABLE HINT(i, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Dimension.sp_Account',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'
EXEC sp_control_plan_guide 
@operation = N'DISABLE',  
@name = N'BULK:Dimension-sp_Account:MissingOpenDate(ThresholdDays=10)'

EXEC sp_create_plan_guide
@name = N'BULK:Dimension-sp_Account:DerivedOpenDate(ThresholdDays=10)',
@stmt = N'
	SELECT	c.AccountId, MIN(ISNULL(t.TransactionDate,a.FromDate)) AccountOpenedDate, CASE COUNT(t.ClientId) WHEN 0 THEN ''Account'' ELSE ''Transaction'' END Origin
	INTO	#DerivedOpenDate
	FROM	#MissingOpenDate c
			INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON (a.AccountID = c.AccountID)
			LEFT JOIN [$(Acquisition)].IntraBet.tblTransactions t ON (t.ClientID = c.ClientID AND t.AccountNumber = c.AccountNumber AND t.FromDate = CONVERT(datetime2(3),''2013-12-19'') AND a.FromDate = CONVERT(datetime2(3),''2013-12-19''))
	GROUP BY c.AccountID
	OPTION (RECOMPILE, TABLE HINT(a, FORCESEEK), TABLE HINT(t, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Dimension.sp_Account',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'
EXEC sp_control_plan_guide 
@operation = N'DISABLE',  
@name = N'BULK:Dimension-sp_Account:DerivedOpenDate(ThresholdDays=10)'

EXEC sp_create_plan_guide
@name = N'BULK:Dimension-sp_Account:CardIssue(ThresholdDays=10)',
@stmt = N'
	SELECT	c.ClientID, MIN(d.Created) CardFirstIssueDate, MAX(d.Created) CardIssueDate
	INTO	#CardIssue
	FROM	#Candidates c
			INNER JOIN [$(Acquisition)].[IntraBet].Cards d ON d.ClientID = c.ClientID AND d.FromDate <= c.FromDate AND d.ToDate > c.FromDate
	GROUP BY c.ClientID
	OPTION (RECOMPILE, TABLE HINT(d, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Dimension.sp_Account',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'
EXEC sp_control_plan_guide 
@operation = N'DISABLE',  
@name = N'BULK:Dimension-sp_Account:CardIssue(ThresholdDays=10)'

EXEC sp_create_plan_guide
@name = N'BULK:Dimension-sp_Account:PasswordChange(ThresholdDays=10)',
@stmt = N'
	SELECT	c.ClientID, MAX(i1.ToDate) PasswordLastChanged
	INTO	#PasswordChange
	FROM	#Candidates c
			INNER JOIN [$(Acquisition)].[IntraBet].tblClients i ON i.ClientID = c.ClientID AND i.FromDate <= c.FromDate AND i.ToDate > c.FromDate
			INNER JOIN [$(Acquisition)].[IntraBet].tblClients i1 ON i1.ClientID = c.ClientID AND i1.ToDate <= i.FromDate AND i1.PWDHash <> i.PWDHash 
	GROUP BY c.ClientID
	OPTION (RECOMPILE, TABLE HINT(i, FORCESEEK), TABLE HINT(i1, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Dimension.sp_Account',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'
EXEC sp_control_plan_guide 
@operation = N'DISABLE',  
@name = N'BULK:Dimension-sp_Account:PasswordChange(ThresholdDays=10)'

EXEC sp_create_plan_guide
@name = N'BULK:Dimension-sp_Account:Assemble(ThresholdDays=10)',
@stmt = N'
	SELECT	x.AccountID LedgerId,
			''IAA'' as LedgerSource,
			CASE ISNULL(C.CB_Client_key,0) WHEN 0 THEN x.AccountID ELSE c.CB_Client_key END LegacyLedgerID,
			x.AccountNumber LedgerSequenceNumber,
			ISNULL(dd.AccountOpenedDate, ci.StartDate) LedgerOpenedDate,
			CONVERT(char(10),CONVERT(date,ISNULL(dd.AccountOpenedDate, ci.StartDate))) LedgerOpenedDayText,
			ISNULL(dd.Origin, ''Client'') LedgerOpenedDateOrigin,
			a.Ledger LedgerTypeID,
			ISNULL(l.LedgerDescription,''Unknown'') LedgerType,
			x.ClientID AccountID,
			''IAC'' AccountSource,
			c.PIN,
			c.Username,
			c.PWDHash PasswordHash,
			pw.PasswordLastChanged,
			ISNULL(CONVERT(varchar(36), sc.exactTargetID),''N/A'') ExactTargetID,
			c.ClientType AccountFlags,
			ISNULL(ci.Channel, -1) AccountOpenedChannelId,
			ISNULL(f.ReferrerClientId,0) ReferralAccountID,
			CASE WHEN f.ReferrerClientId IS NULL THEN ''N/A'' ELSE ''IAC'' END ReferralAccountSource,
			ISNULL(f.BurnCode, ''N/A'') ReferralBurnCode,
			CONVERT(datetime2(3),DATEADD(minute,@MessagingTimeShift,f.BurntDateUTC)) ReferralBurntDate,
			cai.BTag2 SourceBTag2,
			cai.TrafficSource SourceTrafficSource,
			cai.RefURL SourceRefURL,
			cai.CampaignID SourceCampaignID,
			cai.Keywords SourceKeywords,
			af.AffiliateName COLLATE database_default SourceAffiliateName,
			af.SitePromoName COLLATE database_default SourceSitePromoName,
			CONVERT(VARCHAR(20), tad.Channel) [TrackingChannel],
			CONVERT(VARCHAR(20), tad.App_Id) [TrackingAppId],
			CONVERT(VARCHAR(50), tad.[App_Name]) [TrackingAppName],
			CONVERT(VARCHAR(20), tad.App_Version) [TrackingAppVersion],
			CONVERT(VARCHAR(255), tad.Event_Name) [TrackingEventName],
			CONVERT(VARCHAR(50), tad.Tracker) [TrackingTracker],
			CONVERT(VARCHAR(MAX), tad.Tracker_Name) [TrackingTrackerName],
			CONVERT(VARCHAR(255), tad.Adwords_Campaign_Type) [TrackingAdwordsCampaignType],
			CONVERT(VARCHAR(255), tad.Adwords_Campaign_Name) [TrackingAdwordsCampaignName],
			CONVERT(VARCHAR(255), tad.Fb_Campaign_Group_Name) [TrackingFbCampaignGroupName],
			CONVERT(VARCHAR(255), tad.Fb_Campaign_Name) [TrackingFbCampaignName],
			CONVERT(VARCHAR(255), tad.Ip_Address) [TrackingIpAddress],
			CONVERT(VARCHAR(MAX), tad.Partner_Parameters) [TrackingPartnerParameters],
			CONVERT(VARCHAR(30), tad.dcp_btag) [TrackingDcpBtag],
			CONVERT(VARCHAR(30), tad.dcp_campaign) [TrackingDcpCampaign],
			CONVERT(VARCHAR(30), tad.dcp_keyword) [TrackingDcpKeyword],
			CONVERT(VARCHAR(30), tad.dcp_publisher) [TrackingDcpPublisher],
			CONVERT(VARCHAR(30), tad.dcp_source) [TrackingDcpSource],
			CONVERT(VARCHAR(30), tad.label) [TrackingLabel],
			CONVERT(VARCHAR(30), tad.publisher) [TrackingPublisher],
			CONVERT(VARCHAR(30), tad.keyword) [TrackingKeyword],
			CONVERT(VARCHAR(30), tad.campaign) [TrackingCampaign],
			CONVERT(VARCHAR(30), tad.btag) [TrackingBtag],
			CONVERT(VARCHAR(100), tad.source) [TrackingSource],
			ISNULL(sm.[Description], ''Unknown'') StatementMethod,
			ISNULL(sf.[Description], ''Unknown'') StatementFrequency,
			cd.ExternalAccountId CardAccountId,
			cs.CardFirstIssueDate,
			cs.CardIssueDate,
			cd.CardStatus CardStatusId,
			ew.AccountId EachwayAccountId,
			ISNULL(re.RewardStatusId,0) RewardStatusId,
			re.RewardStatusDate,
			vl.VelocityNumber, 
			vl.VelocityLinkedDate,
			c.Title AccountTitle,
			c.Surname AccountSurname,
			c.FirstName AccountFirstName,
			c.MiddleName AccountMiddleName,
			c.Gender AccountGender,
			c.DOB AccountDOB,
			CASE WHEN ha.AddressType IS NOT NULL THEN 1 ELSE 0 END HasHomeAddress,
			hl.AddressLastChanged HomeAddressLastChanged,
			hl.PhoneLastChanged HomePhoneLastChanged,
			hl.EmailLastChanged HomeEmailLastChanged,
			ha.Address1 HomeAddress1,
			ha.Address2 HomeAddress2,
			ha.Suburb HomeSuburb,
			ha.State HomeStateCode,
			hs.StateName HomeState,
			ha.PostCode HomePostCode,
			hc.CountryAbbr HomeCountryCode,
			hc.CountryName HomeCountry,
			ha.Phone HomePhone,
			ha.Mobile HomeMobile,
			ha.Fax HomeFax,
			ha.Email HomeEmail,
			ha.altEmail HomeAlternateEmail,
			CASE WHEN pa.AddressType IS NOT NULL THEN 1 ELSE 0 END HasPostalAddress,
			pl.AddressLastChanged PostalAddressLastChanged,
			pl.PhoneLastChanged PostalPhoneLastChanged,
			pl.EmailLastChanged PostalEmailLastChanged,
			pa.Address1 PostalAddress1,
			pa.Address2 PostalAddress2,
			pa.Suburb PostalSuburb,
			pa.State PostalStateCode,
			ps.StateName PostalState,
			pa.PostCode PostalPostCode,
			pc.CountryAbbr PostalCountryCode,
			pc.CountryName PostalCountry,
			pa.Phone PostalPhone,
			pa.Mobile PostalMobile,
			pa.Fax PostalFax,
			pa.Email PostalEmail,
			pa.altEmail PostalAlternateEmail,
			CASE WHEN ba.AddressType IS NOT NULL THEN 1 ELSE 0 END HasBusinessAddress,
			bl.AddressLastChanged BusinessAddressLastChanged,
			bl.PhoneLastChanged BusinessPhoneLastChanged,
			bl.EmailLastChanged BusinessEmailLastChanged,
			ba.Address1 BusinessAddress1,
			ba.Address2 BusinessAddress2,
			ba.Suburb BusinessSuburb,
			ba.State BusinessStateCode,
			bs.StateName BusinessState,
			ba.PostCode BusinessPostCode,
			bc.CountryAbbr BusinessCountryCode,
			bc.CountryName BusinessCountry,
			ba.Phone BusinessPhone,
			ba.Mobile BusinessMobile,
			ba.Fax BusinessFax,
			ba.Email BusinessEmail,
			ba.altEmail BusinessAlternateEmail,
			CASE WHEN oa.AddressType IS NOT NULL THEN 1 ELSE 0 END HasOtherAddress,
			ol.AddressLastChanged OtherAddressLastChanged,
			ol.PhoneLastChanged OtherPhoneLastChanged,
			ol.EmailLastChanged OtherEmailLastChanged,
			oa.Address1 OtherAddress1,
			oa.Address2 OtherAddress2,
			oa.Suburb OtherSuburb,
			oa.State OtherStateCode,
			os.StateName OtherState,
			oa.PostCode OtherPostCode,
			oc.CountryAbbr OtherCountryCode,
			oc.CountryName OtherCountry,
			oa.Phone OtherPhone,
			oa.Mobile OtherMobile,
			oa.Fax OtherFax,
			oa.Email OtherEmail,
			oa.altEmail OtherAlternateEmail,
			ISNULL(l1.CorrectedDate,x.FirstDate) FirstDate,
			ISNULL(l2.CorrectedDate,x.FromDate) FromDate
	INTO	#Account
	FROM	#Candidates x
			INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON a.AccountID = x.AccountID AND a.FromDate <= x.FromDate AND a.ToDate > x.FromDate
			LEFT JOIN #DerivedOpenDate dd ON dd.AccountID = x.AccountID
			LEFT JOIN #PasswordChange pw ON pw.ClientID = x.ClientID
			LEFT JOIN [$(Acquisition)].IntraBet.tblLULedgers l ON l.Ledger = a.Ledger AND l.FromDate <= x.FromDate AND l.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClients c ON c.ClientID = x.ClientID AND c.FromDate <= x.FromDate AND c.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClientInfo ci ON ci.ClientID = x.ClientID AND ci.FromDate <= x.FromDate AND ci.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.Friend f ON f.FriendClientId = x.ClientID AND f.FromDate <= x.FromDate AND f.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre sc ON sc.clientID = x.ClientID AND sc.FromDate <= x.FromDate AND sc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClientAffiliateInfo cai ON cai.clientID = x.ClientID AND cai.FromDate <= x.FromDate AND cai.ToDate > x.FromDate
			LEFT JOIN #Affiliates af ON af.BTag2 = cai.BTag2 AND af.FromDate <= x.FromDate AND af.ToDate > x.FromDate
			LEFT JOIN #TrackingAdjustData tad ON tad.clientID = x.ClientID AND tad.FromDate <= x.FromDate AND tad.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblLUStatementMethods sm ON sm.StatementMethod = ci.StatementMethod AND sm.FromDate <= x.FromDate AND sm.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblLUStatementFrequency sf ON sf.StatementFrequency = ci.StatementFrequency AND sf.FromDate <= x.FromDate AND sf.ToDate > x.FromDate
			LEFT JOIN #CardIssue cs ON cs.ClientID = x.ClientID
			LEFT JOIN [$(Acquisition)].[IntraBet].Cards cd ON cd.ClientId = x.ClientID AND cd.Created = cs.CardIssueDate AND cd.FromDate <= x.FromDate AND cd.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].EachwayAccounts ew ON ew.ClientId = x.ClientID AND ew.ExternalAccountId = cd.ExternalAccountId AND ew.FromDate <= x.FromDate AND ew.ToDate > x.FromDate
			LEFT JOIN #RewardsEligibility re ON re.ClientId = x.ClientId AND re.FromDate = x.FromDate
			LEFT JOIN #RewardsVelocityLink vl ON vl.ClientId = x.ClientId AND vl.FromDate = x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses ha ON ha.ClientID = x.ClientID AND ha.FromDate <= x.FromDate AND ha.ToDate > x.FromDate AND ha.AddressType=1
			LEFT JOIN #AddressChange hl ON hl.ClientID = x.ClientID AND hl.AddressType=1
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates hs ON hs.Code = ha.StateId AND hs.FromDate <= x.FromDate AND hs.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry hc ON hc.Country = ha.Country AND hc.FromDate <= x.FromDate AND hc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses pa ON pa.ClientID = x.ClientID AND pa.FromDate <= x.FromDate AND pa.ToDate > x.FromDate AND pa.AddressType=2
			LEFT JOIN #AddressChange pl ON pl.ClientID = x.ClientID AND pl.AddressType=2
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates ps ON ps.Code = pa.StateId AND ps.FromDate <= x.FromDate AND ps.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry pc ON pc.Country = pa.Country AND pc.FromDate <= x.FromDate AND pc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses ba ON ba.ClientID = x.ClientID AND ba.FromDate <= x.FromDate AND ba.ToDate > x.FromDate AND ba.AddressType=3
			LEFT JOIN #AddressChange bl ON bl.ClientID = x.ClientID AND bl.AddressType=3
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates bs ON bs.Code = ba.StateId AND bs.FromDate <= x.FromDate AND bs.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry bc ON bc.Country = ba.Country AND bc.FromDate <= x.FromDate AND bc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses oa ON oa.ClientID = x.ClientID AND oa.FromDate <= x.FromDate AND oa.ToDate > x.FromDate AND oa.AddressType=4
			LEFT JOIN #AddressChange ol ON ol.ClientID = x.ClientID AND ol.AddressType=4
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates os ON os.Code = oa.StateId AND os.FromDate <= x.FromDate AND os.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry oc ON oc.Country = oa.Country AND oc.FromDate <= x.FromDate AND oc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].Intrabet.Latency  l1 WITH(FORCESEEK)  on l1.OriginalDate = x.FirstDate --AND l1.OriginalDate >= @FromDate
			LEFT JOIN [$(Acquisition)].Intrabet.Latency  l2 WITH(FORCESEEK)  on l2.OriginalDate = x.FromDate --AND l2.OriginalDate >= @FromDate
	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN) --TABLE HINT(c, FORCESEEK), TABLE HINT(ci, FORCESEEK), TABLE HINT(cai, FORCESEEK), TABLE HINT(sc, FORCESEEK), TABLE HINT(ha, FORCESEEK), TABLE HINT(pa, FORCESEEK), TABLE HINT(ba, FORCESEEK), TABLE HINT(oa, FORCESEEK))
', 
@type = N'OBJECT',
@module_or_batch = 'Dimension.sp_Account',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'
EXEC sp_control_plan_guide 
@operation = N'DISABLE',  
@name = N'BULK:Dimension-sp_Account:Assemble(ThresholdDays=10)'


