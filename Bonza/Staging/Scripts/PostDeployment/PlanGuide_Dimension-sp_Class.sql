﻿EXEC sp_create_plan_guide
@name = N'BULK:Dimension-sp_Class:EventClass(ThresholdDays=10)',
@stmt = N'
	SELECT	''IBT'' Source,
			CONVERT(varchar(32),x.EventType) ClassId,
			e.Description SourceClass, 
			CONVERT(varchar(32),x.SportSubType) SubClassId, 
			s.Description SourceSubClass,
			CONVERT(varchar(255),ve.Description) Keyword,
			COALESCE(CASE WHEN s.FromDate > e.FromDate THEN s.FromDate ELSE e.FromDate END, e.FromDate, s.FromDate, ''1900-01-01'') FromDate
	INTO	#Events
	FROM	(	SELECT	z.EventType, z.SportSubType, v.EventType VenueEventType, MIN(z.FromDate) FromDate
				FROM	[$(Acquisition)].[IntraBet].tblEvents z
						LEFT JOIN [$(Acquisition)].[IntraBet].tblLUVenues v ON v.VenueID = z.VenueID AND v.FromDate <= z.FromDate AND v.ToDate > z.FromDate
				WHERE	z.FromDate > @FromDate AND z.FromDate <= @ToDate AND z.ToDate > @FromDate
						AND ISNULL(z.IsMasterEvent,0) = 0
						AND REPLACE(z.EventName,''X'','''') <> ''''
				GROUP BY z.SportSubType, z.EventType, v.EventType
				HAVING	MAX(z.ToDate) > @ToDate -- only worry about combinations that persist beyond the current ETL batch (we get a lot of temporary edits that only last for seconds or minutes)
			) x
			-- A bit metadata anal - pull out the last LU records for when the (Sub)Class pair was first seen in the period to use as FromDate 
			-- If the names have subsequently changed in the ETL period, that will be picked up by the subsequent (Sub)Class changes
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUEventTypes e ON e.EventType = x.EventType AND e.FromDate <= x.FromDate AND e.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUSubSportTypes s ON s.SportSubType = x.SportSubType AND s.FromDate <= x.FromDate AND s.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUEventTypes ve ON ve.EventType = x.VenueEventType AND ve.FromDate <= x.FromDate AND ve.ToDate > x.FromDate
	OPTION (RECOMPILE, TABLE HINT(z, FORCESEEK([NI_tblEvents_FD(A)](FromDate))))
', 
@type = N'OBJECT',
@module_or_batch = 'Dimension.sp_Class',
@params = NULL,
@hints = N'OPTION (RECOMPILE)'
EXEC sp_control_plan_guide 
@operation = N'DISABLE',  
@name = N'BULK:Dimension-sp_Class:EventClass(ThresholdDays=10)'


