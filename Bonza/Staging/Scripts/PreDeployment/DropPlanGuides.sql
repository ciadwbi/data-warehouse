﻿--DROP all "BULK" Plan Guides referencing stored procedures in case any of the stored procedures is being redeployed
BEGIN
	PRINT N'Dropping Plan Guides referencing Stored Procedures...';
	
	DECLARE	@i int,
			@SQL varchar(MAX)

	BEGIN TRY DROP TABLE #PlanGuides END TRY BEGIN CATCH END CATCH

	SELECT	ROW_NUMBER() OVER (ORDER BY name DESC) i,
			'sp_control_plan_guide ''DROP'', ''' + name + '''' sql
	INTO	#PlanGuides
	FROM	sys.plan_guides
	WHERE	scope_type_desc = 'OBJECT'
			AND name LIKE '%BULK%'

	SET @i = @@ROWCOUNT

	WHILE @i > 0
		BEGIN
			SELECT	@SQL = sql FROM	#PlanGuides	WHERE i = @i
			EXEC(@SQL)
			SET @i = @i - 1	
		END
END
GO
