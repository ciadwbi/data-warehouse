﻿CREATE FUNCTION [Reference].[fn_Mapping_Cashflow]
(
	@TransactionType varchar(50) = NULL
)
RETURNS @CashflowMapping TABLE ( TransactionCode varchar(50), TransactionTypeId char(2), TransactionStatusId char(2), TransactionMethodId char(2), BalanceTypeId char(3), TransactionDirection char(2), AmountFactor money, StakeFactor money, PayoutFactor money) 
AS
BEGIN
	WITH 
		CashflowMapping (	TransactionCode,					TransactionTypeId,	TransactionStatusId,	TransactionMethodId,	BalanceTypeId,	TransactionDirection,	AmountFactor,	StakeFactor,	PayoutFactor	)
		AS	(	-- Lottery		
				SELECT		'Lottery.Place',					'BT',				'RE',					'SY',					'CLI',			'DR',					NULL,			-1,				0				UNION ALL
				SELECT		'Lottery.Place',					'BT',				'RE',					'SY',					'INT',			'CR',					NULL,			+1,				0				UNION ALL
				SELECT		'Lottery.Place',					'BT',				'AC',					'SY',					'INT',			'DR',					NULL,			-1,				0				UNION ALL
				SELECT		'Lottery.Place',					'BT',				'AC',					'SY',					'UNS',			'CR',					NULL,			+1,				0				UNION ALL
				SELECT		'Lottery.Cancel',					'BT',				'CN',					'VO',					'UNS',			'DR',					NULL,			-1,				0				UNION ALL
				SELECT		'Lottery.Cancel',					'BT',				'CN',					'VO',					'CLI',			'CR',					NULL,			+1,				0				UNION ALL
				SELECT		'Lottery.Loss',						'BT',				'LO',					'SE',					'UNS',			'DR',					NULL,			-1,				0				UNION ALL
				SELECT		'Lottery.Loss',						'BT',				'LO',					'SE',					'P&L',			'CR',					NULL,			+1,				0				UNION ALL
				SELECT		'Lottery.Win',						'BT',				'WN',					'SE',					'UNS',			'DR',					NULL,			-1,				0				UNION ALL	
				SELECT		'Lottery.Win',						'BT',				'WN',					'SE',					'P&L',			'DR',					NULL,			+1,				-1				UNION ALL	
				SELECT		'Lottery.Win',						'BT',				'WN',					'SE',					'CLI',			'CR',					NULL,			0,				+1				UNION ALL
				-- Withdrawal
				SELECT		'Withdrawal.Complete.BPay',			'WI',				'CO',					'BY',					'PWD',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Complete.CashCard',		'WI',				'CO',					'CD',					'PWD',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Complete.Cheque',		'WI',				'CO',					'CH',					'PWD',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Complete.EFT',			'WI',				'CO',					'EF',					'PWD',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Complete.Moneybookers', 'WI',				'CO',					'MB',					'PWD',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Complete.Neteller',		'WI',				'CO',					'NE',					'PWD',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Complete.PayPal',		'WI',				'CO',					'PP',					'PWD',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Complete.Unknown',		'WI',				'CO',					'UK',					'PWD',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.BPay',			'WI',				'DE',					'BY',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.BPay',			'WI',				'DE',					'BY',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.Cheque',		'WI',				'DE',					'CH',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.Cheque',		'WI',				'DE',					'CH',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.EFT',			'WI',				'DE',					'EF',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.EFT',			'WI',				'DE',					'EF',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.Moneybookers',	'WI',				'DE',					'MB',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.Moneybookers',	'WI',				'DE',					'MB',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.Neteller',		'WI',				'DE',					'NE',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.Neteller',		'WI',				'DE',					'NE',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.PayPal',		'WI',				'DE',					'PP',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.PayPal',		'WI',				'DE',					'PP',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.Unknown',		'WI',				'DE',					'UK',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Decline.Unknown',		'WI',				'DE',					'UK',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.BPay',			'WI',				'RE',					'BY',					'CLI',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.BPay',			'WI',				'RE',					'BY',					'PWD',			'CR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.CashCard',		'WI',				'RE',					'CD',					'CLI',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.CashCard',		'WI',				'RE',					'CD',					'PWD',			'CR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.Cheque',		'WI',				'RE',					'CH',					'CLI',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.Cheque',		'WI',				'RE',					'CH',					'PWD',			'CR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.EFT',			'WI',				'RE',					'EF',					'CLI',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.EFT',			'WI',				'RE',					'EF',					'PWD',			'CR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.Moneybookers',	'WI',				'RE',					'MB',					'CLI',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.Moneybookers',	'WI',				'RE',					'MB',					'PWD',			'CR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.Neteller',		'WI',				'RE',					'NE',					'CLI',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.Neteller',		'WI',				'RE',					'NE',					'PWD',			'CR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.PayPal',		'WI',				'RE',					'PP',					'CLI',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.PayPal',		'WI',				'RE',					'PP',					'PWD',			'CR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.Unknown',		'WI',				'RE',					'UK',					'CLI',			'DR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Request.Unknown',		'WI',				'RE',					'UK',					'PWD',			'CR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reject.BPay',			'WI',				'RJ',					'BY',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reject.CashCard',		'WI',				'RJ',					'CD',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reject.Cheque',			'WI',				'RJ',					'CH',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reject.EFT',			'WI',				'RJ',					'EF',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reject.Moneybookers',	'WI',				'RJ',					'MB',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reject.Neteller',		'WI',				'RJ',					'NE',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reject.PayPal',			'WI',				'RJ',					'PP',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reject.Unknown',		'WI',				'RJ',					'UK',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.BPay',			'WI',				'RV',					'BY',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.BPay',			'WI',				'RV',					'BY',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.Cheque',		'WI',				'RV',					'CH',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.Cheque',		'WI',				'RV',					'CH',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.EFT',			'WI',				'RV',					'EF',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.EFT',			'WI',				'RV',					'EF',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.Moneybookers',	'WI',				'RV',					'MB',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.Moneybookers',	'WI',				'RV',					'MB',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.Neteller',		'WI',				'RV',					'NE',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.Neteller',		'WI',				'RV',					'NE',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.PayPal',		'WI',				'RV',					'PP',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.PayPal',		'WI',				'RV',					'PP',					'PWD',			'DR',					+1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.Unknown',		'WI',				'RV',					'UK',					'CLI',			'CR',					-1,				NULL,			NULL			UNION ALL
				SELECT		'Withdrawal.Reverse.Unknown',		'WI',				'RV',					'UK',					'PWD',			'DR',					+1,				NULL,			NULL
			)
		INSERT @CashflowMapping SELECT * FROM CashflowMapping WHERE TransactionCode LIKE ISNULL(@TransactionType,'%') + '.%'

	RETURN
END

