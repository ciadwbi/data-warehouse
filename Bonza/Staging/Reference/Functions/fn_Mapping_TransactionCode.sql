﻿CREATE FUNCTION [Reference].[fn_Mapping_TransactionCode]
(
	@TransactionType varchar(50) = NULL
)
RETURNS @TransactionCode TABLE ( TransactionCode int, TransactionType varchar(32), TransactionStatus varchar(32), TransactionMethod varchar(32)) 
AS
BEGIN
	WITH 
		TransactionCode (	TransactionCode,	TransactionType,	TransactionStatus,	TransactionMethod	)
		AS	(	SELECT		5,					'Withdrawal',		'Complete',			'Unknown'			UNION ALL
				SELECT		7,					'Withdrawal',		'Complete',			'Cheque'			UNION ALL
				SELECT		8,					'Withdrawal',		'Complete',			'Cheque'			UNION ALL
				SELECT		10,					'Withdrawal',		'Request',			'EFT'				UNION ALL
				SELECT		11,					'Withdrawal',		'Request',			'Cheque'			UNION ALL
				SELECT		14,					'Withdrawal',		'Request',			'BPay'				UNION ALL
				SELECT		17,					'Withdrawal',		'Request',			'EFT'				UNION ALL
				SELECT		18,					'Withdrawal',		'Complete',			'Unknown'			UNION ALL
				SELECT		25,					'Withdrawal',		'Request',			'PayPal'			UNION ALL
				SELECT		26,					'Withdrawal',		'Complete',			'PayPal'			UNION ALL
				SELECT		27,					'Withdrawal',		'Request',			'Moneybookers'		UNION ALL
				SELECT		28,					'Withdrawal',		'Complete',			'Moneybookers'		UNION ALL
				SELECT		29,					'Withdrawal',		'Request',			'Neteller'			UNION ALL
				SELECT		30,					'Withdrawal',		'Complete',			'Neteller'			UNION ALL
				SELECT		31,					'Withdrawal',		'Request',			NULL				UNION ALL -- Methods left NULL because reversal Transaction Codes are for all Methods 
				SELECT		32,					'Withdrawal',		'Reverse',			NULL				UNION ALL -- and the specific method for a reversal transaction must be obtained from
				SELECT		33,					'Withdrawal',		'Reverse',			NULL				UNION ALL -- related non-reversal transaction records -- see logic in 'Transaction' step
				SELECT		501,				'Withdrawal',		'Complete',			'EFT'				UNION ALL
				SELECT		502,				'Withdrawal',		'Complete',			'BPay'				UNION ALL
				SELECT		1002,				'Withdrawal',		'Complete',			'CashCard'			UNION ALL
				SELECT		5022,				'Withdrawal',		'Complete',			'Unknown'			UNION ALL
				SELECT		5026,				'Withdrawal',		'Complete',			'Unknown'			UNION ALL
				SELECT		6045,				'Withdrawal',		'Complete',			'EFT'			
			)
	INSERT @TransactionCode SELECT * FROM TransactionCode WHERE TransactionType = @TransactionType OR @TransactionType IS NULL

	RETURN
END

