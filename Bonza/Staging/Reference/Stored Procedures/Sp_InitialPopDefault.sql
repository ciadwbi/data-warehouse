Create Procedure Reference.Sp_InitialPopDefault

as

IF not exists (Select [DefaultKey] From [Reference].[Default])
Begin
begin transaction
SET IDENTITY_INSERT [Reference].[Default] ON 

INSERT [Reference].[Default] ([DefaultKey], [ToValue], [Comment]) VALUES (1, 'Unknow', 'Value Expected but Not Available')
INSERT [Reference].[Default] ([DefaultKey], [ToValue], [Comment]) VALUES (2, 'N/A', 'Value Not Available')
INSERT [Reference].[Default] ([DefaultKey], [ToValue], [Comment]) VALUES (3, 'X', 'X')
INSERT [Reference].[Default] ([DefaultKey], [ToValue], [Comment]) VALUES (4, '0', '0')
INSERT [Reference].[Default] ([DefaultKey], [ToValue], [Comment]) VALUES (5, '-1', '-1')
INSERT [Reference].[Default] ([DefaultKey], [ToValue], [Comment]) VALUES (6, 'Client', 'Default Account Type')

SET IDENTITY_INSERT [Reference].[Default] OFF
If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End