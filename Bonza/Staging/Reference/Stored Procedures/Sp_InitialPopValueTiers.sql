﻿Create Procedure Reference.Sp_InitialPopValueTiers

AS

IF not exists (Select [TierName] From [Reference].[ValueTiers])
Begin
begin transaction

INSERT [Reference].[ValueTiers] ([TierNumber], [TierName], [ValueFloor], [ValueCeiling]) VALUES (8, N'<500', 0.0001, 500.0000)
INSERT [Reference].[ValueTiers] ([TierNumber], [TierName], [ValueFloor], [ValueCeiling]) VALUES (7, N'500 <= X < 1000', 500.0000, 1000.0000)
INSERT [Reference].[ValueTiers] ([TierNumber], [TierName], [ValueFloor], [ValueCeiling]) VALUES (6, N'1000 <= X < 5000', 1000.0000, 5000.0000)
INSERT [Reference].[ValueTiers] ([TierNumber], [TierName], [ValueFloor], [ValueCeiling]) VALUES (5, N'5000 <= X < 50000', 5000.0000, 50000.0000)
INSERT [Reference].[ValueTiers] ([TierNumber], [TierName], [ValueFloor], [ValueCeiling]) VALUES (4, N'50000 <= X < 200000', 50000.0000, 200000.0000)
INSERT [Reference].[ValueTiers] ([TierNumber], [TierName], [ValueFloor], [ValueCeiling]) VALUES (3, N'200000 <= X < 500000', 200000.0000, 500000.0000)
INSERT [Reference].[ValueTiers] ([TierNumber], [TierName], [ValueFloor], [ValueCeiling]) VALUES (2, N'500000 <= X < 1000000', 500000.0000, 1000000.0000)
INSERT [Reference].[ValueTiers] ([TierNumber], [TierName], [ValueFloor], [ValueCeiling]) VALUES (1, N'>= 1000000', 1000000.0000, 999999999.0000)
INSERT [Reference].[ValueTiers] ([TierNumber], [TierName], [ValueFloor], [ValueCeiling]) VALUES (0, N'0', 0.0000, 0.0001)

If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End