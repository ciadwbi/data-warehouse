﻿Create Procedure Reference.Sp_InitialPopMapping

as

IF not exists (Select [MappingKey] From [Reference].[Mapping])
Begin
begin transaction
SET IDENTITY_INSERT [Reference].[Mapping] ON 

INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (1, 'Acquisition', N'GenWeb', N'VW_CLIENTS', N'Gender', 'Acquisition', N'Stage_Star', N'Account', N'AccountGender', 3)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (2, 'Acquisition', N'GenWeb', N'VW_CLIENTS', N'StatusID', 'Acquisition', N'Stage_Star', N'Account', N'StatusCode', 1)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (3, 'Acquisition', N'GenWeb', N'VW_CLIENTS', N'AffID', 'Acquisition', N'Stage_Star', N'Account', N'Referrer', 2)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (4, 'Acquisition', N'GenWeb', N'VW_CLIENTS', N'AffID', 'Acquisition', N'Stage_Star', N'Account', N'ReferrerType', 2)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (5, 'Acquisition', N'Stage_Star', N'Account', N'AccountCountry', 'Acquisition', N'Stage_Star', N'Account', N'Country', 1)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (6, 'Acquisition', N'Stage_Star', N'Account', N'AccountState', 'Acquisition', N'Stage_Star', N'Account', N'State', 1)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (7, 'Acquisition', N'Stage_Star', N'Account', N'AccountPostCode', 'Acquisition', N'Stage_Star', N'Account', N'PostCode', 1)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (8, 'Acquisition', N'Stage_Star', N'Account', N'AccountSuburb',' Acquisition', N'Stage_Star', N'Account', N'Suburb', 1)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (9, 'Acquisition', N'Stage_Star', N'Account', N'AccountStreetType', 'Acquisition', N'Stage_Star', N'Account', N'StreetType', 2)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (10, 'Acquisition', N'GenWeb', N'VW_CLIENTS', N'AccountID', 'Acquisition', N'Stage', N'Account', N'AccountTypeName', 6)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (11, 'Acquisition', N'Stage', N'Account', N'AccountClientRating', 'Acquisition', N'Stage', N'Account', N'ClientRating', 2)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (12, 'Acquisition', N'GenWeb', N'Transaction', N'BetTypeID', 'Acquisition', N'Stage', N'Transaction', N'BetTypeName', 2)
INSERT [Reference].[Mapping] ([MappingKey], [FromDatabase], [FromSchema], [FromTable], [FromColumn], [ToDatabase], [ToSchema], [ToTable], [ToColumn], [DefaultKey]) VALUES (13, 'Acquisition', N'IntraBet', N'tblClients', N'ClientiID', 'Acquisition', N'Stage', N'Account', N'AccountTypeName', 6)

SET IDENTITY_INSERT [Reference].[Mapping] OFF
If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End