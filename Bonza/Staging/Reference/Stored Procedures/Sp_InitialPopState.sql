Create Procedure Reference.Sp_InitialPopState

as

IF not exists (Select [CountryNumericCode] From [Reference].[State])
Begin
begin transaction
INSERT [Reference].[State] ([CountryNumericCode], [StateCode], [StateName], [PhoneCode]) VALUES (36, N'ACT', N'AUSTRALIAN CAPITAL TERRITORY', N'02')
INSERT [Reference].[State] ([CountryNumericCode], [StateCode], [StateName], [PhoneCode]) VALUES (36, N'NSW', N'NEW SOUTH WALES', N'02')
INSERT [Reference].[State] ([CountryNumericCode], [StateCode], [StateName], [PhoneCode]) VALUES (36, N'NT', N'NORTHERN TERRITORY', N'08')
INSERT [Reference].[State] ([CountryNumericCode], [StateCode], [StateName], [PhoneCode]) VALUES (36, N'QLD', N'QUEENSLAND', N'07')
INSERT [Reference].[State] ([CountryNumericCode], [StateCode], [StateName], [PhoneCode]) VALUES (36, N'SA', N'SOUTH AUSTRALIA', N'08')
INSERT [Reference].[State] ([CountryNumericCode], [StateCode], [StateName], [PhoneCode]) VALUES (36, N'TAS', N'TASMANIA', N'03')
INSERT [Reference].[State] ([CountryNumericCode], [StateCode], [StateName], [PhoneCode]) VALUES (36, N'VIC', N'VICTORIA', N'03')
INSERT [Reference].[State] ([CountryNumericCode], [StateCode], [StateName], [PhoneCode]) VALUES (36, N'WA', N'WESTERN AUSTRALIA', N'08')
If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End