﻿Create Procedure Reference.Sp_InitialPopLifestages

AS

IF not exists (Select [LifeStage] From [Reference].[LifeStages])
Begin
begin transaction

INSERT [Reference].[LifeStages] ([LifeStage], [DaysSinceAccountOpenedLower], [DaysSinceAccountOpenedUpper], [DaysSinceLastBetLower], [DaysSinceLastBetUpper]) VALUES (N'Acquisition', 0, 1, NULL, NULL)
INSERT [Reference].[LifeStages] ([LifeStage], [DaysSinceAccountOpenedLower], [DaysSinceAccountOpenedUpper], [DaysSinceLastBetLower], [DaysSinceLastBetUpper]) VALUES (N'Convert', 8, NULL, -1, -1)
INSERT [Reference].[LifeStages] ([LifeStage], [DaysSinceAccountOpenedLower], [DaysSinceAccountOpenedUpper], [DaysSinceLastBetLower], [DaysSinceLastBetUpper]) VALUES (N'On Boarding', 2, 11, NULL, NULL)
INSERT [Reference].[LifeStages] ([LifeStage], [DaysSinceAccountOpenedLower], [DaysSinceAccountOpenedUpper], [DaysSinceLastBetLower], [DaysSinceLastBetUpper]) VALUES (N'Retain', 12, NULL, 0, 13)
INSERT [Reference].[LifeStages] ([LifeStage], [DaysSinceAccountOpenedLower], [DaysSinceAccountOpenedUpper], [DaysSinceLastBetLower], [DaysSinceLastBetUpper]) VALUES (N'Win back', 12, NULL, 14, 29)
INSERT [Reference].[LifeStages] ([LifeStage], [DaysSinceAccountOpenedLower], [DaysSinceAccountOpenedUpper], [DaysSinceLastBetLower], [DaysSinceLastBetUpper]) VALUES (N'Re-Activate', 12, NULL, 30, NULL)


If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End