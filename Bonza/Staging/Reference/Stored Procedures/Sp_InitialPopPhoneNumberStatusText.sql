Create Procedure Reference.Sp_InitialPopPhoneNumberStatusText

as

IF not exists (Select [CheckText] From [Reference].[PhoneNumberStatusText])
Begin
begin transaction
INSERT [Reference].[PhoneNumberStatusText] ([CheckText]) VALUES (N'WRONG')
INSERT [Reference].[PhoneNumberStatusText] ([CheckText]) VALUES (N'DISCONNECTED')
INSERT [Reference].[PhoneNumberStatusText] ([CheckText]) VALUES (N'INCORRECT')
INSERT [Reference].[PhoneNumberStatusText] ([CheckText]) VALUES (N'TEXTONLY')
INSERT [Reference].[PhoneNumberStatusText] ([CheckText]) VALUES (N'DISCONN')
INSERT [Reference].[PhoneNumberStatusText] ([CheckText]) VALUES (N'DO NOT')
INSERT [Reference].[PhoneNumberStatusText] ([CheckText]) VALUES (N'OLD')
INSERT [Reference].[PhoneNumberStatusText] ([CheckText]) VALUES (N'OLD')
INSERT [Reference].[PhoneNumberStatusText] ([CheckText]) VALUES (N'BAD')
If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End