﻿CREATE PROCEDURE [Reference].[Sp_InitialPopNonBDMIntroducers]
	
AS

IF not exists (Select ClientID From [Reference].NonBDMIntroducers)
BEGIN
BEGIN transaction

Insert into Reference.NonBDMIntroducers(ClientID)
Values (-1),(0),(10241),(14616),(663028),(681860),(681861),(791020),(1150832),(791017),(1263564)

If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End