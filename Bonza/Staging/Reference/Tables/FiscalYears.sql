﻿CREATE TABLE [Reference].[FiscalYears] (
    [Year]        CHAR (4) NOT NULL,
    [FiscalStart] DATE     NOT NULL,
    [FiscalEnd]   DATE     NOT NULL
);

