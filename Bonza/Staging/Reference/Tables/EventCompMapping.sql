﻿CREATE TABLE [Reference].[EventCompMapping] (
    [EventNameLike]     VARCHAR (100) NULL,
    [EventNameNotLike1] VARCHAR (100) NULL,
    [EventNameNotLike2] VARCHAR (100) NULL,
    [EventNameNotLike3] VARCHAR (100) NULL,
    [EventNameNotLike4] VARCHAR (100) NULL,
    [EventNameNotLike5] VARCHAR (100) NULL,
    [CompetitionName]   VARCHAR (100) NULL,
    [EventType]         VARCHAR (100) NULL
);

