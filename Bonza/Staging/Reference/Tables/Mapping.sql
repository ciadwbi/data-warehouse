﻿CREATE TABLE [Reference].[Mapping] (
    [MappingKey]   INT           IDENTITY (1, 1) NOT NULL,
    [FromDatabase] VARCHAR (128) NOT NULL,
    [FromSchema]   VARCHAR (128) NOT NULL,
    [FromTable]    VARCHAR (128) NOT NULL,
    [FromColumn]   VARCHAR (128) NOT NULL,
    [ToDatabase]   VARCHAR (128) NOT NULL,
    [ToSchema]     VARCHAR (128) NOT NULL,
    [ToTable]      VARCHAR (128) NOT NULL,
    [ToColumn]     VARCHAR (128) NOT NULL,
    [DefaultKey]   INT           NOT NULL,
    CONSTRAINT [Mapping_PK] PRIMARY KEY CLUSTERED ([MappingKey] ASC),
    CONSTRAINT [Default_FK_Mapping] FOREIGN KEY ([DefaultKey]) REFERENCES [Reference].[Default] ([DefaultKey])
);


GO
CREATE NONCLUSTERED INDEX [Mapping_FK_Default]
    ON [Reference].[Mapping]([DefaultKey] ASC);


GO
