﻿CREATE TABLE [Reference].[Default] (
    [DefaultKey] INT           IDENTITY (1, 1) NOT NULL,
    [ToValue]    VARCHAR (20) NOT NULL,
    [Comment]    VARCHAR (100) NULL,
    CONSTRAINT [CI_Default_PK] PRIMARY KEY CLUSTERED ([DefaultKey] ASC)
);


GO
