﻿CREATE TABLE [Reference].[State] (
    [CountryNumericCode] INT           NOT NULL,
    [StateCode]          VARCHAR (3)   NOT NULL,
    [StateName]          VARCHAR (100) NOT NULL,
    [PhoneCode]          VARCHAR (10)  NOT NULL,
    CONSTRAINT [State_PK] PRIMARY KEY CLUSTERED ([CountryNumericCode] ASC, [StateCode] ASC),
    CONSTRAINT [State_FK_Country] FOREIGN KEY ([CountryNumericCode]) REFERENCES [Reference].[Country] ([CountryNumericCode])
);
GO
