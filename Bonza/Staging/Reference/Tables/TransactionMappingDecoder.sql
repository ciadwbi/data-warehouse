﻿CREATE TABLE [Reference].[TransactionMappingDecoder] (
    [Column]   VARCHAR (30) NULL,
    [Like]     VARCHAR (30) NULL,
    [NotLike]  VARCHAR (30) NULL,
    [Return]   VARCHAR (30) NULL,
    [Priority] TINYINT      NULL
);


GO
CREATE CLUSTERED INDEX [ClusteredIndex-20130911-175856]
    ON [Reference].[TransactionMappingDecoder]([Like] ASC);

