﻿CREATE TABLE [Reference].[Transformation] (
    [TransformationKey] INT           IDENTITY (1, 1) NOT NULL,
    [FromValue]         VARCHAR (100) NOT NULL,
    [ToValue]           VARCHAR (100) NOT NULL,
    [Comment]           VARCHAR (100) NULL,
    CONSTRAINT [Transformation_PK] PRIMARY KEY CLUSTERED ([TransformationKey] ASC)
);


GO
