﻿CREATE TABLE [Reference].[SportsKeywordCountryMap](
	[Sport] [varchar](50) NULL,
	[Keyword] [varchar](200) NULL,
	[Country] [varchar](32) NULL
) ON [PRIMARY]
