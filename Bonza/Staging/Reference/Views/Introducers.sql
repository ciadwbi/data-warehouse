CREATE VIEW [Reference].[Introducers]
AS
	SELECT
		ClientID,
		'N' AS BDM
	FROM [$(Acquisition)].[Intrabet].[tblIntroducers]
	WHERE ClientID IN (Select ClientID from Reference.NonBDMIntroducers)	-- Enter List of Non-BDM's here and below
		UNION
	SELECT 
		ClientID,
		'Y' AS BDM
	FROM [$(Acquisition)].[Intrabet].[tblIntroducers]
	WHERE ClientID NOT IN (Select ClientID from Reference.NonBDMIntroducers)
