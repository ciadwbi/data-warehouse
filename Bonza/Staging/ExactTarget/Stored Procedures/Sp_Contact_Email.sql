﻿CREATE PROC [ExactTarget].[Sp_Contact_Email]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_Contact_Email','InsertIntoStaging','Start'

--Declare @fromdate DateTime2(3), @BatchKey INT
--Set @fromdate = '2015-10-29 00:00:00.000'
--Set @BatchKey = 1

INSERT INTO Stage.Contact
---EMAIL SENT---
SELECT 
	Distinct
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	a.JobID,
	'ETE' JobIDSource,
	'N/A' Story,
	9999 ChannelId,
	0 PromotionId,
	'N/A' PromotionSource,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	'IBT' ClassIDSource,
	0 CommunicationCompanyId,
	0 EventId,
	'N/A' EventSource,
	'SNT' InteractionTypeId,
	'ETE' InteractionTypeSource,
	DATEADD(minute,930,a.EventDate) as EventDate, --15.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.EventDate)) EventTime,
	'' Details,
	a.FromDate
  FROM [$(Acquisition)].[ExactTarget].[Sent] a
  INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = Cast(b.exactTargetID as varchar(36))
  Where a.FromDate >= @FromDate and a.FromDate < @ToDate

UNION ALL

---EMAIL CLICK---
SELECT 
	Distinct
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	a.JobID,
	'ETE' JobIDSource,
	'N/A' Story,
	9999 ChannelId,
	0 PromotionId,
	'N/A' PromotionSource,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	'IBT' ClassIDSource,
	0 CommunicationCompanyId,
	0 EventId,
	'N/A' EventSource,
	'CLIC' InteractionTypeId,
	'ETE' InteractionTypeSource,
	DATEADD(minute,930,a.EventDate) as EventDate, --15.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.EventDate)) EventTime,
	URL Details,
	a.FromDate
  FROM [$(Acquisition)].[ExactTarget].[Click] a
   INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = Cast(b.exactTargetID as varchar(36))
  Where a.FromDate >= @FromDate and a.FromDate < @ToDate

UNION ALL

---EMAIL OPEN---
SELECT 
	Distinct
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	a.JobID,
	'ETE' JobIDSource,
	'N/A' Story,
	9999 ChannelId,
	0 PromotionId,
	'N/A' PromotionSource,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	'IBT' ClassIDSource,
	0 CommunicationCompanyId,
	0 EventId,
	'N/A' EventSource,
	'OPN' InteractionTypeId,
	'ETE' InteractionTypeSource,
	DATEADD(minute,930,a.EventDate) as EventDate, --15.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.EventDate)) EventTime,
	'' Details,
	a.FromDate
  FROM [$(Acquisition)].[ExactTarget].[Open] a
   INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = Cast(b.exactTargetID as varchar(36))
  Where a.FromDate >= @FromDate and a.FromDate < @ToDate

UNION ALL

---EMAIL UNSUBSCRIBE---
SELECT 
	Distinct
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	a.JobID,
	'ETE' JobIDSource,
	'N/A' Story,
	9999 ChannelId,
	0 PromotionId,
	'N/A' PromotionSource,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	'IBT' ClassIDSource,
	0 CommunicationCompanyId,
	0 EventId,
	'N/A' EventSource,
	'UNS' InteractionTypeId,
	'ETE' InteractionTypeSource,
	DATEADD(minute,930,a.EventDate) as EventDate, --15.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.EventDate)) EventTime,
	'' Details,
	a.FromDate
  FROM [$(Acquisition)].[ExactTarget].[Unsubscribe] a
   INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = Cast(b.exactTargetID as varchar(36))
  Where a.FromDate >= @FromDate and a.FromDate < @ToDate

UNION ALL

---EMAIL BOUNCE---
SELECT 
	Distinct
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	a.JobID,
	'ETE' JobIDSource,
	'N/A' Story,
	9999 ChannelId,
	0 PromotionId,
	'N/A' PromotionSource,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	'IBT' ClassIDSource,
	0 CommunicationCompanyId,
	0 EventId,
	'N/A' EventSource,
	CONVERT(varchar,a.BounceCategoryID) InteractionTypeId,
	'ETE' InteractionTypeSource,
	DATEADD(minute,930,a.EventDate) as EventDate, --15.5 hour difference between Indianopolis and Darwin
	CONVERT(time,DATEADD(minute,930,a.EventDate)) EventTime,
	BounceSubcategory Details,
	a.FromDate
  FROM [$(Acquisition)].[ExactTarget].[Bounce] a
   INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = Cast(b.exactTargetID as varchar(36))
  Where a.FromDate >= @FromDate and a.FromDate < @ToDate


EXEC [Control].Sp_Log @BatchKey, 'Sp_Contact_Email', NULL, 'Success',@RowsProcessed=@@RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Contact_Email', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO


