﻿CREATE PROC [ExactTarget].[Sp_Communication_Push]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_Communication_Push','InsertIntoStagingETPush','Start'

INSERT INTO [Stage].[Communication]
Select	Distinct
		@BatchKey BatchKey,
		MessageID as JobId,
		'ETP' as Source,
		MessageName as Title,
		IsNull(MAX(MessageText),'') as Details,
		Null as SubDetails
From [$(Acquisition)].[ExactTargetPush].[PushReport] a
LEFT OUTER JOIN [$(Acquisition)].[ExactTargetSms].[LifecycleSmsPush] b on a.AssetID = b.AssetID and b.Channel = 'PUSH'
Where a.FromDate >= @FromDate and a.FromDate < @ToDate
Group By MessageID, MessageName

EXEC [Control].Sp_Log @BatchKey, 'Sp_Communication_Push', NULL, 'Success',@RowsProcessed=@@RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Communication_Push', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO


