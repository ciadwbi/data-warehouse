﻿CREATE PROC [ExactTarget].[Sp_Contact_SMS]
(
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@BatchKey	int
)
WITH RECOMPILE
AS 

BEGIN TRY

EXEC [Control].Sp_Log @BatchKey,'Sp_Contact_SMS','InsertIntoStaging','Start'

INSERT INTO Stage.Contact
Select	
	Distinct 
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	MessageName as JobID,
	'ETS' as JobIDSource,
	'N/A' as Story,
	7777 as ChannelID,
	0 PromotionId,
	'N/A' PromotionSource,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	'IBT' ClassIDSource,
	0 CommunicationCompanyId,
	0 EventId,
	'N/A' EventSource,
	'SNT' InteractionTypeId,
	'ETS' InteractionTypeSource,
	DATEADD(minute,-TZ.OffsetMinutes,a.SendDateTime) as EventDate, --time shift from Sydney to Darwin
	CONVERT(time,DATEADD(minute,-TZ.OffsetMinutes,a.SendDateTime)) EventTime,
	'' Details,
	a.FromDate
From [$(Acquisition)].[ExactTargetSms].[SmsReport] a
INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = Cast(b.exactTargetID as varchar(36))
LEFT OUTER JOIN Reference.[TimeZone] TZ on a.SendDateTime>=TZ.FromDate and a.SendDateTime<TZ.ToDate AND TZ.TimeZone='Analysis'
Where a.FromDate >= @FromDate and a.FromDate < @ToDate

UNION ALL

Select	
	Distinct 
	@BatchKey BatchKey,
	b.clientID AccountId,
	'IAC' AccountSource,
	MessageName as JobID,
	'ETS' as JobIDSource,
	'N/A' as Story,
	7777 as ChannelID,
	0 PromotionId,
	'N/A' PromotionSource,
	'N/A' BetType,
	'N/A' BetGroupType,
	'N/A' BetSubType,
	'N/A' GroupingType,
	'N/A' Legtype,
	0 classId,
	'IBT' ClassIDSource,
	0 CommunicationCompanyId,
	0 EventId,
	'N/A' EventSource,
	'FDL' InteractionTypeId,
	'ETS' InteractionTypeSource,
	DATEADD(minute,-TZ.OffsetMinutes,a.SendDateTime) as EventDate, --time shift from Sydney to Darwin
	CONVERT(time,DATEADD(minute,-TZ.OffsetMinutes,a.SendDateTime)) EventTime,
	'' Details,
	a.FromDate
From [$(Acquisition)].[ExactTargetSms].[SmsReport] a
INNER JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre b on a.SubscriberKey = Cast(b.exactTargetID as varchar(36))
LEFT OUTER JOIN Reference.[TimeZone] TZ on a.SendDateTime>=TZ.FromDate and a.SendDateTime<TZ.ToDate AND TZ.TimeZone='Analysis'
Where Status = 'Undelivered'
and a.FromDate >= @FromDate and a.FromDate < @ToDate


EXEC [Control].Sp_Log @BatchKey, 'Sp_Contact_SMS', NULL, 'Success',@RowsProcessed=@@RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Contact_SMS', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
GO


