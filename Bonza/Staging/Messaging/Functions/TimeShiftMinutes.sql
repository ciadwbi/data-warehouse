﻿CREATE FUNCTION [Messaging].[TimeShiftMinutes]()
RETURNS int AS
BEGIN
  RETURN 570 -- 9.5 hours shift from messaging GMT to system standard Darwin time
END