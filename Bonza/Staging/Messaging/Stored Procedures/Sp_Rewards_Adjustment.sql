﻿CREATE PROCEDURE [Messaging].[Sp_Rewards_Adjustment]
(
	@BatchKey		int,
	@FromDate		datetime2(3),
	@ToDate			datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; -- Don't need sanpshot to guarantee integrity of message sources because they are only inserted, never updated

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Adjustments','Start', @Reload

	SELECT	ClientId,
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),FromDate)) FromDate, 
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) ExecutedAt, 
			Points, 
			Reason
	INTO	#Messages
	FROM	[$(Acquisition)].Messaging.RewardsPointsAdjusted
	WHERE	FromDate > @FromDate 
			AND FromDate <= @ToDate
			AND ToDate = CONVERT(datetime2(3),'9999-12-31') 
			AND IsDuplicate = 0
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Atomic','Start', @RowsProcessed = @@ROWCOUNT

	INSERT	Atomic.[Transaction]
		(	BatchKey, TransactionId, TransactionSource, MasterTransactionTimestamp, TransactionCode, UserId, UserSource, InstrumentId, InstrumentSource,
			LedgerId, LedgerSource, PromotionId, PromotionSource, ChannelId, ChannelSource, NotesId, NotesSource, Cash, Bonus, Points, VelocityPoints, MappingId, FromDate
		)
	SELECT	@BatchKey, 
			NEXT VALUE FOR RewardTransactionId TransactionId, 
			'WRB' TransactionSource, 
			m.ExecutedAt MasterTransactionTimestamp,
			'Rewards.PointsAdjusted' TransactionCode,
			'N/A' UserId,
			'N/A' UserSource,
			'N/A' InstrumentId,
			'N/A' InstrumentSource,
			a.AccountID LedgerId,
			'IAA' LedgerSource,
			0 PromotionId,
			'N/A' PromotionSource,
			0 ChannelId,
			'N/A' ChannelSource,
			CONVERT(char(32),HASHBYTES('MD5',m.Reason),2) NotesId,
			'HSH' NotesSource,
			0 Cash,
			0 Bonus,
			m.Points,
			0 VelocityPoints,
			30001 MappingId,
			m.FromDate
	FROM	#Messages m
			INNER JOIN [$(Acquisition)].Intrabet.tblAccounts a ON a.ClientID = m.ClientID AND a.AccountNumber = 1 AND a.FromDate <= m.ExecutedAt AND a.ToDate > m.ExecutedAt
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

	Update [Control].[ConfigurationItems] Set Value = 'N' Where ConfigurationItem = @Me

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO


