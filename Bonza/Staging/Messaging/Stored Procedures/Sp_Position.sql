﻿CREATE PROCEDURE [Messaging].[Sp_Position]
(
	@BatchKey		int,
	@FromDate		datetime2(3),
	@ToDate			datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount int

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Days','Start'

	-- Messages may not arrive in order, therefore we need to find the earliest Cache record impacted by this batch and replay forward from there

	;WITH
		Mess AS
			(	SELECT	ClientId, FromDate, ToDate, LoadDate FROM Cache.LifetimeAccrual UNION ALL
				SELECT	ClientId, FromDate, ToDate, LoadDate FROM Cache.LifetimeBonusRedemption UNION ALL
				SELECT	ClientId, FromDate, ToDate, LoadDate FROM Cache.LifetimeVelocityRedemption UNION ALL
				SELECT	ClientId, FromDate, ToDate, LoadDate FROM Cache.LifetimeRewardCredit UNION ALL
				SELECT	ClientId, FromDate, ToDate, LoadDate FROM Cache.LifetimeRewardDebit
			),
		ImpactRange AS
			(	SELECT	ClientId, MIN(FromDate) FromDate
				FROM (	SELECT	ClientId, FromDate -- Redo all the way forward from any records inserted during this time window
						FROM	Mess
						WHERE	LoadDate > @FromDate AND LoadDate <= @ToDate
						UNION ALL -- Generate al current balnces within the currnet window whether changed or not
						SELECT	ClientId, CASE WHEN FromDate < @FromDate THEN @FromDate ELSE FromDate END FromDate
						FROM	Mess
						WHERE	FromDate <= @ToDate AND ToDate > @FromDate
				) x
				GROUP BY ClientId
			)
	SELECT	r.ClientId, z.FromDate
	INTO	#Days
	FROM	ImpactRange r
			INNER JOIN Cache.ZoneDay z ON z.FromDate BETWEEN r.FromDate AND @ToDate
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Positions','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	x.ClientId, x.FromDate, 
			a.FirstAccrualDate, a.FirstAccrualBetId, b.FirstBonusRedemptionDate, v.FirstVelocityRedemptionDate,
			a.LastAccrualDate, a.LastAccrualBetId, b.LastBonusRedemptionDate, v.LastVelocityRedemptionDate,
			CASE -- Assert the POints balance from the most recent contributing record
				WHEN a.FromDate > ISNULL(b.FromDate,CONVERT(datetime2(3),'1900-01-01')) AND a.FromDate > ISNULL(v.FromDate,CONVERT(datetime2(3),'1900-01-01')) AND a.FromDate > ISNULL(c.FromDate,CONVERT(datetime2(3),'1900-01-01')) AND a.FromDate > ISNULL(d.FromDate,CONVERT(datetime2(3),'1900-01-01')) THEN a.PointsBalance 
				WHEN b.FromDate > ISNULL(v.FromDate,CONVERT(datetime2(3),'1900-01-01')) AND b.FromDate > ISNULL(c.FromDate,CONVERT(datetime2(3),'1900-01-01')) AND b.FromDate > ISNULL(d.FromDate,CONVERT(datetime2(3),'1900-01-01')) THEN b.PointsBalance 
				WHEN v.FromDate > ISNULL(c.FromDate,CONVERT(datetime2(3),'1900-01-01')) AND v.FromDate > ISNULL(d.FromDate,CONVERT(datetime2(3),'1900-01-01')) THEN v.PointsBalance 
				WHEN c.FromDate > ISNULL(d.FromDate,CONVERT(datetime2(3),'1900-01-01')) THEN c.PointsBalance
				ELSE d.PointsBalance
			END PointsBalance, 
			ISNULL(a.AccrualBalance, 0) AccrualBalance, ISNULL(a.NumberOfAccruals,0) NumberOfAccruals, ISNULL(a.LifetimeAccrual,0) LifetimeAccrual, 
			ISNULL(b.NumberOfBonusRedemptions,0) NumberOfBonusRedemptions, ISNULL(b.LifetimeBonusRedemption,0) LifetimeBonusRedemption, 
			ISNULL(v.NumberOfVelocityRedemptions,0) NumberOfVelocityRedemptions, ISNULL(v.LifetimeVelocityRedemption,0) LifetimeVelocityRedemption,
			ISNULL(c.LifetimeRewardCredit,0) LifetimeRewardCredit, ISNULL(LifetimeRewardDebit,0) LifetimeRewardDebit
	INTO	#Position
	FROM	#Days x
			LEFT JOIN Cache.LifetimeAccrual a ON a.ClientId = x.ClientId AND a.FromDate <= x.FromDate AND a.ToDate > x.FromDate
			LEFT JOIN Cache.LifetimeBonusRedemption b ON b.ClientId = x.ClientId AND b.FromDate <= x.FromDate AND b.ToDate > x.FromDate
			LEFT JOIN Cache.LifetimeVelocityRedemption v ON v.ClientId = x.ClientId AND v.FromDate <= x.FromDate AND v.ToDate > x.FromDate
			LEFT JOIN Cache.LifetimeRewardCredit c ON c.ClientId = x.ClientId AND c.FromDate <= x.FromDate AND c.ToDate > x.FromDate
			LEFT JOIN Cache.LifetimeRewardDebit d ON d.ClientId = x.ClientId AND d.FromDate <= x.FromDate AND d.ToDate > x.FromDate
	OPTION (RECOMPILE, TABLE HINT(c, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Atomic','Start', @RowsProcessed = @@ROWCOUNT

	INSERT	Atomic.Position
		(	BatchKey, LedgerID, LedgerSource, FromDate, 
			FirstAccrualDate, FirstAccrualBetId, FirstBonusRedemptionDate, FirstVelocityRedemptionDate,
			LastAccrualDate, LastAccrualBetId, LastBonusRedemptionDate, LastVelocityRedemptionDate,
			PointsBalance, AccrualBalance, NumberOfAccruals, LifetimeAccrual, 
			NumberOfBonusRedemptions, LifetimeBonusRedemption, NumberOfVelocityRedemptions, LifetimeVelocityRedemption, 
			LifetimeRewardCredit, LifetimeRewardDebit
		)
	SELECT	@BatchKey BatchKey, a.AccountId LedgerID, 'IAA' LedgerSource, p.FromDate, 
			FirstAccrualDate, FirstAccrualBetId, FirstBonusRedemptionDate, FirstVelocityRedemptionDate,
			LastAccrualDate, LastAccrualBetId, LastBonusRedemptionDate, LastVelocityRedemptionDate,
			PointsBalance, AccrualBalance, NumberOfAccruals, LifetimeAccrual, 
			NumberOfBonusRedemptions, LifetimeBonusRedemption, NumberOfVelocityRedemptions, LifetimeVelocityRedemption,
			LifetimeRewardCredit, LifetimeRewardDebit
	FROM	#Position p
			LEFT JOIN [$(Acquisition)].IntraBet.tblAccounts a ON a.ClientID = p.ClientId AND a.AccountNumber = 1 AND a.ToDate = CONVERT(datetime2(3),'9999-12-31')
	OPTION (RECOMPILE, TABLE HINT(a, FORCESEEK))

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowsProcessed

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO


