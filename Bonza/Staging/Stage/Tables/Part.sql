﻿CREATE TABLE [Stage].[Part] (
    [BatchKey]        INT          NOT NULL,
    [ProductCode]     VARCHAR (3)  NOT NULL,
    [OutcomeId]       VARCHAR (32) NOT NULL,
    [PartNumber]      TINYINT      NOT NULL,
    [NumberOfParts]   TINYINT      NOT NULL,
    [WeightingFactor] FLOAT (53)   NOT NULL,
    [PartProductCode] VARCHAR (3)  NOT NULL,
    [PartOutcomeId]   VARCHAR (32) NOT NULL
) ON [Staging] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Part] SET (LOCK_ESCALATION = AUTO)
GO

