﻿CREATE TABLE [Stage].[TransactionDetail] (
    [BatchKey]             INT           NOT NULL,
    [TransactionDetailId]  CHAR (4)      NOT NULL,
    [TransactionType]      VARCHAR (32)  NOT NULL,
    [TransactionStatus]    VARCHAR (32)  NOT NULL,
    [TransactionMethod]    VARCHAR (32)  NOT NULL,
    [TransactionDirection] VARCHAR (3)   NOT NULL,
    [TransactionSign]      SMALLINT      NOT NULL,
    [SnapshotInclude]      VARCHAR (3)   NOT NULL,
    [SnapshotName]         VARCHAR (32)  NOT NULL,
    [BalanceName]          VARCHAR (32)  NOT NULL,
    [BankAccountName]      VARCHAR (100) NOT NULL,
    [Bank]                 VARCHAR (32)  NOT NULL
) ON [Staging] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[TransactionDetail] SET (LOCK_ESCALATION = AUTO)
GO



