﻿CREATE TABLE [Stage].[Channel] (
    [BatchKey] [int] NOT NULL,
	[ChannelId] [smallint] NULL,
	[ChannelDescription] [varchar](200) NULL,
	[ChannelName] [varchar](50) NULL,
	[ChannelTypeName] [varchar](50) NULL
) ON [Staging] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Channel] SET (LOCK_ESCALATION = AUTO)
GO
