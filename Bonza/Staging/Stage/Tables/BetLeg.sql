﻿CREATE TABLE [Stage].[BetLeg] (
    [BatchKey]        INT          NOT NULL,
    [CompanyCode]     VARCHAR (3)  NOT NULL,
    [ContractID]      VARCHAR (32) NOT NULL,
    [LegNumber]       INT          NOT NULL,
    [NumberOfLegs]    INT          NOT NULL,
    [WeightingFactor] FLOAT (53)   NOT NULL,
    [BetTypeName]     VARCHAR (32) NOT NULL,
    [ClassId]         INT          NOT NULL
) ON [Staging] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[BetLeg] SET (LOCK_ESCALATION = AUTO)
GO



