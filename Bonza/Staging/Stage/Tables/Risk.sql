﻿CREATE TABLE [Stage].[Risk] (
    [BatchKey]       INT          NOT NULL,
    [CompanyCode]    VARCHAR (3)  NOT NULL,
    [ProductCode]    VARCHAR (3)  NOT NULL,
    [LegNumber]      TINYINT      NOT NULL,
    [NumberOfLegs]   TINYINT      NOT NULL,
    [CalendarName]   VARCHAR (32) NOT NULL,
    [BetPriceText]   VARCHAR (10) NOT NULL,
    [PriceText]      VARCHAR (10) NOT NULL,
    [StatusCode]     CHAR (1)     NOT NULL,
    [OutcomeCode]    CHAR (1)     NOT NULL,
    [RequestedStake] SMALLMONEY   NOT NULL,
    [Stake]          SMALLMONEY   NOT NULL,
    [NumberOfBets]   INT          NOT NULL,
    [Return]         MONEY        NOT NULL,
    [MinimumReturn]  MONEY        NOT NULL,
    [MaximumReturn]  MONEY        NOT NULL,
    [DayText]        VARCHAR (11) NOT NULL,
    [TimeText]       VARCHAR (5)  NOT NULL,
    [Wallet Id]      INT          NOT NULL,
    [OutcomeId]      VARCHAR (32) NOT NULL
) ON [Staging] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Risk] SET (LOCK_ESCALATION = AUTO)
GO

