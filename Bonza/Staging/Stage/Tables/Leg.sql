﻿CREATE TABLE [Stage].[Leg] (
    [BatchKey]                    INT           NOT NULL,
    [CompanyCode]                 VARCHAR (3)   NOT NULL,
    [BetId]                       VARCHAR (32)  NOT NULL,
    [LegNumber]                   TINYINT       NOT NULL,
    [NumberOfLegs]                TINYINT       NOT NULL,
    [AccountId]                   BIGINT        NOT NULL,
    [WalletId]                    INT           NOT NULL,
    [ProductCode]                 VARCHAR (3)   NOT NULL,
    [OutcomeId]                   VARCHAR (32)  NOT NULL,
    [BetTypeName]                 VARCHAR (32)  NOT NULL,
    [PlacementDayText]            VARCHAR (11)  NOT NULL,
    [PlacementTimeText]           VARCHAR (5)   NOT NULL,
    [PlacementLocalDayText]       VARCHAR (11)  NOT NULL,
    [PlacementLocalTimeText]      VARCHAR (5)   NOT NULL,
    [PlacementBetDayText]         VARCHAR (11)  NOT NULL,
    [PlacementBetTimeText]        VARCHAR (5)   NOT NULL,
    [PlacementBetLocalDayText]    VARCHAR (11)  NOT NULL,
    [PlacementBetLocalTimeText]   VARCHAR (5)   NOT NULL,
    [SettlementDayText]           VARCHAR (11)  NOT NULL,
    [SettlementLocalDayText]      VARCHAR (11)  NOT NULL,
    [SettlementBetDayText]        VARCHAR (11)  NOT NULL,
    [SettlementBetLocalDayText]   VARCHAR (11)  NOT NULL,
    [CalendarName]                VARCHAR (32)  NOT NULL,
    [PlacementPriceName]          VARCHAR (10)  NOT NULL,
    [PlacementBetPriceName]       VARCHAR (10)  NOT NULL,
    [SettlementPriceName]         VARCHAR (10)  NOT NULL,
    [SettlementBetPriceName]      VARCHAR (10)  NOT NULL,
    [SelectionLimitName]          VARCHAR (32)  NOT NULL,
    [LimitName]                   VARCHAR (32)  NOT NULL,
    [AuthorisingUserId]           VARCHAR (32)  NOT NULL,
    [StatusCode]                  CHAR (1)      NOT NULL,
    [OutcomeCode]                 CHAR (1)      NOT NULL,
    [ChannelName]                 VARCHAR (32)  NOT NULL,
    [CampaignId]                  INT           NOT NULL,
    [PrestakeBalance]             MONEY         NOT NULL,
    [AllUpRequestedStake]         SMALLMONEY    NOT NULL,
    [AllUpStake]                  SMALLMONEY    NOT NULL,
    [AllUpReturn]                 MONEY         NOT NULL,
    [EqualShareRequestedStake]    SMALLMONEY    NOT NULL,
    [EqualShareStake]             SMALLMONEY    NOT NULL,
    [EqualShareReturn]            MONEY         NOT NULL,
    [PostsettlementBalance]       MONEY         NOT NULL,
    [EstimatedLevy]               SMALLMONEY    NOT NULL,
    [Levy]                        SMALLMONEY    NOT NULL,
    [PlacementTimestamp]          DATETIME2 (7) NOT NULL,
    [PlacementLocalTimestamp]     DATETIME2 (7) NOT NULL,
    [PlacementBetTimestamp]       DATETIME2 (7) NOT NULL,
    [PlacementBetLocalTimestamp]  DATETIME2 (7) NOT NULL,
    [SettlementTimestamp]         DATETIME2 (7) NOT NULL,
    [SettlementLocalTimestamp]    DATETIME2 (7) NOT NULL,
    [SettlementBetTimestamp]      DATETIME2 (7) NOT NULL,
    [SettlementBetLocalTimestamp] DATETIME2 (7) NOT NULL
) ON [Staging] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Leg] SET (LOCK_ESCALATION = AUTO)
GO

