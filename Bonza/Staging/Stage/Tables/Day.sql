﻿Create TABLE stage.[Day] (
	[DayKey] [int] NOT NULL,
	[DayName] [varchar](11) NULL,
	[DayText] [varchar](11) NULL,
	[DayDate] [date] NULL,
	[DayOfWeekText] [char](3) NULL,
	[DayOfWeekNumber] [tinyint] NULL,
	[DayOfMonthNumber] [tinyint] NULL,
	[DayOfQuarterNumber] [tinyint] NULL,
	[DayOfYearNumber] [smallint] NULL,
	[DayOfTaxQuarterNumber] [tinyint] NULL,
	[DayOfTaxYearNumber] [smallint] NULL,
	[DayOfFiscalWeekNumber] [tinyint] NULL,
	[DayOfFiscalMonthNumber] [tinyint] NULL,
	[DayOfFiscalQuarterNumber] [tinyint] NULL,
	[DayOfFiscalYearNumber] [smallint] NULL,
	[WeekKey] [int] NULL,
	[WeekName] [varchar](8) NULL,
	[WeekText] [varchar](8) NULL,
	[WeekStartDate] [date] NULL,
	[WeekEndDate] [date] NULL,
	[WeekOfMonthText] [varchar](3) NULL,
	[WeekOfMonthNumber] [tinyint] NULL,
	[WeekOfYearText] [char](3) NULL,
	[WeekOfYearNumber] [tinyint] NULL,
	[WeekYearNumber] [smallint] NULL,
	[MonthKey] [int] NULL,
	[MonthName] [varchar](8) NULL,
	[MonthText] [varchar](8) NULL,
	[MonthStartDate] [date] NULL,
	[MonthEndDate] [date] NULL,
	[MonthDays] [tinyint] NULL,
	[MonthOfYearText] [char](3) NULL,
	[MonthOfYearNumber] [tinyint] NULL,
	[QuarterKey] [int] NULL,
	[QuarterName] [varchar](7) NULL,
	[QuarterText] [varchar](7) NULL,
	[QuarterStartDate] [date] NULL,
	[QuarterEndDate] [date] NULL,
	[QuarterDays] [tinyint] NULL,
	[QuarterOfYearText] [varchar](3) NULL,
	[QuarterOfYearNumber] [tinyint] NULL,
	[YearKey] [int] NULL,
	[YearName] [varchar](7) NULL,
	[YearText] [varchar](4) NULL,
	[YearNumber] [smallint] NULL,
	[YearStartDate] [date] NULL,
	[YearEndDate] [date] NULL,
	[YearDays] [smallint] NULL,
	[TaxQuarterKey] [int] NULL,
	[TaxQuarterName] [varchar](8) NULL,
	[TaxQuarterText] [varchar](8) NULL,
	[TaxQuarterStartDate] [date] NULL,
	[TaxQuarterEndDate] [date] NULL,
	[TaxQuarterDays] [tinyint] NULL,
	[TaxQuarterOfYearText] [varchar](3) NULL,
	[TaxQuarterOfYearNumber] [tinyint] NULL,
	[TaxYearKey] [int] NULL,
	[TaxYearName] [varchar](7) NULL,
	[TaxYearText] [varchar](5) NULL,
	[TaxYearNumber] [smallint] NULL,
	[TaxYearStartDate] [date] NULL,
	[TaxYearEndDate] [date] NULL,
	[TaxYearDays] [smallint] NULL,
	[FiscalWeekKey] [int] NULL,
	[FiscalWeekName] [varchar](8) NULL,
	[FiscalWeekText] [varchar](8) NULL,
	[FiscalWeekStartDate] [date] NULL,
	[FiscalWeekEndDate] [date] NULL,
	[FiscalWeekOfMonthText] [varchar](3) NULL,
	[FiscalWeekOfMonthNumber] [tinyint] NULL,
	[FiscalWeekOfYearText] [char](3) NULL,
	[FiscalWeekOfYearNumber] [tinyint] NULL,
	[FiscalMonthKey] [int] NULL,
	[FiscalMonthName] [varchar](8) NULL,
	[FiscalMonthText] [varchar](8) NULL,
	[FiscalMonthStartDate] [date] NULL,
	[FiscalMonthEndDate] [date] NULL,
	[FiscalMonthDays] [tinyint] NULL,
	[FiscalMonthWeeks] [tinyint] NULL,
	[FiscalMonthOfYearText] [varchar](3) NULL,
	[FiscalMonthOfYearNumber] [tinyint] NULL,
	[FiscalQuarterKey] [int] NULL,
	[FiscalQuarterName] [varchar](7) NULL,
	[FiscalQuarterText] [varchar](7) NULL,
	[FiscalQuarterStartDate] [date] NULL,
	[FiscalQuarterEndDate] [date] NULL,
	[FiscalQuarterDays] [tinyint] NULL,
	[FiscalQuarterWeeks] [tinyint] NULL,
	[FiscalQuarterOfYearText] [varchar](3) NULL,
	[FiscalQuarterOfYearNumber] [tinyint] NULL,
	[FiscalYearKey] [int] NULL,
	[FiscalYearName] [varchar](7) NULL,
	[FiscalYearText] [varchar](4) NULL,
	[FiscalYearNumber] [smallint] NULL,
	[FiscalYearStartDate] [date] NULL,
	[FiscalYearEndDate] [date] NULL,
	[FiscalYearDays] [smallint] NULL,
	[FiscalYearWeeks] [tinyint] NULL,
	[PreviousDayKey] [int] NULL,
	[PreviousWeekKey] [int] NULL,
	[PreviousWeekDayKey] [int] NULL,
	[PreviousMonthKey] [int] NULL,
	[PreviousMonthDayKey] [int] NULL,
	[PreviousQuarterKey] [int] NULL,
	[PreviousQuarterMonthKey] [int] NULL,
	[PreviousQuarterDayKey] [int] NULL,
	[PreviousYearKey] [int] NULL,
	[PreviousYearQuarterKey] [int] NULL,
	[PreviousYearMonthKey] [int] NULL,
	[PreviousYearDayKey] [int] NULL,
	[PreviousTaxQuarterKey] [int] NULL,
	[PreviousTaxQuarterMonthKey] [int] NULL,
	[PreviousTaxQuarterDayKey] [int] NULL,
	[PreviousTaxYearKey] [int] NULL,
	[PreviousTaxYearQuarterKey] [int] NULL,
	[PreviousTaxYearMonthKey] [int] NULL,
	[PreviousTaxYearDayKey] [int] NULL,
	[PreviousFiscalWeekKey] [int] NULL,
	[PreviousFiscalWeekDayKey] [int] NULL,
	[PreviousFiscalMonthKey] [int] NULL,
	[PreviousFiscalMonthDayKey] [int] NULL,
	[PreviousFiscalQuarterKey] [int] NULL,
	[PreviousFiscalQuarterMonthKey] [int] NULL,
	[PreviousFiscalQuarterDayKey] [int] NULL,
	[PreviousFiscalYearKey] [int] NULL,
	[PreviousFiscalYearQuarterKey] [int] NULL,
	[PreviousFiscalYearMonthKey] [int] NULL,
	[PreviousFiscalYearDayKey] [int] NULL,
	[NextDayKey] [int] NULL,
	[NextWeekKey] [int] NULL,
	[NextWeekDayKey] [int] NULL,
	[NextMonthKey] [int] NULL,
	[NextMonthDayKey] [int] NULL,
	[NextQuarterKey] [int] NULL,
	[NextQuarterMonthKey] [int] NULL,
	[NextQuarterDayKey] [int] NULL,
	[NextYearKey] [int] NULL,
	[NextYearQuarterKey] [int] NULL,
	[NextYearMonthKey] [int] NULL,
	[NextYearDayKey] [int] NULL,
	[NextTaxQuarterKey] [int] NULL,
	[NextTaxQuarterMonthKey] [int] NULL,
	[NextTaxQuarterDayKey] [int] NULL,
	[NextTaxYearKey] [int] NULL,
	[NextTaxYearQuarterKey] [int] NULL,
	[NextTaxYearMonthKey] [int] NULL,
	[NextTaxYearDayKey] [int] NULL,
	[NextFiscalWeekKey] [int] NULL,
	[NextFiscalWeekDayKey] [int] NULL,
	[NextFiscalMonthKey] [int] NULL,
	[NextFiscalMonthDayKey] [int] NULL,
	[NextFiscalQuarterKey] [int] NULL,
	[NextFiscalQuarterMonthKey] [int] NULL,
	[NextFiscalQuarterDayKey] [int] NULL,
	[NextFiscalYearKey] [int] NULL,
	[NextFiscalYearQuarterKey] [int] NULL,
	[NextFiscalYearMonthKey] [int] NULL,
	[NextFiscalYearDayKey] [int] NULL,
	[CreatedDate] [datetime2](3) NULL,
	[CreatedBatchKey] [int] NULL,
	[CreatedBy] [varchar](32) NULL,
	[ModifiedDate] [datetime2](3) NULL,
	[ModifiedBatchKey] [int] NULL,
	[ModifiedBy] [varchar](32) NULL
) ON [PRIMARY]