﻿CREATE TABLE [Stage].[Promotion] (
    [PromotionId]   INT            NOT NULL,
    [PromotionType] VARCHAR (32)   NOT NULL,
    [PromotionName] VARCHAR (1000) NOT NULL,
    [OfferDate]     DATE           NOT NULL,
    [ExpiryDate]    DATE           NOT NULL,
    [BatchKey]      INT            NOT NULL,
    [Source]        CHAR (3)       NOT NULL
) ON [Staging] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[Promotion] SET (LOCK_ESCALATION = AUTO)
GO



