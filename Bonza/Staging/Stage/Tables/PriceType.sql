﻿CREATE TABLE [Stage].[PriceType] (
    [BatchKey]              INT			 NOT NULL,
	[PriceTypeID]			INT			 NOT NULL,
	[Source]				CHAR(3)		 NOT NULL,
	[PriceTypeName]			VARCHAR(255) NOT NULL
) ON [Staging] ([BatchKey])
WITH (DATA_COMPRESSION = ROW );
GO

ALTER TABLE [Stage].[PriceType] SET (LOCK_ESCALATION = AUTO)
GO
