﻿CREATE TABLE [Audit].[DDLChangeLog] (
    [LogID]        INT            IDENTITY (1, 1) NOT NULL,
    [PostTime]     DATETIME       NOT NULL,
    [DatabaseUser] [sysname]      NOT NULL,
    [Event]        [sysname]      NOT NULL,
    [Schema]       [sysname]      NULL,
    [Object]       [sysname]      NULL,
    [TSQL]         NVARCHAR (MAX) NOT NULL,
    [XmlEvent]     XML            NOT NULL,
    CONSTRAINT [PK_DDLChangeLog_LogID] PRIMARY KEY NONCLUSTERED ([LogID] DESC) WITH (FILLFACTOR = 100)
);

