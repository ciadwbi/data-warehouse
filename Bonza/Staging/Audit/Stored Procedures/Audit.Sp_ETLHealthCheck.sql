﻿CREATE PROC [Audit].[Sp_ETLHealthCheck]
(
	@BatchKey int = 0, -- Optional - Will run for last batch if not supplied
	@SendEmail BIT = 0 -- Will return result set if 0, otherwise sends email if 1
)
WITH RECOMPILE
AS
BEGIN
/*
	Developer: Aaron Jackson
	Date: 01/04/2015
	Desc: Query the ETLController table and reformat the results. This query calculates averages and can optionally send the results as an email.

	Test: EXEC [Audit].[Sp_ETLHealthCheck]

*/

	SET NOCOUNT ON;

	DECLARE @bodyMsg nvarchar(max);
	DECLARE @subject nvarchar(max);
	DECLARE @recipient nvarchar(max) = 'BI.Service@williamhill.com.au';
	DECLARE @tableHTML nvarchar(max);
	DECLARE @RowCount INT;

	DECLARE @BatchStartDate VARCHAR(20), @BatchEndDate VARCHAR(20), @BatchFromDate VARCHAR(20), @BatchToDate VARCHAR(20);

	SET @subject = 'Morning Health Check';

	IF OBJECT_ID('tempdb..#Results') IS NOT NULL BEGIN DROP TABLE #Results END;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	IF @BatchKey = 0 -- if no batch key supplied, run for last batch
	BEGIN
		SELECT @BatchKey = MAX(COALESCE(BatchKey,-1)) FROM [Control].[ETLController];
	END;

	IF @BatchKey = -1 RAISERROR ('@BatchKey not set.. ',16,1);

	With BatchStats AS
	(
		SELECT TOP 28
				BatchKey,
				'Atomic' as [ETLProcessName],
				AtomicStatus as [Status],
				AtomicStartDate as [StartDate],
				AtomicEndDate as [EndDate],
				AtomicRowsProcessed as [RowsProcessed],
				DATEDIFF(mi,AtomicStartDate, COALESCE(AtomicEndDate,GETDATE())) as [ElapsedMinutes]
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
		SELECT TOP 28
				BatchKey,
				'Staging',
				StagingStatus,
				StagingStartDate,
				StagingEndDate,
				StagingRowsProcessed,
				DATEDIFF(mi,StagingStartDate, COALESCE(StagingEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
		SELECT TOP 28
				BatchKey,
				'Recursive',
				RecursiveStatus,
				RecursiveStartDate,
				RecursiveEndDate,
				RecursiveRowsProcessed,
				DATEDIFF(mi,RecursiveStartDate, COALESCE(RecursiveEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
		SELECT TOP 28
				BatchKey,
				'Dimension',
				DimensionStatus,
				DimensionStartDate,
				DimensionEndDate,
				DimensionRowsProcessed,
				DATEDIFF(mi,DimensionStartDate, COALESCE(DimensionEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
		SELECT TOP 28
				BatchKey,
				'Transaction',
				TransactionStatus,
				TransactionStartDate,
				TransactionEndDate,
				TransactionRowsProcessed,
				DATEDIFF(mi,TransactionStartDate, COALESCE(TransactionEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
		SELECT TOP 28
				BatchKey,
				'KPIFact',
				KPIFactStatus,
				KPIFactStartDate,
				KPIFactEndDate,
				KPIFactRowsProcessed,
				DATEDIFF(mi,KPIFactStartDate, COALESCE(KPIFactEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
		SELECT TOP 28
				BatchKey,
				'PositionStaging',
				PositionStagingStatus,
				PositionStagingStartDate,
				PositionStagingEndDate,
				PositionStagingRowsProcessed,
				DATEDIFF(mi,PositionStagingStartDate, COALESCE(PositionStagingEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
		SELECT TOP 28
				BatchKey,
				'PositionFact',
				PositionFactStatus,
				PositionFactStartDate,
				PositionFactEndDate,
				PositionFactRowsProcessed,
				DATEDIFF(mi,PositionFactStartDate, COALESCE(PositionFactEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
		SELECT TOP 28
				BatchKey,
				'BalanceFact',
				BalanceFactStatus,
				BalanceFactStartDate,
				BalanceFactEndDate,
				BalanceFactRowsProcessed,
				DATEDIFF(mi,BalanceFactStartDate, COALESCE(BalanceFactEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
	    SELECT TOP 28
				BatchKey,
				'ContactEmailStaging',
				ContactEmailStagingStatus,
				ContactEmailStagingStartDate,
				ContactEmailStagingEndDate,
				ContactEmailStagingRowsProcessed,
				DATEDIFF(mi,ContactEmailStagingStartDate, COALESCE(ContactEmailStagingEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
			UNION ALL
		SELECT TOP 28
				BatchKey,
				'ContactSmsStaging',
				ContactSmsStagingStatus,
				ContactSmsStagingStartDate,
				ContactSmsStagingEndDate,
				ContactSmsStagingRowsProcessed,
				DATEDIFF(mi,ContactSmsStagingStartDate, COALESCE(ContactSmsStagingEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
			UNION ALL
		SELECT TOP 28
				BatchKey,
				'ContactPushStaging',
				ContactPushStagingStatus,
				ContactPushStagingStartDate,
				ContactPushStagingEndDate,
				ContactPushStagingRowsProcessed,
				DATEDIFF(mi,ContactPushStagingStartDate, COALESCE(ContactPushStagingEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
		SELECT TOP 28
				BatchKey,
				'ContactEmailFact',
				ContactEmailFactStatus,
				ContactEmailFactStartDate,
				ContactEmailFactEndDate,
				ContactEmailFactRowsProcessed,
				DATEDIFF(mi,ContactEmailFactStartDate, COALESCE(ContactEmailFactEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
		SELECT TOP 28
				BatchKey,
				'ContactSmsFact',
				ContactSmsFactStatus,
				ContactSmsFactStartDate,
				ContactSmsFactEndDate,
				ContactSmsFactRowsProcessed,
				DATEDIFF(mi,ContactSmsFactStartDate, COALESCE(ContactSmsFactEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
		UNION ALL
		SELECT TOP 28
				BatchKey,
				'ContactPushFact',
				ContactPushFactStatus,
				ContactPushFactStartDate,
				ContactPushFactEndDate,
				ContactPushFactRowsProcessed,
				DATEDIFF(mi,ContactPushFactStartDate, COALESCE(ContactPushFactEndDate,GETDATE()))
		FROM Control.ETLController
		WHERE BatchKey <= @BatchKey
	),
	AverageStats AS
	(
		SELECT
			ETLProcessName,
			AVG(CAST(RowsProcessed AS BIGINT)) as [AverageRowsProcessed],
			AVG(ElapsedMinutes) as [AverageElapsedMinutes]
		FROM BatchStats
		GROUP BY ETLProcessName
	),
	PreSelect AS
	(
		SELECT
			A.[BatchKey],
			A.[ETLProcessName],
			CASE A.[Status]
				WHEN -3 THEN 'Failed'
				WHEN -2 THEN 'Failed'
				WHEN -1 THEN 'Failed'
				WHEN  0 THEN 'In Progress'
				WHEN  1 THEN 'Success'
				ELSE 'Unknown'
			END as [Status],
			CONVERT(varchar(21), A.[StartDate]) as [StartDate],
			CONVERT(varchar(21), A.[EndDate]) as [EndDate],
			REPLACE(CONVERT(varchar, CAST(B.AverageRowsProcessed AS money), 1), '.00', '') as [AverageRowsProcessed],
			REPLACE(CONVERT(varchar, CAST(A.[RowsProcessed] AS money), 1), '.00', '') as [RowsProcessed],
			REPLACE(CONVERT(varchar, CAST(B.AverageElapsedMinutes AS money), 1), '.00', '') as [AverageElapsedMinutes],
			REPLACE(CONVERT(varchar, CAST(A.[ElapsedMinutes] AS money), 1), '.00', '') as [ElapsedMinutes]	
		FROM BatchStats as A
		INNER JOIN AverageStats as B on A.ETLProcessName = B.ETLProcessName
		WHERE A.BatchKey = @BatchKey
	)
		SELECT
			[BatchKey],
			[ETLProcessName],
			[Status],
			COALESCE([StartDate],'*') as [StartDate],
			COALESCE([EndDate],'*') as [EndDate],
			COALESCE([AverageRowsProcessed],'0') as [AverageRowsProcessed],
			COALESCE([RowsProcessed],'0') as [RowsProcessed],
			COALESCE([AverageElapsedMinutes],'0') as [AverageElapsedMinutes],
			COALESCE([ElapsedMinutes],'0') as [ElapsedMinutes]	
		INTO #Results
		FROM PreSelect
		ORDER BY StartDate;

		SET @RowCount = @@ROWCOUNT;




	IF @SendEmail = 1 AND @RowCount > 0
	BEGIN

		SELECT @BatchStartDate = BatchStartDate, @BatchEndDate = BatchEndDate, @BatchFromDate = BatchFromDate, @BatchToDate = BatchToDate
		FROM Control.ETLController
		WHERE BatchKey = @BatchKey;

		SELECT @tableHTML = 
				N'<style type="text/css">
				#box-table
				{
				font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
				font-size: 12px;
				text-align: center;
				border-collapse: collapse;
				border-top: 7px solid #9baff1;
				border-bottom: 7px solid #9baff1;
				}
				#box-table th
				{
				font-size: 13px;
				font-weight: normal;
				background: #b9c9fe;
				border-right: 2px solid #9baff1;
				border-left: 2px solid #9baff1;
				border-bottom: 2px solid #9baff1;
				color: #039;
				}
				#box-table td
				{
				border-right: 1px solid #aabcfe;
				border-left: 1px solid #aabcfe;
				border-bottom: 1px solid #aabcfe;
				color: #669;
				}
				tr:nth-child(odd)	{ background-color:#eee; }
				tr:nth-child(even)	{ background-color:#fff; }	
				</style>'+	
				N'<H3>' + @subject + '</H3>' +
				N'Batch Start Date: ' + @BatchStartDate +'<br>'+
				N'Batch End Date: ' + @BatchEndDate +'<br><br>'+
				N'Batch From Date: ' + @BatchFromDate +'<br>'+
				N'Batch To Date: ' + @BatchToDate +'<br><br>'+
				N'Server: ' + @@SERVERNAME + '<br><br>'+
				N'<table id="box-table" >' +
				N'<tr><font color="Green">
				<th>Batch Key</th>
				<th>ETL Process Name</th>
				<th>Status</th>
				<th>Start Date</th>
				<th>End Date</th>
				<th>Average Rows Processed</th>
				<th>Rows Processed</th>
				<th>Average Elapsed Minutes</th>
				<th>Elapsed Minutes</th>
				</tr>' +
				CAST ( ( 

				SELECT td = [BatchKey],'',
				td = [ETLProcessName],'',
				td = [Status],'',
				td = [StartDate],'',
				td = [EndDate],'',
				td = REPLACE(CONVERT(varchar, CAST(AverageRowsProcessed AS money), 1), '.00', ''),'',
				td = REPLACE(CONVERT(varchar, CAST([RowsProcessed] AS money), 1), '.00', ''),'',
				td = REPLACE(CONVERT(varchar, CAST(AverageElapsedMinutes AS money), 1), '.00', ''),'',
				td = REPLACE(CONVERT(varchar, CAST([ElapsedMinutes] AS money), 1), '.00', '')
				FROM #Results
				ORDER BY StartDate
				FOR XML PATH('tr'), TYPE 
				) AS NVARCHAR(MAX) ) +
				N'</table>';

				IF LEN(@TableHTML) > 0
				BEGIN

					EXEC msdb.dbo.sp_send_dbmail
					@profile_name	= 'DataWareHouse_Auto_Mails', 
					@recipients= @recipient,
					@subject = @subject,
					@body = @tableHTML,
					@body_format = 'HTML';

				END
	END
	--ELSE
	
		SELECT
			[BatchKey],
			[ETLProcessName],
			[Status],
			[StartDate],
			[EndDate],
			[AverageRowsProcessed],
			[RowsProcessed],
			[AverageElapsedMinutes],
			[ElapsedMinutes]
		FROM #Results
		ORDER BY StartDate;

END
GO
