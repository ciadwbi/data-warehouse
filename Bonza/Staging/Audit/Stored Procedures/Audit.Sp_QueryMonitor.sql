﻿Create PROC [Audit].[Sp_QueryMonitor]
(
	@ElapsedMinutes INT = 60
)
WITH RECOMPILE
AS
BEGIN
/*
	Developer: Aaron Jackson
	Date: 31/03/2015
	Desc: Take a snapshot of SPID activity every 5 minutes, and check for any that breach the @threshold. If breach, send email.
*/

		SET NOCOUNT ON;

		--BEGIN TRY

		DECLARE @bodyMsg nvarchar(max);
		DECLARE @subject nvarchar(max);
		DECLARE @tableHTML nvarchar(max);
		DECLARE @CollectionTime DATETIME;

		exec dbo.sp_WhoIsActive @show_system_spids = 0, @show_sleeping_spids = 0, @get_plans = 1, @get_locks = 1, @destination_table = '[Audit].[QueryPerformance]';

		IF @@ROWCOUNT > 0 -- only query the result if new records were inserted
		BEGIN
			SELECT @CollectionTime = MAX(collection_time) FROM [Audit].[QueryPerformance];

			Select		*
			into		#Data
			From		[Audit].[QueryPerformance]
			Where		collection_time = @CollectionTime
						and login_name != 'PROD\SVC_SQLService'
						and database_name not in ('master', 'msdb')

			If Exists	(
						 Select		*
						 From		#Data
						)
						Begin
							SET @subject = 'Long running query alert!';

							SET @tableHTML = 
							N'<style type="text/css">
							#box-table
							{
							font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
							font-size: 12px;
							text-align: center;
							border-collapse: collapse;
							border-top: 7px solid #9baff1;
							border-bottom: 7px solid #9baff1;
							}
							#box-table th
							{
							font-size: 13px;
							font-weight: normal;
							background: #b9c9fe;
							border-right: 2px solid #9baff1;
							border-left: 2px solid #9baff1;
							border-bottom: 2px solid #9baff1;
							color: #039;
							}
							#box-table td
							{
							border-right: 1px solid #aabcfe;
							border-left: 1px solid #aabcfe;
							border-bottom: 1px solid #aabcfe;
							color: #669;
							}
							tr:nth-child(odd)	{ background-color:#eee; }
							tr:nth-child(even)	{ background-color:#fff; }	
							</style>'+	
							N'<H3>Long running query alert!</H3>' +
							N'Server: ' + @@SERVERNAME + '<br/>'+
							N'<table id="box-table" >' +
							N'<tr><font color="Green"><th>Elapsed Time</th>
							<th>Session ID</th>
							<th>Database</th>
							<th>Login</th>
							<th>Program</th>
							<th>Status</th>
							<th>Wait Info</th>
							<th>Blocking Session ID</th>
							</tr>' +
							CAST ( ( 

							SELECT td = [dd hh:mm:ss.mss],'',
							td = [session_id],'',
							td = [database_name],'',
							td = [login_name],'',
							td = [program_name],'',
							td = [status],'',
							td = [wait_info],'',
							td = [blocking_session_id]
							FROM #Data
							WHERE collection_time = @CollectionTime
							AND DATEDIFF(n,start_time, collection_time) > @ElapsedMinutes	
							ORDER BY DATEDIFF(n,start_time, collection_time) DESC
							FOR XML PATH('tr'), TYPE 
							) AS NVARCHAR(MAX) ) +
							N'</table>' 

							IF (SELECT COUNT(*) FROM #Data
								WHERE collection_time = @CollectionTime
								AND DATEDIFF(n,start_time, collection_time) > @ElapsedMinutes) > 0 -- Only fire email if long running query found
							BEGIN 

								EXEC msdb.dbo.sp_send_dbmail
								@profile_name	= 'DataWareHouse_Auto_Mails', 
								@recipients='bi.Service@williamhill.com.au',
								@subject = @subject,
								@body = @tableHTML,
								@body_format = 'HTML';
							END
						End

		END
		
		--END TRY
		--BEGIN CATCH
			
		--	DECLARE @Error VARCHAR(1000) = (SELECT ERROR_MESSAGE()); 
			
		--	EXEC [PreProd].[Sp_LogProcess] @BatchKey = @BatchKey, @LogID = @LogID, @ObjectID = @@PROCID, @Status = 'Failed', @RowsAffected = @RowsAffected, @Error = @Error;

		--	THROW;

		--END CATCH
END
