﻿CREATE PROCEDURE [SnowFlake].[Sp_Account_LedgerType]
( 
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET		@BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET		@Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY 
			DROP TABLE #ledgertype  
	END TRY 
	BEGIN CATCH 
	END CATCH	-- Pave the way

BEGIN TRY

	EXEC	[Control].[SP_Log] @BatchKey, @Me, 'Cache', 'Start'

	SELECT	Account_LedgerTypeKey, 
			LedgerTypeID, 
			LedgerType, 
			LedgerGrouping, 
			MAX(src) Maxsrc,
			CASE 
				WHEN COUNT(*) OVER (PARTITION BY LedgerTypeID) = 2 THEN 'U' 
				WHEN MAX(src) = 'S' THEN 'X' 
				WHEN MAX(src) = 'F' THEN 'D' 
				ELSE 'I' 
			END Upsert
	INTO	#ledgertype
	FROM (
			SELECT	Account_LedgerTypeKey, s.LedgerTypeID, LedgerType, LedgerGrouping, 'S' src 
			FROM	[$(Dimensional)].[Snowflake].[Account_LedgerType] s
					INNER JOIN (
						SELECT	DISTINCT LedgerTypeID 
						FROM	[$(Dimensional)].[Dimension].[Account] 
						WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)
					) d ON d.LedgerTypeID = s.LedgerTypeID
			UNION ALL
			SELECT	Account_LedgerTypeKey, LedgerTypeID, LedgerType, LedgerGrouping, 'D' src 
			FROM	[$(Dimensional)].[Dimension].[Account] 
			WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)                                 
		) x
	GROUP BY Account_LedgerTypeKey, LedgerTypeID, LedgerType, LedgerGrouping

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'LedgerType Insert', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT  [$(Dimensional)].[Snowflake].[Account_LedgerType] (Account_LedgerTypeKey, LedgerTypeID, LedgerType, LedgerGrouping)
	SELECT	Account_LedgerTypeKey, LedgerTypeID, LedgerType, LedgerGrouping
	FROM	#ledgertype 
	WHERE	Maxsrc = 'D' AND Upsert  = 'I'
	OPTION(RECOMPILE);

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'LedgerType Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	m
	SET		m.LedgerTypeID = t.LedgerTypeID,
			m.LedgerType = t.LedgerType,
			m.LedgerGrouping = t.LedgerGrouping
	FROM	#ledgertype t
	JOIN	[$(Dimensional)].[Snowflake].[Account_LedgerType] m ON t.Account_LedgerTypeKey = m.Account_LedgerTypeKey
	WHERE	t.Maxsrc = 'D' AND t.Upsert = 'U' 
	OPTION(RECOMPILE);
	
	SET		@RowCount = @@ROWCOUNT
	SET		@RowsProcessed = @RowsProcessed + @RowCount

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

