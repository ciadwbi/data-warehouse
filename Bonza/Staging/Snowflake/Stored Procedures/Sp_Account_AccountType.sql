﻿CREATE PROCEDURE [SnowFlake].[Sp_Account_AccountType]
( 
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET		@BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET		@Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY 
			DROP TABLE #accounttype  
	END TRY 
	BEGIN CATCH 
	END CATCH	-- Pave the way

BEGIN TRY

	EXEC	[Control].[SP_Log] @BatchKey, @Me, 'Cache', 'Start'

	SELECT	Account_AccountTypeKey, 
			AccountTypeID, 
			AccountTypeCode, 
			AccountType, 
			MAX(src) Maxsrc,
			CASE 
				WHEN COUNT(*) OVER (PARTITION BY AccountTypeID) = 2 THEN 'U' 
				WHEN MAX(src) = 'S' THEN 'X' 
				WHEN MAX(src) = 'F' THEN 'D' 
				ELSE 'I' 
			END Upsert
	INTO	#accounttype
	FROM (
			SELECT	Account_AccountTypeKey, s.AccountTypeID, AccountTypeCode, AccountType, 'S' src 
			FROM	[$(Dimensional)].[Snowflake].[Account_AccountType] s
					INNER JOIN (
						SELECT	DISTINCT AccountTypeID 
						FROM	[$(Dimensional)].[Dimension].[Account] 
						WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)
					) d ON d.AccountTypeID = s.AccountTypeID
			UNION ALL
			SELECT	Account_AccountTypeKey, AccountTypeID, AccountTypeCode, AccountType, 'D' src 
			FROM	[$(Dimensional)].[Dimension].[Account] 
			WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)                                 
		) x
	GROUP BY Account_AccountTypeKey, AccountTypeID, AccountTypeCode, AccountType

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'AccountType Insert', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT  [$(Dimensional)].[Snowflake].[Account_AccountType] (Account_AccountTypeKey, AccountTypeID, AccountTypeCode, AccountType)
	SELECT	Account_AccountTypeKey, AccountTypeID, AccountTypeCode, AccountType
	FROM	#accounttype 
	WHERE	Maxsrc = 'D' AND Upsert  = 'I'
	OPTION(RECOMPILE);

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'AccountType Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	m
	SET		m.AccountTypeID = t.AccountTypeID,
			m.AccountTypeCode = t.AccountTypeCode,
			m.AccountType = t.AccountType
	FROM	#accounttype t
	JOIN	[$(Dimensional)].[Snowflake].[Account_AccountType] m ON t.Account_AccountTypeKey = m.Account_AccountTypeKey
	WHERE	t.Maxsrc = 'D' AND t.Upsert = 'U' 
	OPTION(RECOMPILE);
	
	SET		@RowCount = @@ROWCOUNT
	SET		@RowsProcessed = @RowsProcessed + @RowCount

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
