﻿CREATE PROCEDURE [Snowflake].[Sp_Snowflake] @BatchKey int
AS

-----------------ACCOUNT-------------------------------------------
	
	EXEC [Snowflake].[Sp_Account_LedgerType]			@BatchKey	
	EXEC [Snowflake].[Sp_Account_Ledger]				@BatchKey			
	EXEC [Snowflake].[Sp_Account_AccountType]			@BatchKey		
	EXEC [Snowflake].[Sp_Account_Country]				@BatchKey		
	EXEC [Snowflake].[Sp_Account_ManagedBy]				@BatchKey	
	EXEC [Snowflake].[Sp_Account_State]					@BatchKey		
	EXEC [Snowflake].[Sp_Account_TrafficSource]			@BatchKey	
	EXEC [Snowflake].[Sp_Account_Statement]				@BatchKey		
	EXEC [Snowflake].[Sp_Account_Customer]				@BatchKey		
	EXEC [Snowflake].[Sp_Account_Account]				@BatchKey		