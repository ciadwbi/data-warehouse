﻿CREATE PROCEDURE [SnowFlake].[Sp_Account_ManagedBy]
( 
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET		@BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET		@Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY 
			DROP TABLE #managedby  
	END TRY 
	BEGIN CATCH 
	END CATCH	-- Pave the way

BEGIN TRY

	EXEC	[Control].[SP_Log] @BatchKey, @Me, 'Cache', 'Start'

	SELECT	Account_ManagedByKey, 
			ManagedById, 
			ManagedBy, 
			BDM, 
			VIPCode, 
			VIP, 
			MAX(src) Maxsrc,
			CASE 
				WHEN COUNT(*) OVER (PARTITION BY ManagedById) = 2 THEN 'U' 
				WHEN MAX(src) = 'S' THEN 'X' 
				WHEN MAX(src) = 'F' THEN 'D' 
				ELSE 'I' 
			END Upsert
	INTO	#managedby
	FROM (
			SELECT	Account_ManagedByKey, s.ManagedById, ManagedBy, BDM, VIPCode, VIP, 'S' src 
			FROM	[$(Dimensional)].[Snowflake].[Account_ManagedBy] s
					INNER JOIN (
						SELECT	DISTINCT ManagedById 
						FROM	[$(Dimensional)].[Dimension].[Account] 
						WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)
					) d ON d.ManagedById = s.ManagedById
			UNION ALL
			SELECT	Account_ManagedByKey, ManagedById, ManagedBy, BDM, VIPCode, VIP, 'D' src 
			FROM	[$(Dimensional)].[Dimension].[Account] 
			WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)                                 
		) x
	GROUP BY Account_ManagedByKey, ManagedById, ManagedBy, BDM, VIPCode, VIP

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'ManagedBy Insert', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT  [$(Dimensional)].[Snowflake].[Account_ManagedBy] (Account_ManagedByKey, ManagedById, ManagedBy, BDM, VIPCode, VIP)
	SELECT	Account_ManagedByKey, ManagedById, ManagedBy, BDM, VIPCode, VIP 
	FROM	#managedby 
	WHERE	Maxsrc = 'D' AND Upsert  = 'I'
	OPTION(RECOMPILE);

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'ManagedBy Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	m
	SET		m.ManagedById = t.ManagedById,
			m.ManagedBy = t.ManagedBy,
			m.BDM = t.BDM,
			m.VIPCode = t.VIPCode,
			m.VIP = t.VIP
	FROM	#managedby t
	JOIN	[$(Dimensional)].[Snowflake].[Account_ManagedBy] m ON t.Account_ManagedByKey = m.Account_ManagedByKey
	WHERE	t.Maxsrc = 'D' AND t.Upsert = 'U' 
	OPTION(RECOMPILE);
	
	SET		@RowCount = @@ROWCOUNT
	SET		@RowsProcessed = @RowsProcessed + @RowCount

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
