﻿CREATE PROCEDURE [SnowFlake].[Sp_Account_State]
( 
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET		@BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET		@Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY 
			DROP TABLE #state  
	END TRY 
	BEGIN CATCH 
	END CATCH	-- Pave the way

BEGIN TRY

	EXEC	[Control].[SP_Log] @BatchKey, @Me, 'Cache', 'Start'

	SELECT	Account_StateKey, 
			StateID, 
			StateCode, 
			State, 
			MAX(src) Maxsrc,
			CASE 
				WHEN COUNT(*) OVER (PARTITION BY StateID) = 2 THEN 'U' 
				WHEN MAX(src) = 'S' THEN 'X' 
				WHEN MAX(src) = 'F' THEN 'D' 
				ELSE 'I' 
			END Upsert
	INTO	#state
	FROM (
			SELECT	Account_StateKey, s.StateID, StateCode, State, 'S' src 
			FROM	[$(Dimensional)].[Snowflake].[Account_State] s
					INNER JOIN (
						SELECT	DISTINCT StateID 
						FROM	[$(Dimensional)].[Dimension].[Account] 
						WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)
					) d ON d.StateID = s.StateID
			UNION ALL
			SELECT	Account_StateKey, StateID, StateCode, State, 'D' src 
			FROM	[$(Dimensional)].[Dimension].[Account] 
			WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)                                 
		) x
	GROUP BY Account_StateKey, StateID, StateCode, State

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'State Insert', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT  [$(Dimensional)].[Snowflake].[Account_State] (Account_StateKey, StateID, StateCode, State)
	SELECT	Account_StateKey, StateID, StateCode, State 
	FROM	#state 
	WHERE	Maxsrc = 'D' AND Upsert  = 'I'
	OPTION(RECOMPILE);

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'State Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	m
	SET		m.StateID = t.StateID,
			m.StateCode = t.StateCode,
			m.State = t.State
	FROM	#state t
	JOIN	[$(Dimensional)].[Snowflake].[Account_State] m ON t.Account_StateKey = m.Account_StateKey
	WHERE	t.Maxsrc = 'D' AND t.Upsert = 'U' 
	OPTION(RECOMPILE);
	
	SET		@RowCount = @@ROWCOUNT
	SET		@RowsProcessed = @RowsProcessed + @RowCount

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
