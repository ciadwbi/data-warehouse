﻿CREATE PROCEDURE [SnowFlake].[Sp_Account_Account]
( 
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET		@BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET		@Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY 
			DROP TABLE #account  
	END TRY 
	BEGIN CATCH 
	END CATCH	-- Pave the way

BEGIN TRY

	EXEC	[Control].[SP_Log] @BatchKey, @Me, 'Cache', 'Start'

	SELECT	Account_AccountKey, AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
			AccountOpenedDate, AccountOpenedDayKey, AccountOpenedChannelKey, ReferralBurnCode, ReferralBurntDate, ReferralAccountKey,
			EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, CardStatusId, CardStatusCode, CardStatus, VelocityNumber,
			VelocityLinkedDate, RewardStatusDate, RewardStatusId, RewardStatusCode, RewardStatus, CompanyKey, Account_AccountTypeKey,
			AccountTypeId, AccountTypeCode, AccountType, Account_ManagedByKey, ManagedById, ManagedBy, BDM, VIPCode, VIP, Account_TrafficSourceKey,
			TrafficSourceId, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords, Account_StatementKey,
			StatementId, StatementMethod, StatementFrequency, Account_CustomerKey, CustomerID, CustomerSource, Title, Surname, FirstName, 
			MiddleName, Gender, DOB, Phone, Mobile, Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode,
			AddressLastChanged, Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient, 
			MAX(src) Maxsrc,
			CASE 
				WHEN COUNT(*) OVER (PARTITION BY AccountID) = 2 THEN 'U' 
				WHEN MAX(src) = 'S' THEN 'X' 
				WHEN MAX(src) = 'F' THEN 'D' 
				ELSE 'I' 
			END Upsert
	INTO	#account
	FROM (
			SELECT	Account_AccountKey, s.AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
					AccountOpenedDate, AccountOpenedDayKey, AccountOpenedChannelKey, ReferralBurnCode, ReferralBurntDate, ReferralAccountKey,
					EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, CardStatusId, CardStatusCode, CardStatus, VelocityNumber,
					VelocityLinkedDate, RewardStatusDate, RewardStatusId, RewardStatusCode, RewardStatus, CompanyKey, Account_AccountTypeKey,
					AccountTypeId, AccountTypeCode, AccountType, Account_ManagedByKey, ManagedById, ManagedBy, BDM, VIPCode, VIP, Account_TrafficSourceKey,
					TrafficSourceId, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords, Account_StatementKey,
					StatementId, StatementMethod, StatementFrequency, Account_CustomerKey, CustomerID, CustomerSource, Title, Surname, FirstName, 
					MiddleName, Gender, DOB, Phone, Mobile, Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode,
					AddressLastChanged, Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient, 
					'S' src 
			FROM	[$(Dimensional)].[Snowflake].[Account_Account] s
					INNER JOIN (
						SELECT	DISTINCT AccountID 
						FROM	[$(Dimensional)].[Dimension].[Account] 
						WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)
					) d ON d.AccountID = s.AccountID
			UNION ALL
			SELECT	Account_AccountKey, AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
					AccountOpenedDate, AccountOpenedDayKey, AccountOpenedChannelKey, ReferralBurnCode, ReferralBurntDate, ReferralAccountKey,
					EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, CardStatusId, CardStatusCode, CardStatus, VelocityNumber,
					VelocityLinkedDate, RewardStatusDate, RewardStatusId, RewardStatusCode, RewardStatus, CompanyKey, Account_AccountTypeKey,
					AccountTypeId, AccountTypeCode, AccountType, Account_ManagedByKey, ManagedById, ManagedBy, BDM, VIPCode, VIP, Account_TrafficSourceKey,
					TrafficSourceId, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords, Account_StatementKey,
					StatementId, StatementMethod, StatementFrequency, Account_CustomerKey, CustomerID, CustomerSource, Title, Surname, FirstName, 
					MiddleName, Gender, DOB, Phone, Mobile, Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode,
					AddressLastChanged, Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient, 
					'D' src
			FROM	[$(Dimensional)].[Dimension].[Account] 
			WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)                                 
		) x
	GROUP BY Account_AccountKey, AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
			AccountOpenedDate, AccountOpenedDayKey, AccountOpenedChannelKey, ReferralBurnCode, ReferralBurntDate, ReferralAccountKey,
			EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, CardStatusId, CardStatusCode, CardStatus, VelocityNumber,
			VelocityLinkedDate, RewardStatusDate, RewardStatusId, RewardStatusCode, RewardStatus, CompanyKey, Account_AccountTypeKey,
			AccountTypeId, AccountTypeCode, AccountType, Account_ManagedByKey, ManagedById, ManagedBy, BDM, VIPCode, VIP, Account_TrafficSourceKey,
			TrafficSourceId, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords, Account_StatementKey,
			StatementId, StatementMethod, StatementFrequency, Account_CustomerKey, CustomerID, CustomerSource, Title, Surname, FirstName, 
			MiddleName, Gender, DOB, Phone, Mobile, Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode,
			AddressLastChanged, Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'Account Insert', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT  [$(Dimensional)].[Snowflake].[Account_Account] (
			Account_AccountKey, AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
			AccountOpenedDate, AccountOpenedDayKey, AccountOpenedChannelKey, ReferralBurnCode, ReferralBurntDate, ReferralAccountKey,
			EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, CardStatusId, CardStatusCode, CardStatus, VelocityNumber,
			VelocityLinkedDate, RewardStatusDate, RewardStatusId, RewardStatusCode, RewardStatus, CompanyKey, Account_AccountTypeKey,
			AccountTypeId, AccountTypeCode, AccountType, Account_ManagedByKey, ManagedById, ManagedBy, BDM, VIPCode, VIP, Account_TrafficSourceKey,
			TrafficSourceId, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords, Account_StatementKey,
			StatementId, StatementMethod, StatementFrequency, Account_CustomerKey, CustomerID, CustomerSource, Title, Surname, FirstName, 
			MiddleName, Gender, DOB, Phone, Mobile, Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode,
			AddressLastChanged, Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient)
	SELECT	
			Account_AccountKey, AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
			AccountOpenedDate, AccountOpenedDayKey, AccountOpenedChannelKey, ReferralBurnCode, ReferralBurntDate, ReferralAccountKey,
			EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, CardStatusId, CardStatusCode, CardStatus, VelocityNumber,
			VelocityLinkedDate, RewardStatusDate, RewardStatusId, RewardStatusCode, RewardStatus, CompanyKey, Account_AccountTypeKey,
			AccountTypeId, AccountTypeCode, AccountType, Account_ManagedByKey, ManagedById, ManagedBy, BDM, VIPCode, VIP, Account_TrafficSourceKey,
			TrafficSourceId, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords, Account_StatementKey,
			StatementId, StatementMethod, StatementFrequency, Account_CustomerKey, CustomerID, CustomerSource, Title, Surname, FirstName, 
			MiddleName, Gender, DOB, Phone, Mobile, Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode,
			AddressLastChanged, Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient
	FROM	#account 
	WHERE	Maxsrc = 'D' AND Upsert  = 'I'
	OPTION(RECOMPILE);

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'Account Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	m
	SET		m.AccountID = t.AccountID,
			m.AccountSource = t.AccountSource,
			m.PIN = t.PIN,
			m.UserName = t.UserName,
			m.PasswordHash = t.PasswordHash,
			m.PasswordLastChanged = t.PasswordLastChanged,
			m.ExactTargetID = t.ExactTargetID,
			m.AccountFlags = t.AccountFlags,
			m.AccountOpenedDate = t.AccountOpenedDate,
			m.AccountOpenedDayKey = t.AccountOpenedDayKey,
			m.AccountOpenedChannelKey = t.AccountOpenedChannelKey,
			m.ReferralBurnCode = t.ReferralBurnCode,
			m.ReferralBurntDate = t.ReferralBurntDate,
			m.ReferralAccountKey = t.ReferralAccountKey,
			m.EachwayAccountId = t.EachwayAccountId,
			m.CardAccountId = t.CardAccountId,
			m.CardFirstIssueDate = t.CardFirstIssueDate,
			m.CardIssueDate = t.CardIssueDate,
			m.CardStatusId = t.CardStatusId,
			m.CardStatusCode = t.CardStatusCode,
			m.CardStatus = t.CardStatus,
			m.VelocityNumber = t.VelocityNumber,
			m.VelocityLinkedDate = t.VelocityLinkedDate,
			m.RewardStatusDate = t.RewardStatusDate,
			m.RewardStatusId = t.RewardStatusId,
			m.RewardStatusCode = t.RewardStatusCode,
			m.RewardStatus = t.RewardStatus,
			m.CompanyKey = t.CompanyKey,
			m.Account_AccountTypeKey = t.Account_AccountTypeKey,
			m.AccountTypeId = t.AccountTypeId,
			m.AccountTypeCode = t.AccountTypeCode,
			m.AccountType = t.AccountType,
			m.Account_ManagedByKey = t.Account_ManagedByKey,
			m.ManagedById = t.ManagedById,
			m.ManagedBy = t.ManagedBy,
			m.BDM = t.BDM,
			m.VIPCode = t.VIPCode,
			m.VIP = t.VIP,
			m.Account_TrafficSourceKey = t.Account_TrafficSourceKey,
			m.TrafficSourceId = t.TrafficSourceId,
			m.BTag2 = t.BTag2,
			m.AffiliateID = t.AffiliateID,
			m.AffiliateName = t.AffiliateName,
			m.SiteID = t.SiteID,
			m.TrafficSource = t.TrafficSource,
			m.RefURL = t.RefURL,
			m.CampaignID = t.CampaignID,
			m.Keywords = t.Keywords,
			m.Account_StatementKey = t.Account_StatementKey,
			m.StatementId = t.StatementId,
			m.StatementMethod = t.StatementMethod,
			m.StatementFrequency = t.StatementFrequency,
			m.Account_CustomerKey = t.Account_CustomerKey,
			m.CustomerID = t.CustomerID,
			m.CustomerSource = t.CustomerSource,
			m.Title = t.Title,
			m.Surname = t.Surname,
			m.FirstName = t.FirstName,
			m.MiddleName = t.MiddleName,
			m.Gender = t.Gender,
			m.DOB = t.DOB,
			m.Phone = t.Phone,
			m.Mobile = t.Mobile,
			m.Fax = t.Fax,
			m.PhoneLastChanged = t.PhoneLastChanged,
			m.Email = t.Email,
			m.EmailLastChanged = t.EmailLastChanged,
			m.Address1 = t.Address1,
			m.Address2 = t.Address2,
			m.Suburb = t.Suburb,
			m.PostCode = t.PostCode,
			m.AddressLastChanged = t.AddressLastChanged,
			m.Account_StateKey = t.Account_StateKey,
			m.StateId = t.StateId,
			m.StateCode = t.StateCode,
			m.[State] = t.[State],
			m.Account_CountryKey = t.Account_CountryKey,
			m.CountryId = t.CountryId,
			m.CountryCode = t.CountryCode,
			m.Country = t.Country,
			m.AustralianClient = t.AustralianClient
	FROM	#account t
	JOIN	[$(Dimensional)].[Snowflake].[Account_Account] m ON t.Account_AccountKey = m.Account_AccountKey
	WHERE	t.Maxsrc = 'D' AND t.Upsert = 'U' 
	OPTION(RECOMPILE);
	
	SET		@RowCount = @@ROWCOUNT
	SET		@RowsProcessed = @RowsProcessed + @RowCount

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

