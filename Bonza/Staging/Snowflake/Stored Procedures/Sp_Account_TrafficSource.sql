﻿CREATE PROCEDURE [SnowFlake].[Sp_Account_TrafficSource]
( 
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET		@BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET		@Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY 
			DROP TABLE #traffic  
	END TRY 
	BEGIN CATCH 
	END CATCH	-- Pave the way

BEGIN TRY

	EXEC	[Control].[SP_Log] @BatchKey, @Me, 'Cache', 'Start'

	SELECT	Account_TrafficSourceKey, 
			TrafficSourceID, 
			BTag2, 
			AffiliateID, 
			AffiliateName,
			SiteID,
			TrafficSource,
			RefURL, 
			CampaignID, 
			Keywords,
			MAX(src) Maxsrc,
			CASE 
				WHEN COUNT(*) OVER (PARTITION BY TrafficSourceID) = 2 THEN 'U' 
				WHEN MAX(src) = 'S' THEN 'X' 
				WHEN MAX(src) = 'F' THEN 'D' 
				ELSE 'I' 
			END Upsert
	INTO	#traffic
	FROM (
			SELECT	Account_TrafficSourceKey, s.TrafficSourceID, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords, 'S' src 
			FROM	[$(Dimensional)].[Snowflake].[Account_TrafficSource] s
					INNER JOIN (
						SELECT	DISTINCT TrafficSourceID 
						FROM	[$(Dimensional)].[Dimension].[Account] 
						WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)
					) d ON d.TrafficSourceID = s.TrafficSourceID
			UNION ALL
			SELECT	Account_TrafficSourceKey, TrafficSourceID, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords, 'D' src 
			FROM	[$(Dimensional)].[Dimension].[Account] 
			WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)                                 
		) x
	GROUP BY Account_TrafficSourceKey, TrafficSourceID, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'TrafficSource Insert', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT  [$(Dimensional)].[Snowflake].[Account_TrafficSource] (Account_TrafficSourceKey, TrafficSourceID, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords)
	SELECT	Account_TrafficSourceKey, TrafficSourceID, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords
	FROM	#traffic 
	WHERE	Maxsrc = 'D' AND Upsert  = 'I'
	OPTION(RECOMPILE);

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'TrafficSource Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	m
	SET		m.TrafficSourceID = t.TrafficSourceID,
			m.BTag2 = t.BTag2,
			m.AffiliateID = t.AffiliateID,
			m.AffiliateName = t.AffiliateName,
			m.SiteID = t.SiteID,
			m.TrafficSource = t.TrafficSource,
			m.RefURL = t.RefURL, 
			m.CampaignID = t.CampaignID, 
			m.Keywords = t.Keywords
	FROM	#traffic t
	JOIN	[$(Dimensional)].[Snowflake].[Account_TrafficSource] m ON t.Account_TrafficSourceKey = m.Account_TrafficSourceKey
	WHERE	t.Maxsrc = 'D' AND t.Upsert = 'U' 
	OPTION(RECOMPILE);
	
	SET		@RowCount = @@ROWCOUNT
	SET		@RowsProcessed = @RowsProcessed + @RowCount

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH