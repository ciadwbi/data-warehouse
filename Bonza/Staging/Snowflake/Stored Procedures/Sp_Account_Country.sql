﻿CREATE PROCEDURE [SnowFlake].[Sp_Account_Country]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET		@BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET		@Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY 
			DROP TABLE #country 
	END TRY 
	BEGIN CATCH 
	END CATCH	-- Pave the way

BEGIN TRY

	EXEC	[Control].[Sp_Log] @BatchKey, @Me, 'Cache', 'Start'

	SELECT	Account_CountryKey, 
			CountryId, 
			CountryCode,
			Country, 
			MAX(src) Maxsrc,
			CASE 
				WHEN COUNT(*) OVER (PARTITION BY CountryId) = 2 THEN 'U' 
				WHEN MAX(src) = 'S' THEN 'X' 
				WHEN MAX(src) = 'F' THEN 'D' 
				ELSE 'I' 
			END Upsert
	INTO	#country
	FROM (
			SELECT	Account_CountryKey, s.CountryId, CountryCode, Country, 'S' src 
			FROM	[$(Dimensional)].[Snowflake].[Account_Country] s
					INNER JOIN (
						SELECT	DISTINCT CountryId 
						FROM	[$(Dimensional)].[Dimension].[Account] 
						WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)
					) d ON d.CountryId = s.CountryId
			UNION ALL
			SELECT	Account_CountryKey, CountryId, CountryCode, Country, 'D' src 
			FROM	[$(Dimensional)].[Dimension].[Account] 
			WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)                                 
		) x
	GROUP BY Account_CountryKey, CountryId, CountryCode, Country

	EXEC	[Control].[Sp_Log] @BatchKey,@Me, 'Country Insert', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT  [$(Dimensional)].[Snowflake].[Account_Country] (Account_CountryKey, CountryId, CountryCode, Country)
	SELECT	Account_CountryKey, CountryId, CountryCode, Country
	FROM	#country 
	WHERE	Maxsrc = 'D' AND Upsert  = 'I'
	OPTION(RECOMPILE);

	EXEC	[Control].[Sp_Log] @BatchKey,@Me, 'Country Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	c
	SET		c.CountryId = t.CountryId,
			c.CountryCode = t.CountryCode,
			c.Country = t.Country
	FROM	#country t
	JOIN	[$(Dimensional)].[Snowflake].[Account_Country] c ON t.Account_CountryKey = c.Account_CountryKey
	WHERE	t.Maxsrc = 'D' AND t.Upsert = 'U' 
	OPTION(RECOMPILE);
	
	SET		@RowCount = @@ROWCOUNT
	SET		@RowsProcessed = @RowsProcessed + @RowCount

	EXEC	[Control].[Sp_Log] @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC	[Control].[Sp_Log] @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
