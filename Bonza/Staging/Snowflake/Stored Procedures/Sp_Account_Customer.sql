﻿CREATE PROCEDURE [SnowFlake].[Sp_Account_Customer]
( 
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET		@BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET		@Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY 
			DROP TABLE #customer  
	END TRY 
	BEGIN CATCH 
	END CATCH	-- Pave the way

BEGIN TRY

	EXEC	[Control].[SP_Log] @BatchKey, @Me, 'Cache', 'Start'

	SELECT	Account_CustomerKey, CustomerID, CustomerSource, Title, Surname, FirstName, MiddleName, Gender, DOB, Phone, Mobile, 
			Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode, AddressLastChanged, 
			Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient, 
			MAX(src) Maxsrc,
			CASE 
				WHEN COUNT(*) OVER (PARTITION BY CustomerID) = 2 THEN 'U' 
				WHEN MAX(src) = 'S' THEN 'X' 
				WHEN MAX(src) = 'F' THEN 'D' 
				ELSE 'I' 
			END Upsert
	INTO	#customer
	FROM (
			SELECT	Account_CustomerKey, s.CustomerID, CustomerSource, Title, Surname, FirstName, MiddleName, Gender, DOB, Phone, Mobile, 
					Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode, AddressLastChanged, 
					Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient, 
					'S' src 
			FROM	[$(Dimensional)].[Snowflake].[Account_Customer] s
					INNER JOIN (
						SELECT	DISTINCT CustomerID 
						FROM	[$(Dimensional)].[Dimension].[Account] 
						WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)
					) d ON d.CustomerID = s.CustomerID
			UNION ALL
			SELECT	Account_CustomerKey, CustomerID, CustomerSource, Title, Surname, FirstName, MiddleName, Gender, DOB, Phone, Mobile, 
					Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode, AddressLastChanged, 
					Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient, 
					'D' src
			FROM	[$(Dimensional)].[Dimension].[Account] 
			WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)                                 
		) x
	GROUP BY Account_CustomerKey, CustomerID, CustomerSource, Title, Surname, FirstName, MiddleName, Gender, DOB, Phone, Mobile, 
			Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode, AddressLastChanged, 
			Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'Customer Insert', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT  [$(Dimensional)].[Snowflake].[Account_Customer] (
			Account_CustomerKey, CustomerID, CustomerSource, Title, Surname, FirstName, MiddleName, Gender, DOB, Phone, Mobile, 
			Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode, AddressLastChanged, 
			Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient)
	SELECT	
			Account_CustomerKey, CustomerID, CustomerSource, Title, Surname, FirstName, MiddleName, Gender, DOB, Phone, Mobile, 
			Fax, PhoneLastChanged, Email, EmailLastChanged, Address1, Address2, Suburb, PostCode, AddressLastChanged, 
			Account_StateKey, StateId, StateCode, [State], Account_CountryKey, CountryId, CountryCode, Country, AustralianClient
	FROM	#customer 
	WHERE	Maxsrc = 'D' AND Upsert  = 'I'
	OPTION(RECOMPILE);

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'Customer Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	m
	SET		m.Account_CustomerKey = t.Account_CustomerKey, 
			m.CustomerID = t.CustomerID, 
			m.CustomerSource = t.CustomerSource, 
			m.Title = t.Title, 
			m.Surname = t.Surname, 
			m.FirstName = t.FirstName, 
			m.MiddleName = t.MiddleName, 
			m.Gender = t.Gender, 
			m.DOB = t.DOB, 
			m.Phone = t.Phone, 
			m.Mobile = t.Mobile,
			m.Fax = t.Fax, 
			m.PhoneLastChanged = t.PhoneLastChanged, 
			m.Email = t.Email, 
			m.EmailLastChanged = t.EmailLastChanged, 
			m.Address1 = t.Address1, 
			m.Address2 = t.Address2, 
			m.Suburb = t.Suburb, 
			m.PostCode = t.PostCode, 
			m.AddressLastChanged = t.AddressLastChanged, 
			m.Account_StateKey = t.Account_StateKey, 
			m.StateId = t.StateId, 
			m.StateCode = t.StateCode, 
			m.[State] = t.[State], 
			m.Account_CountryKey = t.Account_CountryKey, 
			m.CountryId = t.CountryId, 
			m.CountryCode = t.CountryCode, 
			m.Country = t.Country, 
			m.AustralianClient = t.AustralianClient
	FROM	#customer t
	JOIN	[$(Dimensional)].[Snowflake].[Account_Customer] m ON t.Account_CustomerKey = m.Account_CustomerKey
	WHERE	t.Maxsrc = 'D' AND t.Upsert = 'U' 
	OPTION(RECOMPILE);
	
	SET		@RowCount = @@ROWCOUNT
	SET		@RowsProcessed = @RowsProcessed + @RowCount

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
