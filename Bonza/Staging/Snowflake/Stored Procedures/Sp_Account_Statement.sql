﻿CREATE PROCEDURE [SnowFlake].[Sp_Account_Statement]
( 
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET		@BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET		@Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY 
			DROP TABLE #statement  
	END TRY 
	BEGIN CATCH 
	END CATCH	-- Pave the way

BEGIN TRY

	EXEC	[Control].[SP_Log] @BatchKey, @Me, 'Cache', 'Start'

	SELECT	Account_StatementKey, 
			StatementID, 
			StatementMethod, 
			StatementFrequency, 
			MAX(src) Maxsrc,
			CASE 
				WHEN COUNT(*) OVER (PARTITION BY StatementID) = 2 THEN 'U' 
				WHEN MAX(src) = 'S' THEN 'X' 
				WHEN MAX(src) = 'F' THEN 'D' 
				ELSE 'I' 
			END Upsert
	INTO	#statement
	FROM (
			SELECT	Account_StatementKey, s.StatementID, StatementMethod, StatementFrequency, 'S' src 
			FROM	[$(Dimensional)].[Snowflake].[Account_Statement] s
					INNER JOIN (
						SELECT	DISTINCT StatementID 
						FROM	[$(Dimensional)].[Dimension].[Account] 
						WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)
					) d ON d.StatementID = s.StatementID
			UNION ALL
			SELECT	Account_StatementKey, StatementID, StatementMethod, StatementFrequency, 'D' src 
			FROM	[$(Dimensional)].[Dimension].[Account] 
			WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)	AND (Increment = @Increment OR @Increment = 0)                                 
		) x
	GROUP BY Account_StatementKey, StatementID, StatementMethod, StatementFrequency

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'Statement Insert', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT  [$(Dimensional)].[Snowflake].[Account_Statement] (Account_StatementKey, StatementID, StatementMethod, StatementFrequency)
	SELECT	Account_StatementKey, StatementID, StatementMethod, StatementFrequency 
	FROM	#statement 
	WHERE	Maxsrc = 'D' AND Upsert  = 'I'
	OPTION(RECOMPILE);

	EXEC	[Control].[SP_Log] @BatchKey,@Me, 'Statement Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	m
	SET		m.StatementID = t.StatementID,
			m.StatementMethod = t.StatementMethod,
			m.StatementFrequency = t.StatementFrequency
	FROM	#statement t
	JOIN	[$(Dimensional)].[Snowflake].[Account_Statement] m ON t.Account_StatementKey = m.Account_StatementKey
	WHERE	t.Maxsrc = 'D' AND t.Upsert = 'U' 
	OPTION(RECOMPILE);
	
	SET		@RowCount = @@ROWCOUNT
	SET		@RowsProcessed = @RowsProcessed + @RowCount

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC	[Control].[SP_Log] @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH