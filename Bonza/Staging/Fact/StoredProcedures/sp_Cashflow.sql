﻿CREATE PROC	 [Fact].[sp_Cashflow]
(
	@BatchKey		int,
	@MappingId		smallint = NULL,
	@DryRun			bit = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount int

	DECLARE @FactsExist bit

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Legacy','Start'

	SELECT	CONVERT(binary(16),
				CASE TransactionIdSource 
					WHEN 'IBB' THEN -TransactionID/10 
					WHEN 'IBP' THEN -TransactionID/10 
					WHEN 'IBC' THEN TransactionID
					WHEN 'IBT' THEN TransactionId
					WHEN 'WRB' THEN TransactionId
					WHEN 'LTI' THEN LegId
					ELSE LegId
				END) Id,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN 'IBB'
				WHEN 'IBP' THEN 'IBB'
				WHEN 'IBC' THEN 'IAA'
				WHEN 'IBT' THEN 'IBT'
				WHEN 'WRB' THEN 'WRI'
				WHEN 'LTI' THEN 'IBI'
				ELSE LegIdSource
			END IdSource,
			TransactionTypeID,
			TransactionStatusID,
			TransactionMethodID,
			TransactionDirection,
			BalanceTypeID,
			MasterDayText,
			CONVERT(datetime2(3),MasterTransactionTimestamp) MasterTimestamp,
			MasterTimeText,
			ISNULL(DATEADD(MINUTE, z.OffsetMinutes, CONVERT(datetime2(3),MasterTransactionTimestamp)),'1900-01-01') AnalysisTimestamp,
			CONVERT(varchar(5),CONVERT(time,DATEADD(MINUTE, z.OffsetMinutes, MasterTransactionTimestamp))) AnalysisTimeText,
			LegId ContractLegId, -- Temporary measure until entire Contract dimension can be dropped in favour of local columns on fact record
			LegIDSource ContractLegSource,
			LedgerID,
			LedgerSource,
			WalletId,
			PriceTypeId,
			PriceTypeSource,
			BetTypeName BetType,
			LegBetTypeName LegType,
			BetGroupType,
			BetSubTypeName BetSubType,
			GroupingType,
			NumberOfLegs,
			LegNumber,
			ClassId,
			ClassIdSource ClassSource,
			EventId,
			EventIdSource EventSource,
			CONVERT(varchar(255),NULL) SourceEvent, -- Placeholders for subsequent lottery transactions which do not have EventIds, only product and draw date
			CONVERT(datetime2(3),NULL) SourceEventDate,
			MarketId,
			MarketIdSource MarketSource,
			Channel ChannelId,
			ActionChannel ActionChannelId,
			UserId,
			UserIdSource UserSource,
			InstrumentId,
			InstrumentIdSource InstrumentSource,
			CASE TransactionIdSource WHEN 'IBT' THEN TransactionId ELSE 0 END NoteId,
			CASE TransactionIdSource WHEN 'IBT' THEN 'ITN' ELSE 'N/A' END NoteSource,
			CASE InPlay WHEN 'Y' THEN 1 ELSE 0 END HeadlineFeatures,
			InPlay,
			ClickToCall,
			CashoutType,
			IsDoubleDown,
			IsDoubledDown,
			IsChaseTheAce,
			/*0*/ FeatureFlags, -- temp change to run historic against stage.transactionx
			PromotionId,
			PromotionIdSource PromotionSource,
			TransactedAmount Amount,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN -TransactionID/10 
				WHEN 'IBP' THEN -TransactionID/10 
				WHEN 'IBC' THEN TransactionID
				WHEN 'IBT' THEN TransactionId
				WHEN 'WRB' THEN TransactionId
				WHEN 'LTI' THEN TransactionId
				ELSE TransactionId
			END TransactionId,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN 'IBB'
				WHEN 'IBP' THEN 'IBB'
				WHEN 'IBC' THEN 'IAA'
				WHEN 'IBT' THEN 'IBT'
				WHEN 'WRB' THEN 'WRI'
				WHEN 'LTI' THEN 'IBT'
				ELSE TransactionIdSource
			END TransactionSource,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN -TransactionID/10
				WHEN 'IBP' THEN -TransactionID/10
				WHEN 'IBC' THEN TransactionID
				WHEN 'IBT' THEN TransactionId
				WHEN 'WRB' THEN TransactionId
				WHEN 'LTI' THEN BetGroupId
				ELSE BetGroupID
			END GroupId,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN 'IBB'
				WHEN 'IBP' THEN 'IBB'
				WHEN 'IBC' THEN 'IAA'
				WHEN 'IBT' THEN 'IBT'
				WHEN 'WRB' THEN 'WRI'
				WHEN 'LTI' THEN 'IBT'
				ELSE BetGroupIDSource
			END GroupSource,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN NULL 
				WHEN 'IBP' THEN NULL 
				WHEN 'IBC' THEN NULL
				WHEN 'IBT' THEN NULL
				WHEN 'WRB' THEN NULL
				WHEN 'LTI' THEN BetId
				ELSE BetId
			END BetId,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN 'N/A' 
				WHEN 'IBP' THEN 'N/A' 
				WHEN 'IBC' THEN 'N/A'
				WHEN 'IBT' THEN 'N/A'
				WHEN 'WRB' THEN 'N/A'
				WHEN 'LTI' THEN 'IBT'
				ELSE BetIdSource
			END BetSource,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN NULL 
				WHEN 'IBP' THEN NULL 
				WHEN 'IBC' THEN NULL
				WHEN 'IBT' THEN NULL
				WHEN 'WRB' THEN NULL
				WHEN 'LTI' THEN LegId
				ELSE LegId
			END LegId,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN 'N/A' 
				WHEN 'IBP' THEN 'N/A' 
				WHEN 'IBC' THEN 'N/A'
				WHEN 'IBT' THEN 'N/A'
				WHEN 'WRB' THEN 'N/A'
				WHEN 'LTI' THEN 'IBT'
				ELSE BetIdSource
			END LegSource,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN -LegId/10
				WHEN 'IBP' THEN -LegId/10 
				WHEN 'IBC' THEN NULL
				WHEN 'IBT' THEN NULL
				WHEN 'WRB' THEN NULL
				WHEN 'LTI' THEN NULL
				ELSE NULLIF(FreeBetId,0)
			END FreeBetId,
			NULL ExternalReference,
			'N/A' ExternalSource,
			CONVERT(int,MappingId) MappingId,
			CASE 
				WHEN TransactionIdSource = 'LTI' THEN 'L' -- Lottery
				WHEN TransactionIdSource = 'WRB' THEN 'R' -- Reward
				WHEN TransactionTypeId = 'BT' THEN 'B' -- Bet
				WHEN TransactionTypeId = 'FE' THEN 'B' -- Bet (Double Down Fees)
				WHEN TransactionTypeId = 'BO' AND TransactionMethodId = 'BP' THEN 'B' -- Bet (Bonus Accrual)
				WHEN TransactionTypeId = 'DP' THEN 'D' -- Deposit
				WHEN TransactionTypeId = 'WI' THEN 'W' -- Withdrawal
				WHEN TransactionTypeId = 'BO' THEN 'O' -- Bonus
				WHEN TransactionTypeId = 'AD' THEN 'A' -- Adjustment
				WHEN TransactionTypeId = 'TR' THEN 'T' -- Transfer
				ELSE 'X'
			END FunctionalArea,
			s.FromDate
	INTO	#Collect
	FROM	Stage.[Transaction] s
			LEFT JOIN Reference.TimeZone z ON z.FromDate <= CONVERT(datetime2(3),MasterTransactionTimestamp) AND z.ToDate > CONVERT(datetime2(3),MasterTransactionTimestamp) AND z.TimeZone='Analysis'
	WHERE	ExistsInDim = 0
			AND BatchKey = @BatchKey
			AND (MappingId = @MappingId OR @MappingId IS NULL)
			AND TransactionTypeID NOT IN ('WI') -- Withdrawals moved to their own section below based on dedicated Atomic table
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Lottery','Start',@RowsProcessed = @@ROWCOUNT
	INSERT	#Collect
	SELECT	CONVERT(binary(16),TicketId) Id,
			'IBI' IdSource,
			x.TransactionTypeID,
			x.TransactionStatusID,
			x.TransactionMethodID,
			x.TransactionDirection,
			x.BalanceTypeID,
			CONVERT(CHAR(11), CONVERT(DATE, a.MasterTransactionTimestamp)) MasterDayText,
			a.MasterTransactionTimestamp,
			CONVERT(varchar(5),CONVERT(time, a.MasterTransactionTimestamp)) MasterTimeText,
			DATEADD(MINUTE, z.OffsetMinutes, a.MasterTransactionTimestamp) AnalysisTransactionTimestamp, 
			CONVERT(varchar(5), CONVERT(time, DATEADD(MINUTE, z.OffsetMinutes, a.MasterTransactionTimestamp))) AnalysisTimeText,
			TicketId ContractLegId, -- Temporary measure until entire Contract dimension can be dropped in favour of local columns on fact record
			'LTI' ContractLegSource,
			a.LedgerID,
			a.LedgerSource,
			'C' WalletId,
			0 PriceTypeId,
			'N/A' PriceTypeSource,
			'Single' BetType,
			'Win' LegType,
			'Straight' BetGroupType,
			'Single' BetSubType,
			'Standard' GroupingType,
			1 NumberOfLegs,
			1 LegNumber,
			-10 ClassId,
			'LOT' ClassSource,
			a.TicketId EventId,
			'LTE' EventSource,
			a.Product SourceEvent, -- Provided because by default Lottery has no EventId - have to match on product and draw date (unless draw date was null and one-off dummy eventid has been created using TicketId)
			a.DrawDate SourceEventDate,
			0 MarketId,
			'N/A' MarketSource,
			0 ChannelId,
			0 ActionChannelId,
			'N/A' UserId,
			'N/A' UserSource,
			0 InstrumentId,
			'N/A' InstrumentSource,
			0 NoteId,
			'N/A' NoteSource,
			2 HeadlineFeatures,
			'N/A' InPlay,
			'N/A' ClickToCall,
			0 CashoutType,
			0 IsDoubleDown,
			0 IsDoubledDown,
			0 IsChaseTheAce,
			0 FeatureFlags,
			0 PromotionId,
			'N/A' PromotionSource,
			a.Stake * x.StakeFactor + a.Payout * x.PayoutFactor Amount,
			TransactionId,
			'IBT' TransactionSource,
			TicketId GroupId,
			'IBT' GroupSource,
			TicketId BetId,
			'IBT'  BetSource,
			TicketId LegId,
			'IBT' LegSource,
			NULL FreeBetId,
			NULL ExternalReference,
			'N/A' ExternalSource,
			a.MappingId,
			'L' FunctionalArea,
			a.FromDate
	FROM	Atomic.Lottery a
			INNER JOIN Reference.TimeZone z ON z.FromDate <= a.MasterTransactionTimestamp AND z.ToDate > a.MasterTransactionTimestamp AND z.TimeZone='Analysis'
			INNER JOIN Reference.fn_Mapping_Cashflow('Lottery') x ON x.TransactionCode = a.TransactionCode
	WHERE	BatchKey = @BatchKey
			AND (a.MappingId = @MappingId OR @MappingId IS NULL)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Withdrawal','Start',@RowsProcessed = @@ROWCOUNT

	INSERT	#Collect
	SELECT	CONVERT(binary(16),a.TransactionId) Id,
			a.TransactionSource IdSource,
			x.TransactionTypeID,
			x.TransactionStatusID,
			x.TransactionMethodID,
			x.TransactionDirection,
			x.BalanceTypeID,
			CONVERT(CHAR(11), CONVERT(DATE, a.MasterTimestamp)) MasterDayText,
			a.MasterTimestamp,
			CONVERT(varchar(5),CONVERT(time, a.MasterTimestamp)) MasterTimeText,
			DATEADD(MINUTE, z.OffsetMinutes, a.MasterTimestamp) AnalysisTransactionTimestamp, 
			CONVERT(varchar(5), CONVERT(time, DATEADD(MINUTE, z.OffsetMinutes, a.MasterTimestamp))) AnalysisTimeText,
			a.TransactionId ContractLegId, -- Temporary measure until entire Contract dimension can be dropped in favour of local columns on fact record
			a.TransactionSource ContractLegSource,
			a.LedgerID,
			a.LedgerSource,
			'C' WalletId,
			0 PriceTypeId,
			'N/A' PriceTypeSource,
			'N/A' BetType,
			'N/A' LegType,
			'N/A' BetGroupType,
			'N/A' BetSubType,
			'N/A' GroupingType,
			0 NumberOfLegs,
			0 LegNumber,
			0 ClassId,
			'IBT' ClassSource,
			0 EventId,
			'N/A' EventSource,
			CONVERT(varchar(255),NULL) SourceEvent, -- Placeholders from previous lottery transactions which do not have EventIds, only product and draw date
			CONVERT(datetime2(3),NULL) SourceEventDate,
			0 MarketId,
			'N/A' MarketSource,
			a.ChannelId,
			a.ChannelId ActionChannelId,
			a.UserId,
			a.UserSource,
			a.InstrumentId,
			a.InstrumentSource,
			a.NoteId,
			a.NoteSource,
			0 HeadlineFeatures,
			'N/A' InPlay,
			'N/A' ClickToCall,
			0 CashoutType,
			0 IsDoubleDown,
			0 IsDoubledDown,
			0 IsChaseTheAce,
			0 FeatureFlags,
			0 PromotionId,
			'N/A' PromotionSource,
			a.Amount * x.AmountFactor TransactedAmount,
			a.TransactionId,
			a.TransactionSource,
			a.TransactionId GroupId,
			a.TransactionSource GroupSource,
			NULL BetId,
			'N/A' BetSource,
			NULL LegId,
			'N/A' LegSource,
			NULL FreeBetId,
			NULL ExternalReference,
			'N/A' ExternalSource,
			CONVERT(int,MappingId) MappingId,
			'W' FunctionalArea,
			a.FromDate
	FROM	Atomic.Withdrawal a
			INNER JOIN Reference.TimeZone z ON z.FromDate <= a.MasterTimestamp AND z.ToDate > a.MasterTimestamp AND z.TimeZone='Analysis'
			INNER JOIN Reference.fn_Mapping_Cashflow('Withdrawal') x ON x.TransactionCode = a.TransactionCode
	WHERE	BatchKey = @BatchKey
			AND (a.MappingId = @MappingId OR @MappingId IS NULL)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Stage','Start',@RowsProcessed = @@ROWCOUNT

	SELECT	x.Id,
			x.IdSource,
			ISNULL(td.TransactionDetailKey,-1) TransactionDetailKey,
			ISNULL(b.BalanceTypeKey,-1) BalanceTypeKey,
			ISNULL(d.DayKey,-1) DayZoneKey,
			x.MasterTimestamp,
			ISNULL(tm.TimeKey,-1) MasterTimeKey,
			x.AnalysisTimestamp,
			ISNULL(ta.TimeKey,-1) AnalysisTimeKey,
			ISNULL(c.ContractKey,-1) ContractKey,
			ISNULL(a.AccountKey,-1) AccountKey,
			CASE 
				WHEN (x.TransactionTypeID = 'TR' and x.TransactionStatusID IN ('CO') and x.TransactionMethodID = 'MI' and x.TransactionDirection = 'DR') THEN COALESCE(sa1.AccountStatusKey, sa.AccountStatusKey,-1) 
				ELSE COALESCE(sa.AccountStatusKey,-1) 
			END AccountStatusKey,
			ISNULL(s.StructureKey,-1) StructureKey,
			ISNULL(w.WalletKey,-1) WalletKey,
			ISNULL(pt.PriceTypeKey,-1) PriceTypeKey,
			ISNULL(bt.BetTypeKey,-1) BetTypeKey,
			ISNULL(l.LegTypeKey,-1) LegTypeKey,
			ISNULL(cl.ClassKey,-1) ClassKey,
			COALESCE(e.EventKey,e1.EventKey,-99) EventKey,
			ISNULL(m.MarketKey,-1) MarketKey,
			ISNULL(ch.ChannelKey,0) ChannelKey,
			ISNULL(ac.ChannelKey,0) ActionChannelKey,
			ISNULL(u.UserKey,0) UserKey,
			ISNULL(i.InstrumentKey,0) InstrumentKey,
			ISNULL(n.NoteKey,0) NoteKey,
			ISNULL(ip.InPlayKey,-1) InPlayKey,
			ISNULL(p.PromotionKey,-1) PromotionKey,
			Amount,
			x.TransactionId,
			x.TransactionSource,
			x.GroupId,
			x.GroupSource,
			x.BetId,
			x.BetSource,
			x.LegId,
			x.LegSource,
			x.FreeBetId,
			x.ExternalReference,
			x.ExternalSource,
			x.MappingId,
			x.FunctionalArea,
			x.FromDate
	INTO	#Stage
	FROM	#Collect x
			LEFT JOIN [$(Dimensional)].Dimension.TransactionDetail td ON td.TransactionTypeID = x.TransactionTypeID AND td.TransactionStatusID = x.TransactionStatusID AND td.TransactionMethodID = x.TransactionMethodID AND td.TransactionDirection = x.TransactionDirection
			LEFT JOIN [$(Dimensional)].Dimension.BalanceType b ON b.BalanceTypeID = x.BalanceTypeID
			LEFT JOIN [$(Dimensional)].CC.[Day] d ON d.MasterDayText = x.MasterDayText AND d.FromDate <= x.MasterTimestamp AND d.ToDate > x.MasterTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.[Time] tm ON tm.TimeText = x.MasterTimeText
			LEFT JOIN [$(Dimensional)].Dimension.[Time] ta ON ta.TimeText = x.AnalysisTimeText
			LEFT JOIN [$(Dimensional)].Dimension.[Contract] c ON c.LegId = x.ContractLegId AND c.LegIdSource = x.ContractLegSource
			LEFT JOIN [$(Dimensional)].Dimension.Account a ON a.LedgerID = x.LedgerID AND a.LedgerSource = x.LedgerSource
			LEFT JOIN [$(Dimensional)].Dimension.AccountStatus sa ON sa.LedgerID = x.LedgerID AND sa.LedgerSource = x.LedgerSource AND sa.FromDate <= x.MasterTimestamp AND sa.ToDate > x.MasterTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.AccountStatus sa1 ON sa1.LedgerID = x.LedgerID AND sa1.LedgerSource = x.LedgerSource AND sa1.FromDate < x.MasterTimestamp AND sa1.ToDate >= x.MasterTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.Structure s ON s.LedgerId = x.LedgerId AND s.LedgerSource = x.LedgerSource AND s.HeadlineFeatures = x.HeadlineFeatures
			LEFT JOIN [$(Dimensional)].Dimension.Wallet w ON w.WalletId = x.WalletId
			LEFT JOIN [$(Dimensional)].Dimension.PriceType pt ON pt.PriceTypeId = x.PriceTypeId AND pt.Source = x.PriceTypeSource
			LEFT JOIN [$(Dimensional)].Dimension.BetType bt ON bt.BetType = x.BetType AND bt.LegType = x.LegType AND bt.BetGroupType = x.BetGroupType AND bt.BetSubType = x.BetSubType AND bt.GroupingType = x.GroupingType
			LEFT JOIN [$(Dimensional)].Dimension.LegType l ON l.NumberOfLegs = x.NumberOfLegs AND l.LegNumber = x.LegNumber
			LEFT JOIN [$(Dimensional)].Dimension.Class cl ON cl.ClassID = x.ClassID AND cl.Source = x.ClassSource AND cl.FromDate <= x.MasterTimestamp AND cl.ToDate > x.MasterTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.[Event] e ON e.EventID = x.EventID AND e.Source = x.EventSource
			LEFT JOIN [$(Dimensional)].Dimension.[Event] e1 ON e1.SourceEvent = x.SourceEvent AND e1.SourceEventDate = x.SourceEventDate
			LEFT JOIN [$(Dimensional)].Dimension.Market m ON m.MarketID = x.MarketID AND m.Source = x.MarketSource
			LEFT JOIN [$(Dimensional)].Dimension.Channel ch ON ch.ChannelId = x.ChannelId
			LEFT JOIN [$(Dimensional)].Dimension.Channel ac ON ac.ChannelId = x.ActionChannelId
			LEFT JOIN [$(Dimensional)].Dimension.[User] u ON u.UserID = x.UserID AND u.Source = x.UserSource
			LEFT JOIN [$(Dimensional)].Dimension.Instrument i ON i.InstrumentID = x.InstrumentID AND i.Source = x.InstrumentSource
			LEFT JOIN [$(Dimensional)].Dimension.Note n ON n.NoteId = x.NoteId AND n.NoteSource = x.NoteSource
			LEFT JOIN [$(Dimensional)].Dimension.InPlay	ip ON ip.InPlay = x.InPlay and ip.ClickToCall = x.ClickToCall and ip.CashoutType = x.CashoutType AND ip.IsDoubleDown = x.IsDoubleDown AND ip.IsDoubledDown = x.IsDoubledDown AND ip.IsChaseTheAce = x.IsChaseTheAce AND ip.FeatureFlags = x.FeatureFlags
			LEFT JOIN [$(Dimensional)].Dimension.Promotion p ON p.PromotionId = x.PromotionId AND p.Source = x.PromotionSource
	OPTION (RECOMPILE)--, IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Fact','Start',@RowsProcessed = @@ROWCOUNT

	SELECT  Id, IdSource, TransactionDetailKey, BalanceTypeKey, DayZoneKey, 
			MasterTimestamp, MasterTimeKey, AnalysisTimestamp, AnalysisTimeKey, ContractKey, AccountKey, AccountStatusKey, StructureKey, WalletKey,
			PriceTypeKey, BetTypeKey, LegTypeKey, ClassKey, EventKey, MarketKey, ChannelKey, ActionChannelKey, UserKey, InstrumentKey, NoteKey, InPlayKey, PromotionKey,
			Amount, TransactionId, TransactionSource, GroupId, GroupSource, BetId, BetSource, LegId, LegSource, FreeBetId, ExternalReference, ExternalSource,
			MappingId, FunctionalArea, FromDate
	INTO	#Fact
	FROM	[$(Dimensional)].Fact.Cashflow
	WHERE	CreatedBatchKey = @BatchKey
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT

	SET @FactsExist = CASE @RowCount WHEN 0 THEN 0 ELSE 1 END

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start',@RowsProcessed = @RowCount
	
	SELECT  *, CASE WHEN COUNT(*) OVER (PARTITION BY Id, IdSource, TransactionDetailKey, BalanceTypeKey, MasterTimestamp, TransactionId, TransactionSource) = 2 THEN 'U' WHEN src = 'S' THEN 'I' WHEN src = 'F' THEN 'D' ELSE 'X' END Upsert
	INTO	#Differences
	FROM	(	SELECT  Id, IdSource, TransactionDetailKey, BalanceTypeKey, DayZoneKey, 
						MasterTimestamp, MasterTimeKey, AnalysisTimestamp, AnalysisTimeKey, ContractKey, AccountKey, AccountStatusKey, StructureKey, WalletKey,
						PriceTypeKey, BetTypeKey, LegTypeKey, ClassKey, EventKey, MarketKey, ChannelKey, ActionChannelKey, UserKey, InstrumentKey, NoteKey, InPlayKey, PromotionKey,
						Amount, TransactionId, TransactionSource, GroupId, GroupSource, BetId, BetSource, LegId, LegSource, FreeBetId, ExternalReference, ExternalSource,
						MAX(MappingId) MappingId, FunctionalArea, MAX(FromDate) FromDate, MAX(src) src -- FromDate left out of difference check due to historically being datetime2(0) and now datetime2(3) - don't want to update every from date, only if record changing for other reasons
				FROM	(	SELECT *, 'S' src FROM #Stage
							UNION ALL
							SELECT *, 'F' src FROM #Fact
						) x
				GROUP BY Id, IdSource, TransactionDetailKey, BalanceTypeKey, DayZoneKey, 
						MasterTimestamp, MasterTimeKey, AnalysisTimestamp, AnalysisTimeKey, ContractKey, AccountKey, AccountStatusKey, StructureKey, WalletKey,
						PriceTypeKey, BetTypeKey, LegTypeKey, ClassKey, EventKey, MarketKey, ChannelKey, ActionChannelKey, UserKey, InstrumentKey, NoteKey, InPlayKey, PromotionKey,
						Amount, TransactionId, TransactionSource, GroupId, GroupSource, BetId, BetSource, LegId, LegSource, FreeBetId, ExternalReference, ExternalSource,
						/*MappingId, */FunctionalArea --, FromDate
				HAVING	COUNT(*) = 1
			) y
	WHERE	@FactsExist = 1
	OPTION (RECOMPILE)
	
	INSERT #Differences SELECT *, 'S' src, 'I' Upsert  FROM #Stage WHERE @FactsExist = 0

	IF @DryRun = 1 
		SELECT	TOP 10000 
				CASE WHEN Upsert = 'U' AND src = 'F' THEN 'U-' WHEN Upsert = 'U' AND src = 'S' THEN 'U+' ELSE Upsert END act, 
				* 
		FROM	#Differences 
		ORDER BY Id, IdSource, TransactionDetailKey, BalanceTypeKey, MasterTimestamp, LegTypeKey, src desc

	SELECT	* INTO #Prepare FROM #Differences WHERE	Upsert <> 'U' OR src <> 'F' -- Discard the 'before' version record of Update

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start',@RowsProcessed = @@ROWCOUNT

	IF @DryRun = 0 AND @FactsExist = 1
		BEGIN
			UPDATE	d
				SET	MasterTimeKey = p.MasterTimeKey,
					AnalysisTimestamp = p.AnalysisTimestamp,
					AnalysisTimeKey = p.AnalysisTimeKey,
					ContractKey = p.ContractKey,
					AccountKey = p.AccountKey,
					AccountStatusKey = p.AccountStatusKey,
					StructureKey = p.StructureKey,
					WalletKey = p.WalletKey,
					PriceTypeKey = p.PriceTypeKey,
					BetTypeKey = p.BetTypeKey,
					LegTypeKey = p.LegTypeKey,
					ClassKey = p.ClassKey,
					EventKey = p.EventKey,
					MarketKey = p.MarketKey,
					ChannelKey = p.ChannelKey,
					ActionChannelKey = p.ActionChannelKey,
					UserKey = p.UserKey,
					InstrumentKey = p.InstrumentKey,
					NoteKey = p.NoteKey,
					InPlayKey = p.InPlayKey,
					PromotionKey = p.PromotionKey,
					Amount = p.Amount,
					TransactionId = p.TransactionId,
					TransactionSource = p.TransactionSource,
					GroupId = p.GroupId,
					GroupSource = p.GroupSource,
					BetId = p.BetId,
					BetSource = p.BetSource,
					LegId = p.LegId,
					LegSource = p.LegSource,
					FreeBetId = p.FreeBetId,
					ExternalReference = p.ExternalReference,
					ExternalSource = p.ExternalSource,
					MappingId = p.MappingId,
					FunctionalArea = p.FunctionalArea,
					FromDate = p.FromDate,
					CreatedDate = SYSDATETIME(), 
					CreatedBy = @Me
			FROM	[$(Dimensional)].Fact.Cashflow d
					INNER JOIN #Prepare p ON p.Id = d.Id AND p.IdSource = d.IdSource AND p.TransactionDetailKey = d.TransactionDetailKey AND p.BalanceTypeKey = d.BalanceTypeKey 
												AND p.DayZoneKey = d.DayZoneKey AND p.MasterTimestamp = d.MasterTimestamp
												AND p.TransactionId = d.TransactionId AND p.TransactionSource = d.TransactionSource
			WHERE	p.Upsert = 'U'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'U'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start',@RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			INSERT	[$(Dimensional)].Fact.Cashflow
					(	Id, IdSource, TransactionDetailKey, BalanceTypeKey, DayZoneKey, 
						MasterTimestamp, MasterTimeKey, AnalysisTimestamp, AnalysisTimeKey, ContractKey, AccountKey, AccountStatusKey, StructureKey, WalletKey,
						PriceTypeKey, BetTypeKey, LegTypeKey, ClassKey, EventKey, MarketKey, ChannelKey, ActionChannelKey, UserKey, InstrumentKey, NoteKey, InPlayKey, PromotionKey,
						Amount, TransactionId, TransactionSource, GroupId, GroupSource, BetId, BetSource, LegId, LegSource, FreeBetId, ExternalReference, ExternalSource,
						MappingId, FunctionalArea, FromDate, CreatedBatchKey, CreatedDate, CreatedBy
					)
			SELECT	Id, IdSource, TransactionDetailKey, BalanceTypeKey,  DayZoneKey, 
					MasterTimestamp, MasterTimeKey, AnalysisTimestamp, AnalysisTimeKey, ContractKey, AccountKey, AccountStatusKey, StructureKey, WalletKey,
					PriceTypeKey, BetTypeKey, LegTypeKey, ClassKey, EventKey, MarketKey, ChannelKey, ActionChannelKey, UserKey, InstrumentKey, NoteKey, InPlayKey, PromotionKey,
					Amount, TransactionId, TransactionSource, GroupId, GroupSource, BetId, BetSource, LegId, LegSource, FreeBetId, ExternalReference, ExternalSource,
					MappingId, FunctionalArea, FromDate, @BatchKey CreatedBatchKey, SYSDATETIME() CreatedDate, @Me CreatedBy
			FROM	#Prepare
			WHERE	Upsert = 'I'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'I'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Delete','Start',@RowsProcessed = @RowCount

	IF @DryRun = 0 AND @FactsExist = 1
		BEGIN
			DELETE	d
			FROM	[$(Dimensional)].Fact.Cashflow d
					INNER JOIN #Prepare p ON p.Id = d.Id AND p.IdSource = d.IdSource AND p.TransactionDetailKey = d.TransactionDetailKey AND p.BalanceTypeKey = d.BalanceTypeKey 
												AND p.DayZoneKey = d.DayZoneKey AND p.MasterTimestamp = d.MasterTimestamp
												AND p.TransactionId = d.TransactionId AND p.TransactionSource = d.TransactionSource
			WHERE	p.Upsert = 'D'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'D'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success',@RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
