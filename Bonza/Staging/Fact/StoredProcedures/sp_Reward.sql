﻿CREATE PROC	 [Fact].[sp_Reward]
(
	@BatchKey int,
	@MappingId smallint = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY
	
	EXEC [Control].[Sp_Log] @BatchKey,@Me,'Accruals','Start'

	SELECT	b.LegId,
			b.LegIdSource LegSource,
			b.MasterTransactionTimestamp,
			DATEADD(MINUTE, tz.OffsetMinutes, b.MasterTransactionTimestamp) AnalysisTransactionTimestamp, 
			CONVERT(CHAR(11), CONVERT(DATE, MasterTransactionTimestamp)) MasterDayText,
			CONVERT(varchar(5),CONVERT(time,MasterTransactionTimestamp)) MasterTimeText,
			CONVERT(varchar(5), CONVERT(time, DATEADD(MINUTE, OffsetMinutes, MasterTransactionTimestamp))) AnalysisTimeText,
			b.LedgerId,
			b.LedgerSource,
			b.BetType,
			b.LegType,
			b.BetGroupType,
			b.BetSubType,
			b.GroupingType,
			b.NumberOfLegs,
			b.LegNumber,
			b.PriceTypeId,
			b.PriceTypeSource,
			CONVERT(varchar(50),'Rewards.PointsAccrued') TransactionCode,
			b.RewardBetTypeId,
			CONVERT(char(32),HASHBYTES('MD5','N/A'),2) RewardReasonId,
			'+' DirectionCode,
			b.Channel ChannelId,
			b.PromotionId,
			b.PromotionIdSource PromotionSource,
			b.MarketID,
			b.MarketIDSource MarketSource,
			b.EventID,
			b.EventIdSource EventSource,
			b.ClassID,
			b.ClassIdSource ClassSource,
			b.UserID,
			b.UserIDSource UserSource,
			CASE b.InPlay WHEN 'Y' THEN 1 ELSE 0 END HeadlineFeatures,
			CONVERT(varchar(20),b.InPlay) InPlay,
			CONVERT(varchar(20),b.ClickToCall) ClickToCall,
			b.CashoutType,
			b.IsDoubleDown,
			b.IsDoubledDown,
			b.IsChaseTheAce,
			b.FeatureFlags,
			b.LegStakeTrued Points,
			0 VelocityPoints,
			0 BonusCredit,
			b.FromDate,
			b.MappingId
	INTO	#Rewards
	FROM	Atomic.Bet_New b
			INNER JOIN Reference.TimeZone tz ON tz.FromDate <= b.MasterTransactionTimestamp AND tz.ToDate > b.MasterTransactionTimestamp AND tz.TimeZone='Analysis'
	WHERE	Action = 'Accrual'
			AND b.PositionNumber = 1 AND b.GroupingNumber = 1	-- As the Transaction fact is staged at a bet leg grain, just fix the selection to the first position and group for each leg and retrieve leg level stakes and payments (seeabove)
			AND (b.BetType <> 'Exotic' OR b.BetId = b.LegId)	-- Transactions only include one line per exotics, even if the exotic is technically multi-leg - e.g. Quadrella - so only take the first leg of exotics and retrieve bet level stakes and payments (above)
			AND b.BatchKey = @BatchKey
			AND (b.MappingId = @MappingId OR @MappingId IS NULL)
	OPTION (RECOMPILE)

	EXEC [Control].[Sp_Log] @BatchKey,@Me,'Transactions','Start', @RowsProcessed = @@ROWCOUNT

	INSERT	#Rewards
	SELECT	b.TransactionId LegId,
			b.TransactionSource LegSource,
			b.MasterTransactionTimestamp,
			DATEADD(MINUTE, tz.OffsetMinutes, b.MasterTransactionTimestamp) AnalysisTransactionTimestamp, 
			CONVERT(CHAR(11), CONVERT(DATE, MasterTransactionTimestamp)) MasterDayText,
			CONVERT(varchar(5),CONVERT(time,MasterTransactionTimestamp)) MasterTimeText,
			CONVERT(varchar(5), CONVERT(time, DATEADD(MINUTE, OffsetMinutes, MasterTransactionTimestamp))) AnalysisTimeText,
			b.LedgerId,
			b.LedgerSource,
			'N/A' BetType,
			'N/A' LegType,
			'N/A' BetGroupType,
			'N/A' BetSubType,
			'N/A' GroupingType,
			0 NumberOfLegs,
			0 LegNumber,
			0 PriceTypeId,
			'N/A' PriceTypeSource,
			b.TransactionCode,
			-1 RewardBetTypeId,
			b.NotesId RewardReasonId,
			CASE WHEN b.Points < 0 THEN '-' ELSE '+' END DirectionCode,
			0 ChannelId,
			b.PromotionId,
			b.PromotionSource,
			0 MarketID,
			'N/A' MarketSource,
			0 EventID,
			'N/A' EventSource,
			0 ClassID,
			'IBT' ClassSource,
			b.UserID,
			b.UserSource,
			0 HeadlineFeatures,
			'N/A' InPlay,
			'N/A' ClickToCall,
			0 CashoutType,
			0 IsDoubleDown,
			0 IsDoubledDown,
			0 IsChaseTheAce,
			0 FeatureFlags,
			b.Points,
			b.VelocityPoints,
			b.Bonus BonusCredit,
			b.FromDate,
			b.MappingId
	FROM	Atomic.[Transaction] b
			INNER JOIN Reference.TimeZone tz ON tz.FromDate <= b.MasterTransactionTimestamp AND tz.ToDate > b.MasterTransactionTimestamp AND tz.TimeZone='Analysis'
	WHERE	TransactionCode LIKE 'Rewards.%'
			AND b.BatchKey = @BatchKey
			AND (b.MappingId = @MappingId OR @MappingId IS NULL)
	OPTION (RECOMPILE)

	EXEC [Control].[Sp_Log] @BatchKey,@Me,'Insert','Start', @RowsProcessed = @@ROWCOUNT

	INSERT	[$(Dimensional)].Fact.[Reward] 
			(	ContractKey, MasterTransactionTimestamp, AnalysisTransactionTimestamp, DayKey, MasterTimeKey, AnalysisTimeKey, 
				AccountKey, AccountStatusKey, StructureKey, 
				BetTypeKey, LegTypeKey, PriceTypeKey, RewardDetailKey, ChannelKey, InPlayKey, 
				PromotionKey, MarketKey, EventKey, ClassKey,
				Points, VelocityPoints, BonusCredit,
				FromDate, MappingId, CreatedDate, CreatedBatchKey, CreatedBy
			)
	SELECT  ISNULL(c.ContractKey, -1) ContractKey,
			x.MasterTransactionTimestamp,
			x.AnalysisTransactionTimestamp,
			ISNULL(d.DayKey, -1) DayKey,
			ISNULL(tm.TimeKey, -1) MasterTimeKey,
			ISNULL(ta.TimeKey, -1) AnalysisTimeKey,
			ISNULL(a.AccountKey, -1) AccountKey,
			ISNULL(a1.AccountStatusKey, -1) AccountStatusKey,
			ISNULL(s.StructureKey, -1) StructureKey,
			ISNULL(bt.BetTypeKey, -1) BetTypeKey,
			ISNULL(lt.LegTypeKey, -1) LegTypeKey,
			ISNULL(pt.PriceTypeKey, -1) PriceTypeKey,
			ISNULL(r.RewardDetailKey, -1) RewardDetailKey,
			ISNULL(ch.ChannelKey, -1) ChannelKey,
			ISNULL(ip.InPlayKey, -1) InPlayKey,
			ISNULL(p.PromotionKey, -1) PromotionKey,
			ISNULL(m.MarketKey, -1) MarketKey,
			ISNULL(e.EventKey, -1) EventKey,
			ISNULL(cl.ClassKey, -1) ClassKey,
			x.Points,
			x.VelocityPoints,
			x.BonusCredit,
			x.FromDate,
			x.MappingId,
			SYSDATETIME() AS CreatedDate,
			@BatchKey AS CreatedBatchKey,
			@Me AS CreatedBy
	FROM	#Rewards x
			LEFT JOIN [$(Dimensional)].Dimension.[Contract] c		ON c.LegId = x.LegId AND c.LegIDSource = x.LegSource
			LEFT JOIN [$(Dimensional)].CC.[Day] d					ON d.MasterDayText = x.MasterDayText AND d.FromDate <= x.MasterTransactionTimestamp AND d.ToDate > x.MasterTransactionTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.[Time] tm			ON tm.TimeText = x.MasterTimeText
			LEFT JOIN [$(Dimensional)].Dimension.[Time] ta			ON ta. TimeText = x.AnalysisTimeText
			LEFT JOIN [$(Dimensional)].Dimension.Account a			ON a.LedgerID = x.LedgerID AND a.LedgerSource = x.LedgerSource
			LEFT JOIN [$(Dimensional)].Dimension.AccountStatus a1	ON a1.LedgerID = x.LedgerID AND a1.LedgerSource= x.LedgerSource AND a1.FROMDATE <= x.MasterTransactionTimestamp AND a1.ToDate > x.MasterTransactionTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.Structure s		ON s.LedgerId = x.LedgerId AND s.LedgerSource = x.LedgerSource AND s.HeadlineFeatures = x.HeadlineFeatures
			LEFT JOIN [$(Dimensional)].Dimension.BetType bt			ON bt.BetType = x.BetType AND bt.LegType = x.LegType AND bt.BetGroupType = x.BetGroupType AND bt.BetSubType = x.BetSubType AND bt.GroupingType = x.GroupingType
			LEFT JOIN [$(Dimensional)].Dimension.LegType lt			ON lt.NumberOfLegs = x.NumberOfLegs AND lt.LegNumber = x.LegNumber
			LEFT JOIN [$(Dimensional)].Dimension.PriceType pt		ON pt.PriceTypeId = x.PriceTypeId AND pt.Source = x.PriceTypeSource 
			LEFT JOIN [$(Dimensional)].Dimension.RewardDetail r		ON r.TransactionCode = x.TransactionCode AND r.RewardBetTypeId = x.RewardBetTypeId AND r.RewardReasonId = x.RewardReasonId AND r.DirectionCode = x.DirectionCode
			LEFT JOIN [$(Dimensional)].Dimension.Channel ch			ON ch.ChannelId = x.ChannelId
			LEFT JOIN [$(Dimensional)].Dimension.Promotion p		ON p.PromotionId = x.PromotionId AND p.Source = x.PromotionSource 
			LEFT JOIN [$(Dimensional)].Dimension.Market m			ON m.MarketID = x.MarketID AND m.Source = x.MarketSource
			LEFT JOIN [$(Dimensional)].Dimension.[Event] e			ON e.EventID = x.EventID AND e.Source = x.EventSource
			LEFT JOIN [$(Dimensional)].Dimension.Class cl			ON cl.ClassID = x.ClassID AND cl.Source = x.ClassSource AND cl.Fromdate <= x.MasterTransactionTimestamp AND cl.ToDate > x.MasterTransactionTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.[User] u			ON u.UserID = x.UserID AND u.Source = x.UserSource
			LEFT JOIN [$(Dimensional)].Dimension.InPlay ip			ON ip.InPlay = x.InPlay and ip.ClickToCall = x.ClickToCall and ip.CashoutType = x.CashoutType AND ip.IsDoubleDown = x.IsDoubleDown AND ip.IsDoubledDown = x.IsDoubledDown AND ip.IsChaseTheAce = x.IsChaseTheAce AND ip.FeatureFlags = x.FeatureFlags
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

