CREATE PROC [Fact].[sp_KPI]
(
	@BatchKey		int,
	@DryRun			bit = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @Trued char(1) = 'Y'

	DECLARE @RowCount int

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Legacy','Start'

	SELECT	LegId,
			LegIdSource LegSource,
			LedgerId,
			CASE WHEN LedgerId = -1 THEN 'UNK' ELSE LedgerSource END LedgerSource,
			MasterTransactionTimestamp MasterTimestamp,
			CONVERT(datetime2(3),NULL) AnalysisTimestamp, 
			MasterDayText,
			CONVERT(char(5),CONVERT(time,MasterTransactionTimestamp)) MasterTimeText,
			CONVERT(char(5), NULL) AnalysisTimeText,
			BetTypeName BetType,
			LegBetTypeName LegType,
			BetGroupType,
			BetSubTypeName BetSubType,
			GroupingType,
			LegNumber,
			NumberOfLegs,
			--PriceTypeId,
			--PriceTypeSource,
			Channel ChannelId,
			ActionChannel ActionChannelId,
			--PromotionId,
			--PromotionIdSource PromotionSource,
			CampaignId,
			EventId MarketID,
			EventIdSource MarketSource,
			EventID,
			EventIdSource EventSource,
			CONVERT(varchar(255),NULL) SourceEvent, -- Placeholders for subsequent lottery transactions which do not have EventIds, only product and draw date
			CONVERT(datetime2(3),NULL) SourceEventDate,
			ClassID,
			ClassIdSource ClassSource,
			UserID,
			UserIDSource UserSource,
			InstrumentID,
			InstrumentIDSource InstrumentSource,
			CASE InPlay WHEN 'Y' THEN 1 ELSE 0 END HeadlineFeatures,
			InPlay,
			ClickToCall,
			CashoutType,
			IsDoubleDown,
			IsDoubledDown,
			IsChaseTheAce,
			FeatureFlags,
			AccountOpened AccountsOpened,
			FirstDeposit FirstTimeDepositors,
			DepositsRequested,
			DepositsCompleted,
			NULLIF(DepositsRequestedCount, 0) DepositsRequestedCount,
			NULLIF(DepositsCompletedCount, 0) DepositsCompletedCount,
			WithdrawalsRequested,
			WithdrawalsCompleted,
			NULLIF(WithdrawalsRequestedCount, 0) WithdrawalsRequestedCount,
			NULLIF(WithdrawalsCompletedCount, 0) WithdrawalsCompletedCount,
			FirstBet FirstTimeBettors,
			BettorsPlaced,
			BonusBettorsPlaced,
			BetsPlaced,
			BonusBetsPlaced,
			ContractsPlaced,
			BonusContractsPlaced,
			PlayDaysPlaced,
			BonusPlayDaysPlaced,
			PlayFiscalWeeksPlaced,
			BonusPlayFiscalWeeksPlaced,
			PlayCalenderWeeksPlaced PlayWeeksPlaced,
			BonusPlayCalenderWeeksPlaced BonusPlayWeeksPlaced,
			PlayFiscalMonthsPlaced,
			BonusPlayFiscalMonthsPlaced,
			PlayCalenderMonthsPlaced PlayMonthsPlaced,
			BonusPlayCalenderMonthsPlaced BonusPlayMonthsPlaced,
			Stakes TurnoverPlaced,
			BonusStakes BonusTurnoverPlaced,
			Fees,
			BettorsResulted BettorsSettled,
			BonusBettorsResulted BonusBettorsSettled,
			BetsResulted BetsSettled,
			BonusBetsResulted BonusBetsSettled,
			ContractsResulted ContractsSettled,
			BonusContractsResulted BonusContractsSettled,
			PlayDaysResulted PlayDaysSettled,
			BonusPlayDaysResulted BonusPlayDaysSettled,
			PlayFiscalWeeksResulted PlayFiscalWeeksSettled,
			BonusPlayFiscalWeeksResulted BonusPlayFiscalWeeksSettled,
			PlayCalenderWeeksResulted PlayWeeksSettled,
			BonusPlayCalenderWeeksResulted BonusPlayWeeksSettled,
			PlayFiscalMonthsResulted PlayFiscalMonthsSettled,
			BonusPlayFiscalMonthsResulted BonusPlayFiscalMonthsSettled,
			PlayCalenderMonthsResulted PlayMonthsSettled,
			BonusPlayCalenderMonthsResulted BonusPlayMonthsSettled,
			Turnover TurnoverSettled,
			BonusTurnover BonusTurnoverSettled,
			Winnings Payout,
			BonusWinnings BonusPayout,
			GrossWin,
			GrossWinAdjustment GrossWinResettledAdjustment,
			-BonusWinnings BonusGrossWin,
			BettorsCashedOut,
			BetsCashedOut,
			Cashout,
			CashoutDifferential,
			Promotions,
			Transfers,
			Adjustments,
			NetRevenue,
			NetRevenueAdjustment,
			CONVERT(int,NULL) FirstTimeAccruers,
			CONVERT(int,NULL) RewardAccruals,
			CONVERT(int,NULL) RewardAccruers,
			CONVERT(money,0) RewardAccrualPoints,
			CONVERT(int,NULL) FirstTimeBonusRedeemers,
			CONVERT(int,NULL) RewardBonusRedemptions,
			CONVERT(int,NULL) RewardBonusRedeemers,
			CONVERT(money,0) RewardBonusRedeemedPoints,
			CONVERT(money,0) RewardBonusRedeemed,
			CONVERT(int,NULL) FirstTimeVelocityRedeemers,
			CONVERT(int,NULL) RewardVelocityRedemptions,
			CONVERT(int,NULL) RewardVelocityRedeemers,
			CONVERT(money,0) RewardVelocityRedeemedPoints,
			CONVERT(money,0) RewardVelocityRedeemed,
			CONVERT(money,0) RewardVelocityCost,
			CONVERT(int,NULL) FirstTimeRedeemers,
			CONVERT(int,NULL) RewardRedeemers,
			CONVERT(money,0) RewardAdjustmentPoints,
			Betback BetbackGrossWin,
			CreatedDate FromDate,
			CONVERT(int,0) MappingId
	INTO	#Collect
	FROM	Stage.KPI
	WHERE	BatchKey = @BatchKey
			AND NOT(Cashout = 0 and CashoutDifferential <> 0) -- Cashout differentials after the initial cashout are now hanled directly from Atomic below rather than via staging above. However, need to exclude any historic records which were included in staging prior to the change

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Lottery','Start',@RowsProcessed = @@ROWCOUNT

	INSERT	#Collect
	SELECT	a.TicketId LegId, a.TicketSource LegIdSource, a.LedgerId, a.LedgerSource, a.MasterTransactionTimestamp MasterTimestamp, DATEADD(minute, + ISNULL(y.OffsetMinutes,0), a.MasterTransactionTimestamp) AnalysisTimestamp,			
			CONVERT(varchar(11),CONVERT(DATE, a.MasterTransactionTimestamp)) MasterDayText,
			CONVERT(char(5),CONVERT(time, a.MasterTransactionTimestamp)) MasterTimeText,
			CONVERT(char(5),CONVERT(time,DATEADD(minute, + ISNULL(y.OffsetMinutes,0), a.MasterTransactionTimestamp))) AnalysisTimeText,
			'Single' BetType, 'Win' LegType, 'Straight' BetGroupType, 'Single' BetSubType, 'Standard' GroupingType, 1 LegNumber, 1 NumberOfLegs, /*PriceTypeId, PriceTypeSource, */ 
			0 ChannelId, 0 ActionChannelId, /*PromotionId, PromotionSource, */ 0 CampaignId, 
			0 MarketID, 'N/A' MarketSource, a.TicketId EventID, 'LTE' EventSource, 
			a.Product SourceEvent, a.DrawDate SourceEventDate, -- Provided because by default Lottery has no EventId - have to match on product and draw date (unless draw date was null and one-off dummy eventid has been created using TicketId)
			-10 ClassId, 'LOT' ClassIdSource, 'N/A' UserID, 'N/A' UserIDSource, 0 InstrumentID, 'N/A' InstrumentIDSource, 
			2 HeadlineFeatures, 'N/A' InPlay, 'N/A' ClickToCall, 0 CashoutType, 0 IsDoubleDown, 0 IsDoubledDown, 0 IsChaseTheAce, 0 FeatureFlags,
			NULL AccountsOpened, NULL FirstTimeDepositors, 0 DepositsRequested, 0 DepositsCompleted, NULL DepositsRequestedCount, NULL DepositsCompletedCount, 
			0 WithdrawalsRequested, 0 WithdrawalsCompleted, NULL WithdrawalsRequestedCount, NULL WithdrawalsCompletedCount, 
			NULL FirstTimeBettors, 
			NULLIF(a.LedgerId * x.Placed,0) BettorsPlaced, NULL BonusBettorsPlaced, NULLIF(a.TicketId * x.Placed,0) BetsPlaced, NULL BonusBetsPlaced, NULLIF(a.TicketId * x.Placed,0) ContractsPlaced, NULL BonusContractsPlaced,
			NULLIF((CONVERT(bigint,a.LedgerId)*100000000 + d.DayKey) * x.Placed,0) PlayDaysPlaced, NULL BonusPlayDaysPlaced,
			NULLIF((CONVERT(bigint,LedgerId)*1000000 + d.FiscalWeekKey) * x.Placed,0) PlayFiscalWeeksPlaced, NULL BonusPlayFiscalWeeksPlaced,
			NULLIF((CONVERT(bigint,LedgerId)*1000000 + d.WeekKey) * x.Placed,0) PlayWeeksPlaced, NULL BonusPlayWeeksPlaced,
			NULLIF((CONVERT(bigint,LedgerId)*1000000 + d.FiscalMonthKey) * x.Placed,0) PlayFiscalMonthsPlaced, NULL BonusPlayFiscalMonthsPlaced,
			NULLIF((CONVERT(bigint,LedgerId)*1000000 + d.MonthKey) * x.Placed,0) PlayMonthsPlaced, NULL BonusPlayMonthsPlaced,
			- a.StakeDelta * x.Placed TurnoverPlaced, 0 BonusTurnoverPlaced, 0 Fees, 
			NULLIF(a.LedgerId * x.Settled,0) BettorsSettled, NULL BonusBettorsSettled, NULLIF(a.TicketId * x.Settled,0) BetsSettled, NULL BonusBetsSettled, NULLIF(a.TicketId * x.Settled,0) ContractsSettled, NULL BonusContractsSettled,
			NULLIF((CONVERT(bigint,LedgerId)*100000000 + d.DayKey) * x.Settled,0) PlayDaysSettled, NULL BonusPlayDaysSettled,
			NULLIF((CONVERT(bigint,LedgerId)*1000000 + d.FiscalWeekKey) * x.Settled,0) PlayFiscalWeeksSettled, NULL BonusPlayFiscalWeeksSettled,
			NULLIF((CONVERT(bigint,LedgerId)*1000000 + d.WeekKey) * x.Settled,0) PlayWeeksSettled, NULL BonusPlayWeeksSettled,
			NULLIF((CONVERT(bigint,LedgerId)*1000000 + d.FiscalMonthKey) * x.Settled,0) PlayFiscalMonthsSettled, NULL BonusPlayFiscalMonthsSettled,
			NULLIF((CONVERT(bigint,LedgerId)*1000000 + d.MonthKey) * x.Settled,0) PlayMonthsSettled, NULL BonusPlayMonthsSettled,
			- a.StakeDelta * x.Settled  TurnoverSettled, 0 BonusTurnoverSettled, a.PayoutDelta * x.Settled Payout, 0 BonusPayout, (- a.StakeDelta - a.PayoutDelta) * x.Settled GrossWin, 0 GrossWinResettledAdjustment, 0 BonusGrossWin,
			NULL BettorsCashedOut, NULL BetsCashedOut, 0 Cashout, 0 CashoutDifferential,
			0 Promotions, 0 Transfers, 0 Adjustments, (- a.StakeDelta - a.PayoutDelta) * x.Settled NetRevenue, 0 NetRevenueAdjustment,
			NULL FirstTimeAccruers, NULL RewardAccruals, NULL RewardAccruers, 0 RewardAccrualPoints,
			NULL FirstTimeBonusRedeemers, NULL RewardBonusRedemptions, NULL RewardBonusRedeemers, 0 RewardBonusRedeemedPoints, 0 RewardBonusRedeemed, 
			NULL FirstTimeVelocityRedeemers, NULL RewardVelocityRedemptions, NULL RewardVelocityRedeemers, 0 RewardVelocityRedeemedPoints, 0 RewardVelocityRedeemed, 0 RewardVelocityCost,
			NULL FirstTimeRedeemers, NULL RewardRedeemers, 0 RewardAdjustmentPoints,
			0 BetbackGrossWin, a.FromDate, a.MappingId
	FROM	Atomic.Lottery a 
			INNER JOIN	(	SELECT	'Lottery.Place' TransactionCode, 1 Placed, 0 Settled UNION ALL
							SELECT	'Lottery.Cancel' TransactionCode, 1 Placed, 0 Settled UNION ALL
							SELECT	'Lottery.Win' TransactionCode, 0 Placed, 1 Settled UNION ALL
							SELECT	'Lottery.Loss' TransactionCode, 0 Placed, 1 Settled
						) x ON x.TransactionCode = a.TransactionCode
			LEFT JOIN Reference.TimeZone y ON y.Fromdate <= a.MasterTransactionTimestamp AND y.ToDate > a.MasterTransactionTimestamp AND y.TimeZone = 'Analysis'
			LEFT JOIN [$(Dimensional)].Dimension.DayZone z ON z.MasterDayText = CONVERT(varchar(11),CONVERT(DATE, a.MasterTransactionTimestamp)) AND z.FromDate <= a.MasterTransactionTimestamp AND z.ToDate > a.MasterTransactionTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.Day d ON d.DayKey = z.MasterDayKey
	WHERE	BatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'CashoutDifferential','Start',@RowsProcessed = @@ROWCOUNT

	SET @Trued = CASE Control.ParameterValue('Bet','TruedEqualShare') WHEN 'No' THEN 'N' ELSE 'Y' END

	INSERT	#Collect
	SELECT	LegId, LegIdSource, LedgerId, LedgerSource, MasterTransactionTimestamp MasterTimestamp, DATEADD(minute, + ISNULL(y.OffsetMinutes,0), a.MasterTransactionTimestamp) AnalysisTimestamp,			
			CONVERT(varchar(11),CONVERT(DATE, a.MasterTransactionTimestamp)) MasterDayText,
			CONVERT(char(5),CONVERT(time,MasterTransactionTimestamp)) MasterTimeText,
			CONVERT(char(5),CONVERT(time,DATEADD(minute, + ISNULL(y.OffsetMinutes,0), a.MasterTransactionTimestamp))) AnalysisTimeText,
			BetType, LegType, BetGroupType, BetSubType, GroupingType, LegNumber, NumberOfLegs, /*PriceTypeId, PriceTypeSource, */ Channel ChannelId, ActionChannel ActionChannelId, /*PromotionId, PromotionSource, */ CampaignId, 
			MarketID, MarketIdSource MarketSource, EventID, EventIDSource EventSource, CONVERT(varchar(255),NULL) SourceEvent, CONVERT(datetime2(3),NULL) SourceEventDate,
			ClassId, ClassIdSource, UserID, UserIDSource, 0 InstrumentID, 'N/A' InstrumentIDSource, 
			CASE InPlay WHEN 'Y' THEN 1 ELSE 0 END HeadlineFeatures, InPlay, ClickToCall, CashoutType, IsDoubleDown, IsDoubledDown, IsChaseTheAce, FeatureFlags,
			NULL AccountsOpened, NULL FirstTimeDepositors, 0 DepositsRequested, 0 DepositsCompleted, NULL DepositsRequestedCount, NULL DepositsCompletedCount, 
			0 WithdrawalsRequested, 0 WithdrawalsCompleted, NULL WithdrawalsRequestedCount, NULL WithdrawalsCompletedCount, 
			NULL FirstTimeBettors, NULL BettorsPlaced, NULL BonusBettorsPlaced, NULL BetsPlaced, NULL BonusBetsPlaced, NULL ContractsPlaced, NULL BonusContractsPlaced, 
			NULL PlayDaysPlaced, NULL BonusPlayDaysPlaced, NULL PlayFiscalWeeksPlaced, NULL BonusPlayFiscalWeeksPlaced, NULL PlayWeeksPlaced, NULL BonusPlayWeeksPlaced, 
			NULL PlayFiscalMonthsPlaced, NULL BonusPlayFiscalMonthsPlaced, NULL PlayMonthsPlaced, NULL BonusPlayrMonthsPlaced, 
			0 TurnoverPlaced, 0 BonusTurnoverPlaced, 0 Fees, 
			NULL BettorsSettled, NULL BonusBettorsSettled, NULL BetsSettled, NULL BonusBetsSettled, NULL ContractsSettled, NULL BonusContractsSettled, 
			NULL PlayDaysSettled, NULL BonusPlayDaysSettled, NULL PlayFiscalWeeksSettled, NULL BonusPlayFiscalWeeksSettled, NULL PlayWeeksSettled, NULL BonusPlayWeeksSettled,
			NULL PlayFiscalMonthsSettled, NULL BonusPlayFiscalMonthsSettled, NULL PlayMonthsSettled, NULL BonusPlayMonthsSettled,
			0 TurnoverSettled, 0 BonusTurnoverSettled, 0 Payout, 0 BonusPayout, 0 GrossWin, 0 GrossWinResettledAdjustment, 0 BonusGrossWin,
			NULL BettorsCashedOut, NULL BetsCashedOut, 0 Cashout, CASE WHEN a.BetType = 'Exotic' THEN a.BetPayout WHEN @Trued = 'Y' THEN a.LegPayoutTrued ELSE a.LegPayout END CashoutDifferential,
			0 Promotions, 0 Transfers, 0 Adjustments, 0 NetRevenue, 0 NetRevenueAdjustment,
			NULL FirstTimeAccruers, NULL RewardAccruals, NULL RewardAccruers, 0 RewardAccrualPoints,
			NULL FirstTimeBonusRedeemers, NULL RewardBonusRedemptions, NULL RewardBonusRedeemers, 0 RewardBonusRedeemedPoints, 0 RewardBonusRedeemed, 
			NULL FirstTimeVelocityRedeemers, NULL RewardVelocityRedemptions, NULL RewardVelocityRedeemers, 0 RewardVelocityRedeemedPoints, 0 RewardVelocityRedeemed, 0 RewardVelocityCost,
			NULL FirstTimeRedeemers, NULL RewardRedeemers, 0 RewardAdjustmentPoints,
			0 BetbackGrossWin, a.FromDate, MappingId
	FROM	Atomic.Bet_New a 
			-- This join should not be!! Required because KPI staging table does not keep the same correct dayzone natural keys as transaction
			LEFT JOIN Reference.TimeZone y ON y.Fromdate <= a.MasterTransactionTimestamp AND y.ToDate > a.MasterTransactionTimestamp AND y.TimeZone = 'Analysis'
	WHERE	BatchKey = @BatchKey
			AND Action = 'CashoutDif'
			AND PositionNumber = 1 AND GroupingNumber = 1	-- As the Transaction fact is staged at a bet leg grain, just fix the selection to the first position and group for each leg and retrieve leg level stakes and payments (seeabove)
			AND (BetType <> 'Exotic' OR BetId = LegId)	-- Transactions only include one line per exotics, even if the exotic is technically multi-leg - e.g. Quadrella - so only take the first leg of exotics and retrieve bet level stakes and payments (above)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Stage','Start',@RowsProcessed = @@ROWCOUNT

	SELECT	c.ContractKey, 
			a.AccountKey, 
			x.MasterTimestamp, 
			CONVERT(datetime2(3),NULL)			AnalysisTimestamp,
			ISNULL(MAX(d.DayKey),-1)			DayKey,
			ISNULL(MAX(tm.TimeKey),-1)			MasterTimeKey,
			ISNULL(MAX(ta.TimeKey),-1)			AnalysisTimeKey,
			ISNULL(MAX(sa.AccountStatusKey),-1) AccountStatusKey,
			ISNULL(MAX(s.StructureKey),-1)		StructureKey,
			ISNULL(MAX(bt.BetTypeKey),-1)		BetTypeKey,
			ISNULL(MAX(l.LegTypeKey),-1)		LegTypeKey,
			ISNULL(MAX(ch.ChannelKey),-1)		ChannelKey,
			ISNULL(MAX(ac.ChannelKey),-1)		ActionChannelKey,
			ISNULL(MAX(cp.CampaignKey),-1)		CampaignKey,
			ISNULL(MAX(m.MarketKey),-1)			MarketKey,
			ISNULL(MAX(ISNULL(e.EventKey,e1.EventKey)),-1) EventKey,
			ISNULL(MAX(cl.ClassKey),-1)			ClassKey,
			ISNULL(MAX(u.UserKey),-1)			UserKey,
			ISNULL(MAX(i.InstrumentKey),-1)		InstrumentKey,
			ISNULL(MAX(ip.InPlayKey),-1)		InPlayKey,
			MAX(x.AccountsOpened)				AccountsOpened,
			MAX(x.FirstTimeDepositors)			FirstTimeDepositors,
			SUM(x.DepositsRequested)			DepositsRequested,
			SUM(x.DepositsCompleted)			DepositsCompleted,
			MAX(x.DepositsRequestedCount)		DepositsRequestedCount,
			MAX(x.DepositsCompletedCount)		DepositsCompletedCount,
			SUM(x.WithdrawalsRequested)			WithdrawalsRequested,
			SUM(x.WithdrawalsCompleted)			WithdrawalsCompleted,
			MAX(x.WithdrawalsRequestedCount)	WithdrawalsRequestedCount,
			MAX(x.WithdrawalsCompletedCount)	WithdrawalsCompletedCount,
			MAX(x.FirstTimeBettors)				FirstTimeBettors,
			MAX(x.BettorsPlaced)				BettorsPlaced,
			MAX(x.BonusBettorsPlaced)			BonusBettorsPlaced,
			MAX(x.BetsPlaced)					BetsPlaced,
			MAX(x.BonusBetsPlaced)				BonusBetsPlaced,
			MAX(x.ContractsPlaced)				ContractsPlaced,
			MAX(x.BonusContractsPlaced)			BonusContractsPlaced,
			MAX(x.PlayDaysPlaced)				PlayDaysPlaced,
			MAX(x.BonusPlayDaysPlaced)			BonusPlayDaysPlaced,
			MAX(x.PlayFiscalWeeksPlaced)		PlayFiscalWeeksPlaced,
			MAX(x.BonusPlayFiscalWeeksPlaced)	BonusPlayFiscalWeeksPlaced,
			MAX(x.PlayWeeksPlaced)				PlayWeeksPlaced,
			MAX(x.BonusPlayWeeksPlaced)			BonusPlayWeeksPlaced,
			MAX(x.PlayFiscalMonthsPlaced)		PlayFiscalMonthsPlaced,
			MAX(x.BonusPlayFiscalMonthsPlaced)	BonusPlayFiscalMonthsPlaced,
			MAX(x.PlayMonthsPlaced)				PlayMonthsPlaced,
			MAX(x.BonusPlayMonthsPlaced)		BonusPlayMonthsPlaced,
			SUM(x.TurnoverPlaced)				TurnoverPlaced,
			SUM(x.BonusTurnoverPlaced)			BonusTurnoverPlaced,
			SUM(x.Fees)							Fees,
			MAX(x.BettorsSettled)				BettorsSettled,
			MAX(x.BonusBettorsSettled)			BonusBettorsSettled,
			MAX(x.BetsSettled)					BetsSettled,
			MAX(x.BonusBetsSettled)				BonusBetsSettled,
			MAX(x.ContractsSettled)				ContractsSettled,
			MAX(x.BonusContractsSettled)		BonusContractsSettled,
			MAX(x.PlayDaysSettled)				PlayDaysSettled,
			MAX(x.BonusPlayDaysSettled)			BonusPlayDaysSettled,
			MAX(x.PlayFiscalWeeksSettled)		PlayFiscalWeeksSettled,
			MAX(x.BonusPlayFiscalWeeksSettled)	BonusPlayFiscalWeeksSettled,
			MAX(x.PlayWeeksSettled)				PlayWeeksSettled,
			MAX(x.BonusPlayWeeksSettled)		BonusPlayWeeksSettled,
			MAX(x.PlayFiscalMonthsSettled)		PlayFiscalMonthsSettled,
			MAX(x.BonusPlayFiscalMonthsSettled)	BonusPlayFiscalMonthsSettled,
			MAX(x.PlayMonthsSettled)			PlayMonthsSettled,
			MAX(x.BonusPlayMonthsSettled)		BonusPlayMonthsSettled,
			SUM(x.TurnoverSettled)				TurnoverSettled,
			SUM(x.BonusTurnoverSettled)			BonusTurnoverSettled,
			SUM(x.Payout)						Payout,
			SUM(x.BonusPayout)					BonusPayout,
			SUM(x.GrossWin)						GrossWin,
			SUM(x.GrossWinResettledAdjustment)	GrossWinResettledAdjustment,
			SUM(x.BonusGrossWin)				BonusGrossWin,
			MAX(x.BettorsCashedOut)				BettorsCashedOut,
			MAX(x.BetsCashedOut)				BetsCashedOut,
			SUM(x.Cashout)						Cashout,
			SUM(x.CashoutDifferential)			CashoutDifferential,
			SUM(x.Promotions)					Promotions,
			SUM(x.Transfers)					Transfers,
			SUM(x.Adjustments)					Adjustments,
			SUM(x.NetRevenue)					NetRevenue,
			SUM(x.NetRevenueAdjustment)			NetRevenueAdjustment,
			MAX(FirstTimeAccruers)				FirstTimeAccruers,
			MAX(RewardAccruals)					RewardAccruals,
			MAX(RewardAccruers)					RewardAccruers,
			SUM(RewardAccrualPoints)			RewardAccrualPoints,
			MAX(FirstTimeBonusRedeemers)		FirstTimeBonusRedeemers,
			MAX(RewardBonusRedemptions)			RewardBonusRedemptions,
			MAX(RewardBonusRedeemers)			RewardBonusRedeemers,
			SUM(RewardBonusRedeemedPoints)		RewardBonusRedeemedPoints,
			SUM(RewardBonusRedeemed)			RewardBonusRedeemed,
			MAX(FirstTimeVelocityRedeemers)		FirstTimeVelocityRedeemers,
			MAX(RewardVelocityRedemptions)		RewardVelocityRedemptions,
			MAX(RewardVelocityRedeemers)		RewardVelocityRedeemers,
			SUM(RewardVelocityRedeemedPoints)	RewardVelocityRedeemedPoints,
			SUM(RewardVelocityRedeemed)			RewardVelocityRedeemed,
			SUM(RewardVelocityCost)				RewardVelocityCost,
			MAX(FirstTimeRedeemers)				FirstTimeRedeemers,
			MAX(RewardRedeemers)				RewardRedeemers,
			SUM(RewardAdjustmentPoints)			RewardAdjustmentPoints,
			SUM(x.BetbackGrossWin)				BetbackGrossWin,
			MAX(x.FromDate)						FromDate,
			MAX(x.MappingId)					MappingId
	INTO	#Stage
	FROM	#Collect x
			LEFT JOIN [$(Dimensional)].Dimension.[Contract] c		ON c.LegID = x.LegID AND c.LegIdSource = x.LegSource
			LEFT JOIN [$(Dimensional)].Dimension.Account a			ON a.LedgerId = x.LedgerId AND a.LedgerSource = x.LedgerSource
			LEFT JOIN [$(Dimensional)].CC.[Day] d					ON d.MasterDayText = x.MasterDayText and d.FromDate <= x.MasterTimestamp AND d.ToDate > x.MasterTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.[Time] tm			ON tm.TimeText = x.MasterTimeText
			LEFT JOIN [$(Dimensional)].Dimension.[Time] ta			ON ta. TimeText = x.AnalysisTimeText
			LEFT JOIN [$(Dimensional)].Dimension.AccountStatus sa	ON sa.LedgerId = x.LedgerId AND sa.LedgerSource = x.LedgerSource AND sa.FromDate <= x.MasterTimestamp AND sa.ToDate > x.MasterTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.Structure s		ON s.LedgerId = x.LedgerId AND s.LedgerSource = x.LedgerSource AND s.HeadlineFeatures = x.HeadlineFeatures
			LEFT JOIN [$(Dimensional)].Dimension.BetType bt			ON bt.BetType = x.BetType AND bt.LegType = x.LegType AND bt.BetSubType = x.BetSubType AND bt.BetGroupType = x.BetGroupType AND bt.GroupingType = x.GroupingType
			LEFT JOIN [$(Dimensional)].Dimension.LegType l			ON l.LegNumber = x.LegNumber AND l.NumberOfLegs = x.NumberOfLegs
			LEFT JOIN [$(Dimensional)].Dimension.Channel ch			ON ch.ChannelId = x.ChannelId
			LEFT JOIN [$(Dimensional)].Dimension.Channel ac			ON ac.ChannelId = x.ActionChannelId
			LEFT JOIN [$(Dimensional)].Dimension.Campaign cp		ON cp.CampaignId = x.CampaignId
			LEFT JOIN [$(Dimensional)].Dimension.Market m			ON m.MarketID = x.MarketID AND m.Source = x.MarketSource
			LEFT JOIN [$(Dimensional)].Dimension.[Event] e			ON e.EventID = x.EventId AND e.Source = x.EventSource
			LEFT JOIN [$(Dimensional)].Dimension.[Event] e1			ON e1.SourceEvent = x.SourceEvent AND e1.SourceEventDate = x.SourceEventDate
			LEFT JOIN [$(Dimensional)].Dimension.[Class] cl			ON cl.ClassID = x.ClassID AND cl.Source = x.ClassSource AND cl.FromDate <= x.MasterTimestamp AND cl.ToDate > x.MasterTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.[User] u			ON u.UserID=x.UserID and x.UserSource = u.source
			LEFT JOIN [$(Dimensional)].Dimension.Instrument i		ON i.InstrumentID = x.InstrumentID AND i.Source = x.InstrumentSource
			LEFT JOIN [$(Dimensional)].Dimension.InPlay	ip			ON ip.ClickToCall = x.ClickToCall AND ip.InPlay = x.InPlay AND ip.CashoutType = x.CashoutType AND ip.IsDoubleDown = x.IsDoubleDown AND ip.IsDoubledDown = x.IsDoubledDown and ip.IsChaseTheAce = x.IsChaseTheAce AND ip.FeatureFlags = x.FeatureFlags
	GROUP BY c.ContractKey, 
			a.AccountKey, 
			x.MasterTimestamp
	HAVING MAX(x.AccountsOpened) <> 0
		OR SUM(x.DepositsRequested) <> 0
		OR SUM(x.DepositsCompleted) <> 0
		OR SUM(x.WithdrawalsRequested) <> 0
		OR SUM(x.WithdrawalsCompleted) <> 0
		OR SUM(x.TurnoverPlaced) <> 0
		OR SUM(x.BonusTurnoverPlaced) <> 0
		OR SUM(x.Fees) <> 0
		OR SUM(x.TurnoverSettled) <> 0
		OR SUM(x.BonusTurnoverSettled) <> 0
		OR SUM(x.Payout) <> 0
		OR SUM(x.BonusPayout) <> 0
		OR SUM(x.GrossWin) <> 0
		OR SUM(x.GrossWinResettledAdjustment) <> 0
		OR SUM(x.BonusGrossWin) <> 0
		OR SUM(x.Cashout) <> 0
		OR SUM(x.CashoutDifferential) <> 0
		OR SUM(x.Promotions) <> 0
		OR SUM(x.Transfers) <> 0
		OR SUM(x.Adjustments) <> 0
		OR SUM(x.NetRevenue) <> 0
		OR SUM(x.NetRevenueAdjustment) <> 0
		OR SUM(RewardAccrualPoints) <> 0
		OR SUM(RewardBonusRedeemedPoints) <> 0
		OR SUM(RewardBonusRedeemed) <> 0
		OR SUM(RewardVelocityRedeemedPoints) <> 0
		OR SUM(RewardVelocityRedeemed) <> 0
		OR SUM(RewardVelocityCost) <> 0
		OR SUM(RewardAdjustmentPoints) <> 0
		OR SUM(x.BetbackGrossWin) <> 0
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Fact','Start',@RowsProcessed = @@ROWCOUNT

	SELECT	ContractKey, AccountKey, MasterTimestamp, AnalysisTimestamp, DayKey, MasterTimeKey, AnalysisTimeKey, AccountStatusKey, StructureKey,
			BetTypeKey, LegTypeKey, ChannelKey, ActionChannelKey, CampaignKey, MarketKey, EventKey, ClassKey, UserKey, InstrumentKey, InPlayKey,
			AccountsOpened, FirstTimeDepositors, DepositsRequested, DepositsCompleted, DepositsRequestedCount, DepositsCompletedCount, 
			WithdrawalsRequested, WithdrawalsCompleted, WithdrawalsRequestedCount, WithdrawalsCompletedCount,
			FirstTimeBettors, BettorsPlaced, BonusBettorsPlaced, BetsPlaced, BonusBetsPlaced, ContractsPlaced, BonusContractsPlaced,
			PlayDaysPlaced, BonusPlayDaysPlaced, PlayFiscalWeeksPlaced, BonusPlayFiscalWeeksPlaced, PlayWeeksPlaced, BonusPlayWeeksPlaced, 
			PlayFiscalMonthsPlaced, BonusPlayFiscalMonthsPlaced, PlayMonthsPlaced, BonusPlayMonthsPlaced,
			TurnoverPlaced, BonusTurnoverPlaced, Fees, BettorsSettled, BonusBettorsSettled, BetsSettled, BonusBetsSettled, ContractsSettled, BonusContractsSettled,
			PlayDaysSettled, BonusPlayDaysSettled, PlayFiscalWeeksSettled, BonusPlayFiscalWeeksSettled, PlayWeeksSettled, BonusPlayWeeksSettled, 
			PlayFiscalMonthsSettled, BonusPlayFiscalMonthsSettled, PlayMonthsSettled, BonusPlayMonthsSettled, 
			TurnoverSettled, BonusTurnoverSettled, Payout, BonusPayout, GrossWin, GrossWinResettledAdjustment, BonusGrossWin, BettorsCashedOut, BetsCashedOut, Cashout, CashoutDifferential,
			Promotions, Transfers, Adjustments, NetRevenue, NetRevenueAdjustment,
			FirstTimeAccruers, RewardAccruals, RewardAccruers, RewardAccrualPoints,
			FirstTimeBonusRedeemers, RewardBonusRedemptions, RewardBonusRedeemers, RewardBonusRedeemedPoints, RewardBonusRedeemed,
			FirstTimeVelocityRedeemers, RewardVelocityRedemptions, RewardVelocityRedeemers, RewardVelocityRedeemedPoints, RewardVelocityRedeemed, RewardVelocityCost,
			FirstTimeRedeemers, RewardRedeemers, RewardAdjustmentPoints, 
			BetbackGrossWin,
			FromDate, MappingId
	INTO	#Fact
	FROM	[$(Dimensional)].CC.[KPI]
	WHERE	CreatedBatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start',@RowsProcessed = @@ROWCOUNT

	SELECT  *, CASE WHEN COUNT(*) OVER (PARTITION BY DayKey, ContractKey, AccountKey, MasterTimestamp) = 2 THEN 'U' WHEN src = 'S' THEN 'I' WHEN src = 'F' THEN 'D' ELSE 'X' END Upsert
	INTO	#Differences
	FROM	(	SELECT  ContractKey, AccountKey, MasterTimestamp, AnalysisTimestamp, DayKey, MasterTimeKey, AnalysisTimeKey, AccountStatusKey, StructureKey,
						BetTypeKey, LegTypeKey, ChannelKey, ActionChannelKey, CampaignKey, MarketKey, EventKey, ClassKey, UserKey, InstrumentKey, InPlayKey,
						AccountsOpened, FirstTimeDepositors, DepositsRequested, DepositsCompleted, DepositsRequestedCount, DepositsCompletedCount, 
						WithdrawalsRequested, WithdrawalsCompleted, WithdrawalsRequestedCount, WithdrawalsCompletedCount,
						FirstTimeBettors, BettorsPlaced, BonusBettorsPlaced, BetsPlaced, BonusBetsPlaced, ContractsPlaced, BonusContractsPlaced,
						PlayDaysPlaced, BonusPlayDaysPlaced, PlayFiscalWeeksPlaced, BonusPlayFiscalWeeksPlaced, PlayWeeksPlaced, BonusPlayWeeksPlaced, 
						PlayFiscalMonthsPlaced, BonusPlayFiscalMonthsPlaced, PlayMonthsPlaced, BonusPlayMonthsPlaced,
						TurnoverPlaced, BonusTurnoverPlaced, Fees, BettorsSettled, BonusBettorsSettled, BetsSettled, BonusBetsSettled, ContractsSettled, BonusContractsSettled,
						PlayDaysSettled, BonusPlayDaysSettled, PlayFiscalWeeksSettled, BonusPlayFiscalWeeksSettled, PlayWeeksSettled, BonusPlayWeeksSettled, 
						PlayFiscalMonthsSettled, BonusPlayFiscalMonthsSettled, PlayMonthsSettled, BonusPlayMonthsSettled, 
						TurnoverSettled, BonusTurnoverSettled, Payout, BonusPayout, GrossWin, GrossWinResettledAdjustment, BonusGrossWin, BettorsCashedOut, BetsCashedOut, Cashout, CashoutDifferential,
						Promotions, Transfers, Adjustments, NetRevenue, NetRevenueAdjustment,
						FirstTimeAccruers, RewardAccruals, RewardAccruers, RewardAccrualPoints,
						FirstTimeBonusRedeemers, RewardBonusRedemptions, RewardBonusRedeemers, RewardBonusRedeemedPoints, RewardBonusRedeemed,
						FirstTimeVelocityRedeemers, RewardVelocityRedemptions, RewardVelocityRedeemers, RewardVelocityRedeemedPoints, RewardVelocityRedeemed, RewardVelocityCost,
						FirstTimeRedeemers, RewardRedeemers, RewardAdjustmentPoints, 
						BetbackGrossWin,
						MAX(FromDate) FromDate, MAX(src) src
				FROM	(	SELECT *, 'S' src FROM #Stage
							UNION ALL
							SELECT *, 'F' src FROM #Fact
						) x
				GROUP BY ContractKey, AccountKey, MasterTimestamp, AnalysisTimestamp, DayKey, MasterTimeKey, AnalysisTimeKey, AccountStatusKey, StructureKey,
						BetTypeKey, LegTypeKey, ChannelKey, ActionChannelKey, CampaignKey, MarketKey, EventKey, ClassKey, UserKey, InstrumentKey, InPlayKey,
						AccountsOpened, FirstTimeDepositors, DepositsRequested, DepositsCompleted, DepositsRequestedCount, DepositsCompletedCount, 
						WithdrawalsRequested, WithdrawalsCompleted, WithdrawalsRequestedCount, WithdrawalsCompletedCount,
						FirstTimeBettors, BettorsPlaced, BonusBettorsPlaced, BetsPlaced, BonusBetsPlaced, ContractsPlaced, BonusContractsPlaced,
						PlayDaysPlaced, BonusPlayDaysPlaced, PlayFiscalWeeksPlaced, BonusPlayFiscalWeeksPlaced, PlayWeeksPlaced, BonusPlayWeeksPlaced, 
						PlayFiscalMonthsPlaced, BonusPlayFiscalMonthsPlaced, PlayMonthsPlaced, BonusPlayMonthsPlaced,
						TurnoverPlaced, BonusTurnoverPlaced, Fees, BettorsSettled, BonusBettorsSettled, BetsSettled, BonusBetsSettled, ContractsSettled, BonusContractsSettled,
						PlayDaysSettled, BonusPlayDaysSettled, PlayFiscalWeeksSettled, BonusPlayFiscalWeeksSettled, PlayWeeksSettled, BonusPlayWeeksSettled, 
						PlayFiscalMonthsSettled, BonusPlayFiscalMonthsSettled, PlayMonthsSettled, BonusPlayMonthsSettled, 
						TurnoverSettled, BonusTurnoverSettled, Payout, BonusPayout, GrossWin, GrossWinResettledAdjustment, BonusGrossWin, BettorsCashedOut, BetsCashedOut, Cashout, CashoutDifferential,
						Promotions, Transfers, Adjustments, NetRevenue, NetRevenueAdjustment,
						FirstTimeAccruers, RewardAccruals, RewardAccruers, RewardAccrualPoints,
						FirstTimeBonusRedeemers, RewardBonusRedemptions, RewardBonusRedeemers, RewardBonusRedeemedPoints, RewardBonusRedeemed,
						FirstTimeVelocityRedeemers, RewardVelocityRedemptions, RewardVelocityRedeemers, RewardVelocityRedeemedPoints, RewardVelocityRedeemed, RewardVelocityCost,
						FirstTimeRedeemers, RewardRedeemers, RewardAdjustmentPoints, 
						BetbackGrossWin
				HAVING	COUNT(*) = 1
			) y
	OPTION (RECOMPILE)

	IF @DryRun = 1 
		SELECT	TOP 10000 
				CASE WHEN Upsert = 'U' AND src = 'F' THEN 'U-' WHEN Upsert = 'U' AND src = 'S' THEN 'U+' ELSE Upsert END act, 
				* 
		FROM	#Differences 
		ORDER BY DayKey, ContractKey, AccountKey, MasterTimestamp, src desc

	SELECT	* INTO #Prepare FROM #Differences WHERE	Upsert <> 'U' OR src <> 'F' -- Discard the 'before' version record of Update

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start',@RowsProcessed = @@ROWCOUNT

	IF @DryRun = 0 
		BEGIN
			UPDATE	d 
				SET	AccountStatusKey = s.AccountStatusKey,
					StructureKey = s.StructureKey,
					BetTypeKey = s.BetTypeKey,
					LegTypeKey = s.LegTypeKey,
					ChannelKey = s.ChannelKey,
					ActionChannelKey = s.ActionChannelKey,
					CampaignKey = s.CampaignKey,
					MarketKey = s.MarketKey,
					EventKey = s.EventKey,
					ClassKey = s.ClassKey,
					UserKey = s.UserKey,
					InstrumentKey = s.InstrumentKey,
					InPlayKey = s.InPlayKey,
					AccountsOpened = s.AccountsOpened,
					FirstTimeDepositors = s.FirstTimeDepositors,
					DepositsRequested = s.DepositsRequested,
					DepositsCompleted = s.DepositsCompleted,
					DepositsRequestedCount = s.DepositsRequestedCount,
					DepositsCompletedCount = s.DepositsCompletedCount,
					WithdrawalsRequested = s.WithdrawalsRequested,
					WithdrawalsCompleted = s.WithdrawalsCompleted,
					WithdrawalsRequestedCount = s.WithdrawalsRequestedCount,
					WithdrawalsCompletedCount = s.WithdrawalsCompletedCount,
					FirstTimeBettors = s.FirstTimeBettors,
					BettorsPlaced = s.BettorsPlaced,
					BonusBettorsPlaced = s.BonusBettorsPlaced,
					BetsPlaced = s.BetsPlaced,
					BonusBetsPlaced = s.BonusBetsPlaced,
					ContractsPlaced = s.ContractsPlaced,
					BonusContractsPlaced = s.BonusContractsPlaced,
					PlayDaysPlaced = s.PlayDaysPlaced,
					BonusPlayDaysPlaced = s.BonusPlayDaysPlaced,
					PlayFiscalWeeksPlaced = s.PlayFiscalWeeksPlaced,
					BonusPlayFiscalWeeksPlaced = s.BonusPlayFiscalWeeksPlaced,
					PlayWeeksPlaced = s.PlayWeeksPlaced,
					BonusPlayWeeksPlaced = s.BonusPlayWeeksPlaced,
					PlayFiscalMonthsPlaced = s.PlayFiscalMonthsPlaced,
					BonusPlayFiscalMonthsPlaced = s.BonusPlayFiscalMonthsPlaced,
					PlayMonthsPlaced = s.PlayMonthsPlaced,
					BonusPlayMonthsPlaced = s.BonusPlayMonthsPlaced,
					TurnoverPlaced = s.TurnoverPlaced,
					BonusTurnoverPlaced = s.BonusTurnoverPlaced,
					Fees = s.Fees,
					BettorsSettled = s.BettorsSettled,
					BonusBettorsSettled = s.BonusBettorsSettled,
					BetsSettled = s.BetsSettled,
					BonusBetsSettled = s.BonusBetsSettled,
					ContractsSettled = s.ContractsSettled,
					BonusContractsSettled = s.BonusContractsSettled,
					PlayDaysSettled = s.PlayDaysSettled,
					BonusPlayDaysSettled = s.BonusPlayDaysSettled,
					PlayFiscalWeeksSettled = s.PlayFiscalWeeksSettled,
					BonusPlayFiscalWeeksSettled = s.BonusPlayFiscalWeeksSettled,
					PlayWeeksSettled = s.PlayWeeksSettled,
					BonusPlayWeeksSettled = s.BonusPlayWeeksSettled,
					PlayFiscalMonthsSettled = s.PlayFiscalMonthsSettled,
					BonusPlayFiscalMonthsSettled = s.BonusPlayFiscalMonthsSettled,
					PlayMonthsSettled = s.PlayMonthsSettled,
					BonusPlayMonthsSettled = s.BonusPlayMonthsSettled,
					TurnoverSettled = s.TurnoverSettled,
					BonusTurnoverSettled = s.BonusTurnoverSettled,
					Payout = s.Payout,
					BonusPayout = s.BonusPayout,
					GrossWin = s.GrossWin,
					GrossWinResettledAdjustment = s.GrossWinResettledAdjustment,
					BonusGrossWin = s.BonusGrossWin,
					BettorsCashedOut = s.BettorsCashedOut,
					BetsCashedOut = s.BetsCashedOut,
					Cashout = s.Cashout,
					CashoutDifferential = s.CashoutDifferential,
					Promotions = s.Promotions,
					Transfers = s.Transfers,
					Adjustments = s.Adjustments,
					NetRevenue = s.NetRevenue,
					NetRevenueAdjustment = s.NetRevenueAdjustment,
					FirstTimeAccruers = s.FirstTimeAccruers,
					RewardAccruals = s.RewardAccruals,
					RewardAccruers = s.RewardAccruers,
					RewardAccrualPoints = s.RewardAccrualPoints,
					FirstTimeBonusRedeemers = s.FirstTimeBonusRedeemers,
					RewardBonusRedemptions = s.RewardBonusRedemptions,
					RewardBonusRedeemers = s.RewardBonusRedeemers,
					RewardBonusRedeemedPoints = s.RewardBonusRedeemedPoints,
					RewardBonusRedeemed = s.RewardBonusRedeemed,
					FirstTimeVelocityRedeemers = s.FirstTimeVelocityRedeemers,
					RewardVelocityRedemptions = s.RewardVelocityRedemptions,
					RewardVelocityRedeemers = s.RewardVelocityRedeemers,
					RewardVelocityRedeemedPoints = s.RewardVelocityRedeemedPoints,
					RewardVelocityRedeemed = s.RewardVelocityRedeemed,
					RewardVelocityCost = s.RewardVelocityCost,
					FirstTimeRedeemers = s.FirstTimeRedeemers,
					RewardRedeemers = s.RewardRedeemers,
					RewardAdjustmentPoints = s.RewardAdjustmentPoints,
					BetbackGrossWin = s.BetbackGrossWin
			FROM	[$(Dimensional)].CC.KPI d
					INNER JOIN #Prepare p  ON p.DayKey = d.DayKey AND p.ContractKey = d.ContractKey AND p.AccountKey = d.AccountKey AND p.MasterTimestamp = d.MasterTimestamp
					INNER JOIN #Stage s  ON s.DayKey = p.DayKey AND s.ContractKey = p.ContractKey AND s.AccountKey = p.AccountKey AND s.MasterTimestamp = p.MasterTimestamp
			WHERE	p.Upsert = 'U'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'U'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start',@RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			INSERT [$(Dimensional)].CC.[KPI]
				(	ContractKey, AccountKey, MasterTimestamp, AnalysisTimestamp, DayKey, MasterTimeKey, AnalysisTimeKey, AccountStatusKey, StructureKey,
					BetTypeKey, LegTypeKey, ChannelKey, ActionChannelKey, CampaignKey, MarketKey, EventKey, ClassKey, UserKey, InstrumentKey, InPlayKey,
					AccountsOpened, FirstTimeDepositors, DepositsRequested, DepositsCompleted, DepositsRequestedCount, DepositsCompletedCount, 
					WithdrawalsRequested, WithdrawalsCompleted, WithdrawalsRequestedCount, WithdrawalsCompletedCount,
					FirstTimeBettors, BettorsPlaced, BonusBettorsPlaced, BetsPlaced, BonusBetsPlaced, ContractsPlaced, BonusContractsPlaced,
					PlayDaysPlaced, BonusPlayDaysPlaced, PlayFiscalWeeksPlaced, BonusPlayFiscalWeeksPlaced, PlayWeeksPlaced, BonusPlayWeeksPlaced, 
					PlayFiscalMonthsPlaced, BonusPlayFiscalMonthsPlaced, PlayMonthsPlaced, BonusPlayMonthsPlaced,
					TurnoverPlaced, BonusTurnoverPlaced, Fees,
					BettorsSettled, BonusBettorsSettled, BetsSettled, BonusBetsSettled, ContractsSettled, BonusContractsSettled,
					PlayDaysSettled, BonusPlayDaysSettled, PlayFiscalWeeksSettled, BonusPlayFiscalWeeksSettled, PlayWeeksSettled, BonusPlayWeeksSettled,
					PlayFiscalMonthsSettled, BonusPlayFiscalMonthsSettled, PlayMonthsSettled, BonusPlayMonthsSettled,
					TurnoverSettled, BonusTurnoverSettled, Payout, BonusPayout, GrossWin, GrossWinResettledAdjustment, BonusGrossWin,
					BettorsCashedOut, BetsCashedOut, Cashout, CashoutDifferential,
					Promotions, Transfers, Adjustments, NetRevenue, NetRevenueAdjustment,
					FirstTimeAccruers, RewardAccruals, RewardAccruers, RewardAccrualPoints,
					FirstTimeBonusRedeemers, RewardBonusRedemptions, RewardBonusRedeemers, RewardBonusRedeemedPoints, RewardBonusRedeemed,
					FirstTimeVelocityRedeemers, RewardVelocityRedemptions, RewardVelocityRedeemers, RewardVelocityRedeemedPoints, RewardVelocityRedeemed, RewardVelocityCost,
					FirstTimeRedeemers, RewardRedeemers, RewardAdjustmentPoints,
					BetbackGrossWin,
					FromDate, MappingId, CreatedDate, CreatedBatchKey, CreatedBy
				)
			SELECT	s.ContractKey, s.AccountKey, s.MasterTimestamp, s.AnalysisTimestamp, s.DayKey, s.MasterTimeKey, s.AnalysisTimeKey, s.AccountStatusKey, s.StructureKey,
					s.BetTypeKey, s.LegTypeKey, s.ChannelKey, s.ActionChannelKey, s.CampaignKey, s.MarketKey, s.EventKey, s.ClassKey, s.UserKey, s.InstrumentKey, s.InPlayKey,
					s.AccountsOpened, s.FirstTimeDepositors, s.DepositsRequested, s.DepositsCompleted, s.DepositsRequestedCount, s.DepositsCompletedCount,
					s.WithdrawalsRequested, s.WithdrawalsCompleted, s.WithdrawalsRequestedCount, s.WithdrawalsCompletedCount,
					s.FirstTimeBettors, s.BettorsPlaced, s.BonusBettorsPlaced, s.BetsPlaced, s.BonusBetsPlaced, s.ContractsPlaced, s.BonusContractsPlaced,
					s.PlayDaysPlaced, s.BonusPlayDaysPlaced, s.PlayFiscalWeeksPlaced, s.BonusPlayFiscalWeeksPlaced, s.PlayWeeksPlaced, s.BonusPlayWeeksPlaced,
					s.PlayFiscalMonthsPlaced, s.BonusPlayFiscalMonthsPlaced, s.PlayMonthsPlaced, s.BonusPlayMonthsPlaced,
					s.TurnoverPlaced, s.BonusTurnoverPlaced, s.Fees,
					s.BettorsSettled, s.BonusBettorsSettled, s.BetsSettled, s.BonusBetsSettled, s.ContractsSettled, s.BonusContractsSettled,
					s.PlayDaysSettled, s.BonusPlayDaysSettled, s.PlayFiscalWeeksSettled, s.BonusPlayFiscalWeeksSettled, s.PlayWeeksSettled, s.BonusPlayWeeksSettled,
					s.PlayFiscalMonthsSettled, s.BonusPlayFiscalMonthsSettled, s.PlayMonthsSettled, s.BonusPlayMonthsSettled,
					s.TurnoverSettled, s.BonusTurnoverSettled, s.Payout, s.BonusPayout, s.GrossWin, s.GrossWinResettledAdjustment, s.BonusGrossWin,
					s.BettorsCashedOut, s.BetsCashedOut, s.Cashout, s.CashoutDifferential,
					s.Promotions, s.Transfers, s.Adjustments, s.NetRevenue, s.NetRevenueAdjustment,
					s.FirstTimeAccruers, s.RewardAccruals, s.RewardAccruers, s.RewardAccrualPoints,
					s.FirstTimeBonusRedeemers, s.RewardBonusRedemptions, s.RewardBonusRedeemers, s.RewardBonusRedeemedPoints, s.RewardBonusRedeemed,
					s.FirstTimeVelocityRedeemers, s.RewardVelocityRedemptions, s.RewardVelocityRedeemers, s.RewardVelocityRedeemedPoints, s.RewardVelocityRedeemed, s.RewardVelocityCost,
					s.FirstTimeRedeemers, s.RewardRedeemers, s.RewardAdjustmentPoints,
					s.BetbackGrossWin,
					s.FromDate, s.MappingId, SYSDATETIME() CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy
			FROM	#Prepare p
					INNER JOIN #Stage s  ON s.DayKey = p.DayKey AND s.ContractKey = p.ContractKey AND s.AccountKey = p.AccountKey AND s.MasterTimestamp = p.MasterTimestamp
			WHERE	p.Upsert = 'I'
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'I'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Delete','Start',@RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			DELETE	d
			FROM	[$(Dimensional)].CC.[KPI] d
					INNER JOIN #Prepare p ON p.DayKey = d.DayKey AND p.ContractKey = d.ContractKey AND p.AccountKey = d.AccountKey AND p.MasterTimestamp = d.MasterTimestamp
			WHERE	p.Upsert = 'D'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'D'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success',@RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
