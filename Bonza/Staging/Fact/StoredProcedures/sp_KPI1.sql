CREATE PROC [Fact].[sp_KPI1]
(
	@BatchKey		int,
	@DryRun			bit = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @Trued char(1) = 'Y'

	DECLARE @RowCount int

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Legacy','Start'

	SELECT	CONVERT(binary(16),
			CASE TransactionIdSource 
				WHEN 'IBB' THEN -TransactionID/10 
				WHEN 'IBP' THEN -TransactionID/10 
				WHEN 'IBC' THEN TransactionID
				WHEN 'IBT' THEN TransactionId
				WHEN 'WRB' THEN TransactionId
				WHEN 'LTI' THEN LegId
				ELSE LegId
			END) Id
			,CASE TransactionIdSource 
				WHEN 'IBB' THEN 'IBB'
				WHEN 'IBP' THEN 'IBB'
				WHEN 'IBC' THEN 'IAA'
				WHEN 'IBT' THEN 'IBT'
				WHEN 'WRB' THEN 'WRI'
				WHEN 'LTI' THEN 'IBI'
				ELSE LegIdSource
			END IdSource,
			LegId LegacyLegId, LegIdSource LegacyLegSource, LedgerId, CASE WHEN LedgerId = -1 THEN 'UNK' ELSE LedgerSource END LedgerSource,
			MasterTransactionTimestamp MasterTimestamp,
			BetTypeName BetType, LegBetTypeName LegType, BetGroupType, BetSubTypeName BetSubType, GroupingType, LegNumber, NumberOfLegs, Channel ChannelId, ActionChannel ActionChannelId, PromotionId, PromotionIdSource PromotionSource,
			EventId MarketID, EventIdSource MarketSource, EventID, EventIdSource EventSource, ClassID, ClassIdSource ClassSource, UserID, UserIDSource UserSource, InstrumentID, InstrumentIDSource InstrumentSource,
			CASE InPlay WHEN 'Y' THEN 1 ELSE 0 END HeadlineFeatures,
			InPlay, ClickToCall, CashoutType, IsDoubleDown, IsDoubledDown, IsChaseTheAce, FeatureFlags, WalletId,
			CASE
				WHEN TransactionTypeId = 'BT' AND BalanceTypeId = 'UNS' AND TransactionStatusId IN ('LO','WN','UN','CO') THEN -TransactedAmount 
				ELSE TransactedAmount 
			END Amount,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN -TransactionID/10 
				WHEN 'IBP' THEN -TransactionID/10 
				WHEN 'IBC' THEN TransactionID
				WHEN 'IBT' THEN TransactionId
				WHEN 'WRB' THEN TransactionId
				WHEN 'LTI' THEN TransactionId
				ELSE TransactionId
			END TransactionId,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN 'IBB'
				WHEN 'IBP' THEN 'IBB'
				WHEN 'IBC' THEN 'IAA'
				WHEN 'IBT' THEN 'IBT'
				WHEN 'WRB' THEN 'WRI'
				WHEN 'LTI' THEN 'IBT'
				ELSE TransactionIdSource
			END TransactionSource,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN -TransactionID/10
				WHEN 'IBP' THEN -TransactionID/10
				WHEN 'IBC' THEN TransactionID
				WHEN 'IBT' THEN TransactionId
				WHEN 'WRB' THEN TransactionId
				WHEN 'LTI' THEN BetGroupId
				ELSE BetGroupID
			END GroupId,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN 'IBB'
				WHEN 'IBP' THEN 'IBB'
				WHEN 'IBC' THEN 'IAA'
				WHEN 'IBT' THEN 'IBT'
				WHEN 'WRB' THEN 'WRI'
				WHEN 'LTI' THEN 'IBT'
				ELSE BetGroupIDSource
			END GroupSource,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN NULL 
				WHEN 'IBP' THEN NULL 
				WHEN 'IBC' THEN NULL
				WHEN 'IBT' THEN NULL
				WHEN 'WRB' THEN NULL
				WHEN 'LTI' THEN BetId
				ELSE BetId
			END BetId,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN 'N/A' 
				WHEN 'IBP' THEN 'N/A' 
				WHEN 'IBC' THEN 'N/A'
				WHEN 'IBT' THEN 'N/A'
				WHEN 'WRB' THEN 'N/A'
				WHEN 'LTI' THEN 'IBT'
				ELSE BetIdSource
			END BetSource,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN NULL 
				WHEN 'IBP' THEN NULL 
				WHEN 'IBC' THEN NULL
				WHEN 'IBT' THEN NULL
				WHEN 'WRB' THEN NULL
				WHEN 'LTI' THEN LegId
				ELSE LegId
			END LegId,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN 'N/A' 
				WHEN 'IBP' THEN 'N/A' 
				WHEN 'IBC' THEN 'N/A'
				WHEN 'IBT' THEN 'N/A'
				WHEN 'WRB' THEN 'N/A'
				WHEN 'LTI' THEN 'IBT'
				ELSE BetIdSource
			END LegSource,
			CASE TransactionIdSource 
				WHEN 'IBB' THEN -LegId/10
				WHEN 'IBP' THEN -LegId/10 
				WHEN 'IBC' THEN NULL
				WHEN 'IBT' THEN NULL
				WHEN 'WRB' THEN NULL
				WHEN 'LTI' THEN NULL
				ELSE NULLIF(FreeBetId,0)
			END FreeBetId,
			NULL ExternalReference,
			'N/A' ExternalSource,
		-- TransactionCode Restructure:
		--	Bet.Place.System			= BT/RE/SY, (BT/AC/SY)
		--	Bet.Settle.Win			= BT/WI/SE+UNS, BT/WI/AD+UNS
		--	Bet.Settle.Lose			= BT/LO/SE+UNS, BT/LO/AD+UNS
		--	Bet.Settle.Cashout		= BT/CO/SE+UNS, BT/CO/AD+UNS
		--	Bet.Unsettle.Win		= BT/UN/AD+UNS   - compromise with Bet.Unsettle.All in the short term
		--	Bet.Unsettle.Lose		= BT/UN/AD+UNS   - compromise with Bet.Unsettle.All in the short term
		--	Bet.Payout.Win			= BT/WI/SE+CLI, BT/WI/AD+CLI 
		--	Bet.Payout.Cashout		= BT/CO/SE+CLI, BT/CO/AD+CLI
		--	Bet.Reclaim.Win			= BT/UN/AD+CLI
			CASE TransactionTypeId
				WHEN 'AD' THEN 'Adjustment'
				WHEN 'BT' THEN 'Bet'
				WHEN 'BO' THEN CASE TransactionMethodId WHEN 'BP' THEN 'Rebate' WHEN 'RB' THEN 'Rebate' ELSE 'Bonus' END -- Restructure
				WHEN 'DP' THEN 'Deposit'
				WHEN 'FE' THEN 'Fee'
				WHEN 'TR' THEN 'Transfer'
				WHEN 'WI' THEN 'Withdrawal'
				ELSE 'Unknown'
			END
				+ '.' 
				+	CASE TransactionStatusId 
						WHEN 'AC' THEN 'Accept'
						WHEN 'AW' THEN 'Award'
						WHEN 'CN' THEN 'Cancel'
						WHEN 'CO' THEN CASE WHEN TransactionTypeId <> 'BT' THEN 'Complete' WHEN BalanceTypeId = 'CLI' THEN 'Payout' WHEN BalanceTypeId = 'UNS' THEN 'Settle' ELSE 'Unknown' END -- Restructure
						WHEN 'DE' THEN 'Decline'
						WHEN 'EX' THEN 'Expire'
						WHEN 'LO' THEN 'Settle' -- Restructure
						WHEN 'PY' THEN 'Payment'
						WHEN 'RE' THEN CASE TransactionTypeId WHEN 'BT' THEN 'Place' ELSE 'Request' END -- Restructure
						WHEN 'RF' THEN 'Refund'
						WHEN 'RJ' THEN 'Reject'
						WHEN 'RV' THEN 'Reverse'
						WHEN 'UN' THEN CASE BalanceTypeId WHEN 'CLI' THEN 'Reclaim' WHEN 'UNS' THEN 'Unsettle' ELSE 'Unknown' END -- Restructure
						WHEN 'WN' THEN CASE BalanceTypeId WHEN 'CLI' THEN 'Payout' WHEN 'UNS' THEN 'Settle' ELSE 'Unknown' END -- Restructure
						ELSE 'Unknown'
					END
				+ '.' 
				+	CASE TransactionMethodId 
						WHEN 'AA' THEN 'Account'
						WHEN 'AD' THEN CASE WHEN TransactionStatusId = 'WN' THEN 'Win' WHEN TransactionStatusId = 'CO' THEN 'Cashout' WHEN TransactionStatusId = 'LO' THEN 'Lose' WHEN TransactionStatusId = 'UN' AND BalanceTypeId = 'CLI' THEN 'Win' WHEN TransactionStatusId = 'UN' AND BalanceTypeId = 'UNS' THEN 'All' ELSE 'Unknown' END -- Restructure
						WHEN 'AT' THEN 'Account'
						WHEN 'B2' THEN '247'
						WHEN 'BA' THEN 'Bet'
						WHEN 'BO' THEN 'Bonus'
						WHEN 'BP' THEN 'Accrual'
						WHEN 'BP' THEN 'BetPack'
						WHEN 'BY' THEN 'BPay'
						WHEN 'CA' THEN 'Cash'
						WHEN 'CB' THEN 'Chargeback'
						WHEN 'CC' THEN 'Credit Card'
						WHEN 'CD' THEN 'CashCard'
						WHEN 'CH' THEN 'Cheque'
						WHEN 'CL' THEN 'CBLegacy'
						WHEN 'CO' THEN 'Combination'
						WHEN 'CR' THEN 'Credits'
						WHEN 'CT' THEN 'Client'
						WHEN 'CW' THEN 'EachWay'
						WHEN 'DD' THEN 'DoubleDown'
						WHEN 'DU' THEN 'Duplicate'
						WHEN 'EF' THEN 'EFT'
						WHEN 'EO' THEN 'EFTPOS'
						WHEN 'EP' THEN 'EzyPay'
						WHEN 'ER' THEN 'Error'
						WHEN 'IP' THEN 'In Play'
						WHEN 'LI' THEN 'Limit'
						WHEN 'MB' THEN 'Moneybookers'
						WHEN 'MI' THEN 'Migration'
						WHEN 'MP' THEN 'MasterPass'
						WHEN 'NE' THEN 'Neteller'
						WHEN 'NO' THEN 'NotOffered'
						WHEN 'OA' THEN 'Other'
						WHEN 'OF' THEN 'Offer'
						WHEN 'PA' THEN 'Partner'
						WHEN 'PC' THEN 'PriceChange'
						WHEN 'PD' THEN 'Poli'
						WHEN 'PO' THEN 'Poli'
						WHEN 'PP' THEN 'PayPal'
						WHEN 'RB' THEN 'Rebate'
						WHEN 'RE' THEN 'Request'
						WHEN 'SA' THEN 'SportsAlive'
						WHEN 'SC' THEN 'Scratched'
						WHEN 'SE' THEN CASE t.TransactionStatusId WHEN 'WN' THEN 'Win' WHEN 'CO' THEN 'Cashout' WHEN 'LO' THEN 'Lose' ELSE 'Unknown' END -- Restructure
						WHEN 'SK' THEN 'Skrill'
						WHEN 'SU' THEN 'Suspended'
						WHEN 'SY' THEN 'System'
						WHEN 'TC' THEN 'Terms&Conditions'
						WHEN 'TE' THEN 'Test'
						WHEN 'VI' THEN 'VIP'
						WHEN 'VO' THEN 'Void'
						WHEN 'WI' THEN 'Withdrawn'
						WHEN 'WO' THEN 'WriteOff'
						ELSE 'Unknown'
					END TransactionCode,
			CASE 
				WHEN b.FirstBetId IS NOT NULL THEN 1 
				WHEN d.FirstCreditId IS NOT NULL THEN 1 
				ELSE 0 
			END FirstForClient,
			CASE
				WHEN t.TransactionMethodID = 'AD' THEN 0
				ELSE 1
			END FirstForTransaction,
			MappingId,
			CASE 
				WHEN TransactionIdSource = 'LTI' THEN 'L' -- Lottery
				WHEN TransactionIdSource = 'WRB' THEN 'R' -- Reward
				WHEN TransactionTypeId = 'BT' THEN 'B' -- Bet
				WHEN TransactionTypeId = 'FE' THEN 'B' -- Bet (Double Down Fees)
				WHEN TransactionTypeId = 'BO' AND TransactionMethodId = 'BP' THEN 'B' -- Bet (Bonus Accrual)
				WHEN TransactionTypeId = 'DP' THEN 'D' -- Deposit
				WHEN TransactionTypeId = 'WI' THEN 'W' -- Withdrawal
				WHEN TransactionTypeId = 'BO' THEN 'O' -- Bonus
				WHEN TransactionTypeId = 'AD' THEN 'A' -- Adjustment
				WHEN TransactionTypeId = 'TR' THEN 'T' -- Transfer
				ELSE 'X'
			END FunctionalArea,
			FromDate
	INTO	#Collect
	FROM	Stage.[Transaction] t
			-- NOTE: These joins to Posiiton to get first bet are flawed because there are bugs in the Position ETL logic such that the first day of an account reports the wrong first bet but is corrected on subsequent days - however the incorrect first day is the one that is contemporary with the bet record being flagged as FTB, so the wrong bet gets flagged and then doesn't match any subsequent position record. However, first step in migrating to this new procedure is to uphold the existing precendents, warts and all, to build confidence in the new procedure, and then pick of the issues in a co-ordinated way
			LEFT JOIN (SELECT DISTINCT MasterDayText, FirstBetId FROM Stage.Position WHERE BatchKey = @BatchKey AND MasterDayText IS NOT NULL) b ON t.TransactionTypeID = 'BT' AND b.FirstBetId = t.BetId AND b.MasterDayText = CONVERT(varchar(11),CONVERT(DATE, t.MasterTransactionTimestamp))
			LEFT JOIN (SELECT DISTINCT MasterDayText, FirstCreditId FROM Stage.Position WHERE BatchKey = @BatchKey AND MasterDayText IS NOT NULL) d ON t.TransactionTypeID = 'DP' AND d.FirstCreditId = t.TransactionId AND d.MasterDayText = CONVERT(varchar(11),CONVERT(DATE, t.MasterTransactionTimestamp))
	WHERE	BatchKey = @BatchKey
			AND ExistsInDim = 0
			AND (	(TransactionTypeId = 'DP' AND BalanceTypeId = 'PDP' AND TransactionStatusId IN ('RE','DE'))
					OR (TransactionTypeId = 'DP' AND BalanceTypeId = 'CLI' AND TransactionStatusId IN ('CO','RJ'))
					OR (TransactionTypeId = 'BT' AND BalanceTypeId = 'INT' AND TransactionStatusId IN ('RE','DE','CN'))
					OR (TransactionTypeId = 'BT' AND BalanceTypeId = 'UNS' AND TransactionStatusId IN ('CN','LO','WN','UN','CO'))
					OR (TransactionTypeId = 'BT' AND BalanceTypeId = 'CLI' AND TransactionStatusId IN ('LO','WN','UN','CO'))
					OR (TransactionTypeId = 'FE' AND BalanceTypeId = 'P&L')
					OR (TransactionTypeId = 'AD' AND BalanceTypeId = 'CLI')
					OR (TransactionTypeId = 'BO' AND BalanceTypeId = 'CLI' AND WalletId = 'C')
					OR (TransactionTypeId = 'TR' AND BalanceTypeId = 'CLI')
				)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Withdrawal','Start',@RowsProcessed = @@ROWCOUNT

	INSERT	#Collect
	SELECT	CONVERT(binary(16), TransactionId) Id, TransactionSource IdSource, TransactionId LegacyLegId, TransactionSource LegacyLegSource, LedgerId, LedgerSource, a.FromDate MasterTimestamp, -- Temporary amendment to force MasterTimestamp to FromDate in order to replicate the legacy functionality prior to co-ordinated correction to match the new approach to cashflow withdrawal transactions
			'N/A' BetType, 'N/A' LegType, 'Straight'/*'N/A'*/ BetGroupType, 'N/A' BetSubType, 'Straight'/*'N/A'*/ GroupingType, 1/*0*/ LegNumber, 1/*0*/ NumberOfLegs,  -- Tempory amendemnt to Group type definitions to confirm to existing precendent for parallel testing before enforcing correct value
			ChannelId, CASE WHEN TransactionCode LIKE 'Withdrawal.Request.%' THEN ChannelId ELSE ChannelId END ActionChannelId, 0 PromotionId, 'N/A' PromotionSource, -- ActionChannel should only be non-zero for request, but set to same ChannelId throughout for consistency with pre-existing convention before a co-ordinated correction exercise
			0 MarketID, 'N/A' MarketSource, 0 EventID, 'N/A' EventSource, 0 ClassID, 'IBT' ClassSource, CASE UserID WHEN 'N/A' THEN '-4' ELSE UserId END UserId, CASE UserID WHEN 'N/A' THEN 'IUU' ELSE UserSource END UserSource, CASE WHEN TransactionCode LIKE '%.CashCard' THEN InstrumentId WHEN InstrumentID = 0 THEN TransactionId ELSE InstrumentId END InstrumentId, CASE WHEN TransactionCode LIKE '%.BPay' THEN 'BPY' WHEN TransactionCode LIKE '%.EFT' THEN 'EFT' WHEN TransactionCode LIKE '%.Moneybookers' THEN 'MBK' WHEN TransactionCode LIKE '%.Paypal' THEN 'PPL' ELSE 'N/A' END InstrumentSource, -- Tempory amendemnt to User and instrument definitions to confirm to existing precendent for parallel testing before enforcing correct value
			0 HeadlineFeatures,
			'N/A' InPlay, 'N/A' ClickToCall, 0 CashoutType, 0 IsDoubleDown, 0 IsDoubledDown, 0 IsChaseTheAce, 0 FeatureFlags, 'C' WalletId,
			Amount Amount,
			TransactionId, TransactionSource, ISNULL(RequestId, TransactionId) GroupId, CASE WHEN RequestId IS NOT NULL THEN 'ITR' ELSE TransactionSource END GroupSource, 
			NULL BetId, 'N/A' BetSource, NULL LegId, 'N/A' LegSource, NULL FreeBetId, NULL ExternalReference, 'N/A' ExternalSource,
			TransactionCode, 0 FirstForClient, 1 FirstForTransaction, MappingId, 'W' FunctionalArea, a.FromDate 
	FROM	Atomic.Withdrawal a
			INNER JOIN Reference.TimeZone z ON z.FromDate <= a.MasterTimestamp AND z.ToDate > a.MasterTimestamp AND z.TimeZone='Analysis'
	WHERE	BatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'CashoutDifferential','Start',@RowsProcessed = @@ROWCOUNT

	INSERT	#Collect
	SELECT	CONVERT(binary(16), LegId) Id, LegIdSource IdSource, LegId LegacyLegId, LegIdSource LegacyLegSource, LedgerId, LedgerSource, MasterTransactionTimestamp MasterTimestamp,
			BetType, LegType, BetGroupType, BetSubType, GroupingType, LegNumber, NumberOfLegs, Channel ChannelId, ActionChannel ActionChannelId, PromotionId, PromotionIdSource PromotionSource,
			EventId MarketID, EventIdSource MarketSource, EventID, EventIdSource EventSource, ClassID, ClassIdSource ClassSource, UserID, UserIDSource UserSource, 0 InstrumentID, 'N/A' InstrumentSource,
			CASE InPlay WHEN 'Y' THEN 1 ELSE 0 END HeadlineFeatures,
			InPlay, ClickToCall, CashoutType, IsDoubleDown, IsDoubledDown, IsChaseTheAce, FeatureFlags, WalletId,
			- CASE WHEN a.BetType = 'Exotic' THEN a.BetPayout WHEN @Trued = 'Y' THEN a.LegPayoutTrued ELSE a.LegPayout END Amount,
			TransactionId, TransactionIdSource TransactionSource, BetGroupID GroupId, BetGroupIDSource GroupSource, BetId, BetIdSource BetSource, LegId, LegIdSource LegSource, NULLIF(FreeBetId,0) FreeBetId, NULL ExternalReference, 'N/A' ExternalSource,
			'Bet.Differential.Cashout' TransactionCode, 0 FirstForClient, 1 FirstForTransaction, MappingId, 'B' FunctionalArea, FromDate 
	FROM	Atomic.Bet_New a 
	WHERE	BatchKey = @BatchKey
			AND Action = 'CashoutDif'
			AND PositionNumber = 1 AND GroupingNumber = 1	-- As the Transaction fact is staged at a bet leg grain, just fix the selection to the first position and group for each leg and retrieve leg level stakes and payments (seeabove)
			AND (BetType <> 'Exotic' OR BetId = LegId)	-- Transactions only include one line per exotics, even if the exotic is technically multi-leg - e.g. Quadrella - so only take the first leg of exotics and retrieve bet level stakes and payments (above)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Stage','Start',@RowsProcessed = @@ROWCOUNT

	SELECT	ISNULL(ContractKey, -1) ContractKey, ISNULL(AccountKey, -1) AccountKey, MasterTimestamp, AnalysisTimestamp, ISNULL(DayZoneKey,-1) DayKey, ISNULL(MasterTimeKey,-1) MasterTimeKey, ISNULL(AnalysisTimeKey,-1) AnalysisTimeKey,
			ISNULL(AccountStatusKey,-1) AccountStatusKey, ISNULL(StructureKey,-1) StructureKey, ISNULL(BetTypeKey,-1) BetTypeKey, ISNULL(LegTypeKey,-1) LegTypeKey,
			ISNULL(ChannelKey,-1) ChannelKey, ISNULL(ActionChannelKey,-1) ActionChannelKey, ISNULL(MarketKey,-1) MarketKey, ISNULL(EventKey,-1) EventKey, ISNULL(ClassKey,-1) ClassKey,
			ISNULL(UserKey,-1) UserKey, ISNULL(InstrumentKey,-1) InstrumentKey, ISNULL(InPlayKey,-1) InPlayKey,
			AccountsOpened,
			CASE WHEN FirstForClient = 1 AND DepositsRequestedCount IS NOT NULL THEN TransactionId ELSE NULL END	FirstTimeDepositors,
			DepositsRequested, DepositsCompleted, 
			ISNULL(DepositsRequestedCount,0) DepositsRequestedCount, ISNULL(DepositsCompletedCount,0) DepositsCompletedCount,				-- NOTE deposit counts are chenged to zero (which is incorrect as the count distinct used on this column will always count 1 extra) for consistency with current president and then co-ordinated change to the correct approach
			- WithdrawalsRequested WithdrawalsRequested, - WithdrawalsCompleted WithdrawalsCompleted,										-- NOTE Withdrawal amounts are shifted to negative to reflect current KPI convention
			ISNULL(WithdrawalsRequestedCount,0) WithdrawalsRequestedCount, ISNULL(WithdrawalsCompletedCount,0) WithdrawalsCompletedCount,	-- NOTE withdrawal counts are chenged to zero (which is incorrect as the count distinct used on this column will always count 1 extra) for consistency with current president and then co-ordinated change to the correct approach
			CASE WHEN FirstForClient = 1 AND BetsPlaced IS NOT NULL THEN BetId ELSE NULL END											FirstTimeBettors,
			CASE WHEN FreeBet = 0 AND BetsPlaced IS NOT NULL THEN LedgerId ELSE NULL END												BettorsPlaced,
			CASE WHEN FreeBet = 1 AND BetsPlaced IS NOT NULL THEN LedgerId ELSE NULL END												BonusBettorsPlaced,
			CASE WHEN FreeBet = 0 THEN BetsPlaced ELSE NULL END																			BetsPlaced,
			CASE WHEN FreeBet = 1 THEN BetsPlaced ELSE NULL END																			BonusBetsPlaced,
			CASE WHEN FreeBet = 0 AND BetsPlaced IS NOT NULL THEN GroupId ELSE NULL END													ContractsPlaced,
			CASE WHEN FreeBet = 1 AND BetsPlaced IS NOT NULL THEN GroupId ELSE NULL END													BonusContractsPlaced,
			CASE WHEN FreeBet = 0 AND BetsPlaced IS NOT NULL THEN CONVERT(bigint,LedgerId)*100000000 + DayKey ELSE NULL END				PlayDaysPlaced,
			CASE WHEN FreeBet = 1 AND BetsPlaced IS NOT NULL THEN CONVERT(bigint,LedgerId)*100000000 + DayKey ELSE NULL END				BonusPlayDaysPlaced,
			CASE WHEN FreeBet = 0 AND BetsPlaced IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + FiscalWeekKey ELSE NULL END		PlayFiscalWeeksPlaced,
			CASE WHEN FreeBet = 1 AND BetsPlaced IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + FiscalWeekKey ELSE NULL END		BonusPlayFiscalWeeksPlaced,
			CASE WHEN FreeBet = 0 AND BetsPlaced IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + WeekKey ELSE NULL END				PlayWeeksPlaced,
			CASE WHEN FreeBet = 1 AND BetsPlaced IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + WeekKey ELSE NULL END				BonusPlayWeeksPlaced,
			CASE WHEN FreeBet = 0 AND BetsPlaced IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + FiscalMonthKey ELSE NULL END		PlayFiscalMonthsPlaced,
			CASE WHEN FreeBet = 1 AND BetsPlaced IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + FiscalMonthKey ELSE NULL END		BonusPlayFiscalMonthsPlaced,
			CASE WHEN FreeBet = 0 AND BetsPlaced IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + MonthKey ELSE NULL END				PlayMonthsPlaced,
			CASE WHEN FreeBet = 1 AND BetsPlaced IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + MonthKey ELSE NULL END				BonusPlayMonthsPlaced,
			- CASE WHEN FreeBet = 0 THEN TurnoverPlaced ELSE 0 END																		TurnoverPlaced,		 -- Note TurnoverPlaced amount is shifted negative to reflect current KPI convention
			- CASE WHEN FreeBet = 1 THEN TurnoverPlaced ELSE 0 END																		BonusTurnoverPlaced, -- Note TurnoverPlaced amount is shifted negative to reflect current KPI convention
			- Fees																														Fees,				 -- Note Fees amount is shifted negative to reflect current KPI convention
			CASE WHEN FreeBetId IS NULL AND BetsSettled IS NOT NULL THEN LedgerId ELSE NULL END											BettorsSettled,
			CASE WHEN FreeBetId IS NOT NULL AND BetsSettled IS NOT NULL THEN LedgerId ELSE NULL END										BonusBettorsSettled,
			CASE WHEN FreeBet = 0 THEN BetsSettled ELSE NULL END																		BetsSettled,
			CASE WHEN FreeBet = 1 THEN BetsSettled ELSE NULL END																		BonusBetsSettled,
			CASE WHEN FreeBet = 0 AND BetsSettled IS NOT NULL THEN GroupId ELSE NULL END												ContractsSettled,
			CASE WHEN FreeBet = 1 AND BetsSettled IS NOT NULL THEN GroupId ELSE NULL END												BonusContractsSettled,
			CASE WHEN FreeBet = 0 AND BetsSettled IS NOT NULL THEN CONVERT(bigint,LedgerId)*100000000 + DayKey ELSE NULL END			PlayDaysSettled,
			CASE WHEN FreeBet = 1 AND BetsSettled IS NOT NULL THEN CONVERT(bigint,LedgerId)*100000000 + DayKey ELSE NULL END			BonusPlayDaysSettled,
			CASE WHEN FreeBet = 0 AND BetsSettled IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + FiscalWeekKey ELSE NULL END		PlayFiscalWeeksSettled,
			CASE WHEN FreeBet = 1 AND BetsSettled IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + FiscalWeekKey ELSE NULL END		BonusPlayFiscalWeeksSettled,
			CASE WHEN FreeBet = 0 AND BetsSettled IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + WeekKey ELSE NULL END				PlayWeeksSettled,
			CASE WHEN FreeBet = 1 AND BetsSettled IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + WeekKey ELSE NULL END				BonusPlayWeeksSettled,
			CASE WHEN FreeBet = 0 AND BetsSettled IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + FiscalMonthKey ELSE NULL END		PlayFiscalMonthsSettled,
			CASE WHEN FreeBet = 1 AND BetsSettled IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + FiscalMonthKey ELSE NULL END		BonusPlayFiscalMonthsSettled,
			CASE WHEN FreeBet = 0 AND BetsSettled IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + MonthKey ELSE NULL END			PlayMonthsSettled,
			CASE WHEN FreeBet = 1 AND BetsSettled IS NOT NULL THEN CONVERT(bigint,LedgerId)*1000000 + MonthKey ELSE NULL END			BonusPlayMonthsSettled,
			CASE WHEN FreeBet = 0 THEN TurnoverSettledInitial ELSE 0 END																TurnoverSettled,		-- Looks like an anomoly here - turnover only reports the first turnover bur payout and gross win report each evolution of settlement, so for a bet that is initially settled but then reverts to unsettled for a period it will maintain a turnover but with no gross win
			CASE WHEN FreeBet = 1 THEN TurnoverSettledInitial ELSE 0 END																BonusTurnoverSettled,
			CASE WHEN FreeBet = 0 THEN PayoutInitial + PayoutAdjustment ELSE 0 END														Payout,
			CASE WHEN FreeBet = 1 THEN PayoutInitial + PayoutAdjustment ELSE 0 END														BonusPayout,
			CASE WHEN FreeBet = 0 THEN TurnoverSettledInitial + TurnoverSettledAdjustment - PayoutInitial - PayoutAdjustment ELSE 0 END	GrossWin,
			CASE WHEN FreeBet = 0 THEN TurnoverSettledAdjustment - PayoutAdjustment ELSE 0 END											GrossWinResettledAdjustment,
			CASE WHEN FreeBet = 1 THEN - PayoutInitial - PayoutAdjustment ELSE 0 END													BonusGrossWin,
			CASE WHEN BetsCashedOut IS NOT NULL THEN LedgerId ELSE NULL END																BettorsCashedOut,
			BetsCashedOut,
			Cashout,
			- CashoutDifferential CashoutDifferential, -- Note CashoutDifferential amount is shifted negative to reflect current KPI convention
			Promotions,
			Transfers,
			Adjustments,
			CASE WHEN FreeBet = 0 THEN TurnoverSettledInitial + TurnoverSettledAdjustment ELSE 0 END - PayoutInitial - PayoutAdjustment	NetRevenue,
			CASE WHEN FreeBet = 0 THEN TurnoverSettledAdjustment ELSE 0 END - PayoutAdjustment											NetRevenueAdjustment,
			CASE WHEN FirstForClient = 1 AND RewardAccruals IS NOT NULL THEN BetId ELSE NULL END										FirstTimeAccruers,
			CASE WHEN RewardAccruals IS NOT NULL THEN LedgerId ELSE NULL END															RewardAccruers,
			RewardAccruals,
			RewardAccrualPoints,
			CASE WHEN FirstForClient = 1 AND RewardBonusRedemptions IS NOT NULL THEN BetId ELSE NULL END								FirstTimeBonusRedeemers,
			CASE WHEN RewardBonusRedemptions IS NOT NULL THEN LedgerId ELSE NULL END													RewardBonusRedeemers,
			RewardBonusRedemptions,
			RewardBonusRedeemedPoints,
			RewardBonusRedeemed,
			CASE WHEN FirstForClient = 1 AND RewardVelocityRedemptions IS NOT NULL THEN BetId ELSE NULL END								FirstTimeVelocityRedeemers,
			CASE WHEN RewardVelocityRedemptions IS NOT NULL THEN LedgerId ELSE NULL END													RewardVelocityRedeemers,
			RewardVelocityRedemptions,
			RewardVelocityRedeemedPoints,
			RewardVelocityRedeemed,
			RewardVelocityCost,
			RewardAdjustmentPoints,
			BetbackGrossWin,
			FromDate,
			MappingId
	INTO	#Stage
	FROM	(
	SELECT	c.ContractKey, a.AccountKey, x.MasterTimestamp, DATEADD(minute, + ISNULL(y.OffsetMinutes,0), x.MasterTimestamp) AnalysisTimestamp, z.DayKey DayZoneKey, tm.TimeKey MasterTimeKey, ta.TimeKey AnalysisTimeKey,
			sa.AccountStatusKey, s.StructureKey, MAX(bt.BetTypeKey) BetTypeKey, MAX(l.LegTypeKey) LegTypeKey,
			MAX(ch.ChannelKey) ChannelKey, MAX(ac.ChannelKey) ActionChannelKey, MAX(m.MarketKey) MarketKey, MAX(e.EventKey) EventKey, MAX(cl.ClassKey) ClassKey,
			MAX(u.UserKey) UserKey, MAX(i.InstrumentKey) InstrumentKey, MAX(ip.InPlayKey) InPlayKey,
			d.DayKey, d.FiscalWeekKey, d.WeekKey, d.FiscalMonthKey, d.MonthKey,
			MAX(FirstForClient) FirstForClient,
			MAX(CASE WHEN x.TransactionCode = 'Account.Open' THEN x.LedgerId ELSE NULL END)					AccountsOpened,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Deposit.Request.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Deposit.Decline.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Deposit.Reject.%' THEN x.Amount 
					ELSE 0 
				END)																						DepositsRequested,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Deposit.Complete.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Deposit.Reverse.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Deposit.Reject.%' THEN x.Amount 
					ELSE 0 
				END)																						DepositsCompleted,
			MAX(CASE 
					WHEN x.TransactionCode LIKE 'Deposit.Request.%' AND FirstForTransaction = 1 THEN x.TransactionId 
					WHEN x.TransactionCode LIKE 'Deposit.Decline.%' AND FirstForTransaction = 1 THEN x.TransactionId	-- Incorrect, only included to prove consistency with previous version, then remove
					WHEN x.TransactionCode LIKE 'Deposit.Reject.%' AND FirstForTransaction = 1 THEN x.TransactionId		-- Incorrect, only included to prove consistency with previous version, then remove
					ELSE NULL
				END)																						DepositsRequestedCount,
			MAX(CASE 
					WHEN x.TransactionCode LIKE 'Deposit.Complete.%' AND FirstForTransaction = 1 THEN x.TransactionId 
					WHEN x.TransactionCode LIKE 'Deposit.Reverse.%' AND FirstForTransaction = 1 THEN x.TransactionId	-- Incorrect, only included to prove consistency with previous version, then remove
					WHEN x.TransactionCode LIKE 'Deposit.Reject.%' AND FirstForTransaction = 1 THEN x.TransactionId		-- Incorrect, only included to prove consistency with previous version, then remove
					ELSE NULL 
				END)																						DepositsCompletedCount,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Withdrawal.Request.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Withdrawal.Decline.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Withdrawal.Reject.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Withdrawal.Reverse.%' THEN x.Amount 
					ELSE 0 
				END)																						WithdrawalsRequested,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Withdrawal.Complete.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Withdrawal.Reject.%' THEN x.Amount 
					ELSE 0 
				END)																						WithdrawalsCompleted,
			MAX(CASE 
					WHEN x.TransactionCode LIKE 'Withdrawal.Request.%' AND FirstForTransaction = 1 THEN x.TransactionId 
					WHEN x.TransactionCode LIKE 'Withdrawal.Decline.%' AND FirstForTransaction = 1 THEN x.TransactionId	-- Incorrect, only included to prove consistency with previous version, then remove
					WHEN x.TransactionCode LIKE 'Withdrawal.Reject.%' AND FirstForTransaction = 1 THEN x.TransactionId	-- Incorrect, only included to prove consistency with previous version, then remove
					WHEN x.TransactionCode LIKE 'Withdrawal.Reverse.%' AND FirstForTransaction = 1 THEN x.TransactionId	-- Incorrect, only included to prove consistency with previous version, then remove
					ELSE NULL 
			END)																							WithdrawalsRequestedCount,
			MAX(CASE 
					WHEN x.TransactionCode LIKE 'Withdrawal.Complete.%' AND FirstForTransaction = 1 THEN x.TransactionId 
					WHEN x.TransactionCode LIKE 'Withdrawal.Reject.%' AND FirstForTransaction = 1 THEN x.TransactionId	-- Incorrect, only included to prove consistency with previous version, then remove
					ELSE NULL 
				END)																						WithdrawalsCompletedCount,
			MAX(CASE 
					WHEN x.TransactionCode LIKE 'Bet.Place.%' AND FirstForTransaction = 1 THEN x.BetId 
					ELSE NULL 
				END)																						BetsPlaced,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Bet.Place.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Bet.Cancel.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Fee.Payment.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Fee.Refund.%' THEN x.Amount 
					ELSE 0 
				END)																						TurnoverPlaced,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Fee.Payment.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Fee.Refund.%' THEN x.Amount 
					ELSE 0 
				END)																						Fees,
			MAX(CASE 
					WHEN x.TransactionCode LIKE 'Bet.Settle.%' AND FirstForTransaction = 1 THEN x.BetId 
					ELSE NULL 
				END)																						BetsSettled,
			MAX(CASE 
					WHEN x.TransactionCode LIKE 'Bet.Settle.%' AND FirstForTransaction <> 1 THEN x.BetId 
					ELSE NULL 
				END)																						BetsResettled,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Bet.Settle.%' AND FirstForTransaction = 1 THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Fee.Payment.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Fee.Refund.%' THEN x.Amount -- Not convinced this is correct, but follows current convention - goes hand in hand with observation in the outermost select statement regarding mismatch of approaches on initial and subsequent adjustments to turnover vs payour and gross win
					ELSE 0 
				END)																						TurnoverSettledInitial,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Bet.Settle.%' AND FirstForTransaction <> 1 THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Bet.Unsettle.%' THEN x.Amount 
					ELSE 0 
				END)																						TurnoverSettledAdjustment,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Bet.Payout.%' AND FirstForTransaction = 1 THEN x.Amount 
					ELSE 0 
				END)																						PayoutInitial,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Bet.Payout.%' AND FirstForTransaction <> 1 THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Bet.Reclaim.%' THEN x.Amount 
					ELSE 0 
				END)																						PayoutAdjustment,
			MAX(CASE 
					WHEN x.TransactionCode = 'Bet.Settle.Cashout' AND FirstForTransaction = 1 THEN x.BetId 
					ELSE NULL 
				END)																						BetsCashedOut,
			SUM(CASE 
					WHEN x.TransactionCode = 'Bet.Payout.Cashout' THEN x.Amount 
					ELSE 0 
				END)																						Cashout,
			SUM(CASE 
					WHEN x.TransactionCode = 'Bet.Payout.Cashout' THEN - x.Amount 
					WHEN x.TransactionCode = 'Bet.Differential.Cashout' THEN x.Amount 
					ELSE 0 
				END)																						CashoutDifferential,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Bonus.%' AND WalletId = 'C' THEN x.Amount 
					ELSE 0 
				END)																						Promotions,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Transfer.%' THEN x.Amount 
					ELSE 0 
				END)																						Transfers,
			SUM(CASE 
					WHEN x.TransactionCode LIKE 'Adjustment.%' THEN x.Amount 
					WHEN x.TransactionCode LIKE 'Rebate.%' THEN x.Amount 
					ELSE 0 
				END)																						Adjustments,
			MAX(CASE 
					WHEN x.TransactionCode = 'Bonus.Award.Accrual' AND WalletId = 'L' AND FirstForTransaction = 1 THEN x.BetId 
					ELSE NULL 
				END)																						RewardAccruals,
			SUM(CASE 
					WHEN x.TransactionCode = 'Bonus.Award.Accrual' AND WalletId = 'L' THEN x.Amount 
					ELSE 0 
				END)																						RewardAccrualPoints,
			MAX(CASE 
					WHEN x.TransactionCode = 'Bonus.Redemption.Bonus' AND WalletId = 'L' AND FirstForTransaction = 1 THEN x.TransactionId 
					ELSE NULL 
				END)																						RewardBonusRedemptions,
			SUM(CASE 
					WHEN x.TransactionCode = 'Bonus.Redemption.Bonus' AND WalletId = 'L' THEN x.Amount 
					ELSE 0 
				END)																						RewardBonusRedeemedPoints,
			SUM(CASE 
					WHEN x.TransactionCode = 'Bonus.Redemption.Bonus' AND WalletId = 'B' THEN x.Amount 
					ELSE 0 
				END)																						RewardBonusRedeemed,
			MAX(CASE 
					WHEN x.TransactionCode = 'Bonus.Redemption.Velocity' AND WalletId = 'L' AND FirstForTransaction = 1 THEN x.TransactionId 
					ELSE NULL 
				END)																						RewardVelocityRedemptions,
			SUM(CASE 
					WHEN x.TransactionCode = 'Bonus.Redemption.Velocity' AND WalletId = 'L' THEN x.Amount 
					ELSE 0 
				END)																						RewardVelocityRedeemedPoints,
			SUM(CASE 
					WHEN x.TransactionCode = 'Bonus.Redemption.Velocity' AND WalletId = 'V' THEN x.Amount 
					ELSE 0 
				END)																						RewardVelocityRedeemed,
			SUM(CASE 
					WHEN x.TransactionCode = 'Bonus.Redemption.Velocity' AND WalletId = 'C' THEN x.Amount 
					ELSE 0 
				END)																						RewardVelocityCost,
			SUM(CASE 
					WHEN x.TransactionCode = 'Bonus.Award.Adjustment' AND WalletId = 'L' THEN x.Amount 
					WHEN x.TransactionCode = 'Bonus.Cancel.Adjustment' AND WalletId = 'L' THEN x.Amount 
					ELSE 0 
				END)																						RewardAdjustmentPoints,
			0 BetbackGrossWin,
			x.LedgerId,
			MAX(x.TransactionId) TransactionId,  -- Ultimately, we may with to dispense with the current covention of merging transactions with the same timestamp and reinstate transactionid in the grouping
			x.GroupId,
			x.BetId,
			x.FreeBetId,
			--x.WalletId,
			CASE WHEN x.FreeBetId IS NOT NULL THEN 1 ELSE 0 END FreeBet,
			MAX(x.FromDate)						FromDate,
			MAX(x.MappingId)					MappingId
	FROM	#Collect x
			LEFT JOIN Reference.TimeZone y ON y.Fromdate <= x.MasterTimestamp AND y.ToDate > x.MasterTimestamp AND y.TimeZone = 'Analysis' -- Can negate need for this join by including Master and Analysis Offset minutes in CC.Day dimension below
			LEFT JOIN [$(Dimensional)].Dimension.[Contract] c		ON c.LegID = x.LegacyLegID AND c.LegIdSource = x.LegacyLegSource -- Contract dimension only preserved in the interim until implementation and adoption of id columns in main fact table is complete
			LEFT JOIN [$(Dimensional)].Dimension.Account a			ON a.LedgerId = x.LedgerId AND a.LedgerSource = x.LedgerSource
			LEFT JOIN [$(Dimensional)].CC.[Day] z					ON z.MasterDayText = CONVERT(varchar(11),CONVERT(DATE, x.MasterTimestamp)) and z.FromDate <= x.MasterTimestamp AND z.ToDate > x.MasterTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.[Day] d			ON d.DayKey = z.MasterDayKey
			LEFT JOIN [$(Dimensional)].Dimension.[Time] tm			ON tm.TimeText = CONVERT(char(5),CONVERT(time,x.MasterTimestamp))
			LEFT JOIN [$(Dimensional)].Dimension.[Time] ta			ON ta. TimeText = CONVERT(char(5),CONVERT(time,DATEADD(minute, + ISNULL(y.OffsetMinutes,0), x.MasterTimestamp)))
			LEFT JOIN [$(Dimensional)].Dimension.AccountStatus sa	ON sa.LedgerId = x.LedgerId AND sa.LedgerSource = x.LedgerSource AND sa.FromDate <= x.MasterTimestamp AND sa.ToDate > x.MasterTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.Structure s		ON s.LedgerId = x.LedgerId AND s.LedgerSource = x.LedgerSource AND s.HeadlineFeatures = x.HeadlineFeatures
			LEFT JOIN [$(Dimensional)].Dimension.BetType bt		ON bt.BetType = x.BetType AND bt.LegType = x.LegType AND bt.BetSubType = x.BetSubType AND bt.BetGroupType = x.BetGroupType AND bt.GroupingType = x.GroupingType
			LEFT JOIN [$(Dimensional)].Dimension.LegType l			ON l.LegNumber = x.LegNumber AND l.NumberOfLegs = x.NumberOfLegs
			LEFT JOIN [$(Dimensional)].Dimension.Channel ch		ON ch.ChannelId = x.ChannelId
			LEFT JOIN [$(Dimensional)].Dimension.Channel ac		ON ac.ChannelId = x.ActionChannelId
			LEFT JOIN [$(Dimensional)].Dimension.Market m			ON m.MarketID = x.MarketID AND m.Source = x.MarketSource
			LEFT JOIN [$(Dimensional)].Dimension.[Event] e			ON e.EventID = x.EventId AND e.Source = x.EventSource
			LEFT JOIN [$(Dimensional)].Dimension.[Class] cl		ON cl.ClassID = x.ClassID AND cl.Source = x.ClassSource AND cl.FromDate <= x.MasterTimestamp AND cl.ToDate > x.MasterTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.[User] u			ON u.UserID=x.UserID and x.UserSource = u.Source
			LEFT JOIN [$(Dimensional)].Dimension.Instrument i		ON i.InstrumentID = x.InstrumentID AND i.Source = x.InstrumentSource
			LEFT JOIN [$(Dimensional)].Dimension.InPlay ip			ON ip.ClickToCall = x.ClickToCall AND ip.InPlay = x.InPlay AND ip.CashoutType = x.CashoutType AND ip.IsDoubleDown = x.IsDoubleDown AND ip.IsDoubledDown = x.IsDoubledDown and ip.IsChaseTheAce = x.IsChaseTheAce AND ip.FeatureFlags = x.FeatureFlags
	GROUP BY c.ContractKey, a.AccountKey, x.MasterTimestamp, y.OffsetMinutes, z.DayKey, tm.TimeKey, ta.TimeKey, sa.AccountStatusKey, s.StructureKey, d.DayKey, d.FiscalWeekKey, d.WeekKey, d.FiscalMonthKey, d.MonthKey, x.LedgerId, /*x.TransactionId,*/ x.GroupId, x.BetId, x.FreeBetId--, x.WalletId -- Ultimately, we may with to dispense with the current covention of merging transactions with the same timestamp and reinstate transactionid in the grouping
			) x
--	WHERE	BetsResettled IS NULL OR TurnoverSettledAdjustment <> 0 OR PayoutAdjustment <> 0  -- Exclude resettlements that make no net difference, although ultimately may want to include them if we ever wnat to analyse resettlements
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Fact','Start',@RowsProcessed = @@ROWCOUNT

	SELECT	ContractKey, AccountKey, MasterTimestamp, AnalysisTimestamp, DayKey, MasterTimeKey, AnalysisTimeKey, AccountStatusKey, StructureKey,
			BetTypeKey, LegTypeKey, ChannelKey, ActionChannelKey, MarketKey, EventKey, ClassKey, UserKey, InstrumentKey, InPlayKey,
			AccountsOpened, FirstTimeDepositors, DepositsRequested, DepositsCompleted, DepositsRequestedCount, DepositsCompletedCount, 
			WithdrawalsRequested, WithdrawalsCompleted, WithdrawalsRequestedCount, WithdrawalsCompletedCount,
			FirstTimeBettors, BettorsPlaced, BonusBettorsPlaced, BetsPlaced, BonusBetsPlaced, ContractsPlaced, BonusContractsPlaced,
			PlayDaysPlaced, BonusPlayDaysPlaced, PlayFiscalWeeksPlaced, BonusPlayFiscalWeeksPlaced, PlayWeeksPlaced, BonusPlayWeeksPlaced, 
			PlayFiscalMonthsPlaced, BonusPlayFiscalMonthsPlaced, PlayMonthsPlaced, BonusPlayMonthsPlaced,
			TurnoverPlaced, BonusTurnoverPlaced, Fees, BettorsSettled, BonusBettorsSettled, BetsSettled, BonusBetsSettled, ContractsSettled, BonusContractsSettled,
			PlayDaysSettled, BonusPlayDaysSettled, PlayFiscalWeeksSettled, BonusPlayFiscalWeeksSettled, PlayWeeksSettled, BonusPlayWeeksSettled, 
			PlayFiscalMonthsSettled, BonusPlayFiscalMonthsSettled, PlayMonthsSettled, BonusPlayMonthsSettled, 
			TurnoverSettled, BonusTurnoverSettled, Payout, BonusPayout, GrossWin, GrossWinResettledAdjustment, BonusGrossWin, BettorsCashedOut, BetsCashedOut, Cashout, CashoutDifferential,
			Promotions, Transfers, Adjustments, NetRevenue, NetRevenueAdjustment,
			FirstTimeAccruers, RewardAccruals, RewardAccruers, RewardAccrualPoints,
			FirstTimeBonusRedeemers, RewardBonusRedemptions, RewardBonusRedeemers, RewardBonusRedeemedPoints, RewardBonusRedeemed,
			FirstTimeVelocityRedeemers, RewardVelocityRedemptions, RewardVelocityRedeemers, RewardVelocityRedeemedPoints, RewardVelocityRedeemed, RewardVelocityCost,
			RewardAdjustmentPoints, 
			BetbackGrossWin,
			FromDate, MappingId
	INTO	#Fact
	FROM	[$(Dimensional)].CC.[KPI]
	WHERE	CreatedBatchKey = @BatchKey
and AccountsOpened is null
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start',@RowsProcessed = @@ROWCOUNT

	SELECT  *, CASE WHEN COUNT(*) OVER (PARTITION BY DayKey, ContractKey, AccountKey, MasterTimestamp) = 2 THEN 'U' WHEN src = 'S' THEN 'I' WHEN src = 'F' THEN 'D' ELSE 'X' END Upsert
	INTO	#Differences
	FROM	(	SELECT	ContractKey, AccountKey, MasterTimestamp, MAX(AnalysisTimestamp) AnalysisTimestamp, DayKey, MasterTimeKey, MAX(AnalysisTimeKey) AnalysisTimeKey, AccountStatusKey, StructureKey, -- AnalysisTimestamp and AnalysisTimekey have only recently been added to the fact, do not force updates purely because of the differences in these columns at this stage
						BetTypeKey, LegTypeKey, ChannelKey, ActionChannelKey, MarketKey, EventKey, ClassKey, UserKey, InstrumentKey, InPlayKey,
						AccountsOpened, FirstTimeDepositors, DepositsRequested, DepositsCompleted, DepositsRequestedCount, DepositsCompletedCount, 
						WithdrawalsRequested, WithdrawalsCompleted, WithdrawalsRequestedCount, WithdrawalsCompletedCount,
						FirstTimeBettors, BettorsPlaced, BonusBettorsPlaced, BetsPlaced, BonusBetsPlaced, ContractsPlaced, BonusContractsPlaced,
						PlayDaysPlaced, BonusPlayDaysPlaced, PlayFiscalWeeksPlaced, BonusPlayFiscalWeeksPlaced, PlayWeeksPlaced, BonusPlayWeeksPlaced, 
						PlayFiscalMonthsPlaced, BonusPlayFiscalMonthsPlaced, PlayMonthsPlaced, BonusPlayMonthsPlaced,
						TurnoverPlaced, BonusTurnoverPlaced, Fees, BettorsSettled, BonusBettorsSettled, BetsSettled, BonusBetsSettled, ContractsSettled, BonusContractsSettled,
						PlayDaysSettled, BonusPlayDaysSettled, PlayFiscalWeeksSettled, BonusPlayFiscalWeeksSettled, PlayWeeksSettled, BonusPlayWeeksSettled, 
						PlayFiscalMonthsSettled, BonusPlayFiscalMonthsSettled, PlayMonthsSettled, BonusPlayMonthsSettled, 
						TurnoverSettled, BonusTurnoverSettled, Payout, BonusPayout, GrossWin, GrossWinResettledAdjustment, BonusGrossWin, BettorsCashedOut, BetsCashedOut, Cashout, CashoutDifferential,
						Promotions, Transfers, Adjustments, NetRevenue, NetRevenueAdjustment,
						FirstTimeAccruers, RewardAccruals, RewardAccruers, RewardAccrualPoints,
						FirstTimeBonusRedeemers, RewardBonusRedemptions, RewardBonusRedeemers, RewardBonusRedeemedPoints, RewardBonusRedeemed,
						FirstTimeVelocityRedeemers, RewardVelocityRedemptions, RewardVelocityRedeemers, RewardVelocityRedeemedPoints, RewardVelocityRedeemed, RewardVelocityCost,
						RewardAdjustmentPoints, 
						BetbackGrossWin,
						MAX(FromDate) FromDate, MAX(MappingId) MappingId, MAX(src) src
				FROM	(	SELECT *, 'S' src FROM #Stage
							UNION ALL
							SELECT *, 'F' src FROM #Fact
						) x
				GROUP BY ContractKey, AccountKey, MasterTimestamp, /*AnalysisTimestamp,*/ DayKey, MasterTimeKey, /*AnalysisTimeKey,*/ AccountStatusKey, StructureKey,
						BetTypeKey, LegTypeKey, ChannelKey, ActionChannelKey, MarketKey, EventKey, ClassKey, UserKey, InstrumentKey, InPlayKey,
						AccountsOpened, FirstTimeDepositors, DepositsRequested, DepositsCompleted, DepositsRequestedCount, DepositsCompletedCount, 
						WithdrawalsRequested, WithdrawalsCompleted, WithdrawalsRequestedCount, WithdrawalsCompletedCount,
						FirstTimeBettors, BettorsPlaced, BonusBettorsPlaced, BetsPlaced, BonusBetsPlaced, ContractsPlaced, BonusContractsPlaced,
						PlayDaysPlaced, BonusPlayDaysPlaced, PlayFiscalWeeksPlaced, BonusPlayFiscalWeeksPlaced, PlayWeeksPlaced, BonusPlayWeeksPlaced, 
						PlayFiscalMonthsPlaced, BonusPlayFiscalMonthsPlaced, PlayMonthsPlaced, BonusPlayMonthsPlaced,
						TurnoverPlaced, BonusTurnoverPlaced, Fees, BettorsSettled, BonusBettorsSettled, BetsSettled, BonusBetsSettled, ContractsSettled, BonusContractsSettled,
						PlayDaysSettled, BonusPlayDaysSettled, PlayFiscalWeeksSettled, BonusPlayFiscalWeeksSettled, PlayWeeksSettled, BonusPlayWeeksSettled, 
						PlayFiscalMonthsSettled, BonusPlayFiscalMonthsSettled, PlayMonthsSettled, BonusPlayMonthsSettled, 
						TurnoverSettled, BonusTurnoverSettled, Payout, BonusPayout, GrossWin, GrossWinResettledAdjustment, BonusGrossWin, BettorsCashedOut, BetsCashedOut, Cashout, CashoutDifferential,
						Promotions, Transfers, Adjustments, NetRevenue, NetRevenueAdjustment,
						FirstTimeAccruers, RewardAccruals, RewardAccruers, RewardAccrualPoints,
						FirstTimeBonusRedeemers, RewardBonusRedemptions, RewardBonusRedeemers, RewardBonusRedeemedPoints, RewardBonusRedeemed,
						FirstTimeVelocityRedeemers, RewardVelocityRedemptions, RewardVelocityRedeemers, RewardVelocityRedeemedPoints, RewardVelocityRedeemed, RewardVelocityCost,
						RewardAdjustmentPoints, 
						BetbackGrossWin--,
						--FromDate
				HAVING	COUNT(*) = 1
			) x
	OPTION (RECOMPILE)

	IF @DryRun = 1 
		SELECT	TOP 10000 
				CASE WHEN Upsert = 'U' AND src = 'F' THEN 'U-' WHEN Upsert = 'U' AND src = 'S' THEN 'U+' ELSE Upsert END act, 
				* 
		FROM	#Differences 
		ORDER BY DayKey, ContractKey, AccountKey, MasterTimestamp, src desc

	SELECT	* INTO #Prepare FROM #Differences WHERE	Upsert <> 'U' OR src <> 'F' -- Discard the 'before' version record of Update

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start',@RowsProcessed = @@ROWCOUNT

	IF @DryRun = 0 
		BEGIN
			UPDATE	d 
				SET	AccountStatusKey = s.AccountStatusKey,
					StructureKey = s.StructureKey,
					BetTypeKey = s.BetTypeKey,
					LegTypeKey = s.LegTypeKey,
					ChannelKey = s.ChannelKey,
					ActionChannelKey = s.ActionChannelKey,
					MarketKey = s.MarketKey,
					EventKey = s.EventKey,
					ClassKey = s.ClassKey,
					UserKey = s.UserKey,
					InstrumentKey = s.InstrumentKey,
					InPlayKey = s.InPlayKey,
					AccountsOpened = s.AccountsOpened,
					FirstTimeDepositors = s.FirstTimeDepositors,
					DepositsRequested = s.DepositsRequested,
					DepositsCompleted = s.DepositsCompleted,
					DepositsRequestedCount = s.DepositsRequestedCount,
					DepositsCompletedCount = s.DepositsCompletedCount,
					WithdrawalsRequested = s.WithdrawalsRequested,
					WithdrawalsCompleted = s.WithdrawalsCompleted,
					WithdrawalsRequestedCount = s.WithdrawalsRequestedCount,
					WithdrawalsCompletedCount = s.WithdrawalsCompletedCount,
					FirstTimeBettors = s.FirstTimeBettors,
					BettorsPlaced = s.BettorsPlaced,
					BonusBettorsPlaced = s.BonusBettorsPlaced,
					BetsPlaced = s.BetsPlaced,
					BonusBetsPlaced = s.BonusBetsPlaced,
					ContractsPlaced = s.ContractsPlaced,
					BonusContractsPlaced = s.BonusContractsPlaced,
					PlayDaysPlaced = s.PlayDaysPlaced,
					BonusPlayDaysPlaced = s.BonusPlayDaysPlaced,
					PlayFiscalWeeksPlaced = s.PlayFiscalWeeksPlaced,
					BonusPlayFiscalWeeksPlaced = s.BonusPlayFiscalWeeksPlaced,
					PlayWeeksPlaced = s.PlayWeeksPlaced,
					BonusPlayWeeksPlaced = s.BonusPlayWeeksPlaced,
					PlayFiscalMonthsPlaced = s.PlayFiscalMonthsPlaced,
					BonusPlayFiscalMonthsPlaced = s.BonusPlayFiscalMonthsPlaced,
					PlayMonthsPlaced = s.PlayMonthsPlaced,
					BonusPlayMonthsPlaced = s.BonusPlayMonthsPlaced,
					TurnoverPlaced = s.TurnoverPlaced,
					BonusTurnoverPlaced = s.BonusTurnoverPlaced,
					Fees = s.Fees,
					BettorsSettled = s.BettorsSettled,
					BonusBettorsSettled = s.BonusBettorsSettled,
					BetsSettled = s.BetsSettled,
					BonusBetsSettled = s.BonusBetsSettled,
					ContractsSettled = s.ContractsSettled,
					BonusContractsSettled = s.BonusContractsSettled,
					PlayDaysSettled = s.PlayDaysSettled,
					BonusPlayDaysSettled = s.BonusPlayDaysSettled,
					PlayFiscalWeeksSettled = s.PlayFiscalWeeksSettled,
					BonusPlayFiscalWeeksSettled = s.BonusPlayFiscalWeeksSettled,
					PlayWeeksSettled = s.PlayWeeksSettled,
					BonusPlayWeeksSettled = s.BonusPlayWeeksSettled,
					PlayFiscalMonthsSettled = s.PlayFiscalMonthsSettled,
					BonusPlayFiscalMonthsSettled = s.BonusPlayFiscalMonthsSettled,
					PlayMonthsSettled = s.PlayMonthsSettled,
					BonusPlayMonthsSettled = s.BonusPlayMonthsSettled,
					TurnoverSettled = s.TurnoverSettled,
					BonusTurnoverSettled = s.BonusTurnoverSettled,
					Payout = s.Payout,
					BonusPayout = s.BonusPayout,
					GrossWin = s.GrossWin,
					GrossWinResettledAdjustment = s.GrossWinResettledAdjustment,
					BonusGrossWin = s.BonusGrossWin,
					BettorsCashedOut = s.BettorsCashedOut,
					BetsCashedOut = s.BetsCashedOut,
					Cashout = s.Cashout,
					CashoutDifferential = s.CashoutDifferential,
					Promotions = s.Promotions,
					Transfers = s.Transfers,
					Adjustments = s.Adjustments,
					NetRevenue = s.NetRevenue,
					NetRevenueAdjustment = s.NetRevenueAdjustment,
					FirstTimeAccruers = s.FirstTimeAccruers,
					RewardAccruals = s.RewardAccruals,
					RewardAccruers = s.RewardAccruers,
					RewardAccrualPoints = s.RewardAccrualPoints,
					FirstTimeBonusRedeemers = s.FirstTimeBonusRedeemers,
					RewardBonusRedemptions = s.RewardBonusRedemptions,
					RewardBonusRedeemers = s.RewardBonusRedeemers,
					RewardBonusRedeemedPoints = s.RewardBonusRedeemedPoints,
					RewardBonusRedeemed = s.RewardBonusRedeemed,
					FirstTimeVelocityRedeemers = s.FirstTimeVelocityRedeemers,
					RewardVelocityRedemptions = s.RewardVelocityRedemptions,
					RewardVelocityRedeemers = s.RewardVelocityRedeemers,
					RewardVelocityRedeemedPoints = s.RewardVelocityRedeemedPoints,
					RewardVelocityRedeemed = s.RewardVelocityRedeemed,
					RewardVelocityCost = s.RewardVelocityCost,
					RewardAdjustmentPoints = s.RewardAdjustmentPoints,
					BetbackGrossWin = s.BetbackGrossWin
			FROM	[$(Dimensional)].CC.KPI d
					INNER JOIN #Prepare s  ON s.DayKey = d.DayKey AND s.ContractKey = d.ContractKey AND s.AccountKey = d.AccountKey AND s.MasterTimestamp = d.MasterTimestamp
			WHERE	s.Upsert = 'U'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'U'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start',@RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			INSERT [$(Dimensional)].CC.[KPI]
				(	ContractKey, AccountKey, MasterTimestamp, AnalysisTimestamp, DayKey, MasterTimeKey, AnalysisTimeKey, AccountStatusKey, StructureKey,
					BetTypeKey, LegTypeKey, ChannelKey, ActionChannelKey, CampaignKey, MarketKey, EventKey, ClassKey, UserKey, InstrumentKey, InPlayKey,
					AccountsOpened, FirstTimeDepositors, DepositsRequested, DepositsCompleted, DepositsRequestedCount, DepositsCompletedCount, 
					WithdrawalsRequested, WithdrawalsCompleted, WithdrawalsRequestedCount, WithdrawalsCompletedCount,
					FirstTimeBettors, BettorsPlaced, BonusBettorsPlaced, BetsPlaced, BonusBetsPlaced, ContractsPlaced, BonusContractsPlaced,
					PlayDaysPlaced, BonusPlayDaysPlaced, PlayFiscalWeeksPlaced, BonusPlayFiscalWeeksPlaced, PlayWeeksPlaced, BonusPlayWeeksPlaced, 
					PlayFiscalMonthsPlaced, BonusPlayFiscalMonthsPlaced, PlayMonthsPlaced, BonusPlayMonthsPlaced,
					TurnoverPlaced, BonusTurnoverPlaced, Fees,
					BettorsSettled, BonusBettorsSettled, BetsSettled, BonusBetsSettled, ContractsSettled, BonusContractsSettled,
					PlayDaysSettled, BonusPlayDaysSettled, PlayFiscalWeeksSettled, BonusPlayFiscalWeeksSettled, PlayWeeksSettled, BonusPlayWeeksSettled,
					PlayFiscalMonthsSettled, BonusPlayFiscalMonthsSettled, PlayMonthsSettled, BonusPlayMonthsSettled,
					TurnoverSettled, BonusTurnoverSettled, Payout, BonusPayout, GrossWin, GrossWinResettledAdjustment, BonusGrossWin,
					BettorsCashedOut, BetsCashedOut, Cashout, CashoutDifferential,
					Promotions, Transfers, Adjustments, NetRevenue, NetRevenueAdjustment,
					FirstTimeAccruers, RewardAccruals, RewardAccruers, RewardAccrualPoints,
					FirstTimeBonusRedeemers, RewardBonusRedemptions, RewardBonusRedeemers, RewardBonusRedeemedPoints, RewardBonusRedeemed,
					FirstTimeVelocityRedeemers, RewardVelocityRedemptions, RewardVelocityRedeemers, RewardVelocityRedeemedPoints, RewardVelocityRedeemed, RewardVelocityCost,
					RewardAdjustmentPoints,
					BetbackGrossWin,
					FromDate, MappingId, CreatedDate, CreatedBatchKey, CreatedBy
				)
			SELECT	s.ContractKey, s.AccountKey, s.MasterTimestamp, s.AnalysisTimestamp, s.DayKey, s.MasterTimeKey, s.AnalysisTimeKey, s.AccountStatusKey, s.StructureKey,
					s.BetTypeKey, s.LegTypeKey, s.ChannelKey, s.ActionChannelKey, -1 CampaignKey, s.MarketKey, s.EventKey, s.ClassKey, s.UserKey, s.InstrumentKey, s.InPlayKey,
					s.AccountsOpened, s.FirstTimeDepositors, s.DepositsRequested, s.DepositsCompleted, s.DepositsRequestedCount, s.DepositsCompletedCount,
					s.WithdrawalsRequested, s.WithdrawalsCompleted, s.WithdrawalsRequestedCount, s.WithdrawalsCompletedCount,
					s.FirstTimeBettors, s.BettorsPlaced, s.BonusBettorsPlaced, s.BetsPlaced, s.BonusBetsPlaced, s.ContractsPlaced, s.BonusContractsPlaced,
					s.PlayDaysPlaced, s.BonusPlayDaysPlaced, s.PlayFiscalWeeksPlaced, s.BonusPlayFiscalWeeksPlaced, s.PlayWeeksPlaced, s.BonusPlayWeeksPlaced,
					s.PlayFiscalMonthsPlaced, s.BonusPlayFiscalMonthsPlaced, s.PlayMonthsPlaced, s.BonusPlayMonthsPlaced,
					s.TurnoverPlaced, s.BonusTurnoverPlaced, s.Fees,
					s.BettorsSettled, s.BonusBettorsSettled, s.BetsSettled, s.BonusBetsSettled, s.ContractsSettled, s.BonusContractsSettled,
					s.PlayDaysSettled, s.BonusPlayDaysSettled, s.PlayFiscalWeeksSettled, s.BonusPlayFiscalWeeksSettled, s.PlayWeeksSettled, s.BonusPlayWeeksSettled,
					s.PlayFiscalMonthsSettled, s.BonusPlayFiscalMonthsSettled, s.PlayMonthsSettled, s.BonusPlayMonthsSettled,
					s.TurnoverSettled, s.BonusTurnoverSettled, s.Payout, s.BonusPayout, s.GrossWin, s.GrossWinResettledAdjustment, s.BonusGrossWin,
					s.BettorsCashedOut, s.BetsCashedOut, s.Cashout, s.CashoutDifferential,
					s.Promotions, s.Transfers, s.Adjustments, s.NetRevenue, s.NetRevenueAdjustment,
					s.FirstTimeAccruers, s.RewardAccruals, s.RewardAccruers, s.RewardAccrualPoints,
					s.FirstTimeBonusRedeemers, s.RewardBonusRedemptions, s.RewardBonusRedeemers, s.RewardBonusRedeemedPoints, s.RewardBonusRedeemed,
					s.FirstTimeVelocityRedeemers, s.RewardVelocityRedemptions, s.RewardVelocityRedeemers, s.RewardVelocityRedeemedPoints, s.RewardVelocityRedeemed, s.RewardVelocityCost,
					s.RewardAdjustmentPoints,
					s.BetbackGrossWin,
					s.FromDate, s.MappingId, SYSDATETIME() CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy
			FROM	#Prepare s
			WHERE	s.Upsert = 'I'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'I'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Delete','Start',@RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			DELETE	d
			FROM	[$(Dimensional)].CC.[KPI] d
					INNER JOIN #Prepare p ON p.DayKey = d.DayKey AND p.ContractKey = d.ContractKey AND p.AccountKey = d.AccountKey AND p.MasterTimestamp = d.MasterTimestamp
			WHERE	p.Upsert = 'D'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'D'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success',@RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
