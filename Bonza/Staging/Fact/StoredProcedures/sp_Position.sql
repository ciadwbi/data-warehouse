﻿CREATE PROC	 [Fact].[sp_Position]
(
	@BatchKey int,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY
	
	EXEC [Control].[Sp_Log] @BatchKey,@Me,'Stage','Start'

	SELECT  ISNULL(a.AccountKey, -1) AccountKey,
			ISNULL(a1.AccountStatusKey, -1) AccountStatusKey,
			ISNULL(s.StructureKey, -1) StructureKey,
			ISNULL(d.DayKey, -1) DayKey,
			CASE WHEN FirstAccrualDate IS NULL THEN 0 ELSE ISNULL(fa.DayKey, -1) END FirstAccrualDayKey,
			FirstAccrualBetId,
			CASE WHEN FirstBonusRedemptionDate IS NULL THEN 0 ELSE ISNULL(fb.DayKey, -1) END FirstBonusRedemptionDayKey,
			CASE WHEN FirstVelocityRedemptionDate IS NULL THEN 0 ELSE ISNULL(fv.DayKey, -1) END FirstVelocityRedemptionDayKey,
			CASE WHEN LastAccrualDate IS NULL THEN 0 ELSE ISNULL(la.DayKey, -1) END LastAccrualDayKey,
			LastAccrualBetId,
			CASE WHEN LastBonusRedemptionDate IS NULL THEN 0 ELSE ISNULL(lb.DayKey, -1) END LastBonusRedemptionDayKey,
			CASE WHEN LastVelocityRedemptionDate IS NULL THEN 0 ELSE ISNULL(lv.DayKey, -1) END LastVelocityRedemptionDayKey,
			PointsBalance,
			LifetimeAccrual + LifetimeRewardCredit - (LifetimeBonusRedemption + LifetimeVelocityRedemption + LifetimeRewardDebit) LifetimePoints,
			AccrualBalance,
			NumberOfAccruals,
			LifetimeAccrual,
			NumberOfBonusRedemptions,
			LifetimeBonusRedemption,
			NumberOfVelocityRedemptions,
			LifetimeVelocityRedemption,
			LifetimeRewardCredit,
			LifetimeRewardDebit
	INTO	#Stage
	FROM	Atomic.Position x
			LEFT JOIN [$(Dimensional)].Dimension.Account a			ON a.LedgerID = x.LedgerID AND a.LedgerSource = x.LedgerSource
			LEFT JOIN [$(Dimensional)].Dimension.AccountStatus a1	ON a1.LedgerID = x.LedgerID AND a1.LedgerSource= x.LedgerSource AND a1.FromDate <= x.FromDate AND a1.ToDate > x.FromDate
			LEFT JOIN [$(Dimensional)].Dimension.Structure s		ON s.LedgerId = x.LedgerId AND s.LedgerSource = x.LedgerSource AND s.HeadlineFeatures = 0
			LEFT JOIN [$(Dimensional)].cc.Day d						ON d.FromDate = x.FromDate
			LEFT JOIN [$(Dimensional)].cc.Day fa					ON fa.MasterDayDate = CONVERT(date, x.FirstAccrualDate) AND fa.FromDate <= x.FirstAccrualDate AND fa.ToDate > x.FirstAccrualDate
			LEFT JOIN [$(Dimensional)].cc.Day fb					ON fb.MasterDayDate = CONVERT(date, x.FirstBonusRedemptionDate) AND fb.FromDate <= x.FirstBonusRedemptionDate AND fb.ToDate > x.FirstBonusRedemptionDate
			LEFT JOIN [$(Dimensional)].cc.Day fv					ON fv.MasterDayDate = CONVERT(date, x.FirstVelocityRedemptionDate) AND fv.FromDate <= x.FirstVelocityRedemptionDate AND fv.ToDate > x.FirstVelocityRedemptionDate
			LEFT JOIN [$(Dimensional)].cc.Day la					ON la.MasterDayDate = CONVERT(date, x.LastAccrualDate) AND la.FromDate <= x.LastAccrualDate AND la.ToDate > x.LastAccrualDate
			LEFT JOIN [$(Dimensional)].cc.Day lb					ON lb.MasterDayDate = CONVERT(date, x.LastBonusRedemptionDate) AND lb.FromDate <= x.LastBonusRedemptionDate AND lb.ToDate > x.LastBonusRedemptionDate
			LEFT JOIN [$(Dimensional)].cc.Day lv					ON lv.MasterDayDate = CONVERT(date, x.LastVelocityRedemptionDate) AND lv.FromDate <= x.LastVelocityRedemptionDate AND lv.ToDate > x.LastVelocityRedemptionDate
	WHERE	BatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	s.*, 
			CASE 
				WHEN d.AccountKey IS NULL THEN 'I' 
				WHEN d.AccountStatusKey <> s.AccountStatusKey THEN 'U' 
				WHEN d.StructureKey <> s.StructureKey THEN 'U' 
				WHEN d.FirstAccrualDayKey <> s.FirstAccrualDayKey THEN 'U' 
				WHEN d.FirstAccrualBetId <> s.FirstAccrualBetId THEN 'U'
				WHEN d.FirstBonusRedemptionDayKey <> s.FirstBonusRedemptionDayKey THEN 'U'
				WHEN d.FirstVelocityRedemptionDayKey <> s.FirstVelocityRedemptionDayKey THEN 'U'
				WHEN d.LastAccrualDayKey <> s.LastAccrualDayKey THEN 'U'
				WHEN d.LastAccrualBetId <> s.LastAccrualBetId THEN 'U'
				WHEN d.LastBonusRedemptionDayKey <> s.LastBonusRedemptionDayKey THEN 'U'
				WHEN d.LastVelocityRedemptionDayKey <> s.LastVelocityRedemptionDayKey THEN 'U'
				WHEN d.PointsBalance <> s.PointsBalance THEN 'U'
				WHEN d.LifetimePoints <> s.LifetimePoints THEN 'U'
				WHEN d.AccrualBalance <> s.AccrualBalance THEN 'U'
				WHEN d.NumberOfAccruals <> s.NumberOfAccruals THEN 'U'
				WHEN d.LifetimeAccrual <> s.LifetimeAccrual THEN 'U'
				WHEN d.NumberOfBonusRedemptions <> s.NumberOfBonusRedemptions THEN 'U'
				WHEN d.LifetimeBonusRedemption <> s.LifetimeBonusRedemption THEN 'U'
				WHEN d.NumberOfVelocityRedemptions <> s.NumberOfVelocityRedemptions THEN 'U'
				WHEN d.LifetimeVelocityRedemption <> s.LifetimeVelocityRedemption THEN 'U'
				WHEN d.LifetimeRewardCredit <> s.LifetimeRewardCredit THEN 'U'
				WHEN d.LifetimeRewardDebit <> s.LifetimeRewardDebit THEN 'U'
				ELSE 'X' 
			END Upsert
	INTO	#Position
	FROM	#Stage s
			LEFT JOIN [$(Dimensional)].CC.Position d ON d.AccountKey = s.AccountKey AND d.DayKey = s.DayKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	d
	SET		AccountStatusKey = s.AccountStatusKey,
			StructureKey = s.StructureKey,
			FirstAccrualDayKey = s.FirstAccrualDayKey,
			FirstAccrualBetId = s.FirstAccrualBetId,
			FirstBonusRedemptionDayKey = s.FirstBonusRedemptionDayKey,
			FirstVelocityRedemptionDayKey = s.FirstVelocityRedemptionDayKey,
			LastAccrualDayKey = s.LastAccrualDayKey,
			LastAccrualBetId = s.LastAccrualBetId,
			LastBonusRedemptionDayKey = s.LastBonusRedemptionDayKey,
			LastVelocityRedemptionDayKey = s.LastVelocityRedemptionDayKey,
			PointsBalance = s.PointsBalance,
			LifetimePoints = s.LifetimePoints,
			AccrualBalance = s.AccrualBalance,
			NumberOfAccruals = s.NumberOfAccruals,
			LifetimeAccrual = s.LifetimeAccrual,
			NumberOfBonusRedemptions = s.NumberOfBonusRedemptions,
			LifetimeBonusRedemption = s.LifetimeBonusRedemption,
			NumberOfVelocityRedemptions = s.NumberOfVelocityRedemptions,
			LifetimeVelocityRedemption = s.LifetimeVelocityRedemption,
			LifetimeRewardCredit = s.LifetimeRewardCredit,
			LifetimeRewardDebit = s.LifetimeRewardDebit,
			ModifiedDate = CURRENT_TIMESTAMP,
			ModifiedBatchKey = @BatchKey,
			ModifiedBy = @Me
	FROM	[$(Dimensional)].cc.Position d
			INNER JOIN #Position s on s.AccountKey = d.AccountKey AND s.DayKey = d.DayKey
	WHERE	Upsert = 'U'
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @RowsProcessed = @RowCount

	INSERT [$(Dimensional)].CC.Position
		(	AccountKey, AccountStatusKey, StructureKey, DayKey, 
			FirstAccrualDayKey, FirstAccrualBetId, FirstBonusRedemptionDayKey, FirstVelocityRedemptionDayKey,
			LastAccrualDayKey, LastAccrualBetId, LastBonusRedemptionDayKey, LastVelocityRedemptionDayKey,
			PointsBalance, LifetimePoints, AccrualBalance, NumberOfAccruals, LifetimeAccrual,
			NumberOfBonusRedemptions, LifetimeBonusRedemption, NumberOfVelocityRedemptions, LifetimeVelocityRedemption, 
			LifetimeRewardCredit, LifetimeRewardDebit,
			CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy
		)
	SELECT	AccountKey, AccountStatusKey, StructureKey, DayKey, 
			FirstAccrualDayKey, FirstAccrualBetId, FirstBonusRedemptionDayKey, FirstVelocityRedemptionDayKey,
			LastAccrualDayKey, LastAccrualBetId, LastBonusRedemptionDayKey, LastVelocityRedemptionDayKey,
			PointsBalance, LifetimePoints, AccrualBalance, NumberOfAccruals, LifetimeAccrual,
			NumberOfBonusRedemptions, LifetimeBonusRedemption, NumberOfVelocityRedemptions, LifetimeVelocityRedemption, 
			LifetimeRewardCredit, LifetimeRewardDebit, 
			CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy
	FROM	#Position 
	WHERE	Upsert = 'I'
	OPTION (RECOMPILE)
	
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

