CREATE PROC [Fact].[sp_Cashflow1]
(
	@BatchKey		int,
	@MappingId		smallint = NULL,
	@DryRun			bit = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount int

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Legacy','Start'

	SELECT  TransactionId,
			TransactionIdSource TransactionSource,
			TransactionTypeID,
			TransactionStatusID,
			TransactionMethodID,
			TransactionDirection,
			BalanceTypeID,
			LegId,
			LegIDSource LegSource,
			LedgerID,
			LedgerSource,
			WalletId,
			PromotionId,
			PromotionIdSource PromotionSource,
			EventId,
			EventIdSource EventSource,
			CONVERT(varchar(255),NULL) SourceEvent, -- Placeholders for subsequent lottery transactions which do not have EventIds, only product and draw date
			CONVERT(datetime2(3),NULL) SourceEventDate,
			MarketId,
			MarketIdSource MarketSource,
			MasterDayText,
			CONVERT(datetime2(3),MasterTransactionTimestamp) MasterTransactionTimestamp,
			MasterTimeText,
			ISNULL(DATEADD(MINUTE, z.OffsetMinutes, CONVERT(datetime2(3),MasterTransactionTimestamp)),'1900-01-01') AnalysisTransactionTimestamp,
			CONVERT(varchar(5),CONVERT(time,DATEADD(MINUTE, z.OffsetMinutes, MasterTransactionTimestamp))) AnalysisTimeText,
			CampaignId,
			Channel ChannelId,
			ActionChannel ActionChannelId,
			PriceTypeId,
			PriceTypeSource,
			BetTypeName BetType,
			LegBetTypeName LegType,
			BetGroupType,
			BetSubTypeName BetSubType,
			GroupingType,
			ClassId,
			ClassIdSource ClassSource,
			UserId,
			UserIdSource UserSource,
			InstrumentId,
			InstrumentIdSource InstrumentSource,
			CASE TransactionIdSource WHEN 'IBT' THEN TransactionId ELSE 0 END NoteId,
			CASE TransactionIdSource WHEN 'IBT' THEN 'ITN' ELSE 'N/A' END NoteSource,
			NumberOfLegs,
			LegNumber,
			CASE InPlay WHEN 'Y' THEN 1 ELSE 0 END HeadlineFeatures,
			InPlay,
			ClickToCall,
			CashoutType,
			IsDoubleDown,
			IsDoubledDown,
			IsChaseTheAce,
			FeatureFlags,
			TransactedAmount,
			s.FromDate,
			CONVERT(int,MappingId) MappingId
	INTO	#Collect
	FROM	Stage.[Transaction] s
			LEFT JOIN Reference.TimeZone z ON z.FromDate <= CONVERT(datetime2(3),MasterTransactionTimestamp) AND z.ToDate > CONVERT(datetime2(3),MasterTransactionTimestamp) AND z.TimeZone='Analysis'
	WHERE	ExistsInDim = 0
			AND BatchKey = @BatchKey
			AND (MappingId = @MappingId OR @MappingId IS NULL)
			AND s.TransactionTypeID NOT IN ('WI') -- Withdrawals moved to their own section below based on dedicated Atomic table
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Lottery','Start',@RowsProcessed = @@ROWCOUNT

	INSERT	#Collect
	SELECT	a.TransactionId,
			a.TransactionSource,
			x.TransactionTypeID,
			x.TransactionStatusID,
			x.TransactionMethodID,
			x.TransactionDirection,
			x.BalanceTypeID,
			a.TicketId LegId,
			a.TicketSource LegSource,
			LedgerID,
			LedgerSource,
			'C' WalletId,
			0 PromotionId,
			'N/A' PromotionSource,
			a.TicketId EventId,
			'LTE' EventSource,
			a.Product SourceEvent, -- Provided because by default Lottery has no EventId - have to match on product and draw date (unless draw date was null and one-off dummy eventid has been created using TicketId)
			a.DrawDate SourceEventDate,
			0 MarketId,
			'N/A' MarketSource,
			CONVERT(CHAR(11), CONVERT(DATE, a.MasterTransactionTimestamp)) MasterDayText,
			a.MasterTransactionTimestamp,
			CONVERT(varchar(5),CONVERT(time, a.MasterTransactionTimestamp)) MasterTimeText,
			DATEADD(MINUTE, z.OffsetMinutes, a.MasterTransactionTimestamp) AnalysisTransactionTimestamp, 
			CONVERT(varchar(5), CONVERT(time, DATEADD(MINUTE, z.OffsetMinutes, a.MasterTransactionTimestamp))) AnalysisTimeText,
			0 CampaignId,
			0 ChannelId,
			0 ActionChannelId,
			0 PriceTypeId,
			'N/A' PriceTypeSource,
			'Single' BetType,
			'Win' LegType,
			'Straight' BetGroupType,
			'Single' BetSubType,
			'Standard' GroupingType,
			-10 ClassId,
			'LOT' ClassSource,
			'N/A' UserId,
			'N/A' UserSource,
			0 InstrumentId,
			'N/A' InstrumentSource,
			0 NoteId,
			'N/A' NoteSource,
			1 NumberOfLegs,
			1 LegNumber,
			2 HeadlineFeatures,
			'N/A' InPlay,
			'N/A' ClickToCall,
			0 CashoutType,
			0 IsDoubleDown,
			0 IsDoubledDown,
			0 IsChaseTheAce,
			0 FeatureFlags,
			a.Stake * x.StakeFactor + a.Payout * x.PayoutFactor TransactedAmount,
			a.FromDate,
			a.MappingId
	FROM	Atomic.Lottery a
			INNER JOIN Reference.TimeZone z ON z.FromDate <= a.MasterTransactionTimestamp AND z.ToDate > a.MasterTransactionTimestamp AND z.TimeZone='Analysis'
			INNER JOIN Reference.fn_Mapping_Cashflow('Lottery') x ON x.TransactionCode = a.TransactionCode
	WHERE	BatchKey = @BatchKey
			AND (a.MappingId = @MappingId OR @MappingId IS NULL)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Withdrawal','Start',@RowsProcessed = @@ROWCOUNT

	INSERT	#Collect
	SELECT	a.TransactionId,
			a.TransactionSource,
			x.TransactionTypeID,
			x.TransactionStatusID,
			x.TransactionMethodID,
			x.TransactionDirection,
			x.BalanceTypeID,
			a.TransactionId LegId,
			a.TransactionSource LegSource,
			LedgerID,
			LedgerSource,
			'C' WalletId,
			0 PromotionId,
			'N/A' PromotionSource,
			0 EventId,
			'N/A' EventSource,
			CONVERT(varchar(255),NULL) SourceEvent, -- Placeholders from previous lottery transactions which do not have EventIds, only product and draw date
			CONVERT(datetime2(3),NULL) SourceEventDate,
			0 MarketId,
			'N/A' MarketSource,
			CONVERT(CHAR(11), CONVERT(DATE, a.MasterTimestamp)) MasterDayText,
			a.MasterTimestamp,
			CONVERT(varchar(5),CONVERT(time, a.MasterTimestamp)) MasterTimeText,
			DATEADD(MINUTE, z.OffsetMinutes, a.MasterTimestamp) AnalysisTransactionTimestamp, 
			CONVERT(varchar(5), CONVERT(time, DATEADD(MINUTE, z.OffsetMinutes, a.MasterTimestamp))) AnalysisTimeText,
			0 CampaignId,
			a.ChannelId,
			a.ChannelId ActionChannelId,
			0 PriceTypeId,
			'N/A' PriceTypeSource,
			'N/A' BetType,
			'N/A' LegType,
			'N/A' BetGroupType,
			'N/A' BetSubType,
			'N/A' GroupingType,
			0 ClassId,
			'IBT' ClassSource,
			a.UserId,
			a.UserSource,
			a.InstrumentId,
			a.InstrumentSource,
			a.NoteId,
			a.NoteSource,
			0 NumberOfLegs,
			0 LegNumber,
			0 HeadlineFeatures,
			'N/A' InPlay,
			'N/A' ClickToCall,
			0 CashoutType,
			0 IsDoubleDown,
			0 IsDoubledDown,
			0 IsChaseTheAce,
			0 FeatureFlags,
			a.Amount * x.AmountFactor TransactedAmount,
			a.FromDate,
			a.MappingId
	FROM	Atomic.Withdrawal a
			INNER JOIN Reference.TimeZone z ON z.FromDate <= a.MasterTimestamp AND z.ToDate > a.MasterTimestamp AND z.TimeZone='Analysis'
			INNER JOIN Reference.fn_Mapping_Cashflow('Withdrawal') x ON x.TransactionCode = a.TransactionCode
	WHERE	BatchKey = @BatchKey
			AND (a.MappingId = @MappingId OR @MappingId IS NULL)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Stage','Start',@RowsProcessed = @@ROWCOUNT

	SELECT  s.TransactionId,
			s.TransactionSource,
			ISNULL(td.TransactionDetailKey,-1) TransactionDetailKey,
			ISNULL(b.BalanceTypeKey,-1) BalanceTypeKey,
			ISNULL(c.ContractKey,-1) ContractKey,
			ISNULL(a.AccountKey,-1) AccountKey,
			CASE WHEN (s.TransactionTypeID = 'TR' and s.TransactionStatusID IN ('CO') and s.TransactionMethodID = 'MI' and s.TransactionDirection = 'DR') 
				 THEN COALESCE(sa1.AccountStatusKey, sa.AccountStatusKey,-1) ELSE COALESCE(sa.AccountStatusKey,-1) END AS AccountStatusKey,
			ISNULL(st.StructureKey,-1) StructureKey,
			ISNULL(w.WalletKey,-1) WalletKey,
			ISNULL(p.PromotionKey,-1) PromotionKey,
			COALESCE(e.EventKey,e1.EventKey,-99) EventKey,
			ISNULL(m.MarketKey,-1) MarketKey,
			ISNULL(d.DayKey,-1) DayKey,
			s.MasterTransactionTimestamp,
			ISNULL(tm.TimeKey,-1) MasterTimeKey,
			s.AnalysisTransactionTimestamp,
			ISNULL(ta.TimeKey,-1) AnalysisTimeKey,
			ISNULL(ca.CampaignKey,0) CampaignKey,
			ISNULL(ch.ChannelKey,0) ChannelKey,
			ISNULL(ac.ChannelKey,0) ActionChannelKey,
			s.LedgerID,
			s.TransactedAmount,
			ISNULL(pt.PriceTypeKey,-1) PriceTypeKey,
			ISNULL(bt.BetTypeKey,-1) BetTypeKey,
			ISNULL(l.LegTypeKey,-1) LegTypeKey,
			ISNULL(cl.ClassKey,-1) ClassKey,
			ISNULL(u.UserKey,0) UserKey,
			ISNULL(i.InstrumentKey,0) InstrumentKey,
			ISNULL(n.NoteKey,0) NoteKey,
			ISNULL(ip.InPlayKey,-1) InPlayKey,
			s.FromDate
	INTO	#Stage
	FROM	#Collect s
			LEFT JOIN [$(Dimensional)].Dimension.TransactionDetail td ON td.TransactionTypeID = s.TransactionTypeID AND td.TransactionStatusID = s.TransactionStatusID AND td.TransactionMethodID = s.TransactionMethodID AND td.TransactionDirection = s.TransactionDirection
			LEFT JOIN [$(Dimensional)].Dimension.BalanceType b ON b.BalanceTypeID = s.BalanceTypeID
			LEFT JOIN [$(Dimensional)].Dimension.Account a ON a.LedgerID = s.LedgerID AND a.LedgerSource = s.LedgerSource
			LEFT JOIN [$(Dimensional)].Dimension.AccountStatus sa ON sa.LedgerID = s.LedgerID AND sa.LedgerSource = s.LedgerSource AND sa.FromDate <= s.MasterTransactionTimestamp AND sa.ToDate > s.MasterTransactionTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.AccountStatus sa1 ON sa1.LedgerID = s.LedgerID AND sa1.LedgerSource = s.LedgerSource AND sa1.FromDate < s.MasterTransactionTimestamp AND sa1.ToDate >= s.MasterTransactionTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.Structure st ON st.LedgerId = s.LedgerId AND st.LedgerSource = s.LedgerSource AND st.HeadlineFeatures = s.HeadlineFeatures
			LEFT JOIN [$(Dimensional)].Dimension.Campaign ca ON ca.CampaignId = s.CampaignID
			LEFT JOIN [$(Dimensional)].Dimension.Wallet w ON w.WalletId = s.WalletId
			LEFT JOIN [$(Dimensional)].Dimension.Promotion p ON p.PromotionId = s.PromotionId AND p.Source = s.PromotionSource
			LEFT JOIN [$(Dimensional)].Dimension.Channel ch ON ch.ChannelId = s.ChannelId
			LEFT JOIN [$(Dimensional)].Dimension.Channel ac ON ac.ChannelId = s.ActionChannelId
			LEFT JOIN [$(Dimensional)].Dimension.[Contract] c ON c.LegId = s.LegId AND c.LegIdSource = s.LegSource
			LEFT JOIN [$(Dimensional)].Dimension.BetType bt ON bt.BetType = s.BetType AND bt.LegType = s.LegType AND bt.BetGroupType = s.BetGroupType AND bt.BetSubType = s.BetSubType AND bt.GroupingType = s.GroupingType
			LEFT JOIN [$(Dimensional)].Dimension.PriceType pt ON pt.PriceTypeId = s.PriceTypeId AND pt.Source = s.PriceTypeSource
			LEFT JOIN [$(Dimensional)].Dimension.Class cl ON cl.ClassID = s.ClassID AND cl.Source = s.ClassSource AND cl.FromDate <= s.MasterTransactionTimestamp AND cl.ToDate > s.MasterTransactionTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.Market m ON m.MarketID = s.MarketID AND m.Source = S.MarketSource
			LEFT JOIN [$(Dimensional)].Dimension.[Event] e ON e.EventID = s.EventID AND e.Source = s.EventSource
			LEFT JOIN [$(Dimensional)].Dimension.[Event] e1 ON e1.SourceEvent = s.SourceEvent AND e1.SourceEventDate = s.SourceEventDate
			LEFT JOIN [$(Dimensional)].Dimension.[User] u ON u.UserID = s.UserID AND u.Source = s.UserSource
			LEFT JOIN [$(Dimensional)].Dimension.Instrument i ON i.InstrumentID = s.InstrumentID AND i.Source = s.InstrumentSource
			LEFT JOIN [$(Dimensional)].Dimension.Note n ON n.NoteId = s.NoteId AND n.NoteSource = s.NoteSource
			LEFT JOIN [$(Dimensional)].Dimension.LegType l ON l.NumberOfLegs = s.NumberOfLegs AND l.LegNumber = s.LegNumber
			LEFT JOIN [$(Dimensional)].Dimension.[DayZone] d ON d.MasterDayText = s.MasterDayText AND d.FromDate <= s.MasterTransactionTimestamp AND d.ToDate > s.MasterTransactionTimestamp
			LEFT JOIN [$(Dimensional)].Dimension.[Time] tm ON tm.TimeText = s.MasterTimeText
			LEFT JOIN [$(Dimensional)].Dimension.[Time] ta ON ta.TimeText = s.AnalysisTimeText
			LEFT JOIN [$(Dimensional)].Dimension.InPlay	ip ON ip.InPlay = s.InPlay and ip.ClickToCall = s.ClickToCall and ip.CashoutType = s.CashoutType AND ip.IsDoubleDown = s.IsDoubleDown AND ip.IsDoubledDown = s.IsDoubledDown AND ip.IsChaseTheAce = s.IsChaseTheAce AND ip.FeatureFlags = s.FeatureFlags
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Fact','Start',@RowsProcessed = @@ROWCOUNT

	SELECT  TransactionId,
			TransactionIdSource,
			TransactionDetailKey,
			BalanceTypeKey,
			ContractKey,
			AccountKey,
			AccountStatusKey,
			StructureKey,
			WalletKey,
			PromotionKey,
			EventKey,
			MarketKey,
			DayKey,
			MasterTransactionTimestamp,
			MasterTimeKey,
			AnalysisTransactionTimestamp,
			AnalysisTimeKey,
			CampaignKey,
			ChannelKey,
			ActionChannelKey,
			LedgerID,
			TransactedAmount,
			PriceTypeKey,
			BetTypeKey,
			LegTypeKey,
			ClassKey,
			UserKey,
			InstrumentKey,
			NoteKey,
			InPlayKey,
			FromDate
	INTO	#Fact
	FROM	[$(Dimensional)].Fact.[Transaction]
	WHERE	CreatedBatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start',@RowsProcessed = @@ROWCOUNT

	SELECT  *, CASE WHEN COUNT(*) OVER (PARTITION BY TransactionId, TransactionSource, TransactionDetailKey, BalanceTypeKey, MasterTransactionTimestamp, LegTypeKey) = 2 THEN 'U' WHEN src = 'S' THEN 'I' WHEN src = 'F' THEN 'D' ELSE 'X' END Upsert
	INTO	#Differences
	FROM	(	SELECT  TransactionId, TransactionSource, 
						TransactionDetailKey, BalanceTypeKey, ContractKey, AccountKey, AccountStatusKey, StructureKey, WalletKey, PromotionKey, EventKey, MarketKey,
						DayKey, MasterTransactionTimestamp, MasterTimeKey, AnalysisTransactionTimestamp, AnalysisTimeKey,
						CampaignKey, ChannelKey, ActionChannelKey, LedgerID, TransactedAmount,
						PriceTypeKey, BetTypeKey, LegTypeKey, ClassKey, UserKey, InstrumentKey, NoteKey, InPlayKey, MAX(FromDate) FromDate, MAX(src) src -- FromDate left out of difference check due to historically being datetime2(0) and now datetime2(3) - don't want to update every from date, only if record changing for other reasons
				FROM	(	SELECT *, 'S' src FROM #Stage
							UNION ALL
							SELECT *, 'F' src FROM #Fact
						) x
				GROUP BY TransactionId, TransactionSource, 
						TransactionDetailKey, BalanceTypeKey, ContractKey, AccountKey, AccountStatusKey, StructureKey, WalletKey, PromotionKey, EventKey, MarketKey,
						DayKey, MasterTransactionTimestamp, MasterTimeKey, AnalysisTransactionTimestamp, AnalysisTimeKey,
						CampaignKey, ChannelKey, ActionChannelKey, LedgerID, TransactedAmount,
						PriceTypeKey, BetTypeKey, LegTypeKey, ClassKey, UserKey, InstrumentKey, NoteKey, InPlayKey--, FromDate
				HAVING	COUNT(*) = 1
			) y
	OPTION (RECOMPILE)

	IF @DryRun = 1 
		SELECT	TOP 10000 
				CASE WHEN Upsert = 'U' AND src = 'F' THEN 'U-' WHEN Upsert = 'U' AND src = 'S' THEN 'U+' ELSE Upsert END act, 
				* 
		FROM	#Differences 
		ORDER BY TransactionId, TransactionSource, TransactionDetailKey, BalanceTypeKey, MasterTransactionTimestamp, LegTypeKey, src desc

	SELECT	* INTO #Prepare FROM #Differences WHERE	Upsert <> 'U' OR src <> 'F' -- Discard the 'before' version record of Update

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start',@RowsProcessed = @@ROWCOUNT

	IF @DryRun = 0 
		BEGIN
			UPDATE	d
			SET		TransactionId = s.TransactionId, 
					TransactionIdSource = s.TransactionSource, 
					TransactionDetailKey = s.TransactionDetailKey, 
					BalanceTypeKey = s.BalanceTypeKey, 
					ContractKey = s.ContractKey, 
					AccountKey = s.AccountKey, 
					AccountStatusKey = s.AccountStatusKey, 
					StructureKey = s.StructureKey,
					WalletKey = s.WalletKey, 
					PromotionKey = s.PromotionKey, 
					EventKey = s.EventKey, 
					MarketKey =s.MarketKey,
					DayKey = s.DayKey,
					MasterTransactionTimestamp = s.MasterTransactionTimestamp, 
					MasterTimeKey = s.MasterTimeKey, 
					AnalysisTransactionTimestamp = s.AnalysisTransactionTimestamp, 
					AnalysisTimeKey = s.AnalysisTimeKey,
					CampaignKey = s.CampaignKey, 
					ChannelKey = s.ChannelKey, 
					ActionChannelKey = s.ActionChannelKey, 
					LedgerID = s.LedgerID, 
					TransactedAmount = s.TransactedAmount,
					PriceTypeKey = s.PriceTypeKey, 
					BetTypeKey = s.BetTypeKey, 
					LegTypeKey = s.LegTypeKey, 
					ClassKey = s.ClassKey, 
					UserKey = s.UserKey, 
					InstrumentKey = s.InstrumentKey, 
					NoteKey = s.NoteKey, 
					InPlayKey = s.InPlayKey,
					FromDate = s.FromDate,
					CreatedDate = SYSDATETIME(), 
					CreatedBy = @Me
			FROM	[$(Dimensional)].Fact.[Transaction] d
					INNER JOIN #Prepare s ON s.TransactionId = d.TransactionId AND s.TransactionSource = d.TransactionIdSource AND s.TransactionDetailKey = d.TransactionDetailKey AND s.BalanceTypeKey = d.BalanceTypeKey 
												AND s.DayKey = d.DayKey AND s.MasterTransactionTimestamp = d.MasterTransactionTimestamp AND s.LegTypeKey = d.LegTypeKey
			WHERE	s.Upsert = 'U'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'U'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start',@RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			INSERT	[$(Dimensional)].Fact.[Transaction]
					(	TransactionId, TransactionIdSource, 
						TransactionDetailKey, BalanceTypeKey, ContractKey, AccountKey, AccountStatusKey, StructureKey, WalletKey, PromotionKey, EventKey, MarketKey,
						DayKey, MasterTransactionTimestamp, MasterTimeKey, AnalysisTransactionTimestamp, AnalysisTimeKey,
						CampaignKey, ChannelKey, ActionChannelKey, LedgerID, TransactedAmount,
						PriceTypeKey, BetTypeKey, LegTypeKey, ClassKey, UserKey, InstrumentKey, NoteKey, InPlayKey, Fromdate,
						CreatedBatchKey, CreatedDate, CreatedBy
					)
			SELECT	s.TransactionId, s.TransactionSource, 
					s.TransactionDetailKey, s.BalanceTypeKey, s.ContractKey, s.AccountKey, s.AccountStatusKey, s.StructureKey, s.WalletKey, s.PromotionKey, s.EventKey, s.MarketKey,
					s.DayKey, s.MasterTransactionTimestamp, s.MasterTimeKey, s.AnalysisTransactionTimestamp, s.AnalysisTimeKey,
					s.CampaignKey, s.ChannelKey, s.ActionChannelKey, s.LedgerID, s.TransactedAmount,
					s.PriceTypeKey, s.BetTypeKey, s.LegTypeKey, s.ClassKey, s.UserKey, s.InstrumentKey, s.NoteKey, s.InPlayKey, s.FromDate,
					@BatchKey CreatedBatchKey, SYSDATETIME() CreatedDate, @Me CreatedBy
			FROM	#Prepare s
			WHERE	s.Upsert = 'I'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'I'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Delete','Start',@RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			DELETE	d
			FROM	[$(Dimensional)].Fact.[Transaction] d
					INNER JOIN #Prepare p ON p.TransactionId = d.TransactionId AND p.TransactionSource = d.TransactionIdSource AND p.TransactionDetailKey = d.TransactionDetailKey AND p.BalanceTypeKey = d.BalanceTypeKey 
												AND p.DayKey = d.DayKey AND p.MasterTransactionTimestamp = d.MasterTransactionTimestamp AND p.LegTypeKey = d.LegTypeKey
			WHERE	p.Upsert = 'D'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'D'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success',@RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
