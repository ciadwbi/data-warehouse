﻿CREATE TABLE [Cache].[ZoneDay] (
    [FromDate]							DATETIME2(3)	NOT NULL,
    [ToDate]							DATETIME2(3)	NOT NULL,
    [NextFromDate]						DATETIME2(3)	NOT NULL,
    [DayDate]							DATE			NOT NULL,
    [TimeZone]							VARCHAR(32)		NOT NULL,
	[CreatedDate]						DATETIME2(7)	NOT NULL,
	[CreatedBatchKey]					INT				NOT NULL,
	[CreatedBy]							VARCHAR(32)		NOT NULL,
	[ModifiedDate]						DATETIME2(7)	NOT NULL,
	[ModifiedBatchKey]					INT				NOT NULL,
	[ModifiedBy]							VARCHAR(32)		NOT NULL,
	[Increment]							INT				NOT NULL,
    CONSTRAINT [CI_ZoneDay] PRIMARY KEY CLUSTERED ([FromDate])
)
GO




