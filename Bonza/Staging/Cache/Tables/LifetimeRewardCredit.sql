﻿CREATE TABLE [Cache].[LifetimeRewardCredit](
	[FromDate] [datetime2](3) NOT NULL,
	[ToDate] [datetime2](3) NOT NULL,
	[LoadDate] [datetime2](3) NOT NULL,
	[ClientId] [int] NOT NULL,
	[Points] [money] NOT NULL,
    [PointsBalance] [money] NOT NULL,
	[FirstRewardCreditDate] [datetime2](3) NULL,
	[LastRewardCreditDate] [datetime2](3) NULL,
	[NumberOfRewardCredits] [int] NOT NULL,
	[LifetimeRewardCredit] [money] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[Increment] [int] NOT NULL,
 CONSTRAINT [CI_LifetimeRewardCredit] PRIMARY KEY CLUSTERED ([ClientId] ASC, [FromDate] ASC)
)
GO

CREATE NONCLUSTERED INDEX [NI_LifetimeRewardCredit_LoadDate] ON [Cache].[LifetimeRewardCredit]
([LoadDate] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_LifetimeRewardCredit_ToDate] ON [Cache].[LifetimeRewardCredit]
([ToDate] ASC) INCLUDE (FromDate)
GO






