﻿CREATE TABLE [Cache].[RewardLifetime] (
    [FromDate]							DATETIME2(3)	NOT NULL,
    [ToDate]							DATETIME2(3)	NOT NULL,
    [LoadDate]							DATETIME2(3)	NOT NULL,
    [ClientId]							INT				NOT NULL,
    [Points]							MONEY			NOT NULL,
    [LifetimePoints]					MONEY			NOT NULL,
    [PointsBalance]						MONEY			NOT NULL,
    [FirstAccrualDate]					DATETIME2(3)	NULL,
    [FirstAccrualBetId]					BIGINT			NULL,
    [LastAccrualDate]					DATETIME2(3)	NULL,
    [LastAccrualBetId]					BIGINT			NULL,
    [Accruals]							INT				NOT NULL,
    [LifetimePointsAccrued]				MONEY			NOT NULL,
    [PointsAccruedBalance]				MONEY			NOT NULL,
    [FirstRedemptionBonusDate]			DATETIME2(3)	NULL,
    [LastRedemptionBonusDate]			DATETIME2(3)	NULL,
    [RedemptionsBonus]					INT				NOT NULL,
    [LifetimePointsRedeemedBonus]		MONEY			NOT NULL,
    [FirstRedemptionVelocityDate]		DATETIME2(3)	NULL,
    [LastRedemptionVelocityDate]		DATETIME2(3)	NULL,
    [RedemptionsVelocity]				INT				NOT NULL,
    [LifetimePointsRedeemedVelocity]	MONEY			NOT NULL,
    [LifetimePointsAdjustedCredit]		MONEY			NOT NULL,
    [LifetimePointsAdjustedDebit]		MONEY			NOT NULL,
	[CreatedDate]						DATETIME2(7)	NOT NULL,
	[CreatedBatchKey]					INT				NOT NULL,
	[CreatedBy]							VARCHAR(32)		NOT NULL,
	[ModifiedDate]						DATETIME2(7)	NOT NULL,
	[ModifiedBatchKey]					INT				NOT NULL,
	[ModifiedBy]						VARCHAR(32)		NOT NULL,
	[Increment]							INT				NOT NULL,
    CONSTRAINT [CI_RewardLifetime] PRIMARY KEY CLUSTERED ([ClientId],[FromDate])
)
GO

CREATE NONCLUSTERED INDEX [NI_RewardLifetime_LoadDate] ON [Cache].[RewardLifetime]
([LoadDate] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_RewardLifetime_ToDate] ON [Cache].[RewardLifetime]
([ToDate] ASC) INCLUDE (FromDate)
GO



