﻿CREATE TABLE [Cache].[LifetimeRewardDebit](
	[FromDate] [datetime2](3) NOT NULL,
	[ToDate] [datetime2](3) NOT NULL,
	[LoadDate] [datetime2](3) NOT NULL,
	[ClientId] [int] NOT NULL,
	[Points] [money] NOT NULL,
    [PointsBalance] [money] NOT NULL,
	[FirstRewardDebitDate] [datetime2](3) NULL,
	[LastRewardDebitDate] [datetime2](3) NULL,
	[NumberOfRewardDebits] [int] NOT NULL,
	[LifetimeRewardDebit] [money] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[Increment] [int] NOT NULL,
 CONSTRAINT [CI_LifetimeRewardDebit] PRIMARY KEY CLUSTERED ([ClientId] ASC, [FromDate] ASC)
)
GO

CREATE NONCLUSTERED INDEX [NI_LifetimeRewardDebit_LoadDate] ON [Cache].[LifetimeRewardDebit]
([LoadDate] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_LifetimeRewardDebitt_ToDate] ON [Cache].[LifetimeRewardDebit]
([ToDate] ASC) INCLUDE (FromDate)
GO






