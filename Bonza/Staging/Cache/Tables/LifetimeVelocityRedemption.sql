﻿CREATE TABLE [Cache].[LifetimeVelocityRedemption](
	[FromDate] [datetime2](3) NOT NULL,
	[ToDate] [datetime2](3) NOT NULL,
	[LoadDate] [datetime2](3) NOT NULL,
	[ClientId] [int] NOT NULL,
	[Points] [money] NOT NULL,
    [PointsBalance] [money] NOT NULL,
	[FirstVelocityRedemptionDate] [datetime2](3) NULL,
	[LastVelocityRedemptionDate] [datetime2](3) NULL,
	[NumberOfVelocityRedemptions] [int] NOT NULL,
	[LifetimeVelocityRedemption] [money] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[Increment] [int] NOT NULL,
 CONSTRAINT [CI_LifetimeVelocityRedemption] PRIMARY KEY CLUSTERED ([ClientId] ASC, [FromDate] ASC)
)
GO

CREATE NONCLUSTERED INDEX [NI_LifetimeVelocityRedemption_LoadDate] ON [Cache].[LifetimeVelocityRedemption]
([LoadDate] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_LifetimeVelocityRedemption_ToDate] ON [Cache].[LifetimeVelocityRedemption]
([ToDate] ASC) INCLUDE (FromDate)
GO






