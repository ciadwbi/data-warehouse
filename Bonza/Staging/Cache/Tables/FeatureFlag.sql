﻿CREATE TABLE [Cache].[FeatureFlag] (
    [Flag]		  INT			 NOT NULL,
    [FeatureCode] NVARCHAR(200)  NOT NULL,
    CONSTRAINT [PK_FeatureFlag] PRIMARY KEY CLUSTERED ([Flag])
)
GO



