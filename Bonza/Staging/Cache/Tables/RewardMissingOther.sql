﻿CREATE TABLE [Cache].[RewardMissingOther] (
    [FromDate]							DATETIME2(3)	NOT NULL,
    [LoadDate]							DATETIME2(3)	NOT NULL,
    [ClientId]							INT				NOT NULL,
    [Points]							MONEY			NOT NULL,
    CONSTRAINT [CI_RewardMissingOther] PRIMARY KEY CLUSTERED ([ClientId],[FromDate])
)
GO

CREATE NONCLUSTERED INDEX [NI_RewardMissingOther_LoadDate] ON [Cache].[RewardMissingOther]
([LoadDate] ASC)
GO




