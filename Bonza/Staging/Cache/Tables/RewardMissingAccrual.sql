﻿CREATE TABLE [Cache].[RewardMissingAccrual] (
    [FromDate]							DATETIME2(3)	NOT NULL,
    [LoadDate]							DATETIME2(3)	NOT NULL,
    [ClientId]							INT				NOT NULL,
    [Points]							MONEY			NOT NULL,
    CONSTRAINT [CI_RewardMissingAccrual] PRIMARY KEY CLUSTERED ([ClientId],[FromDate])
)
GO

CREATE NONCLUSTERED INDEX [NI_RewardMissingAccrual] ON [Cache].[RewardMissingAccrual]
([LoadDate] ASC)
GO




