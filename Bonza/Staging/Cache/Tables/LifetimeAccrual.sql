﻿CREATE TABLE [Cache].[LifetimeAccrual] (
    [FromDate]							DATETIME2(3)	NOT NULL,
    [ToDate]							DATETIME2(3)	NOT NULL,
    [LoadDate]							DATETIME2(3)	NOT NULL,
    [ClientId]							INT				NOT NULL,
    [Points]							MONEY			NOT NULL,
    [PointsBalance]						MONEY			NOT NULL,
    [AccrualBalance]					MONEY			NOT NULL,
    [FirstAccrualDate]					DATETIME2(3)	NULL,
    [FirstAccrualBetId]					BIGINT			NULL,
    [LastAccrualDate]					DATETIME2(3)	NULL,
    [LastAccrualBetId]					BIGINT			NULL,
    [NumberOfAccruals]					INT				NOT NULL,
    [LifetimeAccrual]					MONEY			NOT NULL,
	[CreatedDate]						DATETIME2(7)	NOT NULL,
	[CreatedBatchKey]					INT				NOT NULL,
	[ModifiedDate]						DATETIME2(7)	NOT NULL,
	[ModifiedBatchKey]					INT				NOT NULL,
	[Increment]							INT				NOT NULL,
    CONSTRAINT [CI_LifetimeAccrual] PRIMARY KEY CLUSTERED ([ClientId],[FromDate])
)
GO

CREATE NONCLUSTERED INDEX [NI_LifetimeAccrual_LoadDate] ON [Cache].[LifetimeAccrual]
([LoadDate] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_LifetimeAccrual_ToDate] ON [Cache].[LifetimeAccrual]
([ToDate] ASC) INCLUDE (FromDate)
GO
