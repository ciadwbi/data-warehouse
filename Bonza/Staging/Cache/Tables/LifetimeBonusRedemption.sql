﻿CREATE TABLE [Cache].[LifetimeBonusRedemption](
	[FromDate] [datetime2](3) NOT NULL,
	[ToDate] [datetime2](3) NOT NULL,
	[LoadDate] [datetime2](3) NOT NULL,
	[ClientId] [int] NOT NULL,
    [PointsBalance] [money] NOT NULL,
	[Points] [money] NOT NULL,
	[FirstBonusRedemptionDate] [datetime2](3) NULL,
	[LastBonusRedemptionDate] [datetime2](3) NULL,
	[NumberOfBonusRedemptions] [int] NOT NULL,
	[LifetimeBonusRedemption] [money] NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[CreatedBatchKey] [int] NOT NULL,
	[ModifiedDate] [datetime2](7) NOT NULL,
	[ModifiedBatchKey] [int] NOT NULL,
	[Increment] [int] NOT NULL,
 CONSTRAINT [CI_LifetimeBonusRedemption] PRIMARY KEY CLUSTERED ([ClientId] ASC, [FromDate] ASC)
)
GO

CREATE NONCLUSTERED INDEX [NI_LifetimeBonusRedemption_LoadDate] ON [Cache].[LifetimeBonusRedemption]
([LoadDate] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_LifetimeBonusRedemption_ToDate] ON [Cache].[LifetimeBonusRedemption]
([ToDate] ASC) INCLUDE (FromDate)
GO






