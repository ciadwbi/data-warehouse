﻿CREATE TABLE [Cache].[TransactionLottery] (
	[TransactionID]						BIGINT				NOT NULL,
	[ClientID]							INT					NOT NULL,
	[AccountNumber]						INT					NOT NULL,
	[TransactionDate]					DATETIME			NOT NULL,
	[Amount]							MONEY				NOT NULL,
	[Stake]								MONEY				NOT NULL,
	[TransactionCode]					INT					NOT NULL,
	[Notes]								VARCHAR(250)		NULL,
	[ExternalId]						UNIQUEIDENTIFIER	NULL,
	[LinkExternalId]					UNIQUEIDENTIFIER	NULL,
	[LinkTransactionId]					BIGINT				NULL,
	[Product]							VARCHAR(255)		NOT NULL,
	[Jackpot]							DECIMAL(38,2)		NOT NULL,
	[DrawDate]							DATETIME2(3)		NULL, -- Unfortunately a bug in source system means we have to cope with NULL draw date
	[FromDate]							DATETIME2(3)		NOT NULL,
	[CreatedDate]						DATETIME2(7)		NOT NULL,
	[CreatedBatchKey]					INT					NOT NULL,
	[CreatedBy]							VARCHAR(32)			NOT NULL,
	[Increment]							INT					NOT NULL,
)
GO

CREATE UNIQUE CLUSTERED INDEX [CI_TransactionLottery] 
	ON [Cache].[TransactionLottery] ([TransactionId] ASC)
GO

CREATE NONCLUSTERED INDEX [NI_TransactionLottery_FD] 
	ON [Cache].[TransactionLottery] ([FromDate] ASC)
GO


CREATE NONCLUSTERED INDEX [NI_TransactionLottery_ExternalId] 
	ON [Cache].[TransactionLottery] ([ExternalId] ASC)
	INCLUDE ([FromDate], [TransactionCode])
GO


