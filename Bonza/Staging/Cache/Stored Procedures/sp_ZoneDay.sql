CREATE PROCEDURE [Cache].[sp_ZoneDay]
(
	@BatchKey	int = 0,
	@FromDate	datetime2(3) = NULL,
	@ToDate		datetime2(3) = NULL,
	@Increment		int = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	SELECT @FromDate = COALESCE(@FromDate,MAX(FromDate),CONVERT(datetime2(3),'1990-01-01')) FROM Cache.ZoneDay
	SET @ToDate = COALESCE(@ToDate,GETDATE())

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Dates','Start'

	;WITH DaySequence AS
		-- create a sequence of all possible days in the specified from/to range with sufficient excess beyond the range boundaries to ensure 
		-- additional days are available for any previous and next values to be derived for each required record within the from/to range
		(	SELECT	CONVERT(datetime2(3),CONVERT(date,@FromDate)) DayDate
			UNION ALL
			SELECT	DATEADD(DAY,1,DayDate) FROM	DaySequence WHERE DayDate <= DATEADD(DAY,1,CONVERT(date, @ToDate))
		)
	SELECT	FromDate, ToDate, NextFromDate, DayDate, TimeZone
	INTO	#Stage
	FROM	(	SELECT	x.FromDate, 
						LEAD(x.FromDate) OVER (ORDER BY x.FromDate) NextFromDate,
						LEAD(x.FromDate) OVER (PARTITION BY x.TimeZone ORDER BY x.FromDate) ToDate,
						x.DayDate,
						x.TimeZone
				FROM	(	SELECT	DATEADD(MINUTE, - OffsetMinutes, DayDate) FromDate, DayDate, TimeZone, OffsetMinutes
							FROM	DaySequence d
									INNER JOIN [Reference].[TimeZone] z ON z.FromDate <= d.DayDate AND z.ToDate > d.DayDate
						) x
			) y
	WHERE	FromDate > @FromDate
			AND FromDate <= @ToDate 
			AND FromDate >= '2013-12-19'
	OPTION (RECOMPILE, MAXRECURSION 0)		

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	s.*, CASE WHEN z.FromDate IS NULL THEN 'I' ELSE 'U' END Upsert
	INTO	#ZoneDay
	FROM	#Stage s
			LEFT JOIN Cache.ZoneDay z ON z.FromDate = s.FromDate
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		ToDate = z.ToDate,
			NextFromDate = z.NextFromDate,
			DayDate = z.DayDate,
			TimeZone = z.TimeZone,
			ModifiedDate = CURRENT_TIMESTAMP,
			ModifiedBatchKey = @BatchKey,
			ModifiedBy = @Me,
			Increment = ISNULL(@Increment,0)
	FROM	#ZoneDay z
			INNER JOIN Cache.ZoneDay u ON u.FromDate = z.FromDate
	WHERE	z.Upsert = 'U'

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @RowsProcessed = @RowCount

	INSERT	Cache.ZoneDay 
			(	FromDate, ToDate, NextFromDate, DayDate, TimeZone, 
				CreatedDate, CreatedBatchKey, CreatedBy, 
				ModifiedDate, ModifiedBatchKey, ModifiedBy,
				Increment
			)
	SELECT	FromDate, ToDate, NextFromDate, DayDate, TimeZone,
			CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, 
			CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy,
			ISNULL(@Increment,0) Increment
	FROM	#ZoneDay
	WHERE	Upsert = 'I'

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
