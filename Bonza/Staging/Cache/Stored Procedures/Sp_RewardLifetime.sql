CREATE PROCEDURE [Cache].[Sp_RewardLifetime]
(
	@BatchKey		int = 0,
	@FromDate		datetime2(3) = NULL,
	@ToDate			datetime2(3) = NULL,
	@Increment		int = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	DECLARE @BaselineExecutedAt datetime
	DECLARE @BaselineFromDate datetime2(3)

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Dates','Start'

	SELECT @FromDate = COALESCE(@FromDate, MAX(LoadDate), CONVERT(datetime2(3),'1900-01-01')) FROM Cache.RewardLifetime
	SELECT @ToDate = COALESCE(@ToDate, MAX(FromDate)) FROM [$(Acquisition)].Messaging.RewardsPointsAccrued

	--	So, LoadDate on the cache record is the FromDate of the Acquisition record it came from, whereas FromDate on cache is ExecutedAt on the source Acquisition record.
	--	Reasons for this are:
	--	1)	A switch in significant time-lines from the order in which the data was acquired to the order it needs to be for analysis reflected in the role of From/To date at different stages
	--	2)	A logical step in the DataVault direction for later
	--	3)	If it was hard to write, it bloody well should be hard to understand - get over it!

	-- We cannot guarantee that messages arrive in chronological order.
	-- We control the Acquisition FromDate for messages as our Load Date into Acquisition, so we can guarantee that the From Date grows chronologically and we are only looking for FromDate since the last time we loaded.
	-- However, because we cannot guarantee the ExecutedAt dates that we really care about arrive in order, there is a strong possibility we may get an ExecutedAt arriving which is in the middle of dates previouslt processed.
	-- Out of sequence messages are a problem when dealing with accumulating balances where not just the late message needs to be added, but any derived accumulating balance on all subsequent messages previously loaded may require updating.
	-- For that reason, having identified all the new messages for processing by Acquisition from date, we then need to ascertain the earliest ExecutedAt date amongst those new messages and begin reprocessing all messages,
	-- new or previous, from that date forward to ensure we slot the out of sequence messages into the correct position in the ExecutedAt timeline.

	SELECT	@BaselineExecutedAt = MIN(ExecutedAt)
	FROM	(	SELECT	ExecutedAt, FromDate, ToDate FROM [$(Acquisition)].Messaging.RewardsPointsAccrued
				UNION ALL
				SELECT	ExecutedAt, FromDate, ToDate FROM [$(Acquisition)].Messaging.RewardsPointsAdjusted
				UNION ALL
				SELECT	ExecutedAt, FromDate, ToDate FROM [$(Acquisition)].Messaging.RewardsBonusRedemption
				UNION ALL
				SELECT	ExecutedAt, FromDate, ToDate FROM [$(Acquisition)].Messaging.RewardsVelocityRedemption
			) x
	WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate = CONVERT(datetime2(3),'9999-12-31') -- Messages never change so we can force the ToDate to a single partition until we develop message-specific Acquisition structures partitioned on FromDate
	OPTION(RECOMPILE)

	SET @BaselineFromDate = CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),@BaselineExecutedAt))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Accrued','Start', @RowsProcessed = 0

	SELECT	CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) FromDate, FromDate LoadDate, ClientId, Points, Points LifetimePoints, [Current] PointsBalance, 
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) FirstAccrualDate, BetId FirstAccrualBetId, 
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) LastAccrualDate, BetId LastAccrualBetId, 
			CONVERT(int, 1) Accruals, Points PointsAccrued, [Lifetime] PointsAccruedBalance,
			CONVERT(datetime2(3), NULL) FirstRedemptionBonusDate, CONVERT(datetime2(3), NULL) LastRedemptionBonusDate, 
			CONVERT(int, 0) RedemptionBonus, CONVERT(money, 0) PointsRedeemedBonus,
			CONVERT(datetime2(3), NULL) FirstRedemptionVelocityDate, CONVERT(datetime2(3), NULL) LastRedemptionVelocityDate, 
			CONVERT(int, 0) RedemptionVelocity, CONVERT(money, 0) PointsRedeemedVelocity,
			CONVERT(money, 0) PointsAdjustedCredit, CONVERT(money, 0) PointsAdjustedDebit 
	INTO	#Rewards
	FROM	[$(Acquisition)].Messaging.RewardsPointsAccrued
	WHERE	isDuplicate = 0 AND ExecutedAt >= @BaselineExecutedAt AND FromDate <= @ToDate AND ToDate = CONVERT(datetime2(3),'9999-12-31')
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Adjusted','Start', @RowsProcessed = @@ROWCOUNT

	INSERT	#Rewards
	SELECT	CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) FromDate, FromDate LoadDate, ClientId, Points, Points LifetimePoints, [Current] PointsBalance, 
			CONVERT(datetime2(3), NULL) FirstAccrualDate, CONVERT(bigint,NULL) FirstAccrualBetId, 
			CONVERT(datetime2(3), NULL) LastAccrualDate, CONVERT(bigint,NULL) LastAccrualBetId, 
			CONVERT(int, 0) Accruals, CONVERT(money, 0) PointsAccrued, [Lifetime] PointsAccruedBalance,
			CONVERT(datetime2(3), NULL) FirstRedemptionBonusDate, CONVERT(datetime2(3), NULL) LastRedemptionBonusDate, 
			CONVERT(int, 0) RedemptionBonus, CONVERT(money, 0) PointsRedeemedBonus,
			CONVERT(datetime2(3), NULL) FirstRedemptionVelocityDate, CONVERT(datetime2(3), NULL) LastRedemptionVelocityDate, 
			CONVERT(int, 0) RedemptionVelocity, CONVERT(money, 0) PointsRedeemedVelocity,
			CASE SIGN(Points) WHEN +1 THEN Points ELSE CONVERT(money, 0) END PointsAdjustedCredit, CASE SIGN(Points) WHEN -1 THEN Points ELSE CONVERT(money, 0) END PointsAdjustedDebit
	FROM	[$(Acquisition)].Messaging.RewardsPointsAdjusted
	WHERE	isDuplicate = 0 AND ExecutedAt >= @BaselineExecutedAt AND FromDate <= @ToDate AND ToDate = CONVERT(datetime2(3),'9999-12-31')
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Bonus','Start', @RowsProcessed = @@ROWCOUNT

	INSERT	#Rewards
	SELECT	CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) FromDate, FromDate LoadDate, ClientId, -Points Points, -Points LifetimePoints, [Current] PointsBalance, 
			CONVERT(datetime2(3), NULL) FirstAccrualDate, CONVERT(bigint,NULL) FirstAccrualBetId, 
			CONVERT(datetime2(3), NULL) LastAccrualDate, CONVERT(bigint,NULL) LastAccrualBetId, 
			CONVERT(int, 0) Accruals, CONVERT(money, 0) PointsAccrued, [Lifetime] PointsAccruedBalance,
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) FirstRedemptionBonusDate, CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) LastRedemptionBonusDate, 
			CONVERT(int, 1) RedemptionBonus, Points PointsRedeemedBonus,
			CONVERT(datetime2(3), NULL) FirstRedemptionVelocityDate, CONVERT(datetime2(3), NULL) LastRedemptionVelocityDate, 
			CONVERT(int, 0) RedemptionVelocity, CONVERT(money, 0) PointsRedeemedVelocity,
			CONVERT(money, 0) PointsAdjustedCredit, CONVERT(money, 0) PointsAdjustedDebit 
	FROM	[$(Acquisition)].Messaging.RewardsBonusRedemption
	WHERE	isDuplicate = 0 AND ExecutedAt >= @BaselineExecutedAt AND FromDate <= @ToDate AND ToDate = CONVERT(datetime2(3),'9999-12-31')
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Velocity','Start', @RowsProcessed = @@ROWCOUNT

	INSERT	#Rewards
	SELECT	CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) FromDate, FromDate LoadDate, ClientId, -Points Points, -Points LifetimePoints, [Current] PointsBalance, 
			CONVERT(datetime2(3), NULL) FirstAccrualDate, CONVERT(bigint,NULL) FirstAccrualBetId, 
			CONVERT(datetime2(3), NULL) LastAccrualDate, CONVERT(bigint,NULL) LastAccrualBetId, 
			CONVERT(int, 0) Accruals, CONVERT(money, 0) PointsAccrued, [Lifetime] PointsAccruedBalance,
			CONVERT(datetime2(3), NULL) FirstRedemptionBonusDate, CONVERT(datetime2(3), NULL) LastRedemptionBonusDate, 
			CONVERT(int, 0) RedemptionBonus, CONVERT(money, 0) PointsRedeemedBonus,
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) FirstRedemptionVelocityDate, CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) LastRedemptionVelocityDate, 
			CONVERT(int, 1) RedemptionVelocity, Points PointsRedeemedVelocity,
			CONVERT(money, 0) PointsAdjustedCredit, CONVERT(money, 0) PointsAdjustedDebit 
	FROM	[$(Acquisition)].Messaging.RewardsVelocityRedemption
	WHERE	isDuplicate = 0 AND ExecutedAt >= @BaselineExecutedAt AND FromDate <= @ToDate AND ToDate = CONVERT(datetime2(3),'9999-12-31')
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Previous','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	DISTINCT ClientId
	INTO	#Clients
	FROM	#Rewards

	INSERT	#Rewards
	SELECT	FromDate, LoadDate, ClientId, Points, LifetimePoints, PointsBalance,
			FirstAccrualDate, FirstAccrualBetId, 
			LastAccrualDate, LastAccrualBetId, 
			Accruals, LifetimePointsAccrued, PointsAccruedBalance,
			FirstRedemptionBonusDate, LastRedemptionBonusDate, 
			RedemptionsBonus, LifetimePointsRedeemedBonus,
			FirstRedemptionVelocityDate, LastRedemptionVelocityDate, 
			RedemptionsVelocity, LifetimePointsRedeemedVelocity,
			LifetimePointsAdjustedCredit, LifetimePointsAdjustedDebit
	FROM	Cache.RewardLifetime 
	WHERE	ClientId IN (SELECT ClientId FROM #Clients)
			AND FromDate < @BaselineFromDate AND ToDate >= @BaselineFromDate -- Want the record immediately preceding the earliest we have in the new batch collected above to act as the baseline for accruals using the new batch
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Balances','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	FromDate, 
			ToDate,
			LoadDate,
			ClientId,
			Points,
			LifetimePoints,
			PointsBalance,
			CASE Accruals WHEN 0 THEN NULL ELSE FirstAccrualDate END FirstAccrualDate,
			CASE Accruals WHEN 0 THEN NULL ELSE FirstAccrualBetId END FirstAccrualBetId,
			CASE Accruals WHEN 0 THEN NULL ELSE LastAccrualDate END LastAccrualDate,
			CASE Accruals WHEN 0 THEN NULL ELSE LastAccrualBetId END LastAccrualBetId,
			Accruals,
			LifetimePointsAccrued,
			PointsAccruedBalance,
			CASE RedemptionsBonus WHEN 0 THEN NULL ELSE FirstRedemptionBonusDate END FirstRedemptionBonusDate,
			CASE RedemptionsBonus WHEN 0 THEN NULL ELSE LastRedemptionBonusDate END LastRedemptionBonusDate,
			RedemptionsBonus,
			LifetimePointsRedeemedBonus,
			CASE RedemptionsVelocity WHEN 0 THEN NULL ELSE FirstRedemptionVelocityDate END FirstRedemptionVelocityDate,
			CASE RedemptionsVelocity WHEN 0 THEN NULL ELSE LastRedemptionVelocityDate END LastRedemptionVelocityDate,
			RedemptionsVelocity,
			LifetimePointsRedeemedVelocity,
			LifetimePointsAdjustedCredit,
			LifetimePointsAdjustedDebit
	INTO	#LifetimeRewards
	FROM	(	SELECT	FromDate, 
						LEAD(FromDate, 1, CONVERT(datetime2(3),'9999-12-31')) OVER (PARTITION BY ClientId ORDER BY FromDate) ToDate,
						LoadDate,
						ClientId,
						Points,
						SUM(LifetimePoints) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LifetimePoints,
						PointsBalance,
						MIN(FirstAccrualDate) OVER (PARTITION BY ClientId) FirstAccrualDate,
						FIRST_VALUE(FirstAccrualBetId) OVER (PARTITION BY ClientId ORDER BY FromDate) FirstAccrualBetId,
						MAX(LastAccrualDate) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LastAccrualDate,
						LAST_VALUE(LastAccrualBetId) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LastAccrualBetId,
						SUM(Accruals) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) Accruals,
						SUM(PointsAccrued) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LifetimePointsAccrued,
						PointsAccruedBalance,
						MIN(FirstRedemptionBonusDate) OVER (PARTITION BY ClientId) FirstRedemptionBonusDate,
						MAX(LastRedemptionBonusDate) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LastRedemptionBonusDate,
						SUM(RedemptionBonus) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) RedemptionsBonus,
						SUM(PointsRedeemedBonus) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LifetimePointsRedeemedBonus,
						MIN(FirstRedemptionVelocityDate) OVER (PARTITION BY ClientId) FirstRedemptionVelocityDate,
						MAX(LastRedemptionVelocityDate) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LastRedemptionVelocityDate,
						SUM(RedemptionVelocity) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) RedemptionsVelocity,
						SUM(PointsRedeemedVelocity) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LifetimePointsRedeemedVelocity,
						SUM(PointsAdjustedCredit) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LifetimePointsAdjustedCredit,
						SUM(PointsAdjustedDebit) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LifetimePointsAdjustedDebit		
				FROM	(	SELECT	FromDate,--MAX(FromDate) FromDate, 
									MAX(LoadDate) LoadDate,
									ClientId,
									SUM(Points) Points,
									SUM(LifetimePoints) LifetimePoints,
									MAX(PointsBalance) PointsBalance, -- As we only appear to have problems with sequencing of the supplied balances for accruals, OK to assume highest Balance is latest
									MIN(FirstAccrualDate) FirstAccrualDate,
									MIN(FirstAccrualBetId) FirstAccrualBetId,
									MAX(LastAccrualDate) LastAccrualDate,
									MAX(LastAccrualBetId) LastAccrualBetId,
									SUM(Accruals) Accruals,
									SUM(PointsAccrued) PointsAccrued,
									MAX(PointsAccruedBalance) PointsAccruedBalance, -- Accrued balance only ever increases to highest value is latest
									MIN(FirstRedemptionBonusDate) FirstRedemptionBonusDate,
									MAX(LastRedemptionBonusDate) LastRedemptionBonusDate,
									SUM(RedemptionBonus) RedemptionBonus,
									SUM(PointsRedeemedBonus) PointsRedeemedBonus,
									MIN(FirstRedemptionVelocityDate) FirstRedemptionVelocityDate,
									MAX(LastRedemptionVelocityDate) LastRedemptionVelocityDate,
									SUM(RedemptionVelocity) RedemptionVelocity,
									SUM(PointsRedeemedVelocity) PointsRedeemedVelocity,
									SUM(PointsAdjustedCredit) PointsAdjustedCredit,
									SUM(PointsAdjustedDebit) PointsAdjustedDebit		
							FROM	#Rewards
							GROUP BY FromDate, ClientId --CONVERT(CHAR(20), FromDate, 120), ClientId
						) y
			) z
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	r.*, CASE WHEN c.ClientId IS NULL THEN 'I' ELSE 'U' END Upsert
	INTO	#Stage
	FROM	#LifetimeRewards r
			LEFT JOIN Cache.RewardLifetime c ON c.ClientId = r.ClientID AND c.FromDate = r.FromDate
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE u 
		SET	ToDate = r.ToDate,
			LoadDate = r.LoadDate,
			Points = r.Points,
			LifetimePoints = r.LifetimePoints,
			PointsBalance = r.PointsBalance,
			FirstAccrualDate = r.FirstAccrualDate,
			FirstAccrualBetId = r.FirstAccrualBetId,
			LastAccrualDate = r.LastAccrualDate,
			LastAccrualBetId = r.LastAccrualBetId,
			Accruals = r.Accruals,
			LifetimePointsAccrued = r.LifetimePointsAccrued,
			PointsAccruedBalance = r.PointsAccruedBalance,
			FirstRedemptionBonusDate = r.FirstRedemptionBonusDate,
			LastRedemptionBonusDate = r.LastRedemptionBonusDate,
			RedemptionsBonus = r.RedemptionsBonus,
			LifetimePointsRedeemedBonus = r.LifetimePointsRedeemedBonus,
			FirstRedemptionVelocityDate = r.FirstRedemptionVelocityDate,
			LastRedemptionVelocityDate = r.LastRedemptionVelocityDate,
			RedemptionsVelocity = r.RedemptionsVelocity,
			LifetimePointsRedeemedVelocity = r.LifetimePointsRedeemedVelocity,
			LifetimePointsAdjustedCredit = r.LifetimePointsAdjustedCredit,
			LifetimePointsAdjustedDebit = r.LifetimePointsAdjustedDebit,
			ModifiedDate = CURRENT_TIMESTAMP, 
			ModifiedBatchKey = @BatchKey, 
			ModifiedBy = @Me,
			Increment = ISNULL(@Increment,0)
	FROM	Cache.RewardLifetime u
			INNER JOIN #Stage r ON r.ClientId = u.ClientId AND r.FromDate = u.FromDate AND r.Upsert = 'U'

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @RowsProcessed = @RowCount

	INSERT Cache.RewardLifetime
		(	FromDate, ToDate, LoadDate, ClientId, Points, LifetimePoints, PointsBalance, 
			FirstAccrualDate, FirstAccrualBetId, LastAccrualDate, LastAccrualBetId, Accruals, LifetimePointsAccrued, PointsAccruedBalance, 
			FirstRedemptionBonusDate, LastRedemptionBonusDate, RedemptionsBonus, LifetimePointsRedeemedBonus,
			FirstRedemptionVelocityDate, LastRedemptionVelocityDate, RedemptionsVelocity, LifetimePointsRedeemedVelocity,
			LifetimePointsAdjustedCredit, LifetimePointsAdjustedDebit,
			CreatedDate, CreatedBatchKey, CreatedBy, 
			ModifiedDate, ModifiedBatchKey, ModifiedBy,
			Increment
		) 
	SELECT	FromDate, ToDate, LoadDate, ClientId, Points, LifetimePoints, PointsBalance, 
			FirstAccrualDate, FirstAccrualBetId, LastAccrualDate, LastAccrualBetId, Accruals, LifetimePointsAccrued, PointsAccruedBalance, 
			FirstRedemptionBonusDate, LastRedemptionBonusDate, RedemptionsBonus, LifetimePointsRedeemedBonus,
			FirstRedemptionVelocityDate, LastRedemptionVelocityDate, RedemptionsVelocity, LifetimePointsRedeemedVelocity,
			LifetimePointsAdjustedCredit, LifetimePointsAdjustedDebit,
			CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, 
			CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy,
			ISNULL(@Increment,0) Increment
	FROM	#Stage
	WHERE	Upsert = 'I' 
		
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
