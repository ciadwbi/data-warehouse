CREATE PROCEDURE [Cache].[sp_InitialPopFeatureFlag]
AS
	SET XACT_ABORT ON;

	BEGIN TRANSACTION FeatureFlag

	;WITH u (	Flag,	FeatureCode		) AS
	(	SELECT	1,		'CHASE_THE_ACE'		UNION ALL
		SELECT	2,		'BET_BOOST'			UNION ALL
		SELECT	4,		'QuickBet'			UNION ALL
		SELECT	8,		'ReBet'				UNION ALL
		SELECT	16,		'NextToJump'		UNION ALL
		SELECT	32,		'NextToGo'			UNION ALL
		SELECT	64,		'HeroWidget'		UNION ALL
		SELECT	128,	'FeatureTiles'		UNION ALL
		SELECT	256,	'TrendingBets'
	)
	MERGE Cache.FeatureFlag m
	USING u ON u.Flag = m.Flag
	WHEN MATCHED AND (m.FeatureCode <> u.FeatureCode) THEN 
		UPDATE SET FeatureCode = u.FeatureCode
	WHEN NOT MATCHED THEN 
		INSERT (Flag, FeatureCode)
		VALUES (u.Flag, u.FeatureCode);

	COMMIT TRANSACTION FeatureFlag;
