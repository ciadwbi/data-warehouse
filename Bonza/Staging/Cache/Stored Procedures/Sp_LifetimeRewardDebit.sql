CREATE PROCEDURE [Cache].[Sp_LifetimeRewardDebit]
(
	@BatchKey		int = 0,
	@FromDate		datetime2(3) = NULL,
	@ToDate			datetime2(3) = NULL,
	@Increment		int = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	DECLARE @BaselineExecutedAt datetime
	DECLARE @BaselineFromDate datetime2(3)

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Dates','Start'

	SELECT @FromDate = COALESCE(@FromDate, MAX(LoadDate), CONVERT(datetime2(3),'1900-01-01')) FROM Cache.LifetimeRewardDebit
	SELECT @ToDate = COALESCE(@ToDate, MAX(FromDate)) FROM [$(Acquisition)].Messaging.RewardsPointsAdjusted

	--	So, LoadDate on the cache record is the FromDate of the Acquisition record it came from, whereas FromDate on cache is ExecutedAt on the source Acquisition record.
	--	Reasons for this are:
	--	1)	A switch in significant time-lines from the order in which the data was acquired to the order it needs to be for analysis reflected in the role of From/To date at different stages
	--	2)	A logical step in the DataVault direction for later
	--	3)	If it was hard to write, it bloody well should be hard to understand - get over it!

	-- We cannot guarantee that messages arrive in chronological order.
	-- We control the Acquisition FromDate for messages as our Load Date into Acquisition, so we can guarantee that the From Date grows chronologically and we are only looking for FromDate since the last time we loaded.
	-- However, because we cannot guarantee the ExecutedAt dates that we really care about arrive in order, there is a strong possibility we may get an ExecutedAt arriving which is in the middle of dates previouslt processed.
	-- Out of sequence messages are a problem when dealing with accumulating balances where not just the late message needs to be added, but any derived accumulating balance on all subsequent messages previously loaded may require updating.
	-- For that reason, having identified all the new messages for processing by Acquisition from date, we then need to ascertain the earliest ExecutedAt date amongst those new messages and begin reprocessing all messages,
	-- new or previous, from that date forward to ensure we slot the out of sequence messages into the correct position in the ExecutedAt timeline.

	SELECT	@BaselineExecutedAt = MIN(ExecutedAt)
	FROM	[$(Acquisition)].Messaging.RewardsPointsAdjusted
	WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate = CONVERT(datetime2(3),'9999-12-31') -- Messages never change so we can force the ToDate to a single partition until we develop message-specific Acquisition structures partitioned on FromDate
	OPTION(RECOMPILE)

	SET @BaselineFromDate = CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),@BaselineExecutedAt))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Debit','Start', @RowsProcessed = 1

	SELECT	CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) FromDate, FromDate LoadDate, ClientId, 
			- Points Points, [Current] PointsBalance, 
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) FirstRewardDebitDate, 
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) LastRewardDebitDate, 
			CONVERT(int, 1) NumberOfRewardDebits, - Points LifetimeRewardDebit
	INTO	#Debit
	FROM	[$(Acquisition)].Messaging.RewardsPointsAdjusted
	WHERE	isDuplicate = 0 AND Points < 0 AND ExecutedAt >= @BaselineExecutedAt AND FromDate <= @ToDate AND ToDate = CONVERT(datetime2(3),'9999-12-31')
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Previous','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	DISTINCT ClientId
	INTO	#Clients
	FROM	#Debit

	INSERT	#Debit
	SELECT	FromDate, LoadDate, ClientId, 
			Points, PointsBalance,
			FirstRewardDebitDate, LastRewardDebitDate, 
			NumberOfRewardDebits, LifetimeRewardDebit
	FROM	Cache.LifetimeRewardDebit
	WHERE	ClientId IN (SELECT ClientId FROM #Clients)
			AND FromDate < @BaselineFromDate AND ToDate >= @BaselineFromDate -- Want the record immediately preceding the earliest we have in the new batch collected above to act as the baseline for accruals using the new batch
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Balances','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	FromDate, 
			ToDate,
			LoadDate,
			ClientId,
			Points,
			PointsBalance,
			CASE NumberOfRewardDebits WHEN 0 THEN NULL ELSE FirstRewardDebitDate END FirstRewardDebitDate,
			CASE NumberOfRewardDebits WHEN 0 THEN NULL ELSE LastRewardDebitDate END LastRewardDebitDate,
			NumberOfRewardDebits,
			LifetimeRewardDebit
	INTO	#LifetimeRewards
	FROM	(	SELECT	FromDate, 
						LEAD(FromDate, 1, CONVERT(datetime2(3),'9999-12-31')) OVER (PARTITION BY ClientId ORDER BY FromDate) ToDate,
						LoadDate,
						ClientId,
						Points,
						PointsBalance,
						MIN(FirstRewardDebitDate) OVER (PARTITION BY ClientId) FirstRewardDebitDate,
						MAX(LastRewardDebitDate) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LastRewardDebitDate,
						SUM(NumberOfRewardDebits) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) NumberOfRewardDebits,
						SUM(LifetimeRewardDebit) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LifetimeRewardDebit
				FROM	(	SELECT	FromDate,--MAX(FromDate) FromDate, 
									MAX(LoadDate) LoadDate,
									ClientId,
									SUM(Points) Points,
									MAX(PointsBalance) PointsBalance,
									MIN(FirstRewardDebitDate) FirstRewardDebitDate,
									MAX(LastRewardDebitDate) LastRewardDebitDate,
									SUM(NumberOfRewardDebits) NumberOfRewardDebits,
									SUM(LifetimeRewardDebit) LifetimeRewardDebit
							FROM	#Debit
							GROUP BY FromDate, ClientId --CONVERT(CHAR(20), FromDate, 120), ClientId
						) y
			) z
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	r.*, CASE WHEN c.ClientId IS NULL THEN 'I' ELSE 'U' END Upsert
	INTO	#Stage
	FROM	#LifetimeRewards r
			LEFT JOIN Cache.LifetimeRewardDebit c ON c.ClientId = r.ClientID AND c.FromDate = r.FromDate
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE u 
		SET	ToDate = r.ToDate,
			LoadDate = r.LoadDate,
			Points = r.Points,
			PointsBalance = r.PointsBalance,
			FirstRewardDebitDate = r.FirstRewardDebitDate,
			LastRewardDebitDate = r.LastRewardDebitDate,
			NumberOfRewardDebits = r.NumberOfRewardDebits,
			LifetimeRewardDebit = r.LifetimeRewardDebit,
			ModifiedDate = CURRENT_TIMESTAMP, 
			ModifiedBatchKey = @BatchKey, 
			Increment = ISNULL(@Increment,0)
	FROM	Cache.LifetimeRewardDebit u
			INNER JOIN #Stage r ON r.ClientId = u.ClientId AND r.FromDate = u.FromDate AND r.Upsert = 'U'

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @RowsProcessed = @RowCount

	INSERT Cache.LifetimeRewardDebit
		(	FromDate, ToDate, LoadDate, ClientId, 
			Points, PointsBalance,
			FirstRewardDebitDate, LastRewardDebitDate, NumberOfRewardDebits, LifetimeRewardDebit,
			CreatedDate, CreatedBatchKey,
			ModifiedDate, ModifiedBatchKey,
			Increment
		) 
	SELECT	FromDate, ToDate, LoadDate, ClientId, 
			Points, PointsBalance, 
			FirstRewardDebitDate, LastRewardDebitDate, NumberOfRewardDebits, LifetimeRewardDebit,
			CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey,
			CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey,
			ISNULL(@Increment,0) Increment
	FROM	#Stage
	WHERE	Upsert = 'I' 
		
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
