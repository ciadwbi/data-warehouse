CREATE PROCEDURE [Cache].[Sp_LifetimeBonusRedemption]
(
	@BatchKey		int = 0,
	@FromDate		datetime2(3) = NULL,
	@ToDate			datetime2(3) = NULL,
	@Increment		int = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	DECLARE @BaselineExecutedAt datetime
	DECLARE @BaselineFromDate datetime2(3)

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Dates','Start'

	SELECT @FromDate = COALESCE(@FromDate, MAX(LoadDate), CONVERT(datetime2(3),'1900-01-01')) FROM Cache.LifetimeBonusRedemption
	SELECT @ToDate = COALESCE(@ToDate, MAX(FromDate)) FROM [$(Acquisition)].Messaging.RewardsBonusRedemption

	--	So, LoadDate on the cache record is the FromDate of the Acquisition record it came from, whereas FromDate on cache is ExecutedAt on the source Acquisition record.
	--	Reasons for this are:
	--	1)	A switch in significant time-lines from the order in which the data was acquired to the order it needs to be for analysis reflected in the role of From/To date at different stages
	--	2)	A logical step in the DataVault direction for later
	--	3)	If it was hard to write, it bloody well should be hard to understand - get over it!

	-- We cannot guarantee that messages arrive in chronological order.
	-- We control the Acquisition FromDate for messages as our Load Date into Acquisition, so we can guarantee that the From Date grows chronologically and we are only looking for FromDate since the last time we loaded.
	-- However, because we cannot guarantee the ExecutedAt dates that we really care about arrive in order, there is a strong possibility we may get an ExecutedAt arriving which is in the middle of dates previouslt processed.
	-- Out of sequence messages are a problem when dealing with accumulating balances where not just the late message needs to be added, but any derived accumulating balance on all subsequent messages previously loaded may require updating.
	-- For that reason, having identified all the new messages for processing by Acquisition from date, we then need to ascertain the earliest ExecutedAt date amongst those new messages and begin reprocessing all messages,
	-- new or previous, from that date forward to ensure we slot the out of sequence messages into the correct position in the ExecutedAt timeline.

	SELECT	@BaselineExecutedAt = MIN(ExecutedAt)
	FROM	[$(Acquisition)].Messaging.RewardsBonusRedemption
	WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate = CONVERT(datetime2(3),'9999-12-31') -- Messages never change so we can force the ToDate to a single partition until we develop message-specific Acquisition structures partitioned on FromDate
	OPTION(RECOMPILE)

	SET @BaselineFromDate = CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),@BaselineExecutedAt))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Bonus','Start', @RowsProcessed = 1

	SELECT	CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) FromDate, FromDate LoadDate, ClientId, 
			Points, [Current] PointsBalance, 
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) FirstBonusRedemptionDate, 
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) LastBonusRedemptionDate, 
			CONVERT(int, 1) NumberOfBonusRedemptions, Points LifetimeBonusRedemption
	INTO	#Bonus
	FROM	[$(Acquisition)].Messaging.RewardsBonusRedemption
	WHERE	isDuplicate = 0 AND ExecutedAt >= @BaselineExecutedAt AND FromDate <= @ToDate AND ToDate = CONVERT(datetime2(3),'9999-12-31')
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Previous','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	DISTINCT ClientId
	INTO	#Clients
	FROM	#Bonus

	INSERT	#Bonus
	SELECT	FromDate, LoadDate, ClientId, 
			Points, PointsBalance,
			FirstBonusRedemptionDate, LastBonusRedemptionDate, 
			NumberOfBonusRedemptions, LifetimeBonusRedemption
	FROM	Cache.LifetimeBonusRedemption
	WHERE	ClientId IN (SELECT ClientId FROM #Clients)
			AND FromDate < @BaselineFromDate AND ToDate >= @BaselineFromDate -- Want the record immediately preceding the earliest we have in the new batch collected above to act as the baseline for accruals using the new batch
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Balances','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	FromDate, 
			ToDate,
			LoadDate,
			ClientId,
			Points,
			PointsBalance,
			CASE NumberOfBonusRedemptions WHEN 0 THEN NULL ELSE FirstBonusRedemptionDate END FirstBonusRedemptionDate,
			CASE NumberOfBonusRedemptions WHEN 0 THEN NULL ELSE LastBonusRedemptionDate END LastBonusRedemptionDate,
			NumberOfBonusRedemptions,
			LifetimeBonusRedemption
	INTO	#LifetimeRewards
	FROM	(	SELECT	FromDate, 
						LEAD(FromDate, 1, CONVERT(datetime2(3),'9999-12-31')) OVER (PARTITION BY ClientId ORDER BY FromDate) ToDate,
						LoadDate,
						ClientId,
						Points,
						PointsBalance,
						MIN(FirstBonusRedemptionDate) OVER (PARTITION BY ClientId) FirstBonusRedemptionDate,
						MAX(LastBonusRedemptionDate) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LastBonusRedemptionDate,
						SUM(NumberOfBonusRedemptions) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) NumberOfBonusRedemptions,
						SUM(LifetimeBonusRedemption) OVER (PARTITION BY ClientId ORDER BY FromDate ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) LifetimeBonusRedemption
				FROM	(	SELECT	FromDate,--MAX(FromDate) FromDate, 
									MAX(LoadDate) LoadDate,
									ClientId,
									SUM(Points) Points,
									MAX(PointsBalance) PointsBalance,
									MIN(FirstBonusRedemptionDate) FirstBonusRedemptionDate,
									MAX(LastBonusRedemptionDate) LastBonusRedemptionDate,
									SUM(NumberOfBonusRedemptions) NumberOfBonusRedemptions,
									SUM(LifetimeBonusRedemption) LifetimeBonusRedemption
							FROM	#Bonus
							GROUP BY FromDate, ClientId --CONVERT(CHAR(20), FromDate, 120), ClientId
						) y
			) z
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	r.*, CASE WHEN c.ClientId IS NULL THEN 'I' ELSE 'U' END Upsert
	INTO	#Stage
	FROM	#LifetimeRewards r
			LEFT JOIN Cache.LifetimeBonusRedemption c ON c.ClientId = r.ClientID AND c.FromDate = r.FromDate
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE u 
		SET	ToDate = r.ToDate,
			LoadDate = r.LoadDate,
			Points = r.Points,
			PointsBalance = r.PointsBalance,
			FirstBonusRedemptionDate = r.FirstBonusRedemptionDate,
			LastBonusRedemptionDate = r.LastBonusRedemptionDate,
			NumberOfBonusRedemptions = r.NumberOfBonusRedemptions,
			LifetimeBonusRedemption = r.LifetimeBonusRedemption,
			ModifiedDate = CURRENT_TIMESTAMP, 
			ModifiedBatchKey = @BatchKey, 
			Increment = ISNULL(@Increment,0)
	FROM	Cache.LifetimeBonusRedemption u
			INNER JOIN #Stage r ON r.ClientId = u.ClientId AND r.FromDate = u.FromDate AND r.Upsert = 'U'

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @RowsProcessed = @RowCount

	INSERT Cache.LifetimeBonusRedemption
		(	FromDate, ToDate, LoadDate, ClientId, 
			Points, PointsBalance,
			FirstBonusRedemptionDate, LastBonusRedemptionDate, NumberOfBonusRedemptions, LifetimeBonusRedemption,
			CreatedDate, CreatedBatchKey,
			ModifiedDate, ModifiedBatchKey,
			Increment
		) 
	SELECT	FromDate, ToDate, LoadDate, ClientId, 
			Points, PointsBalance, 
			FirstBonusRedemptionDate, LastBonusRedemptionDate, NumberOfBonusRedemptions, LifetimeBonusRedemption,
			CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey,
			CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey,
			ISNULL(@Increment,0) Increment
	FROM	#Stage
	WHERE	Upsert = 'I' 
		
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
