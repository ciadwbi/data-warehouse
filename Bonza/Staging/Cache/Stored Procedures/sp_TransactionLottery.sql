CREATE PROCEDURE [Cache].[sp_TransactionLottery]
(
	@BatchKey	int = 0,
	@FromDate	datetime2(3) = NULL,
	@ToDate		datetime2(3) = NULL,
	@Increment		int = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	SELECT @FromDate = COALESCE(@FromDate,MAX(FromDate),CONVERT(datetime2(3),'1900-01-01')) FROM Cache.TransactionLottery
	SET @ToDate = COALESCE(@ToDate,GETDATE())

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Changes','Start', @Reload

	SELECT	*, 
			- Amount Stake,
			NotesXML.value('/root[1]/s[1]','uniqueidentifier') ExternalId, 
			CONVERT(uniqueidentifier,NULLIF(NotesXML.value('/root[1]/s[2]','char(36)'),'')) LinkExternalId, 
			NULLIF(NotesXML.value('/root[1]/s[3]','bigint'),0) LinkTransactionId, 
			NULLIF(NotesXML.value('/root[1]/s[4]','varchar(255)'),'') Product, 
			NULLIF(NotesXML.value('/root[1]/s[5]','decimal(38,2)'),0.00) Jackpot, 
			CONVERT(datetime2(3),SWITCHOFFSET(NULLIF(NotesXML.value('/root[1]/s[6]','datetimeoffset'),'1900-01-01 00:00:00.0000000 +00:00'),'+09:30')) DrawDate -- (convert to Darwin time from UTC)
	INTO	#Transactions
	FROM	(	SELECT	*, CONVERT(xml,' <root> <s>' + REPLACE(Notes,'||','</s> <s>') + '</s>   </root> ') NotesXML
				FROM	[$(Acquisition)].Intrabet.tblTransactions t
				WHERE	TransactionCode in (-1005,1005,1006)  
						AND TransactionId >= 709626969 
						AND FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						AND Notes LIKE '%||%'
			) x
	OPTION (RECOMPILE, TABLE HINT(t,FORCESEEK([NI_tblTransactions_FD(A)](FromDate))))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'LinkCurrent','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	UPDATE t
		SET	Stake = l.Stake,
			Product = l.Product,
			Jackpot = l.Jackpot,
			DrawDate = l.DrawDate,
			LinkTransactionID = ISNULL(t.LinkTransactionId,l.TransactionID) -- Some Link TransactionIds are null although Link ExternalId is populated - in there circumstances also update the Link TransactionId found via Link ExternalId
	FROM	#Transactions t
			INNER JOIN #Transactions l ON l.ExternalId = t.LinkExternalId AND l.TransactionCode = -1005
	WHERE	t.TransactionCode IN (1005, 1006)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'LinkOlder','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	UPDATE t
		SET	Stake = l.Stake,
			Product = l.Product,
			Jackpot = l.Jackpot,
			DrawDate = l.DrawDate,
			LinkTransactionID = ISNULL(t.LinkTransactionId,l.TransactionID) -- Some Link TransactionIds are null although Link ExternalId is populated - in there circumstances also update the Link TransactionId found via Link ExternalId
	FROM	#Transactions t
			INNER JOIN Cache.TransactionLottery l ON l.ExternalId = t.LinkExternalId AND l.TransactionCode = -1005
	WHERE	t.TransactionCode IN (1005, 1006)
			AND t.Product IS NULL

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	INSERT Cache.TransactionLottery
		(	TransactionID, ClientID, AccountNumber, TransactionDate, Amount, Stake, TransactionCode, Notes, ExternalId, LinkExternalId, LinkTransactionId, Product, Jackpot, DrawDate, FromDate, CreatedDate, CreatedBatchKey, CreatedBy, Increment)
	SELECT	TransactionID, ClientID, AccountNumber, TransactionDate, Amount, Stake, TransactionCode, Notes, ExternalId, LinkExternalId, LinkTransactionId, Product, Jackpot, DrawDate, FromDate, CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, ISNULL(@Increment, 0)
	FROM	#Transactions

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Fixes','Start', @Reload, @RowsProcessed = @RowCount

	-- Synthesise missing settlement transactions for early losing tickets
	INSERT Cache.TransactionLottery
		(	TransactionID, ClientID, AccountNumber, TransactionDate, Amount, Stake, TransactionCode, Notes, ExternalId, LinkExternalId, LinkTransactionId, Product, Jackpot, DrawDate, FromDate, CreatedDate, CreatedBatchKey, CreatedBy, Increment)
	SELECT	-x.TransactionId TransactionId, x.ClientId, x.AccountNumber, Cutoff TransactionDate, 0.00 Amount, x.Stake Stake, 1005 TransactionCode, NULL Notes, Null ExternalId, x.ExternalId LinkExternalId, x.TransactionId LinkTransactionId, x.Product, x.Jackpot, x.DrawDate, x.Cutoff FromDate, CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, ISNULL(@Increment, 0)
	FROM	(	SELECT	DATEADD(millisecond,-1,CONVERT(datetime2(3),DATEADD(day,1, CONVERT(date, DrawDate)))) Cutoff, *
				FROM	Cache.TransactionLottery
				WHERE	TransactionCode = -1005
						AND DrawDate < CONVERT(datetime2(3),'2017-07-12')
			) x
			LEFT JOIN Cache.TransactionLottery t ON t.LinkExternalId = x.ExternalId
	WHERE	Cutoff > @FromDate AND Cutoff <= @ToDate
			AND t.LinkExternalId IS NULL

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
