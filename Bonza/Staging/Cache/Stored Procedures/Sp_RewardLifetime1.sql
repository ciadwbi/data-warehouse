CREATE PROCEDURE [Cache].[Sp_RewardLifetime1]
(
	@BatchKey		int = 0,
	@FromDate		datetime2(3) = NULL,
	@ToDate			datetime2(3) = NULL,
	@Increment		int = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	DECLARE @BaselineExecutedAt datetime
	DECLARE @BaselineFromDate datetime2(3)

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Dates','Start'

	SELECT @FromDate = COALESCE(@FromDate, MAX(LoadDate), CONVERT(datetime2(3),'1900-01-01')) FROM Cache.RewardLifetime
	SELECT @ToDate = COALESCE(@ToDate, MAX(FromDate)) FROM Cache.LifetimeAccrual

	--	So, LoadDate on the cache record is the FromDate of the Acquisition record it came from, whereas FromDate on cache is ExecutedAt on the source Acquisition record.
	--	Reasons for this are:
	--	1)	A switch in significant time-lines from the order in which the data was acquired to the order it needs to be for analysis reflected in the role of From/To date at different stages
	--	2)	A logical step in the DataVault direction for later
	--	3)	If it was hard to write, it bloody well should be hard to understand - get over it!

	-- We cannot guarantee that messages arrive in chronological order.
	-- We control the Acquisition FromDate for messages as our Load Date into Acquisition, so we can guarantee that the From Date grows chronologically and we are only looking for FromDate since the last time we loaded.
	-- However, because we cannot guarantee the ExecutedAt dates that we really care about arrive in order, there is a strong possibility we may get an ExecutedAt arriving which is in the middle of dates previouslt processed.
	-- Out of sequence messages are a problem when dealing with accumulating balances where not just the late message needs to be added, but any derived accumulating balance on all subsequent messages previously loaded may require updating.
	-- For that reason, having identified all the new messages for processing by Acquisition from date, we then need to ascertain the earliest ExecutedAt date amongst those new messages and begin reprocessing all messages,
	-- new or previous, from that date forward to ensure we slot the out of sequence messages into the correct position in the ExecutedAt timeline.

	;WITH
	ImpactRange AS
		(	SELECT	DISTINCT ClientId, FromDate
			FROM	(	SELECT	ClientId, FromDate FROM Cache.LifetimeAccrual WHERE LoadDate > @FromDate AND LoadDate <= @ToDate
						UNION ALL
						SELECT	ClientId, FromDate FROM Cache.LifetimeBonusRedemption WHERE LoadDate > @FromDate AND LoadDate <= @ToDate
						UNION ALL
						SELECT	ClientId, FromDate FROM Cache.LifetimeVelocityRedemption WHERE LoadDate > @FromDate AND LoadDate <= @ToDate
						UNION ALL
						SELECT	ClientId, FromDate FROM Cache.LifetimeRewardCredit WHERE LoadDate > @FromDate AND LoadDate <= @ToDate
						UNION ALL
						SELECT	ClientId, FromDate FROM Cache.LifetimeRewardDebit WHERE LoadDate > @FromDate AND LoadDate <= @ToDate
					) x
		)
	SELECT	r.ClientId, r.FromDate
	INTO	#Events
	FROM	ImpactRange r
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Balances','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	x.ClientId, x.FromDate, 
			LEAD(x.FromDate, 1, CONVERT(datetime2(3),'9999-12-31')) OVER (PARTITION BY x.ClientId ORDER BY x.FromDate) ToDate,
			a.FirstAccrualDate, a.FirstAccrualBetId, b.FirstBonusRedemptionDate, v.FirstVelocityRedemptionDate,
			a.LastAccrualDate, a.LastAccrualBetId, b.LastBonusRedemptionDate, v.LastVelocityRedemptionDate,
			a.PointsBalance, 
			ISNULL(a.AccrualBalance, 0) AccrualBalance, ISNULL(a.NumberOfAccruals,0) NumberOfAccruals, ISNULL(a.LifetimeAccrual,0) LifetimeAccrual, 
			ISNULL(b.NumberOfBonusRedemptions,0) NumberOfBonusRedemptions, ISNULL(b.LifetimeBonusRedemption,0) LifetimeBonusRedemption, 
			ISNULL(v.NumberOfVelocityRedemptions,0) NumberOfVelocityRedemptions, ISNULL(v.LifetimeVelocityRedemption,0) LifetimeVelocityRedemption,
			ISNULL(c.LifetimeRewardCredit,0) LifetimeRewardCredit, ISNULL(LifetimeRewardDebit,0) LifetimeRewardDebit
	INTO	#LifetimeRewards
	FROM	#Events x
			LEFT JOIN Cache.LifetimeAccrual a ON a.ClientId = x.ClientId AND a.FromDate <= x.FromDate AND a.ToDate > x.FromDate
			LEFT JOIN Cache.LifetimeBonusRedemption b ON b.ClientId = x.ClientId AND b.FromDate <= x.FromDate AND b.ToDate > x.FromDate
			LEFT JOIN Cache.LifetimeVelocityRedemption v ON v.ClientId = x.ClientId AND v.FromDate <= x.FromDate AND v.ToDate > x.FromDate
			LEFT JOIN Cache.LifetimeRewardCredit c ON c.ClientId = x.ClientId AND c.FromDate <= x.FromDate AND c.ToDate > x.FromDate
			LEFT JOIN Cache.LifetimeRewardDebit d ON d.ClientId = x.ClientId AND d.FromDate <= x.FromDate AND d.ToDate > x.FromDate

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	r.*, CASE WHEN c.ClientId IS NULL THEN 'I' ELSE 'U' END Upsert
	INTO	#Stage
	FROM	#LifetimeRewards r
			LEFT JOIN Cache.RewardLifetime c ON c.ClientId = r.ClientID AND c.FromDate = r.FromDate
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE u 
		SET	ToDate = r.ToDate,
			LoadDate = r.LoadDate,
			Points = r.Points,
			PointsBalance = r.PointsBalance,
			PointsAccruedBalance = r.AccrualBalance,
			LifetimePoints = r.LifetimePoints,
			FirstAccrualDate = r.FirstAccrualDate,
			FirstAccrualBetId = r.FirstAccrualBetId,
			LastAccrualDate = r.LastAccrualDate,
			LastAccrualBetId = r.LastAccrualBetId,
			Accruals = r.NumberOfAccruals,
			LifetimePointsAccrued = r.LifetimeAccrual,
			FirstRedemptionBonusDate = r.FirstBonusRedemptionDate,
			LastRedemptionBonusDate = r.LastBonusRedemptionDate,
			RedemptionsBonus = r.NumberOfBonusRedemptions,
			LifetimePointsRedeemedBonus = r.LifetimeBonusRedemption,
			FirstRedemptionVelocityDate = r.FirstVelocityRedemptionDate,
			LastRedemptionVelocityDate = r.LastVelocityRedemptionDate,
			RedemptionsVelocity = r.NumberOfVelocityRedemptions,
			LifetimePointsRedeemedVelocity = r.LifetimeVelocityRedemption,
			LifetimePointsAdjustedCredit = r.LifetimeRewardCredit,
			LifetimePointsAdjustedDebit = r.LifetimeRewardDebit,
			ModifiedDate = CURRENT_TIMESTAMP, 
			ModifiedBatchKey = @BatchKey, 
			ModifiedBy = @Me,
			Increment = ISNULL(@Increment,0)
	FROM	Cache.RewardLifetime u
			INNER JOIN #Stage r ON r.ClientId = u.ClientId AND r.FromDate = u.FromDate AND r.Upsert = 'U'

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @RowsProcessed = @RowCount

	INSERT Cache.RewardLifetime
		(	FromDate, ToDate, LoadDate, ClientId, 
			Points, PointsBalance, PointsAccruedBalance, LifetimePoints, 
			FirstAccrualDate, FirstAccrualBetId, LastAccrualDate, LastAccrualBetId, Accruals, LifetimePointsAccrued, 
			FirstRedemptionBonusDate, LastRedemptionBonusDate, RedemptionsBonus, LifetimePointsRedeemedBonus,
			FirstRedemptionVelocityDate, LastRedemptionVelocityDate, RedemptionsVelocity, LifetimePointsRedeemedVelocity,
			LifetimePointsAdjustedCredit, LifetimePointsAdjustedDebit,
			CreatedDate, CreatedBatchKey, CreatedBy, 
			ModifiedDate, ModifiedBatchKey, ModifiedBy,
			Increment
		) 
	SELECT	FromDate, ToDate, LoadDate, ClientId, 
			Points, PointsBalance, AccrualBalance, LifetimePoints, 
			FirstAccrualDate, FirstAccrualBetId, LastAccrualDate, LastAccrualBetId, NumberOfAccruals, LifetimeAccrual, 
			FirstBonusRedemptionDate, LastBonusRedemptionDate, NumberOfBonusRedemptions, LifetimeBonusRedemption,
			FirstVelocityRedemptionDate, LastVelocityRedemptionDate, NumberOfVelocityRedemptions, LifetimeVelocityRedemption,
			LifetimeRewardCredit, LifetimeRewardDebit,
			CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, 
			CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy,
			ISNULL(@Increment,0) Increment
	FROM	#Stage
	WHERE	Upsert = 'I' 
		
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
