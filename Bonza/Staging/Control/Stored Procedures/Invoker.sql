CREATE PROCEDURE [Control].[Invoker]
(	
	@ProcedureName	varchar(128),		-- Mandatory - Nominates an ETL stored procedure which is to be run for the specified BatchKey
	@BatchKey		int,				-- Mandatory - Specifies the Batch key for the batch to be run
	@Increment		int = NULL,			-- Optional - Specifies an increment is a sub-batch is to be run
	@MappingId		int = NULL,			-- Optional - Specifies a specific sub-area of the ETL process identified by a numeric mapping id
	@FromDate		datetime2(3) = NULL,-- Optional - Specifies a @FromDate parameter for the invoked stored procedure, either because of an adhoc run where there is no ETLController record for the BatchKey or to override the values in the record 
	@ToDate			datetime2(3) = NULL,-- Optional - Specifies a @ToDate parameter for the invoked stored procedure, either because of an adhoc run where there is no ETLController record for the BatchKey or to override the values in the record
	@DryRun			bit = 0,			-- Optional - (0=FALSE/1=TRUE) - Allows the procedure to be dry run whereby it will report all of its intended actions but not actually execute them
	@RowsProcessed	int = 0 OUTPUT		-- Optional - returns the number of material rows processed as provided by the invoked stored procedure (if supported by the particular procedure, otherwise zero)
)
AS

	SET NOCOUNT ON 

	DECLARE	@i int,
			@SQL nvarchar(MAX)

	DECLARE @BulkMode bit

BEGIN TRY

	SET @ProcedureName = OBJECT_SCHEMA_NAME(OBJECT_ID(@ProcedureName))+'.'+OBJECT_NAME(OBJECT_ID(@ProcedureName))

	-- Retrieve FromDate and ToDate from the Increment or ETLController record if not specified as parameters to this procedure. 
	-- Factor in that the reload parameter may have been set and the FromDate replaced with the start of time, but only if an explicit FromDate has NOT been provided as a parameter (an explicit FromDate parameter has precedence over an implicit reload FromDate)
	SELECT	@FromDate = COALESCE(@FromDate, ReloadFromDate, IncrementFromDate, BatchFromDate, AllIncrementFromDate, CONVERT(datetime2(3),NULL)), 
			@ToDate = COALESCE(@ToDate, IncrementToDate, BatchToDate, AllIncrementToDate, CONVERT(datetime2(3),NULL)) 
	FROM	(SELECT MIN(IncrementFromDate) IncrementFromDate, MAX(IncrementToDate) IncrementToDate FROM [Control].Increment WHERE BatchKey = @BatchKey AND Increment = @Increment) x,
			(SELECT MAX(BatchFromDate) BatchFromDate, MAX(BatchToDate) BatchToDate FROM [Control].ETLController WHERE BatchKey = @BatchKey AND @Increment IS NULL) y,
			(SELECT MIN(IncrementFromDate) AllIncrementFromDate, MAX(IncrementToDate) AllIncrementToDate FROM [Control].Increment WHERE BatchKey = @BatchKey AND @Increment IS NULL) x1,
			(SELECT CASE [Control].ParameterValue('Reload',@ProcedureName) WHEN 'Yes' THEN CONVERT(datetime2(3),'1900-01-01') ELSE CONVERT(datetime2(3),NULL) END ReloadFromDate) z

	-- Decide on whether elapsed period between FromDate and ToDate justifies switching to BULK mode by enabling Plan Guides
	SET @i = DATEDIFF(day,@FromDate,@ToDate)
	EXEC [Control].BulkPlanGuide @ProcedureName, @i

	-- Invoke the actual procedure
	-- Build up the stored procedure call using only the standard parameters defined for the procedure according to the metadata
	SET @SQL = @ProcedureName+' @BatchKey=@BatchKey'
	IF EXISTS(SELECT 1 FROM sys.parameters where object_id = OBJECT_ID(@ProcedureName) AND name = '@Increment') 
		SET @SQL = @SQL+', @Increment=@Increment'
	IF EXISTS(SELECT 1 FROM sys.parameters where object_id = OBJECT_ID(@ProcedureName) AND name = '@MappingId') 
		SET @SQL = @SQL+', @MappingId=@MappingId'
	IF EXISTS(SELECT 1 FROM sys.parameters where object_id = OBJECT_ID(@ProcedureName) AND name = '@FromDate') 
		SET @SQL = @SQL+', @FromDate=@FromDate'
	IF EXISTS(SELECT 1 FROM sys.parameters where object_id = OBJECT_ID(@ProcedureName) AND name = '@ToDate') 
		SET @SQL = @SQL+', @ToDate=@ToDate'
	IF EXISTS(SELECT 1 FROM sys.parameters where object_id = OBJECT_ID(@ProcedureName) AND name = '@DryRun') 
		SET @SQL = @SQL+', @DryRun=@DryRun'
	IF EXISTS(SELECT 1 FROM sys.parameters where object_id = OBJECT_ID(@ProcedureName) AND name = '@RowsProcessed') 
		SET @SQL = @SQL+', @RowsProcessed=@RowsProcessed OUTPUT'

	-- Having built up the procedure call string using only the required parameters, we can now just nominate all possible standard parameters in the actual call to execute and only the ones specified in the
	-- procedure call string will be used; the rest just ignored - which avoids a whole repeat of the above conditional logic to only specify the same required parameters below 
	IF @DryRun = 0 OR EXISTS(SELECT 1 FROM sys.parameters where object_id = OBJECT_ID(@ProcedureName) AND name = '@DryRun')
		BEGIN
			EXECUTE sp_executesql @SQL, N'@BatchKey int, @Increment int, @MappingId int, @FromDate datetime2(3), @ToDate datetime2(3), @DryRun bit, @RowsProcessed int OUTPUT', 
										@BatchKey = @BatchKey, @Increment = @Increment, @MappingId = @MappingId, @FromDate = @FromDate, @ToDate = @ToDate, @DryRun = @DryRun, @RowsProcessed = @RowsProcessed OUTPUT
			EXECUTE Control.ParameterSet 'Reload', @ProcedureName, 'No'
		END
	ELSE
		SELECT @SQL SQL, @BatchKey BatchKey, @Increment Increment, @MappingId MappingId, @FromDate FromDate, @ToDate ToDate

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcedureName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
