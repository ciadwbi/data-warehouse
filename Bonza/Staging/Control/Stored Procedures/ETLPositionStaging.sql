﻿CREATE PROCEDURE [Control].[ETLPositionStaging]
	(
		@BatchKey INT = NULL
	)

AS 

	DECLARE	@RowsProcessed				INT = 0

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRAN ETLPositionStaging 

BEGIN TRY

	IF ISNUMERIC(@BatchKey) <> 1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(PositionStagingStatus,0)=0
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.RecursiveStatus,0) = 1) -- Recursive completed successfully in current batch
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.DimensionStatus,0) = 1) -- Unfortunately, currently dependent upon dimension being loaded before staging can take place!!
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.PositionStagingStatus,0) <> 1) -- No failed position staging steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.TransactionStatus,0) <> 1) -- No failed fact transaction steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.PositionFactStatus,0) <> 1) -- No failed fact position steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden

	IF ISNUMERIC(@BatchKey)=1																									-- Only Load [ETLPositionStaging] after TransactionDimension is loaded.
	BEGIN
	
		UPDATE [Control].[ETLController] SET PositionStagingStartDate=GETDATE() WHERE BatchKey=@BatchKey				-- Set StartTime for Position Load

		COMMIT TRAN ETLPositionStaging -- Commit the ETL Controller initiation and switch to READ UNCOMMITTED for rest of processing with no contention in Staging/Dimensional
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		BEGIN TRAN ETLPositionStaging

		EXEC [Intrabet].[Sp_PositionStaging] @BatchKey, @RowsProcessed OUTPUT
		EXEC [Recursive].[Sp_Position_FactCheck] @BatchKey, @RowsProcessed OUTPUT
			
		UPDATE [Control].[ETLController] SET PositionStagingEndDate=GETDATE(), PositionStagingStatus=1, PositionStagingRowsProcessed=@RowsProcessed WHERE BatchKey=@BatchKey -- Set 'Success' RunStatus for Position Load

	END

	COMMIT TRAN ETLPositionStaging

END TRY
		
BEGIN CATCH
DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLPositionStaging

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - PositionStaging ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;

	UPDATE [Control].[ETLController] SET PositionStagingEndDate=GETDATE(), PositionStagingStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
	THROW;

END CATCH
