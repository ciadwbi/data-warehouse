CREATE Procedure [Control].[ETL]
(
	@BatchKey int				= NULL,
	@Schema varchar(255)		= NULL,
	@NotSchema varchar(255)		= NULL,
	@Procedure varchar(255)		= NULL,
	@NotProcedure varchar(255)	= NULL,
	@Step varchar(255)			= NULL,
	@NotStep varchar(255)		= NULL,
	@FromDateTime datetime2(3)	= NULL,
	@ToDateTime datetime2(3)	= NULL,
	@RowLimit int				= 10000
)
as
SELECT	TOP (@RowLimit)
		LogID,
		BatchKey,
		REPLACE(SUBSTRING([Procedure],1,CHARINDEX('.',[Procedure])),'.','') [Schema],
		REPLACE(SUBSTRING([Procedure],CHARINDEX('.',[Procedure]),255),'.','') [Procedure],
		Step,
		Status,
		StartDateTime,
		EndDateTime,
		DATEDIFF(s,StartDateTime,ISNULL(EndDateTime,GETDATE())) as Elapsed_s,
		ErrorMessage Message,
		RowsProcessed
FROM	Control.Log
WHERE	(BatchKey = @BatchKey OR @BatchKey IS NULL)
		AND (REPLACE(SUBSTRING([Procedure],1,CHARINDEX('.',[Procedure])),'.','') LIKE @Schema OR @Schema IS NULL)
		AND (REPLACE(SUBSTRING([Procedure],1,CHARINDEX('.',[Procedure])),'.','') NOT LIKE @NotSchema OR @NotSchema IS NULL)
		AND (REPLACE(SUBSTRING([Procedure],CHARINDEX('.',[Procedure]),255),'.','') LIKE @Procedure OR @Procedure IS NULL)
		AND (REPLACE(SUBSTRING([Procedure],CHARINDEX('.',[Procedure]),255),'.','') NOT LIKE @NotProcedure OR @NotProcedure IS NULL)
		AND (Step LIKE @Step OR @Step IS NULL)
		AND (Step NOT LIKE @NotStep OR @NotStep IS NULL)
		AND (StartDateTime >= @FromDateTime OR @FromDateTime IS NULL)
		AND (StartDateTime <= @ToDateTime OR @ToDateTime IS NULL)
ORDER BY LogID DESC

