﻿CREATE PROCEDURE [Control].[ETLDimension]
(
	@BatchKey int	= NULL
)
AS 


	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    BEGIN TRAN ETLDimension 

BEGIN TRY

	IF ISNUMERIC(@BatchKey) <> 1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(DimensionStatus,0)=0
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.StagingStatus,0) = 1) -- Staging completed successfully in current batch
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.DimensionRecursiveStatus,0) = 1) -- Dimension Recursive completed successfully in current batch
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.DimensionStatus,0) <> 1) -- No failed dimension steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden

	IF ISNUMERIC(@BatchKey) = 1	-- Only Run if a suitable batch has been nominated or identified above
	BEGIN

		UPDATE [Control].[ETLController] SET DimensionStartDate=GETDATE() WHERE BatchKey=@BatchKey													-- Set StartTime for Dimension

		COMMIT TRAN ETLDimension -- Commit the ETL Controller initiation and switch to READ UNCOMMITTED for rest of processing with no contention in Staging/Dimensional

		-- Disable indexes on Market and Event dimensions which have been converted to direct update in one step
		BEGIN TRY ALTER INDEX [CI_VenueStateId(A)] ON [$(Dimensional)].[SnowFlake].[Event_VenueState] DISABLE END TRY BEGIN CATCH END CATCH
		BEGIN TRY ALTER INDEX [CI_VenueTypeId(A)] ON [$(Dimensional)].[SnowFlake].[Event_VenueType] DISABLE END TRY BEGIN CATCH END CATCH
		BEGIN TRY ALTER INDEX [NC_Event] ON [$(Dimensional)].[Dimension].[Event] DISABLE END TRY BEGIN CATCH END CATCH
		BEGIN TRY ALTER INDEX [NC_Market] ON [$(Dimensional)].[Dimension].[Market] DISABLE END TRY BEGIN CATCH END CATCH

		SET TRANSACTION ISOLATION LEVEL SNAPSHOT
		BEGIN TRAN ETLDimension 

		EXEC [Dimension].Sp_DimDay					@BatchKey
		EXEC [Dimension].Sp_DimDayZone				@BatchKey

		EXEC [Dimension].[Sp_DimPromotion]			@BatchKey
		EXEC [Dimension].[Sp_DimBetType]			@BatchKey
		EXEC [Dimension].[Sp_Contract]				@BatchKey
		EXEC [Dimension].[Sp_DimAccount]			@BatchKey
		EXEC [Dimension].[Sp_DimAccountStatus]		@BatchKey

		EXEC [Dimension].[Sp_DimEvent]				@BatchKey
		EXEC [Dimension].[Sp_DimMarket]				@BatchKey
		EXEC [Control].[Invoker]	'Dimension.Sp_Market', @BatchKey -- new market super dimension... one dimension to bind them all..Market, Event, Class...
		EXEC [Dimension].[Sp_DimInstrument]			@BatchKey 

		EXEC [Dimension].[Sp_DimNote]				@BatchKey
		EXEC [Dimension].[Sp_DimUser]				@BatchKey
		EXEC [Dimension].[Sp_DimClass]				@BatchKey
		EXEC [Dimension].[Sp_DimChannel]			@BatchKey

--		EXEC [Dimension].[Sp_DimStructure]			@BatchKey
		
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

		EXEC [Cleanse].[Sp_Cleanse_ALL]				@BatchKey 

		EXEC [Snowflake].[Sp_Snowflake]				@BatchKey

		COMMIT TRAN ETLDimension -- Commit the main body of work and rebuild indexes before switching to seperate transaction to update status - not ideal but only way to handle index rebuiid until something more permanent
			

		-- Indexes must be re-enabled before status flag is set, otherwise subsequent steps accessing the dimensional tables once the flag is set will deadlock with the index re-enable.
		-- Written to do nothing if any of the create indexes fails - don't want to abandon the entire ETL
		BEGIN TRY ALTER INDEX [CI_VenueStateId(A)] ON [$(Dimensional)].[SnowFlake].[Event_VenueState] REBUILD END TRY BEGIN CATCH END CATCH
		BEGIN TRY ALTER INDEX [CI_VenueTypeId(A)] ON [$(Dimensional)].[SnowFlake].[Event_VenueType] REBUILD END TRY BEGIN CATCH END CATCH
		BEGIN TRY ALTER INDEX [NC_Event] ON [$(Dimensional)].[Dimension].[Event] REBUILD END TRY BEGIN CATCH END CATCH
		BEGIN TRY ALTER INDEX [NC_Market] ON [$(Dimensional)].[Dimension].[Market] REBUILD END TRY BEGIN CATCH END CATCH

		BEGIN TRAN ETLDimension 

		UPDATE [Control].[ETLController] SET DimensionEndDate = GETDATE(), DimensionStatus = 1 WHERE BatchKey = @BatchKey																-- Set 'Success' RunStatus for Dimension
			
		COMMIT TRAN ETLDimension -- Commit the status update and switch to seperate transaction to update stats and rows processed, which are optional
		BEGIN TRAN ETLDimension 

		EXEC [$(Dimensional)].Control.SP_DWUpdateStats 'Dimension'
			
		UPDATE [Control].[ETLController] 
		SET DimensionRowsProcessed=	(SELECT SUM(CNT)
									FROM 
									(	SELECT COUNT(*) AS CNT FROM [$(Dimensional)].Dimension.Account WHERE ModifiedBatchKey=@BatchKey UNION ALL
										SELECT COUNT(*) FROM [$(Dimensional)].Dimension.BetType WHERE ModifiedBatchKey=@BatchKey UNION ALL
										SELECT COUNT(*) FROM [$(Dimensional)].Dimension.[Contract] WHERE ModifiedBatchKey=@BatchKey UNION ALL
										SELECT COUNT(*) FROM [$(Dimensional)].Dimension.Promotion WHERE ModifiedBatchKey=@BatchKey
									) X)
		WHERE BatchKey=@BatchKey	

		UPDATE [$(Dimensional)].[Control].CompleteDate	SET DimensionCompleteDate = ISNULL((SELECT BatchToDate FROM [Control].[ETLController] WHERE BatchKey = @BatchKey),DimensionCompleteDate)

	END

	COMMIT TRAN ETLDimension
			
END TRY

BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLDimension

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - Dimension ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;

	UPDATE [Control].[ETLController] SET DimensionEndDate=GETDATE(), DimensionStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
	THROW;

END CATCH
