CREATE PROCEDURE [Control].[ETLCache]
(
	@BatchKey int	= NULL
)
AS 

	DECLARE	@FromDate		DATETIME,
	 		@ToDate			DATETIME,
			@RowsProcessed	INT = 0

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
	BEGIN TRAN ETLCache

BEGIN TRY
	
	IF ISNUMERIC(@BatchKey)<>1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(CacheStatus,0)=0
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.CacheStatus,0) <> 1) -- No failed cache steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c2 WHERE c2.BatchKey < c.BatchKey AND ISNULL(c2.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden

	IF ISNUMERIC(@BatchKey)=1	-- Only Run Atomic if there is an atomic step that needs to run and the previous batch has finished
	BEGIN
		SELECT @FromDate= BatchFromDate, @ToDate = BatchToDate FROM [Control].[ETLController] WHERE BatchKey=@BatchKey

		UPDATE [Control].[ETLController] SET CacheStartDate=GETDATE() WHERE BatchKey=@BatchKey		-- Set StartTime for Atomic

		COMMIT TRAN ETLCache -- Commit the ETL Controller initiation and switch to SNAPSHOT for rest of processing due to contention issues and phantom updates from constantly updated Acquisition
		SET TRANSACTION ISOLATION LEVEL SNAPSHOT
		BEGIN TRAN ETLCache
                     
		EXEC [Control].[Invoker]	'Cache.sp_ZoneDay',						@BatchKey, @RowsProcessed = @RowsProcessed OUTPUT 

		EXEC [Control].[Invoker]	'Cache.Sp_RewardLifetime',				@BatchKey, @RowsProcessed = @RowsProcessed OUTPUT 
		EXEC [Control].[Invoker]	'Cache.Sp_LifetimeAccrual',				@BatchKey, @RowsProcessed = @RowsProcessed OUTPUT 
		EXEC [Control].[Invoker]	'Cache.Sp_LifetimeBonusRedemption',		@BatchKey, @RowsProcessed = @RowsProcessed OUTPUT 
		EXEC [Control].[Invoker]	'Cache.Sp_LifetimeRewardCredit',		@BatchKey, @RowsProcessed = @RowsProcessed OUTPUT 
		EXEC [Control].[Invoker]	'Cache.Sp_LifetimeRewardDebit',			@BatchKey, @RowsProcessed = @RowsProcessed OUTPUT 
		EXEC [Control].[Invoker]	'Cache.Sp_LifetimeVelocityRedemption',	@BatchKey, @RowsProcessed = @RowsProcessed OUTPUT 

		EXEC [Control].[Invoker]	'Cache.Sp_TransactionLottery',			@BatchKey, @RowsProcessed = @RowsProcessed OUTPUT 

		UPDATE [Control].[ETLController] SET CacheEndDate=GETDATE(), CacheStatus=1, CacheRowsProcessed=@RowsProcessed WHERE BatchKey=@BatchKey         -- Set EndTime for Atomic
	END         
		
	COMMIT TRAN ETLCache

END TRY
BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLCache

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - ETL Cache Failed,check Cache.Log for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;
				
    UPDATE [Control].[ETLController] SET CacheEndDate=GETDATE(), CacheStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
    THROW
END CATCH
