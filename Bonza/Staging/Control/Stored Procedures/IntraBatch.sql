CREATE PROCEDURE [Control].[IntraBatch]
(
	@BatchKey INT		= NULL,
	@Increment INT		= NULL,
	@DryRun bit			= 0
)

AS 
	
	DECLARE @FromDate datetime2(3),
			@ToDate datetime2(3),
			@SafeDate datetime2(3),
			@Rerun bit

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
		
BEGIN TRY

	SELECT	@SafeDate = MIN(ToDate)
	FROM	(	SELECT ToDate FROM [$(Acquisition)].IntraBet.CompleteDate
				UNION ALL
				SELECT ToDate FROM [$(Acquisition)].PricingData.CompleteDate
			) x

	IF @BatchKey IS NULL AND @Increment IS NULL -- Default behaviour - define the next increment
		BEGIN
			SELECT @Rerun = 0, @BatchKey = MAX(BatchKey) + 1 FROM [Control].[ETLController] WHERE BatchStatus = 1
			SELECT @FromDate = MAX(FromDate), @ToDate = Min(ToDate)
			FROM	(	SELECT BatchToDate FromDate, CONVERT(datetime2(3),NULL) ToDate FROM [Control].[ETLController] WHERE BatchKey = @BatchKey - 1
						UNION ALL
						SELECT IncrementToDate FromDate, CONVERT(datetime2(3),NULL) ToDate FROM [Control].Increment WHERE BatchKey = @BatchKey AND IncrementStatus = 1
						UNION ALL
						SELECT CONVERT(datetime2(3),NULL) FromDate, BatchToDate ToDate FROM [Control].[ETLController] WHERE BatchKey = @BatchKey
						UNION ALL
						SELECT CONVERT(datetime2(3),NULL) FromDate, @SafeDate ToDate
					) x
		END
	ELSE IF @BatchKey IS NOT NULL AND @Increment IS NOT NULL -- Rerun a simgle specified increment
		SELECT @Rerun = 1, @FromDate = IncrementFromDate, @ToDate = IncrementToDate FROM [Control].Increment WHERE	BatchKey = @BatchKey AND Increment = @Increment
	ELSE IF @BatchKey IS NOT NULL AND @Increment IS NULL -- rerun an entire batch, either the full batch if defined or if not as many increments as have already been defined for that batch
		SELECT @Rerun = 1, @FromDate = MIN(FromDate), @ToDate = MAX(ToDate)
		FROM (	SELECT BatchFromDate FromDate, BatchToDate ToDate FROM [Control].[ETLController] WHERE BatchKey = @BatchKey AND BatchStatus = 1
				UNION ALL
				SELECT IncrementFromDate FromDate, IncrementToDate ToDate FROM [Control].[Increment] WHERE BatchKey = @BatchKey AND IncrementStatus = 1
			) x
	ELSE
		SELECT @Rerun = 1, @FromDate = CONVERT(datetime2(3),NULL),@ToDate = CONVERT(datetime2(3),NULL) 

	SELECT @Rerun, @BatchKey, @Increment, @FromDate, @ToDate WHERE @DryRun = 1

	IF @ToDate > @FromDate
		BEGIN	
			IF @Rerun = 0 -- Only create an Increment record for a new Increment as opposed to a rerun
				BEGIN
					INSERT Control.Increment (BatchKey, IncrementStatus, IncrementFromDate, IncrementToDate, IncrementStartDate, IncrementEndDate)
					VALUES (@BatchKey, 0, @FromDate, @ToDate, SYSDATETIME(), NULL)
					SET @Increment = @@IDENTITY
				END

			SET TRANSACTION ISOLATION LEVEL SNAPSHOT
			BEGIN TRAN IntraBatch

			EXEC [Control].[Invoker]	'Cache.sp_ZoneDay',			@BatchKey, @Increment, @DryRun = @DryRun
			EXEC [Control].[Invoker]	'Cache.Sp_RewardLifetime',	@BatchKey, @Increment, @DryRun = @DryRun

			EXEC [Control].[Invoker]	'Dimension.sp_Account',		@BatchKey, @Increment, @DryRun = @DryRun
			EXEC [Control].[Invoker]	'Dimension.sp_Structure',	@BatchKey, @Increment, @DryRun = @DryRun -- NOTE - Has to follow Account dimension as it currently reuses AccountKey
			EXEC [Control].[Invoker]	'Cleanse.sp_Structure',		@BatchKey, @Increment, @DryRun = @DryRun
			EXEC [Control].[Invoker]	'Cleanse.sp_Account',		@BatchKey, @Increment, @DryRun = @DryRun -- NOTE - This has to follow the clensing of the Structure dimension above as the results of that are propogated into the account dimension

			COMMIT TRAN IntraBatch
			SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
			BEGIN TRAN IntraBatch

			UPDATE [Control].Increment SET IncrementStatus = 1, IncrementEndDate = SYSDATETIME() WHERE Increment = @Increment AND @DryRun = 0
			UPDATE [$(Dimensional)].[Control].CompleteDate	SET IntraBatchCompleteDate = @ToDate WHERE @DryRun = 0

			COMMIT TRAN IntraBatch
		END
END TRY

BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN IntraBatch

	UPDATE [Control].Increment SET IncrementStatus = -1, IncrementEndDate = SYSDATETIME(), ErrorMessage = @ErrorMsg WHERE Increment = @Increment

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + @@SERVERNAME + ' - IntraBatch Failed'

	EXEC msdb.dbo.sp_send_dbmail
		@profile_name = 'DataWarehouse_Auto_Mails',
		@recipients = 'bi.Service@williamhill.com.au',
		@body			=  @ErrorMsg,
		@subject		=  @EmailSubject;

	THROW;
END CATCH
