﻿CREATE PROCEDURE [Control].[ETLContactFactEmail]
	(
		@BatchKey	int	= NULL
	)
AS 

BEGIN
	
		BEGIN TRY

			IF ISNUMERIC(@BatchKey)<>1																								
				SELECT @BatchKey=MIN(BatchKey) FROM [Control].[ETLController] c WHERE ContactEmailStagingStatus=1 AND ContactDimensionRecursiveStatus=1 AND ISNULL(ContactEmailFactStatus,0)=0
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.ContactEmailFactStatus,0) <> 1)
			
			IF ISNUMERIC(@BatchKey)=1																								
			BEGIN
	
				UPDATE [Control].[ETLController] SET ContactEmailFactStartDate=GETDATE() WHERE BatchKey=@BatchKey	

				EXEC [Dimension].[Sp_DimCommunication_Email] @BatchKey
				EXEC [Dimension].[Sp_ContactFactLoad_Email] @BatchKey
							
				UPDATE [Control].[ETLController] SET ContactEmailFactEndDate=GETDATE() WHERE BatchKey=@BatchKey
				UPDATE [Control].[ETLController] SET ContactEmailFactStatus=1			 WHERE BatchKey=@BatchKey
				
				UPDATE [Control].[ETLController]
				SET ContactEmailFactRowsProcessed=(SELECT COUNT(*) FROM [$(Dimensional)].FACT.Contact c WITH(NOLOCK) 
																	inner join [$(Dimensional)].Dimension.InteractionType I on I.InteractiontypeKey=c.InteractionTypeKey and I.InteractionTypeSource='ETE'
																	 WHERE C.ModifiedBatchKey=@BatchKey) 
				WHERE BatchKey=@BatchKey
				
				UPDATE STATISTICS [$(Dimensional)].FACT.Contact																	

				UPDATE [$(Dimensional)].Control.CompleteDate SET ExactTargetEmailCompleteDate = ISNULL((SELECT BatchToDate FROM [Control].[ETLController] WHERE BatchKey = @BatchKey),ExactTargetEmailCompleteDate)
				
			END

		END TRY
		
		BEGIN CATCH
			UPDATE [Control].[ETLController] SET ContactEmailFactEndDate=GETDATE()	WHERE BatchKey=@BatchKey	
			UPDATE [Control].[ETLController] SET ContactEmailFactStatus=-1			WHERE BatchKey=@BatchKey;
			UPDATE [Control].[ETLController] SET ErrorMessage = ERROR_MESSAGE() WHERE BatchKey=@BatchKey;
			THROW;
		END CATCH

		
END

GO


