﻿CREATE PROCEDURE [Control].[ETLContactFactSms]
	(
		@BatchKey	int	= NULL
	)
AS 

BEGIN
	
		BEGIN TRY

			IF ISNUMERIC(@BatchKey)<>1																								
				SELECT @BatchKey=MIN(BatchKey) FROM [Control].[ETLController] c WHERE ContactSmsStagingStatus=1 AND ISNULL(ContactSmsFactStatus,0)=0
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.ContactSmsFactStatus,0) <> 1)
			
			IF ISNUMERIC(@BatchKey)=1																								
			BEGIN
	
				UPDATE [Control].[ETLController] SET ContactSmsFactStartDate=GETDATE() WHERE BatchKey=@BatchKey	

				EXEC [Dimension].[Sp_DimCommunication_Sms] @BatchKey
				EXEC [Dimension].[Sp_ContactFactLoad_Sms] @BatchKey
							
				UPDATE [Control].[ETLController] SET ContactSmsFactEndDate=GETDATE() WHERE BatchKey=@BatchKey
				UPDATE [Control].[ETLController] SET ContactSmsFactStatus=1			 WHERE BatchKey=@BatchKey
				
				UPDATE [Control].[ETLController]
				SET ContactSmsFactRowsProcessed=(SELECT COUNT(*) FROM [$(Dimensional)].FACT.Contact c WITH(NOLOCK) 
																	inner join [$(Dimensional)].Dimension.InteractionType I on I.InteractiontypeKey=c.InteractionTypeKey and I.InteractionTypeSource='ETS'
																	 WHERE C.ModifiedBatchKey=@BatchKey) 
				WHERE BatchKey=@BatchKey
				
				UPDATE STATISTICS [$(Dimensional)].FACT.Contact																	

				UPDATE [$(Dimensional)].Control.CompleteDate SET ExactTargetSmsCompleteDate = ISNULL((SELECT BatchToDate FROM [Control].[ETLController] WHERE BatchKey = @BatchKey),ExactTargetSmsCompleteDate)
				
			END

		END TRY
		
		BEGIN CATCH
			UPDATE [Control].[ETLController] SET ContactSmsFactEndDate=GETDATE()	WHERE BatchKey=@BatchKey	
			UPDATE [Control].[ETLController] SET ContactSmsFactStatus=-1			WHERE BatchKey=@BatchKey;
			UPDATE [Control].[ETLController] SET ErrorMessage = ERROR_MESSAGE() WHERE BatchKey=@BatchKey;
			THROW;
		END CATCH

		
END

GO


