﻿CREATE PROCEDURE [Control].[ETLTransaction]
(
	@BatchKey		int	= NULL
)
AS 

	DECLARE @RowsProcessed int = 0 

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    BEGIN TRAN ETLTransaction
	
BEGIN TRY

	IF ISNUMERIC(@BatchKey)<>1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(TransactionStatus,0)=0
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.RecursiveStatus,0) = 1) -- Recursive completed successfully in current batch
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.DimensionStatus,0) = 1) -- Dimension completed successfully in current batch
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.TransactionStatus,0) <> 1) -- No failed Transaction Fact steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c2 WHERE c2.BatchKey < c.BatchKey AND ISNULL(c2.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden

	IF ISNUMERIC(@BatchKey)=1																							-- Only LoadKPI after TransactionDimension is loaded.
	BEGIN
	
		UPDATE [Control].[ETLController] SET TransactionStartDate=GETDATE() WHERE BatchKey=@BatchKey					-- Set StartTime for KPI Load

		COMMIT TRAN ETLTransaction -- Commit the ETL Controller initiation and switch to READ UNCOMMITTED for rest of processing with no contention in Staging/Dimensional
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		BEGIN TRAN ETLTransaction 

		EXEC [Control].[Invoker]	'Fact.Sp_Cashflow1', @BatchKey, @RowsProcessed = @RowsProcessed	OUTPUT
		EXEC [Control].[Invoker]	'Fact.Sp_Cashflow', @BatchKey
		EXEC [Control].[Invoker]	'Fact.Sp_Reward', @BatchKey

		--EXEC Dimension.Sp_TransactionFactLoad @Batchkey, @RowsProcessed	OUTPUT												-- Run KPI Load, Get RowCount from OUTPUT
			
		UPDATE [Control].[ETLController] SET TransactionEndDate=GETDATE(), TransactionStatus=1, TransactionRowsProcessed=@RowsProcessed	WHERE BatchKey=@BatchKey	

		COMMIT TRAN ETLTransaction -- Commit main body of updates to progreess non-esential subsequent updates in their own transaction
		BEGIN TRAN ETLTransaction
				
		UPDATE STATISTICS [$(Dimensional)].Fact.[Transaction]																-- Update STATS on Fact Table

		UPDATE [$(Dimensional)].[Control].CompleteDate	SET TransactionCompleteDate = ISNULL((SELECT BatchToDate FROM [Control].[ETLController] WHERE BatchKey = @BatchKey),TransactionCompleteDate)
				
	END

	COMMIT TRAN ETLTransaction

END TRY
		
BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLTransaction

	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - Transaction ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;

	UPDATE [Control].[ETLController] SET TransactionEndDate=GETDATE(), TransactionStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
	THROW;

END CATCH
