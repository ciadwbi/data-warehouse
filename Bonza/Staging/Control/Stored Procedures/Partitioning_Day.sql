﻿CREATE PROCEDURE [Control].[Partitioning_Day]
(	
	@BatchFromDate	date = NULL,			-- Optional - In combination with the next parameter specifies a new window of days (which may fully or partially overlap the period already covered in the ETLController) for which partitions are to be added if not already present
	@BatchToDate	date = NULL,			-- Optional - As described above, specifies an additional period of days from the previous parameter for which partitions are to be created. If the previous parameter is omitted, the period is taken to start from the last day covered by ETLController
	@Schema			varchar(128) = NULL,	-- Optional - Nominates a schema within which all tables partitioned by BatchKey will have their partitions maintained (note that any changes to the partition functions of these tabes will also impact any other tables using those functions)
	@Table			varchar(128) = NULL,	-- Optional - Nominates a single table which if partitioned by BatchKey will have its partitions maintained (note that any changes to the partition function of this tabes will also impact any other tables using that function)
	@GapFill		bit = 0,				-- Optional - (0=FALSE/1=TRUE) - Indicates whether any gaps (BatchKeys in ETL_Controller for which there aren't currently partitions), in additiona tot he new BatchKey in the parameter above should be created 
	@Trim			bit = 0,				-- Optional - (0=FALSE/1=TRUE) - Indicates whether any exisiting partitons for which there aren't current BatchKeys in ETL_Controller or included the above parameter should be merged out.
	@RowBoundary	int = 1,				-- Optional - Sets the boundary between PAGE and ROW compression in the partitions, i.e. 0 = PAGE compression for all populated partitions, 2= ROW compression for last 2 populated partitions and PAGE for all others, etc. NULL = do not perform any compression changes
	@DryRun			bit = 0					-- Optional - (0=FALSE/1=TRUE) - Allows the procedure to be dry run whereby it will report all of its intended actions but not actually execute them
											-- NOTE, this function will always maintain one empty highest partition to speed the processes of adding new highest partitons without having to move data
)
AS

	SET NOCOUNT ON 

	DECLARE	@DayKeyColumn varchar(128) = 'DayKey'

	DECLARE @ControllerFromDate date, @ControllerToDate date, @ProcessedToDate date
	DECLARE	@PagePartitionNumber int

	DECLARE	@i int,
			@SQL varchar(MAX)

BEGIN

	SELECT	@ControllerFromDate = CONVERT(date,MIN(BatchFromDate)), 
			@ControllerToDate = CONVERT(date,MAX(BatchToDate)),
			@ProcessedToDate = CONVERT(date,DATEADD(day,1,MAX(CASE WHEN BatchStatus = 1 THEN BatchToDate ELSE NULL END))) 
	FROM	Control.ETLController WITH (NOLOCK) WHERE BatchKey > 0

--select @ControllerFromDate,@ControllerToDate,@ProcessedToDate

	SET @BatchFromDate = COALESCE(@BatchFromDate, @ControllerToDate)
	SET @BatchToDate = COALESCE(@BatchToDate, @BatchFromDate)
	IF @BatchToDate < @ControllerToDate SET @BatchToDate = @ControllerToDate

--select @BatchFromDate,@BatchToDate

	;WITH	ControllerDates AS (SELECT @ControllerFromDate as DayDate UNION ALL SELECT DATEADD(day, 1, DayDate) FROM ControllerDates WHERE DayDate <= @ControllerToDate),
			BatchDates AS (SELECT @BatchFromDate as DayDate UNION ALL SELECT DATEADD(day, 1, DayDate) FROM BatchDates WHERE DayDate <= @BatchToDate)
	SELECT	DayDate, DATEPART(year,DayDate)*1000000 + DATEPART(month,DayDate)*10000 + DATEPART(day,DayDate)*100 DayKey INTO #Dates FROM ControllerDates
	UNION 
	SELECT	DayDate, DATEPART(year,DayDate)*1000000 + DATEPART(month,DayDate)*10000 + DATEPART(day,DayDate)*100 DayKey FROM BatchDates 
	OPTION (MAXRECURSION 0)

--select * from #Dates

	-- MANAGE PARTITIONS

	SELECT	s.name SchemaName, ic.object_id TableId, t.name TableName, ic.index_id IndexId, i.name IndexName, i.type_desc IndexType, ps.name PartitionScheme, pf.name PartitionFunction, pf.function_id PartitionFunctionId, pf.boundary_value_on_right RightBoundary 
	INTO	#Indexes
	FROM	[$(Dimensional)].sys.columns c WITH (NOLOCK) 
			INNER JOIN [$(Dimensional)].sys.index_columns ic WITH (NOLOCK) on (ic.object_id = c.object_id and ic.column_id = c.column_id)
			INNER JOIN [$(Dimensional)].sys.indexes i WITH (NOLOCK) on (i.object_id = ic.object_id and i.index_id = ic.index_id)
			INNER JOIN [$(Dimensional)].sys.partition_schemes ps WITH (NOLOCK) on (ps.data_space_id = i.data_space_id)
			INNER JOIN [$(Dimensional)].sys.partition_functions pf WITH (NOLOCK) on (pf.function_id = ps.function_id)
			INNER JOIN [$(Dimensional)].sys.tables t WITH (NOLOCK) on (t.object_id = c.object_id)
			INNER JOIN [$(Dimensional)].sys.schemas s WITH (NOLOCK) on (s.schema_id = t.schema_id)
	WHERE	c.name = @DayKeyColumn
			AND t.name = ISNULL(@Table,t.name)
			AND s.name = ISNULL(@Schema,s.name)
			AND ic.partition_ordinal <> 0
			and i.type_desc not like '%columnstore'
			and pf.name not like '%(!)%'
			and pf.name not like '%Seq%'
			and pf.name not like '%Month%'
			and pf.name not like '%Year%'

--select * from #Indexes

	SELECT	PartitionScheme, PartitionFunction, PartitionFunctionId, RightBoundary, CONVERT(int,Value) Value, boundary_id BoundaryId
	INTO	#Values
	FROM	(SELECT DISTINCT PartitionScheme, PartitionFunction, PartitionFunctionId, RightBoundary FROM #Indexes) i
			INNER JOIN [$(Dimensional)].sys.partition_range_values pr WITH (NOLOCK) on (pr.function_id = i.PartitionFunctionId)

--select * from #Values

	SELECT	ROW_NUMBER() OVER (ORDER BY PartitionFunction DESC, Value ASC) i, sql
	INTO	#Partitions
	FROM	(	SELECT	'EXEC [$(Dimensional)].[Control].[Sp_RemotePartitioning] @PartitionFunction = '''+PartitionFunction+''', @PartitionScheme = '''+ PartitionScheme +''', @Value = '''+CONVERT(varchar,Value)+''', @Action = ''SPLIT''' sql, PartitionFunction, Value
				FROM	(	SELECT DISTINCT PartitionScheme, PartitionFunction, DayKey - 1 + RightBoundary Value FROM #Indexes, #Dates WHERE DayDate >= @BatchFromDate OR @GapFill = 1
							EXCEPT
							SELECT PartitionScheme, PartitionFunction, Value from #Values
						) x
				UNION ALL
				SELECT	'EXEC [$(Dimensional)].[Control].[Sp_RemotePartitioning] @PartitionFunction = '''+PartitionFunction+''', @PartitionScheme = '''+ PartitionScheme +''', @Value = '''+CONVERT(varchar,Value)+''', @Action = ''MERGE''' sql, PartitionFunction, Value
				FROM	(	SELECT PartitionScheme, PartitionFunction, Value from #Values WHERE @Trim = 1
							EXCEPT
							SELECT DISTINCT PartitionScheme, PartitionFunction, DayKey - 1 + RightBoundary Value FROM #Indexes, #Dates
						) x
			) y
--	WHERE	sql IS NOT NULL
	SET @i = @@ROWCOUNT

	SELECT sql PartitionManagement FROM #Partitions ORDER BY i

	WHILE @i > 0 AND @DryRun = 0
	BEGIN
		SELECT	@SQL = sql FROM	#Partitions	WHERE i = @i
		IF @DryRun = 0 EXEC(@SQL)
		SET @i = @i - 1	
	END


	-- MANAGE COMPRESSION

	IF @RowBoundary IS NOT NULL
	BEGIN
		WITH PageBoundary AS
			(	SELECT	i.PartitionFunctionId, MAX(pr.boundary_id) - @RowBoundary PageLimit
				FROM	(SELECT DISTINCT PartitionFunctionId, RightBoundary FROM #Indexes) i
						INNER JOIN [$(Dimensional)].sys.partition_range_values pr WITH (NOLOCK) on (pr.function_id = i.PartitionFunctionId)
				WHERE	CONVERT(int,Value) <= DATEPART(year,@ProcessedToDate)*1000000 + DATEPART(month,@ProcessedToDate)*10000 + DATEPART(day,@ProcessedToDate)*100 
				GROUP BY i.PartitionFunctionId
			)
		SELECT	ROW_NUMBER() OVER (ORDER BY SchemaName DESC, TableName DESC, PartitionNumber DESC, IndexName DESC) i, 
				'ALTER ' + CASE WHEN IndexName IS NULL THEN 'TABLE ' ELSE 'INDEX ['+ IndexName +'] ON ' END + 
							'[$(Dimensional)].[' + SchemaName + '].[' + TableName + ']' + 
							' REBUILD PARTITION = ' + CONVERT(VARCHAR(20),PartitionNumber) + 
							' WITH (/*SORT_IN_TEMPDB = ON, ONLINE = ON,*/ DATA_COMPRESSION = ' + TargetCompression + ') ' sql
		INTO	#Compression
		FROM	(	SELECT	i.SchemaName,
							i.TableName, 
							i.IndexName, 
							p.partition_number PartitionNumber, 
							p.data_compression_desc CurrentCompression, 
							CASE WHEN PageLimit >= p.partition_number THEN 'PAGE' ELSE 'ROW' END TargetCompression
					FROM	#Indexes i WITH (NOLOCK) 
							INNER JOIN PageBoundary b ON (b.PartitionFunctionId = i.PartitionFunctionId)
							INNER JOIN [$(Dimensional)].sys.partitions p WITH (NOLOCK) ON (p.object_id = i.TableId AND p.index_id = i.IndexId)
				) x
		WHERE	TargetCompression <> CurrentCompression
		SET @i = @@ROWCOUNT

		SELECT sql  CompressionManagement from #Compression order by i

		WHILE @i > 0 AND @DryRun = 0
		BEGIN
			SELECT	@SQL = sql FROM	#Compression WHERE i = @i
			IF @DryRun = 0 EXEC(@SQL)
			SET @i = @i - 1	
		END
	END

END
