CREATE PROCEDURE [Control].[ETLKPIRecursive]
(
	@BatchKey		int	= NULL
)
AS 

	DECLARE	@RowsProcessed				INT = 0;

	SET TRANSACTION ISOLATION LEVEL READ COMMITTED
    BEGIN TRAN ETLKPIRecursive 

BEGIN TRY

	IF ISNUMERIC(@BatchKey) <> 1																				-- Auto Run - Pick next best batch to run
		SELECT	@BatchKey = MIN(BatchKey) 
		FROM	[Control].[ETLController] c
		WHERE	ISNULL(KPIRecursiveStatus,0)=0
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.StagingStatus,0) = 1) -- Staging completed successfully in current batch so that Account dimension is finished in Staging
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.RecursiveStatus,0) = 1) -- Recursives completed successfully in current batch so that Transaction is finished in Staging
				AND EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey = c.BatchKey AND ISNULL(c1.PositionStagingStatus,0) = 1) -- Position staging completed successfully in current batch
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.KPIRecursiveStatus,0) <> 1) -- No failed Kpi Recursive steps in earlier batches
				AND NOT EXISTS (SELECT 1 FROM [Control].[ETLController] c1 WHERE c1.BatchKey < c.BatchKey AND ISNULL(c1.BatchStatus,0) <> 1 AND Control.ParameterValue ('ETL','SequentialBatches') <> 'No') -- Previous batch complete or Sequential batches overidden


	IF ISNUMERIC(@BatchKey)=1																									-- Only LoadKPI after TransactionDimension is loaded.
	BEGIN
	
		UPDATE [Control].[ETLController] SET KPIRecursiveStartDate=GETDATE() WHERE BatchKey=@BatchKey						-- Set StartTime for KPI Load

		COMMIT TRAN ETLKPIRecursive -- Commit the ETL Controller initiation and switch to READ UNCOMMITTED for rest of processing due to contention issues and phantom updates from constantly updated Acquisition
		SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
		BEGIN TRAN ETLKPIRecursive

		EXEC [Recursive].[Sp_KPI] @BatchKey, @RowsProcessed	OUTPUT
			
		UPDATE [Control].[ETLController] SET KPIRecursiveEndDate=GETDATE(), KPIRecursiveStatus=1, KPIRecursiveRowsProcessed = @RowsProcessed WHERE BatchKey=@BatchKey	-- Set 'Success' RunStatus for KPI Load
				
	END

	COMMIT TRAN ETLKPIRecursive
			
END TRY
		
BEGIN CATCH
	DECLARE @ErrorMsg VARCHAR(4000)
	SET @ErrorMsg=ERROR_MESSAGE()

	ROLLBACK TRAN ETLKPIRecursive
			
	Declare @EmailSubject varchar(2000)
	Set @EmailSubject = '*** Urgent *** ' + '$(ServerName)' + ' - KPIRecursive ETL Failed,check ETL log/ETLController for error details'

	EXEC msdb.dbo.sp_send_dbmail																							-- Send Error mail to DW Team
	@profile_name	= 'DataWareHouse_Auto_Mails',
	@recipients		= 'bi.alert@WilliamHill.com.au',
	@body			=  @ErrorMsg,
	@subject		=  @EmailSubject;

	UPDATE [Control].[ETLController] SET KPIRecursiveEndDate=GETDATE(), KPIRecursiveStatus=-1, ErrorMessage = @ErrorMsg WHERE BatchKey=@BatchKey;
	THROW;

END CATCH

