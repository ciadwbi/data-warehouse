﻿Create Procedure [Control].Sp_InitialPopConfigurationItems

as

IF not exists (Select [ConfigurationItem] From [Control].[ConfigurationItems])
Begin
begin transaction

INSERT [Control].[ConfigurationItems] ([ConfigurationItem], [Value], [Description]) VALUES (N'Intrabet.Sp_Market', N'Y', N'Full historic run on market dimension if value set to "Y"')

If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End