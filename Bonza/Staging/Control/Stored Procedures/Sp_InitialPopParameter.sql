﻿Create Procedure [Control].Sp_InitialPopParameter

as

IF not exists (Select 1 From [Control].[Parameter])
Begin
begin transaction

INSERT INTO [Control].Parameter (Value, Class, Name, AllowedValues, Description) VALUES ('Yes','ETL','SequentialBatches','''Yes'' = Batches must run sequentially, ''No'' = Batches can run concurrently, All other values will default to sequential running','Controls whether ETL batches must execute sequentially (i.e. no step from one batch can run until previous batch is complete) or whether steps can run concurrently for different batches as long as specific step dependencies are observed')
INSERT INTO [Control].Parameter (Value, Class, Name, AllowedValues, Description) VALUES ('Yes','Bet','TruedEqualShare','''Yes'' = Stake/Payout equal shares are trued to add up to original amount, ''No'' = Stake/Payout equal shares are not trued and may not perfectly add up to original amount, All other values will default to being trued','Controls the approach taken to rounding/trueing equal share divisions of bets and stakes - e.g. a 3-way $10 multi untrued will result in a stake of $3.3333 for each leg (but do not add up perfectly to the original stake when summed); if trued, the first leg will have a stake of 3.3334 and the other 2 legs 3.3333 - so the total value is still correct albeit at the expense of minimal disparity at the leg level')
INSERT INTO [Control].Parameter (Value, Class, Name, AllowedValues, Description) VALUES ('hour','ETL','Interval','SQL Server time interval keyword as used in DATEDIFF function - e.g. ''day'', ''hour'', ''minute'', DEFAULT = ''day''','Interval unit being used in combination with ETL+Increment parameter to set the minimum incremental period between ETL runs - e.g. ''1''&''day'' or ''30''&''minutes''')
INSERT INTO [Control].Parameter (Value, Class, Name, AllowedValues, Description) VALUES (1,'ETL','Increment','Integer value defining the minimum number of time intervals between ETL runs','Number of Incements of the Interval unit defined by ETL+Interval parameter to set the minimum incremental period between ETL runs - e.g. ''1''&''day'' or ''30''&''minutes''')

If @@ERROR = 0
Begin
	Commit transaction
End
Else
Begin
	Rollback transaction
End
End