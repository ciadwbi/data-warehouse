﻿CREATE PROCEDURE [Control].WaitForBatchToComplete
AS

DECLARE @BatchKey int, @BatchToDate datetime2(3)

WHILE NOT EXISTS (	SELECT 1 FROM Control.ETLController WHERE BatchStatus = 1 AND BatchToDate > CONVERT(datetime2(3),CONVERT(date,GETDATE())))
			WAITFOR DELAY '00:15'

SELECT @Batchkey = BatchKey, @BatchToDate = BatchToDate FROM Control.ETLController WHERE BatchKey = (SELECT MAX(BatchKey) FROM Control.ETLController WHERE BatchStatus = 1)
PRINT 'Batch ' + CONVERT(varchar(10),@BatchKey) + ' is finished up to '+ CONVERT(varchar,@BatchToDate)

