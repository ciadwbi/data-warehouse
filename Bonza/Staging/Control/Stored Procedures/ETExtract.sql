﻿CREATE PROCEDURE [Control].[ETExtract]
AS 
/*
BEGIN

DECLARE @execution_id	BIGINT,
	  @FromDate	DATETIME,
	  @ToDate	DATETIME

SET @ToDate	=CONVERT(DATE,CONVERT(DATETIME,GETDATE()))
SET @FromDate	=DATEADD(DD,-1,@ToDate)

--SET @FromDate	='2014-11-06'
--SET @ToDate	='2014-11-20'

EXEC [$(SSISDB)].[catalog].[create_execution] @package_name=N'ETExtract.dtsx', @execution_id=@execution_id OUTPUT, @folder_name=N'ETExtract', @project_name=N'ETExtract', @use32bitruntime=False, @reference_id=Null

-- Define Parameters

	-- FromDate
	EXEC [$(SSISDB)].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=30, @parameter_name=N'FromDate', @parameter_value=@FromDate
	-- ToDate
	EXEC [$(SSISDB)].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=30, @parameter_name=N'ToDate', @parameter_value=@ToDate
	-- ToList
	EXEC [$(SSISDB)].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=30, @parameter_name=N'ToList', @parameter_value=N'Michael.Doig@williamhill.com.au; John-Paul.Costales@williamhill.com.au'
	--EXEC [$(SSISDB)].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=30, @parameter_name=N'ToList', @parameter_value=N'David.Robuck@williamhill.com.au'
	-- CC List
	--EXEC [$(SSISDB)].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=30, @parameter_name=N'CCList', @parameter_value=N'Michael.Doig@Williamhill.com.au'
	EXEC [$(SSISDB)].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=30, @parameter_name=N'CCList', @parameter_value=N'BI.Service@Williamhill.com.au;richard.hudson@williamhill.com.au;joseph.george@williamhill.com.au'	
	-- Synchronous Execution Enabled
	EXEC [$(SSISDB)].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=50, @parameter_name=N'SYNCHRONIZED', @parameter_value= 1
	-- Package Logging Enabled
	EXEC [$(SSISDB)].[catalog].[set_execution_parameter_value] @execution_id,  @object_type=50, @parameter_name=N'LOGGING_LEVEL', @parameter_value=1

-- Execute Package

EXEC [$(SSISDB)].[catalog].[start_execution] @execution_id

END
*/