﻿CREATE TABLE [Control].[Batch](
	[BatchKey] [int] IDENTITY(1,1) NOT NULL,
	[FromDate] [datetime] NOT NULL,
	[ToDate] [datetime] NOT NULL,
	[StartDateTime] [datetime] NOT NULL,
	[EndDateTime] [datetime] NULL,
	[Status] [varchar](32) NOT NULL
) ON [PRIMARY]
GO