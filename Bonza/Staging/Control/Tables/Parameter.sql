﻿CREATE TABLE Control.Parameter
(	Value varchar(100),
	Class varchar(30),
	Name varchar(30),
	AllowedValues varchar(max),
	Description varchar(max)
)
