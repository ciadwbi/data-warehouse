﻿CREATE TABLE [Control].[Increment] (
    [Increment]					   INT IDENTITY (1, 1) NOT NULL,
	[BatchKey]					   INT			   NOT NULL,
    [IncrementStatus]              SMALLINT        NULL,
    [IncrementFromDate]            DATETIME2 (3)   NULL,
    [IncrementToDate]              DATETIME2 (3)   NULL,
    [IncrementStartDate]           DATETIME2 (3)   NULL,
    [IncrementEndDate]             DATETIME2 (3)   NULL,
    [ErrorMessage]				   NVARCHAR (4000) NULL
);
GO

CREATE UNIQUE CLUSTERED INDEX [CI_Increment]
    ON [Control].[Increment]([Increment]);
GO
CREATE NONCLUSTERED INDEX [NI_Increment_BatchKey]
    ON [Control].[Increment]([BatchKey]);
GO

