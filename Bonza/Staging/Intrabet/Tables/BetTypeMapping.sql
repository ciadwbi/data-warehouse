﻿CREATE TABLE [Intrabet].[BetTypeMapping] (
    [BetType]           INT          NOT NULL,
    [WinPlace]          TINYINT      NOT NULL,
    [NumberOfLegs]      TINYINT      NOT NULL,
    [NumberOfPositions] TINYINT      NOT NULL,
    [BetTypeName]       VARCHAR (32) NOT NULL,
    [BetSubTypeName]    VARCHAR (32) NOT NULL,
    [LegTypeName]       VARCHAR (32) NOT NULL,
    [GroupingName]      VARCHAR (32) NOT NULL,
    [ProductName]       VARCHAR (32) NOT NULL
);

