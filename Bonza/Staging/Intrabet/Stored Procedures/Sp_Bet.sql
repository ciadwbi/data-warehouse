CREATE PROCEDURE [Intrabet].[Sp_Bet]
(
	@BatchKey	int,
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@MappingId	smallint = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

--- NOTE THIS IS THE NEW VERSION OF Atomic Bets - functionality is being migrated here from Intrabet.sp_AtomicBets

	---------------------------------------------------------------------------
	--- PART 1 - POPULATE CANDIDATE TABLE OF BETS TO BE LOADED INTO STAGING ---
	---------------------------------------------------------------------------


	EXEC [Control].Sp_Log	@BatchKey,@Me,'Changed Bets','Start'

	SELECT	b.Betid, b1.BetId pBetId, b.AllUpId, b.FreeBetID, b.CB_Bet_ID_Transformed LegacyBetId, b.ClientId, b1.ClientID pClientId, b.BetType, b.Channel, b.Fromdate, LT.CorrectedDate, b.ToDate,
			CONVERT(datetime2(3),b.BetDate) BetDate, b.Valid, b1.Valid pValid, b.Settled, b1.Settled pSettled, b.EventId, b1.EventId pEventId, b.CompetitorId, b.UserCode,
			b.AmountToWin, b1.AmountToWin pAmountToWin, b.AmountToPlace, b1.AmountToPlace pAmountToPlace,
			b.PayoutWin, b1.PayoutWin pPayoutWin, b.PayoutPlace, b1.PayoutPlace pPayoutPlace
	INTO	#Bets
	FROM	[$(Acquisition)].Intrabet.tblBets b
			LEFT JOIN [$(Acquisition)].Intrabet.tblBets b1 ON (b1.BetID = b.BetID AND b1.ToDate = b.FromDate)
			LEFT LOOP JOIN [$(Acquisition)].IntraBet.Latency LT ON (b.FromDate=LT.OriginalDate)
	WHERE	b.FromDate > @FromDate
			AND b.FromDate <= @ToDate
			AND b.ToDate > @FromDate
			AND (@MappingId IN (10000, 10003) OR @MappingId IS NULL)
	OPTION (RECOMPILE, TABLE HINT(LT,FORCESEEK))--, TABLE HINT(b, FORCESEEK), TABLE HINT(b1, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Requests/Decline/Accept/Cancel','Start', @RowsProcessed = @@ROWCOUNT

	-- All bet transactions prior to settlement are triggered by a creation/change in the bet table - 19:51 (412813037)
	SELECT	y.Action,
			x.Betid, 
			x.BetDate,
			x.AllUpID,
			x.FreeBetID,
			x.LegacyBetId,
			CASE y.ActionBitmap WHEN 8 THEN x.pClientID ELSE x.ClientID END ClientId, -- in case of cancellation from changed ClientId use previous client ID for all cancellations to cover this eventuality
			x.BetType,
			CONVERT(smallint,0) RewardBetTypeId,
			x.Channel,
			CASE y.ActionBitmap
				WHEN 1 THEN x.Channel -- For request, Action Channel is the bet channel
				WHEN 2 THEN 0 -- Action Channel is N/A for acceptance
				WHEN 4 THEN 0 -- Action Channel is N/A for decline
				WHEN 8 THEN 0 -- Action Channel is N/A for cancellation
				WHEN 16 THEN ISNULL(x.CashoutChannel, -1) -- Action channel is the channel found on the Transaction associated with the cashout otherwise flag it as Unknown
			END  ActionChannel,
			x.FromDate,
			x.EventId, 
			x.CompetitorID,
			x.UserId,
			--CASE y.ActionBitmap
			--	WHEN 1 THEN 0
			--	WHEN 2 THEN 0
			--	WHEN 4 THEN 0
			--	WHEN 8 THEN 0
			--	WHEN 16 THEN ISNULL(x.CashoutType,0)
			--END CashoutType,
			x.BetId TransactionId,
			CASE y.ActionBitmap
				WHEN 1 THEN CASE WHEN x.pValid IS NULL THEN x.BetDate ELSE x.FromDate END -- Use BetDate from first version of Bet record otherwise use FromDate as date of change 
				WHEN 2 THEN CASE WHEN x.pValid IS NULL THEN x.BetDate ELSE x.FromDate END -- Ditto
				WHEN 4 THEN ISNULL(x.CancelledDate, CASE WHEN x.pValid IS NULL THEN x.BetDate ELSE x.FromDate END) -- For a decline, use the cancellation date if record available, otherwise, as above
				WHEN 8 THEN ISNULL(x.CancelledDate, CASE WHEN x.pValid IS NULL THEN x.BetDate ELSE x.FromDate END) -- Ditto for cancellation
				WHEN 16 THEN ISNULL(x.CashoutDate, x.FromDate) -- Cashout Date for cahouts, otherwise use the FromDate of Bet change
			END  TransactionDate,
			CASE y.ActionBitmap
				WHEN 1 THEN CASE WHEN x.ClientID <> x.pClientId THEN ISNULL(x.AmountToWin,0) WHEN ISNULL(x.AmountToWin,0) > ISNULL(x.pAmountToWin,0) THEN ISNULL(x.AmountToWin,0) - ISNULL(x.pAmountToWin,0) ELSE 0 END -- Only request increases to stakes, otherwise set request to 0 and assume a cancel will also be generated for reduction
				WHEN 2 THEN CASE WHEN x.ClientID <> x.pClientId THEN ISNULL(x.AmountToWin,0) WHEN ISNULL(x.pValid,0) = 0 THEN ISNULL(x.AmountToWin,0) WHEN ISNULL(x.AmountToWin,0) > ISNULL(x.pAmountToWin,0) THEN ISNULL(x.AmountToWin,0) - ISNULL(x.pAmountToWin,0) ELSE 0 END -- If bet is changing to valid, accept the full amount otherwise just the difference between current and previous amount
				WHEN 4 THEN CASE WHEN ISNULL(x.pAmountToWin,0) > 0 THEN ISNULL(x.pAmountToWIN,0) ELSE ISNULL(x.AmounttoWin,0) END -- for decline, if previous record has a non-zero value, use that as it will be the value form preceding request
				WHEN 8 THEN CASE WHEN x.ClientID <> x.pClientId THEN ISNULL(x.AmountToWin,0) WHEN x.Valid = 0 AND x.pValid = 1 THEN ISNULL(x.pAmountToWin,0) WHEN x.Valid = 0 THEN ISNULL(x.AmountToWin,0) WHEN ISNULL(x.pAmountToWin,0) > ISNULL(x.AmountToWin,0) THEN ISNULL(x.pAmountToWin,0) - ISNULL(x.AmountToWin,0) ELSE 0 END -- for cancel, if previous record has a non-zero value, use that as it will be the value form preceding request, if current record still valid then check for partial cancellation of stake
				WHEN 16 THEN ISNULL(x.AmountToWin,0)
			END StakeWin,
			CASE y.ActionBitmap
				WHEN 1 THEN CASE WHEN x.ClientID <> x.pClientId THEN ISNULL(x.AmountToPlace,0) WHEN ISNULL(x.AmountToPlace,0) > ISNULL(x.pAmountToPlace,0) THEN ISNULL(x.AmountToPlace,0) - ISNULL(x.pAmountToPlace,0) ELSE 0 END -- Only request increases to stakes, otherwise set request to 0 and assume a cancel will also be generated for reduction
				WHEN 2 THEN CASE WHEN x.ClientID <> x.pClientId THEN ISNULL(x.AmountToPlace,0) WHEN ISNULL(x.pValid,0) = 0 THEN ISNULL(x.AmountToPlace,0) WHEN ISNULL(x.AmountToPlace,0) > ISNULL(x.pAmountToPlace,0) THEN ISNULL(x.AmountToPlace,0) - ISNULL(x.pAmountToPlace,0) ELSE 0 END -- If bet is changing to valid, accept the full amount otherwise just the difference between current and previous amount
				WHEN 4 THEN CASE WHEN ISNULL(x.pAmountToPlace,0) > 0 THEN ISNULL(x.pAmountToPlace,0) ELSE ISNULL(x.AmounttoPlace,0) END -- for decline, if previous record has a non-zero value, use that as it will be the value form preceding request
				WHEN 8 THEN CASE WHEN x.ClientID <> x.pClientId THEN ISNULL(x.AmountToPlace,0) WHEN x.Valid = 0 AND x.pValid = 1 THEN ISNULL(x.pAmountToPlace,0) WHEN x.Valid = 0 THEN ISNULL(x.AmountToPlace,0) WHEN ISNULL(x.pAmountToPlace,0) > ISNULL(x.AmountToPlace,0) THEN ISNULL(x.pAmountToPlace,0) - ISNULL(x.AmountToPlace,0) ELSE 0 END -- for cancel, if previous record has a non-zero value, use that as it will be the value form preceding request, if current record still valid then check for partial cancellation of stake
				WHEN 16 THEN ISNULL(x.AmountToPlace,0)
			END StakePlace,
			ISNULL(x.pAmountToWin,0) as PreviousStakeWin,
			ISNULL(x.pAmountToPlace,0) as PreviousStakePlace,
			CASE y.ActionBitmap
				WHEN 1 THEN CONVERT(money,0)
				WHEN 2 THEN CONVERT(money,0)
				WHEN 4 THEN CONVERT(money,0)
				WHEN 8 THEN CONVERT(money,0)
				WHEN 16 THEN ISNULL(CONVERT(money,x.PayoutWin),0)
			END PayoutWin,
			CASE y.ActionBitmap
				WHEN 1 THEN CONVERT(money,0)
				WHEN 2 THEN CONVERT(money,0)
				WHEN 4 THEN CONVERT(money,0)
				WHEN 8 THEN CONVERT(money,0)
				WHEN 16 THEN ISNULL(CONVERT(money,x.PayoutPlace),0)
			END PayoutPlace,
			CASE y.ActionBitmap
				WHEN 1 THEN CONVERT(money,0)
				WHEN 2 THEN CONVERT(money,0)
				WHEN 4 THEN CONVERT(money,0)
				WHEN 8 THEN CONVERT(money,0)
				WHEN 16 THEN ISNULL(CONVERT(money,x.pPayoutWin),0)
			END PreviousPayoutWin,
			CASE y.ActionBitmap
				WHEN 1 THEN CONVERT(money,0)
				WHEN 2 THEN CONVERT(money,0)
				WHEN 4 THEN CONVERT(money,0)
				WHEN 8 THEN CONVERT(money,0)
				WHEN 16 THEN ISNULL(CONVERT(money,x.pPayoutPlace),0)
			END PreviousPayoutPlace,
			CASE y.ActionBitmap
				WHEN 1 THEN CASE WHEN ISNULL(x.AmountToWin,0) > 0 THEN 1 ELSE 0 END + CASE WHEN ISNULL(x.AmountToPlace,0) > 0 THEN 2 ELSE 0 END
				WHEN 2 THEN CASE WHEN ISNULL(x.AmountToWin,0) > 0 THEN 1 ELSE 0 END + CASE WHEN ISNULL(x.AmountToPlace,0) > 0 THEN 2 ELSE 0 END
				WHEN 4 THEN CASE WHEN ISNULL(x.pAmountToWin,0) > 0 THEN 1 ELSE 0 END + CASE WHEN ISNULL(x.pAmountToPlace,0) > 0 THEN 2 ELSE 0 END
				WHEN 8 THEN CASE WHEN ISNULL(x.pAmountToWin,0) > 0 THEN 1 ELSE 0 END + CASE WHEN ISNULL(x.pAmountToPlace,0) > 0 THEN 2 ELSE 0 END
				WHEN 16 THEN CASE WHEN ISNULL(x.AmountToWin,0) > 0 THEN 1 ELSE 0 END + CASE WHEN ISNULL(x.AmountToPlace,0) > 0 THEN 2 ELSE 0 END
			END EachWayType,
			10000 MappingId
	INTO	#Candidate
	FROM	(	SELECT	CASE -- REQUEST
							WHEN b.AmountToWin > ISNULL(b.pAmountToWin,0) THEN 1 -- Increase in stake (including an increase from no previous stake) requires a request
							WHEN b.AmountToPlace > ISNULL(b.pAmountToPlace,0) THEN 1 
							WHEN b.ClientID <> b.pClientID THEN 1 -- A change of clientid can occur in the life of the bet requiring full cancellation and re-request/accept 
							ELSE 0
						END 
						| CASE -- ACCEPT
							WHEN b.Valid = 1 AND ISNULL(b.pValid,0) = 0 THEN 2 -- First accepted version of record (avoid creating multiple requests each time Bet record changes)
							WHEN b.Valid = 1 AND ISNULL(b.pAmountToWin,0) < b.AmountToWin THEN 2 -- or an increase in amount on a valid record triggers an Accept for the difference
							WHEN b.Valid = 1 AND ISNULL(b.pAmountToPlace,0) < b.AmountToPlace THEN 2
							WHEN b.Settled = 1 AND b.pBetID IS NULL THEN 2 -- Assumption for historic load, if Settled = 1 then must once have been Valid
							WHEN b.FromDate = CONVERT(datetime2(3),'2013-12-19') -- Historic cancellation requiring corresponding accept
								AND b.Valid = 0 AND b.Settled = 0 AND b.PayoutWin = b.AmountToWin AND b.PayoutPlace = b.AmountToPlace THEN 2
							WHEN b.ClientID <> b.pClientID THEN 2 -- A change of clientid can occur in the life of the bet requiring full cancellation and re-request/accept 
							ELSE 0 
						END
						| CASE -- DECLINE
							WHEN b.FromDate > CONVERT(datetime2(3),'2013-12-19')
								AND b.Valid = 0 AND b.Settled = 0 AND b.PayoutWin = b.AmountToWin AND b.PayoutPlace = b.AmountToPlace
								AND ISNULL(b.pValid,0) = 0 AND ISNULL(b.pPayoutWin,0) = 0 AND ISNULL(b.pPayoutPlace,0) = 0 THEN 4
							WHEN b.FromDate = CONVERT(datetime2(3),'2013-12-19') -- Only consider initial pre CDC load
								AND b.ToDate >= CONVERT(datetime2(3),'9999-12-30') -- and records which were not subsequently processed
								AND b.Valid = 0 AND b.Settled = 0 AND (b.PayoutWin <> b.AmountToWin OR b.PayoutPlace <> b.AmountToPlace) THEN 4
							ELSE 0
						END
						| CASE -- CANCEL
							WHEN b.pValid = 1 AND b.Valid = 0 AND b.EventID = b.pEventID THEN 8 -- To be cancelled must previously have been valid for same eventid (to cope with situation when bet event is changed to deal with first past post versus enquiry
							WHEN b.pValid = 1 AND b.Valid = 1 AND b.EventID = b.pEventID AND ISNULL(b.AmountToWin,0) < ISNULL(b.pAmountToWin,0) THEN 8 -- A Decrease in stake amount must be actioned as a partial cancelation
							WHEN b.pValid = 1 AND b.Valid = 1 AND b.EventID = b.pEventID AND ISNULL(b.AmountToPlace,0) < ISNULL(b.pAmountToPlace,0) THEN 8 -- A Decrease in stake amount must be actioned as a partial cancelation
							WHEN b.FromDate = CONVERT(datetime2(3),'2013-12-19') AND b.Valid = 0 AND b.Settled = 1 THEN 8 -- Only consider initial pre CDC load: Either the bet is settled, in which case must once have been accepted and therefore cannot be decline
							WHEN b.FromDate = CONVERT(datetime2(3),'2013-12-19') AND b.Valid = 0 AND b.PayoutWin = b.AmountToWin AND b.PayoutPlace = b.AmountToPlace THEN 8 -- Or bet has stake negated by payouts indicating Cancellation
							WHEN b.ClientID <> b.pClientID THEN 8  -- A change of clientid can occur in the life of the bet requiring full cancellation and re-request/accept
							ELSE 0
						END
						| CASE -- CASHOUT
							WHEN o.BetId IS NOT NULL AND b.PayoutWin <> ISNULL(b.pPayoutWin,0) THEN 16 -- A contemporary Cashout Record when a change in payout amount occurs means cashout
							WHEN o.BetId IS NOT NULL AND b.PayoutPlace <> ISNULL(b.pPayoutPlace,0) THEN 16 -- A contemporary Cashout Record when a change in payout amount occurs means cashout
							WHEN o.BetId IS NOT NULL AND b.ClientID <> b.pClientID THEN 16  -- A change of clientid can occur in the life of the bet requiring full cancellation and re-request/accept/settlement
							ELSE 0
						END	ActionBitmap, 
						b.Betid, b.AllUpId, b.FreeBetID, b.LegacyBetId, b.ClientId, b.pClientId, b.BetType, b.Channel, t.Channel CashoutChannel, b.BetDate, ISNULL(b.CorrectedDate,b.Fromdate) AS FromDate,
						c.CancelledDate, CONVERT(datetime2(3),o.Created) CashoutDate, b.Valid, b.pValid, b.EventId, b.CompetitorId, ISNULL(c.UserCode,b.UserCode) UserId, --o.CashoutType,
						b.AmountToWin, b.pAmountToWin, b.AmountToPlace, b.pAmountToPlace,
						b.PayoutWin, b.pPayoutWin, b.PayoutPlace, b.pPayoutPlace 
				FROM	#Bets b
						LEFT HASH JOIN (	SELECT	DISTINCT BetId, CONVERT(datetime2(3),Date) CancelledDate, 
											MIN(FromDate) OVER (PARTITION BY BetId, Date) FromDate, 
											LAST_VALUE(UserCode) OVER (PARTITION BY BetId, Date ORDER BY FromDate) UserCode , 
											LAST_VALUE(Notes) OVER (PARTITION BY BetId, Date ORDER BY FromDate) Notes 
									FROM	[$(Acquisition)].Intrabet.tblCancelledBets 
									WHERE	Date IS NOT NULL
											AND FromDate > @FromDate
											AND FromDate <= @ToDate
											AND ToDate > @FromDate
								) c ON (c.BetID = b.BetID AND c.FromDate = b.FromDate) -- Attempt to retrieve cancellation date for use with declines/cancels
						LEFT JOIN [$(Acquisition)].Intrabet.tblCashoutBets o ON (o.BetId = b.BetId AND o.FromDate <= b.FromDate AND o.ToDate > b.FromDate)
						LEFT JOIN [$(Acquisition)].Intrabet.tblTransactions t ON (t.BetId = o.BetId AND t.FromDate = o.FromDate AND t.TransactionDate = o.Created)
				WHERE	(b.AmountToWin > 0 OR b.AmountToPlace > 0)	-- Drive from primary leg of AllUps
			) x
			INNER JOIN (	SELECT 1 ActionBitmap, CONVERT(varchar(10),'Request') Action UNION ALL
							SELECT 2 ActionBitmap, CONVERT(varchar(10),'Accept') Action UNION ALL
							SELECT 4 ActionBitmap, CONVERT(varchar(10),'Decline') Action UNION ALL
							SELECT 8 ActionBitmap, CONVERT(varchar(10),'Cancel') Action UNION ALL
							SELECT 16 ActionBitmap, CONVERT(varchar(10),'Cashout') Action
						) y on (y.ActionBitmap & x.ActionBitmap = y.ActionBitmap)
	WHERE	@MappingId = 10000 OR @MappingId IS NULL
	OPTION (RECOMPILE, FORCE ORDER, TABLE HINT(o, FORCESEEK), TABLE HINT(t, FORCESEEK));


	EXEC [Control].Sp_Log	@BatchKey,@Me,'Events','Start', @RowsProcessed = @@ROWCOUNT

	SELECT  e.eventid, e.FromDate, l.CorrectedDate, ISNULL(MIN(e2.FromDate),CONVERT(datetime,'1900-01-01')) PreviousFromDate, e.SettledDate
	INTO	#Events
	FROM	(	SELECT FromDate, EventId, SettledDate FROM [$(Acquisition)].IntraBet.tblEvents x WHERE ToDate > @FromDate
				UNION ALL
				SELECT FromDate, EventId, SettledDate FROM Intrabet.ForceSettlement -- Amendment to allow Force Settlement from metadata table
			) e
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents e1 ON (e1.EventID = e.EventId AND e1.ToDate = e.FromDate AND e1.SettledDate IS NOT NULL)
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents e2 ON (e2.EventID = e1.EventId AND e2.ToDate <= e1.ToDate AND e2.SettledDate = e1.SettledDate)
			LEFT JOIN [$(Acquisition)].IntraBet.Latency l  ON (e.FromDate=l.OriginalDate)
	WHERE	e.FromDate > @FromDate 
			AND e.FromDate <= @ToDate
			AND e.SettledDate IS NOT NULL
			AND ISNULL(e1.SettledDate,CONVERT(datetime,'1900-01-01')) < e.SettledDate -- Discard records where the SettledDate hasn't changed since the previous version of the record
			AND (@MappingId IN (10001, 10002) OR @MappingId IS NULL)
	GROUP BY e.eventid, e.FromDate, l.CorrectedDate, e.SettledDate
	OPTION (RECOMPILE, LOOP JOIN, TABLE HINT(x, FORCESEEK), TABLE HINT(e1, FORCESEEK([CI_tblEvents(A)](EventId))), TABLE HINT(e2, FORCESEEK([CI_tblEvents(A)](EventId))), TABLE HINT(l, FORCESEEK));

	EXEC [Control].Sp_Log	@BatchKey,@Me,'EventBets','Start', @RowsProcessed = @@ROWCOUNT

	-- Take settled events from above and link to bets placed on those events (could be single bet or leg of a mulit/allup) - NOTE these 2 steps split with intermediate Temp table to force SQL server into correct execution plan for both small and large volumes
	SELECT	b.BetId, CONVERT(datetime2(3),b.BetDate) BetDate, b.AllUpID, b.FreeBetID, b.CB_Bet_ID_Transformed LegacyBetId, b.ClientID, b.BetType, b.Channel, b.Valid, b.Settled, b.EventId, b.CompetitorId, b.AmountToWin, b.AmountToPlace, b.PayoutWin, b.PayoutPlace, e.FromDate, e.CorrectedDate, e.PreviousFromDate, e.SettledDate -- 6:07 (312407294) /*(139884676)*/
	INTO	#EventBets
	FROM	#Events e
			INNER JOIN [$(Acquisition)].IntraBet.tblBets b ON e.EventID = b.EventID AND b.FromDate <= e.FromDate AND b.ToDate > e.FromDate
			LEFT JOIN [$(Acquisition)].Intrabet.tblCashoutBets o ON o.BetId = b.BetId AND o.FromDate <= b.FromDate AND o.ToDate > b.FromDate
	WHERE	o.BetId IS NULL -- exclude cashouts - taken care of in main 10000 mapping above
	OPTION (RECOMPILE, TABLE HINT(b, FORCESEEK));

	EXEC [Control].Sp_Log	@BatchKey,@Me,'SingleBets-10001','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #Events

	-- All single bets triggered by settlement date being set/updated on Event record 7:47 (166119717)
	INSERT INTO #Candidate
	SELECT	'Settled' Action,
			c.BetId,
			c.BetDate, 
			c.AllUpID,
			c.FreeBetID, 
			c.LegacyBetId,
			c.ClientID,
			c.BetType,
			CONVERT(smallint,0) RewardBetTypeId,
			c.Channel,
			0 ActionChannel,
			ISNULL(c.CorrectedDate,c.FromDate) AS FromDate, 
			c.EventID,
			c.CompetitorID,
			NULL UserId,
--			0 CashoutType,
			c.BetId TransactionId, 
			c.SettledDate TransactionDate,
			ISNULL(c.AmountToWin,0) StakeWin,
			ISNULL(c.AmountToPlace,0) StakePlace,
			ISNULL(b1.AmountToWin,0) PreviousStakeWin,
			ISNULL(b1.AmountToPlace,0) PreviousStakePlace,
			ISNULL(CONVERT(money,c.PayoutWin),0) PayoutWin,
			ISNULL(CONVERT(money,c.PayoutPlace),0) PayoutPlace,
			ISNULL(CONVERT(money,b1.PayoutWin),0) PreviousPayoutWin,
			ISNULL(CONVERT(money,b1.PayoutPlace),0) PreviousPayoutPlace,
			CASE WHEN ISNULL(c.AmountToWin,0) > 0 THEN 1 ELSE 0 END + CASE WHEN ISNULL(c.AmountToPlace,0) > 0 THEN 2 ELSE 0 END EachWayType,
			10001 MappingId
	FROM	#EventBets c 
			LEFT JOIN [$(Acquisition)].IntraBet.tblBets b1 ON (b1.BetID = c.BetID AND c.PreviousFromDate <> CONVERT(datetime2(3),'1900-01-01') AND b1.FromDate <= c.PreviousFromDate AND b1.ToDate > c.PreviousFromDate AND b1.Valid = 1 AND b1.Settled = 1 AND ISNULL(b1.PayoutWin,0) = ISNULL(c.PayoutWin,0) AND ISNULL(b1.PayoutPlace,0) = ISNULL(c.PayoutPlace,0))
	WHERE	b1.EventID IS NULL -- Only use this event if the preceding version of the event does not link to an identical settled outcome
			AND c.AllUpID IS NULL
			AND (ISNULL(c.AmountToWin,0) > 0 OR ISNULL(c.AmountToPlace,0) > 0)
			AND c.Valid = 1 -- Must be valid
			AND c.Settled = 1 -- and settled..... otherwise may be cancelled or could still be waiting for event to settle
			AND (@MappingId = 10001 OR @MappingId IS NULL)
	OPTION (RECOMPILE, TABLE HINT(b1, FORCESEEK));

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Event AllUps','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	x.BetId TransactionId, x.SettledDate TransactionDate, x.FromDate, x.CorrectedDate, x.PreviousFromDate, u.BetId, u.WinMultiFactor, u.PlaceMultiFactor 
	INTO	#EventAllUps
	FROM	#EventBets x 
			INNER JOIN [$(Acquisition)].IntraBet.tblAllUps u  ON x.AllUpId IS NOT NULL AND u.AllUpID = x.AllUpID AND u.FromDate <= x.FromDate AND u.ToDate > x.FromDate
	WHERE	x.settled = 1
			AND (@MappingId = 10002 OR @MappingId IS NULL)
	OPTION (RECOMPILE, TABLE HINT(u, FORCESEEK));

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Event AllUp Bets','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #EventBets

	SELECT	x.*, b.BetDate, b.AllUpID, b.FreeBetID, b.CB_Bet_ID_Transformed LegacyBetId, b.ClientID, b.BetType, b.Channel, b.EventID, b.CompetitorID, b.FromDate BetFromDate,
			ISNULL(b.AmountToWin,0) StakeWin,	ISNULL(b.AmountToPlace,0) StakePlace, ISNULL(CONVERT(money,b.PayoutWin),0) PayoutWin,ISNULL(CONVERT(money,b.PayoutPlace),0) PayoutPlace 
	INTO	#EventAllUpBets
	FROM	#EventAllUps x 
			INNER JOIN [$(Acquisition)].IntraBet.tblBets b  ON b.BetID = x.BetID AND b.FromDate <= x.FromDate AND b.ToDate > x.FromDate AND b.Valid = 1
	OPTION (RECOMPILE, TABLE HINT(b, FORCESEEK([CI_tblBets(A)](BetId))))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'MultiBets-10002','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #EventAllUps

	-- Identify those multis which are newly settled because of the material changes in Event Settlement and Leg Multi Factor identified above 24:01 (33977414)
	INSERT INTO #Candidate
	SELECT	'Settled' Action,
			x.BetId,
			x.BetDate,
			x.AllUpID, 
			x.FreeBetID,
			x.LegacyBetId,
			x.ClientID,
			x.BetType,
			CONVERT(smallint,0) RewardBetTypeId,
			x.Channel,
			0 ActionChannel,
			ISNULL(x.CorrectedDate,x.FromDate) AS FromDate,
			x.EventID, 
			x.CompetitorID,
			NULL UserId,
--			0 CashoutType,
			MIN(x.TransactionId) TransactionId, 
			x.TransactionDate,
			x.StakeWin,
			x.StakePlace,
			x.PayoutWin,
			x.PayoutPlace,
			ISNULL(CONVERT(money,x.PayoutWin),0) PayoutWin,
			ISNULL(CONVERT(money,x.PayoutPlace),0) PayoutPlace,
			0 PreviousPayoutWin,
			0 PreviousPayoutPlace,
			CASE WHEN ISNULL(x.StakeWin,0) > 0 THEN 1 ELSE 0 END + CASE WHEN ISNULL(x.StakePlace,0) > 0 THEN 2 ELSE 0 END EachWayType,
			10002 MappingId
	FROM	#EventAllUpBets x 
			INNER JOIN #EventAllUpBets b  ON (b.TransactionId = x.TransactionId AND b.TransactionDate = x.TransactionDate) 
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents e  ON (e.EventID = b.EventId AND e.FromDate <= x.FromDate AND e.ToDate > x.FromDate AND e.SettledDate <= x.TransactionDate)
	WHERE	x.BetFromDate > x.PreviousFromDate AND (x.StakeWin > 0 OR x.StakePlace > 0)
			AND (@MappingId = 10002 OR @MappingId IS NULL)
	GROUP BY x.TransactionDate, ISNULL(x.CorrectedDate,x.FromDate), x.PreviousFromDate, x.BetID, x.BetDate, x.AllUpId, x.FreeBetID, x.LegacyBetId, x.ClientId, x.BetType, x.Channel, x.EventId, x.CompetitorId, x.StakeWin, x.StakePlace, x.PayoutWin, x.PayoutPlace
	HAVING	(COUNT(*) = SUM(CASE WHEN (b.WinMultiFactor > 0 OR b.PlaceMultiFactor > 0) AND e.SettledDate IS NOT NULL THEN 1 ELSE 0 END)					-- All legs settled and winning
					OR 	(SUM(CASE WHEN b.WinMultiFactor = 0 AND b.PlaceMultiFactor = 0 AND e.SettledDate IS NOT NULL THEN 1 ELSE 0 END) > 0				-- or losing leg
							AND SUM(CASE WHEN b.WinMultiFactor = 0 AND b.PlaceMultiFactor = 0 AND e.SettledDate < x.TransactionDate THEN 1 ELSE 0 END) = 0))	-- with no previous losing legs
	OPTION (RECOMPILE, TABLE HINT(e, FORCESEEK));

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Adjustments-10003','Start', @RowsProcessed = @@ROWCOUNT

	DROP TABLE #EventAllUpBets

	INSERT INTO #Candidate
	SELECT  'Settled' Action,
			b.BetId,
			b.BetDate,
			b.AllUpID, 
			b.FreeBetID,
			b.LegacyBetId,
			b.ClientID,
			b.BetType,
			CONVERT(smallint,0) RewardBetTypeId,
			b.Channel,
			0 ActionChannel,
			ISNULL(b.CorrectedDate,b.FromDate) AS FromDate,			-- Use Corrected FromDate by joining to Latency Table
			b.EventId, 
			b.CompetitorId,
			NULL UserId,
--			0 CashoutType,
			b.BetId TransactionId, 
			ISNULL(b.CorrectedDate,b.FromDate) AS TransactionDate,		-- Use Corrected FromDate by joining to Latency Table
			ISNULL(b.AmountToWin,0) StakeWin,
			ISNULL(b.AmountToPlace,0) StakePlace,
			ISNULL(b.pAmountToWin,0) PreviousStakeWin,
			ISNULL(b.pAmountToPlace,0) PreviousStakePlace,
			ISNULL(CONVERT(money,b.PayoutWin),0) PayoutWin,
			ISNULL(CONVERT(money,b.PayoutPlace),0) PayoutPlace,
			ISNULL(CONVERT(money,b.pPayoutWin),0) PreviousPayoutWin,
			ISNULL(CONVERT(money,b.pPayoutPlace),0) PreviousPayoutPlace,
			CASE WHEN ISNULL(b.AmountToWin,0) > 0 THEN 1 ELSE 0 END + CASE WHEN ISNULL(b.AmountToPlace,0) > 0 THEN 2 ELSE 0 END EachWayType,
			10003 MappingId
	FROM	#Bets b	
			LEFT JOIN [$(Acquisition)].IntraBet.tblAllUps a ON (a.AllUpID = b.AllUpID AND a.ToDate >= CONVERT(datetime2(3),'9999-12-30'))				-- Get all legs if multi bet using AllUpId
			INNER JOIN [$(Acquisition)].IntraBet.tblBets ba ON (ba.BetID=ISNULL(a.BetID,b.betid) AND ba.ToDate >= CONVERT(datetime2(3),'9999-12-30'))	-- Get all legs if multi bet using AllUpId
			LEFT JOIN [$(Acquisition)].intrabet.tblEvents e ON (e.EventID = ba.EventID AND e.SettledDate IS NOT NULL AND e.FromDate < b.FromDate AND e.ToDate >= b.FromDate)   -- Get bet changes after event Settlement
			LEFT JOIN [$(Acquisition)].intrabet.tblEvents e1 ON (e1.EventID = ba.EventID AND e1.SettledDate IS NOT NULL AND e1.FromDate >= b.FromDate AND e1.Todate > b.FromDate AND e1.ToDate <> CONVERT(datetime2(3),'9999-12-30'))  -- Not resettled after bet settlement
			LEFT JOIN [$(Acquisition)].Intrabet.tblCashoutBets o ON (o.BetId = b.BetId AND o.FromDate <= b.FromDate AND o.ToDate > b.FromDate)
	WHERE	b.Valid = 1
			AND b.Settled = 1
			AND o.BetId IS NULL
			AND (b.amounttowin > 0 or b.AmountToPlace > 0)
            AND (b.PayoutWin <> ISNULL(b.pPayoutWin,0) OR b.PayoutPlace <> ISNULL(b.pPayoutPlace,0) OR b.AmountToWin <> ISNULL(b.pAmountToWin,0) OR b.AmountToPlace <> ISNULL(b.pAmountToPlace,0))
			AND (@MappingId = 10003 OR @MappingId IS NULL)
	GROUP BY b.BetId, b.BetDate, b.AllUpID, b.FreeBetID, b.LegacyBetId, b.ClientID, b.BetType, b.Channel, ISNULL(b.CorrectedDate,b.FromDate), b.EventId, b.CompetitorId ,b.AmountToWin,b.AmountToPlace,b.pAmountToWin,b.pAmountToPlace,b.PayoutWin,b.PayoutPlace,b.pPayoutWin,b.pPayoutPlace
    HAVING	COUNT(ba.BetId) = COUNT(e.EventId)
            AND COUNT(e1.EventId) = 0
	OPTION (RECOMPILE, TABLE HINT(a, FORCESEEK), TABLE HINT(ba, FORCESEEK([CI_tblBets(A)](BetId))), TABLE HINT(e, FORCESEEK), TABLE HINT(e1, FORCESEEK([CI_tblEvents(A)](EventId))))--, TABLE HINT(o, FORCESEEK));

	EXEC [Control].Sp_Log	@BatchKey,@Me,'CashoutDifferentials-10004','Start', @RowsProcessed = @@ROWCOUNT

	INSERT INTO #Candidate
	SELECT  'CashoutDif' Action,
			b.BetId,
			CONVERT(datetime2(3),b.BetDate) BetDate,
			b.AllUpID, 
			b.FreeBetID,
			b.CB_Bet_ID_Transformed LegacyBetId,
			b.ClientID,
			b.BetType,
			CONVERT(smallint,0) RewardBetTypeId,
			b.Channel,
			0 ActionChannel,
			ISNULL(LT.CorrectedDate,b.FromDate) AS FromDate,			-- Use Corrected FromDate by joining to Latency Table
			b.EventId, 
			b.CompetitorId,
			0 UserId,
--			c.CashoutType,
			b.BetId TransactionId, 
			ISNULL(LT.CorrectedDate,b.FromDate) AS TransactionDate,		-- Use Corrected FromDate by joining to Latency Table
			ISNULL(b.AmountToWin,0) StakeWin,
			ISNULL(b.AmountToPlace,0) StakePlace,
			ISNULL(b.AmountToWin,0) PreviousStakeWin,
			ISNULL(b.AmountToPlace,0) PreviousStakePlace,
			- c.SettledValue + ISNULL(c1.SettledValue,0) PayoutWin,
			CONVERT(money,0) PayoutPlace, -- Current assumption is that only win bets cash out, may need to rethink apportionment if this changes
			- ISNULL(c1.SettledValue,0) PreviousPayoutWin,
			CONVERT(money,0) PreviousPayoutPlace,
			CASE WHEN ISNULL(b.AmountToWin,0) > 0 THEN 1 ELSE 0 END + CASE WHEN ISNULL(b.AmountToPlace,0) > 0 THEN 2 ELSE 0 END EachWayType,
			10004 MappingId
	FROM	[$(Acquisition)].Intrabet.tblCashoutBets c WITH (FORCESEEK([NI_tblCashoutBets_FD(A)](FromDate)))
			LEFT JOIN [$(Acquisition)].Intrabet.tblCashoutBets c1 WITH (FORCESEEK([NI_tblCashoutBets_TD(A)](ToDate))) ON (c1.BetID = c.BetID AND c1.ToDate = c.FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.Latency LT ON (c.FromDate=LT.OriginalDate)
			INNER JOIN [$(Acquisition)].Intrabet.tblBets b ON (b.BetId = c.BetId AND b.FromDate <= c.FromDate AND b.ToDate > c.FromDate)
	WHERE	c.FromDate > @FromDate
			AND c.FromDate <= @ToDate
			AND c.ToDate > @FromDate
			AND c.SettledValue <> ISNULL(c1.SettledValue,0)
			AND b.Valid = 1
			AND (@MappingId = 10004 OR @MappingId IS NULL)
	OPTION (RECOMPILE, TABLE HINT(b, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'DoubleDownFees-10005','Start', @RowsProcessed = @@ROWCOUNT

	INSERT INTO #Candidate
	SELECT  'Fee' + CASE WHEN t.TransactionCode < 0 THEN '+' ELSE '-' END Action,
			b.BetId,
			CONVERT(datetime2(3),b.BetDate) BetDate,
			b.AllUpID, 
			b.FreeBetID,
			b.CB_Bet_ID_Transformed LegacyBetId,
			b.ClientID,
			b.BetType,
			CONVERT(smallint,0) RewardBetTypeId,
			b.Channel,
			b.Channel ActionChannel,
			ISNULL(LT.CorrectedDate,b.FromDate) AS FromDate,			-- Use Corrected FromDate by joining to Latency Table
			b.EventId, 
			b.CompetitorId,
			b.UserCode UserId,
--			0 CashoutType,
			b.BetId TransactionId, 
			CASE WHEN pAmount = 0 THEN t.TransactionDate ELSE ISNULL(LT.CorrectedDate,b.FromDate) END TransactionDate,
			ISNULL(t.Amount,0) StakeWin,
			CONVERT(money,0) StakePlace,
			ISNULL(t.pAmount,0) PreviousStakeWin,
			CONVERT(money,0) PreviousStakePlace,
			CONVERT(money,0) PayoutWin,
			CONVERT(money,0) PayoutPlace,
			CONVERT(money,0) PreviousPayoutWin,
			CONVERT(money,0) PreviousPayoutPlace,
			CASE WHEN ISNULL(b.AmountToWin,0) > 0 THEN 1 ELSE 0 END + CASE WHEN ISNULL(b.AmountToPlace,0) > 0 THEN 2 ELSE 0 END EachWayType,
			10005 MappingId
	FROM	(	SELECT	FromDate, TransactionId, BetId, TransactionCode, MAX(TransactionDate) TransactionDate, SUM(Amount) Amount, SUM(pAmount) pAmount
				FROM	(	SELECT	FromDate, TransactionId, BetId, TransactionCode, TransactionDate, CASE WHEN TransactionCode < 0 THEN - Amount ELSE Amount END Amount, CONVERT(money,0) pAmount 
							FROM	[$(Acquisition)].Intrabet.tblTransactions WITH (FORCESEEK([NI_tblTransactions_FD(A)](FromDate))) 
							WHERE	TransactionCode IN (1004, -1004) AND FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
							UNION ALL
							SELECT	ToDate FromDate, TransactionId, BetId, TransactionCode, TransactionDate, CONVERT(money,0) Amount, CASE WHEN TransactionCode < 0 THEN - Amount ELSE Amount END pAmount 
							FROM	[$(Acquisition)].Intrabet.tblTransactions WITH (FORCESEEK([NI_tblTransactions_TD(A)](ToDate))) 
							WHERE	TransactionCode IN (1004, -1004) AND ToDate > @FromDate AND ToDate <= @ToDate
						) x
				GROUP BY FromDate, TransactionId, BetId, TransactionCode
				HAVING SUM(Amount) <> SUM(pAmount)
			) t
--			LEFT JOIN [$(Acquisition)].Intrabet.tblTransactions t1 WITH (FORCESEEK([NI_tblTransactions_TD(A)](ToDate))) ON (t1.TransactionID = t.TransactionID AND t1.ToDate = t.FromDate)
			LEFT JOIN [$(Acquisition)].IntraBet.Latency LT ON (t.FromDate=LT.OriginalDate)
			INNER JOIN [$(Acquisition)].Intrabet.tblBets b ON (b.BetId = t.BetId AND b.FromDate <= t.FromDate AND b.ToDate > t.FromDate)
	WHERE	(@MappingId = 10005 OR @MappingId IS NULL)
	OPTION (RECOMPILE, TABLE HINT(b, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'PointsAccrued-10006','Start', @RowsProcessed = @@ROWCOUNT

 	SELECT	BetId, CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ExecutedAt)) ExecutedAt, TypeOfBet, FromDate, SUM(Points) Points
	INTO	#Messages
	FROM	[$(Acquisition)].Messaging.RewardsPointsAccrued
	WHERE	FromDate > @FromDate 
			AND FromDate <= @ToDate
			AND ToDate > @FromDate 
			AND (@MappingId = 10006 OR @MappingId IS NULL)
			AND isDuplicate = 0
	GROUP BY BetId, TypeOfBet, ExecutedAt, FromDate -- we do see complete duplicate messages which are double counted in the accruals, so summarise where this occurs
	OPTION (RECOMPILE)

	INSERT INTO #Candidate
	SELECT  'Accrual' Action,
			b.BetId,
			CONVERT(datetime2(3),b.BetDate) BetDate,
			b.AllUpID, 
			b.FreeBetID,
			b.CB_Bet_ID_Transformed LegacyBetId,
			b.ClientID,
			b.BetType,
			CASE ISNUMERIC(a.TypeOfBet) WHEN 1 THEN a.TypeOfBet ELSE -1 END RewardBetTypeId,
			b.Channel,
			0 ActionChannel,
			a.FromDate,
			b.EventId, 
			b.CompetitorId,
			b.UserCode UserId,
			b.BetId TransactionId, 
			a.ExecutedAt TransactionDate, 
			CONVERT(money,a.Points) StakeWin,
			CONVERT(money,0) StakePlace,
			CONVERT(money,0) PreviousStakeWin,
			CONVERT(money,0) PreviousStakePlace,
			CONVERT(money,0) PayoutWin,
			CONVERT(money,0) PayoutPlace,
			CONVERT(money,0) PreviousPayoutWin,
			CONVERT(money,0) PreviousPayoutPlace,
			CASE WHEN ISNULL(b.AmountToWin,0) > 0 THEN 1 ELSE 0 END + CASE WHEN ISNULL(b.AmountToPlace,0) > 0 THEN 2 ELSE 0 END EachWayType,
			10006 MappingId
	FROM	#Messages a
			INNER JOIN [$(Acquisition)].Intrabet.tblBets b ON b.BetId = a.BetId AND b.ToDate >= CONVERT(datetime2(3), '9999-12-30')
	OPTION (RECOMPILE, TABLE HINT(b, FORCESEEK([CI_tblBets(A)](BetId))))

	---------------------------------------------------------------
	--- PART 2 - CONVERT CANDIDATE TABLE INTO ATOMIC TABLE LOAD ---
	---------------------------------------------------------------

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Feature Flags','Start', @RowsProcessed = @@ROWCOUNT

	-- Find any bets carrying Feature Tags in the current candidate list
	SELECT	BetId, FeatureCode 
	INTO	#FeatureTags
	FROM	[$(Acquisition)].Intrabet.tblBetFeatureTags 
	WHERE	ToDate >= CONVERT(datetime2(3),'9999-12-30')
			AND BetID IN (SELECT DISTINCT BetId FROM #Candidate)
	UNION ALL
	SELECT	e.BetId, n.Name FeatureCode 
	FROM	[$(Acquisition)].Messaging.FeatureEngagement e
			INNER JOIN [$(Acquisition)].Messaging.WebFeatureNames n ON n.WebFeatureKey = e.WebFeatureKey AND n.ToDate >= CONVERT(datetime2(3),'9999-12-30')
	WHERE	e.ToDate >= CONVERT(datetime2(3),'9999-12-30')
			AND e.BetID IN (SELECT DISTINCT BetId FROM #Candidate)

	-- Identify any Feature Codes not already in the cache and assign them flags based on ascending powers of 2 starting from next power of two after highest existing flag
	INSERT	Cache.FeatureFlag
	SELECT	DISTINCT POWER(2,DENSE_RANK() OVER (ORDER BY t.FeatureCode) + x.Val) Flag, t.FeatureCode
	FROM	#FeatureTags t
			LEFT JOIN Cache.FeatureFlag f ON f.FeatureCode = t.FeatureCode
			CROSS APPLY (SELECT  ISNULL(LOG10(MAX(Flag))/LOG10(2),-1) Val FROM Cache.FeatureFlag) x -- Get the power to which 2 has been raised in the highest existing flag - i.e. Flag = 32, Power = 5
	WHERE	f.FeatureCode IS NULL

	-- Convert any Feature Tags present into their equivalent binary flag values and then sum the distinct list of flags for each bet present in the tagged list to get one row per bet with a single binary column containing all featutre flags
	SELECT	BetId, SUM(DISTINCT Flag) FeatureFlags 
	INTO	#FeatureFlags
	FROM	#FeatureTags t
			INNER JOIN Cache.FeatureFlag f  ON f.FeatureCode = t.FeatureCode
	GROUP BY BetId

	EXEC [Control].Sp_Log	@BatchKey,@Me,'AllUps','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	c.BetId, c.FreeBetID, c.AllUpId, c.LegacyBetId, c.ClientID, c.RewardBetTypeId, c.Channel, c.ActionChannel, c.TransactionId, c.TransactionDate, c.BetDate, c.UserId, /*c.CashoutType,*/ c.FromDate, c.StakeWin + c.StakePlace BetStake, c.PayoutWin + c.PayoutPlace BetPayout,
			CASE
				WHEN (c.PreviousStakeWin + c.PreviousStakePlace) = 0 THEN 0
				ELSE (c.StakeWin + c.StakePlace) - (c.PreviousStakeWin + c.PreviousStakePlace)
			END BetStakeDelta, 
			CASE
				WHEN (c.PreviousPayoutWin + c.PreviousPayoutPlace) = 0 THEN 0
				ELSE (c.PayoutWin + c.PayoutPlace) - (c.PreviousPayoutWin + c.PreviousPayoutPlace)
			END  BetPayoutDelta,
			a.BetId LegId, DENSE_RANK() OVER (PARTITION BY c.BetId, c.FromDate, c.Action, c.TransactionId ORDER BY a.BetId) LegNumber, COUNT(*) OVER (PARTITION BY c.BetId, c.FromDate, c.Action, c.TransactionId) NumberOfLegs,
			c.Action, a.AllUpType,
			a.WinMultiFactor,
			a.PlaceMultiFactor, 
			c.MappingId
	INTO	#AllUps
	FROM	#Candidate c  
--			INNER JOIN	[$(Acquisition)].Intrabet.tblBets b  on (c.BetID = b.BetID AND b.ToDate >= CONVERT(datetime2(3), '9999-12-30')) 
			INNER JOIN	[$(Acquisition)].Intrabet.tblAllUps a  ON (a.AllUpID = c.AllUpID AND a.ToDate >= CONVERT(datetime2(3), '9999-12-30'))
	WHERE	c.AllUpId IS NOT NULL
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Mulit Legs','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	c.BetId, c.FreeBetID, c.AllUpId, c.LegacyBetId, c.ClientID, c.BetType, c.RewardBetTypeId, c.Channel, c.ActionChannel, c.TransactionId, c.TransactionDate, c.BetDate, c.UserId, /*c.CashoutType,*/ c.FromDate, c.BetStake, c.BetPayout, 
			CASE
				WHEN z.PositionType = 'Win' THEN c.PriceToWin
				WHEN z.PositionType = 'Place' THEN c.PriceToPlace
				ELSE c.PriceToWin
			END BetPrice,
			c.BetStakeDelta, c.BetPayoutDelta, 
			c.LegId, c.LegNumber, c.NumberOfLegs, z.LegType, c.EventId, c.CompetitorId,
			c.BetStake/c.NumberOfLegs LegStake, 
			ROUND(c.BetStake/c.NumberOfLegs,4) 
			+	CASE 
					WHEN (c.BetStake - c.NumberOfLegs * ROUND(c.BetStake/c.NumberOfLegs,4))/0.0001 >= LegNumber THEN 0.0001
					WHEN (c.BetStake - c.NumberOfLegs * ROUND(c.BetStake/c.NumberOfLegs,4))/0.0001 <= -LegNumber THEN -0.0001
					ELSE 0.0000
				END LegStakeTrued, 
			c.BetPayout/c.NumberOfLegs LegPayout,
			ROUND(c.BetPayout/c.NumberOfLegs,4) 
			+	CASE 
					WHEN (c.BetPayout - c.NumberOfLegs * ROUND(c.BetPayout/c.NumberOfLegs,4))/0.0001 >= LegNumber THEN 0.0001
					WHEN (c.BetPayout - c.NumberOfLegs * ROUND(c.BetPayout/c.NumberOfLegs,4))/0.0001 <= -LegNumber THEN -0.0001
					ELSE 0.0000
				END LegPayoutTrued,
			z.PositionNumber, z.NumberOfPositions, z.PositionType, CONVERT(decimal(19,8),CASE LegType WHEN 'Each Way' THEN c.BetStake/(c.NumberOfLegs * 2) ELSE c.BetStake/c.NumberOfLegs END) PositionStake, CASE LegType WHEN 'Each Way' THEN c.BetPayout/(c.NumberOfLegs * 2) ELSE c.BetPayout/c.NumberOfLegs END PositionPayout,
			c.Action, z.WinPlace, c.MappingId
	INTO	#Legs
	FROM	(	SELECT	c.BetId, c.FreeBetID, c.AllUpId, c.LegacyBetId, c.ClientID, b.BetType, c.RewardBetTypeId, c.Channel, c.ActionChannel, c.TransactionId, c.TransactionDate, c.BetDate, c.UserId, /*c.CashoutType,*/ c.FromDate, c.BetStake, c.BetPayout,
						BetStakeDelta, BetPayoutDelta,
						c.LegId, c.LegNumber, c.NumberOfLegs, b.EventId, b.CompetitorId,
						c.Action, c.AllUpType,
						COALESCE(CONVERT(money,NULLIF(c.WinMultiFactor, 0)),CONVERT(money,NULLIF(b.PriceToWin,0)),-1) PriceToWin,
						COALESCE(CONVERT(money,NULLIF(c.PlaceMultiFactor, 0)),CONVERT(money,NULLIF(b.PriceToPlace,0)),-1) PriceToPlace, 
						c.MappingId
				FROM	#AllUps c  
						INNER JOIN	[$(Acquisition)].Intrabet.tblBets b  on (b.BetID = c.LegID AND b.ToDate >= CONVERT(datetime2(3), '9999-12-30'))-- Only using these join to find basic bet information rather than status/amounts so the latest version is more than adequate and much faster than the one contemporary with the candidate triggering record
				WHERE	c.AllUpId IS NOT NULL
			) c
			INNER JOIN	(	SELECT 1 AllUpType, 1 WinPlace, 'Win' LegType, 1 PositionNumber, 1 NumberOfPositions, 'Win' PositionType UNION ALL 
							SELECT 2 AllUpType, 1 WinPlace, 'Each Way' LegType, 1 PositionNumber, 2 NumberOfPositions, 'Win' PositionType UNION ALL 
							SELECT 2 AllUpType, 2 WinPlace, 'Each Way' LegType, 2 PositionNumber, 2 NumberOfPositions, 'Place' PositionType UNION ALL
							SELECT 3 AllUpType, 2 WinPlace, 'Place' LegType, 1 PositionNumber, 1 NumberOfPositions, 'Place' PositionType UNION ALL 
							SELECT 4 AllUpType, 2 WinPlace, 'Place' LegType, 1 PositionNumber, 1 NumberOfPositions, 'Place' PositionType UNION ALL 
							SELECT 5 AllUpType, 1 WinPlace, 'Win' LegType, 1 PositionNumber, 1 NumberOfPositions, 'Win' PositionType UNION ALL
							SELECT 6 AllUpType, 1 WinPlace, 'Win' LegType, 1 PositionNumber, 1 NumberOfPositions, 'Win' PositionType UNION ALL 
							SELECT 7 AllUpType, 1 WinPlace, 'Each Way' LegType, 1 PositionNumber, 2 NumberOfPositions, 'Win' PositionType UNION ALL 
							SELECT 7 AllUpType, 2 WinPlace, 'Each Way' LegType, 2 PositionNumber, 2 NumberOfPositions, 'Place' PositionType UNION ALL
							SELECT 10 AllUpType, 1 WinPlace, 'Win' LegType, 1 PositionNumber, 1 NumberOfPositions, 'Win' PositionType UNION ALL 
							SELECT 14 AllUpType, 2 WinPlace, 'Place' LegType, 1 PositionNumber, 1 NumberOfPositions, 'Place' PositionType
						) z ON (z.AllUpType = c.AllUpType)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Single Legs','Start', @RowsProcessed = @@ROWCOUNT

	INSERT	#Legs
	SELECT	c.BetId, c.FreeBetID, c.AllUpID, c.LegacyBetId, c.ClientID, c.BetType, c.RewardBetTypeId, c.Channel, c.ActionChannel, c.TransactionId, c.TransactionDate, c.BetDate, c.UserId, /*c.CashoutType,*/ c.FromDate, c.StakeWin + c.StakePlace BetStake, c.PayoutWin + c.PayoutPlace BetPayout,
			CASE
				WHEN z.PositionType = 'Win' THEN COALESCE(NULLIF(b.PriceToWin,0), -1)
				WHEN z.PositionType = 'Place' THEN COALESCE(NULLIF(b.PriceToPlace,0), -1)
				ELSE COALESCE(NULLIF(b.PriceToWin,0), -1)
			END BetPrice,
			CASE
				WHEN (c.PreviousStakeWin + c.PreviousStakePlace) = 0 THEN 0
				ELSE (c.StakeWin + c.StakePlace) - (c.PreviousStakeWin + c.PreviousStakePlace)
			END BetStakeDelta, 
			CASE
				WHEN (c.PreviousPayoutWin + c.PreviousPayoutPlace) = 0 THEN 0
				ELSE (c.PayoutWin + c.PayoutPlace) - (c.PreviousPayoutWin + c.PreviousPayoutPlace)
			END  BetPayoutDelta,
			c.BetId LegId, 1 LegNumber, 1 NumberOfLegs, z.LegType, c.EventID, c.CompetitorId, c.StakeWin + c.StakePlace LegStake, c.StakeWin + c.StakePlace LegStakeTrued, c.PayoutWin + c.PayoutPlace LegPayout, c.PayoutWin + c.PayoutPlace LegPayoutTrued,
			z.PositionNumber, z.NumberOfPositions, z.PositionType, c.StakeWin * WinFactor + c.StakePlace * PlaceFactor PositionStake, c.PayoutWin * WinFactor + c.PayoutPlace * PlaceFactor PositionPayout,
			c.Action, z.WinPlace, 
			c.MappingId
	FROM	#Candidate c
			INNER JOIN	[$(Acquisition)].Intrabet.tblBets b  on (c.BetID = b.BetID AND b.ToDate >= CONVERT(datetime2(3), '9999-12-30')) 
			INNER JOIN (	SELECT 0 Fees, 1 EachWayType, 1 WinPlace, 'Win' LegType, 1 PositionNumber, 1 NumberOfPositions, 'Win' PositionType, 1 WinFactor, 0 PlaceFactor UNION ALL 
							SELECT 0 Fees, 2 EachWayType, 2 WinPlace, 'Place' LegType, 1 PositionNumber, 1 NumberOfPositions, 'Place' PositionType, 0 WinFactor, 1 PlaceFactor UNION ALL 
							SELECT 0 Fees, 3 EachWayType, 1 WinPlace, 'Each Way' LegType, 1 PositionNumber, 2 NumberOfPositions, 'Win' PositionType, 1 WinFactor, 0 PlaceFactor UNION ALL 
							SELECT 0 Fees, 3 EachWayType, 2 WinPlace, 'Each Way' LegType, 2 PositionNumber, 2 NumberOfPositions, 'Place' PositionType, 0 WinFactor, 1 PlaceFactor UNION ALL 
							SELECT 1 Fees, 1 EachWayType, 1 WinPlace, 'Win' LegType, 1 PositionNumber, 1 NumberOfPositions, 'Win' PositionType, 1 WinFactor, 0 PlaceFactor UNION ALL 
							SELECT 1 Fees, 3 EachWayType, 1 WinPlace, 'Each Way' LegType, 1 PositionNumber, 1 NumberOfPositions, 'Win' PositionType, 1 WinFactor, 0 PlaceFactor
						) z ON (z.EachWayType = c.EachWayType AND z.Fees = CASE Action WHEN 'Fee+' THEN 1 WHEN 'Fee-' THEN 1 ELSE 0 END)
	WHERE	c.AllUpId IS NULL
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Leg Events 1','Start', @RowsProcessed = @@ROWCOUNT

	SELECT DISTINCT LegId, BetDate, EventId INTO #Legs1 FROM #Legs

	DECLARE @MinBetDate datetime2(3)

	SELECT @MinBetDate = MIN(BetDate) FROM #Legs1

	SELECT	c.LegId, c.BetDate, c.EventId, e.EventType, e.MasterEventId, e.IsLiveEvent, e.IsFeaturedEvent
	INTO	#LegEvents1
	FROM	#Legs1 c
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents e ON e.EventId = c.EventId AND e.FromDate <= c.BetDate AND e.ToDate > c.BetDate AND e.ToDate > @MinBetDate
	OPTION(RECOMPILE, TABLE HINT(e, FORCESEEK([CI_tblEvents(A)](EventId))))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Leg Events 2','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	b.LegId, b.BetDate, b.EventId, b.EventType, COALESCE(m.IsLiveEvent,b.IsLiveEvent,0) IsLiveEvent, ISNULL(b.IsFeaturedEvent,0) IsFeaturedEvent
	INTO	#LegEvents2
	FROM	#LegEvents1 b
			LEFT JOIN [$(Acquisition)].IntraBet.tblEvents m ON m.EventId = b.MasterEventId AND m.FromDate <= b.BetDate AND m.ToDate > b.BetDate AND m.IsLiveEvent = 1 AND ISNULL(b.IsLiveEvent,0) = 0
	OPTION(RECOMPILE, TABLE HINT(m, FORCESEEK([CI_tblEvents(A)](EventId))))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Leg Events 3','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	b.LegId, b.BetDate, b.EventId, b.EventType, b.IsLiveEvent, b.IsFeaturedEvent, CONVERT(datetime2(3),ISNULL(MIN(r.CloseTime),'9999-12-31')) CutOff
	INTO	#LegEvents3
	FROM	#LegEvents2 b
			LEFT JOIN [$(Acquisition)].PricingData.Race r ON r.IntrabetId = b.EventId AND b.IsFeaturedEvent = 1 AND r.CloseTime IS NOT NULL
	GROUP BY b.LegId, b.BetDate, b.EventId, b.EventType, b.IsLiveEvent, b.IsFeaturedEvent
	OPTION(RECOMPILE, TABLE HINT(r, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Leg Events','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	b.LegId,
			b.BetDate,
			b.EventId,
			b.EventType,
			CASE
				WHEN o.BetDetails = 'LiveBet' THEN 'Y'
				WHEN b.isLiveEvent = 1 THEN 'Y'
				WHEN b.BetDate >= b.CutOff THEN 'Y'
				ELSE 'N' 
			END InPlay,
			CASE
				WHEN o.BetDetails = 'LiveBet' THEN 'Y'
				ELSE 'N' 
			END ClickToCall
	INTO	#LegEvents
	FROM	#LegEvents3 b
			LEFT JOIN [$(Acquisition)].Intrabet.tblOtherBets o ON o.BetId = b.LegId AND o.FromDate <= b.BetDate AND o.ToDate > b.BetDate 
	OPTION(RECOMPILE, TABLE HINT(o, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Atomic Base','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	c1.Action,
			c1.TransactionId,
			'IBI' TransactionIdSource,
			c1.UserId,
			CASE c1.UserId WHEN 0 THEN 'N/A' ELSE 'IUU' END UserIdSource,
			c1.TransactionDate MasterTransactionTimestamp, 
			c1.FromDate, 
			-- BET GROUP - e.g. Full cover bet such as Goliath or Double Down bets and their subsequent actioned options
			COALESCE(ag.AllUpGroupBetID, eg.ExoticGroupBetID, c1.BetID) BetGroupID, -- If there is an exotic grouping for the bet then force the ExoticGroupBetId as the ContractId to ensure everything is grouped as a Contract  
			'IBI' BetGroupIDSource,
			COALESCE(gg.GroupDesc, 'Straight') BetGroupType,
			eg.ExoticGroupBetID, 
			ISNULL(a.AccountID,-1) LedgerID, 
			CASE WHEN a.AccountID IS NULL THEN 'N/A' ELSE 'IAA' END LedgerSource,
			ISNULL(c1.Channel,0) Channel,
			ISNULL(c1.ActionChannel,0) ActionChannel,
			ISNULL(ISNULL(t.BonusTransID,p.FreeBetID),0) FreeBetId,
			CASE WHEN t.BonusTransID IS NOT NULL THEN 'IBB' WHEN p.FreeBetID IS NOT NULL THEN 'IBF' ELSE 'N/A' END FreeBetIdSource,

			-- WalletID, PromotionID
			CASE
				WHEN c1.Action = 'Accrual' THEN 'L'
				WHEN (p.FreeBetID IS NULL OR p.FreeBetID = 0) THEN 'C' 
				WHEN (p.FreeBetID IS NOT NULL OR p.FreeBetID = 1) THEN 'B'  
				ELSE 'U' 
			END WalletID, 
			CASE WHEN p.FreeBetID IS NOT NULL THEN ISNULL(p.PromotionID,p.FreeBetID) WHEN t.BonusTransID IS NOT NULL THEN -1 ELSE 0 END PromotionId, 
			CASE WHEN t.BonusTransID IS NOT NULL THEN 'IBB' WHEN p.FreeBetID IS NOT NULL AND p.PromotionID IS NOT NULL THEN 'IBP' WHEN p.FreeBetID IS NOT NULL AND p.PromotionID IS NULL THEN 'IBF' ELSE 'N/A' END PromotionIdSource, 

			-- BET
			c1.BetId,
			'IBI' BetIdSource,
			c1.StakeWin + c1.StakePlace BetStake, 
			c1.PayoutWin + c1.PayoutPlace BetPayout,
			l.BetPrice,
			CASE
				WHEN (c1.PreviousStakeWin + c1.PreviousStakePlace) = 0 THEN 0
				ELSE (c1.StakeWin + c1.StakePlace) - (c1.PreviousStakeWin + c1.PreviousStakePlace)
			END BetStakeDelta, 
			CASE
				WHEN (c1.PreviousPayoutWin + c1.PreviousPayoutPlace) = 0 THEN 0
				ELSE (c1.PayoutWin + c1.PayoutPlace) - (c1.PreviousPayoutWin + c1.PreviousPayoutPlace)
			END  BetPayoutDelta,
			c1.RewardBetTypeId,
			CASE c1.RewardBetTypeId WHEN 0 THEN 'N/A' ELSE 'MRB' END RewardBetTypeSource,

			-- PriceTypeID
			c1.BetType PriceTypeID, 
			'IBB' PriceTypeSource, --Intrabet.tblBets.BetType

			c1.LegacyBetId,
			l.NumberOfLegs, 

			-- LEG
			l.LegId, 
			'IBI' LegIdSource,
			l.LegStake, 
			l.LegStakeTrued, 
			l.LegPayout, 
			l.LegPayoutTrued, 
			l.LegNumber, 
			l.BetType LegBetType,
			l.LegType,
			l.WinPlace,
			ISNULL(e.EventType,-1) ClassId, 
			CASE WHEN e.EventType IS NULL THEN 'N/A' ELSE 'IBT' END ClassIdSource,
			l.EventID, 
			'IEI' EventIdSource,
			l.EventID MarketID, --NEW
			'IEI' MarketIdSource, --NEW
			l.CompetitorID,
			l.NumberOfPositions, 
			-- POSITION
			l.PositionNumber, 
			l.PositionType, 
			l.PositionStake, 
			l.PositionPayout,

			e.InPlay, 
			e.ClickToCall,
			CASE WHEN et.betid IS NOT NULL THEN 1 ELSE 0 END IsChaseTheAce,
			ISNULL(ff.FeatureFlags,0) FeatureFlags, 
			0 isFirstBet, 
			CASE WHEN rl.ClientId IS NULL THEN 0 ELSE 1 END isFirstAccrual,

			c1.MappingId
	INTO	#AtomicBase
	FROM	#Candidate c1
			INNER JOIN	#Legs l ON l.BetId = c1.BetId AND l.TransactionDate = c1.TransactionDate AND l.FromDate = c1.FromDate AND l.Action = c1.Action
			INNER JOIN	#LegEvents e ON e.LegId = l.LegId AND e.BetDate = l.BetDate AND e.EventId = l.EventId
			LEFT JOIN	#FeatureFlags ff on ff.BetId = c1.BetId
			LEFT JOIN	[$(Acquisition)].Intrabet.tblAllUpGroupBets ag  ON (ag.BetID = c1.BetID AND ag.ToDate >= CONVERT(datetime2(3), '9999-12-30'))
			LEFT JOIN	[$(Acquisition)].Intrabet.tblLUAllupGroups gg  ON (gg.AllupGroupID = ag.AllupGroupID AND gg.ToDate >= CONVERT(datetime2(3), '9999-12-30'))
			LEFT JOIN	[$(Acquisition)].Intrabet.tblExoticGroupBets eg  ON (eg.BetID = c1.BetID AND eg.ToDate >= CONVERT(datetime2(3), '9999-12-30'))
			LEFT JOIN	[$(Acquisition)].Intrabet.tblBonusTransactions t  ON (t.BetID = c1.BetID AND t.SettlingCode IS NULL AND t.BTransCode = -4 AND t.ToDate >= CONVERT(datetime2(3), '9999-12-30'))
			LEFT JOIN	[$(Acquisition)].Intrabet.tblPromotionalBets p  ON (p.FreeBetID = c1.FreeBetID AND p.ToDate >= CONVERT(datetime2(3), '9999-12-30'))
			LEFT JOIN	[$(Acquisition)].Intrabet.tblAccounts a  ON (a.ClientId = c1.ClientId AND a.AccountNumber = 1 AND a.ToDate >= CONVERT(datetime2(3), '9999-12-30'))
			LEFT JOIN	[$(Acquisition)].IntraBet.Entries et  ON (et.BetId = c1.BetId AND et.ToDate >= CONVERT(datetime2(3),'9999-12-30'))
			LEFT JOIN	Cache.RewardLifetime rl  ON rl.ClientId = c1.ClientId AND rl.FromDate = c1.TransactionDate AND rl.FirstAccrualBetId = c1.BetId 
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Atomic Detail','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	CASE 
				WHEN c1.Action <> 'Settled' THEN c1.Action
				WHEN c1.BetPayout <> 0 THEN 'Win'
				ELSE 'Loss'
			END Action, -- currently derive specific settlement outcome at this point but should push back into #Candidate for when scratching splits outcomes and multiple records are required.
			c1.TransactionId,
			c1.TransactionIdSource,
			c1.UserId,
			c1.UserIdSource,
			c1.MasterTransactionTimestamp, 
			c1.FromDate, 
			-- BET GROUP - e.g. Full cover bet such as Goliath or Double Down bets and their subsequent actioned options
			COALESCE(d.MasterBetId, c1.BetGroupID) BetGroupID, -- If there is an exotic grouping for the bet then force the ExoticGroupBetId as the ContractId to ensure everything is grouped as a Contract  
			c1.BetGroupIDSource,
			COALESCE(CASE WHEN d.MasterBetId IS NOT NULL THEN 'Double Down' ELSE NULL END, c1.BetGroupType) BetGroupType,
			c1.LedgerID, 
			c1.LedgerSource,
			c1.Channel,
			c1.ActionChannel,
			c1.FreeBetId,
			c1.FreeBetIdSource,

			-- WalletID, PromotionID
			c1.WalletID, 
			c1.PromotionId, 
			c1.PromotionIdSource, 

			-- BET
			c1.BetId,
			c1.BetIdSource,
			c1.BetStake, 
			c1.BetPayout, 
			COALESCE(g.AmountToWin, c1.BetPrice) BetPrice,
			c1.BetStakeDelta, 
			c1.BetPayoutDelta,  
			CASE WHEN bm.BetTypeName <> 'Exotic' AND c1.NumberOfLegs > 1 THEN 'Multi' ELSE ISNULL(bm.BetTypeName,'Unknown') END BetType,
			CASE WHEN bm.BetSubTypeName = 'Exacta' AND g.GroupBetType = 1 THEN 'Quinella' ELSE ISNULL(bm.BetSubTypeName,'Unknown') END BetSubType,  -- A GroupBetType = 1 redefines an Exacta to a Quinella, otherwise the BetSubType is as defined by the inbound BetType
			c1.RewardBetTypeId,
			c1.RewardBetTypeSource,

			-- PriceTypeID
			c1.LegBetType PriceTypeID, 
			c1.PriceTypeSource, --Intrabet.tblBets.BetType

			c1.LegacyBetId,
			CASE WHEN g.BetID IS NULL THEN 1 WHEN ISNULL(g.AmountToWin,0) = 0 THEN 0 ELSE c1.BetStake/g.AmountToWin END NumberOfPermutations,
			CASE WHEN g.BetID IS NULL THEN c1.BetStake ELSE ISNULL(g.AmountToWin,0) END PermutationStake,
			c1.NumberOfLegs, 
			-- LEG
			c1.LegId, 
			c1.LegIdSource,
			c1.LegStake, 
			c1.LegStakeTrued, 
			c1.LegPayout, 
			c1.LegPayoutTrued, 
			c1.LegNumber, 
			CASE WHEN bm.BetSubTypeName IN('Exacta','Quinella','Trifecta','First 4') THEN 'Forecast' ELSE ISNULL(c1.LegType,'Unknown') END LegType, 
			c1.ClassId, 
			c1.ClassIdSource,
			c1.EventID, 
			c1.EventIdSource,
			c1.EventID MarketID, --NEW
			c1.MarketIdSource, --NEW
			ISNULL(g.CompetitorID, c1.CompetitorID)  LegCompetitorId, 
			'ICI' LegCompetitorIdSource,						
			ISNULL(MAX(g.Position) OVER (PARTITION BY c1.LegId) - MIN(g.Position) OVER (PARTITION BY c1.LegId) + 1,c1.NumberOfPositions) NumberOfPositions, 
			-- POSITION
			ISNULL(g.Position - MIN(g.Position) OVER (PARTITION BY c1.LegId) + 1,c1.PositionNumber) PositionNumber, 
			c1.PositionStake * c1.NumberOfPositions PositionStake, -- Need to multiply the Position Stake/Payout by the number of Positions for candidate Each Ways which will already have arrived with the stake split by posiiton according to the source record to negate the effect of being divided by number of positions again in the next level of this query
			c1.PositionPayout * c1.NumberOfPositions PositionPayout, 
			c1.PositionType,
			ISNULL(MAX(g.GroupId) OVER (PARTITION BY c1.LegId, g.Position) - MIN(g.GroupId) OVER (PARTITION BY c1.LegId, g.Position) + 1,1) NumberOfGroupings,  
			-- GROUP
			ISNULL(g.GroupID - MIN(g.GroupId) OVER (PARTITION BY c1.LegId, g.Position) + 1,1) GroupingNumber, 
			CASE
				WHEN g.BetID is not NULL and bm.GroupingName='Straight'
				THEN 'Multiple'
				ELSE ISNULL(CASE
								WHEN c1.ExoticGroupBetID is not null
								THEN 'Roving'
								Else CASE
										WHEN bm.GroupingName='Straight'
										THEN 'Standard'
										ELSE bm.GroupingName
										END
								END,'Unknown')
			END	 GroupingType,
			(CONVERT(VARCHAR(100),c1.EventID) + '-' + CONVERT(VARCHAR(100),ISNULL(g.CompetitorID, c1.CompetitorID))) CompetitorId,
			'ICI' CompetitorIdSource,
			c1.InPlay, 
			c1.ClickToCall, 
			ISNULL(o.CashoutType,0) CashoutType, 
			CASE WHEN d.MasterBetId = d.BetId THEN 1 ELSE 0 END IsDoubleDown,
			CASE WHEN d.MasterBetId <> d.BetId THEN 1 ELSE 0 END IsDoubledDown,
			c1.IsChaseTheAce,
			c1.FeatureFlags, 
			c1.isFirstBet,
			c1.isFirstAccrual,
			c1.MappingId
	INTO	#AtomicDetail
	FROM	#AtomicBase c1
			LEFT JOIN	[$(Acquisition)].Intrabet.tblGroupedBets g  ON (g.BetID = c1.LegID AND g.ToDate >= CONVERT(datetime2(3), '9999-12-30')) -- join only to the first of any related GroupedBets records to get the grouping type as additional type information
			LEFT JOIN	[$(Acquisition)].IntraBet.tblCashoutBets o  ON (o.BetId = c1.LegId AND o.FromDate <= c1.FromDate AND o.ToDate > c1.FromDate)
			LEFT JOIN	[$(Acquisition)].IntraBet.tblBetsDoubleDown d  ON (d.BetId = c1.LegId AND d.ToDate >= CONVERT(datetime2(3),'9999-12-30')) -- U-G-L-Y!!! The Double down record often gets created a fraction of a second after the Bet record, so can't use our usual join method of get the version of the record that was current when the bet was created, instead, have to take the one that was current at the end of time and hope the batch latency is sufficient to ensure the Double Down is created before the batch goes looking for it!!
			LEFT JOIN	Intrabet.BetTypeMapping bm  on (bm.BetType = c1.LegBetType AND bm.WinPlace = c1.WinPlace)
	OPTION (RECOMPILE, TABLE HINT(g, FORCESEEK([CI_tblGroupedBets(A)](BetId))), TABLE HINT(o, FORCESEEK([CI_tblCashoutBets(A)](BetId))), TABLE HINT(d, FORCESEEK([CI_tblBetsDoubleDown(A)](BetId))))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Atomic','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	CASE 
				WHEN c2.NumberOfLegs = 1 AND c2.BetType <> 'Exotic' AND c.Scratched = 1 THEN 'Scratch' -- Only implement scratching for non-exotic single bets currently
				ELSE c2.Action
			END Action,
			c2.TransactionId, TransactionIdSource, UserId, UserIdSource, c2.MasterTransactionTimestamp, c2.FromDate,
			c2.BetGroupID, c2.BetGroupIDSource, c2.BetGroupType, c2.LedgerID, c2.LedgerSource, c2.Channel, c2.ActionChannel, 0 CampaignId, 'N/A' CampaignIdSource, c2.FreeBetId, c2.FreeBetIdSource, c2.WalletId, c2.PromotionId, c2.PromotionIdSource, 
			c2.BetId, c2.BetIdSource, c2.BetStake, c2.BetPayout, c2.BetPrice as BetPrice, c2.BetStakeDelta, c2.BetPayoutDelta, c2.BetType, CASE WHEN c2.BetType = 'Multi' THEN CASE c2.NumberOfLegs WHEN 1 THEN 'Single' WHEN 2 THEN 'Double' WHEN 3 THEN 'Treble' WHEN 4 THEN 'Quadrella' ELSE CONVERT(varchar,NumberOfLegs)+' Leg' END ELSE c2.BetSubType END BetSubType, 
			c2.RewardBetTypeId, c2.RewardBetTypeSource,
			c2.PriceTypeId, c2.PriceTypeSource, c2.LegacyBetId, c2.NumberOfPermutations, c2.PermutationStake, c2.NumberOfLegs, 
			c2.LegId, c2.LegIdSource, c2.LegStake, c2.LegStakeTrued, c2.LegPayout, c2.LegPayoutTrued, c2.LegNumber, c2.LegType, c2.ClassId, c2.ClassIdSource, c2.EventID, c2.EventIdSource, c2.MarketId, c2.MarketIdSource, c2.LegCompetitorId, c2.LegCompetitorIdSource, ISNULL(y.NumberOfPositions, c2.NumberOfPositions) NumberOfPositions,
			ISNULL(y.PositionNumber, c2.PositionNumber) PositionNumber,  c2.PositionStake/(ISNULL(y.NumberOfPositions, c2.NumberOfPositions)) PositionStake, c2.PositionPayout/(ISNULL(y.NumberOfPositions, c2.NumberOfPositions)) PositionPayout,
			CASE WHEN c2.LegType = 'Forecast' THEN CASE ISNULL(y.PositionNumber, c2.PositionNumber) WHEN 1 THEN 'Win' WHEN 2 THEN 'Second' WHEN 3 THEN 'Third' WHEN 4 THEN 'Fourth' ELSE 'Unknown' END ELSE c2.PositionType END PositionType, 
			c2.NumberOfGroupings, c2.GroupingNumber, 
			c2.PositionStake/(ISNULL(y.NumberOfPositions, c2.NumberOfPositions) * c2.NumberOfGroupings) GroupingStake, 
			CASE 
				WHEN c2.NumberOfLegs = 1 AND c2.BetType <> 'Exotic' AND c.Scratched = 1 THEN 1.0 -- Scratchings receive full refund across groupings (only working for non-exotic singles currently)
				WHEN c2.Action <> 'Win' THEN 0.0 -- Anything elser which isn't a Win doesn't have a payout
				WHEN c2.NumberOfGroupings = 1 THEN 1.0 -- Any win with single gouping must receive full payout
				WHEN SUM(CASE WHEN r.EventID IS NULL THEN 0 ELSE 1 END) OVER (PARTITION BY c2.LegId, c2.MasterTransactionTimestamp) <> ISNULL(y.NumberOfPositions, c2.NumberOfPositions) THEN 1.0/c2.NumberOfGroupings -- If not every position has a winner then payout will be refund due to scratching, so just distribute evenly across all perms as best option currently
				WHEN r.EventID IS NOT NULL THEN 1.0 -- Assign the entire posiiton payout to the competitor in the grouping which generated the win 
				ELSE 0.0 
			END * c2.PositionPayout/ISNULL(y.NumberOfPositions, c2.NumberOfPositions) GroupingPayout,
			c2.GroupingType, c2.CompetitorId, c2.CompetitorIdSource, ISNULL(c.Scratched,0) Scratched, c2.MappingId, 
			c2.InPlay, c2.ClickToCall, c2.CashoutType, c2.IsDoubleDown, c2.IsDoubledDown,c2.IsChaseTheAce, c2.FeatureFlags, c2.isFirstBet, c2.isFirstAccrual
	INTO	#Atomic
	FROM	#AtomicDetail c2
			LEFT JOIN	(	SELECT	'Exacta' BetSubType, 1 CurrentPositions, 1 PositionNumber, 2 NumberOfPositions UNION ALL -- Boxed bets, unlike all other grouped bets do not have distinct positions against which selected competitors are grouped
							SELECT	'Exacta' BetSubType, 1 CurrentPositions, 2 PositionNumber, 2 NumberOfPositions UNION ALL -- In the interests of consistency and to support identification of winning competitiors versus position to apportion payout 
							SELECT	'Quinella' BetSubType, 1 CurrentPositions, 1 PositionNumber, 2 NumberOfPositions UNION ALL -- shares, this table convets those boxed exotics to have distinct positions against which to common boxed grouping is repeated
							SELECT	'Quinella' BetSubType, 1 CurrentPositions, 2 PositionNumber, 2 NumberOfPositions UNION ALL
							SELECT	'Trifecta' BetSubType, 1 CurrentPositions, 1 PositionNumber, 3 NumberOfPositions UNION ALL
							SELECT	'Trifecta' BetSubType, 1 CurrentPositions, 2 PositionNumber, 3 NumberOfPositions UNION ALL
							SELECT	'Trifecta' BetSubType, 1 CurrentPositions, 3 PositionNumber, 3 NumberOfPositions UNION ALL
							SELECT	'First 4' BetSubType, 1 CurrentPositions, 1 PositionNumber, 4 NumberOfPositions UNION ALL
							SELECT	'First 4' BetSubType, 1 CurrentPositions, 2 PositionNumber, 4 NumberOfPositions UNION ALL
							SELECT	'First 4' BetSubType, 1 CurrentPositions, 3 PositionNumber, 4 NumberOfPositions UNION ALL
							SELECT	'First 4' BetSubType, 1 CurrentPositions, 4 PositionNumber, 4 NumberOfPositions
						) y ON (y.BetSubType = c2.BetSubType AND y.CurrentPositions = c2.NumberOfPositions)
			LEFT JOIN	[$(Acquisition)].IntraBet.tblCompetitors c  ON (c2.BetPayout > 0 AND /*c.Scratched = 1 AND*/ c.EventID = c2.EventID AND c.CompetitorID = c2.LegCompetitorID AND c.FromDate <= c2.FromDate AND c.ToDate > c2.FromDate)
			LEFT JOIN	[$(Acquisition)].IntraBet.tblRacingResults r WITH(FORCESEEK)  ON (c2.Action = 'Win' AND r.EventID = c2.EventID and r.CompetitorID = c2.LegCompetitorId and r.FinalPlacing = ISNULL(y.PositionNumber, c2.PositionNumber) AND r.FromDate <= c2.FromDate AND r.ToDate > c2.FromDate)
	OPTION (RECOMPILE, TABLE HINT(c, FORCESEEK([CI_tblCompetitors(A)](EventId, CompetitorId))), TABLE HINT(r, FORCESEEK([CI_tblRacingResults(A)](EventId, CompetitorId))))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @RowsProcessed = @@ROWCOUNT

	INSERT  Atomic.Bet_New
			(	BatchKey, Action, TransactionId, TransactionIdSource, UserId, UserIdSource, MasterTransactionTimestamp, FromDate,
				BetGroupID, BetGroupIDSource, BetGroupType, LedgerID, LedgerSource, Channel, ActionChannel, CampaignId, CampaignIdSource, FreeBetId, FreeBetIdSource, WalletId, PromotionId, PromotionIdSource, 
				BetId,BetIdSource,BetStake,BetPayout, BetPrice, BetStakeDelta,BetPayoutDelta,BetType,BetSubType, 
				RewardBetTypeId, RewardBetTypeSource, 
				PriceTypeID,PriceTypeSource,LegacyBetId,NumberOfPermutations,PermutationStake,NumberOfLegs,
				LegId,LegIdSource,LegStake,LegStakeTrued,LegPayout,LegPayoutTrued,LegNumber,LegType,ClassId,ClassIdSource,EventID,EventIdSource,MarketID,MarketIdSource,LegCompetitorId,LegCompetitorIdSource,NumberOfPositions,
				PositionNumber,PositionStake,PositionPayout,PositionType,NumberOfGroupings, 
				c.GroupingNumber,GroupingStake,GroupingPayout,GroupingType,CompetitorId,CompetitorIdSource,Scratched,MappingId,
				InPlay, ClickToCall, CashoutType, IsDoubleDown, IsDoubledDown, IsChaseTheAce, FeatureFlags, isFirstBet, IsFirstAccrual) 
	SELECT	@BatchKey BatchKey,	c2.Action, c2.TransactionId, TransactionIdSource, UserId, UserIdSource, c2.MasterTransactionTimestamp, c2.FromDate,
			c2.BetGroupID, c2.BetGroupIDSource, c2.BetGroupType, c2.LedgerID, c2.LedgerSource, c2.Channel, c2.ActionChannel, c2.CampaignId, c2.CampaignIdSource, c2.FreeBetId, c2.FreeBetIdSource, c2.WalletId, c2.PromotionId, c2.PromotionIdSource, 
			c2.BetId, c2.BetIdSource, c2.BetStake, c2.BetPayout, c2.BetPrice, c2.BetStakeDelta, c2.BetPayoutDelta, c2.BetType, c2.BetSubType, 
			c2.RewardBetTypeId, c2.RewardBetTypeSource,
			c2.PriceTypeId, c2.PriceTypeSource, c2.LegacyBetId, c2.NumberOfPermutations, c2.PermutationStake, c2.NumberOfLegs, 
			c2.LegId, c2.LegIdSource, c2.LegStake, c2.LegStakeTrued, c2.LegPayout, c2.LegPayoutTrued, c2.LegNumber, c2.LegType, c2.ClassId, c2.ClassIdSource, c2.EventID, c2.EventIdSource, c2.MarketId, c2.MarketIdSource, c2.LegCompetitorId, c2.LegCompetitorIdSource, c2.NumberOfPositions,
			c2.PositionNumber,  c2.PositionStake, c2.PositionPayout, c2.PositionType, c2.NumberOfGroupings, 
			c2.GroupingNumber, c2.GroupingStake, c2.GroupingPayout, c2.GroupingType, c2.CompetitorId, c2.CompetitorIdSource, c2.Scratched, c2.MappingId, 
			c2.InPlay, c2.ClickToCall, c2.CashoutType, c2.IsDoubleDown, c2.IsDoubledDown, c2.IsChaseTheAce, c2.FeatureFlags, c2.isFirstBet, c2.isFirstAccrual
	FROM	#Atomic c2
	OPTION (RECOMPILE);

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

	DROP TABLE #Candidate

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
