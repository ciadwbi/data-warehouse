﻿CREATE PROCEDURE [Intrabet].[Sp_Withdrawal]
(
	@BatchKey	int,
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Clear','Start' -- Remove any remnants of previous runs in case of rerun

	DELETE Atomic.Withdrawal WHERE BatchKey = @BatchKey

	EXEC [Control].Sp_Log	@BatchKey,@Me,'From Transactions','Start', @RowsProcessed = @@ROWCOUNT -- Replaced/deleted versions of Transaction records - i.e. ones the change event is moving away FROM

	SELECT	t.*, ISNULL(TRY_CONVERT(int, t.Notes),0) RequestId, c.TransactionStatus, c.TransactionMethod, d.UserCode
	INTO	#From
	FROM	[$(Acquisition)].Intrabet.tblTransactions t
			INNER JOIN Reference.fn_Mapping_TransactionCode('Withdrawal') c ON c.TransactionCode = t.TransactionCode
			LEFT JOIN [$(Acquisition)].Intrabet.tblDeletedTransactions d ON d.TransactionId = t.TransactionId AND d.FromDate = t.ToDate AND d.ToDate > @FromDate -- the final ToDate condition on this line is not logically necessary, but it improves partition pruning in the query by giving an explicit lower bound for the partition column
	WHERE	t.ToDate > @FromDate AND t.ToDate <= @ToDate
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'To Transactions','Start', @RowsProcessed = @@ROWCOUNT -- New/updated versions of Transaction records - i.e. ones the change event is moving TO

	SELECT	t.*, ISNULL(TRY_CONVERT(int, Notes),0) RequestId, c.TransactionStatus, c.TransactionMethod, l.UserCode, NULLIF(l.TransactionDate,t.TransactionDate) LogTransactionDate
	INTO	#To
	FROM	[$(Acquisition)].Intrabet.tblTransactions t
			INNER JOIN Reference.fn_Mapping_TransactionCode('Withdrawal') c ON c.TransactionCode = t.TransactionCode
			LEFT JOIN [$(Acquisition)].Intrabet.tblTransactionLogs l ON l.TransactionId = t.TransactionId AND l.FromDate BETWEEN DATEADD(minute,-1, t.FromDate) AND DATEADD(minute,1, t.FromDate) AND l.ToDate > CONVERT(datetime2(3),'2013-08-26 00:01:40.523')
	WHERE	t.FromDate > @FromDate AND t.FromDate <= @ToDate AND t.ToDate > @FromDate -- the final ToDate condition on this line is not logically necessary, but it improves partition pruning in the query by giving an explicit lower bound for the partition column
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Latency','Start', @RowsProcessed = @@ROWCOUNT -- adjust historic from/to dates for any latency in the capture process when CDC was not on live database

	SELECT	*
	INTO	#Latency
	FROM	[$(Acquisition)].IntraBet.Latency
	WHERE	OriginalDate > @FromDate AND OriginalDate <= @ToDate
			AND OriginalDate <> CorrectedDate -- we are only interested in Latency records that change the From/To date - if there is no latency, don't need it as the latency value will only be applied selectively where it exists below
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Candidates','Start', @RowsProcessed = @@ROWCOUNT -- Pick the key data items for events occuring between From and To Transactions which are candidates for creating transaction records

	SELECT	x.TransactionId, x.RequestId, NULLIF(w.WithdrawID,-1) WithdrawId, 
			CASE 
				WHEN UserCode IS NOT NULL THEN COALESCE(x.LogTransactionDate, l.CorrectedDate, x.FromDate)
				ELSE COALESCE(x.LogTransactionDate, x.TransactionDate, l.CorrectedDate, x.FromDate)
			END MasterTimestamp,
			a.AccountId, x.ClientId, x.AccountNumber, x.Channel, x.UserCode, NULLIF(x.Notes,'') Notes,
			x.TransactionCode, x.TransactionMethod, x.FromTransactionStatus, x.ToTransactionStatus, 
			x.FromAmount, x.ToAmount,
			ISNULL(x.LogTransactionDate, x.TransactionDate) TransactionDate,
			ISNULL(l.CorrectedDate, x.FromDate) CaptureDate,
			x.FromDate
	INTO	#Candidate
	FROM	(	SELECT	ISNULL(t.FromDate, f.ToDate) FromDate, ISNULL(t.TransactionId, f.TransactionId) TransactionId, NULLIF(ISNULL(t.RequestId, f.RequestId),0) RequestId, 
						NULLIF(t.TransactionDate, f.TransactionDate) TransactionDate, t.LogTransactionDate, 
						ISNULL(t.ClientId, f.ClientId) ClientId, ISNULL(t.AccountNumber, f.AccountNumber) AccountNumber,
						ISNULL(t.Channel, f.Channel) Channel, ISNULL(t.UserCode, f.UserCode) UserCode, ISNULL(t.Notes, f.Notes) Notes,
						ISNULL(t.TransactionCode, f.TransactionCode) TransactionCode,
						COALESCE(t.TransactionMethod, f.TransactionMethod, m.TransactionMethod, 'Unknown') TransactionMethod,
						ISNULL(f.TransactionStatus, 'None') FromTransactionStatus, COALESCE(r.TransactionStatus, t.TransactionStatus, 'None') ToTransactionStatus, 
						ISNULL(-f.Amount,0) FromAmount, ISNULL(-t.Amount,0) ToAmount
				FROM	#To t
						FULL JOIN #From f ON f.TransactionID = t.TransactionID AND f.ToDate = t.FromDate
						-- Where reversal does not itself contain transaction method, obtain from other non-reversal transactions in same request
						LEFT JOIN #From m ON m.ClientID = t.ClientID AND m.ToDate = t.FromDate AND m.RequestId = t.RequestId AND m.TransactionID <> t.TransactionID AND m.TransactionStatus <> 'Reverse' AND t.TransactionStatus = 'Reverse'
						-- Check for Reversal in parallel with a Request, to identify Requests which implement the other half of a partial reversal (i.e. reverse entire original request and create new smaller request - both the actual reversal and the new smaller request are components of the partial reversal)
						LEFT JOIN #To r ON r.ClientID = t.ClientID AND r.FromDate = t.FromDate AND r.RequestId = t.RequestId AND r.TransactionID <> t.TransactionID AND r.TransactionStatus = 'Reverse' AND f.TransactionStatus IS NULL AND t.TransactionStatus = 'Request'
			) x
			left join [$(Acquisition)].Intrabet.tblClientWithdrawRequest w on x.RequestId IS NOT NULL AND w.RequestID = x.RequestId AND w.FromDate <= x.FromDate AND w.ToDate > x.FromDate AND w.ToDate > @Fromdate
			left join [$(Acquisition)].Intrabet.tblAccounts a on a.ClientId = x.ClientId AND a.AccountNumber = x.AccountNumber AND a.ToDate = CONVERT(datetime2(3),'9999-12-31') -- temporary cop-out pending completion of cache account table which will be small enough to allow efficient joining in date context rather than defaulting to latest version
			LEFT JOIN #Latency l ON l.OriginalDate = x.FromDate
	WHERE	(x.FromAmount <> 0 OR x.ToAmount <> 0 )
			AND (x.FromTransactionStatus <> x.ToTransactionStatus OR x.FromAmount <> x.ToAmount)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Email','Start', @RowsProcessed = @@ROWCOUNT -- The cashbook file includes client email addresses to help with matching in Bank Reconcilliation - feels like this should be added by the cashbook extract as circumstantial to the interface rather than here as a universal convention in the warehouse - need to revisit the whole generic external columns concept

	SELECT	FromDate, ClientId, MAX(Email) Email, COUNT(Distinct Email) NumberOfEmails
	INTO	#Email
	FROM	(
	SELECT	c.FromDate, c.ClientId, CASE x.x WHEN 1 THEN a.Email WHEN 2 THEN a.altEmail ELSE NULL END Email
	FROM	#Candidate c
			INNER JOIN [$(Acquisition)].IntraBet.tblClientAddresses a ON a.ClientID = c.ClientId AND a.FromDate <= c.FromDate AND a.ToDate > c.FromDate
			INNER JOIN (SELECT 1 x UNION ALL SELECT 2 x) x ON (x.x = 1 AND NULLIF(a.Email,'') IS NOT NULL) OR (x.x = 2 AND  NULLIF(a.altEmail,'') IS NOT NULL) -- Convert single row with 2 optional emails into one row per populated email 
			) x
	GROUP BY FromDate, ClientId
	HAVING COUNT(Distinct Email) = 1

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Transactions','Start', @RowsProcessed = @@ROWCOUNT -- Flesh out the Candidate Transaction events with all the required transaction information

	SELECT	x.TransactionId, 
			x.RequestId, x.WithdrawID, x.MasterTimestamp, x.AccountId, x.ClientId, x.AccountNumber, x.Channel, x.UserCode, x.Notes, x.TransactionCode, x.TransactionMethod, x.FromTransactionStatus, x.ToTransactionStatus, x.FromAmount, x.ToAmount, x.TransactionDate, x.CaptureDate, x.FromDate,
			bk.BankId, bk.AccountNumber BankAccountNumber, bk.BankBSB, bk.AccountName BankAccountName, bp.BPayId,	bp.RefNumber BPayRefNumber,	bp.BillerCode BPayBillerCode, e.Email, e.NumberOfEmails
	INTO	#Transaction
	FROM	#Candidate x
			LEFT JOIN [$(Acquisition)].Intrabet.tblClientBankdetails bk ON x.TransactionMethod = 'EFT' AND x.WithdrawId IS NOT NULL AND bk.BankID = x.WithdrawId AND bk.FromDate <= x.FromDate AND bk.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].Intrabet.tblClientBpaydetails bp ON x.TransactionMethod = 'BPay' AND x.WithdrawId IS NOT NULL AND bp.BPayID = x.WithdrawID AND bp.FromDate <= x.FromDate AND bp.ToDate > x.FromDate
			LEFT JOIN #Email e ON e.ClientId = x.ClientId AND e.FromDate = x.FromDate

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Atomic','Start', @RowsProcessed = @@ROWCOUNT -- Finally, map out any changes of status which actually require more than one withdrawal step to effect the change of status

	;WITH 
		Map	(			FromTransactionStatus,	ToTransactionStatus,	AmountChange,	TransactionStatus,	FromFactor,	ToFactor,	MappingId	)
		AS	(	SELECT	'None',					'Request',				+1,				'Request',			0,			+1,			30000		UNION ALL
				SELECT	'None',					'Request',				-1,				'Request',			0,			+1,			30001		UNION ALL
				SELECT	'None',					'Reverse',				+1,				'Reverse',			0,			+1,			30002		UNION ALL
				SELECT	'None',					'Reverse',				-1,				'Reverse',			0,			+1,			30003		UNION ALL
				SELECT	'None',					'Complete',				+1,				'Request',			0,			+1,			30004		UNION ALL
				SELECT	'None',					'Complete',				+1,				'Complete',			0,			+1,			30004		UNION ALL
				SELECT	'None',					'Complete',				-1,				'Reject',			0,			+1,			30005		UNION ALL
				SELECT	'Request',				'Complete',				0,				'Complete',			0,			+1,			30006		UNION ALL
				SELECT	'Complete',				'Request',				0,				'Complete',			-1,			0,			30007		UNION ALL
				SELECT	'Reverse',				'Request',				0,				NULL,				0,			0,			30008		UNION ALL
				SELECT	'Request',				'None',					+1,				'Request',			-1,			0,			30009		UNION ALL
				SELECT	'Request',				'None',					-1,				'Decline',			-1,			0,			30010		UNION ALL
				SELECT	'Reverse',				'None',					+1,				'Reverse',			-1,			0,			30011		UNION ALL
				SELECT	'Reverse',				'None',					-1,				'Reverse',			-1,			0,			30012		UNION ALL
				SELECT	'Complete',				'None',					+1,				'Reject',			-1,			0,			30013		UNION ALL
				SELECT	'Complete',				'None',					-1,				'Reject',			-1,			0,			30014		
			)
	INSERT	Atomic.Withdrawal
		(	BatchKey, TransactionId, TransactionSource, GroupId, GroupSource, RequestId, WithdrawId, ExternalId, External1, External2, MasterTimestamp, TransactionCode,
			LedgerId, LedgerSource, UserId, UserSource, InstrumentID, InstrumentSource, ChannelId, ChannelSource, NoteId, NoteSource, Amount, 
			MappingId, SourceTransactionCode, SourceTransactionDate, CaptureDate, FromDate
		)
	SELECT	@BatchKey BatchKey,
			x.TransactionId,
			'IBT' TransactionSource,
			ISNULL(x.RequestId, x.TransactionId) GroupId,
			CASE WHEN x.RequestId IS NULL THEN 'IBT' ELSE 'ITR' END GroupSource,
			x.RequestId,
			x.WithdrawId,
			ISNULL(	CASE x.TransactionMethod
						WHEN 'BPay' THEN Left(x.BPayRefNumber,6)+'-'+Right(x.BPayRefNumber,4) 
						WHEN 'EFT' THEN NULLIF(x.BankAccountNumber,'')
						WHEN 'Moneybookers' THEN x.Email 
						WHEN 'Paypal' THEN x.Email 
						ELSE 'N/A' 
					END, 
					'Unknown') ExternalId, -- as per above observation on validity of these generic column derivations inside the warehouse - the values they are based on are available in the Instrument dimension and then perform this logic when extracted for the cashbook file
			ISNULL(	CASE x.TransactionMethod
						WHEN 'BPay' THEN CONVERT(varchar,x.BPayID) 
						WHEN 'EFT' THEN NULLIF(x.BankBSB,'')
						WHEN 'Moneybookers' THEN CONVERT(varchar,ISNULL(x.NumberOfEmails,0)) 
						WHEN 'Paypal' THEN CONVERT(varchar,ISNULL(x.NumberOfEmails,0))  
						ELSE 'N/A' 
					END, 
					'Unknown') External1, -- as per above observation on validity of these generic column derivations inside the warehouse - the values they are based on are available in the Instrument dimension and then perform this logic when extracted for the cashbook file
			ISNULL(	CASE x.TransactionMethod
						WHEN 'BPay' THEN x.BPayBillerCode 
						WHEN 'EFT' THEN NULLIF(REPLACE(REPLACE(x.BankAccountName,', ',' '),',',' '),'')
						ELSE 'N/A'
					END, 
					'Unknown') External2, -- as per above observation on validity of these generic column derivations inside the warehouse - the values they are based on are available in the Instrument dimension and then perform this logic when extracted for the cashbook file
			x.MasterTimestamp,
			'Withdrawal' + '.' + m.TransactionStatus + '.' + x.TransactionMethod TransactionCode,
			ISNULL(x.AccountId, x.ClientId) LedgerId,
			CASE WHEN x.AccountId IS NULL THEN 'ICC' ELSE 'IAA' END LedgerSource,
			ISNULL(CONVERT(varchar,x.UserCode),'N/A') UserId,
			CASE WHEN UserCode IS NULL THEN 'N/A' ELSE 'IUU' END UserSource,
			ISNULL(	CASE x.TransactionMethod
						WHEN 'BPay' THEN CONVERT(varchar(50), x.BPayID)
						WHEN 'EFT' THEN CONVERT(varchar(50), x.BankID)
						ELSE  CONVERT(varchar(50),0)
					END,  
					CONVERT(varchar(50),-1)) AS InstrumentID,
			CASE
				WHEN x.TransactionMethod = 'BPay' AND x.BPayID IS NOT NULL THEN 'BPY'
				WHEN x.TransactionMethod = 'BPay' THEN 'UNK'
				WHEN x.TransactionMethod = 'EFT' AND x.BankID IS NOT NULL THEN 'EFT'
				WHEN x.TransactionMethod = 'EFT' THEN 'UNK'
				ELSE 'N/A'
			END InstrumentSource,
			ISNULL(x.Channel, -1)  ChannelId,
			CASE WHEN Channel IS NULL THEN 'UNK' ELSE 'ICC' END ChannelSource,
			CASE WHEN x.RequestId IS NULL AND x.Notes IS NOT NULL THEN x.TransactionId ELSE 0 END NoteId,
			CASE WHEN x.RequestId IS NULL AND x.Notes IS NOT NULL THEN 'ITN' ELSE 'N/A' END NoteSource,
			(x.FromAmount * m.FromFactor) + (x.ToAmount * m.ToFactor) Amount,
			m.MappingId,
			TransactionCode SourceTransactionCode,
			x.TransactionDate SourceTransactionDate,
			x.CaptureDate,
			x.FromDate
	FROM	#Transaction x
			LEFT JOIN Map m ON m.FromTransactionStatus = x.FromTransactionStatus AND m.ToTransactionStatus = x.ToTransactionStatus AND m.AmountChange = SIGN(x.ToAmount - x.FromAmount)
	WHERE	m.TransactionStatus IS NOT NULL

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
