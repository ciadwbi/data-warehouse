﻿CREATE PROC [Intrabet].[Sp_Competitor]
(
	@BatchKey	INT,
	@FromDate	DATETIME2(3),
	@ToDate		DATETIME2(3)
)
WITH RECOMPILE
AS
BEGIN TRY

		SET TRANSACTION ISOLATION LEVEL SNAPSHOT;
		BEGIN TRANSACTION IB_Competitor ;
		
		--------------------- INSERT INTO STAGING ----------------------------------------

		EXEC [Control].Sp_Log	@BatchKey,'Sp_Competitor','InsertIntoTempTable','Start'

		 IF OBJECT_ID('TempDB..#CompetitorStage','U') IS NOT NULL BEGIN DROP TABLE #CompetitorStage END;

		 SELECT
		  @BatchKey  BatchKey, Concat([EventID],'-',[CompetitorID]) CompetitorID,[SaddleNumber],[CompetitorName],[Scratched],[Jockey],[Draw],[Form],	[History],[Weight],	[Trainer] ,[ManualIntPrice],[bestTime] ,[scratchedLate],
		  [rulesOverrideState],	[ManualIntPriceCB],	[TabRollCount],	[IsKnockedOut],	[EVENT_SK],	[ManualIntPriceTW],	FromDate,
		  ISNULL(ROW_NUMBER() OVER(PARTITION BY IsNull(EventID,-1), IsNull(CompetitorID,-1) ORDER BY CONVERT(DATETIME2(3),ISNULL(FromDate,'1900-01-01') ) DESC),-1)  I_RNK
		  
		 into #CompetitorStage
		 from [$(Acquisition)].IntraBet.tblCompetitors
		 where FromDate>=@FromDate and FromDate<@ToDate
		 OPTION (RECOMPILE);

		EXEC [Control].Sp_Log	@BatchKey,'Sp_Competitor','InsertIntoStaging','Start'

		INSERT INTO Stage.Competitor WITH (TABLOCK)
		(BatchKey,[CompetitorID],[CompetitorSource],[SaddleNumber],[CompetitorName],[Scratched],[Jockey],[Draw],[Form],	[History],[Weight],	[Trainer] ,[ManualIntPrice],[bestTime] ,[scratchedLate],
		[rulesOverrideState],	[ManualIntPriceCB],	[TabRollCount],	[IsKnockedOut],	[EVENT_SK],	[ManualIntPriceTW],	[Fromdate]  )
		SELECT BatchKey,
		[CompetitorID],'ICC' [CompetitorSource],[SaddleNumber],[CompetitorName],[Scratched],[Jockey],[Draw],	[Form],	[History],[Weight],	[Trainer] ,[ManualIntPrice],[bestTime] ,[scratchedLate],
		[rulesOverrideState],	[ManualIntPriceCB],	[TabRollCount],	[IsKnockedOut],	[EVENT_SK],	[ManualIntPriceTW],	[Fromdate]
		FROM #CompetitorStage
		WHERE I_RNK = 1  -- Filter out duplicates 

		COMMIT TRANSACTION IB_Competitor ;
		EXEC [Control].Sp_Log @BatchKey, 'Sp_Competitor', NULL, 'Success'
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION IB_Competitor ;
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Competitor', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH