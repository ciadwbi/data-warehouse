﻿CREATE PROCEDURE [Intrabet].[SP_User] 
(	@FromDate datetime, 
	@ToDate DateTime,
	@BatchKey int
)
WITH RECOMPILE
AS 
BEGIN
BEGIN TRY

SET NOCOUNT ON;

	EXEC [Control].Sp_Log	@BatchKey,'Sp_User','InsertIntoStaging','Start'

	BEGIN TRANSACTION Tx_User;

	INSERT INTO Stage.[User] (BatchKey,UserId,Source,SystemName,Forename,Surname,PayrollNumber,FromDate)
	SELECT  
		@BatchKey,
		U1.usercode as userID,
		'IUU' as Source,
		'Intrabet' as systemName,
		Cleanse.CamelCase(U1.FirstName) as Forename,
		Cleanse.CamelCase(U1.Surname),
		0 as PayrollNumber,
		U1.FromDate
	FROM [$(Acquisition)].IntraBet.tblUsers U1 WITH (NOLOCK) 
	LEFT OUTER JOIN  [$(Acquisition)].IntraBet.tblUsers U2 WITH (NOLOCK) ON (U2.UserCode=U1.UserCode AND U1.ToDate=U2.FromDate and U2.FromDate>=@FromDate and U2.FromDate<@ToDate )
	WHERE (U1.FromDate>=@FromDate and U1.FromDate<@ToDate)
	AND U2.UserCode IS NULL
	OPTION (RECOMPILE);
		
	COMMIT TRANSACTION Tx_User;

	EXEC [Control].Sp_Log @BatchKey, 'Sp_User', NULL, 'Success'

END TRY
BEGIN CATCH
	
	ROLLBACK TRANSACTION Tx_User;
		
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_User', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END
