﻿CREATE PROC [Intrabet].[Sp_KPI]
(
	@BatchKey		int,
	@FromDate		datetime2(3),
	@ToDate			datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS 
BEGIN

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	SET NOCOUNT ON;

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT
 
	DECLARE @Trued char(1) = 'Y'
	DECLARE @RowCount INT;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, @Me, 'AccountOpened', 'Start';

	INSERT INTO Stage.KPI 
		(	BatchKey, LegId, LegIdSource, LedgerId, LedgerSource, MasterTransactionTimestamp, MasterDayText, AnalysisDayText,
			BetTypeName, LegBetTypeName, BetSubTypeName, BetGroupType, GroupingType, Channel, ActionChannel, CampaignId, WalletId, LegNumber, NumberOfLegs, 
			ClassId, ClassIdSource, UserID, UserIDSource, EventID, EventIDSource, InstrumentID, InstrumentIDSource, ClickToCall, InPlay, CashoutType, IsDoubleDown, IsDoubledDown, IsChaseTheAce, FeatureFlags,
			DepositsRequested, DepositsCompleted, DepositsRequestedCount, DepositsCompletedCount, Promotions,
			BetsPlaced, BonusBetsPlaced, ContractsPlaced, BonusContractsPlaced, BettorsPlaced, BonusBettorsPlaced, 
			PlayDaysPlaced, BonusPlayDaysPlaced, PlayFiscalWeeksPlaced, BonusPlayFiscalWeeksPlaced, PlayCalenderWeeksPlaced, BonusPlayCalenderWeeksPlaced, 
			PlayFiscalMonthsPlaced, BonusPlayFiscalMonthsPlaced, PlayCalenderMonthsPlaced, BonusPlayCalenderMonthsPlaced, 
			Stakes, BonusStakes, Winnings, BettorsCashedOut, BetsCashedOut, Cashout, CashoutDifferential, Fees, 
			WithdrawalsRequested, WithdrawalsCompleted, WithdrawalsRequestedCount, WithdrawalsCompletedCount, Adjustments, Transfers, 
			BetsResulted, BonusBetsResulted, ContractsResulted, BonusContractsResulted, BettorsResulted, BonusBettorsResulted, 
			PlayDaysResulted, BonusPlayDaysResulted, PlayFiscalWeeksResulted, BonusPlayFiscalWeeksResulted, PlayCalenderWeeksResulted, BonusPlayCalenderWeeksResulted,
			PlayFiscalMonthsResulted, BonusPlayFiscalMonthsResulted, PlayCalenderMonthsResulted, BonusPlayCalenderMonthsResulted,
			Turnover, BonusTurnover, GrossWin, NetRevenue, NetRevenueAdjustment, BonusWinnings, Betback, GrossWinAdjustment,
			FirstBet, FirstDeposit, AccountOpened, CreatedDate)
	SELECT	@BatchKey BatchKey,
			-1 LegID,
			'N/A' LegIdSource,
			LedgerID,
			'IAA' LedgerSource,
			a.AccountOpenedDate MasterTransactionTimestamp,
			CONVERT(varchar(11),CONVERT(DATE, a.AccountOpenedDate)) MasterDayText,
			CONVERT(varchar(11),CONVERT(DATE, DATEADD(minute, + ISNULL(y.OffsetMinutes,0), a.AccountOpenedDate))) AnalysisDayText,
			'N/A' BetTypeName,
			'N/A' LegBetTypeName,
			'N/A' BetSubTypeName,
			'N/A' BetGroupType,
			'N/A' GroupingType,
			0 Channel,
			0 ActionChannel,
			0 CampaignId,
			'C' WalletId,
			0 LegNumber,
			0 NumberOfLegs,
			0 ClassId,
			'IBT' ClassIdSource,
			'0' UserId,
			'N/A' UserIdSource,
			0 EventId,
			'N/A' EventIdSource,
			'0' InstrumentId,
			'N/A' InstrumentIdSource,
			'N' ClickToCall,
			'N' InPlay, 
			0 CashoutType,
			0 IsDoubleDown, 
			0 IsDoubledDown,
			0 IsChaseThaAce,
			0 FeatureFlags,
			0 DepositsRequested,
			0 DepositsCompleted,
			NULL DepositsRequestedCount,
			NULL DepositsCompletedCount,
			0 Promotions,
			NULL BetsPlaced,
			NULL BonusBetsPlaced,
			NULL ContractsPlaced,
			NULL BonusContractsPlaced,
			NULL BettorsPlaced,
			NULL BonusBettorsPlaced,
			NULL PlayDaysPlaced,
			NULL BonusPlayDaysPlaced,
			NULL PlayFiscalWeeksPlaced,
			NULL BonusPlayFiscalWeeksPlaced,
			NULL PlayCalenderWeeksPlaced,
			NULL BonusPlayCalenderWeeksPlaced,
			NULL PlayFiscalMonthsPlaced,
			NULL BonusPlayFiscalMonthsPlaced,
			NULL PlayCalenderMonthsPlaced,
			NULL BonusPlayCalenderMonthsPlaced,
			0 Stakes,
			0 BonusStakes,
			0 Winnings,
			NULL BettorsCashedOut,
			NULL BetsCashedOut,
			0 Cashout,
			0 CashoutDifferential,
			0 Fees,
			0 WithdrawalsRequested,
			0 WithdrawalsCompleted,
			NULL WithdrawalsRequestedCount,
			NULL WithdrawalsCompletedCount,
			0 Adjustments,
			0 Transfers,
			NULL BetsSettled,
			NULL BonusBetsSettled,
			NULL ContractsSettled ,
			NULL BonusContractsSettled,
			NULL BettorsSettled,
			NULL BonusBettorsSettled,
			NULL PlayDaysResulted,
			NULL BonusPlayDaysResulted,
			NULL PlayFiscalWeeksResulted,
			NULL BonusPlayFiscalWeeksResulted,
			NULL PlayCalenderWeeksResulted,
			NULL BonusPlayCalenderWeeksResulted,
			NULL PlayFiscalMonthsResulted,
			NULL BonusPlayFiscalMonthsResulted,
			NULL PlayCalenderMonthsResulted,
			NULL BonusPlayCalenderMonthsResulted,
			0 Turnover,
			0 BonusTurnover,
			0 GrossWin,
			0 NetRevenue,
			0 NetRevenueAdjustment,
			0 BonusWinnings,
			0 Betback,
			0 GrossWinAdjustment,
			NULL FirstBet,
			NULL FirstDeposit,
			LedgerID as AccountOpened,
			CONVERT(datetime2,GETDATE()) CreatedDate
	FROM	(	SELECT  a.AccountID LedgerId, a.FromDate, COALESCE(c.StartDate, c.Fromdate, a.FromDate) AccountOpenedDate --Monica is almost happy!!
				FROM	[$(Acquisition)].Intrabet.tblAccounts a
						LEFT JOIN [$(Acquisition)].Intrabet.tblAccounts a1 ON a1.AccountID = a.AccountID and a1.ToDate = a.Fromdate
						LEFT JOIN [$(Acquisition)].Intrabet.tblclientinfo c ON c.ClientID = a.ClientID and c.ToDate = CONVERT(datetime2(3),'9999-12-31') --Dodgy...Richard...
				WHERE	a.FromDate > @FromDate 
						AND a.FromDate <= @ToDate
						AND a1.AccountID IS NULL
						AND a.AccountNumber = 1
						-- Exclude new clients created as a half-arsed cludge to deal with negative balances at the end of credit betting by migrating the balances to new duplicate clients - do not want to count such accounts as new registrations
						AND a.ClientId NOT IN (SELECT ClientID FROM [$(Acquisition)].IntraBet.tblClients WHERE Username LIKE '%\_Credit' ESCAPE '\' and ToDate = CONVERT(datetime2(3),'9999-12-31'))
			) a
			-- This join should not be!! Required because KPI staging table does not keep the same correct dayzone natural keys as transaction
			LEFT JOIN Reference.TimeZone y ON y.Fromdate <= a.AccountOpenedDate AND y.ToDate > a.AccountOpenedDate AND y.TimeZone = 'Analysis'
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

--	Below logic migrated to Fact.sp_KPI as part of strategy to remove staging and update facts direct from Atomic	

	--EXEC [Control].Sp_Log @BatchKey, @Me, 'Cashout', 'Start', @RowsProcessed = @RowCount

	--SET @Trued = CASE Control.ParameterValue('Bet','TruedEqualShare') WHEN 'No' THEN 'N' ELSE 'Y' END

	--INSERT INTO Stage.KPI 
	--	(	BatchKey, LegId, LegIdSource, LedgerId, LedgerSource, MasterTransactionTimestamp, MasterDayText, AnalysisDayText,
	--		BetTypeName, LegBetTypeName, BetSubTypeName, BetGroupType, GroupingType, Channel, ActionChannel, CampaignId, WalletId, LegNumber, NumberOfLegs, 
	--		ClassId, ClassIdSource, UserID, UserIDSource, EventID, EventIDSource, InstrumentID, InstrumentIDSource, ClickToCall, InPlay, CashoutType, IsDoubleDown, IsDoubledDown, IsChaseTheAce, FeatureFlags,
	--		DepositsRequested, DepositsCompleted, DepositsRequestedCount, DepositsCompletedCount, Promotions,
	--		BetsPlaced, BonusBetsPlaced, ContractsPlaced, BonusContractsPlaced, BettorsPlaced, BonusBettorsPlaced, 
	--		PlayDaysPlaced, BonusPlayDaysPlaced, PlayFiscalWeeksPlaced, BonusPlayFiscalWeeksPlaced, PlayCalenderWeeksPlaced, BonusPlayCalenderWeeksPlaced, 
	--		PlayFiscalMonthsPlaced, BonusPlayFiscalMonthsPlaced, PlayCalenderMonthsPlaced, BonusPlayCalenderMonthsPlaced, 
	--		Stakes, BonusStakes, Winnings, BettorsCashedOut, BetsCashedOut, Cashout, CashoutDifferential, Fees,
	--		WithdrawalsRequested, WithdrawalsCompleted, WithdrawalsRequestedCount, WithdrawalsCompletedCount, Adjustments, Transfers, 
	--		BetsResulted, BonusBetsResulted, ContractsResulted, BonusContractsResulted, BettorsResulted, BonusBettorsResulted, 
	--		PlayDaysResulted, BonusPlayDaysResulted, PlayFiscalWeeksResulted, BonusPlayFiscalWeeksResulted, PlayCalenderWeeksResulted, BonusPlayCalenderWeeksResulted,
	--		PlayFiscalMonthsResulted, BonusPlayFiscalMonthsResulted, PlayCalenderMonthsResulted, BonusPlayCalenderMonthsResulted,
	--		Turnover, BonusTurnover, GrossWin, NetRevenue, NetRevenueAdjustment, BonusWinnings, Betback, GrossWinAdjustment,
	--		FirstBet, FirstDeposit, AccountOpened, CreatedDate)
	--SELECT	BatchKey, LegId, LegIdSource, LedgerId, LedgerSource, MasterTransactionTimestamp, 			
	--		CONVERT(varchar(11),CONVERT(DATE, a.MasterTransactionTimestamp)) MasterDayText,
	--		CONVERT(varchar(11),CONVERT(DATE, DATEADD(minute, + ISNULL(y.OffsetMinutes,0), a.MasterTransactionTimestamp))) AnalysisDayText,
	--		BetType BetTypeName, LegType LegBetTypeName, BetSubType BetSubTypeName, BetGroupType, GroupingType, Channel, ActionChannel, CampaignId, WalletId, LegNumber, NumberOfLegs, 
	--		ClassId, ClassIdSource, UserID, UserIDSource, EventID, EventIDSource, 0 InstrumentID, 'N/A' InstrumentIDSource, ClickToCall, InPlay, CashoutType, IsDoubleDown, IsDoubledDown, IsChaseTheAce, FeatureFlags,
	--		0 DepositsRequested, 0 DepositsCompleted, 0 DepositsRequestedCount, 0 DepositsCompletedCount, 0 Promotions,
	--		NULL BetsPlaced, NULL BonusBetsPlaced, NULL ContractsPlaced, NULL BonusContractsPlaced, NULL BettorsPlaced, NULL BonusBettorsPlaced, 
	--		NULL PlayDaysPlaced, NULL BonusPlayDaysPlaced, NULL PlayFiscalWeeksPlaced, NULL BonusPlayFiscalWeeksPlaced, NULL PlayCalenderWeeksPlaced, NULL BonusPlayCalenderWeeksPlaced, 
	--		NULL PlayFiscalMonthsPlaced, NULL BonusPlayFiscalMonthsPlaced, NULL PlayCalenderMonthsPlaced, NULL BonusPlayCalenderMonthsPlaced, 
	--		0 Stakes, 0 BonusStakes, 0 Winnings, NULL BettorsCashedOut, NULL BetsCashedOut, 0 Cashout, 
	--		CASE WHEN a.BetType = 'Exotic' THEN a.BetPayout WHEN @Trued = 'Y' THEN a.LegPayoutTrued ELSE a.LegPayout END CashoutDifferential, 0 Fees,
	--		0 WithdrawalsRequested, 0 WithdrawalsCompleted, 0 WithdrawalsRequestedCount, 0 WithdrawalsCompletedCount, 0 Adjustments, 0 Transfers, 
	--		NULL BetsResulted, NULL BonusBetsResulted, NULL ContractsResulted, NULL BonusContractsResulted, NULL BettorsResulted, NULL BonusBettorsResulted, 
	--		NULL PlayDaysResulted, NULL BonusPlayDaysResulted, NULL PlayFiscalWeeksResulted, NULL BonusPlayFiscalWeeksResulted, NULL PlayCalenderWeeksResulted, NULL BonusPlayCalenderWeeksResulted,
	--		NULL PlayFiscalMonthsResulted, NULL BonusPlayFiscalMonthsResulted, NULL PlayCalenderMonthsResulted, NULL BonusPlayCalenderMonthsResulted,
	--		0 Turnover, 0 BonusTurnover, 0 GrossWin, 0 NetRevenue, 0 NetRevenueAdjustment, 0 BonusWinnings, 0 Betback, 0 GrossWinAdjustment,
	--		NULL FirstBet, NULL FirstDeposit, NULL AccountOpened, CONVERT(datetime2,GETDATE()) CreatedDate
	--FROM	Atomic.Bet a 
	--		-- This join should not be!! Required because KPI staging table does not keep the same correct dayzone natural keys as transaction
	--		LEFT JOIN Reference.TimeZone y ON y.Fromdate <= a.MasterTransactionTimestamp AND y.ToDate > a.MasterTransactionTimestamp AND y.TimeZone = 'Analysis'
	--WHERE	BatchKey = @BatchKey
	--		AND Action = 'CashoutDif'
	--		AND PositionNumber = 1 AND GroupingNumber = 1	-- As the Transaction fact is staged at a bet leg grain, just fix the selection to the first position and group for each leg and retrieve leg level stakes and payments (seeabove)
	--		AND (BetType <> 'Exotic' OR BetId = LegId)	-- Transactions only include one line per exotics, even if the exotic is technically multi-leg - e.g. Quadrella - so only take the first leg of exotics and retrieve bet level stakes and payments (above)
	--OPTION (RECOMPILE)

	--SET @RowCount = @@ROWCOUNT
	--SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
END
