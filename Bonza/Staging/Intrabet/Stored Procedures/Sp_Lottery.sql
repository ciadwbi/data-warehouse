﻿CREATE PROCEDURE [Intrabet].[Sp_Lottery]
(
	@BatchKey		int,
	@FromDate		datetime2(3),
	@ToDate			datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @RowCount int

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Clear','Start' -- Remove any remnants of previous runs in case of rerun

	DELETE Atomic.Lottery WHERE BatchKey = @BatchKey

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Transactions','Start', @RowsProcessed = @@ROWCOUNT

	SELECT	TransactionID,
			TransactionDate,
			TransactionCode,
			LinkTransactionId,
			ClientID,
			AccountNumber,
			Product,
			DrawDate,
			Amount,
			Stake,
			ExternalId,
			FromDate
	INTO	#Lottery
	FROM	Cache.TransactionLottery
	WHERE	FromDate > @FromDate 
			AND FromDate <= @ToDate
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Atomic','Start', @RowsProcessed = @@ROWCOUNT

	INSERT	Atomic.Lottery
		(	BatchKey, TransactionId, TransactionSource, MasterTransactionTimestamp, TransactionCode, TicketId, TicketSource, LedgerId, LedgerSource, Product, DrawDate, Stake, Payout, StakeDelta, PayoutDelta, ExternalId, MappingId, FromDate)
	SELECT	@BatchKey, 
			TransactionId, 
			'LTI' TransactionSource, 
			TransactionDate MasterTransactionTimestamp,
			CASE TransactionCode WHEN -1005 THEN 'Lottery.Place' WHEN 1006 THEN 'Lottery.Cancel' WHEN 1005 THEN CASE Amount WHEN 0.00 THEN 'Lottery.Loss' ELSE 'Lottery.Win' END ELSE 'Lottery.Unknown' END TransactionCode,
			ISNULL(LinkTransactionId, TransactionId) TicketId,
			'LTI' TicketSource,
			a.AccountID LedgerId,
			'IAA' LedgerSource,
			Product,
			DrawDate,
			Stake,
			CASE TransactionCode WHEN 1005 THEN Amount ELSE 0.00 END Payout,
			CASE TransactionCode WHEN -1005 THEN - Amount WHEN 1006 THEN - Amount WHEN 1005 THEN - Stake ELSE 0.00 END StakeDelta,
			CASE TransactionCode WHEN 1005 THEN Amount ELSE 0.00 END PayoutDelta,
			ExternalId,
			40000 MappingId,
			l.FromDate
	FROM	#Lottery l
			INNER JOIN [$(Acquisition)].Intrabet.tblAccounts a ON a.ClientID = l.ClientID AND a.AccountNumber = l.AccountNumber AND a.FromDate <= l.TransactionDate AND a.ToDate > l.TransactionDate AND a.ToDate > @FromDate
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

	Update [Control].[ConfigurationItems] Set Value = 'N' Where ConfigurationItem = @Me

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO


