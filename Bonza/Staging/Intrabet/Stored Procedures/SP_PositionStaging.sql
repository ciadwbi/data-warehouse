﻿CREATE PROC [Intrabet].[Sp_PositionStaging]
(
	@BatchKey	int,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,'Sp_PositionStaging','Insert Into Staging','Start'

	SELECT	BetType, SUM(WinPlace) WinPlace, BetTypeName, BetSubTypeName, ISNULL(LegTypeName,'Each Way') LegTypeName, GroupingName, ProductName 
	INTO	#BetTypeMapping
	FROM	[Intrabet].[BetTypeMapping]
	WHERE	LegTypeName <> 'Unknown'
	GROUP BY BetType, BetTypeName, BetSubTypeName, GroupingName, ProductName, LegTypeName WITH ROLLUP
	HAVING	GROUPING(ProductName) = 0
			AND (GROUPING(LegTypeName) = 0 OR SUM(WinPlace) > MAX(WinPlace))
	OPTION (RECOMPILE)

	--------------------------------------------------------------------

	INSERT INTO [Stage].[Position]
		(	BatchKey, LedgerID, LedgerSource, BalanceTypeID,
			MasterDayText, AnalysisDayText, OpeningBalance, TransactedAmount, ClosingBalance,
			FirstCreditDate, LastCreditDate, FirstDebitDate, LastDebitDate,
			FirstBetId, FirstBetDate, FirstBetType, FirstBetSubType, FirstGroupingType, FirstBetGroupType, FirstLegType, FirstChannel, FirstEventId, FirstClassId, 
			LastBetId, LastBetDate, LastBetType, LastBetSubType, LastGroupingType, LastBetGroupType, LastLegType, LastChannel, LastEventId, LastClassId,
			HasDeposited, HasWithdrawn, HasBet, HasWon, HasLost, 
			LifetimeTurnover, LifetimeGrossWin, LifetimeBetCount, LifetimeBonus, LifetimeDeposits, LifetimeWithdrawals, 
			DayDate, FromDate, FirstCreditId, LastCreditId,
			TierNumber, TierNumberFiscal, Turnover, TurnoverCalenderMonthToDate, TurnoverFiscalMonthToDate,
			DaysSinceAccountOpened, DaysSinceFirstDeposit, DaysSinceFirstBet, DaysSinceLastDeposit, DaysSinceLastBet, RevenueLifeCycle, LifeStage, PreviousLifeStage
		)
	SELECT	BatchKey, LedgerID, 'IAA' LedgerSource, 'CLI' BalanceTypeID,
			CASE y.DayType WHEN 'Master' THEN CONVERT(VARCHAR,DayDate) ELSE 'N/A' END MasterDayText, 
			CASE y.DayType WHEN 'Analysis' THEN CONVERT(VARCHAR,DayDate) ELSE 'N/A' END AnalysisDayText, 
			ClientBalance OpeningBalance, CONVERT(MONEY,0) TransactedAmount, ClientBalance ClosingBalance,
			FirstCreditDate, LastCreditDate, FirstDebitDate, LastDebitDate,
			FirstBetId, FirstBetDate, FirstBetType, FirstBetSubType, FirstGroupingType, FirstBetGroupType, FirstLegType, FirstChannel, FirstEventId, FirstClassId, 
			LastBetId, LastBetDate, LastBetType, LastBetSubType, LastGroupingType, LastBetGroupType, LastLegType, LastChannel, LastEventId, LastClassId,
			'No' HasDeposited, 'No' HasWithdrawn, 'No' HasBet, 'No' HasWon, 'No' HasLost, 
			LifetimeTurnover, LifetimeGrossWin, LifetimeBetCount, LifetimeBonus, LifetimeDeposits, LifetimeWithdrawals, 
			DayDate, FromDate, FirstCreditId, LastCreditId,
			NULL TierNumber, NULL TierNumberFiscal, NULL Turnover, NULL TurnoverCalenderMonthToDate, NULL TurnoverFiscalMonthToDate,
			NULL DaysSinceAccountOpened, NULL DaysSinceFirstDeposit, NULL DaysSinceFirstBet, NULL DaysSinceLastDeposit, NULL DaysSinceLastBet,
			'N/A' RevenueLifeCycle, 'Unknown' LifeStage, 'Unknown' PreviousLifeStage
	FROM	(	SELECT	a.BatchKey,
						a.LedgerID,
						a.DayDate,
						a.ClientBalance,
						a.FirstCreditDate,
						a.LastCreditDate,
						a.FirstDebitDate,
						a.LastDebitDate,
						CASE WHEN b.FirstBetDate < a.FirstBetDate or a.FirstBetDate is null THEN b.FirstBetId ELSE a.FirstBetId END FirstBetId,
						CASE WHEN b.FirstBetDate < a.FirstBetDate or a.FirstBetDate is null THEN b.FirstBetDate ELSE a.FirstBetDate END FirstBetDate,
						fb.BetTypeName as FirstBetType,
						fb.BetSubTypeName as FirstBetSubType,
						fb.GroupingName as FirstGroupingType,
						CASE WHEN b.FirstBetDate < a.FirstBetDate or a.FirstBetDate is null THEN b.FirstBetGroupType ELSE a.FirstBetGroupType END FirstBetGroupType,
						fb.LegTypeName as FirstLegType,
						CASE WHEN b.FirstBetDate < a.FirstBetDate or a.FirstBetDate is null THEN b.FirstChannel ELSE a.FirstChannel END FirstChannel,
						CASE WHEN b.FirstBetDate < a.FirstBetDate or a.FirstBetDate is null THEN b.FirstEventId ELSE a.FirstEventId END FirstEventId,
						CASE WHEN b.FirstBetDate < a.FirstBetDate or a.FirstBetDate is null THEN b.FirstClassId ELSE a.FirstClassId END FirstClassId,
						CASE WHEN b.LastBetDate > a.LastBetDate THEN b.LastBetId ELSE a.LastBetId END LastBetId,
						CASE WHEN b.LastBetDate > a.LastBetDate THEN b.LastBetDate ELSE a.LastBetDate END LastBetDate,
						lb.BetTypeName as LastBetType,
						lb.BetSubTypeName as LastBetSubType,
						lb.GroupingName as LastGroupingType,
						CASE WHEN b.LastBetDate > a.LastBetDate or a.LastBetDate is null THEN b.LastBetGroupType ELSE a.LastBetGroupType END LastBetGroupType,
						lb.LegTypeName as LastLegType,
						CASE WHEN b.LastBetDate > a.LastBetDate or a.LastBetDate is null THEN b.LastChannel ELSE a.LastChannel END LastChannel,
						CASE WHEN b.LastBetDate > a.LastBetDate or a.LastBetDate is null THEN b.LastEventId ELSE a.LastEventId END LastEventId,
						CASE WHEN b.LastBetDate > a.LastBetDate or a.LastBetDate is null THEN b.LastClassId ELSE a.LastClassId END LastClassId,
						IsNull(a.LifetimeStake,0) - IsNull(a.UnsettledBalance,0) as LifetimeTurnover,
						IsNull(a.LifetimeStake,0) - IsNull(a.UnsettledBalance,0) - IsNull(a.LifetimePayout,0) as LifetimeGrossWin,
						IsNull(a.LifetimebetsPlaced,0) as LifetimeBetCount,
						IsNull(b.LifetimeCredit,0) as LifetimeBonus,
						a.LifetimeCredit as LifetimeDeposits,
						a.LifetimeDebit as LifetimeWithdrawals,
						a.FromDate,
						a.FirstCreditId,
						a.LastCreditId
				FROM	Atomic.OpeningBalance a
						LEFT JOIN Atomic.OpeningBalance b ON (b.LedgerId = a.LedgerId AND b.DayDate = a.DayDate AND b.BatchKey = a.BatchKey AND b.FreeBet = 1)-- AND b.FirstBetId IS NOT NULL)
						LEFT OUTER JOIN #BetTypeMapping fb on (fb.BetType = CASE WHEN b.FirstBetDate < a.FirstBetDate THEN b.FirstBetType ELSE a.FirstBetType END and fb.WinPlace = CASE WHEN b.FirstBetDate < a.FirstBetDate THEN b.FirstWinPlace ELSE a.FirstWinPlace END)
						LEFT OUTER JOIN #BetTypeMapping lb on (lb.BetType = CASE WHEN b.LastBetDate > a.LastBetDate THEN b.LastBetType ELSE a.LastBetType END and lb.WinPlace = CASE WHEN b.LastBetDate > a.LastBetDate THEN b.LastWinPlace ELSE a.LastWinPlace END)
				WHERE	a.BatchKey = @BatchKey
						and a.FreeBet = 0
			) x
			CROSS APPLY (SELECT 'Master' DayType UNION ALL SELECT 'Analysis' DayType) y
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_PositionStaging',NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_PositionStaging', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

GO


