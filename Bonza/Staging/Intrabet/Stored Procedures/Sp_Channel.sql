﻿CREATE PROC [Intrabet].[Sp_Channel]
(
	@BatchKey	int,
	@FromDate	datetime2(3),
	@ToDate		datetime2(3)
)

AS
BEGIN
BEGIN TRY

EXEC [Control].Sp_Log	@BatchKey,'Sp_Channel','InsertIntoStaging','Start'

INSERT INTO [Stage].[Channel]
Select	@BatchKey,
		a.Channel ChannelId,
		IsNull(a.Comment,a.ChannelType) ChannelDescription,
		a.ChannelType ChannelName,
		a.ChannelType ChannelTypeName
From [$(Acquisition)].IntraBet.tblLUChannel a
LEFT OUTER JOIN [Intrabet].[ChannelMapping] b on a.Channel = b.Channel
Where ToDate = '9999-12-31 00:00:00.000' and FromDate > @FromDate AND FromDate <= @ToDate
Order By FromDate

EXEC [Control].Sp_Log @BatchKey, 'Sp_Channel', NULL, 'Success'

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Channel', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

	Update [Control].[ConfigurationItems] Set Value = 'N' Where ConfigurationItem = 'Intrabet.Sp_Channel'

END

