CREATE PROC [Intrabet].[Sp_Instrument_MoneyBookers]
(
	@BatchKey	int,
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS
BEGIN
	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int
	DECLARE @i int
	
	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	------ Initialize Temp Tables ---------------------
	IF OBJECT_ID('TempDB..#Candidates','U') IS NOT NULL BEGIN DROP TABLE #Candidates END;

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Get Moneybookers','Start', @Reload

	BEGIN TRANSACTION Instrument;
	
	SELECT ID,trnType,TimestampPPL,MAX(fromDate) fromDate  
	INTO #Candidates
	FROM (

			----------- tblMoneybookersdepositresponse -------
			SELECT ID,trnType,fromDate,TimestampPPL
			FROM (	
					SELECT pay_from_email AS ID,'MBK' trnType,fromDate,NULL TimestampPPL,ClientID, Status, md5sig,mb_amount,pay_from_email,mb_currency,mb_transaction_id,ReturnedPostBack,
						   IntraBetTransactionID,ErrorNo,ErrorDesc,md5calculated,TimeStamp
					FROM [$(Acquisition)].Intrabet.tblMoneybookersdepositresponse 
					WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate and pay_from_email  is not null and pay_from_email<>'n/a'	
					UNION ALL
					SELECT pay_from_email AS ID,'MBK' trnType,ToDate,NULL TimestampPPL,ClientID, Status, md5sig,mb_amount,pay_from_email,mb_currency,mb_transaction_id,ReturnedPostBack,
						   IntraBetTransactionID,ErrorNo,ErrorDesc,md5calculated,TimeStamp
					FROM [$(Acquisition)].Intrabet.tblMoneybookersdepositresponse 
					WHERE	ToDate > @FromDate AND ToDate <= @ToDate and pay_from_email  is not null and pay_from_email<>'n/a'
			) x
			GROUP BY ID,trnType,fromDate,TimestampPPL,ClientID,Status, md5sig,mb_amount,pay_from_email,mb_currency,mb_transaction_id,ReturnedPostBack,
						   IntraBetTransactionID,ErrorNo,ErrorDesc,md5calculated,TimeStamp
			HAVING COUNT(*) <> 2			
		 ) x
	GROUP BY ID,trnType,TimestampPPL
	OPTION (RECOMPILE)
	

	EXEC [Control].Sp_Log	@BatchKey,@Me,'InsertIntoStaging','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	INSERT	into Stage.Instrument (BatchKey,InstrumentId,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,VerifyAmount,InstrumentType,ProviderName,FromDate)
	SELECT	
			@BatchKey																			AS BatchKey, 
			C.id																				AS instrumentID,
			ISNULL(C.TrnType,'Unknown') 														AS Source, 
			'N/A'																				AS AccountNumber,
			'N/A'																				AS SafeAccountNumber,
			'N/A'																				AS AccountName,		
			'N/A'																				AS BSB,
			 NULL																				AS ExpiryDate,
			'U/K'																				AS Verified,
			NULL																				AS VerifiedAmount,
			'MoneyBookers'																		AS InstrumentType,
			 'N/A'																				AS ProviderName,
			c.FromDate																			AS FromDate		

	FROM #Candidates C  with (nolock)
	LEFT OUTER JOIN [$(Acquisition)].Intrabet.tblMoneybookersdepositresponse B WITH (NOLOCK)	ON 
					C.ID =Cast(B.ID as varchar) 
					AND B.FromDate <= c.FromDate AND B.ToDate > C.FromDate

	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

						
	COMMIT TRANSACTION Instrument;

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Success', @RowsProcessed = @RowsProcessed

	UPDATE [Control].[ConfigurationItems] Set Value = 'N' Where ConfigurationItem = 'Intrabet.Sp_Instrument_MoneyBookers'

END TRY
BEGIN CATCH

	ROLLBACK TRANSACTION Instrument;
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_Instrument_MoneyBookers', NULL, 'Failed', @ErrorMessage;
	RAISERROR(@ErrorMessage,16,1);
	THROW;

END CATCH
																					   																					
END
