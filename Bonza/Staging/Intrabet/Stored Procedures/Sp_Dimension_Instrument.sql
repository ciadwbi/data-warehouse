﻿/*CREATE PROC Intrabet.[Sp_Dimension_Instrument]
(
	@BatchKey	int
)
WITH RECOMPILE
AS 
BEGIN

BEGIN TRY
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

BEGIN TRANSACTION Dimension_Instrument ;
INSERT INTO Lekha.InstrumentDim ( InstrumentID,Source,InstrumentKey,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,
												InstrumentTypeKey,InstrumentType,ProviderKey,ProviderName,FromDate,ToDate,
												FirstDate,CreatedDate,CreatedBatchKey,CreatedBy,ModifiedDate,ModifiedBatchKey,ModifiedBy)
SELECT
Stage.InstrumentID,
Stage.Source,
--CASE WHEN Dim_InstrumentKey.InstrumentKey IS NULL THEN 
--        CASE WHEN Stage.InstrumentID='Unknown' THEN -1
--            WHEN Stage.InstrumentID='N/A' THEN 0
            --ELSE 
			((SELECT ISNULL(MAX(InstrumentKey),0) FROM [$([$(Dimensional)])].Dimension.Instrument)+
			DENSE_RANK() OVER (PARTITION BY Dim_InstrumentKey.InstrumentKey ORDER BY Stage.InstrumentID,Stage.source))
			AS InstrumentKey,
--         End
--ELSE Dim_InstrumentKey.InstrumentKey END AS InstrumentKey,
Stage.AccountNumber,
Stage.SafeAccountNumber,
Stage.AccountName,
Stage.BSB,
Stage.ExpiryDate,
Stage.Verified,
CASE 
	WHEN Dim_InstrumentTypeKey.InstrumentTypeKey IS NULL 
	THEN CASE 
			WHEN Stage.InstrumentType='Unknown' THEN -1
			WHEN Stage.InstrumentType='N/A' THEN 0
			ELSE (SELECT ISNULL(MAX(InstrumentTypeKey),0) FROM [$([$(Dimensional)])].Dimension.Instrument)+
			DENSE_RANK() OVER (PARTITION BY Dim_InstrumentTypeKey.InstrumentTypeKey ORDER BY Stage.InstrumentType)
         End
	ELSE Dim_InstrumentTypeKey.InstrumentTypeKey 
END AS InstrumentTypeKey,
Stage.InstrumentType,
CASE 
	WHEN Dim_ProviderKey .ProviderKey IS NULL 
	THEN CASE 
			WHEN Stage.ProviderName='Unknown' THEN -1
            WHEN Stage.ProviderName='N/A' THEN 0
            ELSE  (SELECT ISNULL(MAX(ProviderKey ),0) FROM [$([$(Dimensional)])].Dimension.Instrument)+
			DENSE_RANK() OVER (PARTITION BY Dim_ProviderKey .ProviderKey  ORDER BY Stage.ProviderName)
		End
	ELSE Dim_ProviderKey .ProviderKey 
END AS ProviderKey ,
Stage.ProviderName,
Stage.FromDate,
'9999-12-31' AS ToDate,
CURRENT_TIMESTAMP AS FirstDate,
CURRENT_TIMESTAMP AS CreatedDate,
@BatchKey AS CreatedBatchKey,
CURRENT_USER AS CreatedBy,
CURRENT_TIMESTAMP AS ModifiedDate,
@BatchKey AS ModifiedBatchKey,
CURRENT_USER  AS ModifiedBy

FROM lekha.Instrument2 Stage
Left outer JOIN lekha.InstrumentDim d  on (Stage.InstrumentID=d.InstrumentID and d.source=Stage.source)
LEFT OUTER JOIN (SELECT DISTINCT InstrumentID,InstrumentKey,Source FROM lekha.InstrumentDim) Dim_InstrumentKey
        ON (Stage.InstrumentID=Dim_InstrumentKey.InstrumentID and Stage.Source=Dim_InstrumentKey.Source)

LEFT OUTER JOIN (SELECT DISTINCT InstrumentTypeKey,InstrumentType,Source FROM lekha.InstrumentDim) Dim_InstrumentTypeKey
        ON (Stage.InstrumentType=Dim_InstrumentTypeKey.InstrumentType and Stage.Source=Dim_InstrumentTypeKey.Source)

LEFT OUTER JOIN (SELECT DISTINCT ProviderKey,ProviderName,Source FROM lekha.InstrumentDim) Dim_ProviderKey
        ON (Stage.ProviderName=Dim_ProviderKey.ProviderName and Stage.Source=Dim_ProviderKey.Source)
WHERE Stage.BatchKey=@BatchKey and d.InstrumentID is null


Update d
set 
       D.InstrumentID=s.InstrumentID,
		--D.InstrumentKey =(CASE WHEN Dim_InstrumentKey.InstrumentKey IS NULL 
		--					  THEN  CASE WHEN s.InstrumentID='Unknown' THEN -1
		--								WHEN s.InstrumentID='N/A' THEN 0
		--								Else (SELECT ISNULL(MAX(InstrumentKey),0) FROM [$([$(Dimensional)])].Dimension.Instrument)+
		--								 (select DENSE_RANK() OVER (PARTITION BY Dim_InstrumentKey.InstrumentKey ORDER BY s.InstrumentID,s.Source))
		--									End
		--					   ELSE Dim_InstrumentKey.InstrumentKey 
		--				  END),
       D.Source=S.Source ,
       D.AccountNumber=S.AccountNumber ,
       D.SafeAccountNumber=S.SafeAccountNumber ,
       D.AccountName=S.AccountName,
       D.BSB=S.BSB ,
	   D.ExpiryDate=s.ExpiryDate,
	   D.Verified=S.Verified,
	   D.InstrumentTypeKey=(CASE 
								WHEN Dim_InstrumentTypeKey.InstrumentTypeKey IS NULL 
								THEN  (CASE 
											WHEN s.InstrumentType='Unknown' THEN -1
											WHEN s.InstrumentType='N/A' THEN 0
											ELSE (SELECT ISNULL(MAX(InstrumentTypeKey),0) FROM [$([$(Dimensional)])].Dimension.Instrument)+
											(select DENSE_RANK() OVER (PARTITION BY Dim_InstrumentTypeKey.InstrumentTypeKey ORDER BY s.InstrumentType))
									  End)
								ELSE Dim_InstrumentTypeKey.InstrumentTypeKey 
							END),
       D.InstrumentType=S.InstrumentType ,
	   D.ProviderKey=CASE 
						WHEN Dim_ProviderKey .ProviderKey IS NULL 
						THEN  CASE 
								WHEN s.ProviderName='Unknown' THEN -1
								WHEN s.ProviderName='N/A' THEN 0
								ELSE (SELECT ISNULL(MAX(ProviderKey ),0) FROM [$([$(Dimensional)])].Dimension.Instrument)+
								(select DENSE_RANK() OVER (PARTITION BY Dim_ProviderKey .ProviderKey  ORDER BY s.ProviderName))
							  End
						ELSE Dim_ProviderKey.ProviderKey 
					  END ,
       D.ProviderName=s.ProviderName ,
       D.FromDate=s.FromDate,
	   D.ModifiedDate=CURRENT_TIMESTAMP,
	   D.ModifiedBatchKey=@BatchKey,
	   D.ModifiedBy=CURRENT_USER

FROM lekha.InstrumentDim d
INNER JOIN lekha.Instrument2 s   on (s.InstrumentID=d.InstrumentID and d.source=s.source)

LEFT OUTER JOIN (SELECT DISTINCT InstrumentID,InstrumentKey,Source FROM lekha.InstrumentDim) Dim_InstrumentKey
        ON (s.InstrumentID=Dim_InstrumentKey.InstrumentID and s.Source=Dim_InstrumentKey.Source)

LEFT OUTER JOIN (SELECT DISTINCT InstrumentTypeKey,InstrumentType,Source FROM lekha.InstrumentDim) Dim_InstrumentTypeKey
        ON (s.InstrumentType=Dim_InstrumentTypeKey.InstrumentType and s.Source=Dim_InstrumentTypeKey.Source)

LEFT OUTER JOIN (SELECT DISTINCT ProviderKey,ProviderName,Source FROM lekha.InstrumentDim) Dim_ProviderKey
        ON (s.ProviderName=Dim_ProviderKey.ProviderName and s.Source=Dim_ProviderKey.Source)

WHERE s.BatchKey=@BatchKey

COMMIT TRANSACTION Dimension_Instrument;
END TRY
BEGIN CATCH
	Rollback TRANSACTION Dimension_Instrument;
	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           -- Log the failure at the object level
                           SET    @Error = ERROR_NUMBER()
                           SET    @ErrorMessage = ERROR_MESSAGE()
						   Raiserror(@ErrorMessage,16,1)

END CATCH

End	*/