CREATE PROC [Intrabet].[Sp_AccountStatus]
(
	@BatchKey	INT,
	@FromDate	DATETIME2(3),
	@ToDate		DATETIME2(3)
)
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

		------ Initialize Temp Tables ---------------------

		IF OBJECT_ID('TempDB..#ClientStage','U') IS NOT NULL BEGIN DROP TABLE #ClientStage END;
		IF OBJECT_ID('TempDB..#tempCAC','U') IS NOT NULL BEGIN DROP TABLE #tempCAC END;
		IF OBJECT_ID('TempDB..#tempCL','U') IS NOT NULL BEGIN DROP TABLE #tempCL END;
		IF OBJECT_ID('TempDB..#MulAccounts','U') IS NOT NULL BEGIN DROP TABLE #MulAccounts END;
		IF OBJECT_ID('TempDB..#AccountStage','U') IS NOT NULL BEGIN DROP TABLE #AccountStage END;

		
		EXEC [Control].Sp_Log	@BatchKey,'Sp_AccountStatus','GetAllType-2Changes-AllClientTables','Start'
		----------- tblClients -----------
		
		SELECT A.ClientID, A.FromDate
		INTO #ClientStage
		FROM [$(Acquisition)].[IntraBet].tblClients A WITH (FORCESEEK)
		LEFT HASH JOIN [$(Acquisition)].[IntraBet].tblClients B WITH (FORCESEEK) ON A.ClientID=B.ClientID AND A.FromDate=B.ToDate 
		WHERE A.FromDate>=@FromDate AND A.FromDate<@ToDate AND A.ToDate>@FromDate 
		AND (ISNULL(A.ClientType,1)<>ISNULL(B.ClientType,1) OR ISNULL(A.ProfileID,1)<>ISNULL(B.ProfileID,1) OR ISNULL(A.OrgID,1)<>ISNULL(B.OrgID,1))

		----------- tblAccounts -----------

		UNION
		SELECT A.CLIENTID,A.FromDate
		FROM [$(Acquisition)].[IntraBet].tblAccounts A WITH (FORCESEEK)
		LEFT HASH JOIN [$(Acquisition)].[IntraBet].tblAccounts B WITH (FORCESEEK) ON A.AccountID=B.AccountID AND A.FromDate=B.ToDate 
		WHERE A.FromDate>=@FromDate AND A.FromDate<@ToDate AND A.ToDate>@FromDate 
		AND (ISNULL(A.AccountEnabled,1)<>ISNULL(B.AccountEnabled,1) OR ISNULL(A.Status,1)<>ISNULL(B.Status,1))


		 ----------- tblClientInfo -----------

		 UNION
		  SELECT A.ClientID, A.FromDate
		  FROM [$(Acquisition)].[IntraBet].tblClientInfo A WITH (FORCESEEK)
		  LEFT HASH JOIN [$(Acquisition)].[IntraBet].tblClientInfo B WITH (FORCESEEK) ON A.ClientID=B.ClientID AND A.FromDate=B.ToDate 
		  WHERE A.FromDate>=@FromDate AND A.FromDate<@ToDate AND A.ToDate>@FromDate 
		  AND (ISNULL(A.VIP,1)<>ISNULL(B.VIP,1) OR ISNULL(A.CreditLimit,1)<>ISNULL(B.CreditLimit,1) OR ISNULL(A.MinBet,1)<>ISNULL(B.MinBet,1) 
			OR ISNULL(A.MaxBet,1)<>ISNULL(B.MaxBet,1) OR ISNULL(A.IntroducedBy,1)<>ISNULL(B.IntroducedBy,1) OR ISNULL(A.Handled,1)<>ISNULL(B.Handled,1) 
			OR ISNULL(A.PhoneColourID,1)<>ISNULL(B.PhoneColourID,1) OR ISNULL(A.DisableDeposit,1)<>ISNULL(B.DisableDeposit,1) OR ISNULL(A.DisableWithdrawal,1)<>ISNULL(B.DisableWithdrawal,1))


		 ----------- tblClientAccountClosures -----------

		 UNION
		  SELECT A.CLIENTID,A.FromDate
		  FROM [$(Acquisition)].[IntraBet].tblClientAccountClosures A WITH (FORCESEEK)
		  LEFT HASH JOIN [$(Acquisition)].[IntraBet].tblClientAccountClosures B WITH (FORCESEEK) ON A.ClientID=B.ClientID AND A.AccountNumber=B.AccountNumber AND A.FromDate=B.ToDate 
		  WHERE A.FromDate>=@FromDate AND A.FromDate<@ToDate AND A.ToDate>@FromDate 
		  AND (ISNULL(A.CAPKID,1)<>ISNULL(B.CAPKID,1))

		 ----------- tblClientLimit ---------

		 UNION
		  SELECT A.CLIENTID,A.FromDate
		  FROM [$(Acquisition)].[IntraBet].tblClientLimit A WITH (FORCESEEK)
		  LEFT HASH JOIN [$(Acquisition)].[IntraBet].tblClientLimit B WITH (FORCESEEK) ON A.ClientID=B.ClientID AND A.FromDate=B.ToDate 
		  WHERE A.FromDate>=@FromDate AND A.FromDate<@ToDate AND A.ToDate>@FromDate 
		  AND ISNULL(A.id,1)<> ISNULL(B.id,1) 
		   
			 ----------- tblSubscriptionCentre ---------

		 UNION
		  SELECT A.CLIENTID,A.FromDate
		  FROM [$(Acquisition)].[IntraBet].tblSubscriptionCentre A WITH (FORCESEEK)
		  LEFT HASH JOIN [$(Acquisition)].[IntraBet].tblSubscriptionCentre B WITH (FORCESEEK) ON A.ClientID=B.ClientID AND A.FromDate=B.ToDate 
		  WHERE A.FromDate>=@FromDate AND A.FromDate<@ToDate AND A.ToDate>@FromDate 
		  AND (ISNULL(A.receiveMarketingEmail,1)<> ISNULL(B.receiveMarketingEmail,1) OR ISNULL(A.receiveCompetitionEmail,1)<>ISNULL(B.receiveCompetitionEmail,1) OR ISNULL(A.receiveSMS,1)<>ISNULL(B.receiveSMS,1) 
			OR ISNULL(A.receivePostalMail,1)<>ISNULL(B.receivePostalMail,1) OR ISNULL(A.receiveFax,1)<>ISNULL(B.receiveFax,1))
		OPTION (RECOMPILE);
			
		--------------------- INSERT INTO STAGING ----------------------------------------

		EXEC [Control].Sp_Log	@BatchKey,'Sp_AccountStatus','InsertIntoTempTable','Start', @RowsProcessed = @@ROWCOUNT

		-------------- Extracting and ranking appropriate records from tblClientAccountClosures to avoid multiple records when joined later-----------------------
		  
		select CS.*,CAC.Date,CAC.DisabledReason,CAC.Enabled,CAC.CAPKID,CAC.Reason,CAC.ReIntroducedBy,CAC.ReIntroducedDate,CAC.UserCode
		, ISNULL(ROW_NUMBER() OVER(PARTITION BY IsNull(CS.ClientID,-1),CS.FromDate  ORDER BY isnull(CAC.CAPKID,-1)  DESC),-1) AS I_RNK_CAPKID 
		into #tempCAC
		from #ClientStage CS
		inner join [$(Acquisition)].Intrabet.tblClientAccountClosures CAC on CS.ClientID=CAC.ClientId and CS.FromDate>=CAC.FromDate AND CS.FromDate <CAC.ToDate AND CAC.ToDate > @FromDate
		OPTION (RECOMPILE, TABLE HINT(CAC, FORCESEEK(NI_tblClientAccountClosures_ClientId_AccountNumber(ClientId))))
		
		--------------------- Extracting and ranking appropriate records from tblClientLimit to avoid multiple records when joined later-----------------------
		  
		select CS.*,Cl.depositLimit,cl.id,cl.limitPeriod,cl.lossLimit,cl.timeStamp, ISNULL(ROW_NUMBER() OVER(PARTITION BY IsNull(CS.ClientID,-1),CS.FromDate  ORDER BY isnull(CL.ID,-1)  DESC),-1) AS I_RNK_CLID 
		into #tempCL
		from #ClientStage CS
		inner join [$(Acquisition)].Intrabet.tblClientLimit Cl on CS.ClientID=CL.ClientId and CS.FromDate>=CL.FromDate AND CS.FromDate <CL.ToDate AND CL.ToDate > @FromDate
		OPTION (RECOMPILE, TABLE HINT (Cl, FORCESEEK(NI_tblClientLimit_ClientId(ClientId))))

		--Identify Clients with multiple Accounts
		select c.clientID
		into #MulAccounts
		from #clientStage c
		INNER JOIN [$(Acquisition)].[IntraBet].tblAccounts A on A.ClientID=c.ClientID and C.FromDate>=A.FromDate AND C.FromDate <A.ToDate AND A.ToDate>=@FromDate
		group by c.ClientID
		having count(distinct A.AccountID)>1 
		OPTION (RECOMPILE, TABLE HINT (A, FORCESEEK(NI_tblAccounts_ClientId(ClientId))))

		    SELECT
		   @BatchKey as BatchKey
		   ,A.AccountID as LedgerID
		   ,'IAA' as LedgerSource
		   ,Case When A.AccountEnabled=1
				 Then 'Yes'
				 Else 'No'
			 End  LedgerEnabled
		   ,Case when A.[Status] in ('',' ') or A.[Status] is null
				 Then 'U'
				 Else A.[Status]
			 End as LedgerStatus
		    ,COALESCE(MA.AccountTypeName,TA.AccountTypeName,BA.AccountTypeName,'Client')  as AccountType
		   ,ISNULL(CP.InternetProfile,'N/A') as InternetProfile
		   ,Case When (ISNULL(CI.VIP%2,2)<>0)
				 Then 'Yes'
				 Else 'No'
			 End  VIP
		   ,CASE 
				WHEN C.orgid = 1 THEN 'WHA'
				WHEN C.orgid = 2 THEN 'CBT'
				WHEN C.orgid = 3 THEN 'RWWA'
				WHEN C.orgid = 8 THEN 'TWH'
				ELSE 'Unk'  
		   END            AS BrandCode
		  ,CASE 
				WHEN I2.BDM = 'Y' AND C.ClientType & 16 != 16 THEN 'BDM' 
				WHEN C.orgid = 3 THEN 'RWWA'
				WHEN C.orgid in (1,2,8) THEN 'Digital' 
				Else 'Unknown'
		   END            AS DivisionName
		   ,CI.CreditLimit as CreditLimit
		   ,CI.MinBet as MinBetAmount
		   ,CI.MaxBet as MaxBetAmount
		   ,ISNULL(I.IntroducedName,'N/A') as Introducer
		   ,Case When CI.Handled=1
				 Then 'Yes'
				 Else 'No'
			End as Handled
		   ,ISNULL(PC.ColourDesc,'N/A') as PhoneColourDesc
		   ,Case when CI.DisableDeposit=1
				 Then 'Yes'
				 Else 'No'
			End as DisableDeposit
		  ,Case  when CI.DisableWithdrawal=1
				 Then 'Yes'
				 Else 'No'
			End as DisableWithdrawal
		   ,ISNULL(CAC.UserCode,0) as AccountClosedByUserID
		   ,case When CAC.UserCode is null
				 Then 'N/A'
				 Else 'IUU'
			End as AccountClosedByUserSource
		   ,CAC.[Date] as AccountClosedDate
		   ,CAC.ReIntroducedBy as ReIntroducedBy
		   ,Case When CAC.ReIntroducedDate='1900-01-01' 
				 Then Null
				 Else CAC.ReIntroducedDate
			End as ReIntroducedDate
		   ,Case When CAC.[Date] is null
				 Then 'N/A'
				 When CAC.DisabledReason is null or CAC.DisabledReason in ('',' ')
				 Then 'Unknown'
				 Else ACR.ClosureReason
			End as AccountClosureReason
		   ,Case When CL.limitPeriod=-1
				 Then 0
				 Else CL.depositLimit 
			End as DepositLimit
			,Case When CL.limitPeriod=-1
				 Then 0
				 Else CL.LossLimit 
			End as LossLimit
		   ,CL.limitPeriod as LimitPeriod
		   ,CL.[TimeStamp] as LimitTimeStamp
		   ,Case When SC.receiveMarketingEmail=1
				 Then 'Yes'
				 Else 'No'
			End as ReceiveMarketingEmail
		   ,Case When SC.receiveCompetitionEmail=1
				 Then 'Yes'
				 Else 'No'
			End as ReceiveCompetitionEmail
			,Case When SC.receiveSMS=1
				 Then 'Yes'
				 Else 'No'
			End as receiveSMS
			,Case When SC.receivePostalMail=1
				 Then 'Yes'
				 Else 'No'
			End as receivePostalMail
			,Case When SC.receiveFax=1
				 Then 'Yes'
				 Else 'No'
			End as receiveFax
			,CONVERT(datetime2(3),NULL) RewardExclusionDate
			,ISNULL(CONVERT(char(10),CONVERT(date,NULL)),'N/A') RewardExclusionDayText
			,0 RewardExclusionReasonId
		  , CASE 
				WHEN OD.FirstFromDate>=Isnull(L.CorrectedDate,X.FromDate) --Check if the record is the First record of an AccountID
				THEN CASE WHEN MAC.ClientID is not NULL and OD.FirstFromDate<>'2013-12-19'--Check if the ClientID has multiple Accounts
						  THEN OD.FirstFromDate
						  ELSE CASE WHEN CI.StartDate>Lat.CorrectedDate --Load the smallest of the FromDate and StartDate in tblCLientInfo
								    THEN COALESCE(Lat.CorrectedDate,CI.StartDate,L.CorrectedDate,X.FromDate)
								    Else COALESCE(CI.StartDate,Lat.CorrectedDate,L.CorrectedDate,X.FromDate)
							   END
					 END
				ELSE Isnull(L.CorrectedDate,X.FromDate)
		   END FromDate
		  INTO #AccountStage
		  FROM #ClientStage X
		  LEFT OUTER JOIN [$(Acquisition)].[IntraBet].tblClients C      ON C.ClientID=X.ClientID AND X.FromDate>=C.FromDate AND X.FromDate<C.ToDate
		  LEFT HASH JOIN (select distinct profileID,InternetProfile from  [$(Acquisition)].IntraBet.tblClientProfiles )CP  on C.ProfileID=CP.ProfileID
		  LEFT HASH JOIN (SELECT A1.AccountID,A1.ClientID,A1.AccountEnabled,Status,MIN(A1.FromDate) AS FromDate,ToDate
								FROM [$(Acquisition)].[IntraBet].tblAccounts A1   inner join #ClientStage CS on CS.ClientID=A1.ClientID
								GROUP BY A1.AccountID,A1.ClientID,A1.AccountEnabled,Status,A1.ToDate) A  ON X.ClientID=A.ClientID AND X.FromDate>=A.FromDate AND X.FromDate<A.ToDate
		  LEFT OUTER JOIN (select ClientID,VIP,MinBet,MaxBet,DisableDeposit,DisableWithdrawal,Handled, CreditLimit, PhoneColourID,IntroducedBy,StartDate,FromDate,ToDate 
								from [$(Acquisition)].[IntraBet].tblClientInfo   )CI  
								 ON X.ClientID=CI.ClientID     AND  X.FromDate>=CI.FromDate AND X.FromDate<CI.ToDate
		  LEFT HASH JOIN [$(Acquisition)].[IntraBet].[tblLUPhoneColour] PC   ON CI.PhoneColourID=PC.PhoneColourID  AND  X.FromDate>=PC.FromDate AND X.FromDate<PC.ToDate
		  LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers I  on CI.IntroducedBy=I.ClientID
		  LEFT HASH JOIN Reference.Introducers  I2         ON CI.IntroducedBy=I2.ClientID  
		  LEFT OUTER JOIN [$(Acquisition)].[IntraBet].tblSubscriptionCentre SC     ON X.ClientID=SC.ClientID     AND  X.FromDate>=SC.FromDate AND X.FromDate<SC.ToDate
		  --LEFT OUTER JOIN (SELECT DISTINCT C2.ClientID,'Test' AS AccountTypeName FROM  #ClientStage CS      -- All Test Accounts 
				--			inner join [$(Acquisition)].IntraBet.tblClients C2 on CS.ClientID=C2.ClientID
				--		   WHERE c2.firstName like '%test%') TA ON X.ClientID=TA.ClientID
		  LEFT HASH JOIN --Amended JOIN based on rules applied in BID
		  (
			select Distinct CS.ClientID, 'Test' AS AccountTypeName
			from #ClientStage CS
			inner join [$(Acquisition)].Intrabet.tblClients c1 on CS.ClientID = c1.ClientID and CS.FromDate>=c1.FromDate AND CS.FromDate <c1.ToDate AND c1.ToDate>@FromDate
			inner join [$(Acquisition)].Intrabet.tblClientAddresses ca1 on CS.ClientID = ca1.ClientID and ca1.AddressType = 2 and CS.FromDate>=ca1.FromDate AND CS.FromDate <ca1.ToDate AND ca1.ToDate>@FromDate
			inner join [$(Acquisition)].Intrabet.tblClientInfo ci1 on CS.ClientID = ci1.ClientID and CS.FromDate>=ci1.FromDate AND CS.FromDate <ci1.ToDate AND ci1.ToDate>@FromDate
			where
			(
				(Username like '%test%' and (Surname like '%test%' or FirstName like '%test%'))
				or (Surname like '%test%' and FirstName like '%test%')
				or c1.ClientID in (1256558,16956,1157662,83769,768605,1269714,1269726,1269741,1269748,1269727,1269742,1269703,1269721,1269715,1269717,1269737,1269738,1269716,1269743,1269718,1269735,1269722,1269719,832054,832875,833262,833458,833775)
				or (
					email like '%@williamhill.com%'
					and isnull(IntroducedBy,0) < 1
					and isnull(suburb,'') = 'sydney'
					and postcode = '2000'
				)
			)
			and c1.ClientID not in (940527,921631,180249,1212424,1243587,236560,796203,39692,416691,957581,50395,247559,223079,230427,265186,100869,586315,370498,746161,749035,812802,953831,909375,1050745,941046,1016061,1045985,871219,1020745,1012267,1109095,1211152,1262098,1246235,1246233,1219050)
		  ) TA ON X.ClientID=TA.ClientID

		  LEFT HASH JOIN (SELECT DISTINCT C2.ClientID,'BetBack' AS AccountTypeName,C2.FROMDATE,C2.TODATE						-- Betback Accounts based on ClientType
							FROM [$(Acquisition)].IntraBet.tblClients C2   inner join #ClientStage CS on CS.ClientID=C2.ClientID
							WHERE C2.ClientType&16=16) BA ON X.ClientID=BA.ClientID AND  X.FromDate>=BA.FromDate AND X.FromDate <BA.ToDate

		  LEFT HASH JOIN (SELECT FromValue AS ClientID,ToValue As AccountTypeName									-- Manual override of Account Types using the Reference Table
							FROM [Reference].[Transformation] T  
							INNER JOIN [Reference].[MappingTransformation] MT   ON T.TransformationKey=MT.TransformationKey
							INNER JOIN [Reference].[Mapping] M   ON M.MappingKey=MT.MappingKey
							WHERE M.FromSchema='IntraBet' AND M.ToColumn='AccountTypeName') MA ON TRY_CONVERT(VARCHAR(100),MA.ClientID)=TRY_CONVERT(VARCHAR(100),X.ClientID)

		  LEFT HASH JOIN (SELECT A2.ACCOUNTID,MIN(ISNULL(l1.CorrectedDate,A2.FROMDATE)) AS FirstFromDate 
						   FROM [$(Acquisition)].[IntraBet].tblAccounts A2   inner join #ClientStage CS on CS.ClientID=A2.ClientID 
						   left outer join [$(Acquisition)].[IntraBet].Latency l1 on l1.OriginalDate=A2.FromDate GROUP BY A2.ACCOUNTID) OD  -- Added to verify OpenedDateTime for Clients 
							ON A.AccountID=OD.AccountID
		LEFT HASH JOIN #tempCAC CAC  on X.ClientID=CAC.ClientID and X.FromDate=CAC.FromDate and CAC.I_RNK_CAPKID=1
		LEFT HASH JOIN [$(Acquisition)].[IntraBet].[tblLUAccountClosureReasons] ACR   on CAC.DisabledReason=ACR.ACRID and X.FromDate>=ACR.FromDate and X.FromDate <ACR.ToDate
		LEFT HASH JOIN #tempCL CL     ON X.ClientID=CL.ClientID     AND  X.FromDate=CL.FromDate AND CL.I_RNK_CLID=1
		LEFT LOOP JOIN [$(Acquisition)].IntraBet.Latency L   on L.OriginalDate=X.FromDate
		LEFT LOOP JOIN [$(Acquisition)].IntraBet.Latency Lat   on Lat.OriginalDate=CI.FromDate
		LEFT HASH JOIN #MulAccounts MAC   on X.ClientID=MAC.ClientID
		where A.AccountID is not null
		OPTION(RECOMPILE, TABLE HINT(C, FORCESEEK), TABLE HINT(SC, FORCESEEK))

		select * ,ROW_NUMBER() OVER(PARTITION BY X.LedgerID, X.FromDate ORDER BY X.FromDate  DESC) AS I_RNK_FD 
		into #AccountStage2
		from #AccountStage X
		OPTION (RECOMPILE)

		EXEC [Control].Sp_Log	@BatchKey,'Sp_AccountStatus','InsertIntoStaging','Start', @RowsProcessed = @@ROWCOUNT

		INSERT INTO Stage.AccountStatus WITH (TABLOCK)
		(BatchKey,[LedgerID], [LedgerSource], [LedgerEnabled],	[LedgerStatus] ,[AccountType],[InternetProfile],[VIP] ,[BrandCode],[DivisionName], [CreditLimit],[MinBetAmount],[MaxBetAmount],[Introducer],[Handled],[PhoneColourDesc],[DisableDeposit],[DisableWithdrawal],
		[AccountClosedByUserID],[AccountClosedByUserSource],[AccountClosedDate],[ReIntroducedBy],[ReIntroducedDate],[AccountClosureReason] ,[DepositLimit],[LossLimit],[LimitPeriod],[LimitTimeStamp],[ReceiveMarketingEmail],
		[ReceiveCompetitionEmail],[ReceiveSMS],[ReceivePostalMail],[ReceiveFax], [RewardExclusionDate], [RewardExclusionDayText], [RewardExclusionReasonId], [FromDate])

		SELECT BatchKey,LedgerID, LedgerSource,[LedgerEnabled],[LedgerStatus] ,[AccountType],[InternetProfile],[VIP] ,[BrandCode], [DivisionName], [CreditLimit],[MinBetAmount],[MaxBetAmount],[Introducer],[Handled],[PhoneColourDesc],[DisableDeposit],[DisableWithdrawal],
		[AccountClosedByUserID],[AccountClosedByUserSource],[AccountClosedDate],[ReIntroducedBy],[ReIntroducedDate],[AccountClosureReason] ,[DepositLimit],[LossLimit],[LimitPeriod],[LimitTimeStamp],[ReceiveMarketingEmail],
		[ReceiveCompetitionEmail],[ReceiveSMS],[ReceivePostalMail],[ReceiveFax], [RewardExclusionDate], [RewardExclusionDayText], [RewardExclusionReasonId], [FromDate]
		FROM #AccountStage2 WHERE I_RNK_FD=1
		OPTION (RECOMPILE)

		EXEC [Control].Sp_Log @BatchKey, 'Sp_AccountStatus', NULL, 'Success', @RowsProcessed = @@ROWCOUNT

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_AccountStatus', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
END
