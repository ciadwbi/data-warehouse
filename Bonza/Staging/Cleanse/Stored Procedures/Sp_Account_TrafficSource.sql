﻿CREATE proc [Cleanse].[Sp_Account_TrafficSource]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET @Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Cache', 'Start'

	SELECT	AccountKey,
			SourceBTag2,
			SourceTrafficSource,
			SourceRefURL,
			SourceCampaignID,
			SourceKeywords,
			BTag2,
			AffiliateId,
			AffiliateName,
			SiteID,
			TrafficSource,
			RefURL,
			CampaignId,
			Keywords
	INTO	#Accounts
	FROM	[$(Dimensional)].Dimension.Account
	WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)
			AND (Increment = @Increment OR @Increment = 0) 
	OPTION (IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)

	EXEC [Control].Sp_Log @BatchKey,@Me, 'BTag2', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		BTag2 = UpdatedBTag2
	FROM	(	SELECT	BTag2,
						CASE ISNULL(LTRIM(RTRIM(SourceBTag2)),'')
							WHEN '' THEN 'N/A'
							ELSE LTRIM(RTRIM(SourceBTag2))
						END UpdatedBTag2
				FROM	#Accounts
			) u
	WHERE	BTag2 <> UpdatedBTag2

	EXEC [Control].Sp_Log @BatchKey,@Me, 'AffiliateId', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		AffiliateId = UpdatedAffiliateId
	FROM	(	SELECT	AffiliateId,
						CASE 
							WHEN SourceBTag2 IS NULL THEN 'N/A'
							WHEN SourceBTag2 NOT LIKE '%-%-%' THEN 'N/A'
							WHEN SourceBTag2 like 'p-%-%-%' THEN REPLACE(SUBSTRING(	SourceBTag2,
																					CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)+1)+1,
																					LEN(SourceBTag2) - CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)+1)
																					),
																		'-','')
							WHEN SourceBTag2 like '%-%-%-%' THEN SUBSTRING(	SourceBTag2,
																			CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)+1,
																			CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)+1) - CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)-1
																			)
							WHEN SourceBTag2 like '%-%-%/%' THEN SUBSTRING(	SourceBTag2,
																			CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)+1,
																			CHARINDEX('/',SourceBTag2,CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)+1) - CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)-1
																			)
							WHEN SourceBTag2 like '%-%-%?%' THEN SUBSTRING(	SourceBTag2,
																			CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)+1,
																			CHARINDEX('?',SourceBTag2,CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)+1) - CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)-1
																			)
							WHEN SourceBTag2 like '%-%-[0-9][0-9]' 
								OR SourceBTag2 like '%-%-[0-9][0-9][0-9]' 
								OR SourceBTag2 like '%-%-[0-9][0-9][0-9][0-9]'
								OR SourceBTag2 like '%-%-[0-9][0-9][0-9][0-9][0-9]'
								OR SourceBTag2 like '%-%-[0-9][0-9][0-9][0-9][0-9]'			
														  THEN SUBSTRING(	SourceBTag2,
																			CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)+1,
																			LEN(SourceBTag2) - CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)-1
																		)
							ELSE 'Unknown'
						END UpdatedAffiliateID
				FROM	#Accounts
			) u
	WHERE	AffiliateId <> UpdatedAffiliateId

	EXEC [Control].Sp_Log @BatchKey,@Me, 'AffiliateName', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	#Accounts
	SET		AffiliateName = 'Unknown'
	WHERE	AffiliateName <> 'Unknown'

	EXEC [Control].Sp_Log @BatchKey,@Me, 'SiteId', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		SiteId = UpdatedSiteId
	FROM	(	SELECT	SiteId,
						CASE 
							WHEN SourceBTag2 like 'p-%-%' THEN SUBSTRING(	SourceBTag2,
																			CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)+1,
																			CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)+1) - CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1)-1
																			)
							WHEN SourceBTag2 like '%[0-9]-%-%' THEN SUBSTRING(	SourceBTag2,
																				CHARINDEX('-',SourceBTag2)+1,
																				CHARINDEX('-',SourceBTag2,CHARINDEX('-',SourceBTag2)+1) - CHARINDEX('-',SourceBTag2)-1
																				)
							WHEN SourceBTag2 like 'a_%b_%' THEN SUBSTRING(	SourceBTag2,
																			3,
																			CHARINDEX('b',SourceBTag2)-3
																			)
							ELSE 'N/A'
						END UpdatedSiteID
				FROM	#Accounts
			) u
	WHERE	SiteId <> UpdatedSiteId

	EXEC [Control].Sp_Log @BatchKey,@Me, 'TrafficSource', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		TrafficSource = UpdatedTrafficSource
	FROM	(	SELECT	TrafficSource,
						CASE 
							WHEN LTRIM(RTRIM(SourceTrafficSource)) = '' THEN 'N/A'
							WHEN SourceBTag2 like '%[0-9]%' AND SourceTrafficSource IS NULL THEN 'Unknown'
							WHEN SourceBTag2 like '%[0-9]%' THEN LTRIM(RTRIM(SourceTrafficSource))
							WHEN SourceTrafficSource IS NOT NULL THEN LTRIM(RTRIM(SourceTrafficSource))
							ELSE 'N/A'
						END UpdatedTrafficSource
				FROM	#Accounts
			) u
	WHERE	TrafficSource <> UpdatedTrafficSource

	EXEC [Control].Sp_Log @BatchKey,@Me, 'RefURL', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		RefURL = UpdatedRefURL
	FROM	(	SELECT	RefURL,
						CASE 
							WHEN LTRIM(RTRIM(SourceRefURL)) = '' THEN 'N/A'
							WHEN SourceRefURL IS NULL THEN 'N/A'
							ELSE LTRIM(RTRIM(SourceRefURL))
						END UpdatedRefURL
				FROM	#Accounts
			) u
	WHERE	RefURL <> UpdatedRefURL

	EXEC [Control].Sp_Log @BatchKey,@Me, 'CampaignId', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		CampaignId = UpdatedCampaignId
	FROM	(	SELECT	CampaignID,
						CASE 
							WHEN LTRIM(RTRIM(SourceCampaignID)) = '' THEN 'N/A'
							WHEN SourceCampaignID IS NULL THEN 'N/A'
							ELSE LTRIM(RTRIM(SourceCampaignID))
						END UpdatedCampaignId
				FROM	#Accounts
			) u
	WHERE	CampaignId <> UpdatedCampaignId

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Keywords', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		Keywords = UpdatedKeywords
	FROM	(	SELECT	Keywords,
						CASE 
							WHEN LTRIM(RTRIM(SourceKeywords)) = '' THEN 'N/A'
							WHEN SourceRefURL IS NULL THEN 'N/A'
							ELSE LTRIM(RTRIM(SourceKeywords))
						END UpdatedKeywords
				FROM	#Accounts
			) u
	WHERE	Keywords <> UpdatedKeywords
	
	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		TrafficSourceId = CONVERT(char(32),HASHBYTES('MD5',u.BTag2+'|'+u.AffiliateID+'|'+u.AffiliateName+'|'+u.SiteID+'|'+u.TrafficSource+'|'+u.RefURL+'|'+u.CampaignID+'|'+u.Keywords),2),
			BTag2 = u.BTag2,
			AffiliateId = u.AffiliateId,
			AffiliateName = u.AffiliateName,
			SiteID = u.SiteID,
			TrafficSource = u.TrafficSource,
			RefURL = u.RefURL,
			CampaignId = u.CampaignId,
			Keywords = u.Keywords,
			CleansedDate = GETDATE(),
			CleansedBatchKey = @BatchKey,
			CleansedBy = @Me
	FROM	[$(Dimensional)].Dimension.Account a
			INNER JOIN #Accounts u ON u.AccountKey = a.AccountKey
	WHERE	a.BTag2 <> u.BTag2
			OR a.AffiliateId <> u.AffiliateId
			OR a.AffiliateName <> u.AffiliateName
			OR a.SiteID <> u.SiteID
			OR a.TrafficSource <> u.TrafficSource
			OR a.RefURL <> u.RefURL
			OR a.CampaignId <> u.CampaignId
			OR a.Keywords <> u.Keywords

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
