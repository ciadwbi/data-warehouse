﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 12/11/2014
-- Description:	Insert Event Names, Round Names, Competition Names for 'Australian Rules' event type
-- =============================================
CREATE PROCEDURE [Cleanse].[SP_Event_AustralianRules] 
	-- Add the parameters for the stored procedure here
	@BatchKey int
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN
BEGIN TRY

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes

-- ================================================================================================================================================================
  --                        PART 1: Extract Event Names in Event Dimension for 'Australian Rules' event type
-- ================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 1','Start'

Update	[$(Dimensional)].[Dimension].[Event]
set		[Event]= (CASE 
							when (SourceEvent like '%Test%' or isnull(ParentEvent,'')  like '%Test%'or isnull(GrandParentEvent,'')  like '%Test%') 
							then 'Test'
							WHEN (isnull(ParentEvent,'') like '%Brownlow%' or isnull(ParentEvent,'') like '%Brownlow%' or isnull(ParentEvent,'') like '%Brownlow%' )
							THEN 'Brownlow Medal'
							WHEN (isnull(ParentEvent,'') like '%Norm Smith%' or isnull(ParentEvent,'') like '%Norm Smith%' or isnull(ParentEvent,'') like '%Norm Smith%' )
							THEN 'Norm Smith Medal'
							WHEN (isnull(ParentEvent,'') like '%Marcus Ashcroft%' or isnull(ParentEvent,'') like '%Marcus Ashcroft%' or isnull(ParentEvent,'') like '%Marcus Ashcroft%' )
							THEN 'Marcus Ashcroft Medal'
							WHEN (isnull(ParentEvent,'') like '%Ross Glendinning%' or isnull(ParentEvent,'') like '%Ross Glendinning%' or isnull(ParentEvent,'') like '%Ross Glendinning%' )
							THEN 'Ross Glendinning Medal'
							WHEN (isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '% @ %' )
							THEN 
								(CASE 
									WHEN isnull(ParentEvent,'') like'% - %'			
									THEN 
										(CASE
										WHEN ((SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '% vs %') or
											(SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '% v %') or
											(SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '% @ %') )
										THEN SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent))
										ELSE SUBString(ParentEvent, CHARINDEX(' - ',ParentEvent)+3,LEN(ParentEvent))
										END)
									ELSE isnull(ParentEvent,'')
								END)
							WHEN (SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '% @ %' )
							THEN 
								(CASE 
									WHEN SourceEvent like'% - %'			
									THEN 
										(CASE
										WHEN ((SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% vs %') or
											(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% v %') or
											(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% @ %') )
										THEN SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent))
										ELSE SUBString(SourceEvent, CHARINDEX(' - ',SourceEvent)+3,LEN(SourceEvent))
										END)
									ELSE SourceEvent
								END)
							ELSE 'NA'
							END) 
where	sourceeventtype='Australian Rules' 
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))


update	[$(Dimensional)].[Dimension].[Event]
set		[Event]=(Case
					When ([Event] like '% Winner%' or [Event] like '%Top %' or [Event] like '%Player %' 
					 or [Event] like '% Win %' or [Event] like '% Win' or [Event] like '%see %')
					 Then 'NA'
					 Else [Event]
				End)
where	SourceEventType='Australian Rules' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
		
--Removing extra spaces in the beginning and ending		

update	[$(Dimensional)].[Dimension].[Event]
set		[Event]=RTRIM(LTRIM([Event]))					
where	sourceeventtype='Australian Rules' 
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))		

-- ================================================================================================================================================================
  --                        PART 2: Extract Round Names in Event Dimension for 'Australian Rules' event type
-- ================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 2','Start'

Update	[$(Dimensional)].[Dimension].[Event]
set		[Round]=(CASE --Setting round Name as NA when the event is test type or AFL NAB Challenge or Foxtel Cup events as they don't have rounds
					when (SourceEvent like '%Test%' or isnull(ParentEvent,'')  like '%Test%'or isnull(GrandParentEvent,'')  like '%Test%')
					THEN 'Test'
					WHEN((SourceEvent like '%Challenge%' and SourceEvent like '%AFL%') or (isnull(ParentEvent,'')  like '%Challenge%' and isnull(ParentEvent,'')  like '%AFL%') 
					or (isnull(GrandParentEvent,'')  like '%Challenge%' and isnull(GrandParentEvent,'')  like '%AFL%') or 
					SourceEvent like '%Foxtel cup%' or isnull(ParentEvent,'')  like '%Foxtel cup%'or isnull(GrandParentEvent,'')  like '%Foxtel cup%')
					then 'NA'
					WHEN(SourceEvent like '%Brownlow%' or isnull(ParentEvent,'')  like '%Brownlow%'or isnull(GrandParentEvent,'')  like '%Brownlow%')
					THEN 'Season'
					WHEN(SourceEvent like '%Norm Smith%' or isnull(ParentEvent,'')  like '%Norm Smith%'or isnull(GrandParentEvent,'')  like '%Norm Smith%')
					THEN 'Grand Final'
					When(isnull(SourceEvent,'')  like '%Team1%Team2%' or isnull(ParentEvent,'')  like '%Team1%Team2%'or 
							isnull(GrandParentEvent,'')  like '%Team1%Team2%')
					THEN 'NA'
					WHEN (isnull(GrandParentEvent,'') like '%Round%[1-9]%' or isnull(GrandParentEvent,'') like '%week%[1-9]%' 
					or isnull(GrandParentEvent,'') like '%Final%' or isnull(GrandParentEvent,'') like '%[1-9][a-z][a-z] Round%'
					 or isnull(GrandParentEvent,'') like '%game%[1-9]%' )
					THEN 
						(CASE 
							WHen (GrandParentEvent like'%Preliminary Final%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Preliminary Final%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%Quarter Final%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Quarter Final%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%Semi%Final%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Semi%Final%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%Grand Final%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Grand %Final%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%Final%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Final%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like '%[1-9][a-z][a-z] Round%')	
							Then SUBString(GrandParentEvent, PATINDEX('%[1-9][a-z][a-z] Round%',GrandParentEvent),9)
							WHen (GrandParentEvent like'%Round%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Round%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%Week%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Week%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%game%')	
							Then SUBString(GrandParentEvent, PATINDEX('%game%',GrandParentEvent),Len(GrandParentEvent))
							ELSE 'check'
						END)
					WHEN (isnull(ParentEvent,'') like '%Round%[1-9]%' or isnull(ParentEvent,'') like '%week%[1-9]%' 
					or isnull(ParentEvent,'') like '%Final%' or isnull(ParentEvent,'') like '%[1-9][a-z][a-z] Round%'
					 or isnull(ParentEvent,'') like '%game%[1-9]%' )
					THEN 
						(CASE 
							WHen (ParentEvent like'%Semi%Final%')	
							Then SUBString(ParentEvent, PATINDEX('%Semi%Final%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%Preliminary%Final%')	
							Then SUBString(ParentEvent, PATINDEX('%Preliminary%Final%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%Quarter%Final%')	
							Then SUBString(ParentEvent, PATINDEX('%Quarter%Final%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%Grand%Final%')	
							Then SUBString(ParentEvent, PATINDEX('%Grand%Final%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%Final%')	
							Then SUBString(ParentEvent, PATINDEX('%Final%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%[1-9][a-z][a-z] Round%')	
							Then SUBString(ParentEvent, PATINDEX('%[1-9][a-z][a-z] Round%',ParentEvent),9)
							WHen (ParentEvent like'%Round%')	
							Then SUBString(ParentEvent, PATINDEX('%Round%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%Week%')	
							Then SUBString(ParentEvent, PATINDEX('%Week%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%game%')	
							Then SUBString(ParentEvent, PATINDEX('%game%',ParentEvent),Len(ParentEvent))
							ELSE 'check'
						END)		
					WHEN (SourceEvent like '%Round%[1-9]%' or SourceEvent like '%week%[1-9]%' or SourceEvent like '%Final%' or 
							SourceEvent like '%[1-9][a-z][a-z] Round%' or SourceEvent like '%game%[1-9]%')
					THEN 
						(CASE 
							When (SourceEvent like'%?')	
							Then 'NA'
							WHen (SourceEvent like'%Semi%Final%')	
							Then SUBString(SourceEvent, PATINDEX('%Semi%Final%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%Preliminary%Final%')	
							Then SUBString(SourceEvent, PATINDEX('%Preliminary%Final%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%Quarter%Final%')	
							Then SUBString(SourceEvent, PATINDEX('%Quarter%Final%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%Grand%Final%')	
							Then SUBString(SourceEvent, PATINDEX('%Grand%Final%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%Final%')	
							Then SUBString(SourceEvent, PATINDEX('%Final%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%[1-9][a-z][a-z] Round%')	
							Then SUBString(SourceEvent, PATINDEX('%[1-9][a-z][a-z] Round%',SourceEvent),9)
							WHen (SourceEvent like'%Round%')	
							Then SUBString(SourceEvent, PATINDEX('%Round%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%Week%')	
							Then SUBString(SourceEvent, PATINDEX('%Week%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%game%')	
							Then SUBString(SourceEvent, PATINDEX('%game%',SourceEvent),Len(SourceEvent))
							ELSE 'check'
						END)
					ELSE (CASE
							WHEN ((SourceEvent like '% vs %') or (isnull(ParentEvent,'')  like '% vs %') or (isnull(GrandParentEvent,'')  like '% vs %')
							or (isnull(ParentEvent,'') like '% v %') or (isnull(ParentEvent,'') like '% @ %')  or (SourceEvent like '% v %') or (SourceEvent like '% @ %')
								or (isnull(GrandParentEvent,'') like '% v %') or (isnull(GrandParentEvent,'') like '% @ %'))
							THEN 'Unknown'
							ELSE 'NA'
							END)
				END)
WHERE	sourceeventtype='Australian Rules'  and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
		
--Truncating part after 'round number' and 'Final' word

Update	[$(Dimensional)].[Dimension].[Event]
set		[Round]= (CASE	
					WHEN ([Round] like '%Round%[1-9]%' and round not like '%Rounds%[1-9]%')
					Then RTRIM(SUBString([Round],1, CHARINDEX('Round',[round])+7))
					WHEN ([Round] like '%Game%[1-9]%')
					Then RTRIM(SUBString([Round],1, CHARINDEX('Game',[round])+6))
					WHEN ([Round] like '%Week%[1-9]%')
					Then RTRIM(SUBString([Round],1, CHARINDEX('Week',round)+6))
					ELSE (CASE
							WHEN [Round] like '%Final%'
							THEN RTRIM(SUBString([Round],1, CHARINDEX('Final',[Round])+5))
							ELSE [Round]
							END)
				END)
WHERE	sourceeventtype='Australian Rules' 
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))
		
Update	[$(Dimensional)].[Dimension].[Event]
set		[Round]= Replace(Replace([Round],'Matches',''),'-','')
where	([Round] like'%Matches%' or [Round] like'%-') and sourceeventtype='Australian Rules' 
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and fromdate<='2013-12-23'  and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

--Removing Year numbers if present

Update	[$(Dimensional)].[Dimension].[Event]
set		[Round]=( CASE 
					WHEN ISNUMERIC(SUBSTRING([Round],PATINDEX('% [1-2][0-9][0-9][0-9] %',[Round]),4))=1 
					THEN REPLACE([Round],SUBSTRING([Round],PATINDEX('%[1-2][0-9][0-9][0-9]%',[Round]),4),'') 
					ELSE [Round] 
				END)
WHERE	sourceeventtype='Australian Rules' 
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))
								
-- ================================================================================================================================================================
  --                        PART 3: Extract Competition s in Event Dimension for 'Australian Rules' event type
-- ================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 3','Start'

update	[$(Dimensional)].[Dimension].[Event]
		set Competition= 
			(CASE 
				WHEN (C1.CompetitionName is not null)
				THEN C1.CompetitionName
				WHEN (C2.CompetitionName is not null)
				THEN  C2.CompetitionName
				WHEN (C3.CompetitionName is not null)
				THEN  C3.CompetitionName
				WHEN (SourceEvent like '%Test%' or isnull(ParentEvent,'')  like '%Test%'
				or isnull(GrandParentEvent,'')  like '%Test%') 
				then 'Test'
				ELSE (CASE
						WHEN (isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '% @ %'
						or SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '% @ %')
						THEN 'Unknown'
						ELSE 'NA'									 
						END)									 
				END)							
from	[$(Dimensional)].[Dimension].[Event]  a
		left join Reference.EventCompMapping C1 ON 
			(a.SourceEventtype=C1.EventType and isnull(a.GrandParentEvent,'')  like C1.EventNameLike and isnull(a.GrandParentEvent,'')  not like C1.EventNameNotLike1
			and isnull(a.GrandParentEvent,'')  not like C1.EventNameNotLike2 and isnull(a.GrandParentEvent,'')  not like C1.EventNameNotLike3
			and isnull(a.GrandParentEvent,'')  not like C1.EventNameNotLike4 and isnull(a.GrandParentEvent,'')  not like C1.EventNameNotLike5)
		LEFT JOIN Reference.EventCompMapping C2	ON 
			(a.SourceEventtype=C2.EventType and isnull(ParentEvent,'')		LIKE C2.EventNameLike AND isnull(ParentEvent,'') NOT LIKE C2.EventNameNotLike1
			AND isnull(ParentEvent,'') NOT LIKE C2.EventNameNotLike2 AND isnull(ParentEvent,'') NOT LIKE C2.EventNameNotLike3
			AND isnull(ParentEvent,'') NOT LIKE C2.EventNameNotLike4 AND isnull(ParentEvent,'') NOT LIKE C2.EventNameNotLike5)
		LEFT JOIN Reference.EventCompMapping C3 ON 
			(a.SourceEventtype=C3.EventType and SourceEvent LIKE C3.EventNameLike AND SourceEvent NOT LIKE C3.EventNameNotLike1
			AND SourceEvent NOT LIKE C3.EventNameNotLike2  AND SourceEvent NOT LIKE C3.EventNameNotLike3
			AND SourceEvent NOT LIKE C3.EventNameNotLike4  AND SourceEvent NOT LIKE C3.EventNameNotLike5)
where	sourceEventType='Australian Rules'
		and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

-- ================================================================================================================================================================
  --                        PART 4: Load ModifiedBy and ModifiedDate in to Event Dimension for 'Australian Rules' event type
-- ================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 4','Start'

Update 	[$(Dimensional)].[Dimension].[Event]
set		ModifiedBy = @Me,
		ModifiedDate = CURRENT_TIMESTAMP,
		CleansedDate = GETDATE(),
		CleansedBatchKey = @BatchKey,
		CleansedBy = @Me
where	ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and SourceEventType='Australian Rules'

EXEC	[Control].Sp_Log	@BatchKey,@Me,NULL,'Success'

END TRY
BEGIN CATCH
	
	declare @Error int;
	declare @ErrorMessage varchar(max);

	-- Log the failure at the object level
    SET		@Error = ERROR_NUMBER()
    SET		@ErrorMessage = ERROR_MESSAGE()
	EXEC	[Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	Raiserror(@ErrorMessage,16,1)

END CATCH
END

