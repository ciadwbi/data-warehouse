﻿CREATE proc [Cleanse].[Sp_Account_Reward]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET @Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start'

	UPDATE	u
	SET		RewardStatusCode = UpdatedRewardStatusCode,
			RewardStatus = UpdatedRewardStatus,
			CleansedDate = GETDATE(),
			CleansedBatchKey = @BatchKey,
			CleansedBy = @Me
	FROM	(	SELECT	RewardStatusCode,
						RewardStatus,
						CASE RewardStatusId WHEN 0 THEN 'I' ELSE 'E' END UpdatedRewardStatusCode,
						CASE RewardStatusId
							WHEN 0 THEN 'Included'
							ELSE SUBSTRING(CASE RewardStatusId & 1 WHEN 0 THEN '' ELSE ' + Excluded' END
											+ CASE RewardStatusId & 2 WHEN 0 THEN '' ELSE ' + Opt-Out' END
											+ CASE RewardStatusId & 4 WHEN 0 THEN '' ELSE ' + State' END
											,4,255) -- String together the values encoded in a bitmap value
						END	UpdatedRewardStatus,
						CleansedDate,
						CleansedBatchKey,
						CleansedBy
				FROM	[$(Dimensional)].Dimension.Account
				WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)
						AND (Increment = @Increment OR @Increment = 0) 
			) u
	WHERE	RewardStatusCode <> UpdatedRewardStatusCode
			OR RewardStatus <> UpdatedRewardStatus
	OPTION (IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)
	
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
