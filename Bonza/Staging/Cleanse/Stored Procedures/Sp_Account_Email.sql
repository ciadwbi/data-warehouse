﻿CREATE proc [Cleanse].[Sp_Account_Email]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET @Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start'

	UPDATE	u
	SET		Email = UpdatedEmail,
			EmailLastChanged = UpdatedEmailLastChanged,
			CleansedDate = GETDATE(),
			CleansedBatchKey = @BatchKey,
			CleansedBy = @Me
	FROM	(	SELECT	Email,		
						EmailLastChanged,
						CASE	
							WHEN REPLACE(HomeEmail,' ','') LIKE '%nomail%' THEN 'N/A'
							WHEN HomeEmail LIKE '%_@_%_.__%' THEN LOWER(HomeEmail)
							WHEN HomeAlternateEmail LIKE '%_@_%_.__%' THEN LOWER(HomeAlternateEmail)
							WHEN PostalEmail LIKE '%_@_%_.__%' THEN LOWER(PostalEmail)
							WHEN PostalAlternateEmail LIKE '%_@_%_.__%' THEN LOWER(PostalAlternateEmail)
							WHEN BusinessEmail LIKE '%_@_%_.__%' THEN LOWER(BusinessEmail)
							WHEN BusinessAlternateEmail LIKE '%_@_%_.__%' THEN LOWER(BusinessAlternateEmail)
							WHEN OtherEmail LIKE '%_@_%_.__%' THEN LOWER(OtherEmail)
							WHEN OtherAlternateEmail LIKE '%_@_%_.__%' THEN LOWER(OtherAlternateEmail)
							ELSE 'Unknown'
						END UpdatedEmail,
						CASE
							WHEN HomeEmailLastChanged >= ISNULL(PostalEmailLastChanged,CONVERT(datetime2(3),'1900-01-01'))
								AND HomeEmailLastChanged >= ISNULL(BusinessEmailLastChanged,CONVERT(datetime2(3),'1900-01-01'))
								AND HomeEmailLastChanged >= ISNULL(OtherEmailLastChanged,CONVERT(datetime2(3),'1900-01-01'))
								THEN HomeEmailLastChanged
							WHEN PostalEmailLastChanged >= ISNULL(BusinessEmailLastChanged,CONVERT(datetime2(3),'1900-01-01'))
								AND PostalEmailLastChanged >= ISNULL(OtherEmailLastChanged,CONVERT(datetime2(3),'1900-01-01'))
								THEN PostalEmailLastChanged
							WHEN BusinessEmailLastChanged >= ISNULL(OtherEmailLastChanged,CONVERT(datetime2(3),'1900-01-01'))
								THEN BusinessEmailLastChanged
							WHEN OtherEmailLastChanged IS NOT NULL
								THEN OtherEmailLastChanged
							ELSE CONVERT(datetime2(3),NULL)
						END UpdatedEmailLastChanged,
						CleansedDate,
						CleansedBatchKey,
						CleansedBy
				FROM	[$(Dimensional)].Dimension.Account
				WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)
						AND (Increment = @Increment OR @Increment = 0) 
			) u
	WHERE	CONVERT(varbinary,ISNULL(Email,'!')) <> CONVERT(varbinary,ISNULL(UpdatedEmail,'!')) -- Case sensitive and more generic than nominating collations!
			OR ISNULL(EmailLastChanged,CONVERT(datetime2(3),'1900-01-01')) <> ISNULL(UpdatedEmailLastChanged,CONVERT(datetime2(3),'1900-01-01'))
	OPTION (IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)
	
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
