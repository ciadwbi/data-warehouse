﻿CREATE PROCEDURE [Cleanse].[Sp_Account_SnowflakeKeys]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RemoveOrphans int = 0,		-- Periodic process to deal with snowflake orphans
	@RowsProcessed	int = 0 OUTPUT

) AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0)						-- Interpret Nulls passed as parameters the same as zeroes
	SET @Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	BEGIN TRY DROP TABLE #cache END TRY BEGIN CATCH END CATCH	-- Pave the way

BEGIN TRY

	EXEC		[Control].Sp_Log @BatchKey, @Me, 'Cache', 'Start'

	SELECT		* 
	INTO		#Cache 
	FROM		(SELECT	MAX(Account_LedgerKey) High_LedgerKey FROM [$(Dimensional)].[Snowflake].[Account_Ledger]) le
				CROSS APPLY	(SELECT	MAX(Account_LedgerTypeKey) High_LedgerTypeKey FROM [$(Dimensional)].[Snowflake].[Account_LedgerType]) lt
				CROSS APPLY	(SELECT	MAX(Account_AccountKey) High_AccountKey FROM [$(Dimensional)].[Snowflake].[Account_Account]) ac
				CROSS APPLY	(SELECT	MAX(Account_AccountTypeKey) High_AccountTypeKey FROM [$(Dimensional)].[Snowflake].[Account_AccountType]) ay
				CROSS APPLY (SELECT	MAX(Account_ManagedByKey) High_ManagedByKey FROM [$(Dimensional)].[Snowflake].[Account_ManagedBy]) ma
				CROSS APPLY	(SELECT	MAX(Account_TrafficSourceKey) High_TrafficSourceKey FROM [$(Dimensional)].[Snowflake].[Account_TrafficSource]) tr
				CROSS APPLY	(SELECT	MAX(Account_StatementKey) High_StatementKey FROM [$(Dimensional)].[Snowflake].[Account_Statement]) sm
				CROSS APPLY	(SELECT	MAX(Account_CustomerKey) High_CustomerKey FROM [$(Dimensional)].[Snowflake].[Account_Customer]) cu
				CROSS APPLY	(SELECT	MAX(Account_StateKey) High_StateKey FROM [$(Dimensional)].[Snowflake].[Account_State]) st
				CROSS APPLY	(SELECT	MAX(Account_CountryKey) High_CountryKey FROM [$(Dimensional)].[Snowflake].[Account_Country]) co
				CROSS APPLY (SELECT	AccountKey,
									Account_LedgerKey, LedgerID, LedgerSource, 
									Account_LedgerTypeKey, LedgerTypeID, 
									Account_AccountKey, AccountID, AccountSource, 
									Account_AccountTypeKey, AccountTypeId, 
									Account_ManagedByKey, ManagedByID, 
									Account_TrafficSourceKey, TrafficSourceID,
									Account_StatementKey, StatementID, 
									Account_CustomerKey, CustomerID, 
									Account_StateKey, StateID, 
									Account_CountryKey, CountryID
							FROM	[$(Dimensional)].[Dimension].[Account]
							WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0) AND (Increment = @Increment OR @Increment = 0) 
	) a

	EXEC	[Control].Sp_Log @BatchKey,@Me, 'LedgerKey', 'Start', @RowsProcessed = @@ROWCOUNT 

	UPDATE	a
	SET		Account_LedgerKey = u.UpdatedAccount_LedgerKey
	FROM	(SELECT	x.LedgerId,
					x.Account_LedgerKey, 
					CASE x.Account_LedgerKey
						WHEN -2 THEN x.High_LedgerKey + DENSE_RANK() OVER (PARTITION BY x.Account_LedgerKey ORDER BY x.LedgerId)
						ELSE x.Account_LedgerKey
					END UpdatedAccount_LedgerKey	
			FROM (
					SELECT		a.LedgerId, ISNULL(MAX(ax.Account_LedgerKey),-2) Account_LedgerKey, ISNULL(a.High_LedgerKey,0) High_LedgerKey
					FROM		(SELECT DISTINCT LedgerId, Account_LedgerKey, High_LedgerKey FROM #Cache) a
					LEFT JOIN	(SELECT LedgerId, Account_LedgerKey FROM [$(Dimensional)].[Snowflake].[Account_Ledger]) ax ON ax.LedgerId = a.LedgerId
					GROUP BY	a.LedgerId, a.High_LedgerKey
				) x
			) u
			JOIN #Cache a ON a.LedgerId = u.LedgerId
	WHERE	a.Account_LedgerKey <> u.UpdatedAccount_LedgerKey
	OPTION(RECOMPILE)

	EXEC	[Control].Sp_Log @BatchKey,@Me, 'LedgerTypeKey', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		Account_LedgerTypeKey = u.UpdatedAccount_LedgerTypeKey
	FROM	(SELECT	x.LedgerTypeId,
					x.Account_LedgerTypeKey, 
					CASE x.Account_LedgerTypeKey
						WHEN -2 THEN x.High_LedgerTypeKey + DENSE_RANK() OVER (PARTITION BY x.Account_LedgerTypeKey ORDER BY x.LedgerTypeId)
						ELSE x.Account_LedgerTypeKey
					END UpdatedAccount_LedgerTypeKey	
			FROM (
					SELECT		a.LedgerTypeId, ISNULL(MAX(ax.Account_LedgerTypeKey),-2) Account_LedgerTypeKey, ISNULL(a.High_LedgerTypeKey,0) High_LedgerTypeKey
					FROM		(SELECT DISTINCT LedgerTypeId, Account_LedgerTypeKey, High_LedgerTypeKey FROM #Cache) a
					LEFT JOIN	(SELECT LedgerTypeId, Account_LedgerTypeKey FROM [$(Dimensional)].[Snowflake].[Account_LedgerType]) ax ON ax.LedgerTypeId = a.LedgerTypeId
					GROUP BY	a.LedgerTypeId, a.High_LedgerTypeKey
				) x
			) u
			JOIN #Cache a ON a.LedgerTypeId = u.LedgerTypeId
	WHERE	a.Account_LedgerTypeKey <> u.UpdatedAccount_LedgerTypeKey
	OPTION(RECOMPILE)

	EXEC	[Control].Sp_Log @BatchKey,@Me, 'AccountKey', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		Account_AccountKey = u.UpdatedAccount_AccountKey
	FROM	(SELECT	x.AccountId,
					x.Account_AccountKey, 
					CASE x.Account_AccountKey
						WHEN -2 THEN x.High_AccountKey + DENSE_RANK() OVER (PARTITION BY x.Account_AccountKey ORDER BY x.AccountId)
						ELSE x.Account_AccountKey
					END UpdatedAccount_AccountKey	
			FROM (	
					SELECT		a.AccountId, ISNULL(MAX(ax.Account_AccountKey),-2) Account_AccountKey, ISNULL(a.High_AccountKey,0) High_AccountKey
					FROM		(SELECT DISTINCT AccountId, Account_AccountKey, High_AccountKey FROM #Cache) a
					LEFT JOIN	(SELECT AccountId, Account_AccountKey FROM [$(Dimensional)].[Snowflake].[Account_Account]) ax ON ax.AccountId = a.AccountId
					GROUP BY	a.AccountId, a.High_AccountKey
				) x
			) u
			JOIN #Cache a ON a.AccountId = u.AccountId
	WHERE	a.Account_AccountKey <> u.UpdatedAccount_AccountKey
	OPTION(RECOMPILE)

	EXEC	[Control].Sp_Log @BatchKey,@Me, 'AccountTypeKey', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		Account_AccountTypeKey = u.UpdatedAccount_AccountTypeKey
	FROM	(SELECT	x.AccountTypeId,
					x.Account_AccountTypeKey, 
					CASE x.Account_AccountTypeKey
						WHEN -2 THEN x.High_AccountTypeKey + DENSE_RANK() OVER (PARTITION BY x.Account_AccountTypeKey ORDER BY x.AccountTypeId)
						ELSE x.Account_AccountTypeKey
					END UpdatedAccount_AccountTypeKey	
			FROM (
					SELECT		a.AccountTypeId, ISNULL(MAX(ax.Account_AccountTypeKey),-2) Account_AccountTypeKey, ISNULL(a.High_AccountTypeKey,0) High_AccountTypeKey
					FROM		(SELECT DISTINCT AccountTypeId, Account_AccountTypeKey, High_AccountTypeKey FROM #Cache) a
					LEFT JOIN	(SELECT AccountTypeId, Account_AccountTypeKey FROM [$(Dimensional)].[Snowflake].[Account_AccountType]) ax ON ax.AccountTypeId = a.AccountTypeId
					GROUP BY	a.AccountTypeId, a.High_AccountTypeKey
				) x
			) u
			JOIN #Cache a ON a.AccountTypeId = u.AccountTypeId
	WHERE	a.Account_AccountTypeKey <> u.UpdatedAccount_AccountTypeKey
	OPTION(RECOMPILE)

	EXEC	[Control].Sp_Log @BatchKey,@Me, 'ManagedByKey', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		Account_ManagedByKey = u.UpdatedAccount_ManagedByKey
	FROM	(SELECT	x.ManagedById,
					x.Account_ManagedByKey, 
					CASE x.Account_ManagedByKey
						WHEN -2 THEN x.High_ManagedByKey + DENSE_RANK() OVER (PARTITION BY x.Account_ManagedByKey ORDER BY x.ManagedById)
						ELSE x.Account_ManagedByKey
					END UpdatedAccount_ManagedByKey	
			FROM (	
					SELECT		a.ManagedById, ISNULL(MAX(ax.Account_ManagedByKey),-2) Account_ManagedByKey, ISNULL(a.High_ManagedByKey,0) High_ManagedByKey
					FROM		(SELECT DISTINCT ManagedById, Account_ManagedByKey, High_ManagedByKey FROM #Cache) a
					LEFT JOIN	(SELECT ManagedById, Account_ManagedByKey FROM [$(Dimensional)].[Snowflake].[Account_ManagedBy]) ax ON ax.ManagedById = a.ManagedById
					GROUP BY	a.ManagedById, a.High_ManagedByKey
				) x
			) u
			JOIN #Cache a ON a.ManagedById = u.ManagedById
	WHERE	a.Account_ManagedByKey <> u.UpdatedAccount_ManagedByKey
	OPTION(RECOMPILE)

	EXEC	[Control].Sp_Log @BatchKey,@Me, 'TrafficSourceKey', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		Account_TrafficSourceKey = u.UpdatedAccount_TrafficSourceKey
	FROM	(SELECT	x.TrafficSourceId,
					x.Account_TrafficSourceKey, 
					CASE x.Account_TrafficSourceKey
						WHEN -2 THEN x.High_TrafficSourceKey + DENSE_RANK() OVER (PARTITION BY x.Account_TrafficSourceKey ORDER BY x.TrafficSourceId)
						ELSE x.Account_TrafficSourceKey
					END UpdatedAccount_TrafficSourceKey	
			FROM (	
					SELECT		a.TrafficSourceId, ISNULL(MAX(ax.Account_TrafficSourceKey),-2) Account_TrafficSourceKey, ISNULL(a.High_TrafficSourceKey,0) High_TrafficSourceKey
					FROM		(SELECT DISTINCT TrafficSourceId, Account_TrafficSourceKey, High_TrafficSourceKey FROM #Cache) a
					LEFT JOIN	(SELECT TrafficSourceId, Account_TrafficSourceKey FROM [$(Dimensional)].[Snowflake].[Account_TrafficSource]) ax ON ax.TrafficSourceId = a.TrafficSourceId
					GROUP BY	a.TrafficSourceId, a.High_TrafficSourceKey
				) x
			) u
			JOIN #Cache a ON a.TrafficSourceId = u.TrafficSourceId
	WHERE	a.Account_TrafficSourceKey <> u.UpdatedAccount_TrafficSourceKey
	OPTION(RECOMPILE)

	EXEC	[Control].Sp_Log @BatchKey,@Me, 'StatementKey', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		Account_StatementKey = u.UpdatedAccount_StatementKey
	FROM	(SELECT	x.StatementId,
					x.Account_StatementKey, 
					CASE x.Account_StatementKey
						WHEN -2 THEN x.High_StatementKey + DENSE_RANK() OVER (PARTITION BY x.Account_StatementKey ORDER BY x.StatementId)
						ELSE x.Account_StatementKey
					END UpdatedAccount_StatementKey	
			FROM	(
					SELECT		a.StatementId, ISNULL(MAX(ax.Account_StatementKey),-2) Account_StatementKey, ISNULL(a.High_StatementKey,0) High_StatementKey
					FROM		(SELECT DISTINCT StatementId, Account_StatementKey, High_StatementKey FROM #Cache) a
					LEFT JOIN	(SELECT StatementId, Account_StatementKey FROM [$(Dimensional)].[Snowflake].[Account_Statement]) ax ON ax.StatementId = a.StatementId
					GROUP BY	a.StatementId, a.High_StatementKey
				) x
			) u
			JOIN #Cache a ON a.StatementId = u.StatementId
	WHERE	a.Account_StatementKey <> u.UpdatedAccount_StatementKey
	OPTION(RECOMPILE)

	EXEC	[Control].Sp_Log @BatchKey,@Me, 'CustomerKey', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		Account_CustomerKey = u.UpdatedAccount_CustomerKey
	FROM	(SELECT	x.CustomerId,
					x.Account_CustomerKey, 
					CASE x.Account_CustomerKey
						WHEN -2 THEN x.High_CustomerKey + DENSE_RANK() OVER (PARTITION BY x.Account_CustomerKey ORDER BY x.CustomerId)
						ELSE x.Account_CustomerKey
					END UpdatedAccount_CustomerKey	
			FROM (
					SELECT		a.CustomerId, ISNULL(MAX(ax.Account_CustomerKey),-2) Account_CustomerKey, ISNULL(a.High_CustomerKey,0) High_CustomerKey
					FROM		(SELECT DISTINCT CustomerId, Account_CustomerKey, High_CustomerKey FROM #Cache) a
					LEFT JOIN	(SELECT CustomerId, Account_CustomerKey FROM [$(Dimensional)].[Snowflake].[Account_Customer]) ax ON ax.CustomerId = a.CustomerId
					GROUP BY	a.CustomerId, a.High_CustomerKey
				) x
			) u
			JOIN #Cache a ON a.CustomerId = u.CustomerId
	WHERE	a.Account_CustomerKey <> u.UpdatedAccount_CustomerKey
	OPTION(RECOMPILE)

	EXEC	[Control].Sp_Log @BatchKey,@Me, 'StateKey', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		Account_StateKey = u.UpdatedAccount_StateKey
	FROM	(SELECT	x.StateId,
					x.Account_StateKey, 
					CASE x.Account_StateKey
						WHEN -2 THEN x.High_StateKey + DENSE_RANK() OVER (PARTITION BY x.Account_StateKey ORDER BY x.StateId)
						ELSE x.Account_StateKey
					END UpdatedAccount_StateKey	
			FROM (
					SELECT		a.StateId, ISNULL(MAX(ax.Account_StateKey),-2) Account_StateKey, ISNULL(a.High_StateKey,0) High_StateKey
					FROM		(SELECT DISTINCT StateId, Account_StateKey, High_StateKey FROM #Cache) a
					LEFT JOIN	(SELECT StateId, Account_StateKey FROM [$(Dimensional)].[Snowflake].[Account_State]) ax ON ax.StateId = a.StateId
					GROUP BY	a.StateId, a.High_StateKey
				) x
			) u
			JOIN #Cache a ON a.StateId = u.StateId
	WHERE	a.Account_StateKey <> u.UpdatedAccount_StateKey
	OPTION(RECOMPILE)

	EXEC	[Control].Sp_Log @BatchKey,@Me, 'CountryKey', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	a
	SET		Account_CountryKey = u.UpdatedAccount_CountryKey
	FROM	(SELECT	x.CountryId,
					x.Account_CountryKey, 
					CASE x.Account_CountryKey
						WHEN -2 THEN x.High_CountryKey + DENSE_RANK() OVER (PARTITION BY x.Account_CountryKey ORDER BY x.CountryId)
						ELSE x.Account_CountryKey
					END UpdatedAccount_CountryKey	
			FROM	(
					SELECT		a.CountryId, ISNULL(MAX(ax.Account_CountryKey),-2) Account_CountryKey, ISNULL(a.High_CountryKey,0) High_CountryKey
					FROM		(SELECT DISTINCT CountryId, Account_CountryKey, High_CountryKey FROM #Cache) a
					LEFT JOIN	(SELECT CountryId, Account_CountryKey FROM [$(Dimensional)].[Snowflake].[Account_Country]) ax ON ax.CountryId = a.CountryId
					GROUP BY	a.CountryId, a.High_CountryKey
				) x
			) u
			JOIN #Cache a ON a.CountryId = u.CountryId
	WHERE	a.Account_CountryKey <> u.UpdatedAccount_CountryKey
	OPTION(RECOMPILE)

	-- final

	UPDATE	a
	SET		Account_LedgerKey = c.Account_LedgerKey,
			Account_LedgerTypeKey = c.Account_LedgerTypeKey,
			Account_AccountKey = c.Account_AccountKey,
			Account_AccountTypeKey = c.Account_AccountTypeKey,
			Account_ManagedByKey = c.Account_ManagedByKey,
			Account_TrafficSourceKey = c.Account_TrafficSourceKey, 
			Account_StatementKey = c.Account_StatementKey,
			Account_CustomerKey = c.Account_CustomerKey, 
			Account_StateKey = c.Account_StateKey,
			Account_CountryKey = c.Account_CountryKey,
			CleansedDate = CURRENT_TIMESTAMP,
			CleansedBatchKey = @BatchKey, 
			CleansedBy = @Me
	FROM	[$(Dimensional)].[Dimension].[Account] a
	JOIN	#Cache c ON c.AccountKey = a.AccountKey
	WHERE	a.Account_LedgerKey <> c.Account_LedgerKey
			OR a.Account_LedgerTypeKey <> c.Account_LedgerTypeKey
			OR a.Account_AccountKey <> c.Account_AccountKey
			OR a.Account_AccountTypeKey <> c.Account_AccountTypeKey
			OR a.Account_ManagedByKey <> c.Account_ManagedByKey
			OR a.Account_TrafficSourceKey <> c.Account_TrafficSourceKey
			OR a.Account_StatementKey <> c.Account_StatementKey
			OR a.Account_CustomerKey <> c.Account_CustomerKey
			OR a.Account_StateKey <> c.Account_StateKey
			OR a.Account_CountryKey <> c.Account_CountryKey
	OPTION(RECOMPILE)
			
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC	[Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC	[Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH