CREATE proc [Cleanse].[Sp_Instrument_Moneybookers]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount INT;

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey, @Me, 'Cache', 'Start'

	SELECT	InstrumentKey, InstrumentID		
	INTO	#Instrument 
	FROM	[$(Dimensional)].Dimension.Instrument I
	WHERE	ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END AND Source ='MBK'

	EXEC [Control].Sp_Log @BatchKey, @Me, 'Instrument ID', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		InstrumentID= UpdatedInstrumentID
	FROM	(	SELECT	InstrumentID,
				CASE  
					WHEN REPLACE(InstrumentID,' ','') LIKE '%nomail%' THEN 'N/A'
					WHEN REPLACE(InstrumentID,' ','') LIKE '%_@_%_.__%'  THEN  LOWER(REPLACE(InstrumentID,' ',''))
					ELSE 'Unknown'
					END UpdatedInstrumentID
				FROM	#Instrument
			) u
	WHERE	CONVERT(varbinary,ISNULL(InstrumentID,'!')) <> CONVERT(varbinary,ISNULL(UpdatedInstrumentID,'!'))
	
	EXEC [Control].Sp_Log @BatchKey, @Me, 'Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	i
	SET		InstrumentID = u.InstrumentID,
			CleansedDate = GETDATE(),
			CleansedBatchKey = @BatchKey,
			CleansedBy = @Me
	FROM	[$(Dimensional)].Dimension.Instrument i
			INNER JOIN #Instrument u ON u.InstrumentKey = i.InstrumentKey
	WHERE	ISNULL(i.InstrumentID,'!') <> ISNULL(u.InstrumentID,'!')

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
