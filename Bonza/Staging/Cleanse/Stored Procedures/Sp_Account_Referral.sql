﻿CREATE proc [Cleanse].[Sp_Account_Referral]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET @Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start'

	UPDATE	u
	SET		ReferralAccountKey = UpdatedReferralAccountKey,
			CleansedDate = GETDATE(),
			CleansedBatchKey = @BatchKey,
			CleansedBy = @Me
	FROM	(	SELECT	a.ReferralAccountKey,
						a1.AccountKey UpdatedReferralAccountKey,
						a.CleansedDate,
						a.CleansedBatchKey,
						a.CleansedBy
				FROM	[$(Dimensional)].Dimension.Account a
						INNER JOIN [$(Dimensional)].Dimension.Account a1 ON a1.AccountID = a.ReferralAccountID AND a1.AccountSource = a.ReferralAccountSource
				WHERE	(a.ModifiedBatchKey = @BatchKey OR @BatchKey = 0)
						AND (a.Increment = @Increment OR @Increment = 0) 
			) u
	WHERE	ReferralAccountKey <> UpdatedReferralAccountKey
	OPTION (IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)
	
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
