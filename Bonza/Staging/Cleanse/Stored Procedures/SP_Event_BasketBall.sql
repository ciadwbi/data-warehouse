﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 14/11/2014
-- Description:	Event Names,RoundNames,Competition Names for 'Basketball-US', 'Basketball-Europe', 'Basketball-AUS', 'Basketball-World
-- =============================================

CREATE PROCEDURE [Cleanse].[SP_Event_BasketBall] 
	-- Add the parameters for the stored procedure here
	@BatchKey int 
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN
BEGIN TRY

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes

--======================================================================================================================================================================   
--                                                PART 1: Insert Competition Names in to Event Dimension for all 'Basketball' event types
--======================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 1','Start'

Update	[$(Dimensional)].Dimension.Event
set		competition=(Case
					When(isnull(GrandParentEvent,'') like '%Test%' or isnull(ParentEvent,'') like '%Test%' or SourceEvent like '%Test%')
					Then 'Test'
					when(GrandParentEvent is not null)
					then (Case
							when (GrandParentEvent like '%Live Betting%' or GrandParentEvent like '% vs %' or GrandParentEvent like '% vs.%' 
							or GrandParentEvent like '% v %' or GrandParentEvent like '%@%' or GrandParentEvent like '% versus%'  or
								GrandParentEvent like '% at %' or GrandParentEvent like '%Total%')
							then 'Unknown'
							When (GrandParentEvent like '%[%]%'  or GrandParentEvent like '%which%' or GrandParentEvent like '%how%' or
								GrandParentEvent like '%will%' or GrandParentEvent like '%who%' or GrandParentEvent like '%what%' or 
								GrandParentEvent like '%first%' or GrandParentEvent like '%new master%'  or GrandParentEvent like '%top %' 
							or GrandParentEvent like '%template%' or GrandParentEvent like '%scoring%'  or GrandParentEvent like '%draft%')
							then 'NA'
							when (GrandParentEvent like '%game #[1-9]%')
							then Substring(GrandParentEvent,1,Patindex('%game #%',GrandParentEvent)-2)
							when (GrandParentEvent like '%2nd Half Wagering%') and Patindex('%2nd Half Wagering%',GrandParentEvent) > 2
							then Substring(GrandParentEvent,1,Patindex('%2nd Half Wagering%',GrandParentEvent)-2)
							when (GrandParentEvent like '%2nd Half Wagering%') and Patindex('%2nd Half Wagering%',GrandParentEvent) <= 2
							then 'Unknown'
							when (GrandParentEvent like '%[1-9]/[1-9]/[1-9][0-9][0-9][0-9]%' or GrandParentEvent like '%[1-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%' or
								GrandParentEvent like '%[0-9][0-9]/[0-9]/[1-9][0-9][0-9][0-9]%' or GrandParentEvent like '%[0-9][0-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%'
								or GrandParentEvent like '%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%')
							then Substring(GrandParentEvent,1,Patindex('%[0-9]/%',GrandParentEvent)-3)
							When (GrandParentEvent like '%Quarter%final%')
							then Substring(GrandParentEvent,1,Patindex('%Quarter%final%',GrandParentEvent)-1)
							When (GrandParentEvent like '%Semi%final%')
							then Substring(GrandParentEvent,1,Patindex('%Semi%final%',GrandParentEvent)-1)
							When (GrandParentEvent like '%preliminary%final%')
							then Substring(GrandParentEvent,1,Patindex('%preliminary%final%',GrandParentEvent)-1)
							When (GrandParentEvent like '%final%')
							then Substring(GrandParentEvent,1,Patindex('%final%',GrandParentEvent)-1)
							When (GrandParentEvent like '%2nd Halves%')
							then Substring(GrandParentEvent,1,Patindex('%2nd Halves%',GrandParentEvent)-2)
							When (GrandParentEvent like '%[1-9][a-z[a-z] Round%')
							then Substring(GrandParentEvent,1,Patindex('%[1-9][a-z[a-z] Round%',GrandParentEvent)-2)
							When (GrandParentEvent like '%[1-9] Round%')
							then Substring(GrandParentEvent,1,Patindex('%[1-9] Round%',GrandParentEvent)-2)
							When (GrandParentEvent like '%Round [1-9]%')
							then Substring(GrandParentEvent,1,Patindex('%Round [1-9]%',GrandParentEvent)-2)
							when (GrandParentEvent like '%|%')
							then Substring(GrandParentEvent,1,Patindex('%|%',GrandParentEvent)-1)
							when (GrandParentEvent like '%Highest%')
							then Substring(GrandParentEvent,1,Patindex('%Highest%',GrandParentEvent)-1)
							when (GrandParentEvent like '%Regular%' or GrandParentEvent like '%Reg %')
							then Substring(GrandParentEvent,1,Patindex('%Reg%',GrandParentEvent)-1)
							else GrandParentEvent
						End)
					when(ParentEvent is not null)
					then (Case
							when (ParentEvent like '%Live Betting%' or ParentEvent like '% vs %' or ParentEvent like '% vs.%'
							or ParentEvent like '% v %' or ParentEvent like '%@%' or ParentEvent like '% versus%'or
								ParentEvent like '% at %' or ParentEvent like '%Total%')
							then 'Unknown'
							When (ParentEvent like '%[%]%'  or ParentEvent like '%which%' or ParentEvent like '%how%' or ParentEvent like '%will%' or 
							ParentEvent like '%who%' or ParentEvent like '%what%' or ParentEvent like '%first%' or ParentEvent like '%new master%' 
							or ParentEvent like '%template%' or ParentEvent like '%scoring%'  or ParentEvent like '%draft%' or ParentEvent like '%top%')
							then 'NA'
							when (ParentEvent like '%game #[1-9]%')
							then Substring(ParentEvent,1,Patindex('%game #%',ParentEvent)-2)
							when (ParentEvent like '%2nd Half Wagering%')
							then Substring(ParentEvent,1,Patindex('%2nd Half Wagering%',ParentEvent)-2)
							when (ParentEvent like '%[1-9]/[1-9]/[1-9][0-9][0-9][0-9]%' or ParentEvent like '%[1-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%' or
								ParentEvent like '%[0-9][0-9]/[0-9]/[0-9][0-9][0-9][0-9]%' or ParentEvent like '%[0-9][0-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%'
								or ParentEvent like '%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%')
							then Substring(ParentEvent,1,Patindex('%/%',ParentEvent)-3)
							When (ParentEvent like '%Quarter%final%')
							then Substring(ParentEvent,1,Patindex('%Quarter%final%',ParentEvent)-2)
							When (ParentEvent like '%Preliminary%final%')
							then Substring(ParentEvent,1,Patindex('%Preliminary%final%',ParentEvent)-2)
							When (ParentEvent like '%Semi%final%')
							then Substring(ParentEvent,1,Patindex('%Semi%',ParentEvent)-2)
							When (ParentEvent like '% final%')
							then Substring(ParentEvent,1,Patindex('% final%',ParentEvent)-1)
							When (ParentEvent like '%2nd Halves%')
							then Substring(ParentEvent,1,Patindex('%2nd Halves%',ParentEvent)-2)
							When (ParentEvent like '%[1-9][a-z[a-z] Round%' or ParentEvent like '%[1-9][a-z[a-z] Round%')
							then Substring(ParentEvent,1,Patindex('%[1-9][a-z[a-z] %',ParentEvent)-2)
							When (ParentEvent like '%[1-9] Round%')
							then Substring(ParentEvent,1,Patindex('%[1-9] Round%',ParentEvent)-2)
							When (ParentEvent like '%Round [1-9]%')
							then Substring(ParentEvent,1,Patindex('%Round [1-9]%',ParentEvent)-2)
							when (ParentEvent like '%|%')
							then Substring(ParentEvent,1,Patindex('%|%',ParentEvent)-1)
							when (ParentEvent like '%Highest%')
							then Substring(ParentEvent,1,Patindex('%Highest%',ParentEvent)-1)
							when (ParentEvent like '%Regular%' or ParentEvent like '%Reg %')
							then Substring(ParentEvent,1,Patindex('%Reg%',ParentEvent)-1)
							Else ParentEvent
						End)

					when (SourceEvent like '% vs %' or SourceEvent like '%@%' or SourceEvent like '% at %' or SourceEvent like '%Total%'
					or SourceEvent like '% v %' or SourceEvent like '% vs.%'or SourceEvent like '% versus%')
					then 'Unknown'
					When (SourceEvent like '%[%]%' or SourceEvent like '%which%' or SourceEvent like '%how%' or SourceEvent like '%will%' or 
					SourceEvent like '%who%' or SourceEvent like '%what%' or SourceEvent like '%first%' or SourceEvent like '%new master%' 
					or SourceEvent like '%template%' or SourceEvent like '%scoring%'  or SourceEvent like '%draft%' or SourceEvent like '%top%')
					then 'NA'
					when (SourceEvent like '%game #[1-9]%')
					then Substring(SourceEvent,1,Patindex('%game #%',SourceEvent)-2)
					when (SourceEvent like '%2nd Half Wagering%')
					then Substring(SourceEvent,1,Patindex('%2nd Half Wagering%',SourceEvent)-2)
					when (SourceEvent like '%[1-9]/[1-9]/[1-9][0-9][0-9][0-9]%' or SourceEvent like '%[1-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%' or
						SourceEvent like '%[0-9][0-9]/[0-9]/[1-9][0-9][0-9][0-9]%' or SourceEvent like '%[0-9][0-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%'
							or SourceEvent like '%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%')
					then Substring(SourceEvent,1,Patindex('%/%',SourceEvent)-3)
					when (SourceEvent like '%|%')
					then Substring(SourceEvent,1,Patindex('%|%',SourceEvent)-1)
					when (SourceEvent like '%Highest %')
					then Substring(SourceEvent,1,Patindex('%Highest%',SourceEvent)-1)
					when (SourceEvent like '%Regular %' or SourceEvent like '%Reg %')
					then Substring(SourceEvent,1,Patindex('%Reg%',SourceEvent)-1)
				else SourceEvent
			end)
where	SourceEventType in ('Basketball - US','Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Removing Dates from Competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition=Substring(Competition,1,Patindex('%[0-9]/%',Competition)-3)
where	(Competition like '%[1-9]/[1-9]/[1-9][0-9][0-9][0-9]%' or Competition like '%[1-9]/[1-9]/[0-9][0-9]%' 
			or Competition like '%[1-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%' or Competition like '%[1-9]/[0-9][0-9]/[0-9][0-9]%'
			or Competition like '%[0-9][0-9]/[0-9]/[1-9][0-9][0-9][0-9]%' or Competition like '%[0-9][0-9]/[0-9]/[0-9][0-9]%'
			or Competition like '%[0-9][0-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%' or Competition like '%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%'
		) 
			and SourceEventType in ('Basketball - US','Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) 
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Removing unnecessary information from Competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition=(Case 
						When Competition like '%[1-9][a-z][a-z]%'
						Then Substring(Competition,1,Patindex('%[1-9][a-z][a-z]%',Competition)-1)
						When Competition like '%first %'
						Then Substring(Competition,1,Patindex('%first %',Competition)-1)
						When Competition like 'first %'
						Then ''
						When Competition like '%Second %'
						Then Substring(Competition,1,Patindex('%Second %',Competition)-1)
						When Competition like '%Third %'
						Then Substring(Competition,1,Patindex('%Third %',Competition)-1)
						When Competition like '%Fourth %'
						Then Substring(Competition,1,Patindex('%Fourth %',Competition)-1)
						When Competition like '%Highest %'
						Then Substring(Competition,1,Patindex('%Highest %',Competition)-1)
						When Competition like '%Round %'
						Then Substring(Competition,1,Patindex('%Round %',Competition)-1)
						When Competition like '%Quarter%Final%'
						Then Substring(Competition,1,Patindex('%Quarter%Final%',Competition)-1)
						When Competition like '%Semi%Final%'
						Then Substring(Competition,1,Patindex('%Semi%Final%',Competition)-1)
						When Competition like '%Preliminary%Final%'
						Then Substring(Competition,1,Patindex('%Preliminary%Final%',Competition)-1)
						When Competition like '%Final%'
						Then Substring(Competition,1,Patindex('%Final%',Competition)-1)
						When Competition like '%qualifier%'
						Then Substring(Competition,1,Patindex('%qualifier%',Competition)-1)
						When Competition like '%exact %'
						Then Substring(Competition,1,Patindex('%exact %',Competition)-1)
						When Competition like '%week %'
						Then Substring(Competition,1,Patindex('%week %',Competition)-1)
						When Competition like '%grand %'
						Then Substring(Competition,1,Patindex('%grand %',Competition)-1)
						When Competition like '%play%off%'
						Then Substring(Competition,1,Patindex('%play%off%',Competition)-1)
						When Competition like '%(%'
						Then Substring(Competition,1,Patindex('%(%',Competition)-1)
						When Competition like '%*%'
						Then Substring(Competition,1,Patindex('%*%',Competition)-1)
						Else Competition
					End)
where	SourceEventType in ('Basketball - US','Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) 
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update	[$(Dimensional)].Dimension.Event
set		Competition=case 
						when Competition like '%Matches%'
						then Substring(Competition,1,Patindex('%Matches%',Competition)-1)
						when Competition like '%NBA%-%'
						then 'NBA'
						when  Competition like '%Game%'
						then Substring(Competition,1,Patindex('%Game%',Competition)+4)
						when  Competition like '%wagering%' or Competition like '%(%'
						then replace(replace(Competition,'wagering',''),'(','')
						else Competition
					End
where	SourceEventType in ('Basketball - US','Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update	[$(Dimensional)].Dimension.Event
set		Competition=case when Competition like '%Winners%'
						then Ltrim(Rtrim(replace(Competition,'Winners','')))
						when Competition like '%Winner%'
						then Ltrim(Rtrim(replace(Competition,'Winner','')))
						when Competition like '%-' or Competition like '%- '
						then Ltrim(Rtrim(replace(Competition,'-','')))
						else Ltrim(Rtrim(Competition))
					End
where	SourceEventType in ('Basketball - US','Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Removing single year numbers from competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition= Substring(Competition,1,PATINDEX('%[1-9][0-9][0-9][0-9]%',Competition)-1)+Substring(Competition,PATINDEX('%[1-9][0-9][0-9][0-9]%',Competition)+4,Len(Competition))
where	SourceEventType in ('Basketball - US','Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) 
			and Competition like '%[1-9][0-9][0-9][0-9]%'
			and Competition not like '%[1-9][0-9][0-9][0-9]%/[0-9]%'
			and Competition not like '%[1-9][0-9][0-9][0-9]%-[0-9]%' 
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Removing erroneous data from competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition='Unknown'
where	SourceEventType in ('Basketball - US','Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))
			and Competition not like '% %' and Competition not in('NBA','NHL','NFl','NA','Test','unknown','WNB','CIT','NBL','euroleague','wnba','wnbl','mlb',
			 'europecup','NB','NIT','nCCA','nascar','ncaa','eurochallenge','enbl','euroleage','cbi','eurobasket','qbl','seabl','eurocup')

Update	[$(Dimensional)].Dimension.Event
set		Competition=(Case
						WHen competition=''
						Then 'unknown'
						Else Ltrim(Rtrim(Replace(competition,'  ',' ')))
					 End)
where	SourceEventType in ('Basketball - US','Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) 
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) 

--======================================================================================================================================================================   
--                PART 2: Insert Round Names in to Event Dimension for Basketball-Europe,Basketball-World,Basketball-Aus event types
--======================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 2','Start',@RowsProcessed = @@RowCount

--Extract Round s from Event s in Intrabet

update	[$(Dimensional)].Dimension.Event
set		Round=(CASE --Setting round  as NA when the event is test type or AFL NAB Challenge or Foxtel Cup events as they don't have rounds
			when (SourceEvent like '%Test%' or isnull(ParentEvent,'')  like '%Test%'or isnull(GrandParentEvent,'')  like '%Test%')
			THEN 'Test'		
			WHEN (isnull(GrandParentEvent,'') like '%Round%' or isnull(GrandParentEvent,'') like '%week%' or isnull(GrandParentEvent,'') like '%Final%' 
					or isnull(GrandParentEvent,'') like '%Quarter%'  or isnull(GrandParentEvent,'') like '%halve%')
			THEN 
				(CASE 
					WHEN (GrandParentEvent like'% - %')			
					THEN 
						(CASE
							WHEN ((SUBString(GrandParentEvent,1, CHARINDEX(' - ',GrandParentEvent)) like '%Round%') or
							(SUBString(GrandParentEvent,1, CHARINDEX(' - ',GrandParentEvent)) like '%week%') or
							(SUBString(GrandParentEvent,1, CHARINDEX(' - ',GrandParentEvent)) like '%Final%') or
							(SUBString(GrandParentEvent,1, CHARINDEX(' - ',GrandParentEvent)) like '%Quarter%') or
							(SUBString(GrandParentEvent,1, CHARINDEX(' - ',GrandParentEvent)) like '%halve%'))
							THEN SUBString(GrandParentEvent,1, CHARINDEX(' - ',GrandParentEvent))
							ELSE SUBString(GrandParentEvent, CHARINDEX(' - ',GrandParentEvent)+3,LEN(GrandParentEvent))
							END)
						ELSE GrandParentEvent
				END)
			WHEN (isnull(ParentEvent,'') like '%Round%' or isnull(ParentEvent,'') like '%week%' or isnull(ParentEvent,'') like '%Final%' 
					or isnull(ParentEvent,'') like '%Quarter%' or isnull(ParentEvent,'') like '%halve%' )
			THEN 
				(CASE 
					WHEN (ParentEvent like'% - %')			
					THEN 
						(CASE
							WHEN ((SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '%Round%') or
									(SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '%week%' and
										SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) not like '%weekend%') or
									(SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '%Final%') or
									(SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '%Quarter%') or
									(SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '%halve%'))
							THEN SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent))
							ELSE SUBString(ParentEvent, CHARINDEX(' - ',ParentEvent)+3,LEN(ParentEvent))
						END)
					ELSE ParentEvent
				END)			
			WHEN (SourceEvent like '%Round%' or SourceEvent like '%week%' or SourceEvent like '%Final%' 
			or SourceEvent like '%Quarter%' or SourceEvent like '%halve%')
			THEN 
				(CASE 
					WHEN (SourceEvent like'% - %')
					THEN 
							(CASE
							WHEN ((SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%Round%') or
									(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%week%') or
									(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%Final%') or
									(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%Quarter%') or
									(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%halve%'))
							THEN SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent))
							ELSE SUBString(SourceEvent, CHARINDEX(' - ',SourceEvent)+3,LEN(SourceEvent))
							END)
					ELSE SourceEvent
					END)
			ELSE (CASE
					WHEN (SourceEvent like '% vs %' or isnull(ParentEvent,'')  like '% vs %'or isnull(GrandParentEvent,'')  like '% vs %'
					or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '% @ %'  or SourceEvent like '% v %' or SourceEvent like '% @ %'
						or isnull(GrandParentEvent,'') like '% v %' or isnull(GrandParentEvent,'') like '% @ %')
					THEN 'Unknown'
					ELSE 'NA'
					END)
		END)
where	sourceEventtype in ('Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  
			and  (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))
	
--Truncating part after 'round number' and 'Final' word

update	[$(Dimensional)].Dimension.Event
set		Round=(CASE	
					WHEN (Round like '%Round [1-9]%' or Round like '%Round[1-9]%')
					Then RTRIM(SUBString(Round,1, CHARINDEX('Round',Round)+8))
					WHEN (Round like '%Game [1-9]%' or Round like '%Game[1-9]%')
					Then RTRIM(SUBString(Round,1, CHARINDEX('Game',Round)+7))
					WHEN (Round like '%Week [1-9]%' or Round like '%Week[1-9]%')
					Then RTRIM(SUBString(Round,1, CHARINDEX('Week',Round)+7))
					ELSE (CASE
							WHEN Round like '%Final%'
							THEN RTRIM(SUBString(Round,1, CHARINDEX('Final',Round)+5))
							ELSE Round
							END)
				END)
where	sourceEventtype in ('Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  
			and  (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))
	
--Removing Year numbers if present

update	[$(Dimensional)].Dimension.Event
set		Round=( CASE 
			WHEN ISNUMERIC(SUBSTRING(Round,PATINDEX('% [1-2][0-9][0-9][0-9] %',Round),4))=1 
			THEN REPLACE(Round,SUBSTRING(Round,PATINDEX('%[1-2][0-9][0-9][0-9]%',Round),4),'') 
			ELSE Round
		END)
where	sourceEventtype in ('Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  
			and  (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))
	
--set the round as 'NA' when Events don't relate to Rounds

update	[$(Dimensional)].Dimension.Event
set		Round='NA'
where	(SourceEvent like '%Team1 vs Team2%' or isnull(isnull(ParentEvent,''),'') like '%Team1 vs Team2%' or isnull(GrandParentEvent,'') like '%Team1 vs Team2%' or round like '% To %' )
			and sourceEventtype in ('Basketball - World','Basketball - Aus','Basketball - Europe') and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  
			and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

--Updating Round to remove Competition s

update	[$(Dimensional)].Dimension.Event
set		Round=(Case 
					when Round like'%Grand Final%'
					then Substring(Round,PATINDEX('%Grand Final%',Round),(Len(Round)+1-PATINDEX('%Grand Final%',Round)))
					when Round like'%Semi Final%'
					then Substring(Round,PATINDEX('%Semi Final%',Round),(Len(Round)+1-PATINDEX('%Semi Final%',Round)))
					when Round like'%Quarter Final%'
					then Substring(Round,PATINDEX('%Quarter Final%',Round),(Len(Round)+1-PATINDEX('%Quarter Final%',Round)))
					when Round like'%Preliminary Final%'
					then Substring(Round,PATINDEX('%Preliminary Final%',Round),(Len(Round)+1-PATINDEX('%Preliminary Final%',Round)))
					when Round like'% Final%'
					then Substring(Round,PATINDEX('%Final%',Round),(Len(Round)+1-PATINDEX('%Final%',Round)))
					when Round like'%Round%[0-9]%'
					then Substring(Round,PATINDEX('%Round%[0-9]%',Round),9)
					When (Round like '%Second%' or  Round like '%Third%' or Round like '%Fourth%' or Round like '%Highest Scoring%' or Round like '%First%' )
					then 'Unknown'
					else Round
				End)
where	sourceEventtype in ('Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
			and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

--Removing extra spaces in the beginning and ending of round s
--Inserting Round s in to Event Dimension

update	[$(Dimensional)].Dimension.Event
set		Round= RTRIM(LTRIM(round))
where	sourceEventtype in ('Basketball - World','Basketball - Aus','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) 
			and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))
	
--======================================================================================================================================================================   
--                PART 3: Insert Round Names in to Event Dimension for Basketball-us event type
--======================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 3','Start',@RowsProcessed = @@RowCount

--Extract Round s for 'Basketball - US' event type and load in to Event dimension
	
Update	[$(Dimensional)].Dimension.Event
set		Round=(Case
				When(isnull(GrandParentEvent,'') like'% Test%' or isnull(ParentEvent,'') like'% Test%'
				or isnull(SourceEvent,'') like'% Test%')
				Then 'NA'
				when (GrandParentEvent like '%game #[1-9]%')
				then Substring(GrandParentEvent,Patindex('%game #%',GrandParentEvent),8)
				when (GrandParentEvent like '%[1-9]/[1-9]/[1-9][0-9][0-9][0-9]%' or GrandParentEvent like '%[1-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%' or
					GrandParentEvent like '%[0-9][0-9]/[0-9]/[1-9][0-9][0-9][0-9]%' or GrandParentEvent like '%[0-9][0-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%'
					or GrandParentEvent like '%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%')
				then Substring(GrandParentEvent,Patindex('%[0-9]/%',GrandParentEvent)-1,Len(GrandParentEvent))
				When (GrandParentEvent like '%Quarterfinals%')
				then 'Quarter Final'
				When (GrandParentEvent like '% final%')
				then 'Final'
				When (GrandParentEvent like '%Semifinal%')
				then 'Semi Final'
				When (GrandParentEvent like '%[1-9][a-z[a-z] Round%')
				then 'Round'+Substring(GrandParentEvent,Patindex('%[1-9][a-z[a-z] Round%',GrandParentEvent),2)
				When (GrandParentEvent like '%[1-9] Round%')
				then 'Round'+Substring(GrandParentEvent,Patindex('%[1-9] Round%',GrandParentEvent),2)
				When (GrandParentEvent like '%Round [1-9]%')
				then 'Round'+Substring(GrandParentEvent,Patindex('%Round [1-9]%',GrandParentEvent)+6,2)
								
				when (ParentEvent like '%game #[1-9]%')
				then Substring(ParentEvent,Patindex('%game #%',ParentEvent),8)
				when (ParentEvent like '%[1-9]/[1-9]/[1-9][0-9][0-9][0-9]%' or ParentEvent like '%[1-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%' or
					ParentEvent like '%[0-9][0-9]/[0-9]/[1-9][0-9][0-9][0-9]%' or ParentEvent like '%[0-9][0-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%'
					or ParentEvent like '%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%')
				then Substring(ParentEvent,Patindex('%[0-9]/%',ParentEvent)-1,Len(ParentEvent))
				When (ParentEvent like '%Quarterfinals%')
				then 'Quarter Final'
				When (ParentEvent like '% final%')
				then 'Final'
				When (ParentEvent like '%Semifinal%')
				then 'Semi Final'
				When (ParentEvent like '%[1-9][a-z[a-z] Round%')
				then 'Round'+Substring(ParentEvent,Patindex('%[1-9][a-z[a-z] Round%',ParentEvent),2)
				When (ParentEvent like '%[1-9] Round%')
				then 'Round'+Substring(ParentEvent,Patindex('%[1-9] Round%',ParentEvent),2)
				When (ParentEvent like '%Round [1-9]%')
				then 'Round'+Substring(ParentEvent,Patindex('%Round [1-9]%',ParentEvent)+6,2)

				when (SourceEvent like '%game #[1-9]%')
				then Substring(SourceEvent,Patindex('%game #%',SourceEvent),8)
				when (SourceEvent like '%[1-9]/[1-9]/[1-9][0-9][0-9][0-9]%' or SourceEvent like '%[1-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%' or
					SourceEvent like '%[0-9][0-9]/[0-9]/[1-9][0-9][0-9][0-9]%' or SourceEvent like '%[0-9][0-9]/[0-9][0-9]/[1-9][0-9][0-9][0-9]%'
					or SourceEvent like '%[0-9][0-9]/[0-9][0-9]/[0-9][0-9]%')
				then Substring(SourceEvent,Patindex('%[0-9]/%',SourceEvent)-1,Len(SourceEvent))
				When (SourceEvent like '%Quarterfinals%')
				then 'Quarter Final'
				When (SourceEvent like '% final%')
				then 'Final'
				When (SourceEvent like '%Semifinal%')
				then 'Semi Final'
				When (SourceEvent like '%[1-9][a-z[a-z] Round%')
				then 'Round'+Substring(SourceEvent,Patindex('%[1-9][a-z[a-z] Round%',SourceEvent),2)
				When (SourceEvent like '%[1-9] Round%')
				then 'Round'+Substring(SourceEvent,Patindex('%[1-9] Round%',SourceEvent),2)
				When (SourceEvent like '%Round [1-9]%')
				then 'Round'+Substring(SourceEvent,Patindex('%Round [1-9]%',SourceEvent)+6,2)
				when (SourceEvent like '% vs %' or SourceEvent like '% @ %' or SourceEvent like '% at %' or
						ParentEvent like '% vs %' or ParentEvent like '% @ %' or ParentEvent like '% at %' or
						GrandParentEvent like '% vs %' or GrandParentEvent like '% @ %' or GrandParentEvent like '% at %')
				Then Cast(Format(Convert(date,SourceEventDate),'dd/MM/yyyy') as varchar)
					
				else 'Unknown'
			end)
where	SourceEventType='Basketball - US' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and  (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

Update	[$(Dimensional)].Dimension.Event
set		Round=Substring(Round,1,PATINDEX('%[1-9][a-z][a-z] half wagering%',Round)-2)
where	SourceEventType='Basketball - US' and Round like '%[1-9][a-z][a-z] half wagering%' 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
			and  (ColumnLock&Power(2,(10-1))<>Power(2,(10-1))) 

Update	[$(Dimensional)].Dimension.Event
set		Round=Ltrim(Rtrim(Round))
where	SourceEventType='Basketball - US' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and  (ColumnLock&Power(2,(10-1))<>Power(2,(10-1))) 

--======================================================================================================================================================================   
--                PART 4: Insert Event Names in to Event Dimension for Basketball-Europe,Basketball-World,Basketball-Aus,Basketball-us event types
--======================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 4','Start',@RowsProcessed = @@RowCount

--Extract Event s from Event s in Intrabet

update	[$(Dimensional)].Dimension.Event
set		Event=(CASE 
				when ((SourceEvent like '%Test%' or isnull(ParentEvent,'')  like '%Test%'or isnull(GrandParentEvent,'')  like '%Test%'))
				then 'Test'
				Else
					CASE	
						WHEN (isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '% @ %' )
						THEN 
							(CASE 
								WHEN ParentEvent like'% - %'			
								THEN 
									(CASE
									WHEN ((SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '%vs%') or
										(SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '%v%') or
										(SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent)) like '%@%') )
									THEN SUBString(ParentEvent,1, CHARINDEX(' - ',ParentEvent))
									ELSE SUBString(ParentEvent, CHARINDEX(' - ',ParentEvent)+3,LEN(ParentEvent))
									END)
									ELSE ParentEvent
								END)
							ELSE
								(CASE
									WHEN (SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '% @ %' )
									THEN 
										(CASE 
											WHEN SourceEvent like'% - %'			
											THEN 
												(CASE
												WHEN ((SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%vs%') or
													(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%v%') or
													(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%@%') )
												THEN SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent))
												ELSE SUBString(SourceEvent, CHARINDEX(' - ',SourceEvent)+3,LEN(SourceEvent))
												END)
											ELSE SourceEvent
											END)
									ELSE 'NA'
								END)
						END
			END)		
where	sourceeventtype in('Basketball - World','Basketball - US','Basketball - AUS','Basketball - Europe') 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  
			and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

-- ================================================================================================================================================================
  --                        PART 5: Update ModifiedBy and ModifiedDate in Event Dimension for 'Basketball' all event types
-- ================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 5','Start',@RowsProcessed = @@RowCount

Update	[$(Dimensional)].Dimension.Event
set		ModifiedBy='SP_Event_BasketBall',
		ModifiedDate=CURRENT_TIMESTAMP,
		CleansedDate = GETDATE(),
		CleansedBatchKey = @BatchKey,
		CleansedBy = @Me
where	ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) 
			and SourceEventType in('Basketball - World','Basketball - US','Basketball - AUS','Basketball - Europe')

EXEC	[Control].Sp_Log	@BatchKey,@Me,NULL,'Success',@RowsProcessed = @@RowCount

END TRY
BEGIN CATCH

	declare @Error int;
	declare @ErrorMessage varchar(max);

	-- Log the failure at the object level

    SET		@Error = ERROR_NUMBER()
    SET		@ErrorMessage = ERROR_MESSAGE()
	EXEC	[Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	Raiserror(@ErrorMessage,16,1)

END CATCH

End


