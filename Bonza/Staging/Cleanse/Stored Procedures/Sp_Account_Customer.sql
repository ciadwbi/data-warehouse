﻿CREATE proc [Cleanse].[Sp_Account_Customer]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET @Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start';

	-- Ultimately, the CustomerId will allow for the grouping of multiple accounts belonging to the same customer and the cleansed Name/Address/Phone/Email colums will be consolidated at the
	-- customer level rather than the current account level.
	-- However, there is no immediate priority to implement this functionality, so in the meantime, just set the CustomerId to the AccountId with no grouping

	UPDATE	[$(Dimensional)].Dimension.Account
	SET		CustomerId = AccountID,
			CustomerSource = 'DW',
			CleansedDate = GETDATE(),
			CleansedBatchKey = @BatchKey,
			CleansedBy = @Me
	WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)
			AND (Increment = @Increment OR @Increment = 0) 
			AND	(	CustomerID <> AccountId
					OR CustomerSource <> 'DW'
				)
	OPTION (IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
