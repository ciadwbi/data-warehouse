﻿CREATE PROCEDURE [Cleanse].[Sp_Account_Referral_Desktop_Mobile]
									(	@BatchKey   INT,
                                      	@DryRun bit = 1,
										@RowsProcessed  int = 0 OUTPUT 
									) 
AS 
    
	DECLARE @Me VARCHAR(256) = Object_schema_name(@@PROCID) + '.' + Object_name(@@PROCID) 
    DECLARE @RowCount INT; 

    SET @BatchKey = Isnull(@BatchKey, -1)  
    SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	SET NOCOUNT ON

  BEGIN TRY 


	  EXEC [Control].Sp_Log @BatchKey, @Me, 'Cache MDM', 'Start', @RowsProcessed = @@RowCount 
	
		SELECT	
				Replace(TrackingChannel		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingChannel,
				Replace(ChannelTypeName		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	ChannelTypeName,
				Replace(ReferralDevice		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	ReferralDevice
		INTO	#MDM_Device
		FROM	[$(MDS_Server)].[$(MDS)].[mdm].[CleanseAccount_Device]

		SET		@RowCount = @RowCount + @@RowCount

		SELECT	
				Replace(TrackingChannel		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingChannel,
				Replace(ChannelTypeName		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	ChannelTypeName,
				Replace(SourceBTag2			, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	SourceBTag2,
				Replace(Promo_Name			, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	Promo
		INTO	#MDM_Promo
		FROM	[$(MDS_Server)].[$(MDS)].[mdm].[CleanseAccount_Promo_DesktopMobile]
		
		SET		@RowCount = @RowCount + @@RowCount

		SELECT	
				Replace(TrackingChannel		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingChannel,
				Replace(ChannelTypeName		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	ChannelTypeName,
				Replace(SourceRefURL		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	SourceRefURL,
				Replace(SourceTrafficSource	, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	SourceTrafficSource,
				Replace(Channel_Name		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	Channel,
				Replace(Publisher_Name		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	Publisher
		INTO	#MDM_ChannelPublisher 
		FROM	[$(MDS_Server)].[$(MDS)].[mdm].[CleanseAccount_ChannelPublisher_DesktopMobile]
		
		SET		@RowCount = @RowCount + @@RowCount


		SELECT	
				Replace(TrackingChannel		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingChannel,
				Replace(ChannelTypeName		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	ChannelTypeName,
				Replace(SourceTrafficSource	, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	SourceTrafficSource,
				Replace(SourceCampaignID	, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	SourceCampaignID,
				Replace(Targeting			, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	Targeting
		INTO	#MDM_Targeting
		FROM	[$(MDS_Server)].[$(MDS)].[mdm].[CleanseAccount_Targeting_DesktopMobile] 
		
		SET		@RowCount = @RowCount + @@RowCount		

		SELECT	
				Replace(TrackingChannel		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingChannel,
				Replace(ChannelTypeName		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	ChannelTypeName,
				Replace(SourceTrafficSource	, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	SourceTrafficSource,
				Replace(SourceKeywords		, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	SourceKeywords,
				Replace(Specifics			, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	Specifics
		INTO	#MDM_Specifics 
		FROM	[$(MDS_Server)].[$(MDS)].[mdm].[CleanseAccount_Specifics_DesktopMobile] 
		
		SET		@RowCount = @RowCount + @@RowCount	


      EXEC [Control].Sp_Log @BatchKey,@Me, 'Prepare Promo', 'Start', @RowsProcessed = @RowCount 
	  
		SELECT
		a.AccountID,
		a.SourceSitePromoName,
		a.SourceBTag2,
		a.SourceRefURL,
		a.SourceTrafficSource,
		a.TrackingChannel,
		b.ChannelTypeName,
		a.ReferralAccountID,
		a.SourceAffiliateName,
		a.SourceCampaignID,
		a.SourceKeywords,
		a.Promo,
		a.ReferralDevice,
		a.ReferralChannel,
		a.ReferralPublisher,
		a.ReferralTargeting,
		a.ReferralSpecifics,
		CONVERT(VARCHAR(100), NULL)		Updated_Promo, 
		CONVERT(VARCHAR(20)	, NULL)		Updated_ReferralDevice, 
		CONVERT(VARCHAR(100), NULL)		Updated_ReferralChannel, 
		CONVERT(VARCHAR(100), NULL)		Updated_ReferralPublisher,
		CONVERT(VARCHAR(100), NULL)		Updated_ReferralTargeting,
		CONVERT(VARCHAR(100), NULL)		Updated_ReferralSpecifics
		INTO	#Account
		FROM	[$(Dimensional)].Dimension.Account A
		JOIN	[$(Dimensional)].Dimension.Channel B 
			ON	A.AccountOpenedChannelKey = B.ChannelKey
		WHERE	A.TrackingChannel IS NULL AND B.ChannelTypeName in ('Internet', 'Mobile')
		AND		(@BatchKey = -1 OR a.ModifiedBatchKey = @Batchkey)
		OPTION (RECOMPILE)

		EXEC [Control].Sp_Log @BatchKey,@Me, 'Clean Promo', 'Start', @RowsProcessed = @@ROWCOUNT 

			UPDATE A
			SET A.Updated_Promo =	CASE	WHEN B.Promo = '[Use SourceSitePromoName]'
												THEN ISNULL(A.SourceSitePromoName,'Unknown') 
											ELSE B.Promo 
									END 
			FROM #Account A
			LEFT JOIN #MDM_Promo B
				ON	ISNULL(A.TrackingChannel, '!')	= ISNULL(B.TrackingChannel	, '!')
				AND ISNULL(A.ChannelTypeName, '!')	= ISNULL(B.ChannelTypeName	, '!')
				AND ISNULL(A.SourceBTag2	, '!')	= ISNULL(B.SourceBTag2		, '!')
	

		EXEC [Control].Sp_Log @BatchKey,@Me, 'Clean ReferralDevice', 'Start', @RowsProcessed = @@ROWCOUNT 

			UPDATE A
			SET A.Updated_ReferralDevice = B.ReferralDevice
			FROM #Account A 
			INNER JOIN #MDM_Device B
				ON	ISNULL(A.TrackingChannel, '!')	= ISNULL(B.TrackingChannel, '!')
				AND ISNULL(A.ChannelTypeName, '!')	= ISNULL(B.ChannelTypeName, '!')


		EXEC [Control].Sp_Log @BatchKey,@Me, 'Clean ReferralChannel and ReferralPublisher', 'Start', @RowsProcessed = @@ROWCOUNT 

			UPDATE A
			SET A.Updated_ReferralChannel =		CASE	WHEN A.ReferralAccountID > 0 
															THEN 'Peer to Peer'
														WHEN B.Channel = '[Use SourceTrafficSource]'	AND A.ReferralAccountID <= 0
															THEN ISNULL(A.SourceTrafficSource, 'Unknown')
														ELSE B.Channel 
												END ,
				A.Updated_ReferralPublisher =	CASE	WHEN a.ReferralAccountID > 0 
															THEN 'Refer A Friend'
														WHEN B.Publisher = '[Use SourceRefURL]'			AND A.ReferralAccountID <= 0		
															THEN ISNULL(A.SourceRefURL, 'Unknown')
														WHEN B.Publisher = '[Use SourceAffiliateName]'	AND A.ReferralAccountID <= 0 
															THEN ISNULL(A.SourceAffiliateName, 'Unknown')
														ELSE B.Publisher 
												END 	
			FROM #Account A 
			INNER JOIN #MDM_ChannelPublisher B 
				ON	ISNULL(A.TrackingChannel	, '!')	= ISNULL(B.TrackingChannel		, '!')
				AND ISNULL(A.ChannelTypeName	, '!')	= ISNULL(B.ChannelTypeName		, '!') 
				AND ISNULL(A.SourceRefURL		, '!')	= ISNULL(B.SourceRefURL			, '!')
				AND ISNULL(A.SourceTrafficSource, '!')	= ISNULL(B.SourceTrafficSource	, '!')


		EXEC [Control].Sp_Log @BatchKey,@Me, 'Clean ReferralTargeting', 'Start', @RowsProcessed = @@ROWCOUNT 

			UPDATE A
			SET A.Updated_ReferralTargeting =	CASE	WHEN A.ReferralAccountID > 0 
															THEN CONVERT(VARCHAR(20), A.ReferralAccountID)
														WHEN B.Targeting = '[Use SourceCampaignID]'		AND A.ReferralAccountID <= 0	
															THEN ISNULL(A.SourceCampaignID, 'Unknown')
														WHEN B.Targeting = '[Use SourceAffiliateName]'	AND A.ReferralAccountID <= 0 
															THEN ISNULL(A.SourceAffiliateName, 'Unknown')
														ELSE B.Targeting 
												END 	
			FROM #Account A 
			INNER JOIN #MDM_Targeting B 
				ON	ISNULL(A.TrackingChannel	, '!')	= ISNULL(B.TrackingChannel		, '!')
				AND ISNULL(A.ChannelTypeName	, '!')	= ISNULL(B.ChannelTypeName		, '!') 
				AND ISNULL(A.SourceCampaignID	, '!')	= ISNULL(B.SourceCampaignID		, '!')
				AND ISNULL(A.SourceTrafficSource, '!')	= ISNULL(B.SourceTrafficSource	, '!')


		EXEC [Control].Sp_Log @BatchKey,@Me, 'Clean ReferralSpecifics', 'Start', @RowsProcessed = @@ROWCOUNT 

			UPDATE A
			SET A.Updated_ReferralSpecifics =	CASE	WHEN A.ReferralAccountID > 0 
															THEN 'Unknown'
														WHEN B.Specifics = '[Use SourceKeywords]' AND a.ReferralAccountID <= 0 
															THEN ISNULL(A.SourceKeywords, 'Unknown')
														ELSE B.Specifics 
												END 	
			FROM #Account A 
			INNER JOIN #MDM_Specifics B 
				ON	ISNULL(A.TrackingChannel	, '!')	= ISNULL(B.TrackingChannel		, '!')
				AND ISNULL(A.SourceKeywords		, '!')	= ISNULL(B.SourceKeywords		, '!')
				AND ISNULL(A.SourceTrafficSource, '!')	= ISNULL(B.SourceTrafficSource	, '!')
				AND A.ChannelTypeName					= B.ChannelTypeName
				
		EXEC [Control].Sp_Log @BatchKey,@Me, 'Pending Records', 'Start', @RowsProcessed = @@RowCount 
		
		UPDATE A
			SET 
				A.Updated_Promo					=  ISNULL(Updated_Promo				, 'Pending'),
				A.Updated_ReferralDevice		=  ISNULL(Updated_ReferralDevice	, 'Pending'),
				A.Updated_ReferralChannel		=  ISNULL(Updated_ReferralChannel	, 'Pending'),
				A.Updated_ReferralPublisher		=  ISNULL(Updated_ReferralPublisher	, 'Pending'),
				A.Updated_ReferralTargeting		=  ISNULL(Updated_ReferralTargeting	, 'Pending'),
				A.Updated_ReferralSpecifics		=  ISNULL(Updated_ReferralSpecifics	, 'Pending')
			FROM #Account A
			WHERE 
				A.Updated_Promo				   IS NULL OR
				A.Updated_ReferralDevice	   IS NULL OR
				A.Updated_ReferralChannel	   IS NULL OR
				A.Updated_ReferralPublisher	   IS NULL OR
				A.Updated_ReferralTargeting	   IS NULL OR
				A.Updated_ReferralSpecifics	   IS NULL


		EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start', @RowsProcessed = @@RowCount

	  IF @DryRun = 0 

	  BEGIN
	  
			UPDATE	U
			SET			U.Promo					= M.Updated_Promo,
						U.ReferralDevice		= M.Updated_ReferralDevice,
						U.ReferralChannel		= M.Updated_ReferralChannel,
						U.ReferralPublisher		= M.Updated_ReferralPublisher,
						U.ReferralSpecifics		= M.Updated_ReferralSpecifics,
						U.ReferralTargeting		= M.Updated_ReferralTargeting
			FROM   [$(Dimensional)].Dimension.Account U
					 INNER JOIN #Account M ON u.AccountID = m.AccountID
			WHERE										
					(
							Isnull(U.Promo				, '!')	<> Isnull(M.Updated_Promo				, '!') 
						OR	Isnull(U.ReferralDevice		, '!')	<> Isnull(M.Updated_ReferralDevice		, '!') 
						OR	Isnull(U.ReferralChannel	, '!')	<> Isnull(M.Updated_ReferralChannel		, '!') 
						OR	Isnull(U.ReferralPublisher	, '!')	<> Isnull(M.Updated_ReferralPublisher	, '!') 
						OR	Isnull(U.ReferralSpecifics	, '!')	<> Isnull(M.Updated_ReferralSpecifics	, '!') 
						OR	Isnull(U.ReferralTargeting	, '!')	<> Isnull(M.Updated_ReferralTargeting	, '!') 
					)
	  
		  EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @@RowCount 
	  
	  END
	  
	  ELSE
	
		BEGIN

				SELECT *
				FROM    #Account
				WHERE									
					(
							Isnull(Promo				, '!')	<> Isnull(Updated_Promo				, '!') 
						OR	Isnull(ReferralDevice		, '!')	<> Isnull(Updated_ReferralDevice	, '!') 
						OR	Isnull(ReferralChannel		, '!')	<> Isnull(Updated_ReferralChannel	, '!') 
						OR	Isnull(ReferralPublisher	, '!')	<> Isnull(Updated_ReferralPublisher	, '!') 
						OR	Isnull(ReferralSpecifics	, '!')	<> Isnull(Updated_ReferralSpecifics	, '!') 
						OR	Isnull(ReferralTargeting	, '!')	<> Isnull(Updated_ReferralTargeting	, '!') 
					)

			EXEC [Control].Sp_Log @BatchKey,@Me, 'DryRun', 'Success', @RowsProcessed = @@ROWCOUNT

		END
	END TRY 

	BEGIN CATCH 
		DECLARE @ErrorMessage VARCHAR(255) = Error_message(); 

		EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage; 

		THROW; 
	END CATCH