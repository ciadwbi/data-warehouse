﻿CREATE proc [Cleanse].[Sp_Account_Ledger]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET @Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY


	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start';

	UPDATE	u
	SET		LedgerGroup = u.UpdatedLedgerGroup,
			LedgerGrouping = u.UpdatedLedgerGrouping,
			CleansedDate = GETDATE(),
			CleansedBatchKey = @BatchKey,
			CleansedBy = @Me
	FROM	(	SELECT	LedgerGroup,
						LedgerGrouping, 
						CASE WHEN LedgerTypeID = 1 THEN 'Credit' WHEN LedgerTypeID = 10 THEN 'Cash' ELSE 'Others' END UpdatedLedgerGroup,
						CASE WHEN LedgerTypeID IN (4,5,6,7,8) THEN 'Financial' WHEN LedgerTypeID IS NOT NULL THEN 'Transactional' ELSE 'Unknown' END UpdatedLedgerGrouping,
						CleansedDate,
						CleansedBatchKey,
						CleansedBy
				FROM	[$(Dimensional)].Dimension.Account
				WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)
						AND (Increment = @Increment OR @Increment = 0) 
			) u
	WHERE	ISNULL(LedgerGroup,'!') <> UpdatedLedgerGroup
			OR ISNULL(LedgerGrouping,'!') <> UpdatedLedgerGrouping
	OPTION (IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
