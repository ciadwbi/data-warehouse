﻿CREATE proc [Cleanse].[Sp_Account_Opened]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET @Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start';

	SELECT	a.AccountKey,
			a.AccountOpenedDate, 
			a.AccountOpenedDayKey, 
			ISNULL(a1.LedgerOpeneddate, a.LedgerOpenedDate) UpdatedAccountOpenedDate, 
			ISNULL(a1.LedgerOpenedDayKey,a.LedgerOpenedDayKey) UpdatedAccountOpenedDayKey
	INTO	#Accounts
	FROM	[$(Dimensional)].Dimension.Account a
			LEFT LOOP JOIN [$(Dimensional)].Dimension.Account a1 ON a.LedgerSequenceNumber > 1 AND a1.AccountID = a.AccountID AND a1.LedgerSource = a.LedgerSource AND a1.LedgerSequenceNumber = 1
	WHERE	(a.ModifiedBatchKey = @BatchKey OR @BatchKey = 0)
			AND (a.Increment = @Increment OR @Increment = 0) 
	OPTION (IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)

	UPDATE	a
	SET		AccountOpenedDate = u.UpdatedAccountOpenedDate,
			AccountOpenedDayKey = u.UpdatedAccountOpenedDayKey,
			CleansedDate = GETDATE(),
			CleansedBatchKey = @BatchKey,
			CleansedBy = @Me
	FROM	[$(Dimensional)].Dimension.Account a
			INNER JOIN #Accounts u ON u.AccountKey = a.AccountKey
	WHERE	ISNULL(a.AccountOpenedDate,CONVERT(datetime2(3),'1900-01-01')) <> ISNULL(u.UpdatedAccountOpenedDate,CONVERT(datetime2(3),'1900-01-01'))
			OR ISNULL(a.AccountOpenedDayKey,-99) <> ISNULL(u.UpdatedAccountOpenedDayKey,-99)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH