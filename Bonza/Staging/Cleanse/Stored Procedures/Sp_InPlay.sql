﻿CREATE PROC [Cleanse].[Sp_InPlay]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start'

	UPDATE	u
	SET		ChaseTheAce =		UpdatedChaseTheAce,
			PricePump =			UpdatedPricePump,
			QuickBet =			UpdatedQuickBet,
			ReBet =				UpdatedReBet,
			NextToJump =		UpdatedNextToJump,
			NextToGo =			UpdatedNextToGo,
			HeroWidget =		UpdatedHeroWidget,
			FeatureTiles =		UpdatedFeatureTiles,
			TrendingBet =		UpdatedTrendingBet,
			HiLoBet =			UpdatedHiLoBet,
			NextToJumpTicker =	UpdatedNextToJumpTicker,
			SportsHomeGrid =	UpdatedSportsHomeGrid,
			SportsHomeList =	UpdatedSportsHomeList,
			OddEvenBet =		UpdatedOddEvenBet,
			MysteryBet =		UpdatedMysteryBet,
			FeaturedRaceVenue =	UpdatedFeaturedRaceVenue,
			MysteryBetPokies =	UpdatedMysteryBetPokies,
			FastMulti =			UpdatedFastMulti,
			SpinAndWin =		UpdatedSpinAndWin,
			MultiPricePump =	UpdatedMultiPricePump,
			HotStreak =			UpdatedHotStreak,
			WolfsTip =			UpdatedWolfsTip,
			TurboTopup =		UpdatedTurboTopup,
			CleansedDate =		GETDATE(),
			CleansedBatchKey =	@BatchKey,
			CleansedBy =		@Me
	FROM	(	SELECT	ChaseTheAce,
						PricePump,
						QuickBet,
						ReBet,
						NextToJump,
						NextToGo,
						HeroWidget,
						FeatureTiles,
						TrendingBet,
						HiLoBet, 
						NextToJumpTicker, 
						SportsHomeGrid, 
						SportsHomeList, 
						OddEvenBet,
						MysteryBet,
						FeaturedRaceVenue,
						MysteryBetPokies,
						FastMulti,
						SpinAndWin,
						MultiPricePump,
						HotStreak,
						WolfsTip,
						TurboTopup,
						CASE WHEN FeatureFlags & 2 = 2 THEN 'Yes' WHEN IsChaseTheAce = 1 THEN 'Yes' ELSE 'No' END UpdatedChaseTheAce,
						CASE FeatureFlags & 2 WHEN 2 THEN 'Yes' ELSE 'No' END UpdatedPricePump,
						CASE FeatureFlags & 4 WHEN 4 THEN 'Yes' ELSE 'No' END UpdatedQuickBet,
						CASE FeatureFlags & 8 WHEN 8 THEN 'Yes' ELSE 'No' END UpdatedReBet,
						CASE FeatureFlags & 16 WHEN 16 THEN 'Yes' ELSE 'No' END UpdatedNextToJump,
						CASE FeatureFlags & 32 WHEN 32 THEN 'Yes' ELSE 'No' END UpdatedNextToGo,
						CASE FeatureFlags & 64 WHEN 64 THEN 'Yes' ELSE 'No' END UpdatedHeroWidget,
						CASE FeatureFlags & 128 WHEN 128 THEN 'Yes' ELSE 'No' END UpdatedFeatureTiles,
						CASE FeatureFlags & 256 WHEN 256 THEN 'Yes' ELSE 'No' END UpdatedTrendingBet,
						CASE FeatureFlags & 512 WHEN 512 THEN 'Yes' ELSE 'No' END UpdatedHiLoBet, 
						CASE FeatureFlags & 1024 WHEN 1024 THEN 'Yes' ELSE 'No' END UpdatedNextToJumpTicker,
						CASE FeatureFlags & 2048 WHEN 2048 THEN 'Yes' ELSE 'No' END UpdatedSportsHomeGrid, 
						CASE FeatureFlags & 4096 WHEN 4096 THEN 'Yes' ELSE 'No' END UpdatedSportsHomeList, 
						CASE FeatureFlags & 8192 WHEN 8192 THEN 'Yes' ELSE 'No' END UpdatedOddEvenBet,
						CASE FeatureFlags & 16384 WHEN 16384 THEN 'Yes' ELSE 'No' END UpdatedMysteryBet,
						CASE FeatureFlags & 32768 WHEN 32768 THEN 'Yes' ELSE 'No' END UpdatedFeaturedRaceVenue,
						CASE FeatureFlags & 65536 WHEN 65536 THEN 'Yes' ELSE 'No' END UpdatedMysteryBetPokies,
						CASE FeatureFlags & 131072 WHEN 131072 THEN 'Yes' ELSE 'No' END UpdatedFastMulti,
						CASE FeatureFlags & 262144 WHEN 262144 THEN 'Yes' ELSE 'No' END UpdatedSpinAndWin,
						CASE FeatureFlags & 524288 WHEN 524288 THEN 'Yes' ELSE 'No' END UpdatedMultiPricePump,
						CASE FeatureFlags & 2097152 WHEN 2097152 THEN 'Yes' ELSE 'No' END UpdatedHotStreak,
						CASE FeatureFlags & 8388608 WHEN 8388608 THEN 'Yes' ELSE 'No' END UpdatedWolfsTip,
						CASE FeatureFlags & 16777216 WHEN 16777216 THEN 'Yes' ELSE 'No' END UpdatedTurboTopup,
						CleansedDate,
						CleansedBatchKey,
						CleansedBy
				FROM	[$(Dimensional)].Dimension.InPlay
				WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)
			) u
	WHERE	ChaseTheAce <> UpdatedChaseTheAce
			OR PricePump <> UpdatedPricePump
			OR QuickBet <> UpdatedQuickBet
			OR ReBet <> UpdatedReBet
			OR NextToJump <> UpdatedNextToJump
			OR NextToGo <> UpdatedNextToGo
			OR HeroWidget <> UpdatedHeroWidget
			OR FeatureTiles <> UpdatedFeatureTiles
			OR TrendingBet <> UpdatedTrendingBet
			OR HiLoBet <> UpdatedHiLoBet 
			OR NextToJumpTicker <> UpdatedNextToJumpTicker
			OR SportsHomeGrid <> UpdatedSportsHomeGrid 
			OR SportsHomeList <> UpdatedSportsHomeList
			OR OddEvenBet <> UpdatedOddEvenBet
			OR MysteryBet <> UpdatedMysteryBet
			OR FeaturedRaceVenue <> UpdatedFeaturedRaceVenue
			OR MysteryBetPokies <> UpdatedMysteryBetPokies
			OR FastMulti <> UpdatedFastMulti
			OR SpinAndWin <> UpdatedSpinAndWin
			OR MultiPricePump <> UpdatedMultiPricePump
			OR HotStreak <> UpdatedHotStreak
			OR WolfsTip <> UpdatedWolfsTip
			OR TurboTopup <> UpdatedTurboTopup
	
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
