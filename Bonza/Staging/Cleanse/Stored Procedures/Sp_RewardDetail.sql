﻿CREATE proc [Cleanse].[Sp_RewardDetail]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Cache', 'Start'

	SELECT	RewardDetailKey, TransactionCode, 
			RewardTransactionTypeKey, RewardTransactionTypeCode, RewardTransactionType, 
			RewardTransactionClassKey, RewardTransactionClassCode, RewardTransactionClass, 
			RewardBetTypeKey, RewardBetTypeId, RewardBetTypeCode, RewardBetType, 
			RewardReasonKey, RewardReasonId, RewardReason, 
			RewardReasonTypeKey, RewardReasonTypeCode, RewardReasonType, DirectionCode,
			RewardTransactionTypeKey UpdatedRewardTransactionTypeKey, RewardTransactionTypeCode UpdatedRewardTransactionTypeCode, RewardTransactionType UpdatedRewardTransactionType, 
			RewardTransactionClassKey UpdatedRewardTransactionClassKey, RewardTransactionClassCode UpdatedRewardTransactionClassCode, RewardTransactionClass UpdatedRewardTransactionClass, 
			RewardBetTypeKey UpdatedRewardBetTypeKey, RewardBetTypeCode UpdatedRewardBetTypeCode, RewardBetType UpdatedRewardBetType, 
			RewardReasonKey UpdatedRewardReasonKey, RewardReason UpdatedRewardReason, 
			RewardReasonTypeKey UpdatedRewardReasonTypeKey, RewardReasonTypeCode UpdatedRewardReasonTypeCode, RewardReasonType UpdatedRewardReasonType
	INTO	#Cache
	FROM	[$(Dimensional)].Dimension.RewardDetail
	WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)

	EXEC [Control].Sp_Log @BatchKey,@Me, 'TransactionType', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		UpdatedRewardTransactionTypeKey = NewRewardTransactionTypeKey,
			UpdatedRewardTransactionTypeCode = NewRewardTransactionTypeCode,
			UpdatedRewardTransactionType = NewRewardTransactionType,
			UpdatedRewardTransactionClassKey = NewRewardTransactionClassKey,
			UpdatedRewardTransactionClassCode = NewRewardTransactionClassCode,
			UpdatedRewardTransactionClass = NewRewardTransactionClass
	FROM	(	SELECT	UpdatedRewardTransactionTypeKey,
						UpdatedRewardTransactionTypeCode,
						UpdatedRewardTransactionType,
						UpdatedRewardTransactionClassKey,
						UpdatedRewardTransactionClassCode,
						UpdatedRewardTransactionClass, 
						ISNULL(r.RewardTransactionTypeKey, m.MaxRewardTransactionTypeKey + ROW_NUMBER() OVER (ORDER BY CASE WHEN r.RewardTransactionTypeKey IS NULL THEN 0 ELSE 1 END)) NewRewardTransactionTypeKey,
						ISNULL(r.RewardTransactionTypeCode, u.RewardTransactionTypeCode) NewRewardTransactionTypeCode,
						ISNULL(r.RewardTransactionType, u.RewardTransactionType) NewRewardTransactionType,
						ISNULL(r.RewardTransactionClassKey, u.RewardTransactionClassKey) NewRewardTransactionClassKey,
						ISNULL(r.RewardTransactionClassCode, u.RewardTransactionClassCode) NewRewardTransactionClassCode,
						ISNULL(r.RewardTransactionClass, u.RewardTransactionClass) NewRewardTransactionClass
				 FROM	(	SELECT	c.TransactionCode, MIN(r.RewardDetailKey) NewRewardDetailKey -- Find the first dimension key having the same natural key (if any)
							FROM	(SELECT DISTINCT TransactionCode FROM #Cache) c
									LEFT JOIN [$(Dimensional)].Dimension.RewardDetail r ON r.TransactionCode = c.TransactionCode AND r.RewardTransactionTypeKey <> -2 AND (ModifiedBatchKey < @BatchKey OR @BatchKey = 0) -- Only try and find an exisitng key in previous batches
							GROUP BY c.TransactionCode
						) x
						INNER JOIN #Cache c ON c.TransactionCode = x.TransactionCode
						LEFT JOIN [$(Dimensional)].Dimension.RewardDetail r ON r.RewardDetailKey = x.NewRewardDetailKey
						CROSS APPLY (SELECT * FROM [$(Dimensional)].Dimension.RewardDetail WHERE RewardDetailKey = -1) u
						CROSS APPLY (SELECT TOP 1 RewardTransactionTypeKey MaxRewardTransactionTypeKey FROM [$(Dimensional)].Dimension.RewardDetail ORDER BY RewardTransactionTypeKey DESC) m
			) u	
	WHERE	UpdatedRewardTransactionTypeKey <> NewRewardTransactionTypeKey
			OR UpdatedRewardTransactionTypeCode <> NewRewardTransactionTypeCode
			OR UpdatedRewardTransactionType <> NewRewardTransactionType
			OR UpdatedRewardTransactionClassKey <> NewRewardTransactionClassKey
			OR UpdatedRewardTransactionClassCode <> NewRewardTransactionClassCode
			OR UpdatedRewardTransactionClass <> NewRewardTransactionClass
	
	EXEC [Control].Sp_Log @BatchKey,@Me, 'BetType', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		UpdatedRewardBetTypeKey = NewRewardBetTypeKey,
			UpdatedRewardBetTypeCode = NewRewardBetTypeCode,
			UpdatedRewardBetType = NewRewardBetType
	FROM	(	SELECT	UpdatedRewardBetTypeKey,
						UpdatedRewardBetTypeCode,
						UpdatedRewardBetType, 
						ISNULL(r.RewardBetTypeKey, m.MaxRewardBetTypeKey + ROW_NUMBER() OVER (ORDER BY CASE WHEN r.RewardBetTypeKey IS NULL THEN 0 ELSE 1 END)) NewRewardBetTypeKey,
						ISNULL(r.RewardBetTypeCode, u.RewardBetTypeCode) NewRewardBetTypeCode,
						ISNULL(r.RewardBetType, u.RewardBetType) NewRewardBetType
				 FROM	(	SELECT	c.RewardBetTypeId, MIN(r.RewardDetailKey) RewardDetailKey -- Find the first dimension key having the same natural key (if any)
							FROM	(SELECT DISTINCT RewardBetTypeId FROM #Cache) c
									LEFT JOIN [$(Dimensional)].Dimension.RewardDetail r ON r.RewardBetTypeId = c.RewardBetTypeId AND r.RewardBetTypeKey <> -2 AND (ModifiedBatchKey < @BatchKey OR @BatchKey = 0) -- Only try and find an exisitng key in previous batches
							GROUP BY c.RewardBetTypeId
						) x
						INNER JOIN #Cache c ON c.RewardBetTypeId = x.RewardBetTypeId
						LEFT JOIN [$(Dimensional)].Dimension.RewardDetail r ON r.RewardDetailKey = x.RewardDetailKey
						CROSS APPLY (SELECT * FROM [$(Dimensional)].Dimension.RewardDetail x WHERE x.RewardDetailKey = -1) u
						CROSS APPLY (SELECT TOP 1 RewardBetTypeKey MaxRewardBetTypeKey FROM [$(Dimensional)].Dimension.RewardDetail ORDER BY RewardBetTypeKey DESC) m
			) u	
	WHERE	UpdatedRewardBetTypeKey <> NewRewardBetTypeKey
			OR UpdatedRewardBetTypeCode <> NewRewardBetTypeCode
			OR UpdatedRewardBetType <> NewRewardBetType
	
	EXEC [Control].Sp_Log @BatchKey,@Me, 'Reason', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		UpdatedRewardReasonKey = NewRewardReasonKey,
			UpdatedRewardReasonTypeKey = NewRewardReasonTypeKey,
			UpdatedRewardReasonTypeCode = NewRewardReasonTypeCode,
			UpdatedRewardReasonType = NewRewardReasonType
	FROM	(	SELECT	UpdatedRewardReasonKey,
						UpdatedRewardReasonTypeKey,
						UpdatedRewardReasonTypeCode,
						UpdatedRewardReasonType, 
						COALESCE(r.RewardReasonKey, x.RewardReasonKey, m.MaxRewardReasonKey + ROW_NUMBER() OVER (ORDER BY CASE WHEN r.RewardReasonKey IS NULL THEN 0 ELSE 1 END)) NewRewardReasonKey,
						COALESCE(r.RewardReasonTypeKey, CASE x.RewardReasonType WHEN 'Testing' THEN 1 WHEN 'Bonus' THEN 2 WHEN 'Survey' THEN 3 WHEN 'Correction' THEN 4 END, u.RewardReasonTypeKey) NewRewardReasonTypeKey,
						COALESCE(r.RewardReasonTypeCode, CASE x.RewardReasonType WHEN 'Testing' THEN 'T' WHEN 'Bonus' THEN 'B' WHEN 'Survey' THEN 'S' WHEN 'Correction' THEN 'C' END, u.RewardReasonTypeCode) NewRewardReasonTypeCode,
						COALESCE(r.RewardReasonType, x.RewardReasonType, u.RewardReasonType) NewRewardReasonType
				 FROM	(	SELECT	c.RewardReasonKey, c.RewardReasonId, c.RewardReasonType, MIN(r.RewardDetailKey) RewardDetailKey -- Find the first dimension key having the same natural key (if any)
							FROM	(	SELECT	RewardReasonId, 
												CASE
													WHEN RewardReason LIKE '%Test%' THEN 'Testing'
													WHEN RewardReason LIKE '%Bonus%' THEN 'Bonus'
													WHEN RewardReason LIKE '%Additional%' THEN 'Bonus'
													WHEN RewardReason LIKE '%Double%' THEN 'Bonus'
													WHEN RewardReason LIKE '%Survey%' THEN 'Survey'
													WHEN RewardReason LIKE '%Feedback%' THEN 'Survey'
													WHEN RewardReason LIKE '%Error%' THEN 'Correction'
													WHEN RewardReason LIKE '%Adjustment%' THEN 'Correction'
													WHEN RewardReason LIKE '%Top%Up%' THEN 'Bonus'
												END RewardReasonType,
												MIN(CASE RewardReasonKey WHEN -2 THEN NULL ELSE RewardReasonKey END) RewardReasonKey
										FROM	#Cache
										WHERE	RewardReason NOT IN ('N/A','Unknown')
										GROUP BY RewardReasonId, RewardReason
									) c
									LEFT JOIN [$(Dimensional)].Dimension.RewardDetail r ON r.RewardReasonId = c.RewardReasonId AND r.RewardReasonKey <> -2 AND (ModifiedBatchKey < @BatchKey OR @BatchKey = 0) -- Only try and find an exisitng key in previous batches
							GROUP BY c.RewardReasonKey, c.RewardReasonId, c.RewardReasonType
						) x
						INNER JOIN #Cache c ON c.RewardReasonId = x.RewardReasonId
						LEFT JOIN [$(Dimensional)].Dimension.RewardDetail r ON r.RewardDetailKey = x.RewardDetailKey
						CROSS APPLY (SELECT * FROM [$(Dimensional)].Dimension.RewardDetail x WHERE x.RewardDetailKey = -1) u
						CROSS APPLY (SELECT TOP 1 RewardReasonKey MaxRewardReasonKey FROM [$(Dimensional)].Dimension.RewardDetail ORDER BY RewardReasonKey DESC) m
			) u	
	WHERE	UpdatedRewardReasonKey <> NewRewardReasonKey
			OR UpdatedRewardReasonTypeKey <> NewRewardReasonTypeKey
			OR UpdatedRewardReasonTypeCode <> NewRewardReasonTypeCode
			OR UpdatedRewardReasonType <> NewRewardReasonType
	
	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		RewardTransactionTypeKey = c.UpdatedRewardTransactionTypeKey,
			RewardTransactionTypeCode = c.UpdatedRewardTransactionTypeCode,
			RewardTransactionType = c.UpdatedRewardTransactionType,
			RewardTransactionClassKey = c.UpdatedRewardTransactionClassKey,
			RewardTransactionClassCode = c.UpdatedRewardTransactionClassCode,
			RewardTransactionClass = c.UpdatedRewardTransactionClass,
			RewardBetTypeKey = c.UpdatedRewardBetTypeKey,
			RewardBetTypeCode = c.UpdatedRewardBetTypeCode,
			RewardBetType = c.UpdatedRewardBetType,
			RewardReasonKey = c.UpdatedRewardReasonKey,
			RewardReason = c.UpdatedRewardReason,
			RewardReasonTypeKey = c.UpdatedRewardReasonTypeKey,
			RewardReasonTypeCode = c.UpdatedRewardReasonTypeCode,
			RewardReasonType = c.UpdatedRewardReasonType,
			CleansedDate = GETDATE(),
			CleansedBatchKey = @BatchKey,
			CleansedBy = @Me
	FROM	[$(Dimensional)].Dimension.RewardDetail u
			INNER JOIN #Cache c ON c.RewardDetailKey = u.RewardDetailKey
	WHERE	c.RewardTransactionTypeKey <> c.UpdatedRewardTransactionTypeKey
			OR c.RewardReasonTypeCode <> c.UpdatedRewardReasonTypeCode
			OR c.RewardTransactionType <> c.UpdatedRewardTransactionType
			OR c.RewardTransactionClassKey <> c.UpdatedRewardTransactionClassKey
			OR c.RewardTransactionClassCode <> c.UpdatedRewardTransactionClassCode
			OR c.RewardTransactionClass <> c.UpdatedRewardTransactionClass
			OR c.RewardBetTypeKey <> c.UpdatedRewardBetTypeKey
			OR c.RewardBetTypeCode <> c.UpdatedRewardBetTypeCode
			OR c.RewardBetType <> c.UpdatedRewardBetType
			OR c.RewardReasonKey <> c.UpdatedRewardReasonKey
			OR c.RewardReason <> c.UpdatedRewardReason
			OR c.RewardReasonTypeKey <> c.UpdatedRewardReasonTypeKey
			OR c.RewardReasonTypeCode <> c.UpdatedRewardReasonTypeCode
			OR c.RewardReasonType <> c.UpdatedRewardReasonType

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
