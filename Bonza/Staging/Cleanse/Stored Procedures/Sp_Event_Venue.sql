﻿CREATE proc [Cleanse].[SP_Event_Venue]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

EXEC	[Control].Sp_Log @BatchKey, @Me, 'Update', 'Start';

UPDATE	x
SET		VenueStateCode = x.NewStateCode, 
		VenueState = CASE x.NewStateCode
						WHEN 'N/A' THEN 'Outside Australia' 
						WHEN 'ACT' THEN 'Australian Capital Territory' 
						WHEN 'NSW' THEN 'New South Wales' 
						WHEN 'VIC' THEN 'Victoria' 
						WHEN 'QLD' THEN 'Queensland' 
						WHEN 'SA' THEN 'South Australia' 
						WHEN 'WA' THEN 'Western Australia' 
						WHEN 'TAS' THEN 'Tasmania' 
						WHEN 'NT' THEN 'Northern Territories'
						ELSE 'Unknown'
					END,
		VenueCountryCode = ISNULL(c.Country3Code,'UNK'),
		VenueCountry = x.NewCountry
FROM	(	SELECT	EventKey,
					VenueStateCode,
					CASE
						WHEN VenueId = 3784 THEN 'NT'
						WHEN VenueId = 3787 THEN 'WA'
						WHEN VenueId = 3822 THEN 'N/A'
						WHEN VenueCountry IN ('UNK','UNKNOWN') THEN 'UNK'
						WHEN VenueCountry <> 'Australia' THEN 'N/A'
						WHEN VenueStateCode = '0' THEN 'N/A'
						WHEN VenueStateCode = '1' THEN 'ACT'
						WHEN VenueStateCode = '2' THEN 'NSW'
						WHEN VenueStateCode = '3' THEN 'VIC'
						WHEN VenueStateCode = '4' THEN 'QLD'
						WHEN VenueStateCode = '5' THEN 'SA'
						WHEN VenueStateCode = '6' THEN 'WA'
						WHEN VenueStateCode = '7' THEN 'TAS'
						WHEN VenueStateCode = '8' THEN 'NT'
						WHEN VenueStateCode = '9' THEN 'UNK'
						WHEN VenueStateCode IS NULL THEN 'UNK'
						ELSE VenueStateCode
					END NewStateCode,
					VenueState,
					VenueCountryCode,
					VenueCountry,
					CASE
						WHEN VenueId = 3784 THEN 'AUSTRALIA'
						WHEN VenueId = 3787 THEN 'AUSTRALIA'
						WHEN VenueId = 3822 THEN 'SWEDEN'
						WHEN VenueCountry = 'AUSTRALIA' AND VenueStateCode = '0' AND GrandParentEvent LIKE '%New Zealand' THEN 'NEW ZEALAND'
						WHEN VenueCountry = 'AUSTRALIA' AND VenueStateCode = '0' AND GrandParentEvent LIKE '%Singapore' THEN 'SINGAPORE'
						WHEN VenueCountry = 'AUSTRALIA' AND VenueStateCode = '0' AND GrandParentEvent LIKE '%Sweden' THEN 'SWEDEN'
						WHEN VenueCountry = 'AUSTRALIA' AND VenueStateCode = '0' AND GrandParentEvent LIKE '%United States' THEN 'UNITED STATES'
						WHEN VenueCountry = 'AUSTRALIA' AND VenueStateCode = '0' THEN 'UNITED KINGDOM'
						WHEN VenueCountry = 'KOREA' AND VenueStateCode = 'N/A' THEN 'KOREA, REPUBLIC OF'
						WHEN VenueCountry = 'UNK' THEN 'UNKNOWN'
						ELSE ISNULL(VenueCountry, 'UNKNOWN')
					END NewCountry
			FROM	[$(Dimensional)].Dimension.Event
			WHERE	EventKey NOT IN (0,-1)
					AND ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN ModifiedBatchKey ELSE @BatchKey END 
		) x
		LEFT JOIN Reference.Country c ON (c.CountryName = x.NewCountry)
WHERE	ISNULL(x.VenueStateCode,'!') <> x.NewStateCode
		OR ISNULL(x.VenueCountry,'!') <> x.NewCountry
		OR ISNULL(c.Country3Code,'!') <> x.VenueCountryCode

UPDATE	x
SET		ScheduledDayKey = DayKey,
		ModifiedBy = @Me,
		ModifiedDate = GETDATE(),
		CleansedDate = GETDATE(),
		CleansedBatchKey = @BatchKey,
		CleansedBy = @Me
FROM	(
		Select	EventKey
				, ScheduledDayKey
				, ScheduledDateTime
				, [DayZone].DayKey
				, [Event].ModifiedBy
				, [Event].ModifiedDate
				, [Event].CleansedDate
				, [Event].CleansedBatchKey
				, [Event].CleansedBy
		FROM	[$(Dimensional)].Dimension.[Event] as [Event]
				left join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on CONVERT(VARCHAR(11),CONVERT(DATE,[Event].ScheduledDateTime)) = [DayZone].MasterDayText
					and [Event].ScheduledDateTime >= [DayZone].FromDate
					and [Event].ScheduledDateTime < [DayZone].ToDate
					AND [Event].ModifiedBatchKey = CASE @BatchKey WHEN 0 THEN [Event].ModifiedBatchKey ELSE @BatchKey END 
		WHERE	EventKey NOT IN (0,-1) and ScheduledDayKey = -1
		) as X
Where	DayKey not in (0, -1)

SET @RowCount = @@ROWCOUNT
SET @RowsProcessed = @RowsProcessed + @RowCount

EXEC	[Control].Sp_Log @BatchKey, @Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC	[Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
