﻿CREATE PROCEDURE [Cleanse].[Sp_Account_Referral_iOS_Android]
	(@BatchKey   INT,--				= 754, 
										@DryRun bit = 1,			-- Allows the procedure to be test run and display results without actually writing them to the table
										@RowsProcessed  int = 0 OUTPUT 
) 
AS 
    
	DECLARE @Me VARCHAR(256) = Object_schema_name(@@PROCID) + '.' + Object_name(@@PROCID) 
    DECLARE @RowCount INT; 

    SET @BatchKey = Isnull(@BatchKey, -1)  -- Interpret Nulls passed as parameters the same as zeroes 
    SET TRANSACTION isolation level READ uncommitted; 
	SET NOCOUNT ON

  BEGIN try 



	  EXEC [Control].Sp_Log @BatchKey, @Me, 'Cache MDM', 'Start', @RowsProcessed = @@ROWCOUNT 
	
		SELECT	
				Replace(TrackingChannel, '[blank]','')			COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingChannel,
				Replace(TrackingEventName, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingEventName,
				Replace(TrackingDcpBtag, '[blank]','')			COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingDcpBtag,
				Replace(SourceSitePromoName, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	SourceSitePromoName,
				Replace(Promo_Name, '[blank]','')					COLLATE SQL_Latin1_General_CP1_CI_AS	Promo
		INTO #MDM_Promo
		FROM [$(MDS_Server)].[$(MDS)].[mdm].[CleanseAccount_Promo_iOS_Android]

		set @RowCount = @@RowCount

		SELECT	
				Replace(TrackingChannel, '[blank]','')			COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingChannel,
				Replace(ChannelTypeName, '[blank]','')			COLLATE SQL_Latin1_General_CP1_CI_AS	ChannelTypeName,
				Replace(ReferralDevice, '[blank]','')			COLLATE SQL_Latin1_General_CP1_CI_AS	ReferralDevice
		INTO #MDM_Device
		FROM [$(MDS_Server)].[$(MDS)].[mdm].[CleanseAccount_Device]

		set @RowCount = @RowCount + @@RowCount

		SELECT	
				Replace(TrackingTrackerName, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingTrackerName,
				Replace(Channel_Name, '[blank]','')					COLLATE SQL_Latin1_General_CP1_CI_AS	Channel
		INTO #MDM_Channel
		FROM [$(MDS_Server)].[$(MDS)].[mdm].[CleanseAccount_Channel_iOS_Android]
		
		set @RowCount = @RowCount + @@RowCount

		SELECT	
				Replace(TrackingTrackerName, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingTrackerName,
				Replace(DCPPublisher, '[blank]','')				COLLATE SQL_Latin1_General_CP1_CI_AS	DCPPublisher,
				Replace(Publisher, '[blank]','')				COLLATE SQL_Latin1_General_CP1_CI_AS	Publisher
		INTO #MDM_Publisher
		FROM [$(MDS_Server)].[$(MDS)].[mdm].[CleanseAccount_Publisher_iOS_Android]
			
		set @RowCount = @RowCount + @@RowCount

		SELECT	
				Replace(TrackingDCPCampaign, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingDCPCampaign,
				Replace(TrackingTrackerName, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingTrackerName,
				Replace(TrackingTrackerName2, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingTrackerName2,
				Replace(TrackingTrackerName3, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingTrackerName3,
				Replace(Targeting, '[blank]','')				COLLATE SQL_Latin1_General_CP1_CI_AS	Targeting
		INTO #MDM_Targeting

		FROM [$(MDS_Server)].[$(MDS)].[mdm].[CleanseAccount_Targeting_iOS_Android]

		set @RowCount = @RowCount + @@RowCount

		SELECT	
				Replace(TrackingTrackerName, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingTrackerName,
				Replace(TrackingDcpKeyword, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingDcpKeyword,
				Replace(TrackingTrackerName2, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingTrackerName2,
				Replace(TrackingTrackerName3, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingTrackerName3,
				Replace(TrackingTrackerName4, '[blank]','')		COLLATE SQL_Latin1_General_CP1_CI_AS	TrackingTrackerName4,
				Replace(Specifics, '[blank]','')				COLLATE SQL_Latin1_General_CP1_CI_AS	Specifics
		INTO #MDM_Specifics
		FROM [$(MDS_Server)].[$(MDS)].[mdm].[CleanseAccount_Specifics_iOS_Android]
		
		set @RowCount = @RowCount + @@RowCount

      EXEC [Control].Sp_Log @BatchKey,@Me, 'Cache Accounts', 'Start', @RowsProcessed = @RowCount 
	  
			SELECT	
				--Unique Identifier
				m.AccountID,

				--Actual Columns
				m.Promo,
				m.referralDevice,
				m.ReferralChannel,
				m.ReferralPublisher,
				m.ReferralTargeting,
				m.ReferralSpecifics,

				--Reference Fields
				--Promo
				m.TrackingChannel,
				m.TrackingEventName,
				m.TrackingDcpBtag,
				m.SourceSitePromoName,

				--Device
				c.ChannelTypeName,

				--Publisher
				TrackingDcpPublisher,

				--Channel
				CASE when TrackingTrackerName like '%::%'
					then Left(TrackingTrackerName, CharIndex('::', TrackingTrackerName) - 1) 
					Else TrackingTrackerName End as TrackingTrackerName,
				
				--Targeting
				TrackingDCPCampaign,
				CASE when TrackingTrackerName like '%::%'
						THEN CASE WHEN CHARINDEX('::',SUBSTRING(TrackingTrackerName, CHARINDEX('::',TrackingTrackerName)+2, LEN(TrackingTrackerName))) = 0 
    						THEN SUBSTRING(TrackingTrackerName, CHARINDEX('::',TrackingTrackerName)+2, LEN(TrackingTrackerName)) 
    						ELSE SUBSTRING(TrackingTrackerName, CHARINDEX('::',TrackingTrackerName)+2,CHARINDEX('::',SUBSTRING(TrackingTrackerName, CHARINDEX('::',TrackingTrackerName)+2, LEN(TrackingTrackerName)))-1)
    						END 
						ELSE NULL 
				END AS TrackingTrackerName2,

				CASE when TrackingTrackerName like '%::%'
						THEN CASE WHEN CHARINDEX('::',SUBSTRING(TrackingTrackerName, CHARINDEX('::',TrackingTrackerName)+2, LEN(TrackingTrackerName))) = 0 
    						THEN SUBSTRING(TrackingTrackerName, CHARINDEX('::',TrackingTrackerName)+2, LEN(TrackingTrackerName)) 
							ELSE SUBSTRING(TrackingTrackerName, CHARINDEX('::',SUBSTRING(TrackingTrackerName, CHARINDEX('::',TrackingTrackerName)+2, LEN(TrackingTrackerName)))-1 + CHARINDEX('::',TrackingTrackerName)+4, 
								CHARINDEX('::', SUBSTRING(TrackingTrackerName, CHARINDEX('::',SUBSTRING(TrackingTrackerName, CHARINDEX('::',TrackingTrackerName)+2, LEN(TrackingTrackerName)))-1 + CHARINDEX('::',TrackingTrackerName)+5, LEN(TrackingTrackerName))))
    						END 
						ELSE NULL 
				END AS TrackingTrackerName3,

				CASE when TrackingTrackerName like '%::%'
						THEN CASE WHEN CHARINDEX('::',SUBSTRING(TrackingTrackerName, CHARINDEX('::',TrackingTrackerName)+2, LEN(TrackingTrackerName))) = 0 
    						THEN SUBSTRING(TrackingTrackerName, CHARINDEX('::',TrackingTrackerName)+2, LEN(TrackingTrackerName)) 
							ELSE SUBSTRING(TrackingTrackerName, CHARINDEX('::', SUBSTRING(TrackingTrackerName, CHARINDEX('::',SUBSTRING(TrackingTrackerName, 
									CHARINDEX('::',TrackingTrackerName)+2, LEN(TrackingTrackerName)))-1 + CHARINDEX('::',TrackingTrackerName)+4, LEN(TrackingTrackerName))) 
									+ CHARINDEX('::',SUBSTRING(TrackingTrackerName, CHARINDEX('::',TrackingTrackerName)+2, LEN(TrackingTrackerName))) + CHARINDEX('::',TrackingTrackerName)+4, LEN(TrackingTrackerName))
    						END 
						ELSE NULL 
				END AS TrackingTrackerName4,

				--Specific
				TrackingDcpKeyword,

				--Updated Values
				CONVERT(VARCHAR(100), NULL)					Updated_Promo, 
				CONVERT(VARCHAR(100), NULL)					Updated_ReferralDevice, 
				CONVERT(VARCHAR(100), NULL)					Updated_ReferralChannel, 
				CONVERT(VARCHAR(100), NULL)					Updated_ReferralPublisher,
				CONVERT(VARCHAR(100),NULL)					Updated_ReferralTargeting,
				CONVERT(VARCHAR(100),NULL)					Updated_ReferralSpecifics
			
			INTO   #Account 
			FROM   [$(Dimensional)].Dimension.Account m
			INNER JOIN [$(Dimensional)].Dimension.Channel c On m.AccountOpenedChannelKey = c.ChannelKey
			WHERE (@BatchKey = -1 OR m.ModifiedBatchKey = @BatchKey)
				 AND TrackingChannel IN ('Adjust SDK iOS','Adjust SDK Android')


		EXEC [Control].Sp_Log @BatchKey,@Me, 'Clean Promo', 'Start', @RowsProcessed = @@ROWCOUNT 

			UPDATE A
			SET A.Updated_Promo = CASE WHEN B.Promo = '[Use SourceSitePromoName]' THEN ISNULL(a.SourceSitePromoName,'Unknown') ELSE B.Promo END 
			FROM #Account A
			INNER JOIN #MDM_Promo B
			ON		ISNULL(A.TrackingChannel, '!')		= ISNULL(B.TrackingChannel, '!')
				AND ISNULL(A.TrackingDcpBtag, '!')		= ISNULL( B.TrackingDcpBtag, '!')
				AND ISNULL(A.SourceSitePromoName, '!')	= ISNULL(B.SourceSitePromoName, '!')


		EXEC [Control].Sp_Log @BatchKey,@Me, 'Clean Channel', 'Start', @RowsProcessed = @@ROWCOUNT 

			UPDATE A
			SET A.Updated_ReferralChannel = B.Channel
			FROM #Account A
			INNER JOIN #MDM_Channel B 
			ON	A.TrackingTrackerName = B.TrackingTrackerName
		
		

		set @RowCount = @@RowCount

		EXEC [Control].Sp_Log @BatchKey,@Me, 'Clean Publisher', 'Start', @RowsProcessed = @@ROWCOUNT 

			UPDATE A
			SET A.Updated_ReferralPublisher = B.Publisher
			FROM #Account A
			INNER JOIN #MDM_Publisher B
			ON	A.TrackingTrackerName = B.TrackingTrackerName
			WHERE  B.TrackingTrackerName not like 'On Site %'

		set @RowCount = @@RowCount

			UPDATE A
			SET A.Updated_ReferralPublisher = B.Publisher
			FROM #Account A
			INNER JOIN #MDM_Publisher B
			ON	ISNULL(A.TrackingTrackerName, '!') = ISNULL(B.TrackingTrackerName, '!')
			AND ISNULL(A.TrackingDcpPublisher, '!') = ISNULL(B.DCPPublisher, '!')
			AND B.TrackingTrackerName = 'On Site - Affiliate'
			
		set @RowCount = @RowCount + @@RowCount

			UPDATE A
			SET A.Updated_ReferralPublisher = B.Publisher
			FROM #Account A
			INNER JOIN #MDM_Publisher B
			ON	ISNULL(A.TrackingTrackerName, '!') like ISNULL(B.TrackingTrackerName, '!')
			AND ISNULL(A.TrackingDcpPublisher, '!') like ISNULL(B.DCPPublisher, '!')
			WHERE B.TrackingTrackerName <> 'On Site - Affiliate'
			AND A.Updated_ReferralPublisher is null


		set @RowCount = @RowCount + @@RowCount

			UPDATE A
			SET A.Updated_ReferralPublisher = A.TrackingDCPPublisher
			FROM #Account A
			INNER JOIN #MDM_Publisher B
			ON	ISNULL(A.TrackingTrackerName, '!') like ISNULL(B.TrackingTrackerName, '!')
			WHERE B.TrackingTrackerName <> 'On Site - Affiliate'
			AND A.Updated_ReferralPublisher is null

		set @RowCount = @RowCount + @@RowCount

		EXEC [Control].Sp_Log @BatchKey,@Me, 'Clean Device', 'Start', @RowsProcessed = @RowCount 

			UPDATE A
			SET A.Updated_ReferralDevice = B.ReferralDevice
			FROM #Account A
			INNER JOIN #MDM_Device B
			ON	ISNULL(A.TrackingChannel, '!') = ISNULL(B.TrackingChannel, '!')
			AND ISNULL(A.ChannelTypeName, '!') = ISNULL(B.ChannelTypeName, '!')


		EXEC [Control].Sp_Log @BatchKey,@Me, 'Clean Targeting', 'Start', @RowsProcessed = @@ROWCOUNT 

			UPDATE A
			SET A.Updated_ReferralTargeting = B.Targeting
			FROM #Account A
			INNER JOIN #MDM_Targeting B
			ON	ISNULL(B.TrackingDcpCampaign, '!') LIKE  ISNULL(A.TrackingDcpCampaign, '!') 
			AND ISNULL(B.TrackingTrackerName, '!') LIKE ISNULL(A.TrackingTrackerName, '!') 
			AND ISNULL(B.TrackingTrackerName2, '!') LIKE ISNULL(A.TrackingTrackerName2, '!') 
			Where B.TrackingTrackerName3 IS NULL
		
		set @RowCount = @@RowCount
			
			UPDATE A
			SET A.Updated_ReferralTargeting = B.Targeting
			FROM #Account A
			INNER JOIN #MDM_Targeting B
			ON	ISNULL(B.TrackingDcpCampaign, '!') LIKE  ISNULL(A.TrackingDcpCampaign, '!') 
			AND ISNULL(B.TrackingTrackerName, '!') LIKE ISNULL(A.TrackingTrackerName, '!') 
			AND ISNULL(B.TrackingTrackerName2, '!') LIKE ISNULL(A.TrackingTrackerName2, '!') 
			AND ISNULL(B.TrackingTrackerName3, '!') LIKE ISNULL(A.TrackingTrackerName3, '!') 
			Where B.TrackingTrackerName3 IS NOT NULL

		set @RowCount = @RowCount + @@RowCount

		EXEC [Control].Sp_Log @BatchKey,@Me, 'Clean Specifics', 'Start', @RowsProcessed = @RowCount 

		UPDATE A
			SET A.Updated_ReferralSpecifics = CASE WHEN B.Specifics = '[Use TrackingTrackerName_4thpart]'  THEN ISNULL(B.TrackingTrackerName4,'Unknown') ELSE B.Specifics END 
			FROM #Account A
			INNER JOIN #MDM_Specifics B
			ON	ISNULL(B.TrackingTrackerName, '!')	= ISNULL(A.TrackingTrackerName, '!') 
			AND ISNULL(B.TrackingDcpKeyword, '!')	= ISNULL(A.TrackingDcpKeyword, '!') 
			AND ISNULL(B.TrackingTrackerName2, '!') = ISNULL(A.TrackingTrackerName2, '!') 
			AND ISNULL(B.TrackingTrackerName3, '!') = ISNULL(A.TrackingTrackerName3, '!') 
			AND ISNULL(B.TrackingTrackerName4, '!') = ISNULL(A.TrackingTrackerName4, '!') 


		EXEC [Control].Sp_Log @BatchKey,@Me, 'Pending Records', 'Start', @RowsProcessed = @@RowCount 
		
		UPDATE A
			SET 
				A.Updated_Promo					=  ISNULL(Updated_Promo				, 'Pending'),
				A.Updated_ReferralDevice		=  ISNULL(Updated_ReferralDevice	, 'Pending'),
				A.Updated_ReferralChannel		=  ISNULL(Updated_ReferralChannel	, 'Pending'),
				A.Updated_ReferralPublisher		=  ISNULL(Updated_ReferralPublisher	, 'Pending'),
				A.Updated_ReferralTargeting		=  ISNULL(Updated_ReferralTargeting	, 'Pending'),
				A.Updated_ReferralSpecifics		=  ISNULL(Updated_ReferralSpecifics	, 'Pending')
			FROM #Account A
			WHERE 
				A.Updated_Promo				   IS NULL OR
				A.Updated_ReferralDevice	   IS NULL OR
				A.Updated_ReferralChannel	   IS NULL OR
				A.Updated_ReferralPublisher	   IS NULL OR
				A.Updated_ReferralTargeting	   IS NULL OR
				A.Updated_ReferralSpecifics	   IS NULL


		EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start', @RowsProcessed = @@RowCount 

	  IF @DryRun = 0 

	  BEGIN
	  
		  UPDATE u 
		  SET		u.Promo						= m.Updated_Promo,
					u.ReferralDevice			= m.Updated_ReferralDevice,
					u.ReferralChannel			= m.Updated_ReferralChannel,
					u.ReferralPublisher			= m.Updated_ReferralPublisher,
					u.ReferralSpecifics			= m.Updated_ReferralSpecifics,
					u.ReferralTargeting			= m.Updated_ReferralTargeting
		FROM   [$(Dimensional)].Dimension.Account u 
				 INNER JOIN #Account m ON u.AccountID = m.AccountID
		 WHERE										
				 (
							Isnull(u.Promo, '!')				<> Isnull(m.Updated_Promo, '!') 
					  OR	Isnull(u.ReferralDevice, '!')		<> Isnull(m.Updated_ReferralDevice, '!') 
					  OR	Isnull(u.ReferralChannel, '!')		<> Isnull(m.Updated_ReferralChannel, '!') 
					  OR	Isnull(u.ReferralPublisher, '!')	<> Isnull(m.Updated_ReferralPublisher, '!') 
					  OR	Isnull(u.ReferralSpecifics, '!')	<> Isnull(m.Updated_ReferralSpecifics, '!') 
					  OR	Isnull(u.ReferralTargeting, '!')	<> Isnull(m.Updated_ReferralTargeting, '!') 
					)
	  
	  EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @@RowCount 
	  
	  END
	  
	  ELSE
	
		Begin

		SELECT * 
		FROM    #Account
		 WHERE									
					(
							Isnull(Promo, '!')				<> Isnull(Updated_Promo, '!') 
					  OR	Isnull(ReferralDevice, '!')		<> Isnull(Updated_ReferralDevice, '!') 
					  OR	Isnull(ReferralChannel, '!')	<> Isnull(Updated_ReferralChannel, '!') 
					  OR	Isnull(ReferralPublisher, '!')	<> Isnull(Updated_ReferralPublisher, '!') 
					  OR	Isnull(ReferralSpecifics, '!')	<> Isnull(Updated_ReferralSpecifics, '!') 
					  OR	Isnull(ReferralTargeting, '!')	<> Isnull(Updated_ReferralTargeting, '!') 
					)

		EXEC [Control].Sp_Log @BatchKey,@Me, 'DryRun', 'Success', @RowsProcessed = @@ROWCOUNT

		End
	END try 

  BEGIN catch 
      DECLARE @ErrorMessage VARCHAR(255) = Error_message(); 

      EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage; 

      THROW; 
  END catch 






