﻿CREATE PROC [Cleanse].[SP_Event_Lottery]
(
	@BatchKey		int = 0,
	@DryRun			bit = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount int

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Cache', 'Start';

	SELECT	CONVERT(int,EventKey) EventKey, SourceEvent, SourcePrizePool, SourceEventDate, Event, ScheduledDateTime, ScheduledDayKey, ResultedDateTime, 
			Grade, Distance, PrizePool, RaceGroup, RaceClass, HybridPricingTemplate, WeightType, TrackCondition, EventWeather, 
			EventAbandoned, EventAbandonedDateTime, LiveVideoStreamed, LiveScoreBoard, Round, RoundSequence, 
			Competition, CompetitionSeason, CompetitionStartDate, CompetitionEndDate, 
			VenueId, Venue, VenueType, VenueStateCode, VenueState, VenueCountryCode, VenueCountry, FirstDate, FromDate
	INTO	#Cache
	FROM	[$(Dimensional)].Dimension.Event
	WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0) and ClassID = -10
	OPTION (RECOMPILE, IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)

	EXEC [Control].Sp_Log @BatchKey,@Me,'CacheEarlierMissingEventDates','Start', @RowsProcessed = @@ROWCOUNT

	INSERT	#Cache
	SELECT	EventKey, SourceEvent, SourcePrizePool, SourceEventDate, Event, ScheduledDateTime, ScheduledDayKey, ResultedDateTime, 
			Grade, Distance, PrizePool, RaceGroup, RaceClass, HybridPricingTemplate, WeightType, TrackCondition, EventWeather, 
			EventAbandoned, EventAbandonedDateTime, LiveVideoStreamed, LiveScoreBoard, Round, RoundSequence, 
			Competition, CompetitionSeason, CompetitionStartDate, CompetitionEndDate, 
			VenueId, Venue, VenueType, VenueStateCode, VenueState, VenueCountryCode, VenueCountry, FirstDate, FromDate
	FROM	[$(Dimensional)].Dimension.Event
	WHERE	(ModifiedBatchKey < @BatchKey OR @BatchKey = 0)
			AND ClassID = -10 AND SourceEventDate IS NULL AND ScheduledDateTime IS NULL
			AND SourceEvent IN (SELECT	DISTINCT SourceEvent FROM #Cache where SourceEventDate IS NOT NULL)
			AND EventKey NOT IN (SELECT	EventKey FROM #Cache)
	OPTION (RECOMPILE, IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)

	EXEC [Control].Sp_Log @BatchKey,@Me,'Dimension','Start', @RowsProcessed = @@ROWCOUNT

	SELECT * INTO #Dimension FROM #Cache

	EXEC [Control].Sp_Log @BatchKey,@Me,'ScheduledDateTime','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	e
	SET		ScheduledDateTime = COALESCE(e.SourceEventDate, x.ScheduledDateTime, e.ResultedDateTime)
	FROM	#Cache e
			LEFT HASH JOIN	(	SELECT	e.EventKey, MIN(e1.SourceEventDate) ScheduledDateTime
								FROM	#Cache e
										INNER JOIN [$(Dimensional)].Dimension.Event e1 ON e1.SourceEvent = e.SourceEvent AND e1.SourceEventDate > e.FirstDate AND e1.SourceEventDate <= ISNULL(e.ResultedDateTime,CONVERT(datetime2(3),'9999-12-31'))
								WHERE	e.SourceEventDate IS NULL
								GROUP BY e.EventKey
							) x ON x.EventKey = e.EventKey
	WHERE	ISNULL(e.ScheduledDateTime, CONVERT(datetime2(3),'1900-01-01')) <> COALESCE(e.SourceEventDate, x.ScheduledDateTime, e.ResultedDateTime)
	OPTION (RECOMPILE, IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)

	EXEC [Control].Sp_Log @BatchKey,@Me,'ScheduledDayKey','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	e
	SET		ScheduledDayKey = ISNULL(d.DayKey,-1)
	FROM	#Cache e
			LEFT JOIN [$(Dimensional)].Dimension.DayZone d ON d.MasterDayText = CONVERT(varchar(11),CONVERT(DATE, e.ScheduledDateTime)) AND d.FromDate <= e.ScheduledDateTime AND d.ToDate > e.ScheduledDateTime
	WHERE	ISNULL(ScheduledDayKey,-99) <> ISNULL(d.DayKey,-1)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log @BatchKey,@Me,'Others','Start', @RowsProcessed = @@ROWCOUNT

	;WITH Ref (		SourceEvent,		Competition,				StateCode,		State,					CountryCode,	Country,			Cycle,			DarwinDayOffset	) AS
		(	SELECT 'austriaLotto',		'Austrian Lotto',			'N/A',			'Outside Australia',	'AT',			'Austria',			'Weekly',		-1				UNION ALL
			SELECT 'cash4Life',			'Cash4Life',				'N/A',			'Outside Australia',	'US',			'United States',	'Weekly',		-1				UNION ALL
			SELECT 'elGordo',			'El Gordo Xmas Lottery',	'N/A',			'Outside Australia',	'ES',			'Spain',			'Annual',		-1				UNION ALL
			SELECT 'elGordoPrimitiva',	'El Gordo Primitiva',		'N/A',			'Outside Australia',	'ES',			'Spain',			'Weekly',		-1				UNION ALL
			SELECT 'elNino',			'El Nino',					'N/A',			'Outside Australia',	'ES',			'Spain',			'Annual',		-1				UNION ALL
			SELECT 'euroJackpot',		'EuroJackpot',				'N/A',			'Outside Australia',	'FI',			'Finland',			'Weekly',		-1				UNION ALL
			SELECT 'euroMillions',		'EuroMillions',				'N/A',			'Outside Australia',	'FR',			'France',			'Weekly',		-1				UNION ALL
			SELECT 'frenchLotto',		'French Lotto',				'N/A',			'Outside Australia',	'FR',			'France',			'Weekly',		-1				UNION ALL
			SELECT 'german6aus49',		'German Lotto',				'N/A',			'Outside Australia',	'DE',			'Germany',			'Weekly',		-1				UNION ALL
			SELECT 'germanKeno',		'German Keno',				'N/A',			'Outside Australia',	'DE',			'Germany',			'Daily',		-1				UNION ALL
			SELECT 'irishLotto',		'Irish Lotto',				'N/A',			'Outside Australia',	'IE',			'Ireland',			'Weekly',		-1				UNION ALL
			SELECT 'keno247',			'Rapid Keno',				'N/A',			'Outside Australia',	'US',			'United States',	'Daily',		-1				UNION ALL
			SELECT 'keNow',				'KeNow',					'N/A',			'Outside Australia',	'PL',			'Poland',			'Daily',		-1				UNION ALL
			SELECT 'markSix',			'HK Mark 6',				'N/A',			'Outside Australia',	'HK',			'Hong Kong',		'Weekly',		0				UNION ALL
			SELECT 'megaMillions',		'MegaMillions',				'N/A',			'Outside Australia',	'US',			'United States',	'Weekly',		-1				UNION ALL
			SELECT 'megaSena',			'Brazillian MegaSena',		'N/A',			'Outside Australia',	'BR',			'Brazil',			'Weekly',		-1				UNION ALL
			SELECT 'monWedOz',			'Oz Mon and Wed Lotto',		'ALL',			'All States',			'AU',			'Australia',		'Weekly',		0				UNION ALL
			SELECT 'multiMulti',		'Multi Keno',				'N/A',			'Outside Australia',	'PL',			'Poland',			'Daily',		-1				UNION ALL
			SELECT 'ozLotto',			'Oz Tue Lotto',				'ALL',			'All States',			'AU',			'Australia',		'Weekly',		0				UNION ALL
			SELECT 'ozPowerBall',		'Oz Thu Lotto',				'ALL',			'All States',			'AU',			'Australia',		'Weekly',		0				UNION ALL
			SELECT 'polishLotto',		'Polish Lotto',				'N/A',			'Outside Australia',	'PL',			'Poland',			'Weekly',		-1				UNION ALL
			SELECT 'polishMiniLotto',	'Polish Mini Lotto',		'N/A',			'Outside Australia',	'PL',			'Poland',			'Daily',		-1				UNION ALL
			SELECT 'powerBall',			'PowerBall',				'N/A',			'Outside Australia',	'US',			'United States',	'Weekly',		-1				UNION ALL
			SELECT 'saturdayOz',		'Oz Sat Lotto',				'ALL',			'All States',			'AU',			'Australia',		'Weekly',		0				UNION ALL
			SELECT 'summerGordo',		'El Gordo del Verano',		'N/A',			'Outside Australia',	'ES',			'Spain',			'Annual',		-1				UNION ALL
			SELECT 'superEnalotto',		'SuperEnalotto',			'N/A',			'Outside Australia',	'IT',			'Italy',			'Weekly',		-1				UNION ALL
			SELECT 'swedishLotto',		'Swedish Lotto',			'N/A',			'Outside Australia',	'SE',			'Sweden',			'Weekly',		-1				UNION ALL
			SELECT 'uKLotto',			'UK Lotto',					'N/A',			'Outside Australia',	'UK',			'United Kingdom',	'Weekly',		-1
		)
	UPDATE	u
		SET	Event = UpdatedEvent, 
			Grade = UpdatedGrade, 
			Distance = UpdatedDistance, 
			PrizePool = UpdatedPrizePool, 
			RaceGroup = UpdatedRaceGroup, 
			RaceClass = UpdatedRaceClass, 
			HybridPricingTemplate = UpdatedHybridPricingTemplate, 
			WeightType = UpdatedWeightType, 
			TrackCondition = UpdatedTrackCondition, 
			EventWeather = UpdatedEventWeather, 
			EventAbandoned = UpdatedEventAbandoned, 
			EventAbandonedDateTime = UpdatedEventAbandonedDateTime, 
			LiveVideoStreamed = UpdatedLiveVideoStreamed, 
			LiveScoreBoard = UpdatedLiveScoreBoard, 
			Round = UpdatedRound, 
			RoundSequence = UpdatedRoundSequence, 
			Competition = UpdatedCompetition, 
			CompetitionSeason = UpdatedCompetitionSeason, 
			CompetitionStartDate = UpdatedCompetitionStartDate, 
			CompetitionEndDate = UpdatedCompetitionEndDate, 
			VenueId = UpdatedVenueId, 
			Venue = UpdatedVenue,
			VenueType = UpdatedVenueType, 
			VenueStateCode = UpdatedVenueStateCode, 
			VenueState = UpdatedVenueState, 
			VenueCountryCode = UpdatedVenueCountryCode, 
			VenueCountry = UpdatedVenueCountry
	FROM	(	SELECT	Event, ScheduledDateTime, ResultedDateTime, Grade, Distance, PrizePool, RaceGroup, RaceClass, 
						HybridPricingTemplate, WeightType, TrackCondition, EventWeather, 
						EventAbandoned, EventAbandonedDateTime, LiveVideoStreamed, LiveScoreBoard, 
						Round, RoundSequence, 
						e.Competition, CompetitionSeason, CompetitionStartDate, CompetitionEndDate, 
						VenueId, Venue, VenueType, VenueStateCode, VenueState, VenueCountryCode, VenueCountry,
						r.Competition UpdatedEvent, 'N/A' UpdatedGrade, 'N/A' UpdatedDistance, SourcePrizePool UpdatedPrizePool, 'N/A' UpdatedRaceGroup, 'N/A' UpdatedRaceClass, 
						'N/A' UpdatedHybridPricingTemplate, 'N/A' UpdatedWeightType, 'N/A' UpdatedTrackCondition, 'N/A' UpdatedEventWeather, 
						'No' UpdatedEventAbandoned, NULL UpdatedEventAbandonedDateTime, 'N/A' UpdatedLiveVideoStreamed, 'N/A' UpdatedLiveScoreBoard, 
						r.Competition UpdatedRound, 0 UpdatedRoundSequence, 
						r.Competition UpdatedCompetition, CASE r.Cycle WHEN 'Weekly' THEN DATENAME(WEEKDAY,DATEADD(DAY, r.DarwinDayOffset, e.ScheduledDateTime)) ELSE r.Cycle END UpdatedCompetitionSeason, NULL UpdatedCompetitionStartDate, NULL UpdatedCompetitionEndDate, 
						NULL UpdatedVenueId, r.Country UpdatedVenue, 'N/A' UpdatedVenueType, r.StateCode UpdatedVenueStateCode, r.State UpdatedVenueState, r.CountryCode UpdatedVenueCountryCode, r.Country UpdatedVenueCountry 
				FROM	#Cache e
						LEFT JOIN Ref r ON r.SourceEvent = e.SourceEvent
			) u
	WHERE	ISNULL(Event,'!') <> ISNULL(UpdatedEvent,'!') 
			OR ISNULL(Grade,'!') <> ISNULL(UpdatedGrade,'!') 
			OR ISNULL(Distance,'!') <> ISNULL(UpdatedDistance,'!') 
			OR ISNULL(PrizePool,-1) <> ISNULL(UpdatedPrizePool,-1) 
			OR ISNULL(RaceGroup,'!') <> ISNULL(UpdatedRaceGroup,'!') 
			OR ISNULL(RaceClass,'!') <> ISNULL(UpdatedRaceClass,'!') 
			OR ISNULL(HybridPricingTemplate,'!') <> ISNULL(UpdatedHybridPricingTemplate,'!') 
			OR ISNULL(WeightType,'!') <> ISNULL(UpdatedWeightType,'!') 
			OR ISNULL(TrackCondition,'!') <> ISNULL(UpdatedTrackCondition,'!') 
			OR ISNULL(EventWeather,'!') <> ISNULL(UpdatedEventWeather,'!') 
			OR ISNULL(EventAbandoned,'!') <> ISNULL(UpdatedEventAbandoned,'!') 
			OR ISNULL(EventAbandonedDateTime,CONVERT(datetime2(3),'1900-01-01')) <> ISNULL(UpdatedEventAbandonedDateTime,CONVERT(datetime2(3),'1900-01-01')) 
			OR ISNULL(LiveVideoStreamed,'!') <> ISNULL(UpdatedLiveVideoStreamed,'!') 
			OR ISNULL(LiveScoreBoard,'!') <> ISNULL(UpdatedLiveScoreBoard,'!') 
			OR ISNULL(Round,'!') <> ISNULL(UpdatedRound,'!') 
			OR ISNULL(RoundSequence,-1) <> ISNULL(UpdatedRoundSequence,-1) 
			OR ISNULL(Competition,'!') <> ISNULL(UpdatedCompetition,'!') 
			OR ISNULL(CompetitionSeason,'!') <> ISNULL(UpdatedCompetitionSeason,'!') 
			OR ISNULL(CompetitionStartDate,CONVERT(datetime2(3),'1900-01-01')) <> ISNULL(UpdatedCompetitionStartDate,CONVERT(datetime2(3),'1900-01-01')) 
			OR ISNULL(CompetitionEndDate,CONVERT(datetime2(3),'1900-01-01')) <> ISNULL(UpdatedCompetitionEndDate,CONVERT(datetime2(3),'1900-01-01')) 
			OR ISNULL(VenueId,-1) <> ISNULL(UpdatedVenueId,-1) 
			OR ISNULL(Venue,'!') <> ISNULL(UpdatedVenue,'!') 
			OR ISNULL(VenueType,'!') <> ISNULL(UpdatedVenueType,'!') 
			OR ISNULL(VenueStateCode,'!') <> ISNULL(UpdatedVenueStateCode,'!') 
			OR ISNULL(VenueState,'!') <> ISNULL(UpdatedVenueState,'!') 
			OR ISNULL(VenueCountryCode,'!') <> ISNULL(UpdatedVenueCountryCode,'!') 
			OR ISNULL(VenueCountry,'!') <> ISNULL(UpdatedVenueCountry,'!')

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Differences', 'Start', @RowsProcessed = @@ROWCOUNT

	SELECT	EventKey, Event, ScheduledDateTime, ScheduledDayKey, 
			Grade, Distance, PrizePool, RaceGroup, RaceClass, HybridPricingTemplate, WeightType, TrackCondition, EventWeather, 
			EventAbandoned, EventAbandonedDateTime, LiveVideoStreamed, LiveScoreBoard, Round, RoundSequence, 
			Competition, CompetitionSeason, CompetitionStartDate, CompetitionEndDate, 
			VenueId, Venue, VenueType, VenueStateCode, VenueState, VenueCountryCode, VenueCountry, 
			MAX(src) src
	INTO	#Differences
	FROM	(	SELECT *, 'S' src FROM #Cache
				UNION ALL
				SELECT *, 'D' src FROM #Dimension
			) x
	GROUP BY EventKey, Event, ScheduledDateTime, ScheduledDayKey, 
			Grade, Distance, PrizePool, RaceGroup, RaceClass, HybridPricingTemplate, WeightType, TrackCondition, EventWeather, 
			EventAbandoned, EventAbandonedDateTime, LiveVideoStreamed, LiveScoreBoard, Round, RoundSequence, 
			Competition, CompetitionSeason, CompetitionStartDate, CompetitionEndDate, 
			VenueId, Venue, VenueType, VenueStateCode, VenueState, VenueCountryCode, VenueCountry
	HAVING	COUNT(*) = 1
	OPTION (RECOMPILE)

	IF @DryRun = 1 SELECT TOP 10000 CASE WHEN src = 'S' THEN 'U+' ELSE 'U-' END act, * FROM #Differences ORDER BY EventKey, src desc

	SELECT	* INTO #Prepare FROM #Differences WHERE	src <> 'D' -- Discard the 'before' version record of Update

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start', @RowsProcessed = @@ROWCOUNT

	IF @DryRun = 0 
		BEGIN
			UPDATE	d
				SET	Event = p.Event,
					ScheduledDateTime = p.ScheduledDateTime,
					ScheduledDayKey = p.ScheduledDayKey,
					Grade = p.Grade, 
					Distance = p.Distance, 
					PrizePool = p.PrizePool, 
					RaceGroup = p.RaceGroup, 
					RaceClass = p.RaceClass, 
					HybridPricingTemplate = p.HybridPricingTemplate, 
					WeightType = p.WeightType, 
					TrackCondition = p.TrackCondition, 
					EventWeather = p.EventWeather, 
					EventAbandoned = p.EventAbandoned, 
					EventAbandonedDateTime = p.EventAbandonedDateTime, 
					LiveVideoStreamed = p.LiveVideoStreamed, 
					LiveScoreBoard = p.LiveScoreBoard, 
					Round = p.Round, 
					RoundSequence = p.RoundSequence, 
					Competition = p.Competition, 
					CompetitionSeason = p.CompetitionSeason, 
					CompetitionStartDate = p.CompetitionStartDate, 
					CompetitionEndDate = p.CompetitionEndDate, 
					VenueId = p.VenueId, 
					Venue = p.Venue,
					VenueType = p.VenueType, 
					VenueStateCode = p.VenueStateCode, 
					VenueState = p.VenueState, 
					VenueCountryCode = p.VenueCountryCode, 
					VenueCountry = p.VenueCountry,
					CleansedDate = GETDATE(),
					CleansedBatchKey = @BatchKey,
					CleansedBy = @Me
			FROM	[$(Dimensional)].Dimension.Event d
					INNER JOIN #Prepare p ON p.EventKey = d.EventKey
			OPTION(RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
