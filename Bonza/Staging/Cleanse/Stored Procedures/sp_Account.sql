﻿CREATE PROCEDURE [Cleanse].[sp_Account]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS

EXEC [Cleanse].[SP_Account_ManagedBy]						@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT -- NOTE - This has to follow the clensing of the Structure dimension as the results of that are propogated into the account dimension
EXEC [Cleanse].[SP_Account_Type]							@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[SP_Account_Opened]							@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[SP_Account_Ledger]							@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[SP_Account_Card]							@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[SP_Account_TrafficSource]					@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[SP_Account_Customer]						@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[SP_Account_Name]							@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[SP_Account_Address]							@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[SP_Account_Phone]							@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[SP_Account_Email]							@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[SP_Account_Referral]						@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[SP_Account_Reward]							@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[Sp_Account_Referral_Desktop_Mobile]			@BatchKey, @Dryrun = 0, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[Sp_Account_Referral_iOS_Android]			@BatchKey, @Dryrun = 0, @RowsProcessed = @RowsProcessed OUTPUT
EXEC [Cleanse].[Sp_Account_SnowflakeKeys]					@BatchKey, @Increment, @RowsProcessed = @RowsProcessed OUTPUT

