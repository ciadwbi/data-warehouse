﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 18/11/2014
-- Description:	Insert eventNames,roundNames,CompetitionNames for Cricket event type
-- =============================================

CREATE PROCEDURE [Cleanse].[SP_Event_Cricket]
	-- Add the parameters for the stored procedure here
	@BatchKey int 
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN
BEGIN TRY

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	
--======================================================================================================================================================================   
--                                                PART 1: Insert Round Names in to Event Dimension for 'Cricket' event type
--====================================================================================================================================================================== 

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 1','Start'

Update	[$(Dimensional)].Dimension.Event
set		round=(Case 
					When (SourceEvent like '%Team[1-9]%' or isnull(ParentEvent,'') like '%Team[1-9]%'or isnull(GrandparentEvent,'') like '%Team[1-9]%'
						or SourceEvent like '%Team[a-z]%' or isnull(ParentEvent,'') like '%Team[a-z]%'or isnull(GrandparentEvent,'') like '%Team[a-z]%'
						or SourceEvent like '%Team [a-z] %' or isnull(ParentEvent,'') like '%Team [a-z] %'or isnull(GrandparentEvent,'') like '%Team [a-z] %'
						or SourceEvent like '%Team [a-z]' or isnull(ParentEvent,'') like '%Team [a-z]'or isnull(GrandparentEvent,'') like '%Team [a-z]'
							or SourceEvent like '%Template%' or isnull(ParentEvent,'') like '%Template%'or isnull(GrandparentEvent,'') like '%Template%'
							or SourceEvent like '%abandoned%' or isnull(ParentEvent,'') like '%abandoned%'or isnull(GrandparentEvent,'') like '%abandoned%'
							or SourceEvent like '%deleted%' or isnull(ParentEvent,'') like '%deleted%'or isnull(GrandparentEvent,'') like '%deleted%'
							or SourceEvent like '%Dump%' or isnull(ParentEvent,'') like '%Dump%'or isnull(GrandparentEvent,'') like '%Dump%'
							or SourceEvent like '%[a,f] v [b,g]%' or isnull(ParentEvent,'') like '%[a,f] v [b,g]%'or isnull(GrandparentEvent,'') like '%[a,f] v [b,g]%'
							or SourceEvent like '%[a,f] vs [b,g]%' or isnull(ParentEvent,'') like '%[a,f] vs [b,g]%'or isnull(GrandparentEvent,'') like '%[a,f] vs [b,g]%')
						Then 'NA'
				When (isnull(GrandparentEvent,'') like '%Final%'  or isnull(GrandparentEvent,'') like '%[1-9][a-z][a-z] ODI%' 
				or isnull(GrandparentEvent,'') like '%[1-9][a-z][a-z] Test%' or isnull(GrandparentEvent,'') like '%[1-9][a-z][a-z] T20%'
				or isnull(GrandparentEvent,'') like '%[1-9][a-z][a-z] Match%' or isnull(GrandparentEvent,'') like '%Qualifier [1-9]%')
				Then (Case 
						When (isnull(GrandparentEvent,'') like '%Semi_Final%' )
						Then 'Semi Final'
						When (isnull(GrandparentEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(GrandparentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(GrandparentEvent,'') like '%[1-9][a-z][a-z] ODI%' )
						Then Substring(isnull(GrandparentEvent,''),Patindex('%[1-9][a-z][a-z] ODI%',isnull(GrandparentEvent,'')),7)
						When (isnull(GrandparentEvent,'') like '%[1-9][a-z][a-z] Test%' )
						Then Substring(isnull(GrandparentEvent,''),Patindex('%[1-9][a-z][a-z] Test%',isnull(GrandparentEvent,'')),8)
						When (isnull(GrandparentEvent,'') like '%[1-9][a-z][a-z] Match%' )
						Then Substring(isnull(GrandparentEvent,''),Patindex('%[1-9][a-z][a-z] Match%',isnull(GrandparentEvent,'')),9)
						When (isnull(GrandparentEvent,'') like '%[1-9][a-z][a-z] T20%' )
						Then Substring(isnull(GrandparentEvent,''),Patindex('%[1-9][a-z][a-z] ODI%',isnull(GrandparentEvent,'')),7)
						When (isnull(GrandparentEvent,'') like '%Qualifier [1-9]%')
						Then Substring(isnull(GrandparentEvent,''),Patindex('%Qualifier [1-9]%',isnull(GrandparentEvent,'')),11)
						Else 'Check'
						End)
				When (isnull(ParentEvent,'') like '%Final%'  or isnull(ParentEvent,'') like '%[1-9][a-z][a-z] ODI%' 
				or isnull(ParentEvent,'') like '%[1-9][a-z][a-z] Test%' or isnull(ParentEvent,'') like '%[1-9][a-z][a-z] T20%'
				or isnull(ParentEvent,'') like '%[1-9][a-z][a-z] Match%' or isnull(ParentEvent,'') like '%Qualifier [1-9]%')
				Then (Case 
						When (isnull(ParentEvent,'') like '%Semi_Final%' )
						Then 'Semi Final'
						When (isnull(ParentEvent,'') like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (isnull(ParentEvent,'') like '%Final%')
						Then 'Final'
						When (isnull(ParentEvent,'') like '%[1-9][a-z][a-z] ODI%')
						Then Substring(isnull(ParentEvent,''),Patindex('%[1-9][a-z][a-z] ODI%',isnull(ParentEvent,'')),7)
						When (isnull(ParentEvent,'') like '%[1-9][a-z][a-z] Test%' )
						Then Substring(isnull(ParentEvent,''),Patindex('%[1-9][a-z][a-z] Test%',isnull(ParentEvent,'')),8)
						When (isnull(ParentEvent,'') like '%[1-9][a-z][a-z] Match%')
						Then Substring(isnull(ParentEvent,''),Patindex('%[1-9][a-z][a-z] Match%',isnull(ParentEvent,'')),9)
						When (isnull(ParentEvent,'') like '%[1-9][a-z][a-z] T20%')
						Then Substring(isnull(ParentEvent,''),Patindex('%[1-9][a-z][a-z] T20%',isnull(ParentEvent,'')),7)
						When (isnull(ParentEvent,'') like '%Qualifier [1-9]%')
						Then Substring(isnull(ParentEvent,''),Patindex('%Qualifier [1-9]%',isnull(ParentEvent,'')),11)
						Else 'Check'
						End)
				When (SourceEvent like '%Final%' or SourceEvent like '%[1-9][a-z][a-z] ODI%' or SourceEvent like '%[1-9][a-z][a-z] Test%'
				or SourceEvent like '%[1-9][a-z][a-z] T20%' or SourceEvent like '%[1-9][a-z][a-z] Match%' or SourceEvent like '%Qualifier [1-9]%')
				Then (Case 
						When (SourceEvent like '%Semi_Final%' )
						Then 'Semi Final'
						When (SourceEvent like '%Quarter_Final%' )
						Then 'Quarter Final'
						When (SourceEvent like '%Final%')
						Then 'Final'
						When (SourceEvent like '%[1-9][a-z][a-z] ODI%')
						Then Substring(SourceEvent,Patindex('%[1-9][a-z][a-z] ODI%',SourceEvent),7)
						When (SourceEvent like '%[1-9][a-z][a-z] test%')
						Then Substring(SourceEvent,Patindex('%[1-9][a-z][a-z] Test%',SourceEvent),8)
						When (SourceEvent like '%[1-9][a-z][a-z] Match%')
						Then Substring(SourceEvent,Patindex('%[1-9][a-z][a-z] Match%',SourceEvent),9)
						When (SourceEvent like '%[1-9][a-z][a-z] T20%')
						Then Substring(SourceEvent,Patindex('%[1-9][a-z][a-z] T20%',SourceEvent),7)
						When (SourceEvent like '%Qualifier [1-9]%')
						Then Substring(SourceEvent,Patindex('%Qualifier [1-9]%',SourceEvent),11)
						Else 'Check'
						End)
				Else  'Unknown'
				End)
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

--======================================================================================================================================================================   
--                                                PART 2: Insert Event Names in to Event Dimension for 'Cricket' event type
--====================================================================================================================================================================== 

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 2','Start',@RowsProcessed = @@RowCount

Update	[$(Dimensional)].Dimension.Event
set		Event=(Case	
					When (SourceEvent like '%Team[1-9]%' or isnull(ParentEvent,'') like '%Team[1-9]%'or isnull(GrandparentEvent,'') like '%Team[1-9]%'
						or SourceEvent like '%Team[a-z]%' or isnull(ParentEvent,'') like '%Team[a-z]%'or isnull(GrandparentEvent,'') like '%Team[a-z]%'
						or SourceEvent like '%Team [a-z] %' or isnull(ParentEvent,'') like '%Team [a-z] %'or isnull(GrandparentEvent,'') like '%Team [a-z] %'
						or SourceEvent like '%Team [a-z]' or isnull(ParentEvent,'') like '%Team [a-z]'or isnull(GrandparentEvent,'') like '%Team [a-z]'
							or SourceEvent like '%Template%' or isnull(ParentEvent,'') like '%Template%'or isnull(GrandparentEvent,'') like '%Template%'
							or SourceEvent like '%abandoned%' or isnull(ParentEvent,'') like '%abandoned%'or isnull(GrandparentEvent,'') like '%abandoned%'
							or SourceEvent like '%deleted%' or isnull(ParentEvent,'') like '%deleted%'or isnull(GrandparentEvent,'') like '%deleted%'
							or SourceEvent like '%Dump%' or isnull(ParentEvent,'') like '%Dump%'or isnull(GrandparentEvent,'') like '%Dump%'
							or SourceEvent like '%[a,f] v [b,g]%' or isnull(ParentEvent,'') like '%[a,f] v [b,g]%'or isnull(GrandparentEvent,'') like '%[a,f] v [b,g]%'
							or SourceEvent like '%[a,f] vs [b,g]%' or isnull(ParentEvent,'') like '%[a,f] vs [b,g]%'or isnull(GrandparentEvent,'') like '%[a,f] vs [b,g]%')
						Then 'NA'
					WHEN (isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '%@%' )
					THEN 
						(CASE 
							WHEN isnull(ParentEvent,'') like'% - %'			
							THEN 
								(CASE
									WHEN ((SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% vs %') or
										(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% v %') or
										(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '%@%') )
									THEN SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))-1)
									ELSE SUBString(isnull(ParentEvent,''), CHARINDEX(' - ',isnull(ParentEvent,''))+3,LEN(isnull(ParentEvent,'')))
								END)
							ELSE isnull(ParentEvent,'')
						END)
						ELSE
						(CASE
							WHEN ((SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '%@%')
									and SourceEvent not like '%Most %' and SourceEvent not like '%Top %' and SourceEvent not like '%Next %'
									and SourceEvent not like '%To %' and SourceEvent not like '%Total %' and SourceEvent not like '%First %' 
									and SourceEvent not like '%NMO %')
							THEN 
								(CASE 
									WHEN SourceEvent like'% - %'			
									THEN 
										(CASE
											WHEN ((SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% vs %') or
												(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% v %') or
												(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%@%') )
											THEN SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)-1)
											ELSE SUBString(SourceEvent, CHARINDEX(' - ',SourceEvent)+3,LEN(SourceEvent))
										END)
									ELSE SourceEvent
									END)
								ELSE 'NA'
							End)
						ENd)
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

--Selecting only required match s by truncating unnecessary information

Update	[$(Dimensional)].Dimension.Event
set		Event=(CASE
				WHEN ((SUBString(Event,1, CHARINDEX('-',Event)) like '% vs %') or
					(SUBString(Event,1, CHARINDEX('-',Event)) like '% v %') or
					(SUBString(Event,1, CHARINDEX('-',Event)) like '%@%') )
				THEN Ltrim(Rtrim(SUBString(Event,1, CHARINDEX('-',Event)-1)))
				ELSE Ltrim(Rtrim(SUBString(Event, CHARINDEX('-',Event)+1,LEN(Event))))
			   END)
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Event like '%-%' and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

--Removing erroneous data from event s

Update	[$(Dimensional)].Dimension.Event
set		Event=Substring(Event,1,CHARINDEX('(',Event)-1)
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Event like '%(%' and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

--Setting 'NA' for player s in event s

Update	[$(Dimensional)].Dimension.Event
set		Event='NA'
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Event like '%[a-z].[a-z]%' and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

--======================================================================================================================================================================   
--                                                PART 3: Insert Competition Names in to Event Dimension for 'Cricket' event type
--====================================================================================================================================================================== 

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 3','Start',@RowsProcessed = @@RowCount

Update	[$(Dimensional)].Dimension.Event
set		Competition=(Case 
						When (SourceEvent like '%Team[1-9]%' or isnull(ParentEvent,'') like '%Team[1-9]%'or isnull(GrandparentEvent,'') like '%Team[1-9]%'
						or SourceEvent like '%Team[a-z]%' or isnull(ParentEvent,'') like '%Team[a-z]%'or isnull(GrandparentEvent,'') like '%Team[a-z]%'
						or SourceEvent like '%Team [a-z] %' or isnull(ParentEvent,'') like '%Team [a-z] %'or isnull(GrandparentEvent,'') like '%Team [a-z] %'
						or SourceEvent like '%Team [a-z]' or isnull(ParentEvent,'') like '%Team [a-z]'or isnull(GrandparentEvent,'') like '%Team [a-z]'
							or SourceEvent like '%Template%' or isnull(ParentEvent,'') like '%Template%'or isnull(GrandparentEvent,'') like '%Template%'
							or SourceEvent like '%abandoned%' or isnull(ParentEvent,'') like '%abandoned%'or isnull(GrandparentEvent,'') like '%abandoned%'
							or SourceEvent like '%deleted%' or isnull(ParentEvent,'') like '%deleted%'or isnull(GrandparentEvent,'') like '%deleted%'
							or SourceEvent like '%Dump%' or isnull(ParentEvent,'') like '%Dump%'or isnull(GrandparentEvent,'') like '%Dump%'
							or SourceEvent like '%[a,f] v [b,g]%' or isnull(ParentEvent,'') like '%[a,f] v [b,g]%'or isnull(GrandparentEvent,'') like '%[a,f] v [b,g]%'
							or SourceEvent like '%[a,f] vs [b,g]%' or isnull(ParentEvent,'') like '%[a,f] vs [b,g]%'or isnull(GrandparentEvent,'') like '%[a,f] vs [b,g]%')
						Then 'NA'
						When(GrandparentEvent is null and ParentEvent is null and SourceEvent like 'Team[1-9]%')
						Then 'NA'
						When (GrandparentEvent is not null and GrandparentEvent<>'Deleted Matches' and GrandparentEvent<>'Cricket Live Betting'
								and GrandparentEvent<>'Unknown Legacy Event')
						Then (Case 
								When (GrandparentEvent like '% - %')
								Then (Case 
										When (GrandparentEvent like 'Live Betting - %')
										Then Substring(GrandparentEvent,Patindex('%Live Betting - %',GrandparentEvent)+15,Len(GrandparentEvent))
										When (GrandparentEvent like '% - Live Betting')
										Then Substring(GrandparentEvent,1,Patindex('% - Live Betting',GrandparentEvent)-1)
										When (GrandparentEvent like 'Betting Live - %')
										Then Substring(GrandparentEvent,Patindex('%Betting Live - %',GrandparentEvent)+15,Len(GrandparentEvent))
										When (GrandparentEvent like '% - Betting Live')
										Then Substring(GrandparentEvent,1,Patindex('% - Betting Live',GrandparentEvent)-1)
										When (GrandparentEvent like '% - Matches')
										Then Substring(GrandparentEvent,1,Patindex('% - Matches',GrandparentEvent)-1)
										Else GrandparentEvent
									  End)
								 Else GrandparentEvent
							   End)
						 When (ParentEvent is not null and ParentEvent<>'Deleted Matches' and ParentEvent<>'Cricket Live Betting'
								 and ParentEvent not like '%?' and ParentEvent<>'Unknown Legacy Event')
						Then (Case 
								When (ParentEvent like '% - %')
								Then (Case 
										When (ParentEvent like 'Live Betting - %')
										Then Substring(ParentEvent,Patindex('%Live Betting - %',ParentEvent)+15,Len(ParentEvent))
										When (ParentEvent like '% - Live Betting')
										Then Substring(ParentEvent,1,Patindex('% - Live Betting',ParentEvent)-1)
										When (ParentEvent like 'Betting Live - %')
										Then Substring(ParentEvent,Patindex('%Betting Live - %',ParentEvent)+15,Len(ParentEvent))
										When (ParentEvent like '% - Betting Live')
										Then Substring(ParentEvent,1,Patindex('% - Betting Live',ParentEvent)-1)
										When (ParentEvent like '% - Matches')
										Then Substring(ParentEvent,1,Patindex('% - Matches',ParentEvent)-1)
										Else ParentEvent
									  End)
								 Else ParentEvent
							   End)
						When(SourceEvent<>'Deleted Matches' and SourceEvent not like '%?' and SourceEvent<>'Unknown Legacy Event')
						Then (Case 
								When (SourceEvent like '% - %')
								Then (Case 
										When (SourceEvent like '%Live Betting - %' or SourceEvent like '%Matches - %' or
										SourceEvent like '%Futures - %' or SourceEvent like '%Semi Final% - %' or
										SourceEvent like '%Quarter Final% - %' or SourceEvent like '%Final% - %'
										 or SourceEvent like '% - %[1-9][0-9][0-9][0-9]%')
										Then Substring(SourceEvent,Patindex('% - %',SourceEvent)+3,Len(SourceEvent))
										When (SourceEvent like '% - Live Betting%' or SourceEvent like '% - Matches%' or
										SourceEvent like '% - Futures%' or SourceEvent like '% - Semi Final%' or
										SourceEvent like '% - Quarter Final%' or SourceEvent like '% - Final%'
										or SourceEvent like  '%[1-9][0-9][0-9][0-9]% - %')
										Then Substring(SourceEvent,1,Patindex('% - %',SourceEvent)-1)
										When (SourceEvent like 'ODI %- %' or SourceEvent like 'T20 %- %' or 
										SourceEvent like 'Test %- %' or SourceEvent like '% - ODI %' or
										 SourceEvent like '% - T20 %' or SourceEvent like '% - Test %')
										Then SourceEvent
										Else 'Unknown'
									 End)
								Else 'Unknown'
							  End)
						Else 'Unknown'
					ENd)								
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Removing Round  from competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition=(Case
						When Competition like '%Semi Final%'
						Then Substring(Competition,1,Patindex('%Semi Final%',Competition)-1)
						When Competition like '%Quarter Final%'
						Then Substring(Competition,1,Patindex('%Quarter Final%',Competition)-1)
						When Competition like '%Final%'
						Then Substring(Competition,1,Patindex('%Final%',Competition)-1)
						Else Competition
					  End)
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) and Competition like '%Final%'

--Removing Event information from competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition=(Case
						When Competition like '%Highest%' or Competition like '%High %'
						Then Substring(Competition,1,Patindex('%High%',Competition)-1)
						When Competition like '%Opening%'
						Then Substring(Competition,1,Patindex('%Opening%',Competition)-1)
						When Competition like '%Innings%' or Competition like '%Inns%'
						Then Substring(Competition,1,Patindex('%Inn%',Competition)-1)
						When Competition like '%Live on%' or Competition like '%Live Betting%'
						Then Substring(Competition,1,Patindex('%Live%',Competition)-1)
						When Competition like '%Man %'
						Then Substring(Competition,1,Patindex('%Man%',Competition)-1)
						When Competition like '%Mode %'
						Then Substring(Competition,1,Patindex('%Mode%',Competition)-1)
						When Competition like '%Total %'
						Then Substring(Competition,1,Patindex('%Total%',Competition)-1)
						When Competition like '%Bowling %'
						Then Substring(Competition,1,Patindex('%Bowling%',Competition)-1)
						When Competition like '%Batting%'
						Then Substring(Competition,1,Patindex('%Batting%',Competition)-1)
						When Competition like '%Matchup%'
						Then Substring(Competition,1,Patindex('%Matchup%',Competition)-1)
						When Competition like '%Team %' and Competition not like '%Steves %'
						Then Substring(Competition,1,Patindex('%Team%',Competition)-1)
						When Competition like '%Of %'
						Then Substring(Competition,1,Patindex('%Of %',Competition)-1)
						When Competition like '%Online %'
						Then Substring(Competition,1,Patindex('%Online %',Competition)-1)
						When Competition like '%[1-9][0-9] overs%'
						Then Substring(Competition,1,Patindex('%[1-9][0-9] overs%',Competition)-3)
						Else Competition
					  End)
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Removing Event  where not required from competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition=(Case
						When Competition like '%Cup%'
						Then Substring(Competition,1,Patindex('%Cup%',Competition)+2)
						When Competition like '%Trophy%'
						Then Substring(Competition,1,Patindex('%Trophy%',Competition)+5)
						When Competition like '%League%'
						Then Substring(Competition,1,Patindex('%League%',Competition)+5)
						When Competition like '%Championship%'
						Then Substring(Competition,1,Patindex('%Championship%',Competition)+11)
						When Competition like '%Bash%'
						Then Substring(Competition,1,Patindex('%Bash%',Competition)+3)
						When Competition like '%Ashes%'
						Then Substring(Competition,1,Patindex('%Ashes%',Competition)+4)
						Else Competition
					  End)
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) 

--Removing single year numbers from competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition= Substring(Competition,1,PATINDEX('%[1-9][0-9][0-9][0-9]%',Competition)-1)+Substring(Competition,PATINDEX('%[1-9][0-9][0-9][0-9]%',Competition)+4,Len(Competition))
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and Competition like '%[1-9][0-9][0-9][0-9]%'and Competition not like '%[1-9][0-9][0-9][0-9]/[0-9]%'
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

--Removing erroneous data from competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition=(Case
						When Competition like '%|%'
						Then Substring(Competition,1,PATINDEX('%|%',Competition)-1)
						When Competition like '%(%'
						Then Substring(Competition,1,Patindex('%(%',Competition)-1)
						When Competition like '%most %'
						Then Substring(Competition,1,Patindex('%most %',Competition)-1)
						Else Competition
					  End)
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

 --Removing remaining Round  from competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition=(case
						when (Competition like '%[1-9][0-9][a-z][a-z] %')
						Then Substring(Competition,1,Patindex('%[1-9][0-9][a-z][a-z] %',Competition)-1)+
						Substring(Competition,Patindex('%[1-9][a-z][a-z] %',Competition)+3,Len(Competition))
						when (Competition like '%[1-9][a-z][a-z] %')
						Then Substring(Competition,1,Patindex('%[1-9][a-z][a-z] %',Competition)-1)+
						Substring(Competition,Patindex('%[1-9][a-z][a-z] %',Competition)+3,Len(Competition))
						else competition
					End)
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) 

Update	[$(Dimensional)].Dimension.Event
set		Competition=Ltrim(Rtrim(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Replace(Competition,'winner',''),'finals',''),'result',''),'*',''),'final',''),'Twenty20','T20'),'-',''),'  ',' '),'Twenty 20','T20')))
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))
 
--Formatting the structure of Competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition=(Case
						When(Competition='Test' or Competition='Test match')
						Then 'Test - '+Event
						When(Competition='Tour match')
						Then 'Tour - '+Event
						When(Competition='ODI' or Competition='ODI match')
						Then 'ODI - '+Event
						When(Competition='T20 ' or Competition='T20' or Competition='Test match')
						Then 'T20 - '+Event
						When(Competition like '%ODI Matches' or Competition like '%ODI Match' or Competition like '%ODI')
						Then 'ODI - '+Substring(Competition,1,Patindex('%ODI%',Competition)-1) 
						When(Competition like '%T20 Matches' or Competition like '%T20 Match' or Competition like '%T20')
						Then 'T20 - '+Substring(Competition,1,Patindex('%T20%',Competition)-1) 
						When(Competition like '%Test Matches' or Competition like '%Test Match' or Competition like '%Test')
						Then 'Test - '+Substring(Competition,1,Patindex('%Test%',Competition)-1) 
						When(Competition like '%ODI Series')
						Then 'ODI Series - '+Substring(Competition,1,Patindex('%ODI%',Competition)-1) 
						When(Competition like '%Test Series')
						Then 'Test Series - '+Substring(Competition,1,Patindex('%Test%',Competition)-1) 
						When(Competition like '%T20 Series')
						Then 'T20 Series - '+Substring(Competition,1,Patindex('%T20%',Competition)-1) 
						When(Competition like 'ODI Series%')
						Then 'ODI Series - '+Substring(Competition,Patindex('ODI %',Competition)+11,Len(Competition)) 
						When(Competition like 'Test Series%')
						Then 'Test Series - '+Substring(Competition,Patindex('Test %',Competition)+12,Len(Competition)) 
						When(Competition like 'T20 Series%')
						Then 'T20 Series - '+Substring(Competition,Patindex('%T20%',Competition)+11,Len(Competition)) 
						When(Competition like 'ODI Match%')
						Then 'ODI - '+Substring(Competition,Patindex('%ODI%',Competition)+10,Len(Competition)) 
						When(Competition like 'T20 Match%')
						Then 'T20 - '+Substring(Competition,Patindex('%T20%',Competition)+10,Len(Competition)) 
						When(Competition like 'Test Match%')
						Then 'Test - '+Substring(Competition,Patindex('%Test%',Competition)+11,Len(Competition))
						When(Competition like 'ODI%')
						Then 'ODI - '+Substring(Competition,Patindex('%ODI%',Competition)+3,Len(Competition)) 
						When( Competition like 'T20%')
						Then 'T20 - '+Substring(Competition,Patindex('%T20%',Competition)+3,Len(Competition)) 
						When(Competition like 'Test%')
						Then 'Test - '+Substring(Competition,Patindex('%Test%',Competition)+4,Len(Competition))
						When(Competition='The Ashes')
						Then 'Ashes'
						WHen (Competition like '%vs%' and Competition not like '%ODI%' and Competition not like '%T20%' and Competition not like '%Test%')
						Then (Case
								When (SourceEvent like '%Test series%' or isnull(ParentEvent,'') like '%Test series%'
								or isnull(GrandparentEvent,'') like '%Test series%')
								Then 'Test Series - '+Competition
								When (SourceEvent like '%Test%' or isnull(ParentEvent,'') like '%Test%'
								or isnull(GrandparentEvent,'') like '%Test%')
								Then 'Test - '+Competition
								When (SourceEvent like '%ODI%' or isnull(ParentEvent,'') like '%ODI%'
								or isnull(GrandparentEvent,'') like '%ODI%')
								Then 'ODI - '+Competition
								When (SourceEvent like '%T20%' or isnull(ParentEvent,'') like '%T20%'
								or isnull(GrandparentEvent,'') like '%T20%')
								Then 'T20 - '+Competition
								Else Competition
							  End)
						Else Competition
					 End)
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) 
			and (Competition like '%ODI%' or Competition like '%T20%' or Competition like '%Test%')

--Removing trailing '-' from competition 

Update	[$(Dimensional)].Dimension.Event
set		Competition=(Case 
						When(Competition like '%-')
						Then Replace(Substring(Competition,1,Len(Competition)-1),'  ',' ')
						When(Competition like '%- ')
						Then Replace(Substring(Competition,1,Len(Competition)-2),'  ',' ')
						Else Replace(Competition,'  ',' ')
					  End)
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (Competition like '%-' or Competition like '%- ' or  Competition like '  ')
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

Update	[$(Dimensional)].Dimension.Event
set		Competition='unknown'
where	SourceEventType='Cricket' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) 
			and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1))) and competition=''

--======================================================================================================================================================================   
--                                                PART 4: Insert Modification details in to Event Dimension for 'Cricket' event type
--====================================================================================================================================================================== 

EXEC [Control].Sp_Log	@BatchKey,@Me,'Part 4','Start',@RowsProcessed = @@RowCount

Update 	[$(Dimensional)].Dimension.Event
set		ModifiedBy = @Me,
		ModifiedDate = CURRENT_TIMESTAMP,
		CleansedDate = GETDATE(),
		CleansedBatchKey = @BatchKey,
		CleansedBy = @Me
where	ModifiedBatchKey = IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and SourceEventType='Cricket'

EXEC	[Control].Sp_Log	@BatchKey,@Me,NULL,'Success',@RowsProcessed = @@RowCount

END TRY
BEGIN CATCH

	declare @Error int;
	declare @ErrorMessage varchar(max);

	-- Log the failure at the object level
    SET		@Error = ERROR_NUMBER()
    SET		@ErrorMessage = ERROR_MESSAGE()
	EXEC	[Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	Raiserror(@ErrorMessage,16,1)

END CATCH

END
