﻿CREATE proc [Cleanse].[Sp_Account_Card]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET @Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start'

	UPDATE	u
	SET		CardStatusCode = UpdatedCardStatusCode,
			CardStatus = UpdatedCardStatus,
			CleansedDate = GETDATE(),
			CleansedBatchKey = @BatchKey,
			CleansedBy = @Me
	FROM	(	SELECT	CardStatusCode,
						CardStatus,
						UpdatedCardStatusCode,
						CASE	
							WHEN UpdatedCardStatusCode = 'P' THEN 'Processing'
							WHEN UpdatedCardStatusCode = 'R' THEN 'Pre-Activation'
							WHEN UpdatedCardStatusCode = 'A' THEN 'Activated'
							WHEN UpdatedCardStatusCode = '?' THEN '??'
							WHEN UpdatedCardStatusCode = 'X' THEN 'N/A' 
							ELSE 'Unknown'
						END UpdatedCardStatus,
						CleansedDate,
						CleansedBatchKey,
						CleansedBy
				FROM	(	SELECT	CardStatusCode,
									CardStatus,		
									CASE	
										WHEN CardStatusId = 'XX' THEN 'P'
										WHEN CardStatusId = 'PA' THEN 'R'
										WHEN CardStatusId = 'AC' THEN 'A'
										WHEN CardStatusId = 'II' THEN '?'
										WHEN CardStatusId = 'IA' THEN '?'
										WHEN CardIssueDate IS NOT NULL THEN 'U' 
										ELSE 'X'
									END UpdatedCardStatusCode,
									CleansedDate,
									CleansedBatchKey,
									CleansedBy
							FROM	[$(Dimensional)].Dimension.Account
							WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)
									AND (Increment = @Increment OR @Increment = 0) 
						) x
			) u
	WHERE	CardStatusCode <> UpdatedCardStatusCode
			OR CONVERT(varbinary,CardStatus) <> CONVERT(varbinary,UpdatedCardStatus) -- Case sensitive and more generic than nominating collations!
	OPTION (IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)
	
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
