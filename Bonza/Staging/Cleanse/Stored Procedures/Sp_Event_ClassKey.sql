﻿CREATE PROCEDURE [Cleanse].[Sp_Event_ClassKey]
		@BatchKey int 
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

BEGIN
BEGIN TRY

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

--===============================================================================================================================================================================================
--                    PART 1: Load ClassKey based on ClassId and Source-- To correct the anamolies introduced before
--=============================================================================================================================================================================================== 

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 1','Start'

Update	e
set		ClassKey= c.ClassKey ,
		ModifiedBy = @Me,
		ModifiedDate = CURRENT_TIMESTAMP,
		CleansedDate = GETDATE(),
		CleansedBatchKey = @BatchKey,
		CleansedBy = @Me
from	[$(Dimensional)].Dimension.Event e 
		inner join [$(Dimensional)].Dimension.Class c on c.ClassID=e.ClassID
where	e.ModifiedBy= 'Sp_Event_ClassKey' and  e.ModifiedBatchKey=IIF(@BatchKey=0,e.ModifiedBatchKey,@BatchKey) and EventKey not in (-1,0)
option(recompile);

--===============================================================================================================================================================================================
--                    PART 2: Correct ClassKey by including SubSportType value in determining ClassKey || Make best use of any clues in the same record for misidentified events
--=============================================================================================================================================================================================== 

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 2','Start', @RowsProcessed = @@ROWCOUNT

Update	e
set		ClassKey= (Case When SubSportType like '%Greyhound%' or SourceEvent like '%Greyhound%' 
					Then (select ClassKey from [$(Dimensional)].Dimension.Class where ClassID=3)
					When SubSportType like '%Harness%'  or SourceEvent like '%Harness%'  or SubSportType like '%Trot%'  
					Then (select ClassKey from [$(Dimensional)].Dimension.Class where ClassID=2)
					Else e.ClassKey
				End) ,
		ModifiedBy = @Me,
		ModifiedDate = CURRENT_TIMESTAMP,
		CleansedDate = GETDATE(),
		CleansedBatchKey = @BatchKey,
		CleansedBy = @Me
from	[$(Dimensional)].Dimension.Event e 
		inner join [$(Dimensional)].Dimension.Class c on c.ClassKey=isnull(e.ClassKey ,-1)
where	e.ClassID in (23,25,52,221,222,238,239,240,241,242)	 and c.ClassName not in('Harness','Greyhound')  
			and (SubSportType like '%Greyhound%'  or SourceEvent like '%Greyhound%' or SubSportType like '%Trot%' or SubSportType like '%Harness%'   or SourceEvent like '%Harness%' )
			and SourceEvent not like '% vs %'  and SourceEvent not like '% v %' and e.ModifiedBatchKey=IIF(@BatchKey=0,e.ModifiedBatchKey,@BatchKey)
option(recompile);

--===============================================================================================================================================================================================
--                    PART 3: Correct ClassKey by including SubSportType value in determining ClassKey || Search based on VenueID and EventDate in other records
--=============================================================================================================================================================================================== 

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 3','Start', @RowsProcessed = @@ROWCOUNT

Update	n
set		ClassKey = correctKey,
		ModifiedBy = @Me,
		ModifiedDate = CURRENT_TIMESTAMP,
		CleansedDate = GETDATE(),
		CleansedBatchKey = @BatchKey,
		CleansedBy = @Me
from	(
			select	e.classKey,e2.ClassKey correctKey,e.ModifiedBy,e.ModifiedDate,e.CleansedDate,e.CleansedBatchKey,e.CleansedBy,ROW_NUMBER() OVER(Partition BY e.EventID ORDER BY DATEDIFF(s,e.SourceEventDate,e2.SourceEventDate)) r
			from	[$(Dimensional)].Dimension.Event e 
					inner join [$(Dimensional)].Dimension.Event e2 on e.VenueId=e2.VenueId and e2.ClassID in (2,3) and convert(date,e.SourceEventDate)=convert(date,e2.SourceEventDate)
					inner join [$(Dimensional)].Dimension.Class c on c.ClassKey=isnull(e.ClassKey ,-1)
			where	e.ClassID in (23,25,52,221,222,238,239,240,241,242)	 and c.ClassName not in('Harness','Greyhound') and e2.ClassKey is not null and e.ModifiedBatchKey=IIF(@BatchKey=0,e.ModifiedBatchKey,@BatchKey)
		) n 
where	r=1
option(recompile);

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 2','Success', @RowsProcessed = @@ROWCOUNT

END TRY
BEGIN CATCH


	declare @Error int;
	declare @ErrorMessage varchar(max);
	                           
	-- Log the failure at the object level
    SET		@Error = ERROR_NUMBER()
    SET		@ErrorMessage = ERROR_MESSAGE()
	EXEC	[Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	Raiserror(@ErrorMessage,16,1)

END CATCH

END
