﻿CREATE proc [Cleanse].[SP_Account_ManagedBy]
(
	@BatchKey int = 0,
	@Increment int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET @Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Copy From Structure', 'Start';

	UPDATE	u SET	ManagedById = CONVERT(char(32),HASHBYTES('MD5',UpdatedBDM+'|'+CONVERT(varchar,UpdatedVIPCode)+'|'+UpdatedVIP),2),
					ManagedBy = UpdatedManagedBy,
					BDM = UpdatedBDM,
					VIPCode = UpdatedVIPCode,
					VIP = UpdatedVIP,
					CompanyKey = UpdatedCompanyKey,
					CleansedDate = GETDATE(),
					CleansedBatchKey = @BatchKey,
					CleansedBy = @Me
	FROM	(	SELECT	a.ManagedById,
						a.ManagedBy,
						a.BDM,
						a.VIPCode,
						a.VIP,
						a.CompanyKey,
						COALESCE(NULLIF(s.BDM,'N/A'),NULLIF(s.VIP,'N/A'),'N/A') UpdatedManagedBy,
						ISNULL(s.VIPCode,-1) UpdatedVIPCode,
						ISNULL(s.VIP,'Unknown') UpdatedVIP,
						ISNULL(s.BDM,'Unknown') UpdatedBDM,
						ISNULL(c.CompanyKey,-1) UpdatedCompanyKey,
						a.CleansedDate,
						a.CleansedBatchKey,
						a.CleansedBy
				FROM	[$(Dimensional)].Dimension.Account a
						LEFT JOIN [$(Dimensional)].Dimension.Structure s ON (s.LedgerId = a.LedgerId AND s.LedgerSource = a.LedgerSource AND s.InPlay = 'N')
						LEFT JOIN [$(Dimensional)].Dimension.Company c ON (c.OrgId = s.OrgId AND c.DivisionId = CASE WHEN s.OrgId = 3 THEN 100 WHEN s.BDM <> 'N/A' THEN 2 ELSE 1 END)
				WHERE	a.LedgerId IN 
						(	SELECT LedgerId FROM [$(Dimensional)].Dimension.Account WHERE (ModifiedBatchKey = @BatchKey OR @BatchKey = 0) AND (Increment = @Increment OR @Increment = 0) 
							UNION 
							SELECT LedgerId FROM [$(Dimensional)].Dimension.Structure WHERE ModifiedBatchKey = @BatchKey AND (Increment = @Increment OR @Increment = 0) 
						)
			) u
	WHERE	ManagedBy <> UpdatedManagedBy
			OR BDM <> UpdatedBDM
			OR VIPCode <> UpdatedVIPCode
			OR VIP <> UpdatedVIP
			OR CompanyKey <> UpdatedCompanyKey
	OPTION (IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
