﻿-- =============================================
-- Author:		Lekha Narra
-- Create date: 12/11/2014
-- Description:	Insert Event names, Round names, Competition names for 'Rugby League' event type
-- =============================================

CREATE PROCEDURE [Cleanse].[SP_Event_RugbyLeague] 
	-- Add the parameters for the stored procedure here
	@Batchkey int 
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN
BEGIN TRY

	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes

-- ================================================================================================================================================================
--                        PART 1: Extract Event names in Event Dimension for 'Rugby League' event type
-- ================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 1','Start'

update	[$(Dimensional)].[Dimension].[Event]
set		[Event]=(CASE 
				when ((isnull(SourceEvent,'') like '%Test%' or isnull(ParentEvent,'')  like '%Test%'or isnull(GrandParentEvent,'')  like '%Test%'
						or isnull(SourceEvent,'')  like '%Team[1-9]%' or isnull(ParentEvent,'') like '%Team[1-9]%' or	
						isnull(GrandParentEvent,'') like '%Team[1-9]%') and SourceEvent not like '%Pacific Island Test%' and 
						isnull(ParentEvent,'')  not like '%Pacific Island Test%'and isnull(GrandParentEvent,'')  not like '%Pacific Island Test%'
						and SourceEvent not like '%Trans Tasman Test%' and isnull(ParentEvent,'')  not like '%Trans Tasman Test%'
						and isnull(GrandParentEvent,'') not like '%Trans Tasman Test%' )
				then 'NA'
				Else
					CASE	
						WHEN (isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '%@%')
						THEN 
							(CASE 
								WHEN isnull(ParentEvent,'') like'% - %'			
								THEN 
									(CASE
										WHEN ((SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% vs %') or
											(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '% v %') or
											(SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,''))) like '%@%') )
										THEN SUBString(isnull(ParentEvent,''),1, CHARINDEX(' - ',isnull(ParentEvent,'')))
										ELSE SUBString(isnull(ParentEvent,''), CHARINDEX(' - ',isnull(ParentEvent,''))+3,LEN(isnull(ParentEvent,'')))
									END)
									ELSE isnull(ParentEvent,'')
								END)
							ELSE
								(CASE
									WHEN (SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '%@%' ) 
									THEN 
										(CASE 
											WHEN SourceEvent like'% - %'			
											THEN 
												(CASE
												WHEN ((SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% vs %') or
													(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '% v %') or
													(SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent)) like '%@%') )
												THEN SUBString(SourceEvent,1, CHARINDEX(' - ',SourceEvent))
												ELSE SUBString(SourceEvent, CHARINDEX(' - ',SourceEvent)+3,LEN(SourceEvent))
												END)
											ELSE SourceEvent
											END)
									ELSE 'NA'
								END)
						END
			END)
where	sourceeventtype = 'Rugby League' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

update	[$(Dimensional)].[Dimension].[Event]
set		[Event]=(Case
					When ([Event] like '% Winner%' or [Event] like '%Top %' or [Event] like '%Player %' 
					 or [Event] like '% Win %' or [Event] like '% Win' or [Event] like '%see %'
					  or [Event] like '%Total %' or [Event] like '%points %' or [Event] like '%team %')
					 Then 'NA'
					 Else [Event]
				End)
where	SourceEventType='Rugby League' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))
						
update	[$(Dimensional)].[Dimension].[Event]
set		[Event]=RTRIM(LTRIM([Event]))
where	SourceEventType='Rugby League' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(2-1))<>Power(2,(2-1)))

-- ================================================================================================================================================================
--                        PART 2: Extract Round names in Event Dimension for 'Rugby League' event type
-- ================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 2','Start', @RowsProcessed = @@ROWCOUNT

Update	[$(Dimensional)].[Dimension].[Event]
set		[Round]=(CASE 
					WHEN(isnull(SourceEvent,'')  like '%Pacific Island Test%' or isnull(ParentEvent,'')  like '%Pacific Island Test%'or 
							isnull(GrandParentEvent,'')  like '%Pacific Island Test%'	or isnull(SourceEvent,'')   like '%Trans Tasman Test%' or 
							isnull(ParentEvent,'')  like '%Trans Tasman Test%' or isnull(GrandParentEvent,'')  like '%Trans Tasman Test%' or 
							isnull(SourceEvent,'')  like '%Team[1-9]%' or isnull(ParentEvent,'')  like '%Team[1-9]%'or 
							isnull(GrandParentEvent,'')  like '%Team[1-9]%')
					THEN 'NA'
					when (SourceEvent like '%Clive Churchill%' or isnull(ParentEvent,'')  like '%Clive Churchill%'or 
							isnull(GrandParentEvent,'')  like '%Clive Churchill%')  
					then 'Grand Final'
					when (SourceEvent like '%Test%' or isnull(ParentEvent,'')  like '%Test%'or isnull(GrandParentEvent,'')  like '%Test%')  
					then 'Test'
					WHEN (isnull(GrandParentEvent,'') like '%Round%[1-9]%' or isnull(GrandParentEvent,'') like '%week%[1-9]%' 
					or isnull(GrandParentEvent,'') like '%Final%' or isnull(GrandParentEvent,'') like '%[1-9][a-z][a-z] Round%'
					 or isnull(GrandParentEvent,'') like '%game%' )
					THEN 
						(CASE 
							WHen (GrandParentEvent like'%Preliminary Final%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Preliminary Final%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%Quarter Final%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Quarter Final%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%Semi%Final%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Semi%Final%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%Grand Final%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Grand %Final%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%Final%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Final%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like '%[1-9][a-z][a-z] Round%')	
							Then SUBString(GrandParentEvent, PATINDEX('%[1-9][a-z][a-z] Round%',GrandParentEvent),9)
							WHen (GrandParentEvent like'%Round%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Round%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%Week%')	
							Then SUBString(GrandParentEvent, PATINDEX('%Week%',GrandParentEvent),Len(GrandParentEvent))
							WHen (GrandParentEvent like'%game%')	
							Then SUBString(GrandParentEvent, PATINDEX('%game%',GrandParentEvent),Len(GrandParentEvent))
							ELSE 'check'
						END)
					WHEN (isnull(ParentEvent,'') like '%Round%[1-9]%' or isnull(ParentEvent,'') like '%week%[1-9]%' 
					or isnull(ParentEvent,'') like '%Final%' or isnull(ParentEvent,'') like '%[1-9][a-z][a-z] Round%'
					 or isnull(ParentEvent,'') like '%game%' )
					THEN 
						(CASE 
							WHen (ParentEvent like'%Semi%Final%')	
							Then SUBString(ParentEvent, PATINDEX('%Semi%Final%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%Preliminary%Final%')	
							Then SUBString(ParentEvent, PATINDEX('%Preliminary%Final%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%Quarter%Final%')	
							Then SUBString(ParentEvent, PATINDEX('%Quarter%Final%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%Grand%Final%')	
							Then SUBString(ParentEvent, PATINDEX('%Grand%Final%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%Final%')	
							Then SUBString(ParentEvent, PATINDEX('%Final%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%[1-9][a-z][a-z] Round%')	
							Then SUBString(ParentEvent, PATINDEX('%[1-9][a-z][a-z] Round%',ParentEvent),9)
							WHen (ParentEvent like'%Round%')	
							Then SUBString(ParentEvent, PATINDEX('%Round%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%Week%')	
							Then SUBString(ParentEvent, PATINDEX('%Week%',ParentEvent),Len(ParentEvent))
							WHen (ParentEvent like'%game%')	
							Then SUBString(ParentEvent, PATINDEX('%game%',ParentEvent),Len(ParentEvent))
							ELSE 'check'
						END)		
					WHEN (SourceEvent like '%Round%[1-9]%' or SourceEvent like '%week%[1-9]%' or SourceEvent like '%Final%' or 
							SourceEvent like '%[1-9][a-z][a-z] Round%' or SourceEvent like '%game%')
					THEN 
						(CASE 
							When (SourceEvent like'%?')	
							Then 'NA'
							WHen (SourceEvent like'%Semi%Final%')	
							Then SUBString(SourceEvent, PATINDEX('%Semi%Final%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%Preliminary%Final%')	
							Then SUBString(SourceEvent, PATINDEX('%Preliminary%Final%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%Quarter%Final%')	
							Then SUBString(SourceEvent, PATINDEX('%Quarter%Final%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%Grand%Final%')	
							Then SUBString(SourceEvent, PATINDEX('%Grand%Final%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%Final%')	
							Then SUBString(SourceEvent, PATINDEX('%Final%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%[1-9][a-z][a-z] Round%')	
							Then SUBString(SourceEvent, PATINDEX('%[1-9][a-z][a-z] Round%',SourceEvent),9)
							WHen (SourceEvent like'%Round%')	
							Then SUBString(SourceEvent, PATINDEX('%Round%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%Week%')	
							Then SUBString(SourceEvent, PATINDEX('%Week%',SourceEvent),Len(SourceEvent))
							WHen (SourceEvent like'%game%')	
							Then SUBString(SourceEvent, PATINDEX('%game%',SourceEvent),Len(SourceEvent))
							ELSE 'check'
						END)
					ELSE (CASE
							WHEN ((SourceEvent like '% vs %') or (isnull(ParentEvent,'')  like '% vs %') or (isnull(GrandParentEvent,'')  like '% vs %')
							or (isnull(ParentEvent,'') like '% v %') or (isnull(ParentEvent,'') like '% @ %')  or (SourceEvent like '% v %') or (SourceEvent like '% @ %')
								or (isnull(GrandParentEvent,'') like '% v %') or (isnull(GrandParentEvent,'') like '% @ %'))
							THEN 'Unknown'
							ELSE 'NA'
							END)
				END)
WHERE	sourceeventtype='Rugby League' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1))) 
	
Update	[$(Dimensional)].[Dimension].[Event]
set		[Round]=Replace(Replace(Replace(Replace([Round],'one','1'),'two','2'),'three','3'),'four','4')
WHERE	sourceeventtype='Rugby League' 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1))) 
			and ([Round] like '%one%' or [Round] like '%two%' or [Round] like '%three%' or [Round] like '%four%')

Update	[$(Dimensional)].[Dimension].[Event]
set		[Round]=(case 
						When [Round] like '%Game III%'
						Then Replace([Round],'Game III','Game 3')
						When [Round] like '%Game II%'
						Then Replace([Round],'Game II','Game 2')
						When [Round] like '%Game I%'
						Then Replace([Round],'Game I','Game 1')
						Else [Round]
					End)
WHERE	sourceeventtype='Rugby League' and [Round] like '%Game %' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1))) 

--Truncating part after 'round number' and 'Final' word

Update	[$(Dimensional)].[Dimension].[Event]
set		[Round]=(CASE	
					WHEN ([Round] like '%Round%[1-9]%' )
					Then RTRIM(SUBString([Round],1, CHARINDEX('Round',[Round])+7))
					WHEN ([Round] like '%Game [1-9]%')
					Then RTRIM(SUBString([Round],1, CHARINDEX('Game',[Round])+6))
					WHEN ([Round] like '%Game%')
					Then 'Unknown'
					WHEN ([Round] like '%Week%[1-9]%')
					Then RTRIM(SUBString([Round],1, CHARINDEX('Week',[Round])+6))
					ELSE (CASE
							WHEN [Round] like '%Final%'
							THEN RTRIM(SUBString([Round],1, CHARINDEX('Final',[Round])+4))
							ELSE [Round]
							END)
				END)
WHERE	sourceeventtype='Rugby League' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1))) 
		
Update	[$(Dimensional)].[Dimension].[Event]
set		[Round]= Replace(Replace([Round],'Matches',''),'-','')
where	([Round] like'%Matches%' or [Round] like'%-') 
			and sourceeventtype='Rugby League' 
			and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

--Removing Year numbers if present

Update	[$(Dimensional)].[Dimension].[Event]
set		[Round]=( CASE 
					WHEN ISNUMERIC(SUBSTRING([Round],PATINDEX('% [1-2][0-9][0-9][0-9] %',[Round]),4))=1 
					THEN REPLACE([Round],SUBSTRING([Round],PATINDEX('%[1-2][0-9][0-9][0-9]%',[Round]),4),'') 
					ELSE [Round] 
				END)
WHERE	sourceeventtype='Rugby League' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)  and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1))) 
		
--Removing Competition Names if present

Update	[$(Dimensional)].[Dimension].[Event]
set		[Round] = ( CASE 
						WHEN c1.EventNameLike is not null
						THEN REPLACE(a.round  collate database_default ,SUBSTRING(a.round  collate database_default ,CHARINDEX(c1.EventNameLike,a.round collate database_default ),LEN(c1.EventNameLike)),'')
						ELSE a.round 
					END)
from	[$(Dimensional)].[Dimension].[Event] a
		left join Reference.EventCompMapping C1 on a.round collate database_default like C1.EventNameLike 
			and a.round collate database_default not like C1.EventNameNotLike1
			and a.round collate database_default not like C1.EventNameNotLike2 
			and a.round collate database_default not like C1.EventNameNotLike3
			and a.round collate database_default not like C1.EventNameNotLike4 
			and a.round collate database_default not like C1.EventNameNotLike5	
			and a.SourceEventType=c1.EventType  
WHERE	(a.ColumnLock&Power(2,(10-1))<>Power(2,(10-1))) and a.SourceEventType='Rugby League'  and a.ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey)

update	[$(Dimensional)].[Dimension].[Event]
set		[Round]= RTRIM(LTRIM([Round]))
where	SourceEventType='Rugby League' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(10-1))<>Power(2,(10-1)))

-- ================================================================================================================================================================
  --                        PART 3: Update Competition names in Event Dimension for 'Rugby League' event type
-- ================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 3','Start', @RowsProcessed = @@ROWCOUNT

update	[$(Dimensional)].[Dimension].[Event]
set		Competition= 
			(CASE 
				WHEN (SourceEvent like '%Origin socceroos%' or isnull(ParentEvent,'')  like '%Origin socceroos%'
				or isnull(GrandParentEvent,'')  like '%Origin socceroos%') 
				then 'NA'
				WHEN (C1.CompetitionName is not null)
				THEN C1.CompetitionName
				WHEN (C2.CompetitionName is not null)
				THEN  C2.CompetitionName
				WHEN (C3.CompetitionName is not null)
				THEN  C3.CompetitionName
				when (SourceEvent like '%Test%' or isnull(ParentEvent,'')  like '%Test%'or isnull(GrandParentEvent,'')  like '%Test%')
				THEN 'Test'
				ELSE (CASE
						WHEN ((isnull(ParentEvent,'') like '% vs %' or isnull(ParentEvent,'') like '% v %' or isnull(ParentEvent,'') like '% @ %'
						or SourceEvent like '% vs %' or SourceEvent like '% v %' or SourceEvent like '% @ %') and isnull(SourceEvent,'')  not like '%Team[1-9]%' and
							isnull(ParentEvent,'') not like '%Team[1-9]%' and	isnull(GrandParentEvent,'') not like '%Team[1-9]%')
						THEN 'Unknown'
						ELSE 'NA'									 
						END)									 
				END)
from	[$(Dimensional)].[Dimension].[Event]  a
		left join Reference.EventCompMapping C1 ON (a.SourceEventtype=C1.EventType and isnull(a.GrandParentEvent,'')  like C1.EventNameLike 
			and isnull(a.GrandParentEvent,'')  not like C1.EventNameNotLike1
			and isnull(a.GrandParentEvent,'')  not like C1.EventNameNotLike2 
			and isnull(a.GrandParentEvent,'')  not like C1.EventNameNotLike3
			and isnull(a.GrandParentEvent,'')  not like C1.EventNameNotLike4 
			and isnull(a.GrandParentEvent,'')  not like C1.EventNameNotLike5)
		LEFT JOIN Reference.EventCompMapping C2	ON (a.SourceEventtype=C2.EventType and isnull(ParentEvent,'') LIKE C2.EventNameLike 
			AND isnull(ParentEvent,'') NOT LIKE C2.EventNameNotLike1
			AND isnull(ParentEvent,'') NOT LIKE C2.EventNameNotLike2 
			AND isnull(ParentEvent,'') NOT LIKE C2.EventNameNotLike3
			AND isnull(ParentEvent,'') NOT LIKE C2.EventNameNotLike4 
			AND isnull(ParentEvent,'') NOT LIKE C2.EventNameNotLike5)
		LEFT JOIN Reference.EventCompMapping C3 ON (a.SourceEventtype=C3.EventType and SourceEvent LIKE C3.EventNameLike 
			AND SourceEvent NOT LIKE C3.EventNameNotLike1
			AND SourceEvent NOT LIKE C3.EventNameNotLike2  
			AND SourceEvent NOT LIKE C3.EventNameNotLike3
			AND SourceEvent NOT LIKE C3.EventNameNotLike4  
			AND SourceEvent NOT LIKE C3.EventNameNotLike5)
where	sourceEventType='Rugby League' and ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and (ColumnLock&Power(2,(12-1))<>Power(2,(12-1)))

-- ================================================================================================================================================================
  --                        PART 4: Load ModifiedBy and ModifiedDate in to Event Dimension for 'Rugby League' event type
-- ================================================================================================================================================================

EXEC	[Control].Sp_Log	@BatchKey,@Me,'Part 4','Start', @RowsProcessed = @@ROWCOUNT

Update 	[$(Dimensional)].[Dimension].[Event]
set		ModifiedBy = @Me,
		ModifiedDate = CURRENT_TIMESTAMP,
		CleansedDate = GETDATE(),
		CleansedBatchKey = @BatchKey,
		CleansedBy = @Me
where	ModifiedBatchKey=IIF(@BatchKey=0,ModifiedBatchKey,@BatchKey) and SourceEventType='Rugby League'

EXEC	[Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @@ROWCOUNT

END TRY
BEGIN CATCH
	
	declare @Error int;
	declare @ErrorMessage varchar(max);

	-- Log the failure at the object level
    SET		@Error = ERROR_NUMBER()
    SET		@ErrorMessage = ERROR_MESSAGE()
	EXEC	[Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	Raiserror(@ErrorMessage,16,1)

END CATCH

END		

