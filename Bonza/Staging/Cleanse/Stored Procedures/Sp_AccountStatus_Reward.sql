﻿CREATE proc [Cleanse].[Sp_AccountStatus_Reward]
(
	@BatchKey int = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount INT;

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

BEGIN TRY


	EXEC [Control].Sp_Log @BatchKey,@Me, 'Exclusion', 'Start';

	UPDATE	u
	SET		RewardExclusionReasonCode = UpdatedRewardExclusionReasonCode,
			RewardExclusionReason = UpdatedRewardExclusionReason,			
			CleansedDate = GETDATE(),
			CleansedBatchKey = @BatchKey,
			CleansedBy = @Me
	FROM	(	SELECT	RewardExclusion,
						RewardExclusionReasonCode,
						RewardExclusionReason,
						CASE WHEN RewardExclusionDate IS NULL THEN 'No ' ELSE 'Yes' END UpdatedRewardExclusion,
						CASE RewardExclusionReasonId 
							WHEN 1 THEN 'E' 
							WHEN 2 THEN 'O' 
							WHEN 3 THEN 'S' 
							ELSE (CASE WHEN RewardExclusionDate IS NULL THEN 'X' ELSE 'U' END) 
						END UpdatedRewardExclusionReasonCode,
						CASE RewardExclusionReasonId 
							WHEN 1 THEN 'Excluded' 
							WHEN 2 THEN 'Opted Out' 
							WHEN 3 THEN 'State' 
							ELSE (CASE WHEN RewardExclusionDate IS NULL THEN 'N/A' ELSE 'Unknown' END) 
						END UpdatedRewardExclusionReason,			
						CleansedDate,
						CleansedBatchKey,
						CleansedBy
				FROM	[$(Dimensional)].Dimension.AccountStatus
				WHERE	AccountStatusKey > 0 -- Don't cleanse the default records
						AND (ModifiedBatchKey = @BatchKey OR @BatchKey = 0)
			) u
	WHERE	CONVERT(varbinary,ISNULL(RewardExclusion,'!')) <> CONVERT(varbinary,ISNULL(UpdatedRewardExclusion,'!')) -- Case sensitive and more generic than nominating collations!
			OR CONVERT(varbinary,ISNULL(RewardExclusionReasonCode,'!')) <> CONVERT(varbinary,ISNULL(UpdatedRewardExclusionReasonCode,'!'))
			OR CONVERT(varbinary,ISNULL(RewardExclusionReason,'!')) <> CONVERT(varbinary,ISNULL(UpdatedRewardExclusionReason,'!'))

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH