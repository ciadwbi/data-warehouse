﻿CREATE proc [Cleanse].[SP_Structure]
(
	@BatchKey		int = 0,
	@Increment		int = 0,
	@DryRun			bit = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount int

	SET @BatchKey = ISNULL(@BatchKey, 0) -- Interpret Nulls passed as parameters the same as zeroes
	SET @Increment = ISNULL(@Increment, 0)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Cache', 'Start';

	SELECT	StructureKey, AccountID, InPlay, HeadlineFeatures,
			-- Source
			FY2018OrgId, FY2017OrgId, FY2016OrgId, FY2015OrgId, FY2014OrgId,
			FY2018IntroducerId, FY2017IntroducerId, FY2016IntroducerId, FY2015IntroducerId, FY2014IntroducerId,
			FY2018Introducer, FY2017Introducer, FY2016Introducer, FY2015Introducer, FY2014Introducer,
			FY2018ManagerId, FY2017ManagerId, FY2016ManagerId, FY2015ManagerId, FY2014ManagerId,
			FY2018Manager, FY2017Manager, FY2016Manager, FY2015Manager, FY2014Manager,
			FY2018CanManageVIP, FY2017CanManageVIP, FY2016CanManageVIP, FY2015CanManageVIP, FY2014CanManageVIP,
			FY2018CanManagePotentialVIP, FY2017CanManagePotentialVIP, FY2016CanManagePotentialVIP, FY2015CanManagePotentialVIP, FY2014CanManagePotentialVIP,
			FY2018CanManageBDM, FY2017CanManageBDM, FY2016CanManageBDM, FY2015CanManageBDM, FY2014CanManageBDM,
			-- Original Values
			FY2018Level0 FY2018Level0_Original, FY2017Level0 FY2017Level0_Original, FY2016Level0 FY2016Level0_Original, FY2015Level0 FY2015Level0_Original, FY2014Level0 FY2014Level0_Original, Level0 Level0_Original, PreviousLevel0 PreviousLevel0_Original,
			FY2018Level1 FY2018Level1_Original, FY2017Level1 FY2017Level1_Original, FY2016Level1 FY2016Level1_Original, FY2015Level1 FY2015Level1_Original, FY2014Level1 FY2014Level1_Original, Level1 Level1_Original, PreviousLevel1 PreviousLevel1_Original,
			FY2018Level2 FY2018Level2_Original, FY2017Level2 FY2017Level2_Original, FY2016Level2 FY2016Level2_Original, FY2015Level2 FY2015Level2_Original, FY2014Level2 FY2014Level2_Original, Level2 Level2_Original, PreviousLevel2 PreviousLevel2_Original,
			FY2018BDM FY2018BDM_Original, FY2017BDM FY2017BDM_Original, FY2016BDM FY2016BDM_Original, FY2015BDM FY2015BDM_Original, FY2014BDM FY2014BDM_Original, BDM BDM_Original, PreviousBDM PreviousBDM_Original,
			FY2018VIPCode FY2018VIPCode_Original, FY2017VIPCode FY2017VIPCode_Original, FY2016VIPCode FY2016VIPCode_Original, FY2015VIPCode FY2015VIPCode_Original, FY2014VIPCode FY2014VIPCode_Original, VIPCode VIPCode_Original, PreviousVIPCode PreviousVIPCode_Original,
			FY2018VIP FY2018VIP_Original, FY2017VIP FY2017VIP_Original, FY2016VIP FY2016VIP_Original, FY2015VIP FY2015VIP_Original, FY2014VIP FY2014VIP_Original, VIP VIP_Original, PreviousVIP PreviousVIP_Original,
			FY2018PotentialVIP FY2018PotentialVIP_Original, FY2017PotentialVIP FY2017PotentialVIP_Original, FY2016PotentialVIP FY2016PotentialVIP_Original, FY2015PotentialVIP FY2015PotentialVIP_Original, FY2014PotentialVIP FY2014PotentialVIP_Original, PotentialVIP PotentialVIP_Original, PreviousPotentialVIP PreviousPotentialVIP_Original,
			FY2018CompanyKey FY2018CompanyKey_Original, FY2017CompanyKey FY2017CompanyKey_Original, FY2016CompanyKey FY2016CompanyKey_Original, FY2015CompanyKey FY2015CompanyKey_Original, FY2014CompanyKey FY2014CompanyKey_Original, CompanyKey CompanyKey_Original, PreviousCompanyKey PreviousCompanyKey_Original,
			-- Cleanse Results
			FY2018Level0, FY2017Level0, FY2016Level0, FY2015Level0, FY2014Level0, Level0, PreviousLevel0,
			FY2018Level1, FY2017Level1, FY2016Level1, FY2015Level1, FY2014Level1, Level1, PreviousLevel1,
			FY2018Level2, FY2017Level2, FY2016Level2, FY2015Level2, FY2014Level2, Level2, PreviousLevel2,
			FY2018BDM, FY2017BDM, FY2016BDM, FY2015BDM, FY2014BDM, BDM, PreviousBDM,
			FY2018VIPCode, FY2017VIPCode, FY2016VIPCode, FY2015VIPCode, FY2014VIPCode, VIPCode, PreviousVIPCode,
			FY2018VIP, FY2017VIP, FY2016VIP, FY2015VIP, FY2014VIP, VIP, PreviousVIP,
			FY2018PotentialVIP, FY2017PotentialVIP, FY2016PotentialVIP, FY2015PotentialVIP, FY2014PotentialVIP, PotentialVIP, PreviousPotentialVIP,
			FY2018CompanyKey, FY2017CompanyKey, FY2016CompanyKey, FY2015CompanyKey, FY2014CompanyKey, CompanyKey, PreviousCompanyKey
	INTO	#Cache
	FROM	[$(Dimensional)].Dimension.Structure
	WHERE	(ModifiedBatchKey = @BatchKey OR @BatchKey = 0)
			AND (Increment = @Increment OR @Increment = 0) 
	OPTION (IGNORE_NONCLUSTERED_COLUMNSTORE_INDEX)

	EXEC [Control].Sp_Log @BatchKey,@Me,'VIP','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		VIP = UpdatedFY2018VIP, 
			PreviousVIP = UpdatedFY2017VIP, 
			FY2014VIP = UpdatedFY2014VIP, 
			FY2015VIP = UpdatedFY2015VIP, 
			FY2016VIP = UpdatedFY2016VIP, 
			FY2017VIP = UpdatedFY2017VIP,
			FY2018VIP = UpdatedFY2018VIP,
			VIPCode = UpdatedFY2018VIPCode, 
			PreviousVIPCode = UpdatedFY2017VIPCode, 
			FY2014VIPCode = UpdatedFY2014VIPCode, 
			FY2015VIPCode = UpdatedFY2015VIPCode, 
			FY2016VIPCode = UpdatedFY2016VIPCode, 
			FY2017VIPCode = UpdatedFY2017VIPCode,
			FY2018VIPCode = UpdatedFY2018VIPCode,
			PotentialVIP = UpdatedFY2018PotentialVIP, 
			PreviousPotentialVIP = UpdatedFY2017PotentialVIP, 
			FY2014PotentialVIP = UpdatedFY2014PotentialVIP, 
			FY2015PotentialVIP = UpdatedFY2015PotentialVIP, 
			FY2016PotentialVIP = UpdatedFY2016PotentialVIP, 
			FY2017PotentialVIP = UpdatedFY2017PotentialVIP, 
			FY2018PotentialVIP = UpdatedFY2018PotentialVIP
	FROM	(	SELECT	VIPCode, PreviousVIPCode, FY2014VIPCode, FY2015VIPCode, FY2016VIPCode, FY2017VIPCode, FY2018VIPCode,
						VIP, PreviousVIP, FY2014VIP, FY2015VIP, FY2016VIP, FY2017VIP, FY2018VIP,
						PotentialVIP, PreviousPotentialVIP, FY2014PotentialVIP, FY2015PotentialVIP, FY2016PotentialVIP, FY2017PotentialVIP, FY2018PotentialVIP,
						UpdatedFY2014VIPCode, UpdatedFY2015VIPCode, UpdatedFY2016VIPCode, UpdatedFY2017VIPCode, UpdatedFY2018VIPCode,
						UpdatedFY2014PotentialVIP, UpdatedFY2015PotentialVIP, UpdatedFY2016PotentialVIP, UpdatedFY2017PotentialVIP, UpdatedFY2018PotentialVIP,
						CASE WHEN [UpdatedFY2014VIPCode] = 0 THEN 'N/A' WHEN [UpdatedFY2014VIPCode] > 0 THEN 'VIP ' + Cast ([UpdatedFY2014VIPCode] as char(1)) ELSE 'Unknown' END UpdatedFY2014VIP,
						CASE WHEN [UpdatedFY2015VIPCode] = 0 THEN 'N/A' WHEN [UpdatedFY2015VIPCode] > 0 THEN 'VIP ' + Cast ([UpdatedFY2015VIPCode] as char(1)) ELSE 'Unknown' END UpdatedFY2015VIP,
						CASE WHEN [UpdatedFY2016VIPCode] = 0 THEN 'N/A' WHEN [UpdatedFY2016VIPCode] > 0 THEN 'VIP ' + Cast ([UpdatedFY2016VIPCode] as char(1)) ELSE 'Unknown' END UpdatedFY2016VIP,
						CASE WHEN [UpdatedFY2017VIPCode] = 0 THEN 'N/A' WHEN [UpdatedFY2017VIPCode] > 0 THEN 'VIP ' + Cast ([UpdatedFY2017VIPCode] as char(1)) ELSE 'Unknown' END UpdatedFY2017VIP,
						CASE WHEN [UpdatedFY2018VIPCode] = 0 THEN 'N/A' WHEN [UpdatedFY2018VIPCode] > 0 THEN 'VIP ' + Cast ([UpdatedFY2018VIPCode] as char(1)) ELSE 'Unknown' END UpdatedFY2018VIP
				FROM	(
							SELECT	VIPCode, PreviousVIPCode, FY2014VIPCode, FY2015VIPCode, FY2016VIPCode, FY2017VIPCode, FY2018VIPCode,
									VIP, PreviousVIP, FY2014VIP, FY2015VIP, FY2016VIP, FY2017VIP, FY2018VIP,
									PotentialVIP, PreviousPotentialVIP, FY2014PotentialVIP, FY2015PotentialVIP, FY2016PotentialVIP, FY2017PotentialVIP, FY2018PotentialVIP,
									CASE 
										WHEN FY2014IntroducerId = 1150832 THEN 1	-- Damien Cooper
										WHEN FY2014IntroducerId = 663028 THEN 1		-- Adam North
										WHEN FY2014IntroducerId = 791020 THEN 1		-- Jess Caine
										ELSE 0 
									END UpdatedFY2014VIPCode,
									CASE 
										WHEN FY2015IntroducerId = 1150832 THEN 1	-- Damien Cooper
										WHEN FY2015IntroducerId = 663028 THEN 1		-- Adam North
										WHEN FY2015IntroducerId = 791020 THEN 1		-- Jess Caine
										ELSE 0 
									END UpdatedFY2015VIPCode,
									CASE
										WHEN ISNULL(FY2016CanManageVIP,0)=0	THEN 0
										WHEN FY2016ManagerId in (-5,1427999,1506167) OR FY2016Manager like '%Kevin Murphy%'  OR FY2016Manager='Kelly, Ben' THEN 1
										WHEN FY2016ManagerId in (663028) OR FY2016Manager like '%North, Adam (CBT)%' THEN 2
										ELSE 0
									END UpdatedFY2016VIPCode,
									CASE
										WHEN ISNULL(FY2017CanManageVIP,0)=0	THEN 0
										WHEN FY2017ManagerId in (-5,1427999,1506167) OR FY2017Manager like '%Kevin Murphy%'  OR FY2017Manager='Kelly, Ben' THEN 1
										WHEN FY2017ManagerId in (663028) OR FY2017Manager like '%North, Adam (CBT)%' THEN 2
										ELSE 0
									END UpdatedFY2017VIPCode,
									CASE
										WHEN FY2018CanManageVIP = 1	THEN 1 -- revert to a 1/0 VIP code for 2018, just indicating VIP or not rahter than grouping VIPs for analysis
										ELSE 0
									END UpdatedFY2018VIPCode,
									CASE WHEN FY2014CanManagePotentialVIP=1 THEN 'Yes' ELSE  'No' END UpdatedFY2014PotentialVIP,
									CASE WHEN FY2015CanManagePotentialVIP=1 THEN 'Yes' ELSE  'No' END UpdatedFY2015PotentialVIP,
									CASE WHEN FY2016CanManagePotentialVIP=1 THEN 'Yes' ELSE  'No' END UpdatedFY2016PotentialVIP,
									CASE WHEN FY2017CanManagePotentialVIP=1 THEN 'Yes' ELSE  'No' END UpdatedFY2017PotentialVIP,
									CASE WHEN FY2018CanManageVIP=1 THEN 'No' WHEN FY2018CanManagePotentialVIP=1 THEN 'Yes' ELSE 'No' END UpdatedFY2018PotentialVIP
							FROM	#Cache
						) x
			) u
	WHERE	ISNULL(VIP, '!') <> UpdatedFY2018VIP 
			OR ISNULL(PreviousVIP, '!') <> UpdatedFY2017VIP 
			OR ISNULL(FY2014VIP, '!') <> UpdatedFY2014VIP 
			OR ISNULL(FY2015VIP, '!') <> UpdatedFY2015VIP 
			OR ISNULL(FY2016VIP, '!') <> UpdatedFY2016VIP 
			OR ISNULL(FY2017VIP, '!') <> UpdatedFY2017VIP
			OR ISNULL(FY2018VIP, '!') <> UpdatedFY2018VIP
			OR ISNULL(VIPCode, -99) <> UpdatedFY2018VIPCode 
			OR ISNULL(PreviousVIPCode, -99) <> UpdatedFY2017VIPCode 
			OR ISNULL(FY2014VIPCode, -99) <> UpdatedFY2014VIPCode 
			OR ISNULL(FY2015VIPCode, -99) <> UpdatedFY2015VIPCode 
			OR ISNULL(FY2016VIPCode, -99) <> UpdatedFY2016VIPCode 
			OR ISNULL(FY2017VIPCode, -99) <> UpdatedFY2017VIPCode
			OR ISNULL(FY2018VIPCode, -99) <> UpdatedFY2018VIPCode
			OR ISNULL (PotentialVIP, '!') <> UpdatedFY2018PotentialVIP 
			OR ISNULL(PreviousPotentialVIP, '!') <> UpdatedFY2017PotentialVIP 
			OR ISNULL(FY2014PotentialVIP, '!') <> UpdatedFY2014PotentialVIP 
			OR ISNULL(FY2015PotentialVIP, '!') <> UpdatedFY2015PotentialVIP 
			OR ISNULL(FY2016PotentialVIP, '!') <> UpdatedFY2016PotentialVIP 
			OR ISNULL(FY2017PotentialVIP, '!') <> UpdatedFY2017PotentialVIP
			OR ISNULL(FY2018PotentialVIP, '!') <> UpdatedFY2018PotentialVIP

	EXEC [Control].Sp_Log @BatchKey,@Me,'BDM','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		BDM = UpdatedFY2018BDM,
			PreviousBDM = UpdatedFY2017BDM,
			FY2018BDM = UpdatedFY2018BDM,
			FY2017BDM = UpdatedFY2017BDM,
			FY2016BDM = UpdatedFY2016BDM,
			FY2015BDM = UpdatedFY2015BDM,
			FY2014BDM = UpdatedFY2014BDM
	FROM	(	SELECT	BDM, PreviousBDM, FY2018BDM, FY2017BDM, FY2016BDM, FY2015BDM, FY2014BDM,
						CASE 
							WHEN ISNULL(FY2018CanManageBDM,0)<>1 THEN 'N/A' 
							Else FY2018Manager
						END UpdatedFY2018BDM,
						CASE 
							WHEN ISNULL(FY2017CanManageBDM,0)<>1 THEN 'N/A' 
							Else FY2017Manager
						END UpdatedFY2017BDM,
						CASE 
							WHEN ISNULL(FY2016ManagerId,0) IN (0,1494692,-1) OR FY2016VIPCode > 0 THEN 'N/A' -- If VIP OR d.Introducer in Darwin Office, Internet client OR d.Ben Kelly Digital, then not BDM
							WHEN FY2016Manager like '%Unassigned BDM%' THEN 'Legacy'
							Else FY2016Manager
						END UpdatedFY2016BDM,
						CASE 
							WHEN FY2015IntroducerId NOT IN (-1, 0, 10241, 14616, 663028, 681860, 681861, 791017, 791020, 1150832, 1263564) THEN FY2015Introducer
							ELSE 'N/A' 
						END UpdatedFY2015BDM,
						CASE 
							WHEN FY2014IntroducerId NOT IN (-1, 0, 10241, 14616, 663028, 681860, 681861, 791017, 791020, 1150832, 1263564) THEN FY2014Introducer
							ELSE 'N/A' 
						END UpdatedFY2014BDM
				FROM	#Cache
			) u
	WHERE	ISNULL(BDM,'!') <> ISNULL(UpdatedFY2018BDM,'!')
			OR ISNULL(PreviousBDM,'!') <> ISNULL(UpdatedFY2017BDM,'!')
			OR ISNULL(FY2018BDM,'!') <> ISNULL(UpdatedFY2018BDM,'!')
			OR ISNULL(FY2017BDM,'!') <> ISNULL(UpdatedFY2017BDM,'!')
			OR ISNULL(FY2016BDM,'!') <> ISNULL(UpdatedFY2016BDM,'!')
			OR ISNULL(FY2015BDM,'!') <> ISNULL(UpdatedFY2015BDM,'!')
			OR ISNULL(FY2014BDM,'!') <> ISNULL(UpdatedFY2014BDM,'!')

	EXEC [Control].Sp_Log @BatchKey,@Me,'Level0','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		Level0 = UpdatedFY2018Level0,
			PreviousLevel0 = UpdatedFY2017Level0,
			FY2018Level0 = UpdatedFY2018Level0,
			FY2017Level0 = UpdatedFY2017Level0,
			FY2016Level0 = UpdatedFY2016Level0,
			FY2015Level0 = UpdatedFY2015Level0,
			FY2014Level0 = UpdatedFY2014Level0
	FROM	(	SELECT	Level0, PreviousLevel0, FY2018Level0, FY2017Level0, FY2016Level0, FY2015Level0, FY2014Level0,
						CASE 
							WHEN FY2018OrgId IS NULL THEN 'N/A' 
							WHEN FY2018OrgId in (1,2,8)  THEN 'WHA Group'
							WHEN FY2018OrgId = 3 THEN 'RWWA'
							ELSE 'Unknown' 
						END UpdatedFY2018Level0,
						CASE 
							WHEN FY2017OrgId IS NULL THEN 'N/A' 
							WHEN FY2017OrgId in (1,2,8)  THEN 'WHA'
							WHEN FY2017OrgId = 3 THEN 'RWWA'
							ELSE 'Unknown' 
						END UpdatedFY2017Level0,
						CASE 
							WHEN FY2016OrgId IS NULL THEN 'N/A' 
							WHEN FY2016BDM NOT IN ('N/A', 'Unknown') THEN 'BDM'
							WHEN FY2016OrgId = 1 THEN 'WHA'
							WHEN FY2016OrgId = 2 THEN 'CBT'
							WHEN FY2016OrgId = 3 THEN 'RWWA'
							WHEN FY2016OrgId = 8 THEN 'TWH' 
							ELSE 'Unknown' 
						END UpdatedFY2016Level0,
						CASE 
							WHEN FY2015OrgId IS NULL THEN 'N/A' 
							WHEN FY2015BDM NOT IN ('N/A', 'Unknown') THEN 'BDM'
							WHEN FY2015OrgId = 1 THEN 'WHA'
							WHEN FY2015OrgId = 2 THEN 'CBT'
							WHEN FY2015OrgId = 3 THEN 'RWWA'
							WHEN FY2015OrgId = 8 THEN 'TWH' 
							ELSE 'Unknown' 
						END UpdatedFY2015Level0,
						CASE 
							WHEN FY2014OrgId IS NULL THEN 'N/A' 
							WHEN FY2014BDM NOT IN ('N/A', 'Unknown') THEN 'BDM'
							WHEN FY2014OrgId = 1 THEN 'WHA'
							WHEN FY2014OrgId = 2 THEN 'CBT'
							WHEN FY2014OrgId = 3 THEN 'RWWA'
							WHEN FY2014OrgId = 8 THEN 'TWH' 
							ELSE 'Unknown' 
						END UpdatedFY2014Level0
				FROM	#Cache
			) u
	WHERE	ISNULL(Level0,'!') <> UpdatedFY2018Level0
			OR ISNULL(PreviousLevel0,'!') <> UpdatedFY2017Level0
			OR ISNULL(FY2018Level0,'!') <> UpdatedFY2018Level0
			OR ISNULL(FY2017Level0,'!') <> UpdatedFY2017Level0
			OR ISNULL(FY2016Level0,'!') <> UpdatedFY2016Level0
			OR ISNULL(FY2015Level0,'!') <> UpdatedFY2015Level0
			OR ISNULL(FY2014Level0,'!') <> UpdatedFY2014Level0

	EXEC [Control].Sp_Log @BatchKey,@Me,'Level1','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		Level1 = UpdatedFY2018Level1,
			PreviousLevel1 = UpdatedFY2017Level1,
			FY2018Level1 = UpdatedFY2018Level1,
			FY2017Level1 = UpdatedFY2017Level1,
			FY2016Level1 = UpdatedFY2016Level1,
			FY2015Level1 = UpdatedFY2015Level1,
			FY2014Level1 = UpdatedFY2014Level1
	FROM	(	SELECT	Level1, PreviousLevel1, FY2018Level1, FY2017Level1, FY2016Level1, FY2015Level1, FY2014Level1,
						CASE 
							WHEN FY2018OrgId IS NULL THEN 'N/A' 
							WHEN HeadlineFeatures = 2 THEN 'Gaming' 
							WHEN AccountID in (1070305, 777860) THEN 'JW'
							WHEN FY2018BDM NOT IN ('N/A', 'Unknown') THEN 'BDM'
							WHEN FY2018OrgId = 2 THEN 'CBT'
							WHEN FY2018OrgId = 1 THEN 'William Hill'
							ELSE FY2018Level0 
						END UpdatedFY2018Level1,
						CASE 
							WHEN FY2017OrgId IS NULL THEN 'N/A' 
							WHEN AccountID in (1070305, 777860) THEN 'JW'
							WHEN FY2017BDM NOT IN ('N/A', 'Unknown') THEN 'BDM'
							WHEN FY2017OrgId = 2 THEN 'CBT'
							WHEN FY2017OrgId = 1 THEN 'WHA'
							ELSE FY2017Level0 
						END UpdatedFY2017Level1,
						CASE AccountID WHEN 1070305 THEN 'JW' WHEN 777860 THEN 'JW' ELSE FY2016Level0 END UpdatedFY2016Level1,
						FY2015Level0 UpdatedFY2015Level1,
						FY2014Level0 UpdatedFY2014Level1
				FROM	#Cache
			) u
	WHERE	ISNULL(Level1,'!') <> UpdatedFY2018Level1
			OR ISNULL(PreviousLevel1,'!') <> UpdatedFY2017Level1
			OR ISNULL(FY2018Level1,'!') <> UpdatedFY2018Level1
			OR ISNULL(FY2017Level1,'!') <> UpdatedFY2017Level1
			OR ISNULL(FY2016Level1,'!') <> UpdatedFY2016Level1
			OR ISNULL(FY2015Level1,'!') <> UpdatedFY2015Level1
			OR ISNULL(FY2014Level1,'!') <> UpdatedFY2014Level1

	EXEC [Control].Sp_Log @BatchKey,@Me,'Level2','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		Level2 = UpdatedFY2018Level2,
			PreviousLevel2 = UpdatedFY2017Level2,
			FY2018Level2 = UpdatedFY2018Level2,
			FY2017Level2 = UpdatedFY2017Level2,
			FY2016Level2 = UpdatedFY2016Level2,
			FY2015Level2 = UpdatedFY2015Level2,
			FY2014Level2 = UpdatedFY2014Level2
	FROM	(	SELECT	Level2, PreviousLevel2, FY2018Level2, FY2017Level2, FY2016Level2, FY2015Level2, FY2014Level2,
						CASE
							WHEN FY2018Level1 = 'William Hill' and FY2018VIPCode>0 THEN 'VIP'
							WHEN FY2018Level1 = 'William Hill' THEN 'Digital'
							ELSE FY2018Level1
						END UpdatedFY2018Level2,
						CASE
							WHEN FY2017Level0 = 'N/A' THEN 'N/A'
							WHEN FY2017Level1 = 'JW' THEN 'JW'
							WHEN FY2017Level1 = 'BDM' THEN 'BDM'
							WHEN FY2017Level1 = 'CBT' THEN 'CBT'
							WHEN FY2017Level1 = 'WHA' and FY2017VIPCode>0 THEN 'VIP'
							WHEN FY2017Level1 = 'WHA' and FY2016OrgId = 2 Then 'CBT Migrated'
							WHEN FY2017Level1 = 'WHA' THEN 'WHA Digital'
							ELSE FY2017Level1
						END UpdatedFY2017Level2,
						CASE
							WHEN FY2016Level0 = 'N/A' THEN 'N/A'
							WHEN InPlay = 'Y' AND FY2016Level1 = 'WHA' THEN 'INP'
							WHEN FY2016VIPCode = 1 THEN 'VIP1'
							WHEN FY2016VIPCode = 2 THEN 'VIP2'
							WHEN FY2015OrgId = 8 THEN 'TWH'
							ELSE FY2016Level1
						END UpdatedFY2016Level2,
						CASE 
							WHEN FY2015Level0 = 'N/A' THEN 'N/A'
							WHEN FY2015VIPCode > 0 THEN 'VIP'
							ELSE FY2015Level1 
						END UpdatedFY2015Level2,
						CASE 
							WHEN FY2014Level0 = 'N/A' THEN 'N/A'
							WHEN FY2014VIPCode > 0 THEN 'VIP'
							ELSE FY2014Level1 
						END UpdatedFY2014Level2
				FROM	#Cache
			) u
	WHERE	ISNULL(Level2,'!') <> UpdatedFY2018Level2
			OR ISNULL(PreviousLevel2,'!') <> UpdatedFY2017Level2
			OR ISNULL(FY2018Level2,'!') <> UpdatedFY2018Level2
			OR ISNULL(FY2017Level2,'!') <> UpdatedFY2017Level2
			OR ISNULL(FY2016Level2,'!') <> UpdatedFY2016Level2
			OR ISNULL(FY2015Level2,'!') <> UpdatedFY2015Level2
			OR ISNULL(FY2014Level2,'!') <> UpdatedFY2014Level2

	EXEC [Control].Sp_Log @BatchKey,@Me,'Company','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	u
	SET		CompanyKey = UpdatedFY2018CompanyKey,
			PreviousCompanyKey = UpdatedFY2017CompanyKey,
			FY2018CompanyKey = UpdatedFY2018CompanyKey,
			FY2017CompanyKey = UpdatedFY2017CompanyKey,
			FY2016CompanyKey = UpdatedFY2016CompanyKey,
			FY2015CompanyKey = UpdatedFY2015CompanyKey,
			FY2014CompanyKey = UpdatedFY2014CompanyKey
	FROM	(	SELECT	c.CompanyKey, c.PreviousCompanyKey, c.FY2018CompanyKey, c.FY2017CompanyKey, c.FY2016CompanyKey, c.FY2015CompanyKey, c.FY2014CompanyKey,
						ISNULL(c2018.CompanyKey,-1) UpdatedFY2018CompanyKey,
						ISNULL(c2017.CompanyKey,-1) UpdatedFY2017CompanyKey,
						ISNULL(c2016.CompanyKey,-1) UpdatedFY2016CompanyKey,
						ISNULL(c2015.CompanyKey,-1) UpdatedFY2015CompanyKey,
						ISNULL(c2014.CompanyKey,-1) UpdatedFY2014CompanyKey
				FROM	(	SELECT	CompanyKey, PreviousCompanyKey, FY2018CompanyKey, FY2017CompanyKey, FY2016CompanyKey, FY2015CompanyKey, FY2014CompanyKey,
									FY2018OrgId, FY2017OrgId, FY2016OrgId, FY2015OrgId, FY2014OrgId, 
									CASE FY2014Level0 WHEN 'N/A' THEN 0 WHEN 'BDM' THEN 2 WHEN 'RWWA' THEN 100 ELSE 1 END FY2014DivisionId, 
									CASE FY2015Level0 WHEN 'N/A' THEN 0 WHEN 'BDM' THEN 2 WHEN 'RWWA' THEN 100 ELSE 1 END FY2015DivisionId, 
									CASE FY2016Level0 WHEN 'N/A' THEN 0 WHEN 'BDM' THEN 2 WHEN 'RWWA' THEN 100 ELSE 1 END FY2016DivisionId,
									CASE FY2017Level1 WHEN 'N/A' THEN 0 WHEN 'BDM' THEN 2 WHEN 'RWWA' THEN 100 ELSE 1 END FY2017DivisionId,
									CASE FY2018Level1 WHEN 'N/A' THEN 0 WHEN 'BDM' THEN 2 WHEN 'RWWA' THEN 100 ELSE 1 END FY2018DivisionId
							FROM	#Cache
						) c
						LEFT JOIN [$(Dimensional)].Dimension.Company c2014 ON (c2014.OrgID = ISNULL(c.FY2014OrgId,0) AND c2014.DivisionId = c.FY2014DivisionId)
						LEFT JOIN [$(Dimensional)].Dimension.Company c2015 ON (c2015.OrgID = ISNULL(c.FY2015OrgId,0) AND c2015.DivisionId = c.FY2015DivisionId)
						LEFT JOIN [$(Dimensional)].Dimension.Company c2016 ON (c2016.OrgID = ISNULL(c.FY2016OrgId,0) AND c2016.DivisionId = c.FY2016DivisionId)
						LEFT JOIN [$(Dimensional)].Dimension.Company c2017 ON (c2017.OrgID = ISNULL(c.FY2017OrgId,0) AND c2017.DivisionId = c.FY2017DivisionId)
						LEFT JOIN [$(Dimensional)].Dimension.Company c2018 ON (c2018.OrgID = ISNULL(c.FY2018OrgId,0) AND c2018.DivisionId = c.FY2018DivisionId)
			) u
	WHERE	ISNULL(CompanyKey,-99) <> UpdatedFY2018CompanyKey
			OR ISNULL(PreviousCompanyKey,-99) <> UpdatedFY2017CompanyKey
			OR ISNULL(FY2018CompanyKey,-99) <> UpdatedFY2018CompanyKey
			OR ISNULL(FY2017CompanyKey,-99) <> UpdatedFY2017CompanyKey
			OR ISNULL(FY2016CompanyKey,-99) <> UpdatedFY2016CompanyKey
			OR ISNULL(FY2015CompanyKey,-99) <> UpdatedFY2015CompanyKey
			OR ISNULL(FY2014CompanyKey,-99) <> UpdatedFY2014CompanyKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log @BatchKey,@Me, 'Prepare', 'Start', @RowsProcessed = @@ROWCOUNT

	SELECT	StructureKey,
			FY2018Level0, FY2017Level0, FY2016Level0, FY2015Level0, FY2014Level0, Level0, PreviousLevel0,
			FY2018Level1, FY2017Level1, FY2016Level1, FY2015Level1, FY2014Level1, Level1, PreviousLevel1,
			FY2018Level2, FY2017Level2, FY2016Level2, FY2015Level2, FY2014Level2, Level2, PreviousLevel2,
			FY2018BDM, FY2017BDM, FY2016BDM, FY2015BDM, FY2014BDM, BDM, PreviousBDM,
			FY2018VIPCode, FY2017VIPCode, FY2016VIPCode, FY2015VIPCode, FY2014VIPCode, VIPCode, PreviousVIPCode,
			FY2018VIP, FY2017VIP, FY2016VIP, FY2015VIP, FY2014VIP, VIP, PreviousVIP,
			FY2018PotentialVIP, FY2017PotentialVIP, FY2016PotentialVIP, FY2015PotentialVIP, FY2014PotentialVIP, PotentialVIP, PreviousPotentialVIP,
			FY2018CompanyKey, FY2017CompanyKey, FY2016CompanyKey, FY2015CompanyKey, FY2014CompanyKey, CompanyKey, PreviousCompanyKey, MAX(src) src
	INTO	#Differences
	FROM	(	SELECT	'C' src, StructureKey,
						FY2018Level0, FY2017Level0, FY2016Level0, FY2015Level0, FY2014Level0, Level0, PreviousLevel0,
						FY2018Level1, FY2017Level1, FY2016Level1, FY2015Level1, FY2014Level1, Level1, PreviousLevel1,
						FY2018Level2, FY2017Level2, FY2016Level2, FY2015Level2, FY2014Level2, Level2, PreviousLevel2,
						FY2018BDM, FY2017BDM, FY2016BDM, FY2015BDM, FY2014BDM, BDM, PreviousBDM,
						FY2018VIPCode, FY2017VIPCode, FY2016VIPCode, FY2015VIPCode, FY2014VIPCode, VIPCode, PreviousVIPCode,
						FY2018VIP, FY2017VIP, FY2016VIP, FY2015VIP, FY2014VIP, VIP, PreviousVIP,
						FY2018PotentialVIP, FY2017PotentialVIP, FY2016PotentialVIP, FY2015PotentialVIP, FY2014PotentialVIP, PotentialVIP, PreviousPotentialVIP,
						FY2018CompanyKey, FY2017CompanyKey, FY2016CompanyKey, FY2015CompanyKey, FY2014CompanyKey, CompanyKey, PreviousCompanyKey
				FROM #Cache
				UNION ALL
				SELECT	'O' src, StructureKey,
						FY2018Level0_Original, FY2017Level0_Original, FY2016Level0_Original, FY2015Level0_Original, FY2014Level0_Original, Level0_Original, PreviousLevel0_Original,
						FY2018Level1_Original, FY2017Level1_Original, FY2016Level1_Original, FY2015Level1_Original, FY2014Level1_Original, Level1_Original, PreviousLevel1_Original,
						FY2018Level2_Original, FY2017Level2_Original, FY2016Level2_Original, FY2015Level2_Original, FY2014Level2_Original, Level2_Original, PreviousLevel2_Original,
						FY2018BDM_Original, FY2017BDM_Original, FY2016BDM_Original, FY2015BDM_Original, FY2014BDM_Original, BDM_Original, PreviousBDM_Original,
						FY2018VIPCode_Original, FY2017VIPCode_Original, FY2016VIPCode_Original, FY2015VIPCode_Original, FY2014VIPCode_Original, VIPCode_Original, PreviousVIPCode_Original,
						FY2018VIP_Original, FY2017VIP_Original, FY2016VIP_Original, FY2015VIP_Original, FY2014VIP_Original, VIP_Original, PreviousVIP_Original,
						FY2018PotentialVIP_Original, FY2017PotentialVIP_Original, FY2016PotentialVIP_Original, FY2015PotentialVIP_Original, FY2014PotentialVIP_Original, PotentialVIP_Original, PreviousPotentialVIP_Original,
						FY2018CompanyKey_Original, FY2017CompanyKey_Original, FY2016CompanyKey_Original, FY2015CompanyKey_Original, FY2014CompanyKey_Original, CompanyKey_Original, PreviousCompanyKey_Original
				FROM #Cache
			) x
	GROUP BY StructureKey,
			FY2018Level0, FY2017Level0, FY2016Level0, FY2015Level0, FY2014Level0, Level0, PreviousLevel0,
			FY2018Level1, FY2017Level1, FY2016Level1, FY2015Level1, FY2014Level1, Level1, PreviousLevel1,
			FY2018Level2, FY2017Level2, FY2016Level2, FY2015Level2, FY2014Level2, Level2, PreviousLevel2,
			FY2018BDM, FY2017BDM, FY2016BDM, FY2015BDM, FY2014BDM, BDM, PreviousBDM,
			FY2018VIPCode, FY2017VIPCode, FY2016VIPCode, FY2015VIPCode, FY2014VIPCode, VIPCode, PreviousVIPCode,
			FY2018VIP, FY2017VIP, FY2016VIP, FY2015VIP, FY2014VIP, VIP, PreviousVIP,
			FY2018PotentialVIP, FY2017PotentialVIP, FY2016PotentialVIP, FY2015PotentialVIP, FY2014PotentialVIP, PotentialVIP, PreviousPotentialVIP,
			FY2018CompanyKey, FY2017CompanyKey, FY2016CompanyKey, FY2015CompanyKey, FY2014CompanyKey, CompanyKey, PreviousCompanyKey
	HAVING	COUNT(*) = 1
	OPTION (RECOMPILE)

	IF @DryRun = 1 
		SELECT TOP 10000 * FROM #Differences d ORDER BY StructureKey, src

	SELECT	* INTO #Prepare FROM #Differences WHERE	src = 'C' -- Discard the 'original' version record of Update


	EXEC [Control].Sp_Log @BatchKey,@Me, 'Update', 'Start', @RowsProcessed = @@ROWCOUNT

	IF @DryRun = 0 
		BEGIN
			UPDATE	d
			SET		FY2018Level0 = c.FY2018Level0, FY2017Level0 = c.FY2017Level0, FY2016Level0 = c.FY2016Level0, FY2015Level0 = c.FY2015Level0, FY2014Level0 = c.FY2014Level0, Level0 = c.Level0, PreviousLevel0 = c.PreviousLevel0,
					FY2018Level1 = c.FY2018Level1, FY2017Level1 = c.FY2017Level1, FY2016Level1 = c.FY2016Level1, FY2015Level1 = c.FY2015Level1, FY2014Level1 = c.FY2014Level1, Level1 = c.Level1, PreviousLevel1 = c.PreviousLevel1,
					FY2018Level2 = c.FY2018Level2, FY2017Level2 = c.FY2017Level2, FY2016Level2 = c.FY2016Level2, FY2015Level2 = c.FY2015Level2, FY2014Level2 = c.FY2014Level2, Level2 = c.Level2, PreviousLevel2 = c.PreviousLevel2,
					FY2018BDM = c.FY2018BDM, FY2017BDM = c.FY2017BDM, FY2016BDM = c.FY2016BDM, FY2015BDM = c.FY2015BDM, FY2014BDM = c.FY2014BDM, BDM = c.BDM, PreviousBDM = c.PreviousBDM,
					FY2018VIPCode = c.FY2018VIPCode, FY2017VIPCode = c.FY2017VIPCode, FY2016VIPCode = c.FY2016VIPCode, FY2015VIPCode = c.FY2015VIPCode, FY2014VIPCode = c.FY2014VIPCode, VIPCode = c.VIPCode, PreviousVIPCode = c.PreviousVIPCode,
					FY2018VIP = c.FY2018VIP, FY2017VIP = c.FY2017VIP, FY2016VIP = c.FY2016VIP, FY2015VIP = c.FY2015VIP, FY2014VIP = c.FY2014VIP, VIP = c.VIP, PreviousVIP = c.PreviousVIP,
					FY2018PotentialVIP = c.FY2018PotentialVIP, FY2017PotentialVIP = c.FY2017PotentialVIP, FY2016PotentialVIP = c.FY2016PotentialVIP, FY2015PotentialVIP = c.FY2015PotentialVIP, FY2014PotentialVIP = c.FY2014PotentialVIP, PotentialVIP = c.PotentialVIP, PreviousPotentialVIP = c.PreviousPotentialVIP,
					FY2018CompanyKey = c.FY2018CompanyKey, FY2017CompanyKey = c.FY2017CompanyKey, FY2016CompanyKey = c.FY2016CompanyKey, FY2015CompanyKey = c.FY2015CompanyKey, FY2014CompanyKey = c.FY2014CompanyKey, CompanyKey = c.CompanyKey, PreviousCompanyKey = c.PreviousCompanyKey,
					CleansedDate = GETDATE(),
					CleansedBatchKey = @BatchKey,
					CleansedBy = @Me
			FROM	[$(Dimensional)].Dimension.Structure d
					INNER JOIN #Prepare c ON c.StructureKey = d.StructureKey
			OPTION(RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
