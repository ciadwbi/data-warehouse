﻿CREATE FUNCTION [Cleanse].[CamelCase]
(@Str varchar(8000))
RETURNS varchar(8000) AS
BEGIN
  DECLARE @Result varchar(2000)=''
  DECLARE @strcheck varchar(1)=''
  DECLARE @strfinal varchar(1)=''
  declare @upper bit=0
  declare @counter int=0
  SET @Str = Ltrim(Rtrim(LOWER(@Str)))
  WHILE @counter<=Len(@str)
  BEGIN
	set @strcheck=isnull(Substring(@str,@counter,1),'')
	IF @Upper=1 
	BEGIN
		set @strfinal=Upper(@strcheck)
	END
	ELSE BEGIN set @strfinal=@strcheck END
	IF @counter=0 or @strcheck in (' ','''','/','-','(',')','.')
	Begin
			set @upper=1
	End
	Else Begin	set @upper=0 End
    set @Result=Concat(@Result,@strfinal)
	set @counter=@counter+1
  END
  RETURN @Result
END

