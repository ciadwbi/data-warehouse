﻿CREATE PROCEDURE [Recursive].[Sp_KPI]
(
	@BatchKey		int,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount int
	DECLARE @ReloadFromDimensional bit = 0

BEGIN TRY

	SET @ReloadFromDimensional = CASE WHEN ISNULL(CONVERT(int,Control.ParameterValue('ETL','StagingLowerBound')),0) <= @BatchKey THEN 0 ELSE 1 END

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Collect Transactions','Start'

	SELECT	@BatchKey BatchKey, LegId, LegIdSource, BetId, BetGroupID, TransactionId, LegNumber, NumberOfLegs, LedgerID, LedgerSource, MasterDayText, AnalysisDayText, MasterTransactionTimestamp,
			BalanceTypeID, TransactionTypeID, TransactionStatusID, TransactionMethodID, WalletId, 
			BetGroupType, BetTypeName, BetSubTypeName, LegBetTypeName, GroupingType, 
			ClassId, ClassIdSource, Channel, ActionChannel, CampaignId, PromotionId, UserId, UserIdSource, 
			EventId, EventIdSource, InstrumentID, InstrumentIdSource, ClickToCall, InPlay, CashoutType, IsDoubleDown, IsDoubledDown, IsChaseTheAce, FeatureFlags,
			TransactedAmount
	INTO	#Transaction
	FROM	(	
				SELECT	c.LegId, c.LegIdSource, ISNULL(c.BetId,c.TransactionID) BetId, c.BetGroupID, t.TransactionId, lt.LegNumber, lt.NumberOfLegs, a.LedgerID, a.LedgerSource, z.MasterDayText, z.AnalysisDayText, t.MasterTransactionTimestamp,
						b.BalanceTypeID, d.TransactionTypeID, d.TransactionStatusID, d.TransactionMethodID, W.WalletId, 
						bt.BetGroupType, bt.BetType BetTypeName, bt.BetSubType BetSubTypeName, bt.LegType LegBetTypeName, bt.GroupingType, 
						cl.ClassId, cl.Source ClassIdSource, ch.ChannelId Channel, ac.ChannelId ActionChannel, cp.CampaignId, pr.PromotionId, u.UserId, u.Source UserIdSource, 
						e.EventId, e.Source EventIdSource, i.InstrumentID, i.Source InstrumentIdSource, ip.ClickToCall, ip.InPlay, ip.CashoutType, ip.IsDoubleDown, ip.IsDoubledDown, ip.IsChaseTheAce, ip.FeatureFlags,
						t.TransactedAmount
				FROM	[$(Dimensional)].Fact.[Transaction] t						
						INNER JOIN [$(Dimensional)].Dimension.BalanceType b ON (b.BalanceTypeKey = t.BalanceTypeKey) 
						INNER JOIN [$(Dimensional)].Dimension.TransactionDetail d ON (d.TransactionDetailKey = t.TransactionDetailKey) 
						INNER JOIN [$(Dimensional)].Dimension.Contract c ON (c.ContractKey = t.ContractKey) 
						INNER JOIN [$(Dimensional)].Dimension.Account a ON (a.AccountKey = t.AccountKey) 
						INNER JOIN [$(Dimensional)].Dimension.DayZone z ON (z.DayKey = t.DayKey) 
						INNER JOIN [$(Dimensional)].Dimension.BetType bt ON (bt.BetTypeKey = t.BetTypeKey) 
						INNER JOIN [$(Dimensional)].Dimension.LegType lt ON (lt.LegTypeKey = t.LegTypeKey) 
						INNER JOIN [$(Dimensional)].Dimension.Wallet w ON (w.WalletKey = t.WalletKey) 
						INNER JOIN [$(Dimensional)].Dimension.Class cl ON (cl.ClassKey = t.ClassKey) 
						INNER JOIN [$(Dimensional)].Dimension.Channel ch ON (ch.ChannelKey = t.ChannelKey) 
						INNER JOIN [$(Dimensional)].Dimension.Channel ac ON (ac.ChannelKey = t.ActionChannelKey) 
						INNER JOIN [$(Dimensional)].Dimension.Campaign cp ON (cp.CampaignKey = t.CampaignKey) 
						INNER JOIN [$(Dimensional)].Dimension.Promotion pr ON (pr.PromotionKey = t.PromotionKey) 
						INNER JOIN [$(Dimensional)].Dimension.[User] u ON (u.UserKey = t.UserKey) 
						INNER JOIN [$(Dimensional)].Dimension.Event e ON (e.EventKey = t.EventKey) 
						INNER JOIN [$(Dimensional)].Dimension.Instrument i ON (i.InstrumentKey = t.InstrumentKey) 
						INNER JOIN [$(Dimensional)].Dimension.InPlay ip ON (ip.InplayKey = t.InplayKey) 

				WHERE	t.CreatedBatchKey = @BatchKey
						AND	@ReloadFromDimensional = 1
				UNION ALL
				SELECT	t.LegId, t.LegIdSource, t.BetID, t.BetGroupID, t.TransactionId, t.LegNumber, t.NumberOfLegs, t.LedgerID, t.LedgerSource, t.MasterDayText, z.AnalysisDayText, t.MasterTransactionTimestamp, 
						t.BalanceTypeID, t.TransactionTypeID, t.TransactionStatusID, t.TransactionMethodID, t.WalletId,
						t.BetGroupType, t.BetTypeName, t.BetSubTypeName,t.LegBetTypeName, t.GroupingType,
						t.ClassId, t.ClassIdSource, t.Channel, t.ActionChannel, t.CampaignId, t.PromotionID, t.UserID, t.UserIDSource, 
						t.EventID, t.EventIDSource, t.InstrumentID, t.instrumentIDSource, t.ClickToCall, t.InPlay, t.CashoutType, t.IsDoubleDown, t.IsDoubledDown, t.IsChaseTheAce, t.FeatureFlags,
						t.TransactedAmount
				FROM	Stage.[Transaction] t						
						INNER JOIN [$(Dimensional)].Dimension.DayZone z ON (z.MasterDayText = t.MasterDayText AND z.FromDate <= t.MasterTransactionTimestamp AND z.ToDate > t.MasterTransactionTimestamp) 
				WHERE	t.BatchKey = @BatchKey
						AND ExistsInDim = 0
						AND	@ReloadFromDimensional = 0
			) x
	WHERE	BalanceTypeID in ('CLI','UNS','P&L','BBK','PDP','PWD')			
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert to Staging','Start',@RowsProcessed = @@ROWCOUNT

	INSERT INTO Stage.KPI (BatchKey
           ,LegId
           ,LegIdSource
           ,LedgerId
           ,LedgerSource
           ,MasterTransactionTimestamp
           ,MasterDayText
           ,AnalysisDayText
           ,BetTypeName
           ,LegBetTypeName
           ,BetSubTypeName
           ,BetGroupType
           ,GroupingType
           ,Channel
           ,ActionChannel
           ,CampaignId
           ,WalletId
           ,LegNumber
           ,NumberOfLegs
           ,ClassId
           ,ClassIdSource
           ,UserID
           ,UserIDSource
           ,EventID
           ,EventIDSource
           ,InstrumentID
           ,InstrumentIDSource
           ,ClickToCall
           ,InPlay
		   ,CashoutType
		   ,IsDoubleDown
		   ,IsDoubledDown
		   ,IsChaseTheAce
		   ,FeatureFlags
           ,DepositsRequested
           ,DepositsCompleted
           ,DepositsRequestedCount
           ,DepositsCompletedCount
           ,Promotions
           ,BetsPlaced
           ,BonusBetsPlaced
           ,ContractsPlaced
           ,BonusContractsPlaced
           ,BettorsPlaced
           ,BonusBettorsPlaced
           ,PlayDaysPlaced
           ,BonusPlayDaysPlaced
           ,PlayFiscalWeeksPlaced
           ,BonusPlayFiscalWeeksPlaced
           ,PlayCalenderWeeksPlaced
           ,BonusPlayCalenderWeeksPlaced
           ,PlayFiscalMonthsPlaced
           ,BonusPlayFiscalMonthsPlaced
           ,PlayCalenderMonthsPlaced
           ,BonusPlayCalenderMonthsPlaced
           ,Stakes
           ,BonusStakes
           ,Winnings
           ,BettorsCashedOut
           ,BetsCashedOut
           ,Cashout
           ,CashoutDifferential
		   ,Fees
           ,WithdrawalsRequested
           ,WithdrawalsCompleted
           ,WithdrawalsRequestedCount
           ,WithdrawalsCompletedCount
           ,Adjustments
           ,Transfers
           ,BetsResulted
           ,BonusBetsResulted
           ,ContractsResulted
           ,BonusContractsResulted
           ,BettorsResulted
           ,BonusBettorsResulted
           ,PlayDaysResulted
           ,BonusPlayDaysResulted
           ,PlayFiscalWeeksResulted
           ,BonusPlayFiscalWeeksResulted
           ,PlayCalenderWeeksResulted
           ,BonusPlayCalenderWeeksResulted
           ,PlayFiscalMonthsResulted
           ,BonusPlayFiscalMonthsResulted
           ,PlayCalenderMonthsResulted
           ,BonusPlayCalenderMonthsResulted
           ,Turnover
           ,BonusTurnover
           ,GrossWin
           ,NetRevenue
           ,NetRevenueAdjustment
           ,BonusWinnings
           ,Betback
           ,GrossWinAdjustment
           ,FirstBet
           ,FirstDeposit
           ,AccountOpened
           ,CreatedDate)

	Select 
		BatchKey,
		LegId,
		LegIdSource, 
		LedgerID,
		LedgerSource,
		MasterTransactionTimestamp,
		MasterDayText,
		AnalysisDayText,
		BetTypeName,
		LegBetTypeName,
		BetSubTypeName,
		BetGroupType,
		GroupingType,
		Channel,
		ActionChannel,
		CampaignId,
		WalletId,
		LegNumber,
		NumberOfLegs,
		ClassId,
		ClassIdSource,
		UserID,
		UserIDSource,
		EventID,
		EventIDSource,
		InstrumentID,
		InstrumentIDSource,
		ClickToCall,
		InPlay,
		CashoutType,
		IsDoubleDown,
		IsDoubledDown,
		IsChaseTheAce,
		FeatureFlags,
		DepositsRequested,
		DepositsCompleted,
		DepositsRequestedCount,
		DepositsCompletedCount,
		Promotions,
		CASE WalletId WHEN 'B' THEN NULL ELSE BetsPlaced END as BetsPlaced,
		CASE WalletId WHEN 'B' THEN BetsPlaced ELSE NULL END as BonusBetsPlaced,
		CASE WalletId WHEN 'B' THEN NULL ELSE ContractsPlaced END as ContractsPlaced,
		CASE WalletId WHEN 'B' THEN ContractsPlaced ELSE NULL END as BonusContractsPlaced,
		CASE WalletId WHEN 'B' THEN NULL ELSE BettorsPlaced END as BettorsPlaced,
		CASE WalletId WHEN 'B' THEN BettorsPlaced ELSE NULL END as BonusBettorsPlaced,
		CASE WalletId WHEN 'B' THEN NULL ELSE PlayDaysPlaced END as PlayDaysPlaced,
		CASE WalletId WHEN 'B' THEN PlayDaysPlaced ELSE NULL END as BonusPlayDaysPlaced,
		CASE WalletId WHEN 'B' THEN NULL ELSE PlayFiscalWeeksPlaced END as PlayFiscalWeeksPlaced,
		CASE WalletId WHEN 'B' THEN PlayFiscalWeeksPlaced ELSE NULL END as BonusPlayFiscalWeeksPlaced,
		CASE WalletId WHEN 'B' THEN NULL ELSE PlayCalenderWeeksPlaced END as PlayCalenderWeeksPlaced,
		CASE WalletId WHEN 'B' THEN PlayCalenderWeeksPlaced ELSE NULL END as BonusPlayCalenderWeeksPlaced,
		CASE WalletId WHEN 'B' THEN NULL ELSE PlayFiscalMonthsPlaced END as PlayFiscalMonthsPlaced,
		CASE WalletId WHEN 'B' THEN PlayFiscalMonthsPlaced ELSE NULL END as BonusPlayFiscalMonthsPlaced,
		CASE WalletId WHEN 'B' THEN NULL ELSE PlayCalenderMonthsPlaced END as PlayCalenderMonthsPlaced,
		CASE WalletId WHEN 'B' THEN PlayCalenderMonthsPlaced ELSE NULL END as BonusPlayCalenderMonthsPlaced,
		CASE WalletId WHEN 'B' THEN 0 ELSE Stakes END  as Stakes,
		CASE WalletId WHEN 'B' THEN Stakes ELSE 0 END  as BonusStakes,
		Winnings,
		BettorsCashedOut,
		BetsCashedOut,
		Cashout,
		CashoutDifferential,
		Fees,
		WithdrawalsRequested,
		WithdrawalsCompleted,
		WithdrawalsRequestedCount,
		WithdrawalsCompletedCount,
		Adjustments,
		Transfers,
		CASE WalletId WHEN 'B' THEN NULL ELSE BetsResulted END as BetsSettled,
		CASE WalletId WHEN 'B' THEN BetsResulted ELSE NULL END as BonusBetsSettled,
		CASE WalletId WHEN 'B' THEN NULL ELSE ContractsResulted END as ContractsSettled ,
		CASE WalletId WHEN 'B' THEN ContractsResulted ELSE NULL END as BonusContractsSettled,
		CASE WalletId WHEN 'B' THEN NULL ELSE BettorsResulted END as BettorsSettled,
		CASE WalletId WHEN 'B' THEN BettorsResulted ELSE NULL END as BonusBettorsSettled,
		CASE WalletId WHEN 'B' THEN NULL ELSE PlayDaysResulted END as PlayDaysResulted,
		CASE WalletId WHEN 'B' THEN PlayDaysResulted ELSE NULL END as BonusPlayDaysResulted,
		CASE WalletId WHEN 'B' THEN NULL ELSE PlayFiscalWeeksResulted END as PlayFiscalWeeksResulted,
		CASE WalletId WHEN 'B' THEN PlayFiscalWeeksResulted ELSE NULL END as BonusPlayFiscalWeeksResulted,
		CASE WalletId WHEN 'B' THEN NULL ELSE PlayCalenderWeeksResulted END as PlayCalenderWeeksResulted,
		CASE WalletId WHEN 'B' THEN PlayCalenderWeeksResulted ELSE NULL END as BonusPlayCalenderWeeksResulted,
		CASE WalletId WHEN 'B' THEN NULL ELSE PlayFiscalMonthsResulted END as PlayFiscalMonthsResulted,
		CASE WalletId WHEN 'B' THEN PlayFiscalMonthsResulted ELSE NULL END as BonusPlayFiscalMonthsResulted,
		CASE WalletId WHEN 'B' THEN NULL ELSE PlayCalenderMonthsResulted END as PlayCalenderMonthsResulted,
		CASE WalletId WHEN 'B' THEN PlayCalenderMonthsResulted ELSE NULL END as BonusPlayCalenderMonthsResulted,
		CASE WalletId WHEN 'B' THEN 0 ELSE Turnover END as Turnover,
		CASE WalletId WHEN 'B' THEN Turnover ELSE 0 END as BonusTurnover,
		GrossWin,
		NetRevenue,
		NetRevenueAdjustment,
		BonusWinnings,
		Betback,
		GrossWinAdjustment,
		FirstBet,
		FirstDeposit,
		AccountOpened,
		CreatedDate 
	From
	(
	SELECT 
		@BatchKey as BatchKey,
		LegId,
		LegIdSource, 
		LedgerID,
		LedgerSource,
		MasterTransactionTimestamp,
		MasterDayText,
		AnalysisDayText,
		BetTypeName,
		LegBetTypeName,
		BetSubTypeName,
		BetGroupType,
		GroupingType,
		Channel,
		ActionChannel,
		CampaignId,
		WalletId,
		LegNumber,
		NumberOfLegs,
		ClassId,
		ClassIdSource,
		UserID,
		UserIDSource,
		EventID,
		EventIDSource,
		InstrumentID,
		InstrumentIDSource,
		ClickToCall,
		InPlay,
		CashoutType,
		IsDoubleDown,
		IsDoubledDown,
		IsChaseTheAce,
		FeatureFlags,
		SUM(DepositsRequested) DepositsRequested,
		SUM(DepositsCompleted) DepositsCompleted,
		MAX(DepositsRequestedCount) DepositsRequestedCount,
		MAX(DepositsCompletedCount) DepositsCompletedCount,
		SUM(Promotions) Promotions,
		MAX(BettorsPlaced) BettorsPlaced,
		MAX(BetsPlaced) BetsPlaced,
		MAX(ContractsPlaced) ContractsPlaced,
		MAX(PlayDaysPlaced) PlayDaysPlaced,
		MAX(PlayFiscalWeeksPlaced) PlayFiscalWeeksPlaced,
		MAX(PlayCalenderWeeksPlaced) PlayCalenderWeeksPlaced,
		MAX(PlayFiscalMonthsPlaced) PlayFiscalMonthsPlaced,
		MAX(PlayCalenderMonthsPlaced) PlayCalenderMonthsPlaced,
		SUM(Stakes) Stakes,
		SUM(Winnings) Winnings,
		MAX(BettorsCashedOut) BettorsCashedOut,
		MAX(BetsCashedOut) BetsCashedOut,
		SUM(Cashout) Cashout,
		SUM(CashoutDifferential) CashoutDifferential,
		SUM(Fees) Fees,
		SUM(WithdrawalsRequested) WithdrawalsRequested,
		SUM(WithdrawalsCompleted) WithdrawalsCompleted,
		SUM(WithdrawalsRequestedCount) WithdrawalsRequestedCount,
		SUM(WithdrawalsCompletedCount) WithdrawalsCompletedCount,
		SUM(Adjustments) Adjustments,
		SUM(Transfers) Transfers,
		MAX(BettorsResulted) BettorsResulted,
		MAX(ContractsResulted) ContractsResulted,
		MAX(BetsResulted) BetsResulted,
		MAX(PlayDaysResulted) PlayDaysResulted,
		MAX(PlayFiscalWeeksResulted) PlayFiscalWeeksResulted,
		MAX(PlayCalenderWeeksResulted) PlayCalenderWeeksResulted,
		MAX(PlayFiscalMonthsResulted) PlayFiscalMonthsResulted,
		MAX(PlayCalenderMonthsResulted) PlayCalenderMonthsResulted,
		SUM(Turnover) Turnover,
		SUM(GrossWin) GrossWin,
		SUM(NetRevenue) NetRevenue,
		SUM(NetRevenueAdjustment) NetRevenueAdjustment,
		SUM(BonusWinnings) BonusWinnings,
		SUM(Betback) Betback,
		SUM(GrossWinAdjustment) GrossWinAdjustment,
		MIN(FirstBet) FirstBet,
		MIN(FirstDeposit) FirstDeposit,
		MAX(AccountOpened) AccountOpened,
		CONVERT(datetime2,GETDATE()) CreatedDate

	FROM  
	( 
		SELECT
		t.BalanceTypeID,
		t.LegId,
		t.LegIdSource,
		t.LedgerID,
		t.LedgerSource,
		t.MasterTransactionTimestamp,
		t.MasterDayText,
		y.AnalysisDayText,
		t.BetTypeName,
		t.LegBetTypeName,
		t.BetSubTypeName,
		t.BetGroupType,
		t.GroupingType,
		t.Channel,
		t.ActionChannel,
		t.CampaignId,
		t.WalletId,
		t.LegNumber,
		t.NumberOfLegs,
		t.ClassId,
		t.ClassIdSource,
		t.UserID,
		t.UserIDSource,
		t.EventID,
		t.EventIDSource,
		t.InstrumentID,
		t.instrumentIDSource,
		t.ClickToCall,
		t.InPlay,
		t.CashoutType,
		t.IsDoubleDown,
		t.IsDoubledDown,
		t.IsChaseTheAce,
		t.FeatureFlags,
		CONVERT(int, NULL) AccountOpened,
		--fd.FirstCreditId FirstDeposit,
		--fb.FirstBetId FirstBet,
		CASE WHEN t.BalanceTypeID = 'PDP'			AND t.TransactionTypeID = 'DP' AND t.TransactionStatusID IN('RE','DE','RJ')				THEN fd.FirstCreditId		ELSE NULL	END		FirstDeposit,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID = 'RE'							THEN fb.FirstBetId			ELSE NULL	END		FirstBet,
		CASE WHEN t.BalanceTypeID = 'PDP'			AND t.TransactionTypeID = 'DP' AND t.TransactionStatusID IN('RE','DE','RJ')				THEN t.TransactedAmount		ELSE 0		END		DepositsRequested,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'DP' AND t.TransactionStatusID IN('CO','RV','RJ')				THEN t.TransactedAmount		ELSE 0		END		DepositsCompleted,
		CASE WHEN t.BalanceTypeID = 'PDP'			AND t.TransactionTypeID = 'DP' AND t.TransactionStatusID IN('RE','DE','RJ')				THEN t.TransactionId		ELSE 0		END		DepositsRequestedCount,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'DP' AND t.TransactionStatusID IN('CO','RV','RJ')				THEN t.TransactionId		ELSE 0		END		DepositsCompletedCount,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BO' AND w.WalletName<>'Bonus' AND TransactionMethodID NOT IN ('BP','RB') THEN t.TransactedAmount		ELSE 0		END		Promotions,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID = 'RE'						THEN t.LedgerID			ELSE NULL	END		BettorsPlaced,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID = 'RE'						THEN CONVERT(BIGINT,CONVERT(VARCHAR,t.LedgerID) + REPLACE(t.MasterDayText,'-',''))	 ELSE NULL END PlayDaysPlaced,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID = 'RE'						THEN CONVERT(BIGINT,CONVERT(VARCHAR,t.LedgerID) + CONVERT(VARCHAR,dd.FiscalWeekKey)) ELSE NULL END PlayFiscalWeeksPlaced,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID = 'RE'						THEN CONVERT(BIGINT,CONVERT(VARCHAR,t.LedgerID) + CONVERT(VARCHAR,dd.WeekKey)) ELSE NULL END PlayCalenderWeeksPlaced,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID = 'RE'						THEN CONVERT(BIGINT,CONVERT(VARCHAR,t.LedgerID) + CONVERT(VARCHAR,dd.FiscalMonthKey)) ELSE NULL END PlayFiscalMonthsPlaced,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID = 'RE'						THEN CONVERT(BIGINT,CONVERT(VARCHAR,t.LedgerID) + CONVERT(VARCHAR,dd.MonthKey)) ELSE NULL END PlayCalenderMonthsPlaced,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID = 'RE'						THEN t.BetID				ELSE NULL	END		BetsPlaced,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID = 'RE'						THEN t.BetGroupID			ELSE NULL	END		ContractsPlaced,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('RE','CN')				THEN t.TransactedAmount		
			WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'FE'														THEN t.TransactedAmount	ELSE 0		END		Stakes,
	--	CASE WHEN t.BalanceTypeID IN ('P&L','UNS')	AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID = 'WN'						THEN -t.TransactedAmount	ELSE 0		END		Winnings_Ancient,
		--CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('LO', 'WN','UN') 
		--																			AND t.TransactionMethodID='SE'						THEN -t.TransactedAmount	
		--	 WHEN t.BalanceTypeID = 'P&L'			AND t.TransactionTypeID = 'BT' AND w.WalletName<>'Free Winnings'					THEN -t.TransactedAmount	ELSE 0		END		Winnings_Old,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('WN','UN','CO') 	
									AND t.TransactionMethodID IN ('SE','AD') AND w.WalletName<>'Bonus'	and t.PromotionID IN (0,-1)		THEN t.TransactedAmount	ELSE 0		END		Winnings,
		CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('CO')					THEN t.LedgerID			ELSE NULL	END		BettorsCashedOut,
		CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('CO')					THEN t.BETID			ELSE NULL	END		BetsCashedOut,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('CO')					THEN t.TransactedAmount	ELSE 0		END		Cashout,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('CO')					THEN t.TransactedAmount	ELSE 0		END		CashoutDifferential,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'FE'														THEN t.TransactedAmount	ELSE 0		END		Fees,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'WI' AND t.TransactionStatusID in('RE','DE','CN','UR','RJ','RV')	THEN t.TransactedAmount		ELSE 0		END		WithdrawalsRequested,
		CASE WHEN t.BalanceTypeID = 'PWD'			AND t.TransactionTypeID = 'WI' AND t.TransactionStatusID IN ('CO')							THEN t.TransactedAmount		
			WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'WI' AND t.TransactionStatusID IN ('RJ')							THEN t.TransactedAmount		ELSE 0		END		WithdrawalsCompleted,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'WI' AND t.TransactionStatusID in('RE','DE','CN','UR','RJ','RV')	THEN t.TransactionId		ELSE 0		END		WithdrawalsRequestedCount,
		CASE WHEN t.BalanceTypeID = 'PWD'			AND t.TransactionTypeID = 'WI' AND t.TransactionStatusID IN ('CO')							THEN t.TransactionId		
			WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'WI' AND t.TransactionStatusID IN ('RJ')							THEN t.TransactionId		ELSE 0		END		WithdrawalsCompletedCount,
		--CASE WHEN t.BalanceTypeID = 'PWD'			AND t.TransactionTypeID = 'WI' AND t.TransactionStatusID IN ('RE','DE','CN','RV')	THEN t.TransactedAmount
		--	 WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'WI' AND t.TransactionStatusID IN ('RJ')					THEN t.TransactedAmount		ELSE 0		END		WithdrawalsCompleted,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'AD'														THEN t.TransactedAmount
			 WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BO' AND w.WalletName<>'Bonus' AND TransactionMethodID IN ('BP','RB') THEN t.TransactedAmount		ELSE 0		END		Adjustments,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'TR'														THEN t.TransactedAmount		ELSE 0		END		Transfers,
		CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('WN','LO','CO')		 
																				AND t.TransactionMethodID='SE'							THEN t.LedgerID			ELSE NULL	END		BettorsResulted,
		CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('WN','LO','CO')		 
																				AND t.TransactionMethodID='SE'							THEN CONVERT(BIGINT,CONVERT(VARCHAR,t.LedgerID) + REPLACE(t.MasterDayText,'-','')) ELSE NULL END PlayDaysResulted,
		CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('WN','LO','CO')		 
																				AND t.TransactionMethodID='SE'							THEN CONVERT(BIGINT,CONVERT(VARCHAR,t.LedgerID) + CONVERT(VARCHAR,dd.FiscalWeekKey)) ELSE NULL END PlayFiscalWeeksResulted,
		CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('WN','LO','CO')		 
																				AND t.TransactionMethodID='SE'							THEN CONVERT(BIGINT,CONVERT(VARCHAR,t.LedgerID) + CONVERT(VARCHAR,dd.WeekKey)) ELSE NULL END PlayCalenderWeeksResulted,
		CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('WN','LO','CO')		 
																				AND t.TransactionMethodID='SE'							THEN CONVERT(BIGINT,CONVERT(VARCHAR,t.LedgerID) + CONVERT(VARCHAR,dd.FiscalMonthKey)) ELSE NULL END PlayFiscalMonthsResulted,
		CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('WN','LO','CO')		 
																				AND t.TransactionMethodID='SE'							THEN CONVERT(BIGINT,CONVERT(VARCHAR,t.LedgerID) + CONVERT(VARCHAR,dd.MonthKey)) ELSE NULL END PlayCalenderMonthsResulted,
		CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('WN','LO','CO')		 
																				AND t.TransactionMethodID='SE'							THEN t.BETID				ELSE NULL	END		BetsResulted,
		CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('WN','LO','CO')		
																				AND t.TransactionMethodID='SE'							THEN t.BetGroupID			ELSE NULL	END		ContractsResulted,
		CASE WHEN t.BalanceTypeID = 'UNS'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('LO', 'WN','UN','CO') 
																				/*AND t.TransactionMethodID='SE'*/						THEN -t.TransactedAmount		
			WHEN t.BalanceTypeID = 'P&L'			AND t.TransactionTypeID = 'FE'														THEN t.TransactedAmount		ELSE 0		END		Turnover,
		--CASE WHEN t.BalanceTypeID = 'P&L'			AND t.TransactionTypeID = 'BT' AND w.WalletName<>'Free Winnings'					THEN t.TransactedAmount		ELSE 0		END		GrossWin,
		CASE WHEN t.BalanceTypeID = 'P&L'			AND t.TransactionTypeID = 'BT' AND w.WalletName<>'Bonus' and t.PromotionID IN (0,-1)THEN t.TransactedAmount		
			WHEN t.BalanceTypeID = 'P&L'			AND t.TransactionTypeID = 'FE'														THEN t.TransactedAmount		ELSE 0		END		GrossWin,
		CASE WHEN t.BalanceTypeID = 'P&L'			AND t.TransactionTypeID = 'BT' AND w.WalletName<>'Bonus'							THEN t.TransactedAmount		
			WHEN t.BalanceTypeID = 'P&L'			AND t.TransactionTypeID = 'FE'														THEN t.TransactedAmount		ELSE 0		END		NetRevenue,
		CASE WHEN t.BalanceTypeID = 'P&L'			AND t.TransactionTypeID = 'BT' AND t.TransactionMethodID='AD' AND w.WalletName<>'Bonus'	THEN t.TransactedAmount		ELSE 0		END		NetRevenueAdjustment,
		--CASE WHEN t.BalanceTypeID = 'P&L'			AND t.TransactionTypeID = 'BT' AND w.WalletName='Free Winnings'						THEN t.TransactedAmount		ELSE 0		END		BonusWinnings,
		CASE WHEN t.BalanceTypeID = 'CLI'			AND t.TransactionTypeID = 'BT' AND t.TransactionStatusID IN ('WN','UN','CO') 	
									AND t.TransactionMethodID IN ('SE','AD') AND w.WalletName<>'Bonus'	and t.PromotionID NOT IN (0,-1)	THEN t.TransactedAmount		ELSE 0		END		BonusWinnings,
		CASE WHEN t.BalanceTypeID = 'BBK'			AND t.TransactionTypeID = 'BB' AND t.TransactionStatusID IN ('WN','LO')				THEN t.TransactedAmount		ELSE 0		END		Betback,
		CASE WHEN t.BalanceTypeID = 'P&L'			AND t.TransactionTypeID = 'BT' AND w.WalletName<>'Bonus' and t.PromotionID IN (0,-1)
																					AND t.TransactionMethodID='AD'						THEN t.TransactedAmount		ELSE 0		END		GrossWinAdjustment

		FROM #Transaction t WITH (NOLOCK)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.DayZone y	WITH (NOLOCK)	ON (t.MasterDayText=y.MasterDayText AND T.MasterTransactionTimestamp>=y.FromDate AND T.MasterTransactionTimestamp<y.ToDate)
		LEFT OUTER JOIN [$(Dimensional)].Dimension.Day dd WITH (NOLOCK) ON y.MasterDayKey = dd.DayKey
		LEFT OUTER JOIN [$(Dimensional)].Dimension.Wallet w	WITH (NOLOCK)	ON (w.WalletId = t.[WalletId])
		LEFT OUTER JOIN Stage.Position fb					WITH (NOLOCK)	ON t.BetId = fb.FirstBetId and t.MasterDayText = fb.MasterDayText and fb.BatchKey = @BatchKey
		LEFT OUTER JOIN Stage.Position fd					WITH (NOLOCK)	ON t.TransactionId = fd.FirstCreditId and t.MasterDayText = fd.MasterDayText and fd.BatchKey = @BatchKey
		WHERE  t.BalanceTypeID in ('CLI','UNS','P&L','BBK','PDP','PWD')
	) x
	GROUP BY 
		LegId,
		LegIdSource,
		LedgerID,
		LedgerSource,
		MasterTransactionTimestamp,
		MasterDayText,
		AnalysisDayText,
		BetTypeName,
		LegBetTypeName,
		BetSubTypeName,
		BetGroupType,
		GroupingType,
		Channel,
		ActionChannel,
		CampaignId,
		WalletId,
		LegNumber,
		NumberOfLegs,
		ClassId,
		ClassIdSource,
		UserID,
		UserIDSource,
		EventID,
		EventIDSource,
		InstrumentID,
		InstrumentIDSource,
		ClickToCall,
		InPlay,
		CashoutType,
		IsDoubleDown,
		IsDoubledDown,
		IsChaseTheAce,
		FeatureFlags
) a
	WHERE  AccountOpened <> 0
		OR DepositsRequested <> 0
		OR DepositsCompleted <> 0
		OR Promotions <> 0
		OR BetsPlaced <> 0
		OR Stakes <> 0
		OR Winnings <> 0
		OR Cashout <> 0
		OR CashoutDifferential <> 0
		OR Fees <> 0
		OR WithdrawalsRequested <> 0
		OR WithdrawalsCompleted <> 0
		OR Adjustments <> 0
		OR Transfers <> 0
		OR BetsResulted <> 0
		OR Turnover <> 0
		OR GrossWin <> 0
		OR Betback <> 0
		OR BonusWinnings <> 0
		OR NetRevenue <> 0
		OR NetRevenueAdjustment <> 0
		OR GrossWinAdjustment <> 0
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY

BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
