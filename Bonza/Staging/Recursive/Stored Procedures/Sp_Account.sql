﻿CREATE PROC [Recursive].[Sp_Account]
(
	@BatchKey	INT,
	@RowsProcessed	INT = 0 OUTPUT
)
AS 
BEGIN TRY

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount int

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, @Me, 'Missing LedgerId','Start';
	
	WITH Missing AS
	(	SELECT	s.LedgerID,
				s.LedgerSource,
				MIN(s.FromDate) FromDate
		FROM	Stage.[Transaction] S
				LEFT OUTER JOIN [$(Dimensional)].Dimension.Account d ON s.LedgerID = d.LedgerID AND s.LedgerSource = d.LedgerSource
		WHERE	s.ExistsInDim = 0
				AND s.LedgerId <> -1
				AND d.AccountKey IS NULL
				AND s.BatchKey = @BatchKey
		GROUP BY S.LedgerID, S.LedgerSource		
	)
	INSERT	[$(Dimensional)].Dimension.Account
			(	CompanyKey, LedgerOpenedDayKey, AccountOpenedDayKey, AccountOpenedChannelKey, 
				LedgerID, LedgerSource, LegacyLedgerID, LedgerSequenceNumber, LedgerTypeID, LedgerType, LedgerGrouping, LedgerOpenedDate, LedgerOpenedDateOrigin, 
				AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags, AccountTypeCode, AccountType, AccountOpenedDate, 
				BDM, VIPCode, VIP, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords, StatementMethod, StatementFrequency, 
				CardAccountId, CardFirstIssueDate, CardIssueDate, CardStatusId, CardStatusCode, CardStatus, EachwayAccountId, VelocityNumber, VelocityLinkedDate, 
				CustomerID, CustomerSource, Title, Surname, FirstName, MiddleName, Gender, DOB, Address1, Address2, Suburb, StateCode, State, PostCode, CountryCode, Country, GeographicalLocation,
				AustralianClient, Phone, Mobile, Fax, Email, AddressLastChanged, PhoneLastChanged, EmailLastChanged,
				AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB, 
				HasHomeAddress, HomeAddressLastChanged, HomePhoneLastChanged, HomeEmailLastChanged, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
				HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail,
				HasPostalAddress, PostalAddressLastChanged, PostalPhoneLastChanged, PostalEmailLastChanged, PostalAddress1, PostalAddress2, PostalSuburb, PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry, 
				PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail, 
				HasBusinessAddress, BusinessAddressLastChanged, BusinessPhoneLastChanged, BusinessEmailLastChanged, BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
				BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail, 
				HasOtherAddress, OtherAddressLastChanged, OtherPhoneLastChanged, OtherEmailLastChanged, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry, 
				OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail,
				SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignID, SourceKeywords, 
				FirstDate, FromDate, ToDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy,Increment
			)
	SELECT	CompanyKey, LedgerOpenedDayKey, AccountOpenedDayKey, AccountOpenedChannelKey, 
			m.LedgerID, m.LedgerSource, LegacyLedgerID, LedgerSequenceNumber, LedgerTypeID, LedgerType, LedgerGrouping, LedgerOpenedDate, LedgerOpenedDateOrigin, 
			AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags, AccountTypeCode, AccountType, AccountOpenedDate, 
			BDM, VIPCode, VIP, BTag2, AffiliateID, AffiliateName, SiteID, TrafficSource, RefURL, CampaignID, Keywords, StatementMethod, StatementFrequency, 
			CardAccountId, CardFirstIssueDate, CardIssueDate, CardStatusId, CardStatusCode, CardStatus, EachwayAccountId, VelocityNumber, VelocityLinkedDate, 
			CustomerID, CustomerSource, Title, Surname, FirstName, MiddleName, Gender, DOB, Address1, Address2, Suburb, StateCode, State, PostCode, CountryCode, Country, GeographicalLocation,
			AustralianClient, Phone, Mobile, Fax, Email, AddressLastChanged, PhoneLastChanged, EmailLastChanged,
			AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB, 
			HasHomeAddress, HomeAddressLastChanged, HomePhoneLastChanged, HomeEmailLastChanged, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
			HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail,
			HasPostalAddress, PostalAddressLastChanged, PostalPhoneLastChanged, PostalEmailLastChanged, PostalAddress1, PostalAddress2, PostalSuburb, PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry, 
			PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail, 
			HasBusinessAddress, BusinessAddressLastChanged, BusinessPhoneLastChanged, BusinessEmailLastChanged, BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
			BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail, 
			HasOtherAddress, OtherAddressLastChanged, OtherPhoneLastChanged, OtherEmailLastChanged, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry, 
			OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail,
			SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignID, SourceKeywords, 
			m.FromDate FirstDate, m.FromDate, CONVERT(datetime2(3),'9999-12-31') ToDate, SYSDATETIME() CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, SYSDATETIME() ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy, NULL Increment
	FROM	Missing m
			INNER JOIN [$(Dimensional)].Dimension.Account a ON a.LedgerID = -1 AND a.LedgerSource = 'Unk'
	OPTION(RECOMPILE);

	SET @RowCount  = @@ROWCOUNT;
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL,'Success', @RowsProcessed = @RowCount;
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH