﻿/*
	Developer: Lekha Narra
	Date: 18/07/2016
	Desc: Search for JobIDs that are missing from dimension table, but exist in fact table. Insert stub records into dimension table.
		
	Test: EXEC [Recursive].[Sp_Dimension_Communication] @BatchKey = 252;
*/

CREATE PROC [Recursive].[Sp_Dimension_Communication]
(
	@BatchKey	INT,
	@RowsProcessed	INT = 0 OUTPUT
)
AS 
BEGIN TRY

	DECLARE @ProcName VARCHAR(255) = (SELECT OBJECT_NAME(@@PROCID));

	DECLARE @RowCount int

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Get Missing Keys','Start';
	
	-- Get list of natural keys from stage.Contact
	-- check natural keys exist in dimension

	-- Create new records based on -1 record
SELECT [CommunicationKey]
      ,[CommunicationId]
      ,[CommunicationSource]
      ,[Title]
      ,[Details]
      ,[SubDetails]
    INTO #DefaultUnknown
    FROM [$(Dimensional)].[Dimension].[Communication]
    WHERE CommunicationKey = -1;

	SELECT DISTINCT
		S.JobID,
		S.JobIDSource
	INTO #Dim
	FROM Stage.[Contact] S
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Communication DC ON S.JobID = DC.CommunicationID AND S.JobIDSource = DC.CommunicationSource
	WHERE s.JobID is not null and DC.CommunicationKey IS NULL
	AND S.BatchKey = @BatchKey
	OPTION(RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey, @ProcName, 'Insert into Staging','Start', @RowsProcessed = @@ROWCOUNT;

	-- insert missing keys into staging.Communication table
	INSERT INTO Stage.Communication ([BatchKey]      ,[CommunicationId]      ,[CommunicationSource]      ,[Title]      ,[Details]      ,[SubDetails]) 
	SELECT DISTINCT
		@BatchKey as [BatchKey]
		,D.[JobID]      
		,D.[JobIDSource]      
		,U.[Title]      
		,U.[Details]      
		,U.[SubDetails]
	FROM #Dim as D
	LEFT JOIN Stage.Communication as C on C.CommunicationId = D.JobID AND C.CommunicationSource = D.JobIDSource AND C.BatchKey = @BatchKey
	CROSS APPLY (SELECT * FROM #DefaultUnknown) as U
	WHERE C.CommunicationID IS NULL
	OPTION(RECOMPILE);

	SET @RowCount  = @@ROWCOUNT;
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL,'Success', @RowsProcessed = @RowCount;
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @ProcName, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH