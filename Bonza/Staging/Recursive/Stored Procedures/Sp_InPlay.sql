﻿CREATE PROC [Recursive].[Sp_InPlay]
(
	@BatchKey	INT,
	@RowsProcessed	INT = 0 OUTPUT
)
AS 
BEGIN TRY

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount int

	--SET STATISTICS IO ON;
	--SET STATISTICS TIME ON;

	SET ANSI_NULLS ON;
	SET QUOTED_IDENTIFIER ON;
	SET NOCOUNT ON;

	EXEC [Control].Sp_Log @BatchKey, @Me, 'Missing InPlay Keys','Start';
	
	WITH Missing AS
	(	SELECT	s.InPlay, s.ClickToCall, s.CashoutType, s.IsDoubleDown, s.IsDoubledDown, s.IsChaseTheAce, s.FeatureFlags,
				MIN(s.FromDate) FromDate
		FROM	Stage.[Transaction] S
				LEFT OUTER JOIN [$(Dimensional)].Dimension.InPlay d ON d.InPlay = s.InPlay and d.ClickToCall = s.ClickToCall and d.CashoutType = s.CashoutType AND d.IsDoubleDown = s.IsDoubleDown AND d.IsDoubledDown = s.IsDoubledDown AND d.IsChaseTheAce = s.IsChaseTheAce AND d.FeatureFlags = s.FeatureFlags
		WHERE	s.ExistsInDim = 0
				AND s.LedgerId <> -1
				AND d.InPlayKey IS NULL
				AND s.BatchKey = @BatchKey
		GROUP BY s.InPlay, s.ClickToCall, s.CashoutType, s.IsDoubleDown, s.IsDoubledDown, s.IsChaseTheAce, s.FeatureFlags
	)
	INSERT	[$(Dimensional)].Dimension.InPlay
			(	InPlay, ClickToCall, CashoutType, CashedOut, StatusAtCashout, IsDoubleDown, IsDoubledDown, DoubleDown, IsChaseTheAce, FeatureFlags,
				FirstDate, FromDate, ToDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy
			)
	SELECT	m.InPlay, m.ClickToCall, m.CashoutType, CashedOut, StatusAtCashout, m.IsDoubleDown, m.IsDoubledDown, DoubleDown, m.IsChaseTheAce, m.FeatureFlags,
			m.FromDate FirstDate, m.FromDate, CONVERT(datetime2(3),'9999-12-31') ToDate, SYSDATETIME() CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, SYSDATETIME() ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy
	FROM	Missing m -- Only the FeatureFlag column makes this a type 1 dimension currently, otherwise all other columns are fixed, so if set of keys encountered which are not present in Dimension, base new record on exisiting record with zero FeatureFlag
			INNER JOIN [$(Dimensional)].Dimension.InPlay i ON i.InPlay = m.InPlay and i.ClickToCall = m.ClickToCall and i.CashoutType = m.CashoutType AND i.IsDoubleDown = m.IsDoubleDown AND i.IsDoubledDown = m.IsDoubledDown AND i.IsChaseTheAce = m.IsChaseTheAce AND i.FeatureFlags = 0
	OPTION(RECOMPILE);

	SET @RowCount  = @@ROWCOUNT;
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL,'Success', @RowsProcessed = @RowCount;
	
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH