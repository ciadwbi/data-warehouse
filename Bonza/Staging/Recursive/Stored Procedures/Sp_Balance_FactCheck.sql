Create PROC [Recursive].[Sp_Balance_FactCheck]
(
	@BatchKey	int
)
AS
BEGIN TRY


	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	EXEC [Control].Sp_Log @BatchKey,'Recursive.Sp_BalanceFactCheck','Initialise current transactions','Start';

	-------------------------------------------------------------------------------
	---Current Batch Transactions that needs to be used throughout the process-----
	-------------------------------------------------------------------------------

	SELECT	BatchKey,
			BalanceTypeID,
			LedgerId,
			LedgerSource,
			CASE 
				WHEN WalletId = 'C' THEN WalletID
				ELSE 'B'
			END AS WalletId,
			TransactedAmount,
			MasterDayText,
			FromDate,
			MasterTransactionTimestamp
	INTO #Txns
	FROM [Stage].[Transaction]
	WHERE BatchKey = @BatchKey
	AND ExistsInDim = 0
	OPTION (RECOMPILE);

	SELECT	
		BatchKey,
		BalanceTypeID,
		LedgerId,
		LedgerSource,
		WalletId,
		MasterDayText,
		AnalysisDayText,
		SUM(IsNull(TransactedAmount,0))	as TransactedAmount	
	INTO #SummedTransactionsAnalysis
	FROM
	(
		SELECT	a.*,
				CONVERT(VARCHAR(11),CONVERT(DATE,DATEADD(MI,Z.OffsetMinutes,a.MasterTransactionTimestamp)))	AS AnalysisDayText			
		FROM #Txns a
		INNER JOIN Reference.TimeZone Z ON a.MasterTransactionTimestamp>=Z.FromDate AND a.MasterTransactionTimestamp<Z.ToDate AND Z.TimeZone='Analysis'
	) a
	GROUP BY
		BatchKey,
		BalanceTypeID,
		LedgerId,
		LedgerSource,
		WalletId,
		MasterDayText,
		AnalysisDayText
	OPTION (RECOMPILE);

	----------------------------------------------------------------------------
	----------------------------------------------------------------------------

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_BalanceFactCheck','Time Boundary Analysis','Start'

	-----------------------------------------------------------------------------
	------------------------CHECK BATCH FOR TIME BOUNDARY CROSSING---------------
	-----------------------------------------------------------------------------

	DECLARE @PreviousAnalysisDayText VARCHAR(11)
	DECLARE @LastDayKey INT
	DECLARE @NextAnalysisDayText VARCHAR(11)

	SET @PreviousAnalysisDayText = null
	SET @PreviousAnalysisDayText = 
	(
	-----IF CROSSED DAY BOUNDARY FOR ANALYSIS-----
	SELECT
			CONVERT(VARCHAR(11),BatchToDate) as AnalysisDayText
	FROM [Control].ETLController a
	INNER JOIN [Reference].[TimeZone] b on a.BatchFromDate >= b.FromDate and a.BatchToDate < b.ToDate
	LEFT OUTER JOIN [$(Acquisition)].[IntraBet].[Latency] c on a.BatchToDate = c.OriginalDate
	WHERE BatchKey = @BatchKey
	AND b.TimeZone = 'Analysis'
	AND IsNull(c.CorrectedDate,BatchToDate) >= DATEADD(MI,-1*OffsetMinutes,(DATEADD(DD,1,CONVERT(DATETIME,CONVERT(DATE,IsNull(c.CorrectedDate,BatchToDate)))))) -- UNCOMMENT WHEN DONE--!!!##@#@
	)


	IF @PreviousAnalysisDayText IS NOT NULL
	BEGIN

		----IF SO, THEN GET DATA FROM DIM AND LOAD----

		SET @NextAnalysisDayText = CONVERT(VARCHAR(11),DATEADD(DD,1,CONVERT(DATE,@PreviousAnalysisDayText)))
		SET @LastDayKey = (Select MAX(DayKey) From [$(Dimensional)].Dimension.DayZone WHERE AnalysisDayText = @PreviousAnalysisDayText and MasterDayText = 'N/A')

		SELECT	@BatchKey as BatchKey,
				BT.BalanceTypeID,
				a.LedgerId,
				a.LedgerSource,
				W.WalletId,
				'N/A' as MasterDayText,
				@NextAnalysisDayText as AnalysisDayText,
				OpeningBalance,
				TotalTransactedAmount as TransactedAmount,
				OpeningBalance as ClosingBalance,
				CONVERT(DateTime,@NextAnalysisDayText) as FromDate
		INTO #LastBalance
		FROM [$(Dimensional)].Fact.Balance a
		INNER JOIN [$(Dimensional)].Dimension.BalanceType BT ON a.BalanceTypeKey = BT.BalanceTypeKey
		INNER JOIN [$(Dimensional)].Dimension.Wallet W ON a.WalletKey = W.WalletKey
		WHERE DayKey = @LastDayKey
		OPTION (RECOMPILE)

		SELECT	DISTINCT DayKey
		Into #DayKey1
		FROM [$(Dimensional)].Dimension.DayZone
		WHERE AnalysisDayText = @PreviousAnalysisDayText
		AND MasterDayKey <> -1
		AND DayKey % 4 <> 3
		OPTION (RECOMPILE)

		SELECT	BalanceTypeKey AS BalanceKey,
				WalletKey, /*UPDATED THIS*/
				DayKey,
				LedgerID,
				TransactedAmount
		INTO #AdditionalAnalysisTime_
		FROM [$(Dimensional)].Fact.[Transaction]
		WHERE DayKey IN
		(
			SELECT DayKey
			FROM  #DayKey1
		)
		AND CreatedBatchKey < @BatchKey --- for interday
		OPTION (RECOMPILE)

		-----------add time that occurred in current batch but not in analysis time---------

		SELECT DayKey
		into #DayKey2
		FROM [$(Dimensional)].Dimension.DayZone
		WHERE MasterDayText = @PreviousAnalysisDayText and AnalysisDayText = @PreviousAnalysisDayText ---don't want anthing that occured only in analysis
		AND DayKey % 4 = 0
		OPTION (RECOMPILE)
	
		INSERT INTO #AdditionalAnalysisTime_
		Select BalanceTypeKey, WalletKey, DayKey, LedgerId, TransactedAmount 
		From #SummedTransactionsAnalysis a
		INNER JOIN [$(Dimensional)].Dimension.DayZone b on a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
		INNER JOIN [$(Dimensional)].Dimension.Wallet W on a.WalletId = W.WalletId
		INNER JOIN [$(Dimensional)].Dimension.BalanceType Bl on a.BalanceTypeID = Bl.BalanceTypeID
		WHERE DayKey IN
		(
			SELECT DayKey
			FROM #DayKey2
		)
		OPTION (RECOMPILE)
		------------------------------------------------------------------------------------

		Select	LedgerID,
				BalanceTypeID,
				WalletId,
				AnalysisDayText,
				SUM(TransactedAmount) as TransactedAmount
		INTO #AdditionalAnalysisSummary_
		FROM #AdditionalAnalysisTime_ a
		INNER JOIN [$(Dimensional)].Dimension.DayZone b on a.DayKey = b.DayKey
		INNER JOIN [$(Dimensional)].Dimension.BalanceType BT ON a.BalanceKey = BT.BalanceTypeKey
		INNER JOIN [$(Dimensional)].Dimension.Wallet W ON a.WalletKey = W.WalletKey
		Group By
			LedgerID,
			BalanceTypeID,
			WalletId,
			AnalysisDayText
		OPTION (RECOMPILE)

		INSERT INTO [Stage].[Balance] WITH(TABLOCK)
		SELECT	a.BatchKey,
				a.BalanceTypeID,
				a.LedgerId,
				a.LedgerSource,
				a.WalletID,
				'N/A' as MasterDayText,
				a.AnalysisDayText,
				IsNull(a.OpeningBalance,0) + IsNull(b.TransactedAmount,0) as OpeningBalance,
				a.TransactedAmount,
				IsNull(a.OpeningBalance,0) + IsNull(b.TransactedAmount,0) as ClosingBalance,
				a.FromDate,
				--DateAdd(MINUTE,-1*Z.OffSetMinutes,a.FromDate) as MasterTimeStamp
				a.FromDate as MasterTimeStamp
		From #LastBalance a 
		LEFT OUTER JOIN #AdditionalAnalysisSummary_ b on a.LedgerId = b.LedgerID and a.BalanceTypeID = b.BalanceTypeID and a.WalletID = b.WalletId
		and a.AnalysisDayText = b.AnalysisDayText
		--INNER JOIN [$(Staging)].Reference.TimeZone Z ON @ToDate>=Z.FromDate AND @ToDate<Z.ToDate AND Z.TimeZone='Analysis'
		OPTION (RECOMPILE)

		DROP TABLE #LastBalance
		DROP TABLE #AdditionalAnalysisTime_
		DROP TABLE #AdditionalAnalysisSummary_

	END

	--------------------------------------------------------------
	---------END ANAYLIS BASE ADD IF DAY BOUNDARY CROSS-----------
	--------------------------------------------------------------

	----------------------------------------------------------
	----------------BEGIN NORMAL ANALYSIS ADD-----------------
	----------------------------------------------------------
	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_BalanceFactCheck','Normal Analysis','Start'

	--------------Get Current Batch already as master in stage------------
	SELECT * 
	INTO #Unpivoted
	FROM Stage.Balance
	WHERE BatchKey = @BatchKey
	AND AnalysisDayText = 'N/A'
	AND MasterDayText NOT IN (ISNULL(@NextAnalysisDayText,'9999-09-09'))
	OPTION (RECOMPILE)

	-------------Less the time difference---------------
	
	SELECT DayKey
	Into #DayKey3
	FROM [$(Dimensional)].Dimension.DayZone
	WHERE AnalysisDayText IN (Select DISTINCT MasterDayText From #Unpivoted)
	and MasterDayKey <> -1
	and DayKey % 4 = 1
	OPTION (RECOMPILE)

	SELECT	BalanceTypeKey AS BalanceKey,
			--AccountKey,
			CASE WHEN WalletKey = 0 THEN 0 ELSE 1 END AS WalletKey,
			DayKey,
			LedgerID,
			TransactedAmount
	INTO #AdditionalAnalysisTime
	FROM [$(Dimensional)].Fact.[Transaction]
	WHERE DayKey IN
	(
		SELECT DayKey
		FROM #DayKey3
	)
	AND CreatedBatchKey < @BatchKey
	OPTION (RECOMPILE)

	SELECT DayKey
	Into #DayKey4
	FROM [$(Dimensional)].Dimension.DayZone
	WHERE AnalysisDayText IN (Select DISTINCT MasterDayText From #Unpivoted)
	AND MasterDayText <> 'N/A'
	AND DayKey % 4 = 1
	OPTION (RECOMPILE)

	INSERT INTO #AdditionalAnalysisTime
	Select BalanceTypeKey, WalletKey, DayKey, LedgerId, TransactedAmount 
	From #SummedTransactionsAnalysis a
	INNER JOIN [$(Dimensional)].Dimension.DayZone b on a.MasterDayText = b.MasterDayText and a.AnalysisDayText = b.AnalysisDayText
	INNER JOIN [$(Dimensional)].Dimension.Wallet W on a.WalletId = W.WalletId
	INNER JOIN [$(Dimensional)].Dimension.BalanceType Bl on a.BalanceTypeID = Bl.BalanceTypeID
	WHERE DayKey IN
	(
		SELECT DayKey
		FROM #DayKey4
	)
	OPTION (RECOMPILE)

	Select	LedgerID,
			BalanceTypeID,
			WalletId,
			AnalysisDayText,
			SUM(TransactedAmount) as TransactedAmount
	INTO #AdditionalAnalysisSummary
	FROM #AdditionalAnalysisTime a
	INNER JOIN [$(Dimensional)].Dimension.DayZone b on a.DayKey = b.DayKey
	INNER JOIN [$(Dimensional)].Dimension.BalanceType BT ON a.BalanceKey = BT.BalanceTypeKey
	INNER JOIN [$(Dimensional)].Dimension.Wallet W ON a.WalletKey = W.WalletKey
	Group By
		LedgerID,
		BalanceTypeID,
		WalletId,
		AnalysisDayText
	OPTION (RECOMPILE)

	--the section below has to return over 15 million records a day and therefore is slow
	--due to a sort function. This index prevent the sort being required, but only takes 6 seconds to build
	Create index temp_Unpivoted on #Unpivoted (LedgerID, BalanceTypeID, WalletID, MasterDayText)
	include (LedgerSource, OpeningBalance, TransactedAmount, FromDate, MasterTimeStamp)


	INSERT INTO [Stage].[Balance]
	Select	a.BatchKey,
			a.BalanceTypeID,
			a.LedgerID,
			a.LedgerSource,
			a.WalletID,
			'N/A' as MasterDayText,
			MasterDayText as AnalysisDayText,
			IsNull(a.OpeningBalance,0) - IsNull(b.TransactedAmount,0) as OpeningBalance,
			a.TransactedAmount,
			IsNull(a.OpeningBalance,0) - IsNull(b.TransactedAmount,0) as ClosingBalance,
			a.FromDate,
			--DateAdd(MINUTE,-1*Z.OffSetMinutes,a.MasterTimeStamp) as MasterTimeStamp
			a.MasterTimeStamp
	From #Unpivoted a 
	LEFT OUTER JOIN #AdditionalAnalysisSummary b on a.LedgerID = b.LedgerID and a.BalanceTypeID = b.BalanceTypeID and a.WalletID = b.WalletId
	and a.MasterDayText = b.AnalysisDayText
	--INNER JOIN [$(Staging)].Reference.TimeZone Z ON a.MasterTimeStamp>=Z.FromDate AND a.MasterTimeStamp<Z.ToDate AND Z.TimeZone='Analysis'
	OPTION (RECOMPILE)


	DROP TABLE #Unpivoted
	DROP TABLE #AdditionalAnalysisTime
	DROP TABLE #AdditionalAnalysisSummary

	--------------------------------------------------------------
	---------------------END ANALYSIS LOAD------------------------
	--------------------------------------------------------------
	EXEC [Control].Sp_Log @BatchKey,'Recursive.Sp_BalanceFactCheck','Transactions Checking','Start'
	-----------------------------------------------------
	----------------------Get Combined-------------------
	-----------------------------------------------------
	EXEC [Control].Sp_Log @BatchKey,'Recursive.Sp_BalanceFactCheck','Transactions Checking 1','Start'

	Select * 
	INTO #CurrentBalance
	From [$(Dimensional)].Fact.Balance
	Where LedgerID IN
	(
	Select Distinct LedgerID From #SummedTransactionsAnalysis
	)
	and DayKey IN
	(
		Select	Distinct
				DayKey 
		From #SummedTransactionsAnalysis c
		INNER JOIN [$(Dimensional)].Dimension.DayZone b on c.AnalysisDayText = b.AnalysisDayText and b.MasterDayText = 'N/A'
		UNION
		Select	Distinct
				DayKey 
		From #SummedTransactionsAnalysis c
		INNER JOIN [$(Dimensional)].Dimension.DayZone b on c.MasterDayText = b.MasterDayText and b.AnalysisDayText = 'N/A'
	)
	OPTION (RECOMPILE)

	Select	BatchKey,
			BalanceTypeID,
			LedgerID,
			LedgerSource,
			WalletKey,
			WalletId,
			MasterDayText,
			AnalysisDayText,
			OpeningBalance,
			SUM(TransactedAmount) as TransactedAmount,
			OpeningBalance + SUM(TransactedAmount) as ClosingBalance,
			FromDate,
			BalanceTypeKey
	INTO #NewTransactions_PreFactTxn
	From
	(
		Select	@BatchKey as BatchKey,
				BT.BalanceTypeID,
				a.LedgerID,
				a.LedgerSource,
				W.WalletKey, /* AJ - Changed.. Added Key */
				W.WalletId,
				b.MasterDayText,
				b.AnalysisDayText as AnalysisDayText,
				a.OpeningBalance,
				IsNull(c.TransactedAmount,0) as TransactedAmount,
				IsNull(a.OpeningBalance,0) + IsNull(c.TransactedAmount,0) as ClosingBalance,
				--c.FromDate
				CASE WHEN b.MasterDayText IN ('UNK','N/A') THEN GetDate() ELSE CONVERT(DateTime, b.MasterDayText) END AS FromDate,
				BT.BalanceTypeKey
	
		From #CurrentBalance a
		INNER HASH JOIN [$(Dimensional)].Dimension.DayZone b on a.DayKey = b.DayKey
		INNER HASH JOIN [$(Dimensional)].Dimension.BalanceType BT ON a.BalanceTypeKey = BT.BalanceTypeKey
		INNER HASH JOIN [$(Dimensional)].Dimension.Wallet W ON a.WalletKey = W.WalletKey
		INNER HASH JOIN #SummedTransactionsAnalysis c on a.LedgerID = c.LedgerID and a.LedgerSource = c.LedgerSource and b.MasterDayText = c.MasterDayText
										and	BT.BalanceTypeID = c.BalanceTypeID 	and W.WalletId = c.WalletId  --and IsNull(b.AnalysisDayText,0) = IsNull(c.AnalysisDayText,0)
		Where b.AnalysisDayText = 'N/A'
	) a
	Group By
		BatchKey,
		BalanceTypeID,
		LedgerID,
		LedgerSource,
		WalletKey,
		WalletId,
		MasterDayText,
		AnalysisDayText,
		OpeningBalance,
		FromDate,
		BalanceTypeKey
	OPTION (RECOMPILE)


	EXEC [Control].Sp_Log @BatchKey,'Recursive.Sp_BalanceFactCheck','Transactions Checking 2','Start'

	Select	@BatchKey as BatchKey,
			BT.BalanceTypeID,
			a.LedgerID,
			a.LedgerSource,
			W.WalletKey,
			W.WalletId,
			c.MasterDayText,
			c.AnalysisDayText as AnalysisDayText,
			a.OpeningBalance,
			IsNull(c.TransactedAmount,0) as TransactedAmount,
			IsNull(a.OpeningBalance,0) + IsNull(c.TransactedAmount,0) as ClosingBalance,
			--c.FromDate
			CASE WHEN b.AnalysisDayText IN ('UNK','N/A') THEN GetDate() ELSE CONVERT(DateTime, b.AnalysisDayText) END AS FromDate,
			BT.BalanceTypeKey
	INTO #NewTransactions_PreFactTxn_Analysis
	FROM #CurrentBalance a
	INNER HASH JOIN [$(Dimensional)].Dimension.DayZone b on a.DayKey = b.DayKey
	INNER HASH JOIN [$(Dimensional)].Dimension.BalanceType BT ON a.BalanceTypeKey = BT.BalanceTypeKey
	INNER HASH JOIN [$(Dimensional)].Dimension.Wallet W ON a.WalletKey = W.WalletKey
	INNER HASH JOIN #SummedTransactionsAnalysis c on a.LedgerID = c.LedgerID and a.LedgerSource = c.LedgerSource and b.AnalysisDayText = c.AnalysisDayText
									and	BT.BalanceTypeID = c.BalanceTypeID 	and W.WalletId = c.WalletId  --and IsNull(b.AnalysisDayText,0) = IsNull(c.AnalysisDayText,0)
	WHERE b.MasterDayText = 'N/A'
	OPTION (RECOMPILE)

	-------------------------------------------------------------------------
	--------------------------See if existing Txns exist---------------------

	EXEC [Control].Sp_Log @BatchKey,'Recursive.Sp_BalanceFactCheck','Transactions Checking 3','Start'

	Select Distinct DayKey Into #DayKeyCheck3 From [$(Dimensional)].Dimension.DayZone
	Where MasterDayText IN
	(
		Select Distinct b2.AnalysisDayText From #NewTransactions_PreFactTxn_Analysis a
		INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.AnalysisDayText = b2.AnalysisDayText --and a.MasterDayText = b2.AnalysisDayText
		and DayKey % 4 <> 3
		UNION
		Select Distinct b2.MasterDayText From #NewTransactions_PreFactTxn a
		INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.MasterDayText = b2.MasterDayText and a.MasterDayText = b2.AnalysisDayText
	)
	UNION
	Select Distinct DayKey From [$(Dimensional)].Dimension.DayZone
	Where AnalysisDayText IN
	(
		Select Distinct b2.AnalysisDayText From #NewTransactions_PreFactTxn_Analysis a
		INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.AnalysisDayText = b2.AnalysisDayText --and a.MasterDayText = b2.AnalysisDayText
		and DayKey % 4 <> 3
		UNION
		Select Distinct b2.MasterDayText From #NewTransactions_PreFactTxn a
		INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.MasterDayText = b2.MasterDayText and a.MasterDayText = b2.AnalysisDayText
	)

	Select	LedgerID,
			DayKey,
			BalanceTypeKey AS BalanceKey,
			WalletKey, /* UPDATED THIS */
			(SELECT WalletID FROM [$(Dimensional)].[Dimension].[Wallet] WHERE T.WalletKey = WalletKey) as WalletID,
			IsNull(T.TransactedAmount,0) as TransactedAmount
	INTO #FactTxn_Analysis
	FROM [$(Dimensional)].Fact.[Transaction] T
	WHERE DayKey IN
	(
		Select DayKey From #DayKeyCheck3
		--Select Distinct DayKey From [$(Dimensional)].Dimension.DayZone
		--Where MasterDayText IN
		--(
		--	Select Distinct b2.AnalysisDayText From #NewTransactions_PreFactTxn_Analysis a
		--	INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.AnalysisDayText = b2.AnalysisDayText --and a.MasterDayText = b2.AnalysisDayText
		--	and DayKey % 4 <> 3
		--	UNION
		--	Select Distinct b2.MasterDayText From #NewTransactions_PreFactTxn a
		--	INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.MasterDayText = b2.MasterDayText and a.MasterDayText = b2.AnalysisDayText
		--)
		--OR
		--AnalysisDayText IN
		--(
		--	Select Distinct b2.AnalysisDayText From #NewTransactions_PreFactTxn_Analysis a
		--	INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.AnalysisDayText = b2.AnalysisDayText --and a.MasterDayText = b2.AnalysisDayText
		--	and DayKey % 4 <> 3
		--	UNION
		--	Select Distinct b2.MasterDayText From #NewTransactions_PreFactTxn a
		--	INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.MasterDayText = b2.MasterDayText and a.MasterDayText = b2.AnalysisDayText
		--)
	)
	and T.CreatedBatchKey < @BatchKey
	OPTION (RECOMPILE)

	-------------SUM THE EXISTING TXN's-------------

	EXEC [Control].Sp_Log @BatchKey,'Recursive.Sp_BalanceFactCheck','Transactions Checking 4','Start'
	Select	LedgerID,
			DayKey,
			BalanceKey,
			WalletKey,
			WalletID,
			SUM(TransactedAmount) as TransactedAmount
	INTO #FactTxnSUM_Analysis
	From #FactTxn_Analysis
	Group By 
		LedgerID,
		DayKey,
		BalanceKey,
		WalletKey,
		WalletID
	OPTION (RECOMPILE)
		
	EXEC [Control].Sp_Log @BatchKey,'Recursive.Sp_BalanceFactCheck','Transactions Checking 5','Start'

	Select	BatchKey,
			BalanceTypeID,
			a.LedgerID,
			a.LedgerSource,
			a.WalletId,
			a.MasterDayText,
			a.AnalysisDayText as AnalysisDayText,
			a.OpeningBalance,
			a.TransactedAmount + IsNull(T.TransactedAmount,0) as TransactedAmount,
			a.OpeningBalance + a.TransactedAmount + IsNull(T.TransactedAmount,0) as ClosingBalance,
			a.FromDate,
			a.FromDate as MasterTimeStamp
	INTO #NewTransactions
	From #NewTransactions_PreFactTxn a
	LEFT OUTER JOIN 
	(
		Select	a.LedgerID,
				a.BalanceKey,
				a.WalletKey,
				b2.MasterDayText,
				SUM(TransactedAmount) as TransactedAmount
		From #FactTxnSUM_Analysis a
		INNER JOIN [$(Dimensional)].Dimension.DayZone b2 on a.DayKey = b2.DayKey
		Group By	a.LedgerID,
					a.BalanceKey,
					a.WalletKey,
					b2.MasterDayText
	) T on	a.LedgerID = T.LedgerID and a.MasterDayText = T.MasterDayText and a.BalanceTypeKey = T.BalanceKey
												and a.WalletKey = T.WalletKey
	OPTION (RECOMPILE)
	----here----
	

	Select	BatchKey,
			BalanceTypeID,
			a.LedgerID,
			a.LedgerSource,
			a.WalletId,
			'N/A' as MasterDayText,
			a.AnalysisDayText as AnalysisDayText,
			a.OpeningBalance,
			a.TransactedAmount + IsNull(T.TransactedAmount,0) as TransactedAmount,
			a.OpeningBalance + a.TransactedAmount + IsNull(T.TransactedAmount,0) as ClosingBalance,
			a.FromDate,
			a.FromDate as MasterTimeStamp
	INTO #NewTransactionsAnalysis
	From 
	(
	
	Select	BatchKey, BalanceTypeID, LedgerID, LedgerSource, WalletKey, WalletID, 'N/A' as MasterDayText, AnalysisDayText, OpeningBalance,
			SUM(TransactedAmount) as TransactedAmount,
			OpeningBalance + SUM(TransactedAmount) as ClosingBalance,
			FromDate, BalanceTypeKey
	From #NewTransactions_PreFactTxn_Analysis
	Group By BatchKey, BalanceTypeID, LedgerID, LedgerSource, WalletKey, WalletId, AnalysisDayText, OpeningBalance,FromDate, BalanceTypeKey
	) a
	LEFT OUTER JOIN 
	(
	Select	a.LedgerID,
			a.BalanceKey,
			a.WalletKey,
			b2.AnalysisDayText,
			SUM(TransactedAmount) as TransactedAmount
	From #FactTxnSUM_Analysis a
	INNER JOIN [$(Dimensional)].Dimension.DayZone b2  on a.DayKey = b2.DayKey
	Group By a.LedgerID,
			a.BalanceKey,
			a.WalletKey,
			b2.AnalysisDayText
) T on a.LedgerID = T.LedgerID and a.AnalysisDayText = T.AnalysisDayText and a.BalanceTypeKey = T.BalanceKey
												and a.WalletKey = T.WalletKey
	OPTION (RECOMPILE)



	---------------------------------------------------------------------------------------------------------------
	--------------------------------------------UPDATE NEW ITEMS---------------------------------------------------
	---------------------------------------------------------------------------------------------------------------

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_BalanceFactCheck','Update Existing','Start'

	Update b
	Set b.TransactedAmount = IsNull(b.TransactedAmount,0) + IsNull(a.TransactedAmount,0),
		b.ClosingBalance = IsNull(b.ClosingBalance,0) + IsNull(a.TransactedAmount,0)
	from 
	(
	Select	BatchKey,
			BalanceTypeID,
			LedgerID,
			LedgerSource,
			WalletId,
			MasterDayText,
			'N/A' as AnalysisDayText,
			SUM(TransactedAmount) as  TransactedAmount
	From #SummedTransactionsAnalysis
	Group By
		BatchKey,
		BalanceTypeID,
		LedgerID,
		LedgerSource,
		WalletId,
		MasterDayText
	) a
INNER JOIN [Stage].[Balance] b on a.BalanceTypeID = b.BalanceTypeID
									and a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and a.WalletId = b.WalletId 
									and a.MasterDayText = b.MasterDayText and b.AnalysisDayText = 'N/A'
	Where b.BatchKey = (Select MAX(BatchKey) From #SummedTransactionsAnalysis)
	OPTION (RECOMPILE)


	Update b
	Set b.TransactedAmount = IsNull(b.TransactedAmount,0) + IsNull(a.TransactedAmount,0),
		b.ClosingBalance = IsNull(b.ClosingBalance,0) + IsNull(a.TransactedAmount,0)
	from 
	(
	Select	BatchKey,
			BalanceTypeID,
			LedgerID,
			LedgerSource,
			WalletId,
			'N/A' as MasterDayText,
			AnalysisDayText,
			SUM(TransactedAmount) as  TransactedAmount
	From #SummedTransactionsAnalysis
	Group By
		BatchKey,
		BalanceTypeID,
		LedgerID,
		LedgerSource,
		WalletId,
		AnalysisDayText
	) a
	INNER JOIN [Stage].[Balance] b on a.BalanceTypeID = b.BalanceTypeID
									and a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and a.WalletId = b.WalletId 
									--and a.MasterDayText = b.MasterDayText 
									and a.AnalysisDayText = b.AnalysisDayText and b.MasterDayText = 'N/A'
	Where b.BatchKey = (Select MAX(BatchKey) From #SummedTransactionsAnalysis)
	OPTION (RECOMPILE)


	---------------------------------------------------------------------------------------------------------------
	--------------------------------------------INSERT NEW ITEMS---------------------------------------------------
	---------------------------------------------------------------------------------------------------------------

	EXEC [Control].Sp_Log	@BatchKey,'Recursive.Sp_BalanceFactCheck','Final Loading New Items','Start'

	/*  AJ - 01/09 - "Id rather ask for forgiveness then ask for permission"
		Get list of ids for a given batch
		load into temp table
		drive query from this temp table
	*/

	SELECT DISTINCT
		BatchKey,
		LedgerID,
		LedgerSource,
		BalanceTypeID,
		WalletId,
		MasterDayText,
		AnalysisDayText
	INTO #Candidate
	FROM Stage.Balance
	WHERE BatchKey = @BatchKey
	OPTION (RECOMPILE);

	INSERT INTO [Stage].[Balance]
	Select 
			a.BatchKey,
			a.BalanceTypeID,
			a.LedgerID,
			a.LedgerSource,
			a.WalletId,
			a.MasterDayText,
			a.AnalysisDayText,
			a.OpeningBalance,
			a.TransactedAmount,
			a.ClosingBalance,
			a.FromDate,
			a.MasterTimeStamp
	From #NewTransactions a
	LEFT OUTER JOIN #Candidate b on	a.BatchKey = b.BatchKey and a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and	a.BalanceTypeID = b.BalanceTypeID and a.WalletId = b.WalletId and a.MasterDayText = b.MasterDayText
	Where b.LedgerID is null
	OPTION (RECOMPILE);


	INSERT INTO [Stage].[Balance]
	Select 	a.BatchKey,
			a.BalanceTypeID,
			a.LedgerID,
			a.LedgerSource,
			a.WalletId,
			a.MasterDayText,
			a.AnalysisDayText,
			a.OpeningBalance,
			a.TransactedAmount,
			a.ClosingBalance,
			a.FromDate,
			a.MasterTimeStamp
	From #NewTransactionsAnalysis a
	LEFT OUTER JOIN #Candidate b on	a.BatchKey = b.BatchKey and a.LedgerID = b.LedgerID and a.LedgerSource = b.LedgerSource and	a.BalanceTypeID = b.BalanceTypeID and a.WalletId = b.WalletId and a.AnalysisDayText = b.AnalysisDayText
	Where b.LedgerID IS NULL
	OPTION (RECOMPILE);

	EXEC [Control].Sp_Log @BatchKey,'Recursive.Sp_BalanceFactCheck',NULL,'Success'

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Recursive.Sp_BalanceFactCheck', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
