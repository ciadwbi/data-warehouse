﻿CREATE PROCEDURE [Utility].[sp_ColdStart]
	 @FromDate DATETIME2(3) = null
	,@ToDate DATETIME2(3) = null
	,@BatchKey int  = null
	,@Purge BIT = 0
AS

If @FromDate is null
Set @FromDate = '2013-12-24 23:00:00.000'

If @ToDate is null
Set @ToDate = '2014-01-01 01:00:00.000'

If @BatchKey is null
Set @BatchKey = 1

DECLARE @InitialBatchKey INT = 0
Declare @InitialFromDate DATETIME2(3) = '1900-01-01 00:00:00.000'

IF @Purge = 1
BEGIN
	EXEC [Utility].[Sp_PurgeData] @AreYouSure = 'Y', @IncludeControl = 'Y'
	EXEC Dimension.[Sp_PurgeData] @AreYouSure = 'Y', @Schema = 'Fact,Dimension'
END

exec Dimension.[Sp_DimDay] @InitialBatchKey
exec Dimension.[Sp_DimDayZone] @InitialBatchKey

SET IDENTITY_INSERT [Control].[ETLController] ON
INSERT INTO [Control].[ETLController] (BatchKey, BatchFromDate, BatchToDate, BatchStartDate)
SELECT @InitialBatchKey, @InitialFromDate, @FromDate, GETDATE();
SET IDENTITY_INSERT [Control].[ETLController] OFF;

Insert into [$(Dimensional)].[Control].CompleteDate (TransactionCompleteDate) Values (NULL)

UPDATE [Control].[ETLController]
SET	   AtomicStatus = 1,
       StagingStatus = 1,
       RecursiveStatus = 1,
	   DimensionRecursiveStatus = 1,
       DimensionStatus = 1,
       TransactionStatus = 1,
	   BalanceFactStatus = 1,
       KPIFactStatus = 1, 
	   KPIRecursiveStatus = 1,
       PositionStagingStatus = 1,
       PositionFactStatus = 1,
	   ContactEmailStagingStatus = 1,
	   ContactEmailFactStatus = 1,
	   ContactSmsStagingStatus = 1,
	   ContactSmsFactStatus = 1,
	   ContactPushStagingStatus = 1,
	   ContactPushFactStatus = 1,
	   ContactDimensionRecursiveStatus = 1
WHERE BatchKey = 0;

BEGIN
	   PRINT '[Centrebet].[Sp_Balance]'
       EXEC [Centrebet].[Sp_Balances] @InitialBatchKey;
END

BEGIN
	   --PRINT '[Intrabet].[Sp_Competitor]'
       --EXEC [Intrabet].[Sp_Competitor] @InitialBatchKey, @InitialFromDate, @FromDate;
       PRINT '[Intrabet].[Sp_Promotion]'
       EXEC [Intrabet].[Sp_Promotion] @InitialBatchKey, @InitialFromDate, @FromDate;
       PRINT '[Intrabet].[Sp_Account]'
       EXEC Control.Invoker '[Intrabet].[Sp_Account]', @InitialBatchKey
	   PRINT '[Intrabet].[Sp_AccountStatus]'
       EXEC [Intrabet].[Sp_AccountStatus] @InitialBatchKey, @InitialFromDate, @FromDate;
       PRINT '[Intrabet].[Sp_Event]'
       EXEC Control.Invoker '[Intrabet].[Sp_Event]', @InitialBatchKey
	   PRINT '[Intrabet].[Sp_Market]'
       EXEC [Intrabet].[Sp_Market] @InitialFromDate, @FromDate, @InitialBatchKey;
	   --PRINT '[Intrabet].[Sp_Instrument]'
	   --EXEC  [Intrabet].[Sp_Instrument] @InitialFromDate, @FromDate, @InitialBatchKey;
	   PRINT '[Intrabet].[Sp_Instrument_EFT]'
	   EXEC Control.Invoker '[Intrabet].[Sp_Instrument_EFT]', @InitialBatchKey
	   PRINT '[Intrabet].[Sp_Instrument_BPay]'
	   EXEC Control.Invoker '[Intrabet].[Sp_Instrument_BPay]', @InitialBatchKey
	   PRINT '[Intrabet].[Sp_Instrument_MoneyBookers]'
	   EXEC Control.Invoker '[Intrabet].[Sp_Instrument_MoneyBookers]', @InitialBatchKey
	   PRINT '[Intrabet].[Sp_Instrument_Paypal]'
	   EXEC Control.Invoker '[Intrabet].[Sp_Instrument_Paypal]', @InitialBatchKey

	   PRINT '[Intrabet].[Sp_PriceType]'
       EXEC [Intrabet].[Sp_PriceType] @InitialFromDate, @FromDate, @InitialBatchKey;
	   PRINT '[Intrabet].[Sp_Class]'
	   EXEC [Intrabet].[Sp_Class] @InitialFromDate, @FromDate, @InitialBatchKey;
	   PRINT '[Intrabet].[Sp_Structure]'
	   EXEC [Intrabet].[Sp_Structure] @InitialBatchKey, @InitialFromDate, @FromDate;
	   PRINT '[Intrabet].[Sp_User]'
	   EXEC [Intrabet].[Sp_User] @InitialFromDate, @FromDate, @InitialBatchKey;
	   PRINT '[ExactTarget].[Sp_Communication_Email]'
       EXEC [ExactTarget].[Sp_Communication_Email] @InitialFromDate, @FromDate, @InitialBatchKey;
	   PRINT '[ExactTarget].[Sp_Communication_Sms]'
       EXEC [ExactTarget].[Sp_Communication_Sms] @InitialFromDate, @FromDate, @InitialBatchKey;
	   PRINT '[ExactTarget].[Sp_Communication_Push]'
       EXEC [ExactTarget].[Sp_Communication_Push] @InitialFromDate, @FromDate, @InitialBatchKey;
END

BEGIN
	   --PRINT '[Dimension].[Sp_DimCompetitor]'
       --EXEC [Dimension].[Sp_DimCompetitor] @InitialBatchKey;
	   PRINT '[Dimension].[Sp_DimPromotion]'
       EXEC [Dimension].[Sp_DimPromotion] @InitialBatchKey;
       PRINT '[Dimension].[Sp_DimAccount]'
       EXEC [Dimension].[Sp_DimAccount] @InitialBatchKey;
	   PRINT '[Dimension].[Sp_DimAccountStatus]'
       EXEC [Dimension].[Sp_DimAccountStatus] @InitialBatchKey;
       PRINT '[Dimension].[Sp_DimEvent]'
       EXEC [Dimension].[Sp_DimEvent] @InitialBatchKey;
	   PRINT '[Dimension].[Sp_DimMarket]'
       EXEC [Dimension].[Sp_DimMarket] @InitialBatchKey;
	   PRINT '[Dimension].[Sp_DimIntrument]'
       EXEC [Dimension].[Sp_DimInstrument] @InitialBatchKey;
	   PRINT '[Dimension].[Sp_DimPriceType]'
       EXEC [Dimension].[Sp_DimPriceType] @InitialBatchKey;
	   PRINT '[Dimension].[Sp_DimClass]'
       EXEC [Dimension].[Sp_DimClass] @InitialBatchKey;
	   PRINT '[Dimension].[Sp_DimStructure]'
       EXEC [Dimension].[Sp_DimStructure] @InitialBatchKey;
	   PRINT '[Dimension].[Sp_DimUser]'
       EXEC [Dimension].[Sp_DimUser] @InitialBatchKey;
	   PRINT '[Dimension].[Sp_DimCommunication_Email]'
       EXEC [Dimension].[Sp_DimCommunication_Email] @InitialBatchKey;
	   PRINT '[Dimension].[Sp_DimCommunication_Sms]'
       EXEC [Dimension].[Sp_DimCommunication_Sms] @InitialBatchKey;
	   PRINT '[Dimension].[Sp_DimCommunication_Push]'
       EXEC [Dimension].[Sp_DimCommunication_Push] @InitialBatchKey;
END

--Action the Cleanse scripts on the backfilled data
Begin
	Exec [Cleanse].[Sp_Cleanse_ALL] @InitialBatchKey;
End

Exec [$(Acquisition)].dbo.sp_updatestats

Update [Control].[ETLController] set BatchStatus = 1, BatchEndDate = getdate() Where BatchKey = 0

Go