﻿CREATE FUNCTION [Utility].[SplitString](@input AS Varchar(4000) )
RETURNS
      @Result TABLE(Value VARCHAR(4000))
AS
BEGIN
      DECLARE @str VARCHAR(4000)
      DECLARE @ind Int
      IF(@input is not null)
      BEGIN
            SET @ind = CharIndex(',',@input)
            WHILE @ind > 0
            BEGIN
                  SET @str = SUBSTRING(@input,1,@ind-1)
                  SET @input = SUBSTRING(@input,@ind+1,LEN(@input)-@ind)
                  INSERT INTO @Result values (RTRIM(LTRIM(@str)))
                  SET @ind = CharIndex(',',@input)
            END
            SET @str = @input
            INSERT INTO @Result values (RTRIM(LTRIM(@str)))
      END
      RETURN
END