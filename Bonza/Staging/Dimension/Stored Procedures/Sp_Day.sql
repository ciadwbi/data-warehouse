﻿CREATE PROC [Dimension].[sp_Day]
(
	@BatchKey	int,
	@FromDate	datetime2(3),
	@ToDate		datetime2(3),
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount INT;

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
	
BEGIN TRY

	IF @FromDate < '2013-12-19' SET @FromDate = DATEADD(MILLISECOND, -1, CONVERT(datetime2(3),'2013-12-19')) -- Temporary fix to prevent day records beign created before the start of CDC until an historic load is performed

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Stage','Start', @Reload

	;WITH DaySequence AS
		-- create a sequence of all possible days in the specified from/to range with sufficient excess beyond the range boundaries to ensure 
		-- additional days are available for any previous and next values to be derived for each required record within the from/to range
		(	SELECT	CONVERT(datetime2(3),CONVERT(date,@FromDate)) DayDate
			UNION ALL
			SELECT	DATEADD(DAY,1,DayDate) FROM	DaySequence WHERE DayDate <= DATEADD(DAY,1,CONVERT(date, @ToDate))
		)
	SELECT	FromDate, ToDate, DayToDate,
			REPLACE (CONVERT (char(10),MasterDayDate,102), '.', '-') MasterDayText,
			REPLACE (CONVERT (char(10),AnalysisDayDate,102), '.', '-') AnalysisDayText,
			MasterDayDate,
			AnalysisDayDate,
			TimeZone
	INTO	#Stage
	FROM	(	SELECT	x.FromDate, 
						LEAD(x.FromDate) OVER (ORDER BY x.FromDate) ToDate,
						LEAD(x.FromDate) OVER (PARTITION BY x.TimeZone ORDER BY x.FromDate) DayToDate,
						CONVERT(date,DATEADD(MINUTE,m.OffsetMinutes,x.FromDate)) MasterDayDate,
						CONVERT(date,DATEADD(MINUTE,a.OffsetMinutes,x.FromDate)) AnalysisDayDate,
						x.TimeZone
				FROM	(	SELECT	DATEADD(MINUTE, -OffsetMinutes, DayDate) FromDate, TimeZone
							FROM	DaySequence d
									INNER JOIN [Reference].[TimeZone] z ON z.FromDate <= d.DayDate AND z.ToDate > d.DayDate
						) x
						INNER JOIN [Reference].[TimeZone] m ON m.FromDate <= x.FromDate AND m.ToDate > x.FromDate AND m.TimeZone = 'Master'
						INNER JOIN [Reference].[TimeZone] a ON a.FromDate <= x.FromDate AND a.ToDate > x.FromDate AND a.TimeZone = 'Analysis'
			) y
	WHERE	FromDate > @FromDate
			AND FromDate <= @ToDate 
	OPTION (RECOMPILE, MAXRECURSION 0)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	SELECT	d.DayKey,
			m.DayKey MasterDayKey, a.DayKey AnalysisDayKey,
			CASE s.TimeZone WHEN 'Master' THEN m.DayKey ELSE 0 END OpeningMasterDayKey, CASE s.TimeZone WHEN 'Analysis' THEN a.DayKey ELSE 0 END OpeningAnalysisDayKey,
			CASE s.TimeZone WHEN 'Master' THEN cm.DayKey ELSE 0 END ClosingMasterDayKey, CASE s.TimeZone WHEN 'Analysis' THEN ca.DayKey ELSE 0 END ClosingAnalysisDayKey,
			s.MasterDayText, s.AnalysisDayText,
			s.MasterDayDate, s.AnalysisDayDate,
			s.FromDate, s.ToDate, s.DayToDate
	INTO	#Day
	FROM	#Stage s
			INNER JOIN [$(Dimensional)].Dimension.[Day] m ON m.DayText = s.MasterDayText 
			LEFT JOIN [$(Dimensional)].Dimension.[Day] cm ON cm.DayDate = m.YesterdayDate 
			INNER JOIN [$(Dimensional)].Dimension.[Day] a ON a.DayText = s.AnalysisDayText
			LEFT JOIN [$(Dimensional)].Dimension.[Day] ca ON ca.DayDate = a.YesterdayDate 
			LEFT JOIN [$(Dimensional)].CC.[Day] d ON d.FromDate = s.FromDate
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	UPDATE	d
	SET		MasterDayKey = u.MasterDayKey,
			AnalysisDayKey = u.AnalysisDayKey,
			OpeningMasterDayKey = u.OpeningMasterDayKey,
			OpeningAnalysisDayKey = u.OpeningAnalysisDayKey,
			ClosingMasterDayKey = u.ClosingMasterDayKey,
			ClosingAnalysisDayKey = u.ClosingAnalysisDayKey,
			MasterDayText = u.MasterDayText,
			AnalysisDayText = u.AnalysisDayText,
			MasterDayDate = u.MasterDayDate,
			AnalysisDayDate = u.AnalysisDayDate,
			FromDate = u.FromDate, 
			ToDate = u.ToDate, 
			DayToDate = u.DayToDate,
			CreatedDate = SYSDATETIME() , 
			CreatedBatchKey = @BatchKey, 
			CreatedBy = @Me,
			ModifiedDate = SYSDATETIME(), 
			ModifiedBatchKey = @BatchKey, 
			ModifiedBy = @Me
	FROM	[$(Dimensional)].CC.[Day] d
			INNER JOIN #Day u on u.DayKey = d.DayKey
	WHERE	u.DayKey IS NOT NULL
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @Reload, @RowsProcessed = @RowCount

	INSERT [$(Dimensional)].CC.[Day] 
	(	[MasterDayKey], [AnalysisDayKey], 
		[OpeningMasterDayKey], [OpeningAnalysisDayKey], 
		[ClosingMasterDayKey], [ClosingAnalysisDayKey], 
		[MasterDayText], [AnalysisDayText], 
		[MasterDayDate], [AnalysisDayDate], 
		[FromDate], [ToDate], [DayToDate],
		[CreatedDate], [CreatedBatchKey], [CreatedBy],
		[ModifiedDate], [ModifiedBatchKey], [ModifiedBy])
	SELECT	MasterDayKey,
			AnalysisDayKey,
			OpeningMasterDayKey,
			OpeningAnalysisDayKey,
			ClosingMasterDayKey,
			ClosingAnalysisDayKey,
			MasterDayText,
			AnalysisDayText,
			MasterDayDate,
			AnalysisDayDate,
			FromDate, ToDate, DayToDate,
			SYSDATETIME() CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy,
			SYSDATETIME() ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy
	FROM	#Day
	WHERE	DayKey IS NULL
	ORDER BY FromDate 
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
