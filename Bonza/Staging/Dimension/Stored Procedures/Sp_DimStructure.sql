﻿CREATE PROC [Dimension].[Sp_DimStructure]
(
	@BatchKey	int,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @RowCount int

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimStructure','PreJoin Dimensions','Start'

	SELECT	ISNULL(d.StructureKey, CASE WHEN s.Inplay = 'Y' THEN - a.AccountKey ELSE a.AccountKey END) StructureKey, -- temporary measure to create structure dimension against computed key in the fact table to avoid reload
			s.LedgerId, s.LedgerSource, s.InPlay, s.AccountId, 
			s.OrgId, s.OrgIdFromDate, s.PreviousOrgId, s.FY2014OrgId, s.FY2015OrgId, s.FY2016OrgId, s.FY2017OrgId,
			s.IntroducerId, s.Introducer, s.IntroducerFromDate, s.PreviousIntroducerId, s.PreviousIntroducer, s.FY2014IntroducerId, s.FY2014Introducer, s.FY2015IntroducerId, s.FY2015Introducer, s.FY2016IntroducerId, s.FY2016Introducer, s.FY2017IntroducerId, s.FY2017Introducer,
			s.ManagerId, s.Manager, s.CanManageVIP, s.CanManageBDM, s.ManagerFromDate, s.PreviousManagerId, s.PreviousManager, s.PreviousCanManageVIP, s.PreviousCanManageBDM, s.FY2014ManagerId, s.FY2014Manager, s.FY2014CanManageVIP, s.FY2014CanManageBDM, s.FY2015ManagerId, s.FY2015Manager, 
			s.FY2015CanManageVIP, s.FY2015CanManageBDM, s.FY2016ManagerId, s.FY2016Manager, s.FY2016CanManageVIP, s.FY2016CanManageBDM,  s.FY2017ManagerId, s.FY2017Manager, s.FY2017CanManageVIP, s.FY2017CanManageBDM, 
			s.HandlerId, s.Handler, s.HandlerFromDate, s.PreviousHandlerId, s.PreviousHandler, s.FY2014HandlerId, s.FY2014Handler, s.FY2015HandlerId, s.FY2015Handler, s.FY2016HandlerId, s.FY2016Handler, s.FY2017HandlerId, s.FY2017Handler,
			s.VIPType, s.VIPTypeFromDate, s.PreviousVIPType, s.FY2014VIPType, s.FY2015VIPType, s.FY2016VIPType,  s.FY2017VIPType, s.CanManagePotentialVIP,
			s.FromDate, s.BatchKey,
			CASE WHEN d.StructureKey IS NULL THEN 'I' ELSE 'U' END Upsert  
	INTO	#Structure
	FROM	Stage.Structure s
			INNER JOIN [$(Dimensional)].Dimension.Account a ON (a.LedgerId = s.LedgerId AND a.LedgerSource = s.LedgerSource)  -- NOTE - whilst the Structure dimension is implemented as a fix, the Account Dimension must already be loaded to donate the AccountKey for new records - hence the INNER JOIN
			LEFT JOIN [$(Dimensional)].Dimension.Structure d ON (d.LedgerId = s.LedgerId AND d.LedgerSource = s.LedgerSource AND d.InPlay = s.Inplay)
	WHERE	s.BatchKey = @BatchKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log @BatchKey,'Sp_DimStructure','Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE	d
	SET		d.OrgId = s.OrgId,
			d.OrgIdFromDate = s.OrgIdFromDate,
			d.PreviousOrgId = s.PreviousOrgId,
			d.FY2014OrgId = s.FY2014OrgId,
			d.FY2015OrgId = s.FY2015OrgId,
			d.FY2016OrgId = s.FY2016OrgId,
			d.FY2017OrgId = s.FY2017OrgId,
			d.IntroducerId = s.IntroducerId,
			d.Introducer = s.Introducer, 
			d.IntroducerFromDate = s.IntroducerFromDate,
			d.PreviousIntroducerId = s.PreviousIntroducerId,
			d.PreviousIntroducer = s.PreviousIntroducer,
			d.FY2014IntroducerId = s.FY2014IntroducerId,
			d.FY2014Introducer = s.FY2014Introducer,
			d.FY2015IntroducerId = s.FY2015IntroducerId,
			d.FY2015Introducer = s.FY2015Introducer,
			d.FY2016IntroducerId = s.FY2016IntroducerId,
			d.FY2016Introducer = s.FY2016Introducer,
			d.FY2017IntroducerId = s.FY2017IntroducerId,
			d.FY2017Introducer = s.FY2017Introducer,
			d.ManagerId = s.ManagerId,
			d.Manager = s.Manager, 
			d.CanManageVIP = s.CanManageVIP, 
			d.CanManageBDM = s.CanManageBDM, 
			d.ManagerFromDate = s.ManagerFromDate,
			d.PreviousManagerId = s.PreviousManagerId,
			d.PreviousManager = s.PreviousManager,
			d.PreviousCanManageVIP = s.PreviousCanManageVIP, 
			d.PreviousCanManageBDM = s.PreviousCanManageBDM, 
			d.FY2014ManagerId = s.FY2014ManagerId,
			d.FY2014Manager = s.FY2014Manager,
			d.FY2014CanManageVIP = s.FY2014CanManageVIP, 
			d.FY2014CanManageBDM = s.FY2014CanManageBDM, 
			d.FY2015ManagerId = s.FY2015ManagerId,
			d.FY2015Manager = s.FY2015Manager,
			d.FY2015CanManageVIP = s.FY2015CanManageVIP, 
			d.FY2015CanManageBDM = s.FY2015CanManageBDM, 
			d.FY2016ManagerId = s.FY2016ManagerId,
			d.FY2016Manager = s.FY2016Manager,
			d.FY2016CanManageVIP = s.FY2016CanManageVIP, 
			d.FY2016CanManageBDM = s.FY2016CanManageBDM, 
			d.FY2017ManagerId = s.FY2017ManagerId,
			d.FY2017Manager = s.FY2017Manager,
			d.FY2017CanManageVIP = s.FY2017CanManageVIP, 
			d.FY2017CanManageBDM = s.FY2017CanManageBDM, 
			d.HandlerId = s.HandlerId,
			d.Handler = s.Handler, 
			d.HandlerFromDate = s.HandlerFromDate,
			d.PreviousHandlerId = s.PreviousHandlerId,
			d.PreviousHandler = s.PreviousHandler,
			d.FY2014HandlerId = s.FY2014HandlerId,
			d.FY2014Handler = s.FY2014Handler,
			d.FY2015HandlerId = s.FY2015HandlerId,
			d.FY2015Handler = s.FY2015Handler,
			d.FY2016HandlerId = s.FY2016HandlerId,
			d.FY2016Handler = s.FY2016Handler,
			d.FY2017HandlerId = s.FY2017HandlerId,
			d.FY2017Handler = s.FY2017Handler,
			d.VIPType = s.VIPType,
			d.VIPTypeFromDate = s.VIPTypeFromDate,
			d.PreviousVIPType = s.PreviousVIPType,
			d.FY2014VIPType = s.FY2014VIPType,
			d.FY2015VIPType = s.FY2015VIPType,
			d.FY2016VIPType = s.FY2016VIPType,
			d.FY2017VIPType = s.FY2017VIPType,
			d.CanManagePotentialVIP = s.CanManagePotentialVIP,
			d.Level0 = 'Unknown',
			d.Level1 = 'Unknown',
			d.Level2 = 'Unknown',
			d.BDM = 'Unknown',
			d.VIPCode = -1,
			d.VIP = 'Unknown',
			d.PotentialVIP = 'Unknown',
			d.CompanyKey = -1,
			d.PreviousLevel0 = 'Unknown',
			d.PreviousLevel1 = 'Unknown',
			d.PreviousLevel2 = 'Unknown',
			d.PreviousBDM = 'Unknown',
			d.PreviousVIPCode = -1,
			d.PreviousVIP = 'Unknown',
			d.PreviousCompanyKey = -1,
			d.FY2014Level0 = 'Unknown',
			d.FY2014Level1 = 'Unknown',
			d.FY2014Level2 = 'Unknown',
			d.FY2014BDM = 'Unknown',
			d.FY2014VIPCode = -1,
			d.FY2014VIP = 'Unknown',
			d.FY2014CompanyKey = -1,
			d.FY2015Level0 = 'Unknown',
			d.FY2015Level1 = 'Unknown',
			d.FY2015Level2 = 'Unknown',
			d.FY2015BDM = 'Unknown',
			d.FY2015VIPCode = -1,
			d.FY2015VIP = 'Unknown',
			d.FY2015CompanyKey = -1,
			d.FY2016Level0 = 'Unknown',
			d.FY2016Level1 = 'Unknown',
			d.FY2016Level2 = 'Unknown',
			d.FY2016BDM = 'Unknown',
			d.FY2016VIPCode = -1,
			d.FY2016VIP = 'Unknown',
			d.FY2016CompanyKey = -1,
			d.FY2017Level0 = 'Unknown',
			d.FY2017Level1 = 'Unknown',
			d.FY2017Level2 = 'Unknown',
			d.FY2017BDM = 'Unknown',
			d.FY2017VIPCode = -1,
			d.FY2017VIP = 'Unknown',
			d.FY2017CompanyKey = -1,
			d.ModifiedDate = GetDate(),
			d.ModifiedBatchKey = s.BatchKey,
			d.ModifiedBy = 'Sp_DimStructure'
	FROM	#Structure s
			INNER JOIN [$(Dimensional)].[Dimension].[Structure] d on d.StructureKey = s.StructureKey
	WHERE	s.Upsert = 'U'
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimStructure','Insert','Start', @RowsProcessed = @RowCount

	INSERT	[$(Dimensional)].[Dimension].[Structure]
			(	StructureKey, LedgerId, LedgerSource, InPlay, AccountId, 
				OrgId, OrgIdFromDate, PreviousOrgId, FY2014OrgId, FY2015OrgId, FY2016OrgId, FY2017OrgId,
				IntroducerId, Introducer, IntroducerFromDate, PreviousIntroducerId, PreviousIntroducer, FY2014IntroducerId, FY2014Introducer, FY2015IntroducerId, 
				FY2015Introducer, FY2016IntroducerId, FY2016Introducer, FY2017IntroducerId, FY2017Introducer,	ManagerId, Manager, CanManageVIP, CanManageBDM, ManagerFromDate, PreviousManagerId, PreviousManager, 
				PreviousCanManageVIP, PreviousCanManageBDM, FY2014ManagerId, FY2014Manager, FY2014CanManageVIP, FY2014CanManageBDM, FY2015ManagerId, FY2015Manager, FY2015CanManageVIP, FY2015CanManageBDM, FY2016ManagerId, 
				FY2016Manager, FY2016CanManageVIP, FY2016CanManageBDM, FY2017ManagerId, FY2017Manager, FY2017CanManageVIP, FY2017CanManageBDM, HandlerId, Handler, HandlerFromDate, PreviousHandlerId, PreviousHandler, FY2014HandlerId, FY2014Handler, 
				FY2015HandlerId, FY2015Handler, FY2016HandlerId, FY2016Handler, FY2017HandlerId, FY2017Handler, VIPType, VIPTypeFromDate, PreviousVIPType, FY2014VIPType, FY2015VIPType, 
				FY2016VIPType, FY2017VIPType, CanManagePotentialVIP, Level0, Level1, Level2, BDM, VIPCode, VIP, PotentialVIP, CompanyKey, 
				PreviousLevel0, PreviousLevel1, PreviousLevel2, PreviousBDM, PreviousVIPCode, PreviousVIP, PreviousCompanyKey, 
				FY2014Level0, FY2014Level1, FY2014Level2, FY2014BDM, FY2014VIPCode, FY2014VIP, FY2014CompanyKey, 
				FY2015Level0, FY2015Level1, FY2015Level2, FY2015BDM, FY2015VIPCode, FY2015VIP, FY2015CompanyKey, 
				FY2016Level0, FY2016Level1, FY2016Level2, FY2016BDM, FY2016VIPCode, FY2016VIP, FY2016CompanyKey,
				FY2017Level0, FY2017Level1, FY2017Level2, FY2017BDM, FY2017VIPCode, FY2017VIP, FY2017CompanyKey,
				FromDate, ToDate, FirstDate,
				CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy
			)
	Select 	StructureKey, LedgerId, LedgerSource, InPlay, AccountId, 
			OrgId, OrgIdFromDate, PreviousOrgId, FY2014OrgId, FY2015OrgId, FY2016OrgId, FY2017OrgId,
			IntroducerId, Introducer, IntroducerFromDate, PreviousIntroducerId, PreviousIntroducer, FY2014IntroducerId, FY2014Introducer, FY2015IntroducerId, 
			FY2015Introducer, FY2016IntroducerId, FY2016Introducer, FY2017IntroducerId, FY2017Introducer,	ManagerId, Manager, CanManageVIP, CanManageBDM, ManagerFromDate, PreviousManagerId, PreviousManager, 
			PreviousCanManageVIP, PreviousCanManageBDM, FY2014ManagerId, FY2014Manager, FY2014CanManageVIP, FY2014CanManageBDM, FY2015ManagerId, FY2015Manager,FY2015CanManageVIP, FY2015CanManageBDM, FY2016ManagerId, 
			FY2016Manager, FY2016CanManageVIP, FY2016CanManageBDM, FY2017ManagerId, FY2017Manager, FY2017CanManageVIP, FY2017CanManageBDM, HandlerId, Handler, HandlerFromDate, PreviousHandlerId, PreviousHandler, FY2014HandlerId, FY2014Handler, 
			FY2015HandlerId, FY2015Handler, FY2016HandlerId, FY2016Handler,FY2017HandlerId, FY2017Handler, 
			VIPType, VIPTypeFromDate, PreviousVIPType, FY2014VIPType, FY2015VIPType, FY2016VIPType, FY2017VIPType, CanManagePotentialVIP, 
			'Unknown' Level0, 'Unknown' Level1, 'Unknown' Level2, 'Unknown' BDM, -1 VIPCode, 'Unknown' VIP, 'Unknown' PotentialVIP, -1 CompanyKey, 
			'Unknown' PreviousLevel0, 'Unknown' PreviousLevel1, 'Unknown' PreviousLevel2, 'Unknown' PreviousBDM, -1 PreviousVIPCode, 'Unknown' PreviousVIP, -1 PreviousCompanyKey, 
			'Unknown' FY2014Level0, 'Unknown' FY2014Level1, 'Unknown' FY2014Level2, 'Unknown' FY2014BDM, -1 FY2014VIPCode, 'Unknown' FY2014VIP, -1 FY2014CompanyKey, 
			'Unknown' FY2015Level0, 'Unknown' FY2015Level1, 'Unknown' FY2015Level2, 'Unknown' FY2015BDM, -1 FY2015VIPCode, 'Unknown' FY2015VIP, -1 FY2015CompanyKey, 
			'Unknown' FY2016Level0, 'Unknown' FY2016Level1, 'Unknown' FY2016Level2, 'Unknown' FY2016BDM, -1 FY2016VIPCode, 'Unknown' FY2016VIP, -1 FY2016CompanyKey,
			'Unknown' FY2017Level0, 'Unknown' FY2017Level1, 'Unknown' FY2017Level2, 'Unknown' FY2017BDM, -1 FY2017VIPCode, 'Unknown' FY2017VIP, -1 FY2017CompanyKey,
			FromDate, CONVERT(datetime2(3),'9999-12-31') ToDate, FromDate FirstDate,
			GETDATE() CreatedDate, BatchKey CreatedBatchKey, 'Sp_DimStructure' CreatedBy,
			GETDATE() ModifiedDate,	BatchKey ModifiedBatchKey, 'Sp_DimStructure' ModifiedBy
	FROM	#Structure
	WHERE	Upsert = 'I'
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimStructure',NULL,'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimStructure', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
