CREATE PROC [Dimension].[Sp_Market]
(
	@BatchKey		int,
	@FromDate		datetime2(3),
	@ToDate			datetime2(3),
	@DryRun			bit = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int, @LastRowCount int

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Latency','Start',@Reload -- adjust historic from/to dates for any latency in the capture process when CDC was not on live database

	SELECT	*
	INTO	#Latency
	FROM	[$(Acquisition)].IntraBet.Latency
	WHERE	OriginalDate > @FromDate AND OriginalDate <= @ToDate
			AND OriginalDate <> CorrectedDate -- we are only interested in Latency records that change the From/To date - if there is no latency, don't need it as the latency value will only be applied selectively where it exists below
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'EventChanges','Start',@Reload, @RowsProcessed = @@ROWCOUNT

	SELECT	EventId, MIN(FromDate) FirstDate, MAX(FromDate) FromDate, MAX(ISNULL(IsMasterEvent,0)) IsMasterEvent
	INTO	#Candidates
	FROM	(
				----------- Intrabet.tblEvents -----------
				SELECT	EventId, FromDate, MAX(CONVERT(tinyint,IsMasterEvent)) IsMasterEvent
				FROM (	SELECT	EventID, FromDate, EventName, EventType, EventSubType, SportSubType, IsLiveEvent, LiveStreamPerformId, VenueID, Venue, IsMasterEvent,
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE EventDate END EventDate, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE RaceNumber END RaceNumber, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE Suspended END Suspended, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE ResultsIn END ResultsIn, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE IsAbandonedEvent END IsAbandonedEvent, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE BetsSettled END BetsSettled, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE SettledDate END SettledDate, 
								1 trg
						FROM	[$(Acquisition)].[IntraBet].tblEvents
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	EventID, ToDate, EventName, EventType, EventSubType, SportSubType, IsLiveEvent, LiveStreamPerformId, VenueID, Venue, IsMasterEvent,
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE EventDate END EventDate, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE RaceNumber END RaceNumber, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE Suspended END Suspended, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE ResultsIn END ResultsIn, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE IsAbandonedEvent END IsAbandonedEvent, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE BetsSettled END BetsSettled, 
								CASE IsMasterEvent WHEN 1 THEN NULL ELSE SettledDate END SettledDate, 
								2 trg
						FROM	[$(Acquisition)].[IntraBet].tblEvents
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY EventID, FromDate, EventName, EventType, EventSubType, SportSubType, IsLiveEvent, LiveStreamPerformId, VenueID, Venue, EventDate, RaceNumber, LiveStreamPerformId, Suspended, ResultsIn, IsAbandonedEvent, BetsSettled, SettledDate
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- PricingData.Race -----------
				SELECT	IntrabetID, FromDate, 0 IsMasterEvent
				FROM (	SELECT	IntrabetID, FromDate, TotalPrizeMoney, Distance, RaceGroup, RaceClass, HybridPricingTemplate, WeightType, NswEnabled, VicEnabled, UniEnabled, 1 trg
						FROM	[$(Acquisition)].[PricingData].Race
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	IntrabetID, FromDate, TotalPrizeMoney, Distance, RaceGroup, RaceClass, HybridPricingTemplate, WeightType, NswEnabled, VicEnabled, UniEnabled, 2 trg
						FROM	[$(Acquisition)].[PricingData].Race
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY IntrabetID, FromDate, TotalPrizeMoney, Distance, RaceGroup, RaceClass, HybridPricingTemplate, WeightType, NswEnabled, VicEnabled, UniEnabled
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
			) x
	GROUP BY EventId
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'EventHierarchy','Start',@Reload, @RowsProcessed = @@ROWCOUNT

	SET @RowCount = 0
	SET @LastRowCount = -1
	WHILE @RowCount <> @LastRowCount
		BEGIN
			SET @LastRowCount = @RowCount

			SELECT	x.EventId, x.IsMasterEvent, x.Firstdate, x.FromDate, 
					CASE WHEN ex.EventId IS NULL THEN 'I' WHEN x.FromDate > ex.FromDate THEN 'U' ELSE 'X' END Upsert
			INTO	#Hierarchy
			FROM	(	SELECT	e1.EventId, e1.IsMasterEvent, MIN(e.Firstdate) FirstDate, MAX(e.FromDate) FromDate
						FROM	#Candidates e
								INNER JOIN [$(Acquisition)].IntraBet.tblEvents e1 ON e1.MasterEventID = e.EventID AND e1.FromDate <= e.FromDate AND e1.ToDate > e.FromDate AND e1.EventID <> e.EventID
						WHERE	e.IsMasterEvent = 1
						GROUP BY e1.EventId, e1.IsMasterEvent
					) x
					LEFT JOIN #Candidates ex ON ex.EventId = x.EventId --AND ex.IsMasterEvent = x.IsMasterEvent
			WHERE	ex.EventID IS NULL OR x.FromDate > ex.FromDate
			OPTION (RECOMPILE);

			UPDATE	u 
				SET FromDate = x.FromDate
			FROM	#Candidates u
					INNER JOIN #Hierarchy x on x.eventid = u.eventid and x.Upsert = 'U'
			SET @RowCount = @RowCount + @@ROWCOUNT

			INSERT #Candidates SELECT EventId, FirstDate, FromDate, IsMasterEvent FROM #Hierarchy WHERE Upsert = 'I'
			SET @RowCount = @RowCount + @@ROWCOUNT

			DROP TABLE #Hierarchy

		END

	DELETE FROM #Candidates WHERE IsMasterEvent <> 0

	SET @RowCount = @RowCount - @@ROWCOUNT

	EXEC [Control].Sp_Log	@BatchKey,@Me,'PricingDataRace','Start',@Reload, @RowsProcessed = @RowCount

	--there is an issue with errors in the pricing data where the intrabetid is used more than once for different Ids - working assumption is the later of the 2 Ids contains the latest information
	SELECT	c.EventId, MAX(r.Id) Id
	INTO	#Race
	FROM	#Candidates c
			INNER JOIN [$(Acquisition)].PricingData.Race r on r.IntrabetID = c.EventId AND r.FromDate <= c.FromDate AND r.ToDate > c.FromDate
	WHERE	c.IsMasterEvent = 0
	GROUP BY c.EventId
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'StatusDates','Start',@Reload, @RowsProcessed = @@ROWCOUNT

	SELECT	EventId, 
			Opened,
			MinOpenedDateTime FirstOpenedDateTime, 
			CASE WHEN MaxNotClosedDateTime <= @ToDate THEN MaxNotClosedDateTime ELSE NULL END LastClosedDateTime, 
			CASE WHEN MaxNotAbandonedDateTime <= @ToDate THEN MaxNotAbandonedDateTime ELSE NULL END LastAbandonedDateTime, 
			CASE WHEN MaxNotResultedDateTime <= @ToDate THEN MaxNotResultedDateTime ELSE NULL END LastResultedDateTime, 
			MaxSettledDateTime LastSettledDateTime
	INTO	#StatusDates
	FROM	(	SELECT	c.EventId, 
						MAX(CASE e.Suspended WHEN 0 THEN 'Yes' ELSE 'No' END) Opened, -- the only status which persists - i.e. once opened then always opened, whereas all the others are based entirely on current state - closed, abandoned, resulted, etc
						MIN(CASE e.Suspended WHEN 0 THEN ISNULL(lf.CorrectedDate, e.FromDate) ELSE NULL END) MinOpenedDateTime, 
						MAX(CASE e.Suspended WHEN 0 THEN ISNULL(lt.CorrectedDate, e.ToDate) ELSE NULL END) MaxNotClosedDateTime, 
						MAX(CASE e.IsAbandonedEvent WHEN 0 THEN ISNULL(lt.CorrectedDate, e.ToDate) ELSE NULL END) MaxNotAbandonedDateTime,
						MAX(CASE e.ResultsIn WHEN 0 THEN ISNULL(lt.CorrectedDate, e.ToDate) ELSE NULL END) MaxNotResultedDateTime, 
						MAX(e.SettledDate) MaxSettledDateTime 
				FROM	#Candidates c
						INNER JOIN [$(Acquisition)].[IntraBet].[tblEvents] e ON e.EventID = c.EventId AND e.FromDate <= @ToDate
						LEFT JOIN #Latency lf ON lf.OriginalDate = e.FromDate
						LEFT JOIN #Latency lt ON lt.OriginalDate = e.ToDate
				WHERE	c.IsMasterEvent = 0
				GROUP BY c.EventID
			) x
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Level0','Start',@Reload, @RowsProcessed = @@RowCount
	-- Do the levels step by step rather than all in one statement to give the optimiser a chance vs the cardinalites with From/To date ranges
	SELECT	c.EventId MarketID,						
			CASE e.Suspended WHEN 1 THEN 'Yes' ELSE 'No' END Suspended,					
			CASE e.BetsSettled WHEN 1 THEN 'Yes' ELSE 'No' END Settled,					
			e.Limit MaxWinnings,					
			e.MaxStake,						
			CASE e.IsFutureEvent WHEN 1 THEN 'Yes' ELSE 'No' END FutureMarket,					
			e.EventDate ScheduledDateTime,				
			CASE e.ResultsIn WHEN 1 THEN 'Yes' ELSE 'No' END Resulted,						
			CASE e.IsAbandonedEvent WHEN 1 THEN 'Yes' ELSE 'No' END Abandoned,						
			e.EventId, e.EventName Event, e.EventType EventTypeId, et.description EventType, e.SportSubType SportSubTypeId, st.description SportSubType, e.IsLiveEvent, e.LiveStreamPerformId,
			e.VenueID, v.VenueName Venue, v.VenueType, v.EventType VenueEventTypeId, vt.Description VenueEventType, v.StateCode VenueStateCode, vs.StateName VenueState, v.CountryCode VenueCountryCode, vc.CountryName VenueCountry,
			0 Level,
			e.RaceNumber,				
			e.MasterEventID,
			c.FirstDate,						
			c.FromDate
	INTO	#Level0
	FROM	#Candidates c
			INNER JOIN [$(Acquisition)].IntraBet.tblEvents e ON e.EventId = c.EventId AND e.FromDate <= c.FromDate AND e.ToDate > c.FromDate
			LEFT JOIN [$(Acquisition)].[Intrabet].[tblLUEventTypes] et on et.eventtype = e.eventtype and et.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUSubSportTypes] st on st.SportSubType = e.SportSubType and st.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUVenues] v on v.VenueId = e.VenueId AND v.FromDate <= c.FromDate AND v.ToDate > c.FromDate
			LEFT JOIN [$(Acquisition)].[Intrabet].[tblLUEventTypes] vt on vt.eventtype = v.eventtype and vt.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUStates] vs on vs.Code = v.StateCode and vs.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUCountry] vc on vc.Country = v.CountryCode and vc.ToDate = '9999-12-31'
	WHERE	c.IsMasterEvent <> 1
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Level1','Start',@Reload, @RowsProcessed = @@RowCount

	SELECT	e.EventId, e.EventName Event, e.EventType EventTypeId, et.description EventType, e.SportSubType SportSubTypeId, st.description SportSubType, e.IsLiveEvent, LiveStreamPerformId,
			e.VenueID, v.VenueName Venue, v.VenueType, v.EventType VenueEventTypeId, vt.Description VenueEventType, v.StateCode VenueStateCode, vs.StateName VenueState, v.CountryCode VenueCountryCode, vc.CountryName VenueCountry,
			1 Level,
			e.MasterEventID,
			c.FromDate
	INTO	#Level1
	FROM	( SELECT MasterEventId EventId, MAX(FromDate) FromDate FROM #Level0 GROUP BY MasterEventId ) c
			INNER JOIN [$(Acquisition)].IntraBet.tblEvents e ON e.EventId = c.EventId AND e.FromDate <= c.FromDate AND e.ToDate > c.FromDate
			LEFT JOIN [$(Acquisition)].[Intrabet].[tblLUEventTypes] et on et.eventtype = e.eventtype and et.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUSubSportTypes] st on st.SportSubType = e.SportSubType and st.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUVenues] v on v.VenueId = e.VenueId AND v.FromDate <= c.FromDate AND v.ToDate > c.FromDate
			LEFT JOIN [$(Acquisition)].[Intrabet].[tblLUEventTypes] vt on vt.eventtype = v.eventtype and vt.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUStates] vs on vs.Code = v.StateCode and vs.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUCountry] vc on vc.Country = v.CountryCode and vc.ToDate = '9999-12-31'
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Level2','Start',@Reload, @RowsProcessed = @@RowCount

	SELECT	e.EventId, e.EventName Event, e.EventType EventTypeId, et.description EventType, e.SportSubType SportSubTypeId, st.description SportSubType, e.IsLiveEvent, LiveStreamPerformId,
			e.VenueID, v.VenueName Venue, v.VenueType, v.EventType VenueEventTypeId, vt.Description VenueEventType, v.StateCode VenueStateCode, vs.StateName VenueState, v.CountryCode VenueCountryCode, vc.CountryName VenueCountry,
			2 Level,
			e.MasterEventID,
			c.FromDate
	INTO	#Level2
	FROM	( SELECT MasterEventId EventId, MAX(FromDate) FromDate FROM #Level1 GROUP BY MasterEventId ) c
			INNER JOIN [$(Acquisition)].IntraBet.tblEvents e ON e.EventId = c.EventId AND e.FromDate <= c.FromDate AND e.ToDate > c.FromDate
			LEFT JOIN [$(Acquisition)].[Intrabet].[tblLUEventTypes] et on et.eventtype = e.eventtype and et.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUSubSportTypes] st on st.SportSubType = e.SportSubType and st.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUVenues] v on v.VenueId = e.VenueId AND v.FromDate <= c.FromDate AND v.ToDate > c.FromDate
			LEFT JOIN [$(Acquisition)].[Intrabet].[tblLUEventTypes] vt on vt.eventtype = v.eventtype and vt.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUStates] vs on vs.Code = v.StateCode and vs.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUCountry] vc on vc.Country = v.CountryCode and vc.ToDate = '9999-12-31'
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Level3','Start',@Reload, @RowsProcessed = @@RowCount

	SELECT	e.EventId, e.EventName Event, e.EventType EventTypeId, et.description EventType, e.SportSubType SportSubTypeId, st.description SportSubType, e.IsLiveEvent, LiveStreamPerformId,
			e.VenueID, v.VenueName Venue, v.VenueType, v.EventType VenueEventTypeId, vt.Description VenueEventType, v.StateCode VenueStateCode, vs.StateName VenueState, v.CountryCode VenueCountryCode, vc.CountryName VenueCountry,
			3 Level,
			e.MasterEventID,
			c.FromDate
	INTO	#Level3
	FROM	( SELECT MasterEventId EventId, MAX(FromDate) FromDate FROM #Level2 GROUP BY MasterEventId ) c
			INNER JOIN [$(Acquisition)].IntraBet.tblEvents e ON e.EventId = c.EventId AND e.FromDate <= c.FromDate AND e.ToDate > c.FromDate
			LEFT JOIN [$(Acquisition)].[Intrabet].[tblLUEventTypes] et on et.eventtype = e.eventtype and et.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUSubSportTypes] st on st.SportSubType = e.SportSubType and st.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUVenues] v on v.VenueId = e.VenueId AND v.FromDate <= c.FromDate AND v.ToDate > c.FromDate
			LEFT JOIN [$(Acquisition)].[Intrabet].[tblLUEventTypes] vt on vt.eventtype = v.eventtype and vt.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUStates] vs on vs.Code = v.StateCode and vs.ToDate = '9999-12-31'
			LEFT JOIN [$(Acquisition)].[IntraBet].[tblLUCountry] vc on vc.Country = v.CountryCode and vc.ToDate = '9999-12-31'
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Staging','Start',@Reload, @RowsProcessed = @@RowCount

	SELECT	e.MarketID,						
			'IEI' Source,						
			d.Opened,					
			d.FirstOpenedDateTime,			
			CASE WHEN e.Suspended = 'Yes' AND d.Opened = 'Yes' THEN 'Yes' ELSE 'No' END Closed,					
			d.LastClosedDateTime,			
			e.Settled,					
			d.LastSettledDateTime,			
			e.MaxWinnings,					
			e.MaxStake,						
			e.FutureMarket,					
			e.ScheduledDateTime,				
			e.Resulted,
			d.LastResultedDateTime,				
			e.Abandoned,
			d.LastAbandonedDateTime,
			e.EventId EventId0, e.Event Event0, e.EventTypeId EventTypeId0, e.EventType EventType0, e.SportSubTypeId SportSubTypeId0, e.SportSubType SportSubType0, e.IsLiveEvent IsLiveEvent0, e.LiveStreamPerformId LiveStreamPerformId0,
			e.VenueID VenueId0, e.Venue Venue0, e.VenueType VenueType0, e.VenueEventTypeId VenueEventTypeId0, e.VenueEventType VenueEventType0, e.VenueStateCode VenueStateCode0, e.VenueState VenueState0, e.VenueCountryCode VenueCountryCode0, e.VenueCountry VenueCountry0,
			e1.EventId EventId1, e1.Event Event1, e1.EventTypeId EventTypeId1, e1.EventType EventType1, e1.SportSubTypeId SportSubTypeId1, e1.SportSubType SportSubType1, e1.IsLiveEvent IsLiveEvent1, e1.LiveStreamPerformId LiveStreamPerformId1,
			e1.VenueID VenueId1, e1.Venue Venue1, e1.VenueType VenueType1, e1.VenueEventTypeId VenueEventTypeId1, e1.VenueEventType VenueEventType1, e1.VenueStateCode VenueStateCode1, e1.VenueState VenueState1, e1.VenueCountryCode VenueCountryCode1, e1.VenueCountry VenueCountry1,
			e2.EventId EventId2, e2.Event Event2, e2.EventTypeId EventTypeId2, e2.EventType EventType2, e2.SportSubTypeId SportSubTypeId2, e2.SportSubType SportSubType2, e2.IsLiveEvent IsLiveEvent2, e2.LiveStreamPerformId LiveStreamPerformId2,
			e2.VenueID VenueId2, e2.Venue Venue2, e2.VenueType VenueType2, e2.VenueEventTypeId VenueEventTypeId2, e2.VenueEventType VenueEventType2, e2.VenueStateCode VenueStateCode2, e2.VenueState VenueState2, e2.VenueCountryCode VenueCountryCode2, e2.VenueCountry VenueCountry2,
			e3.EventId EventId3, e3.Event Event3, e3.EventTypeId EventTypeId3, e3.EventType EventType3, e3.SportSubTypeId SportSubTypeId3, e3.SportSubType SportSubType3, e3.IsLiveEvent IsLiveEvent3, e3.LiveStreamPerformId LiveStreamPerformId3,
			e3.VenueID VenueId3, e3.Venue Venue3, e3.VenueType VenueType3, e3.VenueEventTypeId VenueEventTypeId3, e3.VenueEventType VenueEventType3, e3.VenueStateCode VenueStateCode3, e3.VenueState VenueState3, e3.VenueCountryCode VenueCountryCode3, e3.VenueCountry VenueCountry3,
			COALESCE(e3.Level, e2.Level, e1.Level, e.Level) TopLevel,
			e.RaceNumber,				
			tc.Track,					
			tc.Weather,					
			r.TotalPrizeMoney,			
			r.Distance SourceDistance,				
			r.RaceGroup,				
			r.RaceClass SourceRaceClass,				
			r.HybridPricingTemplate SourceHybridPricingTemplate,	
			r.WeightType SourceWeightType,		
			CONVERT(int,r.NSWEnabled)*1 + CONVERT(int,r.VICEnabled)*2 + CONVERT(int,r.UNIEnabled)*4 TABListing,
			e.FirstDate,						
			e.FromDate
	INTO	#Stage 
	FROM	#Level0 e
			LEFT JOIN #Level1 e1  ON e1.EventID = e.MasterEventId
			LEFT JOIN #Level2 e2  ON e2.EventID = e1.MasterEventId
			LEFT JOIN #Level3 e3  ON e3.EventID = e2.MasterEventId
			LEFT JOIN #Race rx ON rx.EventId = e.EventID
			LEFT JOIN [$(Acquisition)].[PricingData].[Race] r ON r.ID = rx.Id AND r.FromDate <= e.FromDate AND r.ToDate > e.FromDate
			LEFT JOIN #StatusDates d ON d.EventId = e.EventID
			LEFT JOIN [$(Acquisition)].IntraBet.tblTrackConditions tc on tc.VenueID = e.VenueID AND tc.EventType = ISNULL(e.VenueEventTypeId,e.EventTypeId) AND tc.FromDate <= e.FromDate AND tc.ToDate > e.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblTrackConditions tc2 on tc2.VenueID = e.VenueID AND tc2.FromDate <= e.FromDate AND tc2.ToDate > e.FromDate AND tc.VenueID is null
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'ForeignKeys','Start',@RowsProcessed = @@ROWCOUNT

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT	e.EventKey MarketKey,
			CASE s.Opened WHEN 'Yes' THEN ISNULL(od.DayKey,-1) ELSE 0 END OpenedDayKey,
			CASE s.Closed WHEN 'Yes' THEN ISNULL(cd.DayKey,-1) ELSE 0 END ClosedDayKey,
			CASE s.Settled WHEN 'Yes' THEN ISNULL(sd.DayKey,-1) ELSE 0 END SettledDayKey,
			ISNULL(sc.DayKey,-1) ScheduledDayKey,
			CASE s.Resulted WHEN 'Yes' THEN ISNULL(rd.DayKey,-1) ELSE 0 END ResultedDayKey,
			CASE s.Abandoned WHEN 'Yes' THEN ISNULL(ad.DayKey,-1) ELSE 0 END AbandonedDayKey,
			s.*
	INTO	#StageFK
	FROM	#Stage s
			LEFT JOIN [$(Dimensional)].CC.Day od ON od.MasterDayText = CONVERT(varchar(11),CONVERT(DATE, s.FirstOpenedDateTime)) and od.FromDate <= s.FirstOpenedDateTime AND od.ToDate > s.FirstOpenedDateTime
			LEFT JOIN [$(Dimensional)].CC.Day cd ON od.MasterDayText = CONVERT(varchar(11),CONVERT(DATE, s.LastClosedDateTime)) and cd.FromDate <= s.LastClosedDateTime AND cd.ToDate > s.LastClosedDateTime
			LEFT JOIN [$(Dimensional)].CC.Day sd ON od.MasterDayText = CONVERT(varchar(11),CONVERT(DATE, s.LastSettledDateTime)) and sd.FromDate <= s.LastSettledDateTime AND sd.ToDate > s.LastSettledDateTime
			LEFT JOIN [$(Dimensional)].CC.Day sc ON od.MasterDayText = CONVERT(varchar(11),CONVERT(DATE, s.ScheduledDateTime)) and sc.FromDate <= s.ScheduledDateTime AND sc.ToDate > s.ScheduledDateTime
			LEFT JOIN [$(Dimensional)].CC.Day rd ON od.MasterDayText = CONVERT(varchar(11),CONVERT(DATE, s.LastResultedDateTime)) and rd.FromDate <= s.LastResultedDateTime AND rd.ToDate > s.LastResultedDateTime
			LEFT JOIN [$(Dimensional)].CC.Day ad ON od.MasterDayText = CONVERT(varchar(11),CONVERT(DATE, s.LastAbandonedDateTime)) and ad.FromDate <= s.LastAbandonedDateTime AND ad.ToDate > s.LastAbandonedDateTime
			-- Temporary measure to align keys with current EventKey
			LEFT JOIN [$(Dimensional)].Dimension.Event e ON e.EventId = s.MarketId

	EXEC [Control].Sp_Log	@BatchKey,@Me,'NULL Key','Start',@RowsProcessed = @@ROWCOUNT

	UPDATE #StageFK SET MarketKey = NEXT VALUE FOR [$(Dimensional)].CC.MarketKey WHERE MarketKey IS NULL

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Dimension','Start',@RowsProcessed = @@ROWCOUNT

	SELECT	MarketKey, OpenedDayKey, ClosedDayKey, SettledDayKey, ScheduledDayKey, ResultedDayKey, AbandonedDayKey,
			MarketID, Source,						
			Opened, FirstOpenedDateTime, Closed, LastClosedDateTime, Settled, LastSettledDateTime,			
			MaxWinnings, MaxStake, FutureMarket,ScheduledDateTime,				
			Resulted, LastResultedDateTime, Abandoned, LastAbandonedDateTime,
			EventId0, Event0, EventTypeId0, EventType0, SportSubTypeId0, SportSubType0, IsLiveEvent0, LiveStreamPerformId0,
			VenueId0, Venue0, VenueType0, VenueEventTypeId0, VenueEventType0, VenueStateCode0, VenueState0, VenueCountryCode0, VenueCountry0,
			EventId1, Event1, EventTypeId1, EventType1, SportSubTypeId1, SportSubType1, IsLiveEvent1, LiveStreamPerformId1,
			VenueId1, Venue1, VenueType1, VenueEventTypeId1, VenueEventType1, VenueStateCode1, VenueState1, VenueCountryCode1, VenueCountry1,
			EventId2, Event2, EventTypeId2, EventType2, SportSubTypeId2, SportSubType2, IsLiveEvent2, LiveStreamPerformId2,
			VenueId2, Venue2, VenueType2, VenueEventTypeId2, VenueEventType2, VenueStateCode2, VenueState2, VenueCountryCode2, VenueCountry2,
			EventId3, Event3, EventTypeId3, EventType3, SportSubTypeId3, SportSubType3, IsLiveEvent3, LiveStreamPerformId3,
			VenueId3, Venue3, VenueType3, VenueEventTypeId3, VenueEventType3, VenueStateCode3, VenueState3, VenueCountryCode3, VenueCountry3,
			TopLevel, RaceNumber, Track, Weather,					
			TotalPrizeMoney, SourceDistance, RaceGroup, SourceRaceClass, SourceHybridPricingTemplate, SourceWeightType, TABListing,
			FirstDate, FromDate
	INTO	#Fact
	FROM	[$(Dimensional)].CC.Market
	WHERE	MarketId IN (SELECT DISTINCT MarketId FROM #StageFK)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start',@RowsProcessed = @@ROWCOUNT

	SELECT	*, CASE WHEN COUNT(*) OVER (PARTITION BY MarketId, Source) > 1 THEN 'U' WHEN src = 'S' THEN 'I' WHEN src = 'F' THEN 'D' ELSE 'X' END Upsert
	INTO	#Differences
	FROM	(	SELECT	MAX(MarketKey) MarketKey, OpenedDayKey, ClosedDayKey, SettledDayKey, ScheduledDayKey, ResultedDayKey, AbandonedDayKey,
						MarketID, Source,						
						Opened, FirstOpenedDateTime, Closed, LastClosedDateTime, Settled, LastSettledDateTime,			
						MaxWinnings, MaxStake, FutureMarket,ScheduledDateTime,				
						Resulted, LastResultedDateTime, Abandoned, LastAbandonedDateTime,
						EventId0, Event0, EventTypeId0, EventType0, SportSubTypeId0, SportSubType0, IsLiveEvent0, LiveStreamPerformId0,
						VenueId0, Venue0, VenueType0, VenueEventTypeId0, VenueEventType0, VenueStateCode0, VenueState0, VenueCountryCode0, VenueCountry0,
						EventId1, Event1, EventTypeId1, EventType1, SportSubTypeId1, SportSubType1, IsLiveEvent1, LiveStreamPerformId1,
						VenueId1, Venue1, VenueType1, VenueEventTypeId1, VenueEventType1, VenueStateCode1, VenueState1, VenueCountryCode1, VenueCountry1,
						EventId2, Event2, EventTypeId2, EventType2, SportSubTypeId2, SportSubType2, IsLiveEvent2, LiveStreamPerformId2,
						VenueId2, Venue2, VenueType2, VenueEventTypeId2, VenueEventType2, VenueStateCode2, VenueState2, VenueCountryCode2, VenueCountry2,
						EventId3, Event3, EventTypeId3, EventType3, SportSubTypeId3, SportSubType3, IsLiveEvent3, LiveStreamPerformId3,
						VenueId3, Venue3, VenueType3, VenueEventTypeId3, VenueEventType3, VenueStateCode3, VenueState3, VenueCountryCode3, VenueCountry3,
						TopLevel, RaceNumber, Track, Weather,					
						TotalPrizeMoney, SourceDistance, RaceGroup, SourceRaceClass, SourceHybridPricingTemplate, SourceWeightType,	TABListing,
						MIN(FirstDate) FirstDate, FromDate, MAX(src) src
				FROM	(	SELECT *, 'S' src FROM #StageFK
							UNION ALL
							SELECT *, 'F' src FROM #Fact
						) x
				GROUP BY OpenedDayKey, ClosedDayKey, SettledDayKey, ScheduledDayKey, ResultedDayKey, AbandonedDayKey,
						MarketID, Source,						
						Opened, FirstOpenedDateTime, Closed, LastClosedDateTime, Settled, LastSettledDateTime,			
						MaxWinnings, MaxStake, FutureMarket,ScheduledDateTime,				
						Resulted, LastResultedDateTime, Abandoned, LastAbandonedDateTime,
						EventId0, Event0, EventTypeId0, EventType0, SportSubTypeId0, SportSubType0, IsLiveEvent0, LiveStreamPerformId0,
						VenueId0, Venue0, VenueType0, VenueEventTypeId0, VenueEventType0, VenueStateCode0, VenueState0, VenueCountryCode0, VenueCountry0,
						EventId1, Event1, EventTypeId1, EventType1, SportSubTypeId1, SportSubType1, IsLiveEvent1, LiveStreamPerformId1,
						VenueId1, Venue1, VenueType1, VenueEventTypeId1, VenueEventType1, VenueStateCode1, VenueState1, VenueCountryCode1, VenueCountry1,
						EventId2, Event2, EventTypeId2, EventType2, SportSubTypeId2, SportSubType2, IsLiveEvent2, LiveStreamPerformId2,
						VenueId2, Venue2, VenueType2, VenueEventTypeId2, VenueEventType2, VenueStateCode2, VenueState2, VenueCountryCode2, VenueCountry2,
						EventId3, Event3, EventTypeId3, EventType3, SportSubTypeId3, SportSubType3, IsLiveEvent3, LiveStreamPerformId3,
						VenueId3, Venue3, VenueType3, VenueEventTypeId3, VenueEventType3, VenueStateCode3, VenueState3, VenueCountryCode3, VenueCountry3,
						TopLevel, RaceNumber, Track, Weather,					
						TotalPrizeMoney, SourceDistance, RaceGroup, SourceRaceClass, SourceHybridPricingTemplate, SourceWeightType, TABListing,
						FromDate
				HAVING	COUNT(*) = 1
			) x
	OPTION (RECOMPILE)

	IF @DryRun = 1 
		SELECT	TOP 10000 
				CASE WHEN Upsert = 'U' AND src = 'F' THEN 'U-' WHEN Upsert = 'U' AND src = 'S' THEN 'U+' ELSE Upsert END act, 
				* 
		FROM	#Differences 
		ORDER BY MarketId, Source, src desc

	SELECT	* INTO #Prepare FROM #Differences WHERE	Upsert <> 'U' OR src <> 'F' -- Discard the 'before' version record of Update

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start',@RowsProcessed = @@ROWCOUNT

	IF @DryRun = 0 
		BEGIN
			UPDATE	d 
				SET	OpenedDayKey = p.OpenedDayKey,
					ClosedDayKey = p.ClosedDayKey,
					SettledDayKey = p.SettledDayKey,
					ScheduledDayKey = p.ScheduledDayKey,
					ResultedDayKey = p.ResultedDayKey,
					AbandonedDayKey = p.AbandonedDayKey,
					Opened = p.Opened,
					FirstOpenedDateTime = p.FirstOpenedDateTime,
					Closed = p.Closed,
					LastClosedDateTime = p.LastClosedDateTime,
					Settled = p.Settled,
					LastSettledDateTime = p.LastSettledDateTime,			
					MaxWinnings = p.MaxWinnings,
					MaxStake = p.MaxStake,
					FutureMarket = p.FutureMarket,
					ScheduledDateTime = p.ScheduledDateTime,
					Resulted = p.Resulted,
					LastResultedDateTime = p.LastResultedDateTime,
					Abandoned = p.Abandoned,
					LastAbandonedDateTime = p.LastAbandonedDateTime,
					EventId0 = p.EventId0,
					Event0 = p.Event0,
					EventTypeId0 = p.EventTypeId0,
					EventType0 = p.EventType0,
					SportSubTypeId0 = p.SportSubTypeId0,
					SportSubType0 = p.SportSubType0,
					IsLiveEvent0 = p.IsLiveEvent0,
					LiveStreamPerformId0 = p.LiveStreamPerformId0,
					VenueId0 = p.VenueId0,
					Venue0 = p.Venue0,
					VenueType0 = p.VenueType0,
					VenueEventTypeId0 = p.VenueEventTypeId0,
					VenueEventType0 = p.VenueEventType0,
					VenueStateCode0 = p.VenueStateCode0,
					VenueState0 = p.VenueState0,
					VenueCountryCode0 = p.VenueCountryCode0,
					VenueCountry0 = p.VenueCountry0,
					EventId1 = p.EventId1,
					Event1 = p.Event1,
					EventTypeId1 = p.EventTypeId1,
					EventType1 = p.EventType1,
					SportSubTypeId1 = p.SportSubTypeId1,
					SportSubType1 = p.SportSubType1,
					IsLiveEvent1 = p.IsLiveEvent1,
					LiveStreamPerformId1 = p.LiveStreamPerformId1,
					VenueId1 = p.VenueId1,
					Venue1 = p.Venue1,
					VenueType1 = p.VenueType1,
					VenueEventTypeId1 = p.VenueEventTypeId1,
					VenueEventType1 = p.VenueEventType1,
					VenueStateCode1 = p.VenueStateCode1,
					VenueState1 = p.VenueState1,
					VenueCountryCode1 = p.VenueCountryCode1,
					VenueCountry1 = p.VenueCountry1,
					EventId2 = p.EventId2,
					Event2 = p.Event2,
					EventTypeId2 = p.EventTypeId2,
					EventType2 = p.EventType2,
					SportSubTypeId2 = p.SportSubTypeId2,
					SportSubType2 = p.SportSubType2,
					IsLiveEvent2 = p.IsLiveEvent2,
					LiveStreamPerformId2 = p.LiveStreamPerformId2,
					VenueId2 = p.VenueId2,
					Venue2 = p.Venue2,
					VenueType2 = p.VenueType2,
					VenueEventTypeId2 = p.VenueEventTypeId2,
					VenueEventType2 = p.VenueEventType2,
					VenueStateCode2 = p.VenueStateCode2,
					VenueState2 = p.VenueState2,
					VenueCountryCode2 = p.VenueCountryCode2,
					VenueCountry2 = p.VenueCountry2,
					EventId3 = p.EventId3,
					Event3 = p.Event3,
					EventTypeId3 = p.EventTypeId3,
					EventType3 = p.EventType3,
					SportSubTypeId3 = p.SportSubTypeId3,
					SportSubType3 = p.SportSubType3,
					IsLiveEvent3 = p.IsLiveEvent3,
					LiveStreamPerformId3 = p.LiveStreamPerformId3,
					VenueId3 = p.VenueId3,
					Venue3 = p.Venue3,
					VenueType3 = p.VenueType3,
					VenueEventTypeId3 = p.VenueEventTypeId3,
					VenueEventType3 = p.VenueEventType3,
					VenueStateCode3 = p.VenueStateCode3,
					VenueState3 = p.VenueState3,
					VenueCountryCode3 = p.VenueCountryCode3,
					VenueCountry3 = p.VenueCountry3,
					TopLevel = p.TopLevel,
					RaceNumber = p.RaceNumber,
					Track = p.Track,
					Weather = p.Weather,
					TotalPrizeMoney = p.TotalPrizeMoney,
					SourceDistance = p.SourceDistance,
					RaceGroup = p.RaceGroup,
					SourceRaceClass = p.SourceRaceClass,
					SourceHybridPricingTemplate = p.SourceHybridPricingTemplate,
					SourceWeightType = p.SourceWeightType,
					TABListing = p.TABListing,
					FromDate = p.FromDate,
					ModifiedDate  = SYSDATETIME(), 
					ModifiedBatchKey = @BatchKey,
					ModifiedBy = @Me
			FROM	[$(Dimensional)].CC.Market d
					INNER JOIN #Prepare p  ON p.MarketId = d.MarketId AND p.Source = d.Source
			WHERE	p.Upsert = 'U'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'U'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start',@RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			INSERT [$(Dimensional)].CC.Market
				(	MarketKey, OpenedDayKey, ClosedDayKey, SettledDayKey, ScheduledDayKey, ResultedDayKey, AbandonedDayKey,
					MarketID, Source, Market, 
					Opened, FirstOpenedDateTime, FirstOpenedVenueDateTime, 
					Closed, LastClosedDateTime, LastClosedVenueDateTime, 
					Settled, LastSettledDateTime, LastSettledVenueDateTime,
					MaxWinnings, MaxStake, LevyRate, 
					Market_MarketTypeKey, MarketType, LiveMarket, FutureMarket,
					Market_MarketCategoryKey, MarketCategory,
					Market_EventKey, Event,
					ScheduledDateTime, ScheduledVenueDateTime,
					Resulted, LastResultedDateTime, LastResultedVenueDateTime,
					Abandoned, LastAbandonedDateTime, LastAbandonedVenueDateTime,
					Distance, PrizePool, Grade, RaceClass, Age, Gender, WeightType, TrackCondition, EventWeather,
					HybridPricingTemplate, LiveEvent, LiveVideoStreamed, LiveScoreBoard, NSWTAB, VICTAB, UNITAB,
					Market_RoundKey, Round, RoundSequence, 
					Market_CompetitionKey, Competition, CompetitionSeason, CompetitionStartDate, CompetitionEndDate, 
					Market_CompetitionClassKey, CompetitionClass, 
					Market_CompetitionTypeKey, CompetitionType,
					Market_VenueKey, Venue, VenueLocation, VenueType,
					Market_VenueStateKey, VenueStateCode, VenueState,
					Market_VenueCountryKey, VenueCountryCode, VenueCountry,
					Market_SubClassKey, SubClassCode, SubClass,
					Market_ClassKey, ClassCode, Class,
					Market_SuperClassKey, SuperClassCode, SuperClass,
					EventId0, Event0, EventTypeId0, EventType0, SportSubTypeId0, SportSubType0, IsLiveEvent0, LiveStreamPerformId0,
					VenueId0, Venue0, VenueType0, VenueEventTypeId0, VenueEventType0, VenueStateCode0, VenueState0, VenueCountryCode0, VenueCountry0,
					EventId1, Event1, EventTypeId1, EventType1, SportSubTypeId1, SportSubType1, IsLiveEvent1, LiveStreamPerformId1,
					VenueId1, Venue1, VenueType1, VenueEventTypeId1, VenueEventType1, VenueStateCode1, VenueState1, VenueCountryCode1, VenueCountry1,
					EventId2, Event2, EventTypeId2, EventType2, SportSubTypeId2, SportSubType2, IsLiveEvent2, LiveStreamPerformId2,
					VenueId2, Venue2, VenueType2, VenueEventTypeId2, VenueEventType2, VenueStateCode2, VenueState2, VenueCountryCode2, VenueCountry2,
					EventId3, Event3, EventTypeId3, EventType3, SportSubTypeId3, SportSubType3, IsLiveEvent3, LiveStreamPerformId3,
					VenueId3, Venue3, VenueType3, VenueEventTypeId3, VenueEventType3, VenueStateCode3, VenueState3, VenueCountryCode3, VenueCountry3,
					TopLevel, RaceNumber, Track, Weather,					
					TotalPrizeMoney, SourceDistance, RaceGroup, SourceRaceClass, SourceHybridPricingTemplate, SourceWeightType,	TABListing,
					FirstDate, FromDate, ToDate,
					CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy
				)
			SELECT	MarketKey, OpenedDayKey, ClosedDayKey, SettledDayKey, ScheduledDayKey, ResultedDayKey, AbandonedDayKey,
					MarketID, Source, 'Pending' Market, 
					Opened, FirstOpenedDateTime, NULL FirstOpenedVenueDateTime, 
					Closed, LastClosedDateTime, NULL LastClosedVenueDateTime, 
					Settled, LastSettledDateTime, NULL LastSettledVenueDateTime,
					MaxWinnings, MaxStake, 'Pending' LevyRate, 
					-2 Market_MarketTypeKey, 'Pending' MarketType, 'PDG' LiveMarket, FutureMarket,
					-2 Market_MarketCategoryKey, 'Pending' MarketCategory,
					-2 Market_EventKey, 'Pending' Event,
					ScheduledDateTime, NULL ScheduledVenueDateTime,
					Resulted, LastResultedDateTime, NULL LastResultedVenueDateTime,
					Abandoned, LastAbandonedDateTime, NULL LastAbandonedVenueDateTime,
					'Pending' Distance, NULL PrizePool, 'Pending' Grade, 'Pending' RaceClass, 'Pending' Age, 'Pending' Gender, 'Pending' WeightType, 
					'Pending' TrackCondition, 'Pending' EventWeather,
					'Pending' HybridPricingTemplate, 'PDG' LiveEvent, 'PDG' LiveVideoStreamed, 'PDG' LiveScoreBoard, 'PDG' NSWTAB, 'PDG' VICTAB, 'PDG' UNITAB,
					-2 Market_RoundKey, 'Pending' Round, NULL RoundSequence, 
					-2 Market_CompetitionKey, 'Pending' Competition, 'Pending' CompetitionSeason, NULL CompetitionStartDate, NULL CompetitionEndDate, 
					-2 Market_CompetitionClassKey, 'Pending' CompetitionClass, 
					-2 Market_CompetitionTypeKey, 'Pending' CompetitionType,
					-2 Market_VenueKey, 'Pending' Venue, 'Pending' VenueLocation, 'Pending' VenueType,
					-2 Market_VenueStateKey, 'PDG' VenueStateCode, 'Pending' VenueState,
					-2 Market_VenueCountryKey, 'PDG' VenueCountryCode, 'Pending' VenueCountry,
					-2 Market_SubClassKey, 'Pending' SubClassCode, 'Pending' SubClass,
					-2 Market_ClassKey, 'PDG' ClassCode, 'Pending' Class,
					-2 Market_SuperClassKey, 'PDG' SuperClassCode, 'Pending' SuperClass,
					EventId0, Event0, EventTypeId0, EventType0, SportSubTypeId0, SportSubType0, IsLiveEvent0, LiveStreamPerformId0,
					VenueId0, Venue0, VenueType0, VenueEventTypeId0, VenueEventType0, VenueStateCode0, VenueState0, VenueCountryCode0, VenueCountry0,
					EventId1, Event1, EventTypeId1, EventType1, SportSubTypeId1, SportSubType1, IsLiveEvent1, LiveStreamPerformId1,
					VenueId1, Venue1, VenueType1, VenueEventTypeId1, VenueEventType1, VenueStateCode1, VenueState1, VenueCountryCode1, VenueCountry1,
					EventId2, Event2, EventTypeId2, EventType2, SportSubTypeId2, SportSubType2, IsLiveEvent2, LiveStreamPerformId2,
					VenueId2, Venue2, VenueType2, VenueEventTypeId2, VenueEventType2, VenueStateCode2, VenueState2, VenueCountryCode2, VenueCountry2,
					EventId3, Event3, EventTypeId3, EventType3, SportSubTypeId3, SportSubType3, IsLiveEvent3, LiveStreamPerformId3,
					VenueId3, Venue3, VenueType3, VenueEventTypeId3, VenueEventType3, VenueStateCode3, VenueState3, VenueCountryCode3, VenueCountry3,
					TopLevel, RaceNumber, Track, Weather,					
					TotalPrizeMoney, SourceDistance, RaceGroup, SourceRaceClass, SourceHybridPricingTemplate, SourceWeightType, TABListing,
					FirstDate, FromDate, '9999-12-31' ToDate,
					SYSDATETIME() CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, SYSDATETIME() ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy
			FROM	#Prepare p
			WHERE	p.Upsert = 'I'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'I'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Success',@RowsProcessed = @RowCount
END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

