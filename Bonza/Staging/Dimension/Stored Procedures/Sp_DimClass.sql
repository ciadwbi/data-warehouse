﻿CREATE PROC [Dimension].[Sp_DimClass]
(
	@BatchKey	int,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @RowCount int

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'InsertIntoDim','Start'

	INSERT INTO [$(Dimensional)].Dimension.Class
           ([ClassId]
           ,[Source]
           ,[SourceClassName]
           ,[ClassCode]
           ,[ClassName]
           ,[ClassIcon]
           ,[SuperclassKey]
           ,[SuperclassName]
           ,[FromDate]
           ,[ToDate]
		   ,[CreatedDate]
		   ,[CreatedBatchKey]
		   ,[CreatedBy]
		   ,[ModifiedDate]
		   ,[ModifiedBatchKey]
		   ,[ModifiedBy]
		   )
	Select a.[ClassId]
		  ,a.[Source]
		  ,a.[SourceclassName]
		  ,a.[ClassCode]
		  ,a.[ClassName]
		  ,a.[classIcon]
		  ,a.[SuperClassKey]
		  ,a.[SuperClassName]
		  ,a.[FromDate]
		  ,a.[ToDate] 
		  , getdate()
		  , @BatchKey
		  , 'Sp_DimClass'
		  , getdate()
		  , @BatchKey
		  , 'Sp_DimClass'
	From [Stage].[Class] a
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Class b on a.ClassId = b.ClassId
	Where a.BatchKey = @BatchKey and b.ClassId is null

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount	

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
