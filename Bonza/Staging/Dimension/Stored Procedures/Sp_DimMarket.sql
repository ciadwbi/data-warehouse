CREATE PROC [Dimension].[Sp_DimMarket]
(
	@BatchKey	int
)
AS 

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY

	SELECT	m.Source, 
			m.MarketId, 
			m.Market, 
			m.MarketType, 
			m.MarketGroup, 
			m.MarketClosed,
			m.MarketClosedDateTime,
			m.MarketOpen,
			m.MarketOpenedDateTime, 
			m.MarketSettled,
			m.MarketSettledDateTime, 
			ISNULL(SESD.DayKey,-1) MarketSettledDayKey,  
			ISNULL(ODK.DayKey,-1) MarketOpenedDayKey, 
			ISNULL(SDK.DayKey,-1)MarketClosedDayKey,  
			m.LiveMarket,
			m.FutureMarket,
			m.MaxWinnings,
			m.MaxStake,
			m.LevyRate, 
			m.Sport, 
			m.FromDate,
			CASE WHEN d.MarketId IS NULL THEN 'I' ELSE 'U' END Upsert
	INTO	#Market
	FROM 	Stage.Market m
			LEFT JOIN [$(Dimensional)].Dimension.Market d ON (d.MarketId = m.MarketId AND d.Source = m.Source) 
			LEFT JOIN [$(Dimensional)].Dimension.DayZone SESD ON SESD.MasterDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,m.MarketSettledDateTime)),'N/A') and SESD.AnalysisDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,m.MarketSettledDateTime)),'N/A')
			LEFT JOIN [$(Dimensional)].Dimension.DayZone ODK ON ODK.MasterDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,m.MarketOpenedDateTime)),'N/A') and ODK.AnalysisDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,m.MarketOpenedDateTime)),'N/A')
			LEFT JOIN [$(Dimensional)].Dimension.DayZone SDK ON SDK.MasterDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,m.MarketClosedDateTime)),'N/A') and SDK.AnalysisDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,m.MarketClosedDateTime)),'N/A')
	WHERE BatchKey = @BatchKey

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimMarket','Type1-UpdateStaging','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE d
		SET	d.Market = m.Market,
			d.MarketType = m.MarketType,
			d.MarketGroup = m.MarketGroup,
			d.MarketClosed = m.MarketClosed,
			d.MarketClosedDateTime = m.MarketClosedDateTime,
			d.MarketOpen = m.MarketOpen,
			d.MarketOpenedDatetime = m.MarketOpenedDateTime, 
			d.MarketSettled = m.MarketSettled,
			d.MarketSettledDateTime = m.MarketSettledDateTime, 
			d.MarketSettledDayKey = m.MarketSettledDayKey,
			d.MarketOpenedDayKey = m.MarketOpenedDayKey,
			d.MarketClosedDayKey = m.MarketClosedDayKey,
			d.LiveMarket = m.LiveMarket,
			d.Futuremarket = m.FutureMarket,
			d.MaxWinnings = m.MaxWinnings,
			d.MaxStake = m.MaxStake,
			d.LevyRate = m.LevyRate,
			d.Sport = m.Sport,
			d.FromDate = m.FromDate, 
			d.ModifiedDate = CURRENT_TIMESTAMP, 
			d.ModifiedBatchKey = @BatchKey,
			d.ModifiedBy = 'Sp_DimMarket'
	FROM 	#Market m
			INNER JOIN [$(Dimensional)].Dimension.Market d ON (d.MarketId = m.MarketId AND d.Source = m.Source) 
	WHERE 	m.Upsert = 'U'

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimMarket','Type1-InsertIntoStaging','Start', @RowsProcessed = @@ROWCOUNT

	INSERT INTO [$(Dimensional)].Dimension.Market 
			(Source, MarketId, Market, MarketType, MarketGroup, MarketClosed, MarketClosedDateTime, MarketOpen, MarketOpenedDatetime, MarketSettled, MarketSettledDateTime, 
			MarketSettledDayKey, MarketOpenedDayKey, MarketClosedDayKey, LiveMarket, FutureMarket, MaxWinnings, MaxStake, LevyRate, Sport, FromDate, FirstDate, 
			CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy)
	SELECT	m.Source, m.MarketId, m.Market, m.MarketType, m.MarketGroup, m.MarketClosed,  m.MarketClosedDateTime, m.MarketOpen, m.MarketOpenedDatetime, m.MarketSettled, m.MarketSettledDateTime,
			m.MarketSettledDayKey,m.MarketOpenedDayKey, m.MarketClosedDayKey, m.LiveMarket, m.FutureMarket, m.MaxWinnings, m.MaxStake,  m.LevyRate, m.Sport, m.FromDate, m.FromDate FirstDate, 
			CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, 'Sp_DimMarket' CreatedBy, CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey, 'Sp_DimMarket' [ModifiedBy]
	FROM 	#Market m
	WHERE	m.Upsert = 'I'
	option(recompile);

EXEC [Control].Sp_Log @BatchKey, 'Sp_DimMarket', NULL, 'Success', @RowsProcessed = @@ROWCOUNT

END TRY
BEGIN CATCH

	declare @Error int = ERROR_NUMBER();
	declare @ErrorMessage varchar(max) =  ERROR_MESSAGE();
	
	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimMarket', NULL, 'Failed', @ErrorMessage;

	THROW;
	
END CATCH