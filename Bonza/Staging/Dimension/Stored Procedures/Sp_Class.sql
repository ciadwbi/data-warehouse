﻿CREATE PROCEDURE [Dimension].[Sp_Class]
(
	@BatchKey	INT,
	@FromDate	DATETIME2(3),
	@ToDate		DATETIME2(3),
	@Increment		int = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int
	DECLARE @i int

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'EventClass','Start', @Reload

	SELECT	'IBT' Source,
			CONVERT(varchar(32),x.EventType) ClassId,
			e.Description SourceClass, 
			CONVERT(varchar(32),x.SportSubType) SubClassId, 
			s.Description SourceSubClass,
			CONVERT(varchar(255),ve.Description) Keyword,
			COALESCE(CASE WHEN s.FromDate > e.FromDate THEN s.FromDate ELSE e.FromDate END, e.FromDate, s.FromDate, '1900-01-01') FromDate
	INTO	#Events
	FROM	(	SELECT	z.EventType, z.SportSubType, v.EventType VenueEventType, MIN(z.FromDate) FromDate
				FROM	[$(Acquisition)].[IntraBet].tblEvents z
						LEFT JOIN [$(Acquisition)].[IntraBet].tblLUVenues v ON v.VenueID = z.VenueID AND v.FromDate <= z.FromDate AND v.ToDate > z.FromDate
				WHERE	z.FromDate > @FromDate AND z.FromDate <= @ToDate AND z.ToDate > @FromDate
						AND ISNULL(z.IsMasterEvent,0) = 0
						AND REPLACE(z.EventName,'X','') <> ''
				GROUP BY z.SportSubType, z.EventType, v.EventType
				HAVING	MAX(z.ToDate) > @ToDate -- only worry about combinations that persist beyond the current ETL batch (we get a lot of temporary edits that only last for seconds or minutes)
			) x
			-- A bit metadata anal - pull out the last LU records for when the (Sub)Class pair was first seen in the period to use as FromDate 
			-- If the names have subsequently changed in the ETL period, that will be picked up by the subsequent (Sub)Class changes
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUEventTypes e ON e.EventType = x.EventType AND e.FromDate <= x.FromDate AND e.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUSubSportTypes s ON s.SportSubType = x.SportSubType AND s.FromDate <= x.FromDate AND s.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUEventTypes ve ON ve.EventType = x.VenueEventType AND ve.FromDate <= x.FromDate AND ve.ToDate > x.FromDate
	OPTION (RECOMPILE, TABLE HINT(z, FORCESEEK([NI_tblEvents_FD(A)](FromDate))))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'LotteryClass','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	INSERT	#Events
	SELECT	'LOT' Source,
			'Lottery' ClassId,
			'Lottery' SourceClass, 
			Product SubClassId, 
			Product SourceSubClass,
			NULL Keyword,
			MIN(FromDate) FromDate
	FROM	Cache.TransactionLottery
	WHERE	FromDate > @FromDate AND FromDate <= @ToDate
	GROUP BY Product
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'ChangedSubClass','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	SELECT	CONVERT(varchar(32),SportSubType) SubClassId, FromDate, Description SourceSubClass
	INTO	#Subclasses
	FROM	(	SELECT	SportSubType, FromDate, Description, RANK() OVER (PARTITION BY SportSubType ORDER BY FromDate DESC) r
				FROM	(	SELECT	SportSubType, FromDate, Description, 1 trg
							FROM	[$(Acquisition)].[IntraBet].tblLUSubSportTypes
							WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
							UNION ALL
							SELECT	SportSubType, ToDate, Description, 2 trg
							FROM	[$(Acquisition)].[IntraBet].tblLUSubSportTypes
							WHERE	ToDate > @FromDate AND ToDate <= @ToDate
						) x
				GROUP BY SportSubType, FromDate, Description
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
			) x
	WHERE	r = 1
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'ChangedClass','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	SELECT	CONVERT(varchar(32),EventType) ClassId, FromDate, Description SourceClass
	INTO	#Classes
	FROM	(	SELECT	EventType, FromDate, Description, RANK() OVER (PARTITION BY EventType ORDER BY FromDate DESC) r
				FROM	(	SELECT	EventType, FromDate, Description, 1 trg
							FROM	[$(Acquisition)].[IntraBet].tblLUEventTypes
							WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
							UNION ALL
							SELECT	EventType, ToDate, Description, 2 trg
							FROM	[$(Acquisition)].[IntraBet].tblLUEventTypes
							WHERE	ToDate > @FromDate AND ToDate <= @ToDate
						) x
				GROUP BY EventType, FromDate, Description
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
			) x
	WHERE	r = 1
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
	
	SELECT	s.*, CASE WHEN d.HashKey IS NULL THEN 'I' ELSE 'X' END Upsert
	INTO	#Class
	FROM	(SELECT HASHBYTES('MD5','|'+ClassId+'|'+ISNULL(SubClassId,'')+'|'+ISNULL(Keyword,'')+'|') HashKey, * FROM #Events) s
			LEFT JOIN [$(Dimensional)].CC.Class d ON d.HashKey = s.HashKey
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	INSERT	[$(Dimensional)].CC.Class
		(	HashKey, Source, Keyword, 
			Class_SubClassKey, SubClassId, SourceSubClass, SubClassCode, SubClass, 
			Class_ClassKey, ClassId, SourceClass, ClassCode, Class, ClassIcon, 
			Class_SuperClassKey, SuperClass,
			FirstDate, FromDate, ToDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy, Increment
		)
	SELECT	HashKey, Source, Keyword, 
			-2 Class_SubclassKey, SubClassId, SourceSubClass, 'PDG' SubClassCode, 'Pending' SubClass, 
			-2 Class_ClassKey, ClassId, SourceClass, 'PDG' ClassCode, 'Pending' Class, 0x ClassIcon, 
			-2 Class_SuperClassKey, 'Pending' SuperClass,
			FromDate FirstDate, FromDate, '9999-12-31' ToDate, CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy, @Increment Increment
	FROM	#Class
	WHERE	Upsert = 'I'
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @Reload, @RowsProcessed = @RowCount

	UPDATE	d
	SET		SourceClass = ISNULL(c.SourceClass, d.SourceClass),
			SourceSubClass = ISNULL(s.SourceSubClass, d.SourceSubClass),
			FromDate = CASE WHEN c.FromDate > d.FromDate AND c.FromDate > ISNULL(s.FromDate,'1900-01-01') THEN c.FromDate WHEN s.FromDate > d.FromDate THEN s.FromDate ELSE d.FromDate END, -- biggest of the FromDates, taking into account that either of the new ones could be NULL
			ModifiedDate = CURRENT_TIMESTAMP,
			ModifiedBatchKey = @BatchKey,
			ModifiedBy = @Me,
			Increment = @Increment
	FROM	[$(Dimensional)].CC.Class d 
			LEFT JOIN #Classes c ON c.ClassId = d.ClassId
			LEFT JOIN #Subclasses s ON s.SubClassId = d.SubClassId
	WHERE	(c.ClassId IS NOT NULL OR s.SubClassId IS NOT NULL)
			AND	(	ISNULL(d.SourceClass,'!') <> COALESCE(c.SourceClass, d.SourceClass, '!')
					OR ISNULL(d.SourceSubClass,'!') <> COALESCE(s.SourceSubClass, d.SourceSubClass, '!')
				)
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount	

	Update [Control].[ConfigurationItems] Set Value = 'N' Where ConfigurationItem = @Me

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
