CREATE PROCEDURE [Dimension].[Sp_Structure]
(
	@BatchKey		int,
	@FromDate		datetime2(3),
	@ToDate			datetime2(3),
	@Increment		int = NULL,
	@DryRun			bit = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Message varchar(255) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount INT

	DECLARE @FY2014Start datetime2(3)	= CONVERT(datetime2(3),'2014-01-01 00:00:00.000')
	DECLARE @FY2015Start datetime2(3)	= CONVERT(datetime2(3),'2014-12-31 00:00:00.000')
	DECLARE @FY2016Start datetime2(3)	= CONVERT(datetime2(3),'2015-12-30 00:00:00.000')
	DECLARE @FY2017Start datetime2(3)	= CONVERT(datetime2(3),'2016-12-28 00:00:00.000')
	DECLARE @FY2018Start datetime2(3)	= CONVERT(datetime2(3),'2017-12-27 00:00:00.000')
	DECLARE @FY2019Start datetime2(3)	= CONVERT(datetime2(3),'2019-01-02 00:00:00.000')
	DECLARE @FY2014End datetime2(3)		= CASE WHEN @ToDate BETWEEN @FY2014Start AND @FY2015Start THEN @ToDate ELSE @FY2015Start END
	DECLARE @FY2015End datetime2(3)		= CASE WHEN @ToDate BETWEEN @FY2015Start AND @FY2016Start THEN @ToDate ELSE @FY2016Start END
	DECLARE @FY2016End datetime2(3)		= CASE WHEN @ToDate BETWEEN @FY2016Start AND @FY2017Start THEN @ToDate ELSE @FY2017Start END
	DECLARE @FY2017End datetime2(3)		= CASE WHEN @ToDate BETWEEN @FY2017Start AND @FY2018Start THEN @ToDate ELSE @FY2018Start END
	DECLARE @FY2018End datetime2(3)		= CASE WHEN @ToDate BETWEEN @FY2018Start AND @FY2019Start THEN @ToDate ELSE @FY2019Start END

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT

BEGIN TRY

	---------------------------------------------------- Get unique ClientID and latest FromDate for all pertinent changes within batch window --------------------------------------

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Introducer Changes','Start',@Message

	SELECT	ClientId, FromDate
	INTO	#Introducers
	FROM	(	SELECT ClientId, FromDate, IntroducedName FROM [$(Acquisition)].IntraBet.tblIntroducers WHERE FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
				UNION ALL
				SELECT ClientId, ToDate, IntroducedName FROM [$(Acquisition)].IntraBet.tblIntroducers WHERE ToDate > @FromDate AND ToDate <= @ToDate
			) x
	GROUP BY ClientId, FromDate, IntroducedName
	HAVING COUNT(*) <> 2
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Manager Changes','Start',@Message, @RowsProcessed = @@ROWCOUNT

	SELECT	ClientId, FromDate
	INTO	#Managers
	FROM	(	SELECT ClientId, FromDate, IntroducedName, CanManageVIP, CanManageBDM FROM [$(Acquisition)].IntraBet.tblIntroducers WHERE FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
				UNION ALL
				SELECT ClientId, ToDate, IntroducedName, CanManageVIP, CanManageBDM FROM [$(Acquisition)].IntraBet.tblIntroducers WHERE ToDate > @FromDate AND ToDate <= @ToDate
			) x
	GROUP BY ClientId, FromDate, IntroducedName, CanManageVIP, CanManageBDM
	HAVING COUNT(*) <> 2
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Handler Changes','Start',@Message, @RowsProcessed = @@ROWCOUNT

	SELECT	DebtHandlerId, FromDate
	INTO	#Handlers
	FROM	(	SELECT DebtHandlerId, FromDate, HandlerName FROM [$(Acquisition)].IntraBet.tblLUDebtHandlers WITH(FORCESEEK) WHERE FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
				UNION ALL
				SELECT DebtHandlerId, ToDate, HandlerName FROM [$(Acquisition)].IntraBet.tblLUDebtHandlers WITH(FORCESEEK) WHERE ToDate > @FromDate AND ToDate <= @ToDate
			) x
	GROUP BY DebtHandlerId, FromDate, HandlerName
	HAVING COUNT(*) <> 2
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Client Changes','Start',@Message, @RowsProcessed = @@ROWCOUNT

	SELECT	ClientId, MAX(FromDate) FromDate, MIN(FromDate) FirstDate
	INTO	#Candidates
	FROM	(	SELECT	ClientId, FromDate
				FROM	(	SELECT ClientId, FromDate, OrgId FROM [$(Acquisition)].IntraBet.tblClients WITH(FORCESEEK) WHERE FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
							UNION ALL
							SELECT ClientId, ToDate, OrgId FROM [$(Acquisition)].IntraBet.tblClients WITH(FORCESEEK) WHERE ToDate > @FromDate AND ToDate <= @ToDate
						) x
				GROUP BY ClientId, FromDate, OrgId
				HAVING COUNT(*) <> 2
				UNION ALL
				SELECT	ClientId, FromDate
				FROM	(	SELECT ClientId, FromDate, IntroducedBy, ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WITH(FORCESEEK) WHERE FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
							UNION ALL
							SELECT ClientId, ToDate, IntroducedBy, ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WITH(FORCESEEK) WHERE ToDate > @FromDate AND ToDate <= @ToDate
						) x
				GROUP BY ClientId, FromDate, IntroducedBy, ManagedBy, Handled, VIP
				HAVING COUNT(*) <> 2
				UNION ALL
				SELECT	c.ClientId, i.FromDate
				FROM	[$(Acquisition)].IntraBet.tblClientInfo c
						INNER JOIN #Introducers i ON (i.ClientId = c.IntroducedBy AND i.FromDate >= c.FromDate AND i.FromDate < c.ToDate)
				UNION ALL
				SELECT	c.ClientId, i.FromDate
				FROM	[$(Acquisition)].IntraBet.tblClientInfo c
						INNER JOIN #Managers i ON (i.ClientId = c.IntroducedBy AND i.FromDate >= c.FromDate AND i.FromDate < c.ToDate)
				UNION ALL
				SELECT	c.ClientId, h.FromDate
				FROM	[$(Acquisition)].IntraBet.tblClientInfo c
						INNER JOIN #Handlers h ON (h.DebtHandlerId = c.IntroducedBy AND h.FromDate >= c.FromDate AND h.FromDate < c.ToDate)
				UNION ALL
				SELECT	ClientId, FromDate
				FROM	[$(Cache_Intrabet)].IntraBet.tblAccounts_First WITH(FORCESEEK)
				WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
			) x
	GROUP BY ClientId
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Current Attributes','Start',@Message, @RowsProcessed = @@ROWCOUNT

	DROP TABLE #Introducers
	DROP TABLE #Handlers

	SELECT	x.ClientId, x.FromDate, x.FirstDate, ISNULL(c.OrgID,0) OrgId, ISNULL(i.IntroducedBy,0) IntroducedBy, ISNULL(i.ManagedBy,0) ManagedBy, ISNULL(i.Handled,0) Handled, ISNULL(i.VIP,0) VIPType
	INTO	#Current
	FROM	#Candidates x
			INNER JOIN [$(Acquisition)].IntraBet.tblClients c ON (c.ClientID = x.ClientID AND c.FromDate <= x.FromDate AND c.ToDate > x.FromDate)
			INNER JOIN [$(Acquisition)].IntraBet.tblClientInfo i ON (i.ClientID = x.ClientID AND i.FromDate <= x.FromDate AND i.ToDate > x.FromDate)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Previous Organisation','Start',@Message, @RowsProcessed = @@ROWCOUNT

	DROP TABLE #Candidates

	SELECT	ClientId, FromDate, FirstDate,
			OrgId, 
			CASE PreviousOrgId WHEN OrgId THEN AbsoluteFromDate ELSE OrgIdFromDate END OrgIdFromDate,
			CASE PreviousOrgId WHEN OrgId THEN NULL ELSE PreviousOrgId END PreviousOrgId,
			IntroducedBy, ManagedBy, Handled, VIPType
	INTO	#PreviousOrg
	FROM	(	SELECT	DISTINCT x.ClientId, x.FromDate, x.FirstDate, x.OrgID, x.IntroducedBy, x.ManagedBy, x.Handled, x.VIPType,
						FIRST_VALUE(c.ToDate) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.OrgId,0) <> x.OrgId THEN 1 ELSE 2 END, c.FromDate DESC) OrgIdFromDate,
						FIRST_VALUE(ISNULL(c.OrgID,0)) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.OrgId,0) <> x.OrgId THEN 1 ELSE 2 END, c.FromDate DESC) PreviousOrgId,
						FIRST_VALUE(c.FromDate) OVER (PARTITION BY x.ClientId ORDER BY c.FromDate) AbsoluteFromDate
				FROM	#Current x
						INNER JOIN [$(Acquisition)].IntraBet.tblClients c ON (c.ClientID = x.ClientID AND c.FromDate <= x.FromDate)
			) x
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Previous Attributes','Start',@Message, @RowsProcessed = @@ROWCOUNT

	DROP TABLE #Current

	SELECT	ClientId, FromDate, FirstDate, OrgId, OrgIdFromDate, PreviousOrgId, 
			IntroducedBy, 
			CASE PreviousIntroducedBy WHEN IntroducedBy THEN AbsoluteFromDate ELSE IntroducedByFromDate END IntroducedByFromDate,
			CASE PreviousIntroducedBy WHEN IntroducedBy THEN NULL ELSE PreviousIntroducedBy END PreviousIntroducedBy,
			ManagedBy, 
			CASE PreviousManagedBy WHEN ManagedBy THEN AbsoluteFromDate ELSE ManagedByFromDate END ManagedByFromDate,
			CASE PreviousManagedBy WHEN ManagedBy THEN NULL ELSE PreviousManagedBy END PreviousManagedBy,
			Handled, 
			CASE PreviousHandled WHEN Handled THEN AbsoluteFromDate ELSE HandledFromDate END HandledFromDate,
			CASE PreviousHandled WHEN Handled THEN NULL ELSE PreviousHandled END PreviousHandled,
			VIPType,
			CASE PreviousVIPType WHEN VIPType THEN AbsoluteFromDate ELSE VIPTypeFromDate END VIPTypeFromDate,
			CASE PreviousVIPType WHEN VIPType THEN NULL ELSE PreviousVIPType END PreviousVIPType
	INTO	#Previous
	FROM	(	SELECT	DISTINCT x.ClientId, x.FromDate, x.FirstDate, x.OrgID, x.OrgIdFromDate, x.PreviousOrgId, x.IntroducedBy, x.ManagedBy, x.Handled, x.VIPType,
						FIRST_VALUE(c.ToDate) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.IntroducedBy,0) <> x.IntroducedBy THEN 1 ELSE 2 END, c.FromDate DESC) IntroducedByFromDate,
						FIRST_VALUE(ISNULL(c.IntroducedBy,0)) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.IntroducedBy,0) <> x.IntroducedBy THEN 1 ELSE 2 END, c.FromDate DESC) PreviousIntroducedBy,
						FIRST_VALUE(c.ToDate) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.ManagedBy,0) <> x.ManagedBy THEN 1 ELSE 2 END, c.FromDate DESC) ManagedByFromDate,
						FIRST_VALUE(ISNULL(c.ManagedBy,0)) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.ManagedBy,0) <> x.ManagedBy THEN 1 ELSE 2 END, c.FromDate DESC) PreviousManagedBy,
						FIRST_VALUE(c.ToDate) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.Handled,0) <> x.Handled THEN 1 ELSE 2 END, c.FromDate DESC) HandledFromDate,
						FIRST_VALUE(ISNULL(c.Handled,0)) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.Handled,0) <> x.Handled THEN 1 ELSE 2 END, c.FromDate DESC) PreviousHandled,
						FIRST_VALUE(c.ToDate) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.VIP,0) <> x.VIPType THEN 1 ELSE 2 END, c.FromDate DESC) VIPTypeFromDate,
						FIRST_VALUE(ISNULL(c.VIP,0)) OVER (PARTITION BY x.ClientId ORDER BY CASE WHEN ISNULL(c.VIP,0) <> x.VIPType THEN 1 ELSE 2 END, c.FromDate DESC) PreviousVIPType,
						FIRST_VALUE(c.FromDate) OVER (PARTITION BY x.ClientId ORDER BY c.FromDate) AbsoluteFromDate
				FROM	#PreviousOrg x
						LEFT JOIN [$(Acquisition)].IntraBet.tblClientInfo c ON (c.ClientID = x.ClientID AND c.FromDate <= x.FromDate)
			) x
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Financial Year Attributes','Start',@Message, @RowsProcessed = @@ROWCOUNT

	DROP TABLE #PreviousOrg

	SELECT	x.ClientId, x.FromDate, x.FirstDate,
			x.OrgID, 
			x.OrgIdFromDate,
			x.PreviousOrgId,
			c2014.OrgID FY2014OrgId,
			c2015.OrgID FY2015OrgId,
			c2016.OrgID FY2016OrgId,
			c2017.OrgID FY2017OrgId,
			c2018.OrgID FY2018OrgId,
			x.IntroducedBy, 
			x.IntroducedByFromDate,
			x.PreviousIntroducedBy,
			i2014.IntroducedBy FY2014IntroducedBy,
			i2015.IntroducedBy FY2015IntroducedBy,
			i2016.IntroducedBy FY2016IntroducedBy,
			i2017.IntroducedBy FY2017IntroducedBy,
			i2018.IntroducedBy FY2018IntroducedBy,
			x.ManagedBy, 
			x.ManagedByFromDate,
			x.PreviousManagedBy,
			i2014.ManagedBy FY2014ManagedBy,
			i2015.ManagedBy FY2015ManagedBy,
			i2016.ManagedBy FY2016ManagedBy,
			i2017.ManagedBy FY2017ManagedBy,
			i2018.ManagedBy FY2018ManagedBy,
			x.Handled, 
			x.HandledFromDate,
			x.PreviousHandled,
			i2014.Handled FY2014Handled,
			i2015.Handled FY2015Handled,
			i2016.Handled FY2016Handled,
			i2017.Handled FY2017Handled,
			i2018.Handled FY2018Handled,
			x.VIPType,
			x.VIPTypeFromDate,
			x.PreviousVIPType,
			i2014.VIP FY2014VIPType,
			i2015.VIP FY2015VIPType,
			i2016.VIP FY2016VIPType,
			i2017.VIP FY2017VIPType,
			i2018.VIP FY2018VIPType
	INTO	#FY
	FROM	#Previous x
			LEFT JOIN	(SELECT ClientId, OrgId FROM [$(Acquisition)].IntraBet.tblClients WHERE FromDate <= @FY2014End AND ToDate > @FY2014End) c2014 ON (c2014.ClientID = x.ClientID)
			LEFT JOIN	(	SELECT	DISTINCT p.ClientId, FIRST_VALUE(c.OrgId) OVER (PARTITION BY p.ClientId ORDER BY c.FromDate) OrgId -- For 2015, if account was ever TW, that takes precedence, otherwise default to what the OrgId was at fiscal year end. The WHERE clause will ensure that only these 2 possibilities are returned; TW will always be earlier of any multiple records found by FromDate
							FROM	#Previous p
									INNER JOIN [$(Acquisition)].IntraBet.tblClients c ON (c.ClientID = p.ClientID)
							WHERE	c.FromDate <= @FY2015End
									AND c.ToDate > @FY2015Start
									AND ((c.FromDate < @FY2015End AND c.ToDate >= @FY2015End) OR c.OrgID = 8)
						) c2015 ON (c2015.ClientId = x.ClientId)
			LEFT JOIN	(	SELECT	DISTINCT p.ClientId, FIRST_VALUE(c.OrgId) OVER (PARTITION BY p.ClientId ORDER BY c.FromDate) OrgId -- As with TW in 2015, for 2016, if account was ever CBT, that takes precedence, otherwise default to what the OrgId was at fiscal year end. The WHERE clause will ensure that only these 2 possibilities are returned; CBT will always be earlier of any multiple records found by FromDate
							FROM	#Previous p
									INNER JOIN [$(Acquisition)].IntraBet.tblClients c ON (c.ClientID = p.ClientID)
							WHERE	c.FromDate < @FY2016End
									AND c.ToDate >= @FY2016Start
									AND ((c.FromDate < @FY2016End AND c.ToDate >= @FY2016End) OR c.OrgID = 2) -- get all versions of orgid for each client for the year, if CBT (orgid = 2) then take the earliest using analytic function above, otherwise further limit the selection to whatever the final value for the year was
						) c2016 ON (c2016.ClientID = x.ClientID)
			LEFT JOIN	(SELECT ClientId, OrgId FROM [$(Acquisition)].IntraBet.tblClients WHERE FromDate <= @FY2017End AND ToDate > @FY2017End) c2017 ON (c2017.ClientID = x.ClientID)
			LEFT JOIN	(SELECT ClientId, OrgId FROM [$(Acquisition)].IntraBet.tblClients WHERE FromDate <= @FY2018End AND ToDate > @FY2018End) c2018 ON (c2018.ClientID = x.ClientID)
			LEFT JOIN	(SELECT ClientId, IntroducedBy, ISNULL(ManagedBy,0) ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WHERE FromDate < @FY2014End AND ToDate >= @FY2014End) i2014 ON (i2014.ClientID = x.ClientID)
			LEFT JOIN	(SELECT ClientId, IntroducedBy, ISNULL(ManagedBy,0) ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WHERE FromDate < @FY2015End AND ToDate >= @FY2015End) i2015 ON (i2015.ClientID = x.ClientID)
			LEFT JOIN	(SELECT ClientId, IntroducedBy, ISNULL(ManagedBy,0) ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WHERE FromDate < @FY2016End AND ToDate >= @FY2016End) i2016 ON (i2016.ClientID = x.ClientID)-- DISTINCT added to eliminate records with incorrect ToDates in Acquisition for couple of records without affecting performance much
			LEFT JOIN	(SELECT ClientId, IntroducedBy, ISNULL(ManagedBy,0) ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WHERE FromDate < @FY2017End AND ToDate >= @FY2017End) i2017 ON (i2017.ClientID = x.ClientID)
			LEFT JOIN	(SELECT ClientId, IntroducedBy, ISNULL(ManagedBy,0) ManagedBy, Handled, VIP FROM [$(Acquisition)].IntraBet.tblClientInfo WHERE FromDate < @FY2018End AND ToDate >= @FY2018End) i2018 ON (i2018.ClientID = x.ClientID)
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Assemble','Start',@Message, @RowsProcessed = @@ROWCOUNT

	DROP TABLE #Previous

	SELECT	a.AccountID LedgerId, 'IAA' LedgerSource, y.HeadlineFeatures, y.InPlay, x.ClientId AccountId, 
			x.OrgId, x.OrgIdFromDate, x.PreviousOrgId, x.FY2014OrgId, x.FY2015OrgId, x.FY2016OrgId, x.FY2017OrgId, x.FY2018OrgId,
			x.IntroducedBy IntroducerId, x.IntroducedByFromDate IntroducerFromDate, x.PreviousIntroducedBy PreviousIntroducerId, x.FY2014IntroducedBy FY2014IntroducerId, x.FY2015IntroducedBy FY2015IntroducerId, x.FY2016IntroducedBy FY2016IntroducerId, x.FY2017IntroducedBy FY2017IntroducerId, x.FY2018IntroducedBy FY2018IntroducerId,
			i.IntroducedName Introducer, ip.IntroducedName PreviousIntroducer, i14.IntroducedName FY2014Introducer, i15.IntroducedName FY2015Introducer, i16.IntroducedName FY2016Introducer, i17.IntroducedName FY2017Introducer, i18.IntroducedName FY2018Introducer,
			x.ManagedBy ManagerId, x.ManagedByFromDate ManagerFromDate, x.PreviousManagedBy PreviousManagerId, x.FY2014ManagedBy FY2014ManagerId, x.FY2015ManagedBy FY2015ManagerId, x.FY2016ManagedBy FY2016ManagerId, x.FY2017ManagedBy FY2017ManagerId, x.FY2018ManagedBy FY2018ManagerId,
			m.IntroducedName Manager, mp.IntroducedName PreviousManager, m14.IntroducedName FY2014Manager, m15.IntroducedName FY2015Manager, m16.IntroducedName FY2016Manager, m17.IntroducedName FY2017Manager, m18.IntroducedName FY2018Manager,
			m.CanManageVIP, mp.CanManageVIP PreviousCanManageVIP, m14.CanManageVIP FY2014CanManageVIP, m15.CanManageVIP FY2015CanManageVIP, m16.CanManageVIP FY2016CanManageVIP, m17.CanManageVIP FY2017CanManageVIP, m18.CanManageVIP FY2018CanManageVIP,
			m.CanManagePotentialVIP, mp.CanManagePotentialVIP PreviousCanManagePotentialVIP, m14.CanManagePotentialVIP FY2014CanManagePotentialVIP, m15.CanManagePotentialVIP FY2015CanManagePotentialVIP, m16.CanManagePotentialVIP FY2016CanManagePotentialVIP, m17.CanManagePotentialVIP FY2017CanManagePotentialVIP, m18.CanManagePotentialVIP FY2018CanManagePotentialVIP,
			m.CanManageBDM, mp.CanManageBDM PreviousCanManageBDM, m14.CanManageBDM FY2014CanManageBDM, m15.CanManageBDM FY2015CanManageBDM, m16.CanManageBDM FY2016CanManageBDM, m17.CanManageBDM FY2017CanManageBDM, m18.CanManageBDM FY2018CanManageBDM,
			x.Handled HandlerId, x.HandledFromDate HandlerFromDate, x.PreviousHandled PreviousHandlerId, x.FY2014Handled FY2014HandlerId, x.FY2015Handled FY2015HandlerId, x.FY2016Handled FY2016HandlerId, x.FY2017Handled FY2017HandlerId, x.FY2018Handled FY2018HandlerId,
			h.HandlerName Handler, hp.HandlerName PreviousHandler, h14.HandlerName FY2014Handler, h15.HandlerName FY2015Handler, h16.HandlerName FY2016Handler, h17.HandlerName FY2017Handler, h18.HandlerName FY2018Handler,
			x.VIPType, x.VIPTypeFromDate, x.PreviousVIPType, x.FY2014VIPType, x.FY2015VIPType, x.FY2016VIPType, x.FY2017VIPType, x.FY2018VIPType, 
			x.FirstDate, x.FromDate
	INTO	#Structure
	FROM	#FY x
			INNER JOIN [$(Cache_Intrabet)].IntraBet.tblAccounts_NonVolatile a ON (a.ClientID = x.ClientID AND a.FromDate <= x.FromDate AND a.ToDate > x.FromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers i ON (i.ClientID = x.IntroducedBy AND i.FromDate <= x.FromDate AND i.ToDate > x.FromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers ip ON (ip.ClientID = x.PreviousIntroducedBy AND ip.FromDate <= x.IntroducedByFromDate AND ip.ToDate > x.IntroducedByFromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers i14 ON (i14.ClientID = x.FY2014IntroducedBy AND i14.FromDate < CONVERT(datetime2(3),'2014-12-31') AND i14.ToDate >= CONVERT(datetime2(3),'2014-12-31'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers i15 ON (i15.ClientID = x.FY2015IntroducedBy AND i15.FromDate < CONVERT(datetime2(3),'2015-12-30') AND i15.ToDate >= CONVERT(datetime2(3),'2015-12-30'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers i16 ON (i16.ClientID = x.FY2016IntroducedBy AND i16.FromDate < CONVERT(datetime2(3),'2016-12-28') AND i16.ToDate >= CONVERT(datetime2(3),'2016-12-28'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers i17 ON (i17.ClientID = x.FY2017IntroducedBy AND i17.FromDate < CONVERT(datetime2(3),'2017-12-27') AND i17.ToDate >= CONVERT(datetime2(3),'2017-12-27'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers i18 ON (i18.ClientID = x.FY2018IntroducedBy AND i18.FromDate < CONVERT(datetime2(3),'2018-12-26') AND i18.ToDate >= CONVERT(datetime2(3),'2018-12-26'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers m ON (m.ClientID = x.ManagedBy AND m.FromDate <= x.FromDate AND m.ToDate > x.FromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers mp ON (mp.ClientID = x.PreviousManagedBy AND mp.FromDate <= x.ManagedByFromDate AND mp.ToDate > x.ManagedByFromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers m14 ON (m14.ClientID = x.FY2014ManagedBy AND m14.FromDate < CONVERT(datetime2(3),'2014-12-31') AND m14.ToDate >= CONVERT(datetime2(3),'2014-12-31'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers m15 ON (m15.ClientID = x.FY2015ManagedBy AND m15.FromDate < CONVERT(datetime2(3),'2015-12-30') AND m15.ToDate >= CONVERT(datetime2(3),'2015-12-30'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers m16 ON (m16.ClientID = x.FY2016ManagedBy AND m16.FromDate < CONVERT(datetime2(3),'2016-12-28') AND m16.ToDate >= CONVERT(datetime2(3),'2016-12-28'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers m17 ON (m17.ClientID = x.FY2017ManagedBy AND m17.FromDate < CONVERT(datetime2(3),'2017-12-27') AND m17.ToDate >= CONVERT(datetime2(3),'2017-12-27'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblIntroducers m18 ON (m18.ClientID = x.FY2018ManagedBy AND m18.FromDate < CONVERT(datetime2(3),'2018-12-26') AND m18.ToDate >= CONVERT(datetime2(3),'2018-12-26'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers h ON (h.DebtHandlerID = x.Handled AND h.FromDate <= x.FromDate AND h.ToDate > x.FromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers hp ON (hp.DebtHandlerID = x.PreviousHandled AND hp.FromDate <= x.HandledFromDate AND hp.ToDate > x.HandledFromDate)
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers h14 ON (h14.DebtHandlerID = x.FY2014Handled AND h14.FromDate < CONVERT(datetime2(3),'2014-12-31') AND h14.ToDate >= CONVERT(datetime2(3),'2014-12-31'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers h15 ON (h15.DebtHandlerID = x.FY2015Handled AND h15.FromDate < CONVERT(datetime2(3),'2015-12-30') AND h15.ToDate >= CONVERT(datetime2(3),'2015-12-30'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers h16 ON (h16.DebtHandlerID = x.FY2016Handled AND h16.FromDate < CONVERT(datetime2(3),'2016-12-28') AND h16.ToDate >= CONVERT(datetime2(3),'2016-12-28'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers h17 ON (h17.DebtHandlerID = x.FY2017Handled AND h17.FromDate < CONVERT(datetime2(3),'2017-12-27') AND h17.ToDate >= CONVERT(datetime2(3),'2017-12-27'))
			LEFT HASH JOIN [$(Acquisition)].IntraBet.tblLUDebtHandlers h18 ON (h18.DebtHandlerID = x.FY2018Handled AND h18.FromDate < CONVERT(datetime2(3),'2018-12-26') AND h18.ToDate >= CONVERT(datetime2(3),'2018-12-26'))
			CROSS APPLY (	SELECT 'N' InPlay, 0 HeadlineFeatures UNION ALL -- Normal transactions
							SELECT 'Y' InPlay, 1 HeadlineFeatures UNION ALL -- InPlay transactions
							SELECT 'N' InPlay, 2 HeadlineFeatures) y		-- Gaming transactions
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Stage','Start',@Message, @RowsProcessed = @@ROWCOUNT

	DROP TABLE #FY

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT	CASE	 -- temporary measure to create structure dimension against computed key in the fact table to avoid reload
				WHEN s.HeadlineFeatures = 2 THEN 100000000 + a.AccountKey
				WHEN s.HeadlineFeatures = 1 THEN - a.AccountKey 
				ELSE a.AccountKey 
			END StructureKey,
			s.*
	INTO	#Stage
	FROM	#Structure s
			INNER JOIN [$(Dimensional)].Dimension.Account a ON (a.LedgerId = s.LedgerId AND a.LedgerSource = s.LedgerSource)  -- NOTE - whilst the Structure dimension is implemented as a fix, the Account Dimension must already be loaded to donate the AccountKey for new records - hence the INNER JOIN
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log @BatchKey,@Me,'Dimension','Start',@Message, @RowsProcessed = @@ROWCOUNT

	SELECT	StructureKey, d.LedgerId, d.LedgerSource, d.HeadlineFeatures, d.InPlay, AccountId, 
			OrgId, OrgIdFromDate, PreviousOrgId, FY2014OrgId, FY2015OrgId, FY2016OrgId, FY2017OrgId, FY2018OrgId,
			IntroducerId, IntroducerFromDate, PreviousIntroducerId, FY2014IntroducerId, FY2015IntroducerId, FY2016IntroducerId, FY2017IntroducerId, FY2018IntroducerId,
			Introducer, PreviousIntroducer, FY2014Introducer, FY2015Introducer, FY2016Introducer, FY2017Introducer, FY2018Introducer,
			ManagerId, ManagerFromDate, PreviousManagerId, FY2014ManagerId, FY2015ManagerId, FY2016ManagerId, FY2017ManagerId, FY2018ManagerId,
			Manager, PreviousManager, FY2014Manager, FY2015Manager, FY2016Manager, FY2017Manager, FY2018Manager,
			CanManageVIP, PreviousCanManageVIP, FY2014CanManageVIP, FY2015CanManageVIP, FY2016CanManageVIP, FY2017CanManageVIP, FY2018CanManageVIP,
			CanManagePotentialVIP, PreviousCanManagePotentialVIP, FY2014CanManagePotentialVIP, FY2015CanManagePotentialVIP, FY2016CanManagePotentialVIP, FY2017CanManagePotentialVIP, FY2018CanManagePotentialVIP,
			CanManageBDM, PreviousCanManageBDM, FY2014CanManageBDM, FY2015CanManageBDM, FY2016CanManageBDM, FY2017CanManageBDM, FY2018CanManageBDM,
			HandlerId, HandlerFromDate, PreviousHandlerId, FY2014HandlerId, FY2015HandlerId, FY2016HandlerId, FY2017HandlerId, FY2018HandlerId,
			Handler, PreviousHandler, FY2014Handler, FY2015Handler, FY2016Handler, FY2017Handler, FY2018Handler,
			VIPType, VIPTypeFromDate, PreviousVIPType, FY2014VIPType, FY2015VIPType, FY2016VIPType, FY2017VIPType,  FY2018VIPType, 
			FirstDate, FromDate
	INTO	#Dimension
	FROM	[$(Dimensional)].Dimension.Structure d
			INNER JOIN (SELECT DISTINCT LedgerId, LedgerSource, HeadlineFeatures FROM #Stage) s ON s.LedgerId = d.LedgerID AND s.LedgerSource = d.LedgerSource AND s.HeadlineFeatures = d.HeadlineFeatures
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Differences','Start',@Message,@RowsProcessed = @@ROWCOUNT

	SELECT	MAX(MAX(StructureKey)) OVER (PARTITION BY LedgerId, LedgerSource, HeadlineFeatures) StructureKey, 
			LedgerId, LedgerSource, HeadlineFeatures, InPlay, AccountId, 
			OrgId, OrgIdFromDate, PreviousOrgId, FY2014OrgId, FY2015OrgId, FY2016OrgId, FY2017OrgId, FY2018OrgId,
			IntroducerId, IntroducerFromDate, PreviousIntroducerId, FY2014IntroducerId, FY2015IntroducerId, FY2016IntroducerId, FY2017IntroducerId, FY2018IntroducerId,
			Introducer, PreviousIntroducer, FY2014Introducer, FY2015Introducer, FY2016Introducer, FY2017Introducer, FY2018Introducer,
			ManagerId, ManagerFromDate, PreviousManagerId, FY2014ManagerId, FY2015ManagerId, FY2016ManagerId, FY2017ManagerId, FY2018ManagerId,
			Manager, PreviousManager, FY2014Manager, FY2015Manager, FY2016Manager, FY2017Manager, FY2018Manager,
			CanManageVIP, PreviousCanManageVIP, FY2014CanManageVIP, FY2015CanManageVIP, FY2016CanManageVIP, FY2017CanManageVIP, FY2018CanManageVIP,
			CanManagePotentialVIP, PreviousCanManagePotentialVIP, FY2014CanManagePotentialVIP, FY2015CanManagePotentialVIP, FY2016CanManagePotentialVIP, FY2017CanManagePotentialVIP, FY2018CanManagePotentialVIP,
			CanManageBDM, PreviousCanManageBDM, FY2014CanManageBDM, FY2015CanManageBDM, FY2016CanManageBDM, FY2017CanManageBDM, FY2018CanManageBDM,
			HandlerId, HandlerFromDate, PreviousHandlerId, FY2014HandlerId, FY2015HandlerId, FY2016HandlerId, FY2017HandlerId, FY2018HandlerId,
			Handler, PreviousHandler, FY2014Handler, FY2015Handler, FY2016Handler, FY2017Handler, FY2018Handler,
			VIPType, VIPTypeFromDate, PreviousVIPType, FY2014VIPType, FY2015VIPType, FY2016VIPType, FY2017VIPType, FY2018VIPType, 
			MIN(FirstDate) FirstDate, FromDate, MAX(src) src,
			CASE WHEN COUNT(*) OVER (PARTITION BY LedgerId, LedgerSource, HeadlineFeatures) = 2 THEN 'U' WHEN MAX(src) = 'S' THEN 'I' WHEN MAX(src) = 'F' THEN 'D' ELSE 'X' END Upsert
	INTO	#Differences
	FROM	(	SELECT /*NULL StructureKey,*/ *, 'S' src FROM #Stage
				UNION ALL
				SELECT *, 'D' src FROM #Dimension
			) x
	GROUP BY LedgerId, LedgerSource, HeadlineFeatures, InPlay, AccountId, 
			OrgId, OrgIdFromDate, PreviousOrgId, FY2014OrgId, FY2015OrgId, FY2016OrgId, FY2017OrgId, FY2018OrgId,
			IntroducerId, IntroducerFromDate, PreviousIntroducerId, FY2014IntroducerId, FY2015IntroducerId, FY2016IntroducerId, FY2017IntroducerId, FY2018IntroducerId,
			Introducer, PreviousIntroducer, FY2014Introducer, FY2015Introducer, FY2016Introducer, FY2017Introducer, FY2018Introducer,
			ManagerId, ManagerFromDate, PreviousManagerId, FY2014ManagerId, FY2015ManagerId, FY2016ManagerId, FY2017ManagerId, FY2018ManagerId,
			Manager, PreviousManager, FY2014Manager, FY2015Manager, FY2016Manager, FY2017Manager, FY2018Manager,
			CanManageVIP, PreviousCanManageVIP, FY2014CanManageVIP, FY2015CanManageVIP, FY2016CanManageVIP, FY2017CanManageVIP, FY2018CanManageVIP,
			CanManagePotentialVIP, PreviousCanManagePotentialVIP, FY2014CanManagePotentialVIP, FY2015CanManagePotentialVIP, FY2016CanManagePotentialVIP, FY2017CanManagePotentialVIP, FY2018CanManagePotentialVIP,
			CanManageBDM, PreviousCanManageBDM, FY2014CanManageBDM, FY2015CanManageBDM, FY2016CanManageBDM, FY2017CanManageBDM, FY2018CanManageBDM,
			HandlerId, HandlerFromDate, PreviousHandlerId, FY2014HandlerId, FY2015HandlerId, FY2016HandlerId, FY2017HandlerId, FY2018HandlerId,
			Handler, PreviousHandler, FY2014Handler, FY2015Handler, FY2016Handler, FY2017Handler, FY2018Handler,
			VIPType, VIPTypeFromDate, PreviousVIPType, FY2014VIPType, FY2015VIPType, FY2016VIPType, FY2017VIPType, FY2018VIPType, 
			CASE @FromDate WHEN CONVERT(datetime2(3),'1900-01-01') THEN FirstDate ELSE NULL END, FromDate -- We only want FirstDate to factor in the diff during a RELOAD, otherwise it is just payload added to a new insert
	HAVING	COUNT(*) = 1
	OPTION (RECOMPILE)

	IF @DryRun = 1 
		SELECT	TOP 10000 
				CASE WHEN Upsert = 'U' AND src = 'D' THEN 'U-' WHEN Upsert = 'U' AND src = 'S' THEN 'U+' ELSE Upsert END act, 
				* 
		FROM	#Differences 
		ORDER BY LedgerId, LedgerSource, HeadlineFeatures, src desc

	SELECT	* INTO #Prepare FROM #Differences WHERE	Upsert <> 'U' OR src <> 'D' -- Discard the 'before' version record of Update

	EXEC [Control].Sp_Log @BatchKey,@Me,'Update','Start',@Message, @RowsProcessed = @@ROWCOUNT

	IF @DryRun = 0 
		BEGIN
			UPDATE	d
			SET		d.OrgId = s.OrgId,
					d.OrgIdFromDate = s.OrgIdFromDate,
					d.PreviousOrgId = s.PreviousOrgId,
					d.FY2014OrgId = s.FY2014OrgId,
					d.FY2015OrgId = s.FY2015OrgId,
					d.FY2016OrgId = s.FY2016OrgId,
					d.FY2017OrgId = s.FY2017OrgId,
					d.FY2018OrgId = s.FY2018OrgId,
					d.IntroducerId = s.IntroducerId,
					d.Introducer = s.Introducer, 
					d.IntroducerFromDate = s.IntroducerFromDate,
					d.PreviousIntroducerId = s.PreviousIntroducerId,
					d.PreviousIntroducer = s.PreviousIntroducer,
					d.FY2014IntroducerId = s.FY2014IntroducerId,
					d.FY2014Introducer = s.FY2014Introducer,
					d.FY2015IntroducerId = s.FY2015IntroducerId,
					d.FY2015Introducer = s.FY2015Introducer,
					d.FY2016IntroducerId = s.FY2016IntroducerId,
					d.FY2016Introducer = s.FY2016Introducer,
					d.FY2017IntroducerId = s.FY2017IntroducerId,
					d.FY2017Introducer = s.FY2017Introducer,
					d.FY2018IntroducerId = s.FY2018IntroducerId,
					d.FY2018Introducer = s.FY2018Introducer,
					d.ManagerId = s.ManagerId,
					d.Manager = s.Manager, 
					d.CanManageVIP = s.CanManageVIP, 
					d.CanManagePotentialVIP = s.CanManagePotentialVIP, 
					d.CanManageBDM = s.CanManageBDM, 
					d.ManagerFromDate = s.ManagerFromDate,
					d.PreviousManagerId = s.PreviousManagerId,
					d.PreviousManager = s.PreviousManager,
					d.PreviousCanManageVIP = s.PreviousCanManageVIP, 
					d.PreviousCanManagePotentialVIP = s.PreviousCanManagePotentialVIP, 
					d.PreviousCanManageBDM = s.PreviousCanManageBDM, 
					d.FY2014ManagerId = s.FY2014ManagerId,
					d.FY2014Manager = s.FY2014Manager,
					d.FY2014CanManageVIP = s.FY2014CanManageVIP, 
					d.FY2014CanManagePotentialVIP = s.FY2014CanManagePotentialVIP, 
					d.FY2014CanManageBDM = s.FY2014CanManageBDM, 
					d.FY2015ManagerId = s.FY2015ManagerId,
					d.FY2015Manager = s.FY2015Manager,
					d.FY2015CanManageVIP = s.FY2015CanManageVIP, 
					d.FY2015CanManagePotentialVIP = s.FY2015CanManagePotentialVIP, 
					d.FY2015CanManageBDM = s.FY2015CanManageBDM, 
					d.FY2016ManagerId = s.FY2016ManagerId,
					d.FY2016Manager = s.FY2016Manager,
					d.FY2016CanManageVIP = s.FY2016CanManageVIP, 
					d.FY2016CanManagePotentialVIP = s.FY2016CanManagePotentialVIP, 
					d.FY2016CanManageBDM = s.FY2016CanManageBDM, 
					d.FY2017ManagerId = s.FY2017ManagerId,
					d.FY2017Manager = s.FY2017Manager,
					d.FY2017CanManageVIP = s.FY2017CanManageVIP, 
					d.FY2017CanManagePotentialVIP = s.FY2017CanManagePotentialVIP, 
					d.FY2017CanManageBDM = s.FY2017CanManageBDM, 
					d.FY2018ManagerId = s.FY2018ManagerId,
					d.FY2018Manager = s.FY2018Manager,
					d.FY2018CanManageVIP = s.FY2018CanManageVIP, 
					d.FY2018CanManagePotentialVIP = s.FY2018CanManagePotentialVIP, 
					d.FY2018CanManageBDM = s.FY2018CanManageBDM, 
					d.HandlerId = s.HandlerId,
					d.Handler = s.Handler, 
					d.HandlerFromDate = s.HandlerFromDate,
					d.PreviousHandlerId = s.PreviousHandlerId,
					d.PreviousHandler = s.PreviousHandler,
					d.FY2014HandlerId = s.FY2014HandlerId,
					d.FY2014Handler = s.FY2014Handler,
					d.FY2015HandlerId = s.FY2015HandlerId,
					d.FY2015Handler = s.FY2015Handler,
					d.FY2016HandlerId = s.FY2016HandlerId,
					d.FY2016Handler = s.FY2016Handler,
					d.FY2017HandlerId = s.FY2017HandlerId,
					d.FY2017Handler = s.FY2017Handler,
					d.FY2018HandlerId = s.FY2018HandlerId,
					d.FY2018Handler = s.FY2018Handler,
					d.VIPType = s.VIPType,
					d.VIPTypeFromDate = s.VIPTypeFromDate,
					d.PreviousVIPType = s.PreviousVIPType,
					d.FY2014VIPType = s.FY2014VIPType,
					d.FY2015VIPType = s.FY2015VIPType,
					d.FY2016VIPType = s.FY2016VIPType,
					d.FY2017VIPType = s.FY2017VIPType,
					d.FY2018VIPType = s.FY2018VIPType,
					d.FromDate = s.FromDate,
					d.FirstDate = CASE @FromDate WHEN CONVERT(datetime2(3),'1900-01-01') THEN s.FirstDate ELSE d.Firstdate END, -- Only update First date on full reload otherwise can't guarantee an incremental FirstDate is a true first date on updates, only on inserts
					d.ModifiedDate = GetDate(),
					d.ModifiedBatchKey = @BatchKey,
					d.ModifiedBy = @Me,
					d.Increment = @Increment
			FROM	#Prepare s
					INNER JOIN [$(Dimensional)].[Dimension].[Structure] d on d.StructureKey = s.StructureKey
			WHERE	s.Upsert = 'U'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'U'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start',@Message, @RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			INSERT	[$(Dimensional)].[Dimension].[Structure]
					(	StructureKey, LedgerId, LedgerSource, HeadlineFeatures, InPlay, AccountId, 
						OrgId, OrgIdFromDate, PreviousOrgId, FY2014OrgId, FY2015OrgId, FY2016OrgId, FY2017OrgId, FY2018OrgId,
						IntroducerId, IntroducerFromDate, PreviousIntroducerId, FY2014IntroducerId, FY2015IntroducerId, FY2016IntroducerId, FY2017IntroducerId, FY2018IntroducerId,
						Introducer, PreviousIntroducer, FY2014Introducer, FY2015Introducer, FY2016Introducer, FY2017Introducer, FY2018Introducer,
						ManagerId, ManagerFromDate, PreviousManagerId, FY2014ManagerId, FY2015ManagerId, FY2016ManagerId, FY2017ManagerId, FY2018ManagerId,
						Manager, PreviousManager, FY2014Manager, FY2015Manager, FY2016Manager, FY2017Manager, FY2018Manager,
						CanManageVIP, PreviousCanManageVIP, FY2014CanManageVIP, FY2015CanManageVIP, FY2016CanManageVIP, FY2017CanManageVIP, FY2018CanManageVIP,
						CanManagePotentialVIP, PreviousCanManagePotentialVIP, FY2014CanManagePotentialVIP, FY2015CanManagePotentialVIP, FY2016CanManagePotentialVIP, FY2017CanManagePotentialVIP, FY2018CanManagePotentialVIP,
						CanManageBDM, PreviousCanManageBDM, FY2014CanManageBDM, FY2015CanManageBDM, FY2016CanManageBDM, FY2017CanManageBDM, FY2018CanManageBDM,
						HandlerId, HandlerFromDate, PreviousHandlerId, FY2014HandlerId, FY2015HandlerId, FY2016HandlerId, FY2017HandlerId, FY2018HandlerId,
						Handler, PreviousHandler, FY2014Handler, FY2015Handler, FY2016Handler, FY2017Handler, FY2018Handler,
						VIPType, VIPTypeFromDate, PreviousVIPType, FY2014VIPType, FY2015VIPType, FY2016VIPType, FY2017VIPType, FY2018VIPType, 
						FromDate, ToDate, FirstDate,
						CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy, Increment
					)
			SELECT 	StructureKey, LedgerId, LedgerSource, HeadlineFeatures, InPlay, AccountId, 
					OrgId, OrgIdFromDate, PreviousOrgId, FY2014OrgId, FY2015OrgId, FY2016OrgId, FY2017OrgId, FY2018OrgId,
					IntroducerId, IntroducerFromDate, PreviousIntroducerId, FY2014IntroducerId, FY2015IntroducerId, FY2016IntroducerId, FY2017IntroducerId, FY2018IntroducerId,
					Introducer, PreviousIntroducer, FY2014Introducer, FY2015Introducer, FY2016Introducer, FY2017Introducer, FY2018Introducer,
					ManagerId, ManagerFromDate, PreviousManagerId, FY2014ManagerId, FY2015ManagerId, FY2016ManagerId, FY2017ManagerId, FY2018ManagerId,
					Manager, PreviousManager, FY2014Manager, FY2015Manager, FY2016Manager, FY2017Manager, FY2018Manager,
					CanManageVIP, PreviousCanManageVIP, FY2014CanManageVIP, FY2015CanManageVIP, FY2016CanManageVIP, FY2017CanManageVIP, FY2018CanManageVIP,
					CanManagePotentialVIP, PreviousCanManagePotentialVIP, FY2014CanManagePotentialVIP, FY2015CanManagePotentialVIP, FY2016CanManagePotentialVIP, FY2017CanManagePotentialVIP, FY2018CanManagePotentialVIP,
					CanManageBDM, PreviousCanManageBDM, FY2014CanManageBDM, FY2015CanManageBDM, FY2016CanManageBDM, FY2017CanManageBDM, FY2018CanManageBDM,
					HandlerId, HandlerFromDate, PreviousHandlerId, FY2014HandlerId, FY2015HandlerId, FY2016HandlerId, FY2017HandlerId, FY2018HandlerId,
					Handler, PreviousHandler, FY2014Handler, FY2015Handler, FY2016Handler, FY2017Handler, FY2018Handler,
					VIPType, VIPTypeFromDate, PreviousVIPType, FY2014VIPType, FY2015VIPType, FY2016VIPType, FY2017VIPType, FY2018VIPType, 
					FromDate, CONVERT(datetime2(3),'9999-12-31') ToDate, FirstDate,
					GETDATE() CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy,
					GETDATE() ModifiedDate,	@BatchKey ModifiedBatchKey, @Me ModifiedBy, @Increment
			FROM	#Prepare
			WHERE	Upsert = 'I'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'I'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey,@Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
