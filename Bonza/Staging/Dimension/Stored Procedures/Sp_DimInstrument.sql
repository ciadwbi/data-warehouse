CREATE PROCEDURE [Dimension].[Sp_DimInstrument]
	@BatchKey	int
WITH RECOMPILE
AS 

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN

	SET NOCOUNT ON;
	
		------ Initialize Temp Tables ---------------------
	IF OBJECT_ID('TempDB..#Instrument','U') IS NOT NULL BEGIN DROP TABLE #Instrument END;

	BEGIN TRY

		BEGIN TRANSACTION Instrument;

		EXEC [Control].Sp_Log	@BatchKey,'Sp_DimInstrument','Prepare','Start'

		SELECT
		     d.InstrumentKey,
			Stage.InstrumentID,
			Stage.Source,
			Stage.AccountNumber,
			Stage.SafeAccountNumber,
			Stage.AccountName,
			Stage.BSB,
			Stage.ExpiryDate,
			Stage.Verified,
			Stage.VerifyAmount,
			-1 InstrumentTypeKey,
			Stage.InstrumentType,
			-1  AS ProviderKey,
			Stage.ProviderName,
			Stage.FromDate
		INTO #Instrument
		FROM Stage.Instrument Stage
		LEFT OUTER JOIN [$(Dimensional)].Dimension.Instrument d  on (Stage.InstrumentID=d.InstrumentID and d.source=Stage.source)
		WHERE Stage.BatchKey=@BatchKey;

		EXEC [Control].Sp_Log	@BatchKey,'Sp_DimInstrument','Update','Start',@RowsProcessed = @@ROWCOUNT 

		UPDATE D
		SET 
			   D.InstrumentID=s.InstrumentID,
			   D.Source=S.Source ,
			   D.AccountNumber=S.AccountNumber ,
			   D.SafeAccountNumber=S.SafeAccountNumber ,
			   D.AccountName=S.AccountName,
			   D.BSB=S.BSB ,
			   D.ExpiryDate=s.ExpiryDate,
			   D.Verified=S.Verified,
			   D.InstrumentTypeKey=s.InstrumentTypeKey ,
			   D.InstrumentType=S.InstrumentType ,
			   D.ProviderKey=s.ProviderKey ,
			   D.ProviderName=s.ProviderName ,
			   D.FromDate=s.FromDate,
			   D.ModifiedDate=CURRENT_TIMESTAMP,
			   D.ModifiedBatchKey=@BatchKey,
			   D.ModifiedBy='Sp_DimInstrument'
		FROM [$(Dimensional)].Dimension.Instrument d
		INNER JOIN #Instrument s   on (s.InstrumentID=d.InstrumentID and d.source=s.source)
		WHERE S.InstrumentKey IS NOT NULL

		EXEC [Control].Sp_Log	@BatchKey,'Sp_DimInstrument','Insert','Start',@RowsProcessed = @@ROWCOUNT

		INSERT INTO [$(Dimensional)].Dimension.Instrument ( InstrumentID,Source,AccountNumber,SafeAccountNumber,AccountName,BSB,ExpiryDate,Verified,
														VerifyAmount,InstrumentTypeKey,InstrumentType,ProviderKey,ProviderName,FromDate,ToDate,
														FirstDate,CreatedDate,CreatedBatchKey,CreatedBy,ModifiedDate,ModifiedBatchKey,ModifiedBy)
		SELECT
			Stage.InstrumentID,
			Stage.Source,
			Stage.AccountNumber,
			Stage.SafeAccountNumber,
			Stage.AccountName,
			Stage.BSB,
			Stage.ExpiryDate,
			Stage.Verified,
			NULL AS VerifyAmount,
			Stage.InstrumentTypeKey,
			Stage.InstrumentType,
			Stage.ProviderKey ,
			Stage.ProviderName,
			Stage.FromDate,
			'9999-12-31' AS ToDate,
			CURRENT_TIMESTAMP AS FirstDate,
			CURRENT_TIMESTAMP AS CreatedDate,
			@BatchKey AS CreatedBatchKey,
			'Sp_DimInstrument' AS CreatedBy,
			CURRENT_TIMESTAMP AS ModifiedDate,
			@BatchKey AS ModifiedBatchKey,
			'Sp_DimInstrument'  AS ModifiedBy
		FROM #Instrument Stage
		WHERE InstrumentKey IS NULL

		EXEC [Control].Sp_Log	@BatchKey,'Sp_DimInstrument',NULL,'Success',@RowsProcessed = @@ROWCOUNT
		COMMIT TRANSACTION Instrument;

	END TRY
	BEGIN CATCH

		Rollback TRANSACTION Instrument;
		declare @Error int = ERROR_NUMBER();
		declare @ErrorMessage varchar(max)  = ERROR_MESSAGE();
	
		 -- Log the failure at the object level
	 	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimInstrument', NULL, 'Failed', @ErrorMessage;
		THROW;

	END CATCH

End