﻿CREATE PROC [Dimension].[Sp_DimEvent]
(
	@BatchKey	int
)
WITH RECOMPILE
AS 

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN Try

	--BEGIN TRY DROP TABLE #EventID END TRY BEGIN CATCH END CATCH
	BEGIN TRY DROP TABLE #Event END TRY BEGIN CATCH END CATCH

	--SELECT DISTINCT SourceEventID INTO #EventID FROM Stage.Event WITH(NOLOCK) WHERE BatchKey=@BatchKey

	EXEC [Control].Sp_Log @BatchKey,'Sp_DimEvent','InsertIntoTempTables','Start'

	SELECT	s.Source, s.SourceEventId,
			s.SourceEvent, s.SourceEventType, s.ParentEventId, s.ParentEvent, s.GrandparentEventId, s.GrandparentEvent, s.GreatGrandparentEventId, s.GreatGrandparentEvent,
			s.SourceEventDate, 
			s.ClassID, 
			ISNULL(C.ClassKey,-1) ClassKey,
			s.SourcePrizePool,
			s.SourceTrackCondition,
			s.SourceEventWeather,
			s.ResultedDateTime, 
			s.SourceRaceNumber,
			ISNULL(RDK.DayKey,-1) ResultedDayKey,
			s.EventAbandonedDateTime,
			ISNULL(EAK.DayKey,-1) EventAbandonedDayKey,
			s.SourceVenue, s.SourceVenueType, s.SourceVenueEventType, s.SourceVenueEventTypeName, 
			s.SourceVenueStateCode, s.SourceVenueState, s.SourceVenueCountryCode, s.SourceVenueCountry, 
			s.SourceVenueId,  
			s.SourceSubSportType,
			s.SourceRaceGroup,
			s.SourceRaceClass,
			s.SourceHybridPricingTemplate,
			s.SourceWeightType,
			s.SourceDistance,
			s.SourceEventAbandoned,
			s.SourceLiveStreamPerformId,
			s.FromDate,
			ISNULL(cc.ClassKey,-1) NewClassKey,
			s.SourceSubSportTypeId,
			CASE WHEN d.EventId IS NULL THEN 'I' ELSE 'U' END Upsert
	INTO	#Event
	FROM	Stage.Event s
			LEFT JOIN [$(Dimensional)].Dimension.Event d ON (d.EventId = s.SourceEventId AND d.Source = s.Source) 
			LEFT JOIN [$(Dimensional)].Dimension.DayZone RDK ON RDK.MasterDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,s.ResultedDateTime)),'N/A') and RDK.AnalysisDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,s.ResultedDateTime)),'N/A')
			LEFT JOIN [$(Dimensional)].Dimension.DayZone EAK ON EAK.MasterDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,s.EventAbandonedDateTime)),'N/A') and EAK.AnalysisDayText = ISNULL(CONVERT(VARCHAR,CONVERT(DATE,s.EventAbandonedDateTime)),'N/A')
			LEFT JOIN [$(Dimensional)].Dimension.Class C ON C.ClassId=S.ClassID and C.Source=S.ClassSource AND S.FromDate>=c.FromDate AND S.FromDate<c.ToDate
			LEFT JOIN [$(Dimensional)].CC.Class cc ON cc.HashKey = HASHBYTES('MD5','|'+ISNULL(CONVERT(varchar(20),s.ClassId),'')+'|'+ISNULL(CONVERT(varchar(20),s.SourceSubSportTypeId),'')+'|'+ISNULL(s.SourceVenueEventTypeName,'')+'|')
	WHERE	BatchKey = @BatchKey
	OPTION (RECOMPILE);

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimEvent','Update','Start', @RowsProcessed = @@ROWCOUNT

	UPDATE d
		SET	d.SourceEvent = s.SourceEvent, 
			d.SourceEventType = s.SourceEventType, 
			d.ParentEventId = s.ParentEventId, 
			d.ParentEvent = s.ParentEvent, 
			d.GrandparentEventId = s.GrandparentEventId, 
			d.GrandparentEvent = s.GrandparentEvent, 
			d.GreatGrandparentEventId = s.GreatGrandparentEventId , 
			d.GreatGrandparentEvent = s.GreatGrandparentEvent,
			d.SourceEventDate = s.SourceEventDate, 
			d.ClassID = s.ClassID, 
			d.ClassKey = s.ClassKey, 
			d.SourcePrizePool = s.SourcePrizePool,
			d.SourceTrackCondition = s.SourceTrackCondition,
			d.SourceEventWeather = s.SourceEventWeather,
			d.ResultedDateTime = s.ResultedDateTime,
			d.ResultedDayKey = s.ResultedDayKey,
			d.SourceRaceNumber = s.SourceRaceNumber,
			d.SourceVenue = s.SourceVenue, 
			d.SourceVenueType = s.SourceVenueType, 
			d.SourceVenueEventType = s.SourceVenueEventType, 
			d.SourceVenueEventTypeName = s.SourceVenueEventTypeName,
			d.SourceVenueStateCode = s.SourceVenueStateCode,
			d.SourceVenueState = s.SourceVenueState,
			d.SourceVenueCountryCode = s.SourceVenueCountryCode,
			d.SourceVenueCountry = s.SourceVenueCountry,
			d.SourceVenueId = s.SourceVenueId, 
			d.SourceSubSportType = s.SourceSubSportType,
			d.SourceRaceGroup = s.SourceRaceGroup,
			d.SourceRaceClass = s.SourceRaceClass,
			d.SourceHybridPricingTemplate = s.SourceHybridPricingTemplate,
			d.SourceWeightType = s.SourceWeightType,
			d.SourceDistance = s.SourceDistance,
			d.SourceEventAbandoned = s.SourceEventAbandoned,
			d.EventAbandonedDateTime = s.EventAbandonedDateTime,
			d.EventAbandonedDayKey = s.EventAbandonedDayKey,
			d.SourceLiveStreamPerformId = s.SourceLiveStreamPerformId,
			d.FromDate = s.FromDate, 
			d.ModifiedDate = CURRENT_TIMESTAMP, 
			d.ModifiedBatchKey = @BatchKey,
			d.ModifiedBy = 'Sp_DimEvent',
			d.NewClassKey = s.NewClassKey,
			d.SourceSubSportTypeId = s.SourceSubSportTypeId
	FROM	#Event s
			INNER JOIN [$(Dimensional)].Dimension.Event d ON (d.EventId = s.SourceEventId AND d.Source = s.Source) 
	WHERE	s.Upsert = 'U'

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimEvent','Insert','Start', @RowsProcessed = @@ROWCOUNT

	INSERT INTO [$(Dimensional)].Dimension.Event
			(Source, SourceEventID, EventId, SourceEvent, SourceEventType, ParentEventId, ParentEvent, 
				GrandparentEventId, GrandparentEvent, GreatGrandparentEventId, GreatGrandparentEvent,
				SourceEventDate, ClassID, ClassKey, SourceRaceNumber, Event, Round, RoundSequence, 
				Competition, CompetitionSeason,CompetitionStartDate, CompetitionEndDate,
				Grade, Distance, SourceprizePool, PrizePool, SourceTrackCondition, SourceEventWeather, TrackCondition, EventWeather, EventAbandoned, EventAbandonedDateTime, EventAbandonedDayKey,
				ScheduledDateTime, ScheduledDayKey, ScheduledVenueDateTime,
				ResultedDateTime, ResultedDayKey, ResultedVenueDateTime, 
				SourceVenueID, SourceVenue, SourceVenueType, SourceVenueEventType, SourceVenueEventTypeName, 
				SourceVenueStateCode, SourceVenueState, SourceVenueCountryCode, SourceVenueCountry, 
				VenueId, Venue, VenueType, VenueStateCode, VenueState, VenueCountryCode, VenueCountry, 
				SourceSubSportType,SourceRaceGroup, SourceRaceClass, SourceHybridPricingTemplate, SourceWeightType,
				SubSportType,RaceGroup, RaceClass, HybridPricingTemplate, WeightType, SourceDistance, SourceEventAbandoned,
				SourceLiveStreamPerformId, LiveVideoStreamed, LiveScoreBoard,
				ColumnLock, FromDate, FirstDate, CreatedDate, CreatedBatchKey, CreatedBy, 
				ModifiedDate, ModifiedBatchKey,ModifiedBy,
				NewClassKey,
				SourceSubSportTypeId
				)
	SELECT	s.Source, s.SourceEventId, s.SourceEventId, s.SourceEvent, s.SourceEventType, s.ParentEventId, s.ParentEvent, 
				s.GrandparentEventId, s.GrandparentEvent, s.GreatGrandparentEventId, s.GreatGrandparentEvent,
				s.SourceEventDate, s.ClassID, s.ClassKey, s.SourceRaceNumber, 'Pending', 'Pending', NULL, 
				'Pending', 'Pending', NULL,NULL,
				'Pending', 'Pending', s.SourcePrizePool, NULL, s.SourceTrackCondition, s.SourceEventWeather, 'Pending', 'Pending', 'Pending', s.EventAbandonedDateTime, s.EventAbandonedDayKey,
				NULL, NULL, NULL,
				s.ResultedDateTime, s.ResultedDayKey, NULL,
				s.SourceVenueId, s.SourceVenue, s.SourceVenueType, s.SourceVenueEventType, s.SourceVenueEventTypeName, 
				s.SourceVenueStateCode, s.SourceVenueState, s.SourceVenueCountryCode, s.SourceVenueCountry,
				NULL, 'Pending', 'Pending', 'PDG', 'Pending', 'PDG', 'Pending',
				s.SourceSubSportType, s.SourceRaceGroup, s.SourceRaceClass, s.SourceHybridPricingTemplate, s.SourceWeightType, 
				'Pending', 'Pending', 'Pending', 'Pending', 'Pending', s.SourceDistance, s.SourceEventAbandoned,
				s.SourceLiveStreamPerformId, 'PDG', 'PDG',
				0 ColumnLock, s.FromDate, s.FromDate FirstDate, CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, 'Sp_DimEvent' CreatedBy, 
				CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey, 'Sp_DimEvent' ModifiedBy, 
				NewClassKey, SourceSubSportTypeId				 
	FROM	#Event s
	WHERE	s.Upsert = 'I'

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimEvent',NULL,'Success', @RowsProcessed = @@ROWCOUNT

END Try

BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimEvent', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH