﻿
CREATE PROC [Dimension].[Sp_DimPromotion]
(
	@BatchKey	INT
)
WITH RECOMPILE
AS 

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN

	/* Developer: Aaron Jackson
	   Date: 25/06/2015
	   Desc: Populate Dim Promotion

	   I erroneously created this a type 1 dimension. This needs to be updated to be a type 2.

	   TODO: Add Try Catch
	*/

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimPromotion','Type1-UpdateDim','Start';

	UPDATE Dim
	SET Dim.PromotionName	=	Stage.PromotionName,
		Dim.PromotionType	=	Stage.PromotionType,
		Dim.OfferDate		=	Stage.OfferDate,
		Dim.ExpiryDate		=	Stage.ExpiryDate,
		Dim.ModifiedDate	=	Stage.ModifiedDate,
		Dim.ModifiedBatchKey=	Stage.ModifiedBatchKey,
		Dim.ModifiedBy		=	Stage.ModifiedBy
	FROM [$(Dimensional)].Dimension.Promotion Dim
	INNER JOIN 
			(
				SELECT
					Stage.[PromotionId],
					Stage.[PromotionType],
					Stage.PromotionName,
					Stage.OfferDate,
					Stage.ExpiryDate,
					CONVERT(DATETIME2(7),GETDATE()) AS ModifiedDate,
					@BatchKey AS ModifiedBatchKey,
					'Sp_DimPromotion'  AS ModifiedBy,
					Stage.Source
				FROM Stage.Promotion Stage
				LEFT OUTER JOIN [$(Dimensional)].Dimension.Promotion Dim ON Stage.PromotionId = Dim.PromotionId and Stage.Source = Dim.Source
				WHERE Dim.PromotionId IS NOT NULL
				AND Stage.BatchKey = @BatchKey
			) Stage
			ON Dim.PromotionId=Stage.PromotionId AND Stage.Source=Dim.Source;

	EXEC [Control].Sp_Log @BatchKey,'Sp_DimPromotion','InsertIntoDim','Start';

	INSERT INTO [$(Dimensional)].Dimension.Promotion (PromotionId, PromotionType, PromotionName, OfferDate, ExpiryDate, FromDate, ToDate, FirstDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy, Source)

	SELECT 
		PromotionId,
		PromotionType,
		PromotionName,
		OfferDate,
		ExpiryDate,
		FromDate,
		ToDate,
		FirstDate,
		CreatedDate,
		CreatedBatchKey,
		CreatedBy,
		ModifiedDate,
		ModifiedBatchKey,
		ModifiedBy,
		[Source]
	FROM
	(
		SELECT DISTINCT
			Stage.[PromotionId],
			Stage.[PromotionType],
			Stage.PromotionName,
			Stage.OfferDate,
			Stage.ExpiryDate,
			CONVERT(DATETIME2(0),GETDATE()) AS FromDate,
			CONVERT(DATETIME2(0),GETDATE()) AS ToDate,
			CONVERT(DATETIME2(0),GETDATE()) AS FirstDate,
			CONVERT(DATETIME2(7),GETDATE()) AS CreatedDate,
			@BatchKey AS CreatedBatchKey,
			'Sp_DimPromotion' AS CreatedBy,
			CONVERT(DATETIME2(7),GETDATE()) AS ModifiedDate,
			@BatchKey AS ModifiedBatchKey,
			'Sp_DimPromotion'  AS ModifiedBy,
			Stage.Source,
			row_number() over(partition by Stage.PromotionId order by Stage.ExpiryDate DESC) as RowNum
		FROM Stage.Promotion Stage
		LEFT OUTER JOIN [$(Dimensional)].Dimension.Promotion Dim ON Stage.PromotionId=Dim.PromotionId and Stage.Source=Dim.Source
		WHERE Dim.PromotionId IS NULL
		AND Stage.BatchKey = @BatchKey
	) a
	Where Rownum = 1

	EXEC [Control].Sp_Log	@BatchKey,'Sp_DimPromotion',NULL,'Success'

END