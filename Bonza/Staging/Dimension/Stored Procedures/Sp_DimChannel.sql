﻿CREATE PROC [Dimension].[Sp_DimChannel]
(
	@BatchKey	int,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @RowCount int

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'InsertIntoDim','Start'

	INSERT INTO [$(Dimensional)].Dimension.Channel (ChannelId, ChannelDescription, ChannelName, ChannelTypeName, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy)
	Select	a.ChannelId,
			a.ChannelDescription,
			a.ChannelName,
			a.ChannelTypeName,
			GetDate() CreatedDate,@BatchKey CreatedBatchKey,'Sp_DimChannel' CreatedBy,GetDate() ModifiedDate,@BatchKey ModifiedBatchKey,'Sp_DimChannel' ModifiedBy
	From [Stage].[Channel] a
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Channel b on a.ChannelId = b.ChannelId
	Where BatchKey = @BatchKey and b.ChannelId is null

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount	

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
