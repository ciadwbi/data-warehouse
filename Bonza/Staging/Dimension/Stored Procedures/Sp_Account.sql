CREATE PROCEDURE [Dimension].[Sp_Account]
(
	@BatchKey		int,
	@FromDate		datetime2(3),
	@ToDate			datetime2(3),
	@Increment		int = NULL,
	@DryRun			bit = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Message varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int
	DECLARE @i int

	DECLARE @MessagingTimeShift int = Messaging.TimeShiftMinutes()

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	 EXEC [Control].Sp_Log	@BatchKey,@Me,'ClientChanges','Start', @Message

	SELECT	ClientId, MIN(FromDate) FirstDate, MAX(FromDate) FromDate
	INTO	#Clients
	FROM	(
				----------- tblClients -----------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, FromDate, Title, FirstName, MiddleName, Surname, DOB, Gender, OrgID, ClientType, PIN, CB_Client_key, PWDHash, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblClients
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, ToDate, Title, FirstName, MiddleName, Surname, DOB, Gender, OrgID, ClientType, PIN, CB_Client_key, PWDHash, 2 trg
						FROM	[$(Acquisition)].[IntraBet].tblClients
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, FromDate, Title, FirstName, MiddleName, Surname, DOB, Gender, OrgID, ClientType, PIN, CB_Client_key, PWDHash
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- tblClientInfo -----------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, FromDate, StartDate, StatementFrequency, StatementMethod, Channel, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientInfo
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, ToDate, StartDate, StatementFrequency, StatementMethod, Channel, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientInfo
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, FromDate, StartDate, StatementFrequency, StatementMethod, Channel
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- tblClientAddresses ---------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, AddressType, FromDate, EMAIL, Phone, FAX, Address1, Address2, Suburb, State, PostCode, Mobile, Country, altEmail, StateID, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientAddresses
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, AddressType, ToDate, EMAIL, Phone, FAX, Address1, Address2, Suburb, State, PostCode, Mobile, Country, altEmail, StateID, 2 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientAddresses
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, AddressType, FromDate, EMAIL, Phone, FAX, Address1, Address2, Suburb, State, PostCode, Mobile, Country, altEmail, StateID
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- tblClientAffiliateInfo ---------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, FromDate, BTag2, TrafficSource, RefURL, CampaignID, Keywords, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientAffiliateInfo
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, ToDate, BTag2, TrafficSource, RefURL, CampaignID, Keywords, 2 trg
						FROM	[$(Acquisition)].[IntraBet].tblClientAffiliateInfo
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, FromDate, BTag2, TrafficSource, RefURL, CampaignID, Keywords
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- Tracking Adjust Data ---------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientId, FromDate, App_Id, [App_Name], App_Version, Event_Name, Tracker, Tracker_Name, Adwords_Campaign_Type, Adwords_Campaign_Name, Fb_Campaign_Group_Name, Fb_Campaign_Name, Ip_Address, Partner_Parameters, dcp_btag, dcp_campaign, dcp_keyword, dcp_publisher, dcp_source, label, publisher, keyword, campaign, btag, source, 1 trg
						FROM	[$(Messaging)].Messaging.TrackingAdjustData
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate 
							AND event_name in ('Create New Account','Registration : Confirm','Registration')
						UNION ALL
						SELECT	ClientId, FromDate, App_Id, [App_Name], App_Version, Event_Name, Tracker, Tracker_Name, Adwords_Campaign_Type, Adwords_Campaign_Name, Fb_Campaign_Group_Name, Fb_Campaign_Name, Ip_Address, Partner_Parameters, dcp_btag, dcp_campaign, dcp_keyword, dcp_publisher, dcp_source, label, publisher, keyword, campaign, btag, source, 2 trg
						FROM	[$(Messaging)].Messaging.TrackingAdjustData
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
							AND event_name in ('Create New Account','Registration : Confirm','Registration')
					) x
				WHERE clientID <> 0
				GROUP BY ClientId, FromDate, App_Id, [App_Name], App_Version, Event_Name, Tracker, Tracker_Name, Adwords_Campaign_Type, Adwords_Campaign_Name, Fb_Campaign_Group_Name, Fb_Campaign_Name, Ip_Address, Partner_Parameters, dcp_btag, dcp_campaign, dcp_keyword, dcp_publisher, dcp_source, label, publisher, keyword, campaign, btag, source
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- tblInternetAffiliates ---------
				SELECT
				b.ClientID, a.FromDate
				FROM (
					SELECT AffiliateID, SiteID, MAX(FromDate) FromDate
					FROM (
						SELECT	AffiliateID, SiteID, FromDate
						FROM(
							SELECT	AffiliateID, SiteID, FromDate, AffiliateName, SitePromoName, 1 trg
							FROM	[$(Acquisition)].Utility.InternetAffiliates
							WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate 
							AND Active = 1
							UNION ALL
							SELECT	AffiliateID, SiteID, ToDate, AffiliateName, SitePromoName, 2 trg
							FROM	[$(Acquisition)].Utility.InternetAffiliates
							WHERE	ToDate > @FromDate AND ToDate <= @ToDate
							AND Active = 1
							) x
				GROUP BY AffiliateID, SiteID, FromDate, AffiliateName, SitePromoName
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
							) y
				GROUP BY AffiliateID, SiteID
					) a
				JOIN [$(Acquisition)].Intrabet.tblClientAffiliateInfo b ON a.AffiliateID = b.AffiliateID and a.SiteID = b.SiteID and a.FromDate >= b.FromDate and a.FromDate < b.ToDate
				UNION ALL
				----------- tblSubscriptionCentre ---------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, FromDate, exactTargetID, 1 trg
						FROM	[$(Acquisition)].[IntraBet].tblSubscriptionCentre
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, ToDate, exactTargetID, 2 trg
						FROM	[$(Acquisition)].[IntraBet].tblSubscriptionCentre
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, FromDate, exactTargetID
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- Applications (Cards) ---------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, FromDate, Created, 1 trg
						FROM	[$(Acquisition)].[IntraBet].Applications
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, ToDate, Created, 2 trg
						FROM	[$(Acquisition)].[IntraBet].Applications
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, FromDate, Created
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- Cards ---------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, FromDate, ExternalAccountId, CardStatus, Created, 1 trg
						FROM	[$(Acquisition)].[IntraBet].Cards
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, ToDate, ExternalAccountId, CardStatus, Created, 2 trg
						FROM	[$(Acquisition)].[IntraBet].Cards
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, FromDate, ExternalAccountId, CardStatus, Created
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- Eachway (Cards) ---------
				SELECT	ClientId, FromDate
				FROM (	SELECT	ClientID, FromDate, AccountId, 1 trg
						FROM	[$(Acquisition)].[IntraBet].EachwayAccounts
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	ClientID, ToDate, AccountId, 2 trg
						FROM	[$(Acquisition)].[IntraBet].EachwayAccounts
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				GROUP BY ClientID, FromDate, AccountId
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- Referral ---------
				SELECT	ClientId, FromDate
				FROM (	SELECT	FriendClientId ClientID, FromDate, ReferrerClientId, BurnCode, BurntDateUTC, 1 trg
						FROM	[$(Acquisition)].[IntraBet].Friend
						WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
						UNION ALL
						SELECT	FriendClientId ClientID, ToDate, ReferrerClientId, BurnCode, BurntDateUTC, 2 trg
						FROM	[$(Acquisition)].[IntraBet].Friend
						WHERE	ToDate > @FromDate AND ToDate <= @ToDate
					) x
				WHERE ClientID IS NOT NULL
				GROUP BY ClientID, FromDate, ReferrerClientId, BurnCode, BurntDateUTC
				HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
				UNION ALL
				----------- Reward Eligibility --------- NOTE: for messages, have to assume arrival of a message is a change, no easy way to check
				SELECT	ClientId, FromDate
				FROM 	[$(Acquisition)].[Messaging].RewardsUpdateEligibility
				WHERE	FromDate > @FromDate AND FromDate <= @ToDate
						AND IsDuplicate = 0
				UNION ALL
				----------- Velocity Link Create --------- NOTE: for messages, have to assume arrival of a message is a change, no easy way to check
				SELECT	ClientId, FromDate
				FROM 	[$(Acquisition)].[Messaging].RewardsCreateVelocityLink
				WHERE	FromDate > @FromDate AND FromDate <= @ToDate
						AND IsDuplicate = 0
				UNION ALL
				----------- Velocity Link Update --------- NOTE: for messages, have to assume arrival of a message is a change, no easy way to check
				SELECT	ClientId, FromDate
				FROM 	[$(Acquisition)].[Messaging].RewardsUpdateVelocityLink
				WHERE	FromDate > @FromDate AND FromDate <= @ToDate
						AND IsDuplicate = 0
			) x
	GROUP BY ClientId
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'AccountChanges','Start', @Message, @RowsProcessed = @@ROWCOUNT

	SELECT	AccountId, ClientId, AccountNumber, MIN(FromDate) FirstDate, MAX(FromDate) FromDate, MAX(NewLedger) NewLedger
	INTO	#Accounts
	FROM	(	SELECT	AccountId, ClientId, AccountNumber, FromDate, MIN(NewLedger) NewLedger
				FROM	(	SELECT	AccountId, ClientId, AccountNumber, FromDate, MIN(NewLedger) NewLedger
							FROM (	SELECT	AccountId, ClientID, AccountNumber, FromDate, 1 NewLedger, Ledger, 1 trg
									FROM	[$(Acquisition)].[IntraBet].tblAccounts
									WHERE	FromDate > @FromDate AND FromDate <= @ToDate AND ToDate > @FromDate
									UNION ALL
									SELECT	AccountId, ClientID, AccountNumber, ToDate, 0 NewLedger, Ledger, 2 trg
									FROM	[$(Acquisition)].[IntraBet].tblAccounts
									WHERE	ToDate > @FromDate AND ToDate <= @ToDate
								) x
							GROUP BY AccountId, ClientID, AccountNumber, FromDate, Ledger
							HAVING COUNT(*) <> 2 AND MIN(trg) = 1 -- capture changes excluding deletions (want to keep last version prior to deletion)
						) x
				GROUP BY AccountId, ClientID, AccountNumber, FromDate
			) y
	GROUP BY AccountId, ClientID, AccountNumber
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'CombinedChanges','Start', @Message, @RowsProcessed = @@ROWCOUNT

	SELECT	AccountId, ClientId, AccountNumber, MAX(FirstDate) FirstDate, MAX(FromDate) FromDate, MAX(NewLedger) NewLedger
	INTO	#Candidates
	FROM	(	SELECT	AccountId, ClientId, AccountNumber, FirstDate, FromDate, NewLedger
				FROM	#Accounts
				UNION ALL
				SELECT	a.AccountId, a.ClientId, a.AccountNumber, c.FirstDate, c.FromDate, 0 NewLedger
				FROM	#Clients c
						INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON (a.ClientID = c.ClientID AND a.FromDate <= c.FromDate AND a.ToDate > c.Fromdate)
			) x
	GROUP BY AccountId, ClientId, AccountNumber
	OPTION (RECOMPILE, TABLE HINT(a, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'MissingOpenDate','Start', @Message, @RowsProcessed = @@ROWCOUNT

	SELECT	c.AccountId, c.ClientID, c.AccountNumber, c.FromDate
	INTO	#MissingOpenDate
	FROM	#Candidates c
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientInfo i ON i.ClientID = c.ClientID AND i.FromDate <= c.FromDate AND i.ToDate > c.FromDate
	WHERE	c.AccountNumber > 1 OR i.StartDate = CONVERT(datetime,'1900-01-01') OR i.StartDate IS NULL
	OPTION (RECOMPILE, TABLE HINT(i, FORCESEEK))


	EXEC [Control].Sp_Log	@BatchKey,@Me,'DerivedOpenDate','Start', @Message, @RowsProcessed = @@ROWCOUNT

	SELECT	c.AccountId, MIN(ISNULL(t.TransactionDate,a.FromDate)) AccountOpenedDate, CASE COUNT(t.ClientId) WHEN 0 THEN 'Account' ELSE 'Transaction' END Origin
	INTO	#DerivedOpenDate
	FROM	#MissingOpenDate c
			INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON (a.AccountID = c.AccountID)
			LEFT JOIN [$(Acquisition)].IntraBet.tblTransactions t ON (t.ClientID = c.ClientID AND t.AccountNumber = c.AccountNumber AND t.FromDate = CONVERT(datetime2(3),'2013-12-19') AND a.FromDate = CONVERT(datetime2(3),'2013-12-19'))
	GROUP BY c.AccountID
	OPTION (RECOMPILE, TABLE HINT(a, FORCESEEK), TABLE HINT(t, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'CardIssue','Start', @Message, @RowsProcessed = @@ROWCOUNT

	SELECT	c.ClientID, MIN(d.Created) CardFirstIssueDate, MAX(d.Created) CardIssueDate
	INTO	#CardIssue
	FROM	#Candidates c
			INNER JOIN [$(Acquisition)].[IntraBet].Cards d ON d.ClientID = c.ClientID AND d.FromDate <= c.FromDate AND d.ToDate > c.FromDate
	GROUP BY c.ClientID
	OPTION (RECOMPILE, TABLE HINT(d, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'ClientAffiliateInfo','Start', @Message, @RowsProcessed = @@ROWCOUNT

	select *
	INTO #TrackingAdjustData
	from (
		select *, row_number() over (partition by ClientID order by FromDate desc) rn
		from [$(Messaging)].[Messaging].[TrackingAdjustData]
		WHERE event_name in ('Create New Account','Registration : Confirm')
	) k
	where rn = 1
	order by clientid

	SELECT
	ia.AffiliateName, ia.SitePromoName, b.BTag2, ia.FromDate, ia.ToDate
	INTO #Affiliates
	FROM [$(Acquisition)].Utility.InternetAffiliates ia
	JOIN (SELECT DISTINCT d.BTag2
			FROM #Candidates c
			INNER JOIN [$(Acquisition)].Intrabet.tblClientAffiliateInfo d ON c.ClientID = d.ClientID AND d.FromDate <= c.FromDate AND d.ToDate > c.FromDate
		) b ON b.BTag2 like '%-' + CONVERT(VARCHAR(10), ia.SiteId) + '-' + CONVERT(VARCHAR(10), ia.AffiliateId) + '-%'
	
	EXEC [Control].Sp_Log	@BatchKey,@Me,'PasswordChange','Start', @Message, @RowsProcessed = @@ROWCOUNT

	SELECT	c.ClientID, MAX(i1.ToDate) PasswordLastChanged
	INTO	#PasswordChange
	FROM	#Candidates c
			INNER JOIN [$(Acquisition)].[IntraBet].tblClients i ON i.ClientID = c.ClientID AND i.FromDate <= c.FromDate AND i.ToDate > c.FromDate
			INNER JOIN [$(Acquisition)].[IntraBet].tblClients i1 ON i1.ClientID = c.ClientID AND i1.ToDate <= i.FromDate AND i1.PWDHash <> i.PWDHash 
	GROUP BY c.ClientID
	OPTION (RECOMPILE, TABLE HINT(i, FORCESEEK), TABLE HINT(i1, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'AddressChange','Start', @Message, @RowsProcessed = @@ROWCOUNT

	SELECT	a.ClientID, a.AddressType, a.FromDate, a.ToDate, a.Address1, a.Address2, a.Suburb, a.State, a.PostCode, a.Country, a.Phone, a.Fax, a.Email, a.Mobile, a.altEmail
	INTO	#ClientAddresses
	FROM	#Candidates c
			INNER JOIN [$(Acquisition)].[IntraBet].tblClientAddresses a ON a.ClientID = c.ClientID AND a.FromDate <= c.FromDate

	SELECT	ClientId, AddressType, MAX(AddressLastChanged) AddressLastChanged, MAX(PhoneLastChanged) PhoneLastChanged, MAX(EmailLastChanged) EmailLastChanged
	INTO	#AddressChange
	FROM	(	SELECT	ClientID, AddressType, MAX(FromDate) AddressLastChanged, CONVERT(datetime2(3), NULL) PhoneLastChanged, CONVERT(datetime2(3), NULL) EmailLastChanged
				FROM	(	SELECT	ClientID, AddressType, FromDate 
							FROM	(	SELECT  ClientID, AddressType, FromDate, ISNULL(Address1,'') Address1, ISNULL(Address2,'') Address2, ISNULL(Suburb,'') Suburb, ISNULL(State,'') State, ISNULL(PostCode,'') PostCode, ISNULL(Country,'') Country FROM #ClientAddresses
										UNION ALL
										SELECT  ClientID, AddressType, ToDate, ISNULL(Address1,'') Address1, ISNULL(Address2,'') Address2, ISNULL(Suburb,'') Suburb, ISNULL(State,'') State, ISNULL(PostCode,'') PostCode, ISNULL(Country,'') Country FROM #ClientAddresses 
										WHERE	ToDate <= @ToDate
									) x
							GROUP BY  ClientID, AddressType, FromDate, Address1, Address2, Suburb, State, PostCode, Country
							HAVING COUNT(*) <> 2
						) y
				GROUP BY ClientID, AddressType
				HAVING	COUNT(*) > 1 -- will always include initial version of record, so must be more than one version for a change to actually have taken place
				UNION ALL
				SELECT	ClientID, AddressType, CONVERT(datetime2(3), NULL) AddressLastChanged, MAX(FromDate) PhoneLastChanged, CONVERT(datetime2(3), NULL) EmailLastChanged
				FROM	(	SELECT	ClientID, AddressType, FromDate 
							FROM	(	SELECT  ClientID, AddressType, FromDate, ISNULL(Phone,'') Phone, ISNULL(Fax,'') Fax, ISNULL(Mobile,'') Mobile FROM #ClientAddresses
										UNION ALL
										SELECT  ClientID, AddressType, ToDate, ISNULL(Phone,'') Phone, ISNULL(Fax,'') Fax, ISNULL(Mobile,'') Mobile FROM #ClientAddresses 
										WHERE	ToDate <= @ToDate
									) x
							GROUP BY  ClientID, AddressType, FromDate, Phone, Fax, Mobile
							HAVING COUNT(*) <> 2
						) y
				GROUP BY ClientID, AddressType
				HAVING	COUNT(*) > 1 -- will always include initial version of record, so must be more than one version for a change to actually have taken place
				UNION ALL
				SELECT	ClientID, AddressType, CONVERT(datetime2(3), NULL) AddressLastChanged, CONVERT(datetime2(3), NULL) PhoneLastChanged, MAX(FromDate) EmailLastChanged
				FROM	(	SELECT	ClientID, AddressType, FromDate 
							FROM	(	SELECT  ClientID, AddressType, FromDate, ISNULL(Email,'') Email, ISNULL(altEmail,'') altEmail FROM #ClientAddresses
										UNION ALL
										SELECT  ClientID, AddressType, ToDate, ISNULL(Email,'') Email, ISNULL(altEmail,'') altEmail FROM #ClientAddresses 
										WHERE	ToDate <= @ToDate
									) x
							GROUP BY  ClientID, AddressType, FromDate, Email, altEmail
							HAVING COUNT(*) <> 2
						) y
				GROUP BY ClientID, AddressType
				HAVING	COUNT(*) > 1 -- will always include initial version of record, so must be more than one version for a change to actually have taken place
			) z
	GROUP BY ClientId, AddressType
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'RewardEligibility','Start', @Message, @RowsProcessed = @@ROWCOUNT

	SELECT	c.ClientId, c.FromDate, MAX(r.FromDate) MessageFromDate
	INTO	#RewardsEligibilityKeys
	FROM	#Candidates c
			INNER JOIN [$(Acquisition)].[Messaging].[RewardsUpdateEligibility] r ON r.ClientID = c.ClientID AND r.FromDate <= c.FromDate
	GROUP BY c.ClientId, c.FromDate
	OPTION (RECOMPILE)

	-- It is possible to get multiple similtaneous eligibility messages, so sollect the distinct list of exclusion reasons as a bitmap value with 0 as not excluded
	SELECT	k.ClientID, k.FromDate, CASE MAX(CONVERT(int,r.Excluded)) WHEN 0 THEN 0 ELSE SUM(DISTINCT POWER(2,r.ExclusionReason-1)) END RewardStatusId, CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),MIN(r.ExecutedAt)))  RewardStatusDate
	INTO	#RewardsEligibility
	FROM	#RewardsEligibilityKeys k
			INNER JOIN [$(Acquisition)].[Messaging].[RewardsUpdateEligibility] r ON r.ClientID = k.ClientID AND r.FromDate = k.MessageFromDate
	GROUP BY k.ClientID, k.FromDate
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'RewardVelocityLink','Start', @Message, @RowsProcessed = @@ROWCOUNT

	SELECT	ClientId, FromDate, MAX(MessageFromDate) MessageFromDate
	INTO	#RewardsVelocityLinkKeys
	FROM	(	SELECT	c.ClientId, c.FromDate, r.FromDate MessageFromDate
				FROM	#Candidates c
						INNER JOIN [$(Acquisition)].[Messaging].[RewardsCreateVelocityLink] r ON r.ClientID = c.ClientID AND r.FromDate <= c.FromDate
				UNION ALL
				SELECT	c.ClientId, c.FromDate, r.FromDate MessageFromDate
				FROM	#Candidates c
						INNER JOIN [$(Acquisition)].[Messaging].[RewardsUpdateVelocityLink] r ON r.ClientID = c.ClientID AND r.FromDate <= c.FromDate
			) x
	GROUP BY ClientId, FromDate
	OPTION (RECOMPILE)

	SELECT	k.ClientID, k.FromDate, ISNULL(u.VelocityNumber, c.VelocityNumber) VelocityNumber, CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),ISNULL(u.ExecutedAt, c.ExecutedAt)))  VelocityLinkedDate
	INTO	#RewardsVelocityLink
	FROM	#RewardsVelocityLinkKeys k
			LEFT JOIN [$(Acquisition)].[Messaging].[RewardsCreateVelocityLink] c ON c.ClientID = k.ClientID AND c.FromDate = k.MessageFromDate
			LEFT JOIN [$(Acquisition)].[Messaging].[RewardsUpdateVelocityLink] u ON u.ClientID = k.ClientID AND u.FromDate = k.MessageFromDate
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Assemble','Start', @Message, @RowsProcessed = @@ROWCOUNT

	SELECT	x.AccountID LedgerId,
			'IAA' as LedgerSource,
			CASE ISNULL(C.CB_Client_key,0) WHEN 0 THEN x.AccountID ELSE c.CB_Client_key END LegacyLedgerID,
			x.AccountNumber LedgerSequenceNumber,
			ISNULL(dd.AccountOpenedDate, ci.StartDate) LedgerOpenedDate,
			CONVERT(char(10),CONVERT(date,ISNULL(dd.AccountOpenedDate, ci.StartDate))) LedgerOpenedDayText,
			ISNULL(dd.Origin, 'Client') LedgerOpenedDateOrigin,
			a.Ledger LedgerTypeID,
			ISNULL(l.LedgerDescription,'Unknown') LedgerType,
			x.ClientID AccountID,
			'IAC' AccountSource,
			c.PIN,
			c.Username,
			c.PWDHash PasswordHash,
			pw.PasswordLastChanged,
			ISNULL(CONVERT(varchar(36), sc.exactTargetID),'N/A') ExactTargetID,
			c.ClientType AccountFlags,
			ISNULL(ci.Channel, -1) AccountOpenedChannelId,
			ISNULL(f.ReferrerClientId,0) ReferralAccountID,
			CASE WHEN f.ReferrerClientId IS NULL THEN 'N/A' ELSE 'IAC' END ReferralAccountSource,
			ISNULL(f.BurnCode, 'N/A') ReferralBurnCode,
			CONVERT(datetime2(3),DATEADD(minute,@MessagingTimeShift,f.BurntDateUTC)) ReferralBurntDate,
			cai.BTag2 SourceBTag2,
			cai.TrafficSource SourceTrafficSource,
			cai.RefURL SourceRefURL,
			cai.CampaignID SourceCampaignID,
			cai.Keywords SourceKeywords,
			af.AffiliateName COLLATE database_default SourceAffiliateName,
			af.SitePromoName COLLATE database_default SourceSitePromoName,
			CONVERT(VARCHAR(20), tad.Channel) [TrackingChannel],
			CONVERT(VARCHAR(20), tad.App_Id) [TrackingAppId],
			CONVERT(VARCHAR(50), tad.[App_Name]) [TrackingAppName],
			CONVERT(VARCHAR(20), tad.App_Version) [TrackingAppVersion],
			CONVERT(VARCHAR(255), tad.Event_Name) [TrackingEventName],
			CONVERT(VARCHAR(50), tad.Tracker) [TrackingTracker],
			CONVERT(VARCHAR(MAX), tad.Tracker_Name) [TrackingTrackerName],
			CONVERT(VARCHAR(255), tad.Adwords_Campaign_Type) [TrackingAdwordsCampaignType],
			CONVERT(VARCHAR(255), tad.Adwords_Campaign_Name) [TrackingAdwordsCampaignName],
			CONVERT(VARCHAR(255), tad.Fb_Campaign_Group_Name) [TrackingFbCampaignGroupName],
			CONVERT(VARCHAR(255), tad.Fb_Campaign_Name) [TrackingFbCampaignName],
			CONVERT(VARCHAR(255), tad.Ip_Address) [TrackingIpAddress],
			CONVERT(VARCHAR(MAX), tad.Partner_Parameters) [TrackingPartnerParameters],
			CONVERT(VARCHAR(30), tad.dcp_btag) [TrackingDcpBtag],
			CONVERT(VARCHAR(30), tad.dcp_campaign) [TrackingDcpCampaign],
			CONVERT(VARCHAR(30), tad.dcp_keyword) [TrackingDcpKeyword],
			CONVERT(VARCHAR(30), tad.dcp_publisher) [TrackingDcpPublisher],
			CONVERT(VARCHAR(30), tad.dcp_source) [TrackingDcpSource],
			CONVERT(VARCHAR(30), tad.label) [TrackingLabel],
			CONVERT(VARCHAR(30), tad.publisher) [TrackingPublisher],
			CONVERT(VARCHAR(30), tad.keyword) [TrackingKeyword],
			CONVERT(VARCHAR(30), tad.campaign) [TrackingCampaign],
			CONVERT(VARCHAR(30), tad.btag) [TrackingBtag],
			CONVERT(VARCHAR(100), tad.source) [TrackingSource],
			ISNULL(sm.[Description], 'Unknown') StatementMethod,
			ISNULL(sf.[Description], 'Unknown') StatementFrequency,
			cd.ExternalAccountId CardAccountId,
			cs.CardFirstIssueDate,
			cs.CardIssueDate,
			cd.CardStatus CardStatusId,
			ew.AccountId EachwayAccountId,
			ISNULL(re.RewardStatusId,0) RewardStatusId,
			re.RewardStatusDate,
			vl.VelocityNumber, 
			vl.VelocityLinkedDate,
			c.Title AccountTitle,
			c.Surname AccountSurname,
			c.FirstName AccountFirstName,
			c.MiddleName AccountMiddleName,
			c.Gender AccountGender,
			c.DOB AccountDOB,
			CASE WHEN ha.AddressType IS NOT NULL THEN 1 ELSE 0 END HasHomeAddress,
			hl.AddressLastChanged HomeAddressLastChanged,
			hl.PhoneLastChanged HomePhoneLastChanged,
			hl.EmailLastChanged HomeEmailLastChanged,
			ha.Address1 HomeAddress1,
			ha.Address2 HomeAddress2,
			ha.Suburb HomeSuburb,
			ha.State HomeStateCode,
			hs.StateName HomeState,
			ha.PostCode HomePostCode,
			hc.CountryAbbr HomeCountryCode,
			hc.CountryName HomeCountry,
			ha.Phone HomePhone,
			ha.Mobile HomeMobile,
			ha.Fax HomeFax,
			ha.Email HomeEmail,
			ha.altEmail HomeAlternateEmail,
			CASE WHEN pa.AddressType IS NOT NULL THEN 1 ELSE 0 END HasPostalAddress,
			pl.AddressLastChanged PostalAddressLastChanged,
			pl.PhoneLastChanged PostalPhoneLastChanged,
			pl.EmailLastChanged PostalEmailLastChanged,
			pa.Address1 PostalAddress1,
			pa.Address2 PostalAddress2,
			pa.Suburb PostalSuburb,
			pa.State PostalStateCode,
			ps.StateName PostalState,
			pa.PostCode PostalPostCode,
			pc.CountryAbbr PostalCountryCode,
			pc.CountryName PostalCountry,
			pa.Phone PostalPhone,
			pa.Mobile PostalMobile,
			pa.Fax PostalFax,
			pa.Email PostalEmail,
			pa.altEmail PostalAlternateEmail,
			CASE WHEN ba.AddressType IS NOT NULL THEN 1 ELSE 0 END HasBusinessAddress,
			bl.AddressLastChanged BusinessAddressLastChanged,
			bl.PhoneLastChanged BusinessPhoneLastChanged,
			bl.EmailLastChanged BusinessEmailLastChanged,
			ba.Address1 BusinessAddress1,
			ba.Address2 BusinessAddress2,
			ba.Suburb BusinessSuburb,
			ba.State BusinessStateCode,
			bs.StateName BusinessState,
			ba.PostCode BusinessPostCode,
			bc.CountryAbbr BusinessCountryCode,
			bc.CountryName BusinessCountry,
			ba.Phone BusinessPhone,
			ba.Mobile BusinessMobile,
			ba.Fax BusinessFax,
			ba.Email BusinessEmail,
			ba.altEmail BusinessAlternateEmail,
			CASE WHEN oa.AddressType IS NOT NULL THEN 1 ELSE 0 END HasOtherAddress,
			ol.AddressLastChanged OtherAddressLastChanged,
			ol.PhoneLastChanged OtherPhoneLastChanged,
			ol.EmailLastChanged OtherEmailLastChanged,
			oa.Address1 OtherAddress1,
			oa.Address2 OtherAddress2,
			oa.Suburb OtherSuburb,
			oa.State OtherStateCode,
			os.StateName OtherState,
			oa.PostCode OtherPostCode,
			oc.CountryAbbr OtherCountryCode,
			oc.CountryName OtherCountry,
			oa.Phone OtherPhone,
			oa.Mobile OtherMobile,
			oa.Fax OtherFax,
			oa.Email OtherEmail,
			oa.altEmail OtherAlternateEmail,
			ISNULL(l1.CorrectedDate,x.FirstDate) FirstDate,
			ISNULL(l2.CorrectedDate,x.FromDate) FromDate
	INTO	#Account
	FROM	#Candidates x
			INNER JOIN [$(Acquisition)].IntraBet.tblAccounts a ON a.AccountID = x.AccountID AND a.FromDate <= x.FromDate AND a.ToDate > x.FromDate
			LEFT JOIN #DerivedOpenDate dd ON dd.AccountID = x.AccountID
			LEFT JOIN #PasswordChange pw ON pw.ClientID = x.ClientID
			LEFT JOIN [$(Acquisition)].IntraBet.tblLULedgers l ON l.Ledger = a.Ledger AND l.FromDate <= x.FromDate AND l.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClients c ON c.ClientID = x.ClientID AND c.FromDate <= x.FromDate AND c.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClientInfo ci ON ci.ClientID = x.ClientID AND ci.FromDate <= x.FromDate AND ci.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.Friend f ON f.FriendClientId = x.ClientID AND f.FromDate <= x.FromDate AND f.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblSubscriptionCentre sc ON sc.clientID = x.ClientID AND sc.FromDate <= x.FromDate AND sc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblClientAffiliateInfo cai ON cai.clientID = x.ClientID AND cai.FromDate <= x.FromDate AND cai.ToDate > x.FromDate
			LEFT JOIN #Affiliates af ON af.BTag2 = cai.BTag2 AND af.FromDate <= x.FromDate AND af.ToDate > x.FromDate
			LEFT JOIN #TrackingAdjustData tad ON tad.clientID = x.ClientID AND tad.FromDate <= x.FromDate AND tad.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblLUStatementMethods sm ON sm.StatementMethod = ci.StatementMethod AND sm.FromDate <= x.FromDate AND sm.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].IntraBet.tblLUStatementFrequency sf ON sf.StatementFrequency = ci.StatementFrequency AND sf.FromDate <= x.FromDate AND sf.ToDate > x.FromDate
			LEFT JOIN #CardIssue cs ON cs.ClientID = x.ClientID
			LEFT JOIN [$(Acquisition)].[IntraBet].Cards cd ON cd.ClientId = x.ClientID AND cd.Created = cs.CardIssueDate AND cd.FromDate <= x.FromDate AND cd.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].EachwayAccounts ew ON ew.ClientId = x.ClientID AND ew.ExternalAccountId = cd.ExternalAccountId AND ew.FromDate <= x.FromDate AND ew.ToDate > x.FromDate
			LEFT JOIN #RewardsEligibility re ON re.ClientId = x.ClientId AND re.FromDate = x.FromDate
			LEFT JOIN #RewardsVelocityLink vl ON vl.ClientId = x.ClientId AND vl.FromDate = x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses ha ON ha.ClientID = x.ClientID AND ha.FromDate <= x.FromDate AND ha.ToDate > x.FromDate AND ha.AddressType=1
			LEFT JOIN #AddressChange hl ON hl.ClientID = x.ClientID AND hl.AddressType=1
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates hs ON hs.Code = ha.StateId AND hs.FromDate <= x.FromDate AND hs.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry hc ON hc.Country = ha.Country AND hc.FromDate <= x.FromDate AND hc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses pa ON pa.ClientID = x.ClientID AND pa.FromDate <= x.FromDate AND pa.ToDate > x.FromDate AND pa.AddressType=2
			LEFT JOIN #AddressChange pl ON pl.ClientID = x.ClientID AND pl.AddressType=2
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates ps ON ps.Code = pa.StateId AND ps.FromDate <= x.FromDate AND ps.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry pc ON pc.Country = pa.Country AND pc.FromDate <= x.FromDate AND pc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses ba ON ba.ClientID = x.ClientID AND ba.FromDate <= x.FromDate AND ba.ToDate > x.FromDate AND ba.AddressType=3
			LEFT JOIN #AddressChange bl ON bl.ClientID = x.ClientID AND bl.AddressType=3
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates bs ON bs.Code = ba.StateId AND bs.FromDate <= x.FromDate AND bs.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry bc ON bc.Country = ba.Country AND bc.FromDate <= x.FromDate AND bc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblClientAddresses oa ON oa.ClientID = x.ClientID AND oa.FromDate <= x.FromDate AND oa.ToDate > x.FromDate AND oa.AddressType=4
			LEFT JOIN #AddressChange ol ON ol.ClientID = x.ClientID AND ol.AddressType=4
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUStates os ON os.Code = oa.StateId AND os.FromDate <= x.FromDate AND os.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].[IntraBet].tblLUCountry oc ON oc.Country = oa.Country AND oc.FromDate <= x.FromDate AND oc.ToDate > x.FromDate
			LEFT JOIN [$(Acquisition)].Intrabet.Latency  l1 WITH(FORCESEEK)  on l1.OriginalDate = x.FirstDate --AND l1.OriginalDate >= @FromDate
			LEFT JOIN [$(Acquisition)].Intrabet.Latency  l2 WITH(FORCESEEK)  on l2.OriginalDate = x.FromDate --AND l2.OriginalDate >= @FromDate
	OPTION (RECOMPILE, FORCE ORDER, LOOP JOIN) --TABLE HINT(c, FORCESEEK), TABLE HINT(ci, FORCESEEK), TABLE HINT(cai, FORCESEEK), TABLE HINT(sc, FORCESEEK), TABLE HINT(ha, FORCESEEK), TABLE HINT(pa, FORCESEEK), TABLE HINT(ba, FORCESEEK), TABLE HINT(oa, FORCESEEK))

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Stage','Start', @Message, @RowsProcessed = @@ROWCOUNT

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT	ISNULL(d.DayKey,-1) LedgerOpenedDayKey, ISNULL(c.ChannelKey,-1) AccountOpenedChannelKey,
			LedgerID, LedgerSource, LegacyLedgerID, LedgerSequenceNumber, LedgerOpenedDate, LedgerOpenedDateOrigin, 
			LedgerTypeID, LedgerType,
			AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
			ReferralAccountID, ReferralAccountSource, ReferralBurnCode, ReferralBurntDate,
			EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, VelocityNumber, VelocityLinkedDate, RewardStatusDate,
			CardStatusId, RewardStatusId,
			CONVERT(char(32),HASHBYTES('MD5',StatementMethod+'|'+StatementFrequency),2) StatementID, StatementMethod, StatementFrequency,
			AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB,
			HasHomeAddress,HomeAddressLastChanged, HomePhoneLastChanged, HomeEmailLastChanged, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
			HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail, 
			HasPostalAddress, PostalAddressLastChanged, PostalPhoneLastChanged, PostalEmailLastChanged, PostalAddress1, PostalAddress2, PostalSuburb,PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry,
			PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail, 
			HasBusinessAddress,	BusinessAddressLastChanged, BusinessPhoneLastChanged, BusinessEmailLastChanged, BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
			BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail, 
			HasOtherAddress, OtherAddressLastChanged, OtherPhoneLastChanged, OtherEmailLastChanged, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry,
			OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail, 
			SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignID, SourceKeywords, 
			SourceAffiliateName, SourceSitePromoName, TrackingChannel, TrackingAppId, TrackingAppName, TrackingAppVersion, TrackingEventName, TrackingTracker, TrackingTrackerName,
			TrackingAdwordsCampaignType, TrackingAdwordsCampaignName, TrackingFbCampaignGroupName, TrackingFbCampaignName, TrackingIpAddress, TrackingPartnerParameters,
			TrackingDcpBtag, TrackingDcpCampaign, TrackingDcpKeyword, TrackingDcpPublisher, TrackingDcpSource, TrackingLabel, TrackingPublisher, TrackingKeyword,
			TrackingCampaign, TrackingBtag, TrackingSource,
			a.FirstDate, a.FromDate
	INTO	#Stage
	FROM	#Account a
			LEFT JOIN [$(Dimensional)].Dimension.[DayZone] d ON d.MasterDayText = a.LedgerOpenedDayText AND d.FromDate <= a.LedgerOpenedDate AND d.ToDate > a.LedgerOpenedDate
			LEFT JOIN [$(Dimensional)].Dimension.[Channel] c ON c.ChannelId = a.AccountOpenedChannelId
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log @BatchKey,@Me,'Dimension','Start',@Message, @RowsProcessed = @@ROWCOUNT

	SELECT	AccountKey, LedgerOpenedDayKey, AccountOpenedChannelKey,
			d.LedgerID, d.LedgerSource, LegacyLedgerID, LedgerSequenceNumber, LedgerOpenedDate, LedgerOpenedDateOrigin, 
			LedgerTypeID, LedgerType,
			AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
			ReferralAccountID, ReferralAccountSource, ReferralBurnCode, ReferralBurntDate,
			EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, VelocityNumber, VelocityLinkedDate, RewardStatusDate,
			CardStatusId, RewardStatusId,
			StatementID, StatementMethod, StatementFrequency,
			AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB,
			HasHomeAddress,HomeAddressLastChanged, HomePhoneLastChanged, HomeEmailLastChanged, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
			HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail, 
			HasPostalAddress, PostalAddressLastChanged, PostalPhoneLastChanged, PostalEmailLastChanged, PostalAddress1, PostalAddress2, PostalSuburb,PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry,
			PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail, 
			HasBusinessAddress,	BusinessAddressLastChanged, BusinessPhoneLastChanged, BusinessEmailLastChanged, BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
			BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail, 
			HasOtherAddress, OtherAddressLastChanged, OtherPhoneLastChanged, OtherEmailLastChanged, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry,
			OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail, 
			SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignID, SourceKeywords, 
			SourceAffiliateName, SourceSitePromoName, TrackingChannel, TrackingAppId, TrackingAppName, TrackingAppVersion, TrackingEventName, TrackingTracker, TrackingTrackerName,
			TrackingAdwordsCampaignType, TrackingAdwordsCampaignName, TrackingFbCampaignGroupName, TrackingFbCampaignName, TrackingIpAddress, TrackingPartnerParameters,
			TrackingDcpBtag, TrackingDcpCampaign, TrackingDcpKeyword, TrackingDcpPublisher, TrackingDcpSource, TrackingLabel, TrackingPublisher, TrackingKeyword,
			TrackingCampaign, TrackingBtag, TrackingSource,
			FirstDate, FromDate
	INTO	#Dimension
	FROM	[$(Dimensional)].Dimension.Account d
			INNER JOIN (SELECT DISTINCT LedgerId, LedgerSource FROM #Stage) s ON s.LedgerId = d.LedgerID AND s.LedgerSource = d.LedgerSource
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Differences','Start',@Message,@RowsProcessed = @@ROWCOUNT

	SELECT	MAX(MAX(AccountKey)) OVER (PARTITION BY LedgerId, LedgerSource) AccountKey, LedgerOpenedDayKey, AccountOpenedChannelKey,
			LedgerID, LedgerSource, LegacyLedgerID, LedgerSequenceNumber, LedgerOpenedDate, LedgerOpenedDateOrigin, 
			LedgerTypeID, LedgerType,
			AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
			ReferralAccountID, ReferralAccountSource, ReferralBurnCode, ReferralBurntDate,
			EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, VelocityNumber, VelocityLinkedDate, RewardStatusDate,
			CardStatusId, RewardStatusId,
			StatementID, StatementMethod, StatementFrequency,
			AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB,
			HasHomeAddress,HomeAddressLastChanged, HomePhoneLastChanged, HomeEmailLastChanged, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
			HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail, 
			HasPostalAddress, PostalAddressLastChanged, PostalPhoneLastChanged, PostalEmailLastChanged, PostalAddress1, PostalAddress2, PostalSuburb,PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry,
			PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail, 
			HasBusinessAddress,	BusinessAddressLastChanged, BusinessPhoneLastChanged, BusinessEmailLastChanged, BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
			BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail, 
			HasOtherAddress, OtherAddressLastChanged, OtherPhoneLastChanged, OtherEmailLastChanged, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry,
			OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail, 
			SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignID, SourceKeywords, 
			SourceAffiliateName, SourceSitePromoName, TrackingChannel, TrackingAppId, TrackingAppName, TrackingAppVersion, TrackingEventName, TrackingTracker, TrackingTrackerName,
			TrackingAdwordsCampaignType, TrackingAdwordsCampaignName, TrackingFbCampaignGroupName, TrackingFbCampaignName, TrackingIpAddress, TrackingPartnerParameters,
			TrackingDcpBtag, TrackingDcpCampaign, TrackingDcpKeyword, TrackingDcpPublisher, TrackingDcpSource, TrackingLabel, TrackingPublisher, TrackingKeyword,
			TrackingCampaign, TrackingBtag, TrackingSource,
			MIN(FirstDate) FirstDate, FromDate, MAX(src) src,
			CASE WHEN COUNT(*) OVER (PARTITION BY LedgerId, LedgerSource) = 2 THEN 'U' WHEN MAX(src) = 'S' THEN 'I' WHEN MAX(src) = 'F' THEN 'D' ELSE 'X' END Upsert
			INTO	#Differences
	FROM	(	SELECT NULL AccountKey, *, 'S' src FROM #Stage
				UNION ALL
				SELECT *, 'D' src FROM #Dimension
			) x
	GROUP BY LedgerOpenedDayKey, AccountOpenedChannelKey,
			LedgerID, LedgerSource, LegacyLedgerID, LedgerSequenceNumber, LedgerOpenedDate, LedgerOpenedDateOrigin, 
			LedgerTypeID, LedgerType,
			AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
			ReferralAccountID, ReferralAccountSource, ReferralBurnCode, ReferralBurntDate,
			EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, VelocityNumber, VelocityLinkedDate, RewardStatusDate,
			CardStatusId, RewardStatusId,
			StatementID, StatementMethod, StatementFrequency,
			AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB,
			HasHomeAddress,HomeAddressLastChanged, HomePhoneLastChanged, HomeEmailLastChanged, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
			HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail, 
			HasPostalAddress, PostalAddressLastChanged, PostalPhoneLastChanged, PostalEmailLastChanged, PostalAddress1, PostalAddress2, PostalSuburb,PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry,
			PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail, 
			HasBusinessAddress,	BusinessAddressLastChanged, BusinessPhoneLastChanged, BusinessEmailLastChanged, BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
			BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail, 
			HasOtherAddress, OtherAddressLastChanged, OtherPhoneLastChanged, OtherEmailLastChanged, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry,
			OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail, 
			SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignID, SourceKeywords,
			SourceAffiliateName, SourceSitePromoName, TrackingChannel, TrackingAppId, TrackingAppName, TrackingAppVersion, TrackingEventName, TrackingTracker, TrackingTrackerName,
			TrackingAdwordsCampaignType, TrackingAdwordsCampaignName, TrackingFbCampaignGroupName, TrackingFbCampaignName, TrackingIpAddress, TrackingPartnerParameters,
			TrackingDcpBtag, TrackingDcpCampaign, TrackingDcpKeyword, TrackingDcpPublisher, TrackingDcpSource, TrackingLabel, TrackingPublisher, TrackingKeyword,
			TrackingCampaign, TrackingBtag, TrackingSource,			
			CASE @FromDate WHEN CONVERT(datetime2(3),'1900-01-01') THEN FirstDate ELSE NULL END, FromDate -- We only want FirstDate to factor in the diff during a RELOAD, otherwise it is just payload added to a new insert
	HAVING	COUNT(*) = 1
	OPTION (RECOMPILE)

	IF @DryRun = 1 
		SELECT	TOP 10000 
				CASE WHEN Upsert = 'U' AND src = 'D' THEN 'U-' WHEN Upsert = 'U' AND src = 'S' THEN 'U+' ELSE Upsert END act, 
				* 
		FROM	#Differences 
		ORDER BY LedgerId, LedgerSource, src desc

	SELECT	* INTO #Prepare FROM #Differences WHERE	Upsert <> 'U' OR src <> 'D' -- Discard the 'before' version record of Update

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @Message, @RowsProcessed = @@ROWCOUNT

	IF @DryRun = 0 
		BEGIN
			UPDATE	d
			SET		LedgerOpenedDayKey = s.LedgerOpenedDayKey,
					AccountOpenedChannelKey = s.AccountOpenedChannelKey,
					LegacyLedgerID = s.LegacyLedgerID, 
					LedgerSequenceNumber = s.LedgerSequenceNumber, 
					LedgerOpenedDate = s.LedgerOpenedDate, 
					LedgerOpenedDateOrigin = s.LedgerOpenedDateOrigin,
					LedgerTypeID = s.LedgerTypeID,
					LedgerType = s.LedgerType, 
					AccountID = s.AccountID,
					AccountSource = s.AccountSource,
					PIN = s.PIN,
					UserName = s.UserName,
					PasswordHash = s.PasswordHash,
					PasswordLastChanged = s.PasswordLastChanged,
					ExactTargetID = s.ExactTargetID,
					AccountFlags = s.AccountFlags,
					ReferralAccountID = s.ReferralAccountID, 
					ReferralAccountSource = s.ReferralAccountSource, 
					ReferralBurnCode = s.ReferralBurnCode, 
					ReferralBurntDate = s.ReferralBurntDate,
					StatementId = s.StatementId,
					StatementMethod = s.StatementMethod,
					StatementFrequency = s.StatementFrequency,
					CardAccountId = s.CardAccountId,
					CardFirstIssueDate = s.CardFirstIssueDate,
					CardIssueDate = s.CardIssueDate,
					CardStatusId = s.CardStatusId,
					EachwayAccountId = s.EachwayAccountId,
					RewardStatusId = s.RewardStatusId,
					RewardStatusDate = s.RewardStatusDate,
					VelocityNumber = s.VelocityNumber,
					VelocityLinkedDate = s.VelocityLinkedDate,
					AccountTitle = s.AccountTitle,
					AccountSurname = s.AccountSurname,
					AccountFirstName = s.AccountFirstName,
					AccountMiddleName = s.AccountMiddleName,
					AccountGender = s.AccountGender,
					AccountDOB = s.AccountDOB,
					HasHomeAddress = s.HasHomeAddress,
					HomeAddressLastChanged = s.HomeAddressLastChanged,
					HomePhoneLastChanged = s.HomePhoneLastChanged,
					HomeEmailLastChanged = s.HomeEmailLastChanged,
					HomeAddress1 = s.HomeAddress1,
					HomeAddress2 = s.HomeAddress2,
					HomeSuburb = s.HomeSuburb,
					HomeStateCode = s.HomeStateCode,
					HomeState = s.HomeState,
					HomePostCode = s.HomePostCode,
					HomeCountryCode = s.HomeCountryCode,
					HomeCountry = s.HomeCountry,
					HomePhone = s.HomePhone,
					HomeMobile = s.HomeMobile,
					HomeFax = s.HomeFax,
					HomeEmail = s.HomeEmail,
					HomeAlternateEmail = s.HomeAlternateEmail,
					HasPostalAddress = s.HasPostalAddress,
					PostalAddressLastChanged = s.PostalAddressLastChanged,
					PostalPhoneLastChanged = s.PostalPhoneLastChanged,
					PostalEmailLastChanged = s.PostalEmailLastChanged,
					PostalAddress1 = s.PostalAddress1,
					PostalAddress2 = s.PostalAddress2,
					PostalSuburb = s.PostalSuburb,
					PostalStateCode = s.PostalStateCode,
					PostalState = s.PostalState,
					PostalPostCode = s.PostalPostCode,
					PostalCountryCode= s.PostalCountryCode,
					PostalCountry = s.PostalCountry,
					PostalPhone = s.PostalPhone,
					PostalMobile = s.PostalMobile,
					PostalFax = s.PostalFax,
					PostalEmail = s.PostalEmail,
					PostalAlternateEmail = s.PostalAlternateEmail,
					HasBusinessAddress = s.HasBusinessAddress,
					BusinessAddressLastChanged = s.BusinessAddressLastChanged,
					BusinessPhoneLastChanged = s.BusinessPhoneLastChanged,
					BusinessEmailLastChanged = s.BusinessEmailLastChanged,
					BusinessAddress1 = s.BusinessAddress1,
					BusinessAddress2 = s.BusinessAddress2,
					BusinessSuburb = s.BusinessSuburb,
					BusinessStateCode = s.BusinessStateCode,
					BusinessState = s.BusinessState,
					BusinessPostCode = s.BusinessPostCode,
					BusinessCountryCode = s.BusinessCountryCode,
					BusinessCountry = s.BusinessCountry,
					BusinessPhone = s.BusinessPhone,
					BusinessMobile = s.BusinessMobile,
					BusinessFax = s.BusinessFax,
					BusinessEmail = s.BusinessEmail,
					BusinessAlternateEmail = s.BusinessAlternateEmail,
					HasOtherAddress = s.HasOtherAddress,
					OtherAddressLastChanged = s.OtherAddressLastChanged,
					OtherPhoneLastChanged = s.OtherPhoneLastChanged,
					OtherEmailLastChanged = s.OtherEmailLastChanged,
					OtherAddress1 = s.OtherAddress1,
					OtherAddress2 = s.OtherAddress2,
					OtherSuburb = s.OtherSuburb,
					OtherStateCode = s.OtherStateCode,
					OtherState = s.OtherState,
					OtherPostCode = s.OtherPostCode,
					OtherCountryCode = s.OtherCountryCode,
					OtherCountry = s.OtherCountry,
					OtherPhone = s.OtherPhone,
					OtherMobile = s.OtherMobile,
					OtherFax = s.OtherFax,
					OtherEmail = s.OtherEmail,
					OtherAlternateEmail = s.OtherAlternateEmail,
					SourceBTag2 = s.SourceBTag2,
					SourceTrafficSource = s.SourceTrafficSource,
					SourceRefURL = s.SourceRefURL,
					SourceCampaignID = s.SourceCampaignID,
					SourceKeywords = s.SourceKeywords,

			SourceAffiliateName = s.SourceAffiliateName,
			SourceSitePromoName = s.SourceSitePromoName,
			TrackingChannel = s.TrackingChannel,
			TrackingAppId = s.TrackingAppId,
			TrackingAppName = s.TrackingAppName,
			TrackingAppVersion = s.TrackingAppVersion,
			TrackingEventName = s.TrackingEventName,
			TrackingTracker = s.TrackingTracker,
			TrackingTrackerName = s.TrackingTrackerName,
			TrackingAdwordsCampaignType = s.TrackingAdwordsCampaignType,
			TrackingAdwordsCampaignName = s.TrackingAdwordsCampaignName,
			TrackingFbCampaignGroupName = s.TrackingFbCampaignGroupName,
			TrackingFbCampaignName = s.TrackingFbCampaignName,
			TrackingIpAddress = s.TrackingIpAddress,
			TrackingPartnerParameters = s.TrackingPartnerParameters,
			TrackingDcpBtag = s.TrackingDcpBtag, 
			TrackingDcpCampaign = s.TrackingDcpCampaign, 
			TrackingDcpKeyword = s.TrackingDcpKeyword, 
			TrackingDcpPublisher = s.TrackingDcpPublisher, 
			TrackingDcpSource = s.TrackingDcpSource, 
			TrackingLabel = s.TrackingLabel, 
			TrackingPublisher = s.TrackingPublisher, 
			TrackingKeyword = s.TrackingKeyword,
			TrackingCampaign = s.TrackingCampaign, 
			TrackingBtag = s.TrackingBtag, 
			TrackingSource = s.TrackingSource,
			
					FromDate = s.FromDate,
					ModifiedDate = CURRENT_TIMESTAMP,
					ModifiedBatchKey = @BatchKey,
					ModifiedBy = @Me,
					Increment = @Increment
			FROM	#Prepare s
					INNER JOIN [$(Dimensional)].[Dimension].[Account] d on d.AccountKey = s.AccountKey
			WHERE	s.Upsert = 'U'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'U'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @Message, @RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			INSERT [$(Dimensional)].Dimension.Account 
				(	LedgerOpenedDayKey, AccountOpenedChannelKey,
					LedgerID, LedgerSource, LegacyLedgerID, LedgerSequenceNumber, LedgerOpenedDate, LedgerOpenedDateOrigin, 
					LedgerTypeID, LedgerType,
					AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
					ReferralAccountID, ReferralAccountSource, ReferralBurnCode, ReferralBurntDate,
					EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, VelocityNumber, VelocityLinkedDate, RewardStatusDate,
					CardStatusId, RewardStatusId,
					StatementID, StatementMethod, StatementFrequency,
					AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB,
					HasHomeAddress,HomeAddressLastChanged, HomePhoneLastChanged, HomeEmailLastChanged, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
					HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail, 
					HasPostalAddress, PostalAddressLastChanged, PostalPhoneLastChanged, PostalEmailLastChanged, PostalAddress1, PostalAddress2, PostalSuburb,PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry,
					PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail, 
					HasBusinessAddress,	BusinessAddressLastChanged, BusinessPhoneLastChanged, BusinessEmailLastChanged, BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
					BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail, 
					HasOtherAddress, OtherAddressLastChanged, OtherPhoneLastChanged, OtherEmailLastChanged, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry,
					OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail, 
					SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignID, SourceKeywords, 
					SourceAffiliateName, SourceSitePromoName, TrackingChannel, TrackingAppId, TrackingAppName, TrackingAppVersion, TrackingEventName, TrackingTracker, TrackingTrackerName,
					TrackingAdwordsCampaignType, TrackingAdwordsCampaignName, TrackingFbCampaignGroupName, TrackingFbCampaignName, TrackingIpAddress, TrackingPartnerParameters,
					TrackingDcpBtag, TrackingDcpCampaign, TrackingDcpKeyword, TrackingDcpPublisher, TrackingDcpSource, TrackingLabel, TrackingPublisher, TrackingKeyword,
					TrackingCampaign, TrackingBtag, TrackingSource,
					FirstDate, FromDate, ToDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy, Increment
				)
			SELECT	LedgerOpenedDayKey, AccountOpenedChannelKey,
					LedgerID, LedgerSource, LegacyLedgerID, LedgerSequenceNumber, LedgerOpenedDate, LedgerOpenedDateOrigin, 
					LedgerTypeID, LedgerType,
					AccountID, AccountSource, PIN, UserName, PasswordHash, PasswordLastChanged, ExactTargetID, AccountFlags,
					ReferralAccountID, ReferralAccountSource, ReferralBurnCode, ReferralBurntDate,
					EachwayAccountId, CardAccountId, CardFirstIssueDate, CardIssueDate, VelocityNumber, VelocityLinkedDate, RewardStatusDate,
					CardStatusId, RewardStatusId,
					StatementID, StatementMethod, StatementFrequency,
					AccountTitle, AccountSurname, AccountFirstName, AccountMiddleName, AccountGender, AccountDOB,
					HasHomeAddress, HomeAddressLastChanged, HomePhoneLastChanged, HomeEmailLastChanged, HomeAddress1, HomeAddress2, HomeSuburb, HomeStateCode, HomeState, HomePostCode, HomeCountryCode, HomeCountry,
					HomePhone, HomeMobile, HomeFax, HomeEmail, HomeAlternateEmail, 
					HasPostalAddress, PostalAddressLastChanged, PostalPhoneLastChanged, PostalEmailLastChanged, PostalAddress1, PostalAddress2, PostalSuburb,PostalStateCode, PostalState, PostalPostCode, PostalCountryCode, PostalCountry,
					PostalPhone, PostalMobile, PostalFax, PostalEmail, PostalAlternateEmail, 
					HasBusinessAddress, BusinessAddressLastChanged, BusinessPhoneLastChanged, BusinessEmailLastChanged,	BusinessAddress1, BusinessAddress2, BusinessSuburb, BusinessStateCode, BusinessState, BusinessPostCode, BusinessCountryCode, BusinessCountry,
					BusinessPhone, BusinessMobile, BusinessFax, BusinessEmail, BusinessAlternateEmail, 
					HasOtherAddress, OtherAddressLastChanged, OtherPhoneLastChanged, OtherEmailLastChanged, OtherAddress1, OtherAddress2, OtherSuburb, OtherStateCode, OtherState, OtherPostCode, OtherCountryCode, OtherCountry,
					OtherPhone, OtherMobile, OtherFax, OtherEmail, OtherAlternateEmail, 
					SourceBTag2, SourceTrafficSource, SourceRefURL, SourceCampaignID, SourceKeywords, 
					SourceAffiliateName, SourceSitePromoName, TrackingChannel, TrackingAppId, TrackingAppName, TrackingAppVersion, TrackingEventName, TrackingTracker, TrackingTrackerName,
					TrackingAdwordsCampaignType, TrackingAdwordsCampaignName, TrackingFbCampaignGroupName, TrackingFbCampaignName, TrackingIpAddress, TrackingPartnerParameters,
					TrackingDcpBtag, TrackingDcpCampaign, TrackingDcpKeyword, TrackingDcpPublisher, TrackingDcpSource, TrackingLabel, TrackingPublisher, TrackingKeyword,
					TrackingCampaign, TrackingBtag, TrackingSource,					
					FirstDate, FromDate, '9999-12-31' ToDate, CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy, @Increment
			FROM	#Prepare
			WHERE	Upsert = 'I'
			OPTION (RECOMPILE)
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Prepare WHERE Upsert = 'I'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount	

	Update [Control].[ConfigurationItems] Set Value = 'N' Where ConfigurationItem = 'Intrabet.Sp_Account'

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
