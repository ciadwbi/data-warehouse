CREATE PROCEDURE [Dimension].[Sp_RewardDetail]
(
	@BatchKey		int,
	@FromDate		datetime2(3),
	@ToDate			datetime2(3),
	@Increment		int = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int

	-- Rewards messages are timestamped GMT whereas ETL and currently rest of WHA is on Darwin time.
	DECLARE @MessagingFromDate datetime2(3) = DATEADD(minute,-Messaging.TimeShiftMinutes(),@FromDate)
	DECLARE @MessagingToDate datetime2(3) = DATEADD(minute,-Messaging.TimeShiftMinutes(),@ToDate)

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED; -- Don't need sanpshot to guarantee integrity of message sources because they are only inserted, never updated

BEGIN TRY

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Rewards.PointsAccrued','Start', @Reload

	SELECT	CONVERT(varchar(50), 'Rewards.PointsAccrued') TransactionCode, 
			TypeOfBet RewardBetTypeId, 
			CONVERT(char(32),HASHBYTES('MD5','N/A'),2) RewardReasonId, 
			CONVERT(varchar(255),'N/A') RewardReason, 
			'+' DirectionCode,
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),FromDate)) FromDate
	INTO	#RewardDetail
	FROM	(	SELECT	CASE ISNUMERIC(TypeOfBet) WHEN 1 THEN TypeOfBet ELSE -1 END TypeOfBet,
						MIN(FromDate) FromDate
				FROM	[$(Acquisition)].Messaging.RewardsPointsAccrued
				WHERE	FromDate > @MessagingFromDate 
						AND FromDate <= @MessagingToDate
						AND ToDate = CONVERT(datetime2(3),'9999-12-31') 
				GROUP BY CASE ISNUMERIC(TypeOfBet) WHEN 1 THEN TypeOfBet ELSE -1 END
			) x

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Rewards.PointsAdjusted','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	INSERT	#RewardDetail
	SELECT	'Rewards.PointsAdjusted' TransactionCode, 
			-1 RewardBetTypeId, 
			CONVERT(char(32),HASHBYTES('MD5',Reason),2) RewardReasonId, 
			Reason RewardReason, 
			CASE PointsSign WHEN 1 THEN '+' WHEN -1 THEN '-' ELSE 'X' END DirectionCode,
			CONVERT(datetime2(3),DATEADD(minute,Messaging.TimeShiftMinutes(),FromDate)) FromDate
	FROM	(	SELECT	Reason, 
						SIGN(Points) PointsSign,
						MIN(FromDate) FromDate
				FROM	[$(Acquisition)].Messaging.RewardsPointsAdjusted
				WHERE	FromDate > @MessagingFromDate 
						AND FromDate <= @MessagingToDate
						AND ToDate = CONVERT(datetime2(3),'9999-12-31') 
				GROUP BY Reason, SIGN(Points)
			) x

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Prepare','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT	d.RewardDetailKey, s.*, CASE WHEN d.TransactionCode IS NULL THEN 'I' WHEN d.RewardReason <> s.RewardReason THEN 'U' ELSE 'X' END Upsert
	INTO	#Prepare
	FROM	#RewardDetail s
			LEFT JOIN [$(Dimensional)].Dimension.RewardDetail d ON d.TransactionCode = s.TransactionCode AND d.RewardBetTypeId = s.RewardBetTypeId AND d.RewardReasonId = s.RewardReasonId AND d.DirectionCode = s.DirectionCode
	OPTION (RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start', @Reload, @RowsProcessed = @@ROWCOUNT

	UPDATE	d
	SET		RewardReason = s.RewardReason,
			FromDate = s.FromDate,
			ModifiedDate = CURRENT_TIMESTAMP,
			ModifiedBatchKey = @BatchKey,
			ModifiedBy = @Me
	FROM	[$(Dimensional)].Dimension.RewardDetail d
			INNER JOIN #Prepare s on s.RewardDetailKey = d.RewardDetailKey
	WHERE	Upsert = 'U'
	OPTION (RECOMPILE)

	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start', @Reload, @RowsProcessed = @RowCount

	INSERT [$(Dimensional)].Dimension.RewardDetail
		(	TransactionCode, RewardTransactionTypeKey, RewardTransactionTypeCode, RewardTransactionType, RewardTransactionClassKey, RewardTransactionClassCode, RewardTransactionClass, 
			RewardBetTypeKey, RewardBetTypeId, RewardBetTypeCode, RewardBetType, 
			RewardReasonKey, RewardReasonId, RewardReason, RewardReasonTypeKey, RewardReasonTypeCode, RewardReasonType, DirectionCode, 
			FirstDate, FromDate, ToDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy
		)
	SELECT	TransactionCode, -2 TransactionTypeKey, 'P' RewardTransactionTypeCode, 'Pending' RewardTransactionType, -2 RewardTransactionClassKey, 'P' RewardTransactionClassCode, 'Pending' RewardTransactionClass, 
			-2 RewardBetTypeKey, RewardBetTypeId, 'P' RewardBetTypeCode, 'Pending' RewardBetType, 
			-2 RewardReasonKey, RewardReasonId, RewardReason, -2 RewardReasonTypeKey, 'P' RewardReasonTypeCode, 'Pending' RewardReasonType, DirectionCode, 
			FromDate FirstDate, FromDate, '9999-12-31' ToDate, CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy
	FROM	#Prepare 
	WHERE	Upsert = 'I'
	OPTION (RECOMPILE)
	
	SET @RowCount = @@ROWCOUNT
	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success', @RowsProcessed = @RowCount	

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log @BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
