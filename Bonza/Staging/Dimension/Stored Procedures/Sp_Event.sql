﻿CREATE PROC [Dimension].[Sp_Event]
(
	@BatchKey		int,
	@FromDate		datetime2(3),
	@ToDate			datetime2(3),
	@Increment		int = NULL,
	@RowsProcessed	int = 0 OUTPUT
)
AS

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)
	DECLARE @Reload varchar(6) = CASE @FromDate WHEN '1900-01-01' THEN 'RELOAD' ELSE NULL END
	DECLARE @RowCount int
	DECLARE @i int

	SET TRANSACTION ISOLATION LEVEL SNAPSHOT;

BEGIN TRY

	EXEC [Control].Sp_Log @BatchKey,@Me,'Lottery Transactions','Start',@Reload

	SELECT	EventId SourceEventId, 
			'LTE' Source,
			Product SourceEvent, 
			DrawDate SourceEventDate, 
			CONVERT(bigint,MAX(LastJackpot)) SourcePrizePool,
			MIN(SettledDate) ResultedDateTime,
			ISNULL(CONVERT(char(10),CONVERT(date,MIN(SettledDate))),'N/A') ResultedDayText,
			MIN(FromDate) FirstDate,
			MAX(FromDate) FromDate
	INTO	#Stage
	FROM	(	SELECT	*,
						CASE TransactionCode WHEN -1005 THEN FromDate ELSE NULL END PlacedDate, 
						CASE TransactionCode WHEN 1005 THEN FromDate ELSE NULL END SettledDate, 
						FIRST_VALUE(Jackpot) OVER(PARTITION BY Product, DrawDate, CASE WHEN DrawDate IS NULL THEN ISNULL(LinkTransactionId,TransactionId) ELSE NULL END ORDER BY FromDate DESC) LastJackpot, 
						FIRST_VALUE(ISNULL(LinkTransactionId,TransactionId)) OVER(PARTITION BY Product, DrawDate, CASE WHEN DrawDate IS NULL THEN ISNULL(LinkTransactionId,TransactionId) ELSE NULL END ORDER BY FromDate) EventId 
				FROM	Cache.TransactionLottery
				WHERE	FromDate > @FromDate AND FromDate <= @ToDate
			) x
	GROUP BY EventId, Product, DrawDate

	EXEC [Control].Sp_Log @BatchKey,@Me,'Prepare','Start',@Reload,@RowsProcessed = @@ROWCOUNT

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;

	SELECT	ISNULL(e.EventKey, e1.EventKey) EventKey, c.ClassId, ISNULL(c.ClassKey,-1) ClassKey, CASE WHEN s.ResultedDateTime IS NULL THEN 0 ELSE ISNULL(rd.DayKey,-1) END ResultedDayKey, ISNULL(cc.ClassKey,-1) NewClassKey, s.*
	INTO	#Event
	FROM	#Stage s
			LEFT JOIN [$(Dimensional)].Dimension.Event e ON (e.SourceEventId = s.SourceEventId AND e.Source = s.Source) 
			LEFT JOIN [$(Dimensional)].Dimension.Event e1 ON (e1.SourceEvent = s.SourceEvent AND e1.SourceEventDate = s.SourceEventDate) 
			LEFT JOIN [$(Dimensional)].Dimension.DayZone rd ON rd.MasterDayText = s.ResultedDayText AND rd.FromDate <= s.ResultedDateTime AND rd.ToDate > s.ResultedDateTime
			LEFT JOIN [$(Dimensional)].Dimension.Class c ON c.ClassId = -10 and c.Source = 'LOT'
			LEFT JOIN [$(Dimensional)].CC.Class cc ON cc.HashKey = HASHBYTES('MD5','|Lottery|'+s.SourceEvent+'||')
	OPTION (RECOMPILE);

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Update','Start',@Reload,@RowsProcessed = @@ROWCOUNT

	UPDATE d
		SET	d.SourceEvent = s.SourceEvent, 
			d.SourceEventDate = s.SourceEventDate, 
			d.ClassId = s.ClassId, 
			d.ClassKey = s.ClassKey, 
			d.SourcePrizePool = s.SourcePrizePool,
			d.ResultedDateTime = ISNULL(d.ResultedDateTime, s.ResultedDateTime), -- Only update if doesn't already exist
			d.ResultedDayKey = CASE d.ResultedDayKey WHEN 0 THEN s.ResultedDayKey WHEN 1 THEN s.ResultedDayKey ELSE d.ResultedDayKey END, -- Only update if doesn't already exist
			d.FromDate = s.FromDate, 
			d.ModifiedDate = CURRENT_TIMESTAMP, 
			d.ModifiedBatchKey = @BatchKey,
			d.ModifiedBy = @Me,
			d.NewClassKey = s.NewClassKey
	FROM	#Event s
			INNER JOIN [$(Dimensional)].Dimension.Event d ON (d.EventKey = s.EventKey) 
	OPTION(RECOMPILE)

	EXEC [Control].Sp_Log	@BatchKey,@Me,'Insert','Start',@Reload,@RowsProcessed = @@ROWCOUNT

	INSERT INTO [$(Dimensional)].Dimension.Event
			(Source, SourceEventID, EventId, SourceEvent, SourceEventType, ParentEventId, ParentEvent, 
				GrandparentEventId, GrandparentEvent, GreatGrandparentEventId, GreatGrandparentEvent,
				SourceEventDate, ClassID, ClassKey, SourceRaceNumber, Event, Round, RoundSequence, 
				Competition, CompetitionSeason,CompetitionStartDate, CompetitionEndDate,
				Grade, Distance, SourceprizePool, PrizePool, SourceTrackCondition, SourceEventWeather, TrackCondition, EventWeather, EventAbandoned, EventAbandonedDateTime, EventAbandonedDayKey,
				ScheduledDateTime, ScheduledDayKey, ScheduledVenueDateTime,
				ResultedDateTime, ResultedDayKey, ResultedVenueDateTime, 
				SourceVenueID, SourceVenue, SourceVenueType, SourceVenueEventType, SourceVenueEventTypeName, 
				SourceVenueStateCode, SourceVenueState, SourceVenueCountryCode, SourceVenueCountry, 
				VenueId, Venue, VenueType, VenueStateCode, VenueState, VenueCountryCode, VenueCountry, 
				SourceSubSportType,SourceRaceGroup, SourceRaceClass, SourceHybridPricingTemplate, SourceWeightType,
				SubSportType,RaceGroup, RaceClass, HybridPricingTemplate, WeightType, SourceDistance, SourceEventAbandoned,
				SourceLiveStreamPerformId, LiveVideoStreamed, LiveScoreBoard,
				ColumnLock, FromDate, FirstDate, CreatedDate, CreatedBatchKey, CreatedBy, 
				ModifiedDate, ModifiedBatchKey,ModifiedBy,
				NewClassKey,
				SourceSubSportTypeId
				)
	SELECT		Source, SourceEventId, SourceEventId EventId, SourceEvent, NULL SourceEventType, NULL ParentEventId, NULL ParentEvent, 
				NULL GrandparentEventId, NULL GrandparentEvent, NULL GreatGrandparentEventId, NULL GreatGrandparentEvent,
				SourceEventDate, ClassID, ClassKey, NULL SourceRaceNumber, 'Pending' Event, 'Pending' Round, NULL RoundSequence, 
				'Pending' Competition, 'Pending' CompetitionSeason, NULL CompetitionStartDate, NULL CompetitionEndDate,
				'Pending' Grade, 'Pending' Distance, SourcePrizePool, NULL, NULL SourceTrackCondition, NULL SourceEventWeather, 'Pending' TrackCondition, 'Pending' EventWeather, 'Pending' EventAbandoned, NULL EventAbandonedDateTime, 0 EventAbandonedDayKey,
				NULL ScheduledDateTime, NULL ScheduledDayKey, NULL ScheduledVenueDateTime,
				ResultedDateTime, ResultedDayKey, NULL ResultedVenueDateTime,
				NULL SourceVenueId, NULL SourceVenue, NULL SourceVenueType, NULL SourceVenueEventType, NULL SourceVenueEventTypeName, 
				NULL SourceVenueStateCode, NULL SourceVenueState, NULL SourceVenueCountryCode, NULL SourceVenueCountry,
				NULL VenueId, 'Pending' Venue, 'Pending' VenueType, 'PDG' VenueStateCode, 'Pending' VenueState, 'PDG' VenueCountryCode, 'Pending' VenueCountry,
				NULL SourceSubSportType, NULL SourceRaceGroup, NULL SourceRaceClass, NULL SourceHybridPricingTemplate, NULL SourceWeightType, 
				'Pending' SubSportType, 'Pending' RaceGroup, 'Pending' RaceClass, 'Pending' HybridPricingTemplate, 'Pending' WeightType, NULL SourceDistance, NULL SourceEventAbandoned,
				NULL SourceLiveStreamPerformId, 'PDG' LiveVideoStreamed, 'PDG' LiveScoreboard,
				0 ColumnLock, FromDate, FirstDate, CURRENT_TIMESTAMP CreatedDate, @BatchKey CreatedBatchKey, @Me CreatedBy, 
				CURRENT_TIMESTAMP ModifiedDate, @BatchKey ModifiedBatchKey, @Me ModifiedBy, 
				NewClassKey, NULL SourceSubSportTypeId				 
	FROM	#Event
	WHERE	EventKey IS NULL

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success',@RowsProcessed = @@ROWCOUNT

END Try

BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Failed',@ErrorMessage;

	THROW;

END CATCH