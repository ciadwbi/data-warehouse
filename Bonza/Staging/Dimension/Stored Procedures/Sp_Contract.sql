﻿CREATE PROCEDURE [Dimension].[Sp_Contract]
(
	@BatchKey		int,
	@DryRun			bit = 0,
	@RowsProcessed	int = 0 OUTPUT
)
AS 

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

	DECLARE @RowCount int

	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

BEGIN TRY
	
	EXEC [Control].[Sp_Log]	@BatchKey, @Me, 'Legacy', 'Start'

	SELECT 	MAX(CASE WHEN TransactionTypeId IN ('BT','BB','FE','TR') THEN x.BetGroupID ELSE NULL END) BetGroupID,
			MAX(CASE WHEN TransactionTypeId IN ('BT','BB','FE','TR') THEN x.BetGroupIDSource ELSE NULL END) BetGroupIDSource,
			MAX(CASE WHEN TransactionTypeId IN ('BT','BB','FE') THEN x.BetId ELSE NULL END) BetId,
			MAX(CASE WHEN TransactionTypeId IN ('BT','BB','FE') THEN x.BetIdSource ELSE NULL END) BetIdSource,
			LegId,
			LegIdSource,
			MAX(TransactionId) TransactionId,
			MAX(TransactionIdSource) TransactionIdSource,
			MAX(FreeBetID) FreeBetID,
			MAX(CASE ExternalID WHEN '' THEN 'N/A' WHEN 'NULL' THEN 'N/A' ELSE ISNULL(ExternalID,'N/A') END) ExternalID,
			MAX(CASE External1 WHEN '' THEN 'N/A' WHEN 'NULL' THEN 'N/A' ELSE ISNULL(External1,'N/A') END) External1,
			MAX(CASE External2 WHEN '' THEN 'N/A' WHEN 'NULL' THEN 'N/A' ELSE ISNULL(External2,'N/A') END) External2,
			MAX(RequestID) RequestID,
			MAX(WithdrawID) WithdrawID,
			MIN(x.FromDate) FromDate
	INTO	#ContractBuffer
	FROM	Stage.[Transaction] x
	WHERE	BatchKey = @BatchKey
			AND ExistsInDim = 0
			AND TransactionTypeID NOT IN ('WI')
	GROUP BY LegId,
			LegIdSource

	EXEC [Control].[Sp_Log] @BatchKey, @Me, 'Bet', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT	#ContractBuffer
	SELECT 	MAX(BetGroupID) BetGroupID,
			MAX(BetGroupIDSource) BetGroupIDSource,
			MAX(BetId) BetId,
			MAX(BetIdSource) BetIdSource,
			LegId,
			LegIdSource,
			MAX(TransactionId) TransactionId,
			MAX(TransactionIdSource) TransactionIdSource,
			MAX(FreeBetID) FreeBetID,
			'N/A' ExternalID,
			'N/A' External1,
			'N/A' External2,
			NULL RequestID,
			NULL WithdrawID,
			MIN(FromDate) FromDate
	FROM	Atomic.[Bet_New] x
	WHERE	BatchKey = @BatchKey
			AND NOT EXISTS (SELECT 1 FROM #ContractBuffer c WHERE c.LegId = x.LegId AND c.LegIdSource = x.LegIdSource)
	GROUP BY LegId,
			LegIdSource

	EXEC [Control].[Sp_Log] @BatchKey, @Me, 'Transaction', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT	#ContractBuffer
	SELECT 	TransactionId BetGroupID,
			TransactionSource BetGroupIDSource,
			TransactionId BetId,
			TransactionSource BetIdSource,
			TransactionId LegId,
			TransactionSource LegIdSource,
			TransactionId,
			TransactionSource TransactionIdSource,
			0 FreeBetID,
			'N/A' ExternalID,
			'N/A' External1,
			'N/A' External2,
			NULL RequestID,
			NULL WithdrawID,
			MIN(FromDate) FromDate
	FROM	Atomic.[Transaction] x
	WHERE	BatchKey = @BatchKey
			AND NOT EXISTS (SELECT 1 FROM #ContractBuffer c WHERE c.LegId = x.TransactionId AND c.LegIdSource = x.TransactionSource)
	GROUP BY TransactionId,
			TransactionSource

	EXEC [Control].[Sp_Log] @BatchKey, @Me, 'Lottery', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT	#ContractBuffer
	SELECT 	TicketId BetGroupID,
			TicketSource BetGroupIDSource,
			TicketId BetId,
			TicketSource BetIdSource,
			TicketId LegId,
			TicketSource LegIdSource,
			TicketId TransactionId,
			TicketSource TransactionIdSource,
			0 FreeBetID,
			'N/A' ExternalID,
			'N/A' External1,
			'N/A' External2,
			NULL RequestID,
			NULL WithdrawID,
			MIN(FromDate) FromDate
	FROM	Atomic.[Lottery] x
	WHERE	BatchKey = @BatchKey
			AND NOT EXISTS (SELECT 1 FROM #ContractBuffer c WHERE c.LegId = x.TicketId AND c.LegIdSource = x.TicketSource)
	GROUP BY TicketId,
			TicketSource

	EXEC [Control].[Sp_Log] @BatchKey, @Me, 'Withdrawal', 'Start', @RowsProcessed = @@ROWCOUNT

	INSERT	#ContractBuffer
	SELECT 	NULL BetGroupID,
			NULL BetGroupIDSource,
			NULL BetId,
			NULL BetIdSource,
			TransactionId LegId,
			TransactionSource LegIdSource,
			TransactionId,
			TransactionSource TransactionIdSource,
			0 FreeBetID,
			MAX(ExternalID) ExternalID,
			MAX(External1) External1,
			MAX(External2) External2,
			MAX(RequestID) RequestID,
			MAX(WithdrawID) WithdrawID,
			MIN(x.FromDate) FromDate
	FROM	Atomic.[Withdrawal] x
	WHERE	BatchKey = @BatchKey
			AND NOT EXISTS (SELECT 1 FROM #ContractBuffer c WHERE c.LegId = x.TransactionId AND c.LegIdSource = x.TransactionSource)
	GROUP BY TransactionId,
			TransactionSource

	EXEC [Control].[Sp_Log] @BatchKey, @Me, 'Prejoin', 'Start', @RowsProcessed = @@ROWCOUNT

	SELECT	B.*,
			CASE WHEN A.LegId IS NULL THEN 'I'
			ELSE 'U'
			END Upsert
	INTO	#Contract
	FROM	#ContractBuffer B
			LEFT JOIN [$(Dimensional)].Dimension.Contract A ON A.LegId=B.LegId AND A.LegIdSource=B.LegIdSource
	WHERE	A.LegId IS NULL
			OR (A.ExternalID = 'N/A' AND B.ExternalID <> 'N/A')
			OR (A.External1 = 'N/A' AND B.External1 <> 'N/A')
			OR (A.External2 = 'N/A' AND B.External2 <> 'N/A')
			OR (A.RequestID IS NULL AND B.RequestID IS NOT NULL)
			OR (A.WithdrawID IS NULL AND B.WithdrawID IS NOT NULL)
			OR (B.BetGroupIdSource ='IBT' AND ISNULL(A.BetGroupID,-1) <> ISNULL(B.BetGroupID,-1))
			OR (B.BetGroupIdSource ='IBT' AND ISNULL(A.BetGroupIDSource,'!') <> ISNULL(b.BetGroupIDSource,'!'))
	SET @RowCount = @@ROWCOUNT

	IF @DryRun = 1 SELECT * FROM #Contract ORDER BY LegId, LegIdSource

	EXEC [Control].[Sp_Log] @BatchKey, @Me, 'Update', 'Start', @RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			UPDATE	A
			SET		A.BetGroupID		=	CASE WHEN A.BetGroupIdSource ='IBT' THEN B.BetGroupID		ELSE A.BetGroupId END, -- only process changes for non-bets
					A.BetGroupIDSource	=	CASE WHEN A.BetGroupIdSource ='IBT' THEN B.BetGroupIDSource ELSE A.BetGroupIdSource END, -- only process changes for non-bets
					A.ExternalID		=	CASE WHEN A.ExternalID = 'N/A'		THEN B.ExternalID		ELSE A.ExternalID	END,
					A.External1			=	CASE WHEN A.External1 = 'N/A'		THEN B.External1		ELSE A.External1	END,
					A.External2			=	CASE WHEN A.External2 = 'N/A'		THEN B.External2		ELSE A.External2	END,
					A.RequestID			=	CASE WHEN A.RequestID  IS NULL		THEN B.RequestID		ELSE A.RequestID	END,
					A.WithdrawID		=	CASE WHEN A.WithdrawID IS NULL		THEN B.WithdrawID		ELSE A.WithdrawID	END,
					A.ModifiedDate		= GETDATE(),
					A.ModifiedBatchKey	= @BatchKey,
					A.ModifiedBy		= @Me
			FROM	[$(Dimensional)].Dimension.Contract A
					INNER JOIN #Contract B ON A.LegId=B.LegId AND A.LegIdSource=B.LegIdSource
			WHERE	B.Upsert = 'U'
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Contract WHERE Upsert = 'U'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].[Sp_Log] @BatchKey, @Me,'Insert' ,'Start' , @RowsProcessed = @RowCount

	IF @DryRun = 0 
		BEGIN
			INSERT INTO [$(Dimensional)].Dimension.Contract 
				(	BetGroupID,BetGroupIDSource,BetId,BetIdSource,LegId,LegIdSource,
					TransactionID,TransactionIDSource,FreeBetID,
					ExternalID,External1,External2,RequestID,WithdrawID,
					FromDate,ToDate,FirstDate,CreatedDate,CreatedBatchKey,CreatedBy,ModifiedDate,ModifiedBatchKey,ModifiedBy)
			SELECT	A.BetGroupID,A.BetGroupIDSource,A.BetId,A.BetIdSource,A.LegId,A.LegIdSource,
					A.TransactionID,A.TransactionIDSource,A.FreeBetID,
					A.ExternalID,A.External1,A.External2,A.RequestID,A.WithdrawID,
					A.FromDate,'9999-12-31',A.FromDate,GETDATE(),@BatchKey,@Me,GETDATE(),@BatchKey,@Me
			FROM	#Contract A
			WHERE	A.Upsert = 'I'
			SET @RowCount = @@ROWCOUNT
		END
	ELSE
		SELECT @RowCount = COUNT(*) FROM #Contract WHERE Upsert = 'I'

	SET @RowsProcessed = @RowsProcessed + @RowCount

	EXEC [Control].[Sp_Log] @BatchKey, @Me, NULL, 'Success', @RowsProcessed = @RowCount

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH
