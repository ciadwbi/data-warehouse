﻿CREATE PROC [Dimension].[Sp_DimPriceType]
(
	@BatchKey	INT
)
WITH RECOMPILE
AS 
BEGIN TRY

	/* 
	   Developer: Aaron Jackson
	   Date: 07/08/2015
	   Desc: Populate Dim PriceType
	*/

	EXEC [Control].Sp_Log @BatchKey,'Sp_DimPriceType','Type1-UpdateDim','Start';

	UPDATE Dim
	SET Dim.PriceTypeName	=	Stage.PriceTypeName,
		Dim.ModifiedDate	=	Stage.ModifiedDate,
		Dim.ModifiedBatchKey=	Stage.ModifiedBatchKey,
		Dim.ModifiedBy		=	Stage.ModifiedBy
	FROM [$(Dimensional)].Dimension.PriceType Dim
	INNER JOIN 
			(
				SELECT
					Stage.[PriceTypeID],
					Stage.[Source],
					Stage.[PriceTypeName],
					CONVERT(DATETIME2(3),GETDATE()) AS ModifiedDate,
					@BatchKey AS ModifiedBatchKey,
					'Sp_DimPriceType'  AS ModifiedBy
				FROM Stage.PriceType Stage
				LEFT OUTER JOIN [$(Dimensional)].Dimension.PriceType Dim ON Stage.PriceTypeID = Dim.PriceTypeID and Stage.Source = Dim.Source
				WHERE Dim.PriceTypeID IS NOT NULL
				AND Stage.BatchKey = @BatchKey
			) Stage
			ON Dim.PriceTypeID = Stage.PriceTypeID and Dim.Source = Stage.Source;

	EXEC [Control].Sp_Log @BatchKey,'Sp_DimPriceType','InsertIntoDim','Start';

	INSERT INTO [$(Dimensional)].Dimension.PriceType (PriceTypeId, Source, PriceTypeName, FromDate, ToDate, FirstDate, CreatedDate, CreatedBatchKey, CreatedBy, ModifiedDate, ModifiedBatchKey, ModifiedBy)
	SELECT DISTINCT
		Stage.[PriceTypeID],
		Stage.[Source],
		Stage.[PriceTypeName],
		CONVERT(DATETIME2(3),GETDATE()) AS FromDate,
		CONVERT(DATETIME2(3),GETDATE()) AS ToDate,
		CONVERT(DATETIME2(3),GETDATE()) AS FirstDate,
		CONVERT(DATETIME2(3),GETDATE()) AS CreatedDate,
   		@BatchKey AS CreatedBatchKey,
		'Sp_DimPriceType' AS CreatedBy,
		CONVERT(DATETIME2(7),GETDATE()) AS ModifiedDate,
		@BatchKey AS ModifiedBatchKey,
		'Sp_DimPriceType'  AS ModifiedBy
	FROM Stage.PriceType Stage
	LEFT OUTER JOIN [$(Dimensional)].Dimension.PriceType Dim ON Stage.PriceTypeID = Dim.PriceTypeID and Stage.Source = Dim.Source
	WHERE Dim.PriceTypeID IS NULL
	AND Stage.BatchKey = @BatchKey;

	EXEC [Control].Sp_Log @BatchKey,'Sp_DimPriceType',NULL,'Success'
	
END TRY
BEGIN CATCH

	declare @Error int = ERROR_NUMBER();
	declare @ErrorMessage varchar(max) =  ERROR_MESSAGE();
	
	EXEC [Control].Sp_Log @BatchKey, 'Sp_DimPriceType', NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH