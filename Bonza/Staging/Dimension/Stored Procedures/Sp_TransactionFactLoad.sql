﻿CREATE PROC	 [Dimension].[Sp_TransactionFactLoad]
(
	@BatchKey INT,
	@RowCnt   INT OUTPUT
)
AS 
BEGIN

	DECLARE @Me varchar(256) = OBJECT_SCHEMA_NAME(@@PROCID)+'.'+OBJECT_NAME(@@PROCID)

/*
	Developer: Joseph George
	Date: Unknown
	Desc: Transaction Fact Load

	Last Changed By: Edward Chen
	Last Changed Date: 6/JUL/2015
	Last Change Desc: Remove TransactionDetailID Dependency
*/

BEGIN TRY
	
	EXEC [Control].[Sp_Log] @BatchKey,@Me,'InsertIntoFactTable','Start'

	INSERT INTO [$(Dimensional)].Fact.[Transaction] (TransactionId,TransactionIdSource,TransactionDetailKey,BalanceTypeKey,ContractKey,AccountKey,AccountStatusKey,WalletKey,PromotionKey, EventKey, MarketKey, DayKey,MasterTransactionTimestamp,MasterTimeKey,
	AnalysisTransactionTimestamp,AnalysisTimeKey,CampaignKey,ChannelKey,ActionChannelKey,LedgerID,TransactedAmount, PriceTypeKey, BetTypeKey,LegTypeKey, ClassKey,UserKey,InstrumentKey,NoteKey,InPlayKey,FromDate,CreatedDate,CreatedBatchKey,CreatedBy)
	SELECT  
		S.TransactionId,
		S.TransactionIdSource,
		ISNULL(DTDK.TransactionDetailKey,-1) AS TransactionDetailKey,
		ISNULL(DBK.BalanceTypeKey,-1) AS BalanceTypeKey,
		ISNULL(DCOK.ContractKey,-1) AS ContractKey,
		ISNULL(DAC.AccountKey,-1) AS AccountKey,
		CASE WHEN (S.TransactionTypeID = 'TR' and S.TransactionStatusID IN ('CO') and S.TransactionMethodID = 'MI' and S.TransactionDirection = 'DR') 
			 THEN COALESCE(DAK1.AccountStatusKey,DAK.AccountStatusKey,-1) ELSE COALESCE(DAK.AccountStatusKey,-1) END AS AccountStatusKey,
		ISNULL(DWK.WalletKey,-1) AS WalletKey,
		ISNULL(DPK.PromotionKey,-1) AS PromotionKey,
		ISNULL(DE.EventKey,-99) AS EventKey,
		ISNULL(DM.MarketKey,-1) AS MarketKey,
		ISNULL(D.DayKey,-1) AS DayKey,
		S.MasterTransactionTimestamp,
		ISNULL(TMD.TimeKey,-1) AS MasterTimeKey,
		ISNULL(DATEADD(MI,TZ.OffsetMinutes,S.MasterTransactionTimestamp),'1900-01-01') AS AnalysisTransactionTimestamp,
		ISNULL(TAD.TimeKey,-1) AS AnalysisTimeKey,
		ISNULL(DCK.CampaignKey,0) AS CampaignKey,
		ISNULL(DCHK.ChannelKey,0) AS ChannelKey,
		ISNULL(DACK.ChannelKey,0) AS ActionChannelKey,
		S.LedgerID,
		S.TransactedAmount,
		ISNULL(DPTK.PriceTypeKey,-1) as PriceTypeKey, /* PriceType */
		ISNULL(DBT.BetTypeKey,-1) AS BetTypeKey,
		ISNULL(DLTK.LegTypeKey,-1) AS LegTypeKey,
		ISNULL(DCLK.ClassKey,-1) AS ClassKey,
		ISNULL(DUK.UserKey,0) AS UserKey,
		ISNULL(DIK.InstrumentKey,0) InstrumentKey,
		ISNULL(DN.NoteKey,0) NoteKey,
		ISNULL(IP.InPlayKey,-1) InPlayKey,
		CONVERT(DATETIME2(0),S.FromDate) AS FromDate,
		CONVERT(DATETIME2(7),GETDATE()) AS CreatedDate,
		S.BatchKey AS CreatedBatchKey,
		@Me AS CreatedBy
	FROM Stage.[Transaction]			S    WITH(NOLOCK)
	LEFT OUTER JOIN [$(Dimensional)].Dimension.TransactionDetail DTDK WITH(NOLOCK) ON S.TransactionTypeID = DTDK.TransactionTypeID AND S.TransactionStatusID = DTDK.TransactionStatusID AND S.TransactionMethodID = DTDK.TransactionMethodID AND S.TransactionDirection = DTDK.TransactionDirection
	LEFT OUTER JOIN [$(Dimensional)].Dimension.BalanceType		DBK  WITH(NOLOCK) ON S.BalanceTypeID=DBK.BalanceTypeID
	LEFT OUTER JOIN [$(Dimensional)].Dimension.ACCOUNT			DAC  WITH(NOLOCK) ON S.LedgerID=DAC.LedgerID AND S.LedgerSource= DAC.LedgerSource
	LEFT OUTER JOIN [$(Dimensional)].Dimension.ACCOUNTSTATUS	DAK  WITH(NOLOCK) ON S.LedgerID=DAK.LedgerID AND S.LedgerSource= DAK.LedgerSource AND CONVERT(DATETIME2(3),S.MasterTransactionTimestamp) >=DAK.FROMDATE AND CONVERT(DATETIME2(3),S.MasterTransactionTimestamp) <DAK.TODATE
	LEFT OUTER JOIN [$(Dimensional)].Dimension.ACCOUNTSTATUS	DAK1 WITH(NOLOCK) ON S.LedgerID=DAK1.LedgerID AND S.LedgerSource= DAK1.LedgerSource AND CONVERT(DATETIME2(3),S.MasterTransactionTimestamp) >DAK1.FROMDATE AND CONVERT(DATETIME2(3),S.MasterTransactionTimestamp) <=DAK1.TODATE
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Campaign			DCK  WITH(NOLOCK) ON S.CampaignId=DCK.CampaignID
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Wallet			DWK  WITH(NOLOCK) ON S.[WalletId]=DWK.[WalletId]
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Promotion		DPK	 WITH(NOLOCK) ON S.PromotionId = DPK.PromotionId AND S.PromotionIDSource = DPK.Source 
	LEFT OUTER JOIN [$(Dimensional)].Dimension.CHANNEL			DCHK WITH(NOLOCK) ON S.Channel=DCHK.ChannelId
	LEFT OUTER JOIN [$(Dimensional)].Dimension.CHANNEL			DACK WITH(NOLOCK) ON S.ActionChannel=DACK.ChannelId
	LEFT OUTER JOIN [$(Dimensional)].Dimension.[Contract]		DCOK WITH(NOLOCK) ON S.LegId=DCOK.LegId AND S.LegIDSource=DCOK.LegIDSource
	LEFT OUTER JOIN [$(Dimensional)].Dimension.BetType			DBT  WITH(NOLOCK) ON S.BetTypeName=DBT.BetType AND S.LegBetTypeName=DBT.LegType AND S.BetGroupType=DBT.BetGroupType AND S.BetSubTypeName=DBT.BetSubType AND S.GroupingType=DBT.GroupingType
	LEFT OUTER JOIN [$(Dimensional)].Dimension.PriceType		DPTK WITH(NOLOCK) ON S.PriceTypeId = DPTK.PriceTypeId AND S.PriceTypeSource = DPTK.Source 
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Class			DCLK WITH(NOLOCK) ON S.ClassID=DCLK.ClassID AND DCLK.Source=S.ClassIdSource AND S.MasterTransactionTimestamp >=DCLK.FROMDATE AND S.MasterTransactionTimestamp <DCLK.TODATE
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Market			DM	 WITH(NOLOCK) ON S.MarketID=DM.MarketID AND S.MarketIDSource=DM.Source
	LEFT OUTER JOIN [$(Dimensional)].Dimension.[Event] 			DE	 WITH(NOLOCK) ON S.EventID=DE.EventID AND S.EventIDSource=DE.Source
	LEFT OUTER JOIN [$(Dimensional)].Dimension.[User]			DUK  WITH(NOLOCK) ON S.UserID=DUK.UserID AND S.UserIDSource=DUK.Source
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Instrument		DIK  WITH(NOLOCK) ON S.InstrumentID=DIK.InstrumentID AND S.InstrumentIDSource=DIK.Source
	LEFT OUTER JOIN [$(Dimensional)].Dimension.Note				DN   WITH(NOLOCK) ON S.TransactionId = DN.NoteID 
	LEFT OUTER JOIN [$(Dimensional)].Dimension.LegType			DLTK WITH(NOLOCK) ON S.NumberOfLegs=DLTK.NumberOfLegs AND S.LegNumber=DLTK.LegNumber
	LEFT OUTER JOIN Reference.TimeZone							TZ   WITH(NOLOCK) ON S.MasterTransactionTimestamp>=TZ.FromDate AND S.MasterTransactionTimestamp<TZ.ToDate AND TZ.TimeZone='Analysis'
	LEFT OUTER JOIN [$(Dimensional)].Dimension.[DayZone]		D    WITH(NOLOCK) ON S.MasterDayText=D.MasterDayText AND S.MasterTransactionTimestamp>=D.FromDate AND S.MasterTransactionTimestamp<D.ToDate	-- Using the new Day Table Structure
	LEFT OUTER JOIN [$(Dimensional)].Dimension.[TIME]			TMD  WITH(NOLOCK) ON S.MasterTimeText=TMD.TimeText
	LEFT OUTER JOIN [$(Dimensional)].Dimension.[TIME]			TAD  WITH(NOLOCK) ON CONVERT(VARCHAR(5),CONVERT(TIME,DATEADD(MI,TZ.OffsetMinutes,S.MasterTransactionTimestamp)))=TAD.TimeText
	LEFT OUTER JOIN [$(Dimensional)].Dimension.InPlay			IP	 WITH(NOLOCK) ON S.InPlay = IP.InPlay and S.ClickToCall = IP.ClickToCall and S.CashoutType = IP.CashoutType AND S.IsDoubleDown = IP.IsDoubleDown AND S.IsDoubledDown = IP.IsDoubledDown AND IP.IsChaseTheAce = S.IsChaseTheAce AND IP.FeatureFlags = S.FeatureFlags
	WHERE S.ExistsInDim = 0
	AND S.BatchKey = @BatchKey
	OPTION (RECOMPILE)

	SET @RowCnt  = @@ROWCOUNT

	EXEC [Control].Sp_Log	@BatchKey,@Me,NULL,'Success';

END TRY
BEGIN CATCH

	DECLARE @ErrorMessage VARCHAR(255) = ERROR_MESSAGE();

	EXEC [Control].Sp_Log	@BatchKey, @Me, NULL, 'Failed', @ErrorMessage;

	THROW;

END CATCH

END

