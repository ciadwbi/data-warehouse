﻿Create View [Control].ObjectsUnderTest

as

select		[Objects].name as [Table]
			, [Columns].name as [Column]
from		[$(Dimensional)].sys.all_objects as [Objects]
			inner join [$(Dimensional)].sys.schemas as [Schemas] on [Objects].[schema_id] = [Schemas].[schema_id]
			inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
where		[type] = 'U'
			and [Schemas].name = 'fact'
union
select		[Objects].name as [Table]
			, 'FACT' as [Column]
from		[$(Dimensional)].sys.all_objects as [Objects]
			inner join [$(Dimensional)].sys.schemas as [Schemas] on [Objects].[schema_id] = [Schemas].[schema_id]
where		[type] = 'U'
			and [Schemas].name = 'fact'

GO