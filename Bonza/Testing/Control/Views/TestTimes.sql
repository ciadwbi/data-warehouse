﻿CREATE VIEW [Test].[TestTimes]
	
AS 

Select		top 100 percent --to allow sort to be used
			[Type] 
			, RunType
			, Avg(Diff) as AverageDiff
			, Max(Diff) as MaxDiff
			, Min(Diff) as MinDiff
From		(
			SELECT		'Type' = CASE
							When RunType like 'Suite%' then  'Suite'
							Else 'Test'
							End
						, RunType
						, DateDiff(Second, Start, [End]) as Diff
			FROM		[control].runs
			) as Data
Group by	[Type], RunType
Order by	[Type], RunType

