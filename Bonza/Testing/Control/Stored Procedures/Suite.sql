﻿--Test Code
--exec Test.Suite

Create Procedure [Control].Suite @Balance int, @Position int, @KPI int, @Transaction int, @Bet int, @Risk int, @Contact int, @Factor int

as

/*
Uses test levels
No Changes = 0
Minor Changes = 4
Large Changes = 5
*/

Declare @TestRunNo varchar(23)
Set @TestRunNo = convert (varchar(23),getdate(),126)

--To ensure the day data is up to date
	Exec [$(Staging)].Dimension.Sp_DimDay 999
	Exec [$(Staging)].Dimension.Sp_DimDayZone 999

exec [control].ExecuteTestSuite @Balance,@Position,@KPI,@Transaction, @Bet, @Risk, @Contact, @Factor,0,@TestRunNo

Select		*
From		[test].[tblResults]
where		RunID = @TestRunNo
			and [Status] != 'Pass'
order by	TestID

Exec test.CrossRefHistory

GO
