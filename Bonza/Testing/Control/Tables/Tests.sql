﻿Create table [control].Tests
							(
							ID int identity (1,1)
							, TestName varchar(100)
							, TestType varchar(100)
							, StoredProcedure varchar(1000) 
							, [Complete] INT NULL
							)	