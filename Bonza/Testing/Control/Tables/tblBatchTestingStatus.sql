﻿CREATE TABLE [Control].[tblBatchTestingStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[BatchKey] [int] NOT NULL,
	[StartDate] [datetime2](7) NOT NULL,
	[CompleteDate] [datetime2](7) NULL
) ON [PRIMARY]