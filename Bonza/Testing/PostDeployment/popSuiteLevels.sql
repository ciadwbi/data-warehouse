﻿truncate table [control].suitelevels

insert  into [control].SuiteLevels (Suite, [Level]) values ('Batch', 1)
insert  into [control].SuiteLevels (Suite, [Level]) values ('Daily', 2)
insert  into [control].SuiteLevels (Suite, [Level]) values ('Monthly', 3)
insert  into [control].SuiteLevels (Suite, [Level]) values ('Minor', 4)
insert  into [control].SuiteLevels (Suite, [Level]) values ('Repopulated', 5)