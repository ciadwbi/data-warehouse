﻿Truncate table [control].TestedObjects

--Facts
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'FACT', 1, 'KPI')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'FACT', 1, 'Position')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'FACT', 1, 'Balance')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'FACT', 1, 'Transaction')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'FACT', 1, 'Bet')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Risk', 'FACT', 1, 'Risk')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Factor', 'FACT', 1, 'Factor')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'FACT', 1, 'Contact')

--Dimensions
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'AccountKey', 1, 'Account')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'AccountStatusKey', 1, 'AccountStatus')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'LedgerID', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'LedgerSource', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'DayKey', 1, 'Day')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'BalanceTypeKey', 1, 'BalanceType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'WalletKey', 1, 'Wallet')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'OpeningBalance', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'TotalTransactedAmount', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'ClosingBalance', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'CreatedDate', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'CreatedBatchKey', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'ModifiedDate', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'ModifiedBatchKey', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Balance', 'StructureKey', 1, 'Structure')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'AccountKey', 1, 'Account')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'AccountStatusKey', 1, 'AccountStatus')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'CommunicationId', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'CommunicationKey', 1, 'Communication')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'StoryKey', 1, 'Story')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'ChannelKey', 1, 'Channel')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'PromotionKey', 1, 'Promotion')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'BetTypeKey', 1, 'BetType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'ClassKey', 1, 'Class')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'CommunicationCompanyKey', 1, 'Company')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'EventKey', 1, 'Event')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'InteractionTypeKey', 1, 'InteractionType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'InteractionDayKey', 1, 'Day')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'InteractionTimeKey', 1, 'Time')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'Details', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'CreatedDate', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'CreatedBatchKey', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'ModifiedDate', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Contact', 'ModifiedBatchKey', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'ContractKey', 1, 'Contract')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'DayKey', 1, 'Day')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'AccountKey', 1, 'Account')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'AccountStatusKey', 1, 'AccountStatus')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'BetTypeKey', 1, 'BetType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'LegTypeKey', 1, 'LegType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'ChannelKey', 1, 'Channel')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'CampaignKey', 1, 'Campaign')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'MarketKey', 1, 'Market')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'WalletKey', 1, 'Wallet')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'ClassKey', 1, 'Class')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'UserKey', 1, 'User')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'InstrumentKey', 1, 'Instrument')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'InPlayKey', 1, 'InPlay')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'AccountOpened', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'FirstDeposit', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'FirstBet', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'DepositsRequested', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'DepositsCompleted', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'Promotions', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'Transfers', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'BetsPlaced', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'ContractsPlaced', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'BettorsPlaced', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'PlayDaysPlaced', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'PlayFiscalWeeksPlaced', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'PlayCalenderWeeksPlaced', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'PlayFiscalMonthsPlaced', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'PlayCalenderMonthsPlaced', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'Stakes', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'Winnings', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'WithdrawalsRequested', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'WithdrawalsCompleted', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'Adjustments', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'BetsResulted', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'ContractsResulted', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'BettorsResulted', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'PlayDaysResulted', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'PlayFiscalWeeksResulted', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'PlayCalenderWeeksResulted', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'PlayFiscalMonthsResulted', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'PlayCalenderMonthsResulted', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'Turnover', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'GrossWin', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'NetRevenue', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'NetRevenueAdjustment', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'BonusWinnings', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'Betback', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'GrossWinAdjustment', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'CreatedDate', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'CreatedBatchKey', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'CreatedBy', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'ModifiedDate', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'ModifiedBatchKey', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'ModifiedBy', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('KPI', 'StructureKey', 1, 'Structure')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'BalanceTypeKey', 1, 'BalanceType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'AccountKey', 1, 'Account')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'AccountStatusKey', 1, 'AccountStatus')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LedgerID', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LedgerSource', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'DayKey', 1, 'Day')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'ActivityKey', 1, 'Activity')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'FirstDepositDayKey', 1, 'Day')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'FirstBetDayKey', 1, 'Day')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'FirstBetTypeKey', 1, 'BetType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'FirstClassKey', 1, 'Class')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'FirstChannelKey', 1, 'Channel')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LastBetDayKey', 1, 'Day')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LastBetTypeKey', 1, 'BetType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LastClassKey', 1, 'Class')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LastChannelKey', 1, 'Channel')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'OpeningBalance', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'TransactedAmount', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'ClosingBalance', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'DaysSinceAccountOpened', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'DaysSinceFirstDeposit', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'DaysSinceFirstBet', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'DaysSinceLastDeposit', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'DayssinceLastBet', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LifetimeTurnover', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LifetimeGrossWin', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LifetimeBetCount', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LifetimeBonus', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LifetimeDeposits', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LifetimeWithdrawals', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'TurnoverMTDCalender', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'TurnoverMTDFiscal', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'TierKey', 1, 'ValueTiers')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'TierKeyFiscal', 1, 'ValueTiers')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'RevenueLifeCycleKey', 1, 'RevenueLifeCycle')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'LifeStageKey', 1, 'LifeStage')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'PreviousLifeStageKey', 1, 'LifeStage')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'CreatedDate', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'CreatedBatchKey', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'CreatedBy', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'ModifiedDate', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'ModifiedBatchKey', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'ModifiedBy', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Position', 'StructureKey', 1, 'Structure')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'TransactionId', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'TransactionIdSource', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'TransactionDetailKey', 1, 'TransactionDetail')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'BalanceTypeKey', 1, 'BalanceType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'ContractKey', 1, 'Contract')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'AccountKey', 1, 'Account')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'AccountStatusKey', 1, 'AccountStatus')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'WalletKey', 1, 'Wallet')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'PromotionKey', 1, 'Promotion')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'MarketKey', 1, 'Market')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'EventKey', 1, 'Event')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'DayKey', 1, 'Day')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'MasterTransactionTimestamp', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'MasterTimeKey', 1, 'Time')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'AnalysisTransactionTimestamp', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'AnalysisTimeKey', 1, 'Time')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'CampaignKey', 1, 'Campaign')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'ChannelKey', 1, 'Channel')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'LedgerID', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'TransactedAmount', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'PriceTypeKey', 1, 'PriceType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'BetTypeKey', 1, 'BetType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'LegTypeKey', 1, 'LegType')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'ClassKey', 1, 'Class')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'UserKey', 1, 'User')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'InstrumentKey', 1, 'Instrument')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'NoteKey', 1, 'Note')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'InPlayKey', 1, 'InPlay')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'FromDate', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'CreatedDate', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'CreatedBatchKey', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'CreatedBy', 0, '')
insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Transaction', 'StructureKey', 1, 'Structure')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'PlacedDayKey', 1, 'Day')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'PlacedTimeKey', 1, 'Time')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'SelectionKey', 1, 'Competitor')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'PriceKey', 1, 'Price')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'AccountKey', 1, 'Account')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'WalletKey', 1, 'Wallet')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'PromotionKey', 1, 'Promotion')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'ChannelKey', 1, 'Channel')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'ContractKey', 1, 'Contract')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'MarketKey', 1, 'Market')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'PriceTypeKey', 1, 'PriceType')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'ClassKey', 1, 'Class')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'BetTypeKey', 1, 'BetType')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'OriginalBetStatusKey', 1, 'BetStatus')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'OriginalSettledDayKey', 1, 'Day')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'BetStatusKey', 1, 'BetStatus')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'SettledDayKey', 1, 'Day')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'StakeOriginal', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'StakeAdjustment', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'Stake', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'RefundOriginal', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'RefundAdjustment', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'Refund', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'PayoutOriginal', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'PayoutAdjustment', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'Payout', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'NumberOfSettlements', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'EstimatedFees', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'Fees', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'CreatedDate', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'CreatedBatchKey', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'CreatedBy', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'ModifiedDate', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'ModifiedBatchKey', 0, '')
--insert into [control].TestedObjects (StarName, FieldName, IsTestable, TestName) values ('Bet', 'ModifiedBy', 0, '')
