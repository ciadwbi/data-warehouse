﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/

/*Version Info */
Insert into [$(Dimensional)].[Control].[Version] values ('$(VersionNumber)', 'Testing', getdate(), SUser_Name())
Truncate table [Control].[Acquisition] 
Insert into [Control].[Acquisition] values ('$(Acquisition)')

--Data
exec test.popInitialBinaryValues
:r .\popTestedObjects.sql
:r .\popTests.sql
:r .\popTestTypes.sql
:r .\popTableTypes.sql
:r .\popSuites.sql
:r .\popSuiteLevels.sql
:r .\popCrossRef.sql
:r .\popWarehouseBugs.sql

--SQL Jobs
:r .\TestSuites_Daily.sql
:r .\TestSuites_Monthly.sql
:r .\TestSuites_Performance.sql
:r .\TestSuites_Batch.sql
--:r .\TestSuites_AcquisitionMonitor.sql
:r .\TestSuites_Uptime.sql
:r .\TestSuites_SevenAmCheck.sql
