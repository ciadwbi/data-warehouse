﻿CREATE TABLE [Test].[tblCashFlowStatus](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[TestDate] [date] NOT NULL,
	[Balance] [varchar](100) NOT NULL,
	Facts varchar(100) NULL,
	[Status] [varchar](23) NOT NULL
) ON [PRIMARY]