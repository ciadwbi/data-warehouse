﻿CREATE TABLE [Test].[tblCrossRef] (
    [ID] varchar(50) NULL,
	[TestDate] date	NULL,
    [Type] varchar(23) NULL,
	Balance varchar(23) NULL,
	WalletName varchar(23) NULL,
	BrandCode varchar(23) NULL,
	AccountID varchar(23) NULL,
	LedgerID varchar(23) NULL,
	SourceOpeningBalance decimal (18,4) NULL,
	TransactedAmount decimal (18,4) NULL, 
	TransactionCount int NULL,
	NetClosingBalance decimal (18,4),
	NextSourceOpeningBalance decimal (18,4),
	DayClosingDiscrepancy decimal (18,4),
	OngoingClosingDiscrepancy decimal (18,4),
	TotalDiscrepancy decimal (18,4)
	)