﻿CREATE TABLE [Test].[tblJira] (
    	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](23) NOT NULL,
	[JiraID] [varchar](23) NOT NULL,
	[Description] [varchar](max) NULL,
	[AssignedTo] [varchar](1000) NULL,
	[RaisedBy] [varchar](1000) NULL,
	[Priority] [varchar](100) NULL,
	[Status] [varchar](100) NULL,
	[Resolution] [varchar](100) NULL,
	[Created] [datetime] NULL,
	[Updated] [datetime] NULL,
	[Version] [varchar](50) NULL
    CONSTRAINT [PK_tblJira] PRIMARY KEY CLUSTERED ([ID] ASC), 
    [FoundVersion] VARCHAR(50) NULL
);

