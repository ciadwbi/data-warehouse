﻿--Test Code
--exec Test.[114DataCheckSum_Dimension_Wallet] 'ADHOC'
Create Procedure Test.[114DataCheckSum_Dimension_Wallet] @TestRunNo varchar(23)

as

If	(Select		Count(*)
	 From		[$(Dimensional)].Dimension.Wallet
	 ) = 4
Begin
	Insert into test.tblResults Values (@TestRunNo, '114', 'DataCheckSum_Dimension_Wallet', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '114', 'DataCheckSum_Dimension_Wallet', 'Fail', 'The number of records is incorrect')
End