﻿--Test Code
--exec Test.[111BVA_Dimension_Channel] 'ADHOC', null
--exec Test.[111BVA_Dimension_Channel] 'ADHOC', '2015-04-30'

Create Procedure Test.[111BVA_Dimension_Channel] @TestRunNo varchar(23), @FromDate datetime

as

Select		*
Into		#Data
From		[$(Dimensional)].[Dimension].Channel with (nolock)

--ChannelKey n/a

--ChannelId
If not exists	(
				select		* 
				from		#Data
				where		ChannelId < 1
							and ChannelKey not in (0, -1)
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '111', 'DataValid_Dimension_Channel ChannelId', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '111', 'DataValid_Dimension_Channel ChannelId', 'Fail', 'ChannelId Incorrect')
End

--ChannelDescription
If not exists	(
				select		* 
				from		#Data
				where		ChannelDescription = ''
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '111.1', 'DataValid_Dimension_Channel ChannelDescription', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '111.1', 'DataValid_Dimension_Channel ChannelDescription', 'Fail', 'ChannelDescription Incorrect')
End

--ChannelName
If not exists	(
				select		* 
				from		#Data
				where		ChannelName = ''
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '111.2', 'DataValid_Dimension_Channel ChannelName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '111.2', 'DataValid_Dimension_Channel ChannelName', 'Fail', 'ChannelName Incorrect')
End

--ChannelTypeName
If not exists	(
				select		* 
				from		#Data
				where		ChannelTypeName = ''
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '111.3', 'DataValid_Dimension_Channel ChannelTypeName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '111.3', 'DataValid_Dimension_Channel ChannelTypeName', 'Fail', 'ChannelTypeName Incorrect')
End

--Metadata n/a
--CreatedDate
--CreatedBatchKey
--CreatedBy
--ModifiedDate
--ModifiedBatchKey
--ModifiedBy

