﻿--Test Code
--exec Test.[108QueryPerformance_Balance] 'JF_5May_33'
Create Procedure [Test].[108QueryPerformance_Balance] @TestRunNo varchar(23)

as

Declare @Time int

--Simple
exec [Test].[spExecuteMDX] @TestRunNo, 'Position', 'Simple', 10800,
	' Select * From OpenQuery([EQ3WNPRDBDW01\TABULAR_],''
		SELECT 
		NON EMPTY
		{[Measures].[AvgClosingBalance], [Measures].[AvgOpeningBalance]} on Columns,
		[Company].[CompanyName].[CompanyName] on rows
		FROM [Bonza]
		WHERE 
		([Day].[DayDate].&[2015-03-03T00:00:00])
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'Position'
							and [TestName] = 'Simple'
				)
IF @Time <= 10800
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '108', 'QueryPerformance_Balance_Simple', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '108', 'QueryPerformance_Balance_Simple', 'Fail', 'Time taken is ' + cast (@Time as varchar(108)) + ' msec and should be less than 10800')
End

--Medium
exec [Test].[spExecuteMDX] @TestRunNo, 'Position', 'Medium', 60000,
	' Select * From OpenQuery([EQ3WNPRDBDW01\TABULAR_],''
		 WITH MEMBER [Measures].[NetValue] AS [Measures].[AvgClosingBalance] - [Measures].[AvgOpeningBalance]
         SELECT 
         NON EMPTY
         {[Measures].[AvgClosingBalance], [Measures].[AvgOpeningBalance],[Measures].[TotalTransactedAmount]} on Columns,
         CROSSJOIN([Company].[BusinessUnitName].[BusinessUnitName],[Day].[FiscalWeekStartDate].MEMBERS) on rows
         FROM [Bonza]
         WHERE 
         {([Day].[FiscalMonthText].&[Mar-2015],[Company].[CompanyName].&[William Hill Australia])
		 , ([Day].[FiscalMonthText].&[Apr-2015],[Company].[CompanyName].&[William Hill Australia])}
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'Position'
							and [TestName] = 'Medium'
				)
IF @Time <= 60000
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '108.1', 'QueryPerformance_Balance_Medium', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '108.1', 'QueryPerformance_Balance_Medium', 'Fail', 'Time taken is ' + cast (@Time as varchar(108)) + ' msec and should be less than 60000')
End

--Complex
exec [Test].[spExecuteMDX] @TestRunNo, 'Position', 'Complex', 1200000,
	' Select * From OpenQuery([EQ3WNPRDBDW01\TABULAR_],''
		 WITH MEMBER [Measures].[NetValue] AS [Measures].[AvgClosingBalance] - [Measures].[AvgOpeningBalance]
         SELECT 
         NON EMPTY
         {[Measures].[AvgClosingBalance], [Measures].[AvgOpeningBalance], [Measures].[Avg ClosingBalance]} on Columns,
         CROSSJOIN([Company].[BusinessUnitName].[BusinessUnitName],[Day].[FiscalWeekStartDate].MEMBERS) on rows
         FROM [Bonza]
         WHERE 
         {([Day].[FiscalMonthText].&[Mar-2015],[Company].[CompanyName].&[William Hill Australia])
		 , ([Day].[FiscalMonthText].&[Apr-2015],[Company].[CompanyName].&[William Hill Australia])}
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'Position'
							and [TestName] = 'Complex'
				)
IF @Time <= 1200000
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '108.2', 'QueryPerformance_Balance_Complex', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '108.2', 'QueryPerformance_Balance_Complex', 'Fail', 'Time taken is ' + cast (@Time as varchar(108)) + ' msec and should be less than 1200000')
End

GO


