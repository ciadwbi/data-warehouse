﻿--Test Code
--exec Test.[142BVA_Dimension_BetType] 'ADHOC', '2015-04-03'

Create Procedure Test.[142BVA_Dimension_BetType] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

Select		* 
into		#BetType
From		[$(Dimensional)].Dimension.BetType
where		BetTypeKey not in (0,-1)
			and ((FromDate > @FromDate)
					or (@FromDate is null))

--Keys are Metadata are not tested
--[BetTypeKey] n/a
--[BetGroupType]
If not exists(
				Select		BetTypeKey
				From		#BetType
				where		BetGroupType is null or BetGroupType = ''
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '142', 'BVA_Dimension_BetType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '142', 'BVA_Dimension_BetType', 'Fail', 'There are records with an invalid BetGroupType' + @ErrorText)
End

--[BetType]
If not exists(
				Select		BetTypeKey
				From		#BetType
				where		BetType not in ('Multi', 'Exotic', 'Single')
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '142.1', 'BVA_Dimension_AccountStatus', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5553'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '142.1', 'BVA_Dimension_AccountStatus', 'Pass', 'CCIA-5553')
	End	
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '142.1', 'BVA_Dimension_AccountStatus', 'Fail', 'There are records with an invalid BetType' + @ErrorText)
	End	
End

--[BetSubType]
If not exists(
				Select		BetTypeKey
				From		#BetType
				where		BetSubType is null or BetSubType = ''
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '142.2', 'BVA_Dimension_BetType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '142.2', 'BVA_Dimension_BetType', 'Fail', 'There are records with an invalid BetSubType' + @ErrorText)
End

--[GroupingType]
If not exists(
				Select		BetTypeKey
				From		#BetType
				where		GroupingType not in ('Multiple', 'Standard', 'Straight', 'Standout', 'Boxed', 'Roving')
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '142.3', 'BVA_Dimension_AccountStatus', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5941'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '142.3', 'BVA_Dimension_AccountStatus', 'Pass', 'CCIA-5941')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '142.3', 'BVA_Dimension_AccountStatus', 'Fail', 'There are records with an invalid GroupingType' + @ErrorText)
	End
End

--[LegType]
If not exists(
				Select		BetTypeKey
				From		#BetType
				where		LegType not in ('Win', 'N/A', 'Forecast', 'Place', 'Each Way')
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '142.4', 'BVA_Dimension_AccountStatus', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '142.4', 'BVA_Dimension_AccountStatus', 'Fail', 'There are records with an invalid LegType' + @ErrorText)
End

--Metadata n/a