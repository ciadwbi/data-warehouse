﻿--Test Code
--exec [Test].[128DataDefault_Dimension_Campaign] 'ADHOC'
Create Procedure [Test].[128DataDefault_Dimension_Campaign] @TestRunNo varchar(23)

as
If exists(
		Select		CampaignKey
		From		[$(Dimensional)].[Dimension].Campaign
		where		CampaignKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '128', 'DataDefault_Dimension_Campaign Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '128', 'DataDefault_Dimension_Campaign Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		CampaignKey
		From		[$(Dimensional)].[Dimension].Campaign
		where		CampaignKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '128.1', 'DataDefault_Dimension_Campaign Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '128.1', 'DataDefault_Dimension_Campaign Table ID-1', 'Fail', 'No ID -1 record found')
End
