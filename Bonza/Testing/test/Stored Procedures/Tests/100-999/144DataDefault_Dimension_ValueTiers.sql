﻿--Test Code
--exec [Test].[144DataDefault_Dimension_ValueTiers] 'ADHOC'
Create Procedure [Test].[144DataDefault_Dimension_ValueTiers] @TestRunNo varchar(23)

as
If exists(
		Select		TierKey
		From		[$(Dimensional)].[Dimension].ValueTiers
		where		TierKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '144', 'DataDefault_Dimension_ValueTiersTable ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '144', 'DataDefault_Dimension_ValueTiersTable Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		TierKey
		From		[$(Dimensional)].[Dimension].ValueTiers
		where		TierKey =  -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '144.1', 'DataDefault_Dimension_ValueTiersTable Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '144.1', 'DataDefault_Dimension_ValueTiersTable Table ID-1', 'Fail', 'No ID -1 record found')
End
