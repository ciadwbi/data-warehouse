﻿--Test Code
--exec Test.[145DataCount_Dimension_ValueTiers] 'ADHOC'
Create Procedure Test.[145DataCount_Dimension_ValueTiers] @TestRunNo varchar(23)

as

If	(Select		Count(*)
	 From		[$(Dimensional)].Dimension.ValueTiers
	 ) = 10
Begin
	Insert into test.tblResults Values (@TestRunNo, '145', 'DataCount_Dimension_ValueTierst', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '145', 'DataCount_Dimension_ValueTiers', 'Fail', 'The number of records is incorrect')
End



