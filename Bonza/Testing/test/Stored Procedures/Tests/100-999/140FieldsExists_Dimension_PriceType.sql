﻿--Test Code
--exec Test.[140FieldExists_Dimension_PriceType] 'ADHOC'
Create Procedure Test.[140FieldExists_Dimension_PriceType] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'select top 1
[PriceTypeKey]
      ,[PriceTypeID]
      ,[Source]
      ,[PriceTypeName]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
	FROM ' + @Database + '.[Dimension].[PriceType]'

Exec Test.spSelectValid @TestRunNo, '140','Structure - Dimension PriceType Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'PriceType'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 13
Begin
	Insert into test.tblResults Values (@TestRunNo, '140.1', 'Structure - Dimension PriceType Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '140.1', 'Structure - Dimension PriceType Table', 'Fail', 'Wrong number of columns')
End

