﻿--Test Code
--exec Test.[150CrossRef_KPI] 'AdHoc'
Create Procedure [Test].[150CrossRef_KPI] @TestRunNo varchar(23)

as

Declare @FromDate date

Set @FromDate = (Select Max (BatchToDate) from [$(Staging)].[Control].[ETLController] where BatchStatus = 1)
Set @FromDate = DateAdd(Day,-1,@FromDate)

Declare @Counter int

Create Table #Errors (
	[TestDate] [date] NULL,
	[BrandCode] [varchar](23) NULL,
	[AccountID] [varchar](23) NULL,
	[LedgerID] [varchar](23) NULL,
	[SourceOpeningBalance] [decimal](18, 4) NULL,
	[TransactedAmount] [decimal](18, 4) NULL,
	[TransactionCount] [int] NULL,
	[NetClosingBalance] [decimal](18, 4) NULL,
	[NextSourceOpeningBalance] [decimal](18, 4) NULL,
	[DayClosingDiscrepancy] [decimal](18, 4) NULL,
	[OngoingClosingDiscrepancy] [decimal](18, 4) NULL,
	[TotalDiscrepancy] [decimal](18, 4) NULL
	)

Insert into #Errors
exec Test.[CrossRef_KPI] @TestRunNo, @FromDate

Declare @CashFlowStatus varchar(20)
Declare @TotalOut decimal (18,2)
Set @Counter =			(
						Select		Count(*) as NumberOf
						From		#Errors
						where		BrandCode != 'RWWA'
						)
	
Set @TotalOut =		(Select		Sum (Total) as Total
						From		(
								Select		'Total' = CASE
												When TotalDiscrepancy < 0 then TotalDiscrepancy * -1
												Else TotalDiscrepancy
												End
								From		#Errors
								where		BrandCode != 'RWWA'
								) as Data
					)

If 	@Counter >= 20 or @TotalOut >= 1000
Begin
	Set @CashFlowStatus = 'Red'
End		
Else
Begin
	If 	@Counter >= 1 or @TotalOut >= 1
	Begin
		Set @CashFlowStatus = 'Orange'
	End	
	Else
	Begin
		Set @CashFlowStatus = 'Green'
	End
End	

Delete from [Test].[tblCashFlowStatus] where TestDate = @FromDate and Facts = 'KPI/Position'					
Insert into [Test].[tblCashFlowStatus] ([TestDate] ,[Balance],[Status],Facts) Values (getdate(), 'Client', @CashFlowStatus, 'KPI/Position')

Begin Transaction
		Delete from test.tblCrossRef where TestDate = @FromDate and [Type] = 'KPI/Position'
		Insert Into	test.tblCrossRef
		Select		@TestRunNo as ID
					, @FromDate as TestDate
					, 'KPI/Position' as [Type]
					, 'Client' as Balance
					, 'Cash' as WalletName
					, BrandCode
					, AccountID
					, LedgerID
					, SourceOpeningBalance
					, TransactedAmount
					, TransactionCount
					, NetClosingBalance
					, NextSourceOpeningBalance
					, DayClosingDiscrepancy
					, OngoingClosingDiscrepancy
					, TotalDiscrepancy
		From		#Errors
	If @@ERROR != 0
		Begin
			Rollback Transaction
		End
	Else
		Begin
			Commit Transaction
		End

If (Select Count(*) From test.tblCrossRef where TestDate = @FromDate and [Type] = 'KPI/Position') = 0
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '150', 'CrossRef_KPI/Position', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '150', 'CrossRef_KPI/Position', 'Fail', 'There is a mismatch between Master_KPI/Position, please use http://businessintelligence/Reports/Pages/Report.aspx?ItemPath=%2fSystem+Monitoring%2fCross+Reference+Checks for investigation')
End

--Test on AnalysisDate
Truncate table #Errors

Insert into #Errors
exec Test.[CrossRefA_KPI] @TestRunNo, @FromDate

Begin Transaction
		Delete from test.tblCrossRef where TestDate = @FromDate and [Type] = 'A_KPI/Position'
		Insert Into	test.tblCrossRef
		Select		@TestRunNo as ID
					, @FromDate as TestDate
					, 'A_KPI/Position' as [Type]
					, 'Client' as Balance
					, 'Cash' as WalletName
					, BrandCode
					, AccountID
					, LedgerID
					, SourceOpeningBalance
					, TransactedAmount
					, TransactionCount
					, NetClosingBalance
					, NextSourceOpeningBalance
					, DayClosingDiscrepancy
					, OngoingClosingDiscrepancy
					, TotalDiscrepancy
		From		#Errors
	If @@ERROR != 0
		Begin
			Rollback Transaction
		End
	Else
		Begin
			Commit Transaction
		End

If (Select Count(*) From test.tblCrossRef where TestDate = @FromDate and [Type] = 'A_KPI/Position') = 0
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '150.1', 'CrossRef_KPI/Position AnalysisDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '150.1', 'CrossRef_KPI/Position AnalysisDate', 'Fail', 'There is a mismatch between Analysis_KPI/Position, please use http://businessintelligence/Reports/Pages/Report.aspx?ItemPath=%2fSystem+Monitoring%2fCross+Reference+Checks for investigation')
End