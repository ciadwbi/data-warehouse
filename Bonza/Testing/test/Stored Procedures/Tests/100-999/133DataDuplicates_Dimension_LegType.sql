﻿--test
--exec [Test].[133DataDuplicates_Dimension_LegType] 'ADHOC', null

CREATE PROCEDURE [Test].[133DataDuplicates_Dimension_LegType] @TestRunNo varchar(23), @FromDate datetime

as


If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.LegType as LegType
		where		@FromDate is null
					or CreatedDate >= @FromDate
		Group by	LegTypeName
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '133', 'DataDuplicates_Dimension_LegType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '133', 'DataDuplicates_Dimension_LegType', 'Fail', 'Duplicates of LegType found')
End