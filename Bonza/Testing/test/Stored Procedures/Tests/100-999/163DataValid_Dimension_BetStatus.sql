﻿--Test Code
--exec Test.[163DataValid_Dimension_BetStatus] 'ADHOC'
Create Procedure Test.[163DataValid_Dimension_BetStatus] @TestRunNo varchar(23)

as

Select		*
Into		#Data
From		[$(Dimensional)].[Dimension].[BetStatus]

--StatusKey n/a
--StatusCode
If not exists	  (
					Select		*
					From		#Data
					where		StatusCode = ''
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '163', 'DataValid_Dimension_BetStatus StatusCode', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '163', 'DataValid_Dimension_BetStatus StatusCode', 'Fail', 'StatusCode Incorrect')
End

--StatusName
If not exists	  (
					Select		*
					From		#Data
					where		StatusName = ''
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '163.1', 'DataValid_Dimension_BetStatus StatusName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '163.1', 'DataValid_Dimension_BetStatus StatusName', 'Fail', 'StatusName Incorrect')
End

--OutcomeCode
If not exists	  (
					Select		*
					From		#Data
					where		OutcomeCode = ''
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '163.2', 'DataValid_Dimension_BetStatus OutcomeCode', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '163.2', 'DataValid_Dimension_BetStatus OutcomeCode', 'Fail', 'OutcomeCode Incorrect')
End

--OutcomeName
If not exists	  (
					Select		*
					From		#Data
					where		OutcomeName = ''
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '163.3', 'DataValid_Dimension_BetStatus OutcomeName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '163.4', 'DataValid_Dimension_BetStatus OutcomeName', 'Fail', 'OutcomeName Incorrect')
End

--Metadata n/a
--FromDate
--ToDate
--FirstDate
--CreatedDate
--CreatedBatchKey
--CreatedBy
--ModifiedDate
--ModifiedBatchKey
--ModifiedBy

