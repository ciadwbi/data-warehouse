﻿--Test Code
--exec Test.[105CrossRef_Transaction] 'AdHoc'
Create Procedure [Test].[105CrossRef_Transaction] @TestRunNo varchar(23)

as

Declare @FromDate date

Set @FromDate = (Select Max (BatchToDate) from [$(Staging)].[Control].[ETLController] where BatchStatus = 1)
Set @FromDate = DateAdd(Day,-1,@FromDate)

Declare @Counter int

Create Table #Errors (
	[Balance] [varchar](23) NULL,
	[WalletName] [varchar](23) NULL,
	[TestDate] [date] NULL,
	[BrandCode] [varchar](23) NULL,
	[AccountID] [varchar](23) NULL,
	[LedgerID] [varchar](23) NULL,
	[SourceOpeningBalance] [decimal](18, 4) NULL,
	[TransactedAmount] [decimal](18, 4) NULL,
	[TransactionCount] [int] NULL,
	[NetClosingBalance] [decimal](18, 4) NULL,
	[NextSourceOpeningBalance] [decimal](18, 4) NULL,
	[DayClosingDiscrepancy] [decimal](18, 4) NULL,
	[OngoingClosingDiscrepancy] [decimal](18, 4) NULL,
	[TotalDiscrepancy] [decimal](18, 4) NULL
	)

Insert into #Errors
exec Test.[CrossRef_Transaction] @TestRunNo, @FromDate

Declare @CashFlowStatus varchar(20)
Declare @TotalOut decimal (18,2)
Set @Counter =			(
						Select		Count(*) as NumberOf
						From		#Errors
						where		BrandCode != 'RWWA'
						)
	
Set @TotalOut =		(Select		Sum (Total) as Total
						From		(
								Select		'Total' = CASE
												When TotalDiscrepancy < 0 then TotalDiscrepancy * -1
												Else TotalDiscrepancy
												End
								From		#Errors
								where		BrandCode != 'RWWA'
								) as Data
					)

If 	@Counter >= 20 or @TotalOut >= 10000
Begin
	Set @CashFlowStatus = 'Red'
End		
Else
Begin
	If 	@Counter >= 1 or @TotalOut >= 1
	Begin
		Set @CashFlowStatus = 'Orange'
	End	
	Else
	Begin
		Set @CashFlowStatus = 'Green'
	End
End	

Delete from [Test].[tblCashFlowStatus] where TestDate = @FromDate and Facts = 'Transaction/Position'				
Insert into [Test].[tblCashFlowStatus] ([TestDate] ,[Balance],[Status], Facts) Values (getdate(), 'Client', @CashFlowStatus, 'Transaction/Position')

Begin Transaction
		Delete from test.tblCrossRef where TestDate = @FromDate and [Type] = 'Transaction/Position'
		Insert Into	test.tblCrossRef
		Select		@TestRunNo as ID
					, @FromDate as TestDate
					, 'Transaction/Position' as [Type]
					, Balance
					, WalletName
					, BrandCode
					, AccountID
					, LedgerID
					, SourceOpeningBalance
					, TransactedAmount
					, TransactionCount
					, NetClosingBalance
					, NextSourceOpeningBalance
					, DayClosingDiscrepancy
					, OngoingClosingDiscrepancy
					, TotalDiscrepancy
		From		#Errors
	If @@ERROR != 0
		Begin
			Rollback Transaction
		End
	Else
		Begin
			Commit Transaction
		End

If (Select Count(*) From test.tblCrossRef where TestDate = @FromDate and [Type] = 'Transaction/Position') = 0
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '105', 'CrossRef_Transaction/Position', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '105', 'CrossRef_Transaction/Position', 'Fail', 'There is a mismatch between Master_Transaction/Balance, please use http://businessintelligence/Reports/Pages/Report.aspx?ItemPath=%2fSystem+Monitoring%2fCross+Reference+Checks for investigation')
End

--Test on AnalysisDate
Truncate table #Errors

Insert into #Errors
exec Test.[CrossRefA_Transaction] @TestRunNo, @FromDate

Delete from [Test].[tblCashFlowStatus] where TestDate = @FromDate and Facts = 'Transaction/Position'				
Insert into [Test].[tblCashFlowStatus] ([TestDate] ,[Balance],[Status], Facts) Values (getdate(), 'Client', @CashFlowStatus, 'Transaction/Position')

Begin Transaction
		Delete from test.tblCrossRef where TestDate = @FromDate and [Type] = 'A_Transaction/Position'
		Insert Into	test.tblCrossRef
		Select		@TestRunNo as ID
					, @FromDate as TestDate
					, 'A_Transaction/Position' as [Type]
					, Balance
					, WalletName
					, BrandCode
					, AccountID
					, LedgerID
					, SourceOpeningBalance
					, TransactedAmount
					, TransactionCount
					, NetClosingBalance
					, NextSourceOpeningBalance
					, DayClosingDiscrepancy
					, OngoingClosingDiscrepancy
					, TotalDiscrepancy
		From		#Errors
	If @@ERROR != 0
		Begin
			Rollback Transaction
		End
	Else
		Begin
			Commit Transaction
		End

If (Select Count(*) From test.tblCrossRef where TestDate = @FromDate and [Type] = 'A_Transaction/Position') = 0
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '105.1', 'CrossRef_Transaction/Position AnalysisDate', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '105.1', 'CrossRef_Transaction/Position AnalysisDate', 'Fail', 'There is a mismatch between Analysis_Transaction/Balance, please use http://businessintelligence/Reports/Pages/Report.aspx?ItemPath=%2fSystem+Monitoring%2fCross+Reference+Checks for investigation')
End