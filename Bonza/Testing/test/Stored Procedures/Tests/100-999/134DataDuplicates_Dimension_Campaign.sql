﻿--test
--exec [Test].[134DataDuplicates_Dimension_Campaign] 'ADHOC', null

CREATE PROCEDURE [Test].[134DataDuplicates_Dimension_Campaign] @TestRunNo varchar(23), @FromDate datetime

as


If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.Campaign as Campaign
		where		@FromDate is null
					or CreatedDate >= @FromDate
		Group by	CampaignId
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '134', 'DataDuplicates_Dimension_Campaign', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '134', 'DataDuplicates_Dimension_Campaign', 'Fail', 'Duplicates of Campaign found')
End
