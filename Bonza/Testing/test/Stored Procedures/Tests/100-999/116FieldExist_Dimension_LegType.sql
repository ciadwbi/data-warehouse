﻿--Test Code
--exec Test.[116FieldExist_Dimension_LegType] 'ADHOC'
Create Procedure Test.[116FieldExist_Dimension_LegType] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
[LegTypeKey]
      ,[LegTypeName]
      ,[LegNumber]
      ,[NumberOfLegs]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
			from ' + @Database + '.[Dimension].[LegType]'

Exec Test.spSelectValid @TestRunNo, '116','Structure - Dimension LegType Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'LegType'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 13
Begin
	Insert into test.tblResults Values (@TestRunNo, '116.1', 'Structure - Dimension LegType Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '116.1', 'Structure - Dimension LegType Table', 'Fail', 'Wrong number of columns')
End