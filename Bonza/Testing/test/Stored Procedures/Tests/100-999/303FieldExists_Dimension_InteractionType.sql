--Test Code
--exec Test.[303FieldExists_Dimension_InteractionType] 'ADHOC'
Create Procedure Test.[303FieldExists_Dimension_InteractionType] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
			 [InteractiontypeKey]
			,[InteractionTypeId]
			,[InteractionTypeSource]
			,[InteractionTypeName]
			--, FirstDate CCIA-6648
			--, FromDate CCIA-6648
			--, ToDate CCIA-6648
			,[CreatedDate]
			,[CreatedBatchKey]
			,[CreatedBy]
			,[ModifiedDate]
			,[ModifiedBatchKey]
			,[ModifiedBy]
			from ' + @Database + '.[Dimension].[InteractionType]'

Exec Test.spSelectValid @TestRunNo, '303','Structure - Dimension InteractionType Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'InteractionType'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 10
Begin
	Insert into test.tblResults Values (@TestRunNo, '303.1', 'Structure - Dimension InteractionType Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '303.1', 'Structure - Dimension InteractionType Table', 'Fail', 'Wrong number of columns')
End


