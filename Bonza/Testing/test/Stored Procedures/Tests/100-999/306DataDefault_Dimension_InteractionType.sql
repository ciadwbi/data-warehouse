﻿--Test Code
--exec [Test].[306DataDefault_Dimension_InteractionType] 'ADHOC'
Create Procedure [Test].[306DataDefault_Dimension_InteractionType] @TestRunNo varchar(23)

as
If exists(
		Select		InteractionTypeKey
		From		[$(Dimensional)].[Dimension].InteractionType
		where		InteractionTypeKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '306', 'DataDefault_Dimension_InteractionType Table ID0', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6651'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '306', 'DataDefault_Dimension_InteractionType Table ID0', 'Pass', 'CCIA-6650')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '306', 'DataDefault_Dimension_InteractionType Table ID0', 'Fail', 'No ID 0 record found')
	End
End

If exists(
		Select		InteractionTypeKey
		From		[$(Dimensional)].[Dimension].InteractionType
		where		InteractionTypeKey =  -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '306.1', 'DataDefault_Dimension_InteractionType Table ID-1', 'Pass', Null)
End
Else
Begin
If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6651'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '306.1', 'DataDefault_Dimension_InteractionType Table ID-1', 'Pass', 'CCIA-6651')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '306.1', 'DataDefault_Dimension_InteractionType Table ID-1', 'Fail', 'No ID -1 record found')
	End
End
