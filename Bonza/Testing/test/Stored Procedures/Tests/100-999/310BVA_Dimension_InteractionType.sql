﻿--Test Code
--exec Test.[310BVA_Dimension_InteractionType] 'ADHOC', '2015-04-03'

Create Procedure Test.[310BVA_Dimension_InteractionType] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

Select		* 
into		#Data
From		[$(Dimensional)].Dimension.InteractionType
where		[InteractionTypeKey] not in (0, -1)
			--and ((FromDate > @FromDate)
			--		or (@FromDate is null))
			--and ToDate = '9999-12-31'

--InteractiontypeKey n/a
--InteractionTypeId
If not exists(
				Select		*
				From		#Data
				where		InteractionTypeId = ''
							or InteractionTypeId is null
						
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '310.0', 'BVA_Dimension_Communication InteractionTypeId', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '310.0', 'BVA_Dimension_Communication InteractionTypeId', 'Fail', 'There are records with an invalid InteractionTypeId' + @ErrorText)
End

--InteractionTypeSource
If not exists(
				Select		*
				From		#Data
				where		InteractionTypeSource != 'ET'					
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '310.1', 'BVA_Dimension_Communication InteractionTypeSource', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '310.1', 'BVA_Dimension_Communication InteractionTypeSource', 'Fail', 'There are records with an invalid InteractionTypeSource' + @ErrorText)
End

--InteractionTypeName
If not exists(
				Select		*
				From		#Data
				where		InteractionTypeName = ''
							or InteractionTypeName is null
						
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '310.2', 'BVA_Dimension_Communication InteractionTypeName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '310.2', 'BVA_Dimension_Communication InteractionTypeName', 'Fail', 'There are records with an invalid InteractionTypeName' + @ErrorText)
End

--CreatedDate n/a
--CreatedBatchKey n/a
--CreatedBy n/a
--ModifiedDate n/a
--ModifiedBatchKey n/a
--ModifiedBy n/a