﻿--Test Code
--exec Test.[139DataValid_Fact_KPI] 'ADHOC'
CREATE PROCEDURE [Test].[139DataValid_Fact_KPI] @TestRunNo varchar(23)

AS

--ContractKey
--DayKey
--AccountKey
--AccountStatusKey
--BetTypeKey
--LegTypeKey
--ChannelKey
--CampaignKey
--MarketKey
--EventKey
--ClassKey
--UserKey
--InstrumentKey
--InPlayKey
--[AccountOpened]
If not exists  (
				Select		KPI.DayKey
							, Account.AccountKey
							, Account.AccountOpenedDayKey
							, KPI.AccountOpened
							, Account.LedgerID
				FROM		[$(Dimensional)].[Fact].[KPI] as KPI
							inner join [$(Dimensional)].Dimension.Account as Account on KPI.AccountKey = Account.AccountKey
				Where		AccountOpened is not null
							and (
								KPI.DayKey != Account.AccountOpenedDayKey
								or 
								KPI.AccountOpened != Account.LedgerID
								)
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.0a', 'DataValid_Fact_KPI AccountOpened', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.0a', 'DataValid_Fact_KPI AccountOpened', 'Fail', 'Errors in AccountOpened')
End

If not exists  (
				Select		Count(*)
				From		[$(Dimensional)].[Fact].[KPI]
				Where		AccountOpened is not null
				Group by	AccountKey	
				Having		Count(*) >= 2
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.0b', 'DataValid_Fact_KPI AccountOpened', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.0b', 'DataValid_Fact_KPI AccountOpened', 'Fail', 'Errors in AccountOpened')
End

If not exists  (
				Select		*
				From		(
							Select		Account.AccountKey
										, Account.AccountOpenedDayKey
										, Max(KPI.AccountOpened) as AccountOpened
							FROM		[$(Dimensional)].Dimension.Account as Account
										left join [$(Dimensional)].[Fact].[KPI] as KPI on KPI.AccountKey = Account.AccountKey
							Group by	Account.AccountKey, Account.AccountOpenedDayKey
							) as Data
				Where		AccountOpened is null
							and AccountOpenedDayKey >= (Select min(DayKey) from [$(Dimensional)].[Fact].[KPI])
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.0c', 'DataValid_Fact_KPI AccountOpened', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.0c', 'DataValid_Fact_KPI AccountOpened', 'Fail', 'Errors in AccountOpened')
End

--[FirstDeposit]
If not exists  (
				Select		KPI.DayKey
							, KPI.FirstDeposit
							, KPI.DepositsCompleted
				FROM		[$(Dimensional)].[Fact].[KPI] as KPI
				where		FirstDeposit is not null
							and KPI.DepositsCompleted is null
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.1a', 'DataValid_Fact_KPI FirstDeposit', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.1a', 'DataValid_Fact_KPI FirstDeposit', 'Fail', 'Errors in FirstDeposit')
End

If not exists  (
				Select		Count(*)
				From		[$(Dimensional)].[Fact].[KPI]
				Where		FirstDeposit is not null
				Group by	AccountKey	
				Having		Count(*) >= 2
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.1b', 'DataValid_Fact_KPI FirstDeposit', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.1b', 'DataValid_Fact_KPI FirstDeposit', 'Fail', 'Errors in FirstDeposit')
End

--[FirstBet]
If not exists  (
				Select		KPI.DayKey
							, KPI.FirstBet
							, KPI.BetsPlaced
				FROM		[$(Dimensional)].[Fact].[KPI] as KPI
				where		FirstBet is not null
							and KPI.BetsPlaced is null
							and KPI.BonusBetsPlaced is null
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.2a', 'DataValid_Fact_KPI FirstBet', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7112'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.2a', 'DataValid_Fact_KPI FirstBet', 'Pass', 'CCIA-7112')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.2a', 'DataValid_Fact_KPI FirstBet', 'Fail', 'Errors in FirstBet')
	End
End

If not exists  (
				Select		Count(Distinct FirstBet)
				From		[$(Dimensional)].[Fact].[KPI]
				Where		FirstBet is not null
				Group by	AccountKey	
				Having		Count(Distinct FirstBet) >= 2
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.2b', 'DataValid_Fact_KPI FirstBet', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7113'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.2b', 'DataValid_Fact_KPI FirstBet', 'Pass', 'CCIA-7113')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.2b', 'DataValid_Fact_KPI FirstBet', 'Fail', 'Errors in FirstBet')
	End
End

--[DepositsRequested]
If not exists  (
				Select		*
				FROM		(Select		[Transaction].AccountKey
										, [Transaction].DayKey
										, Sum (TransactedAmount) as TransactedAmount
							 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
										inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
							 Where		TransactionDetail.TransactionTypeID = 'DP'
										and TransactionDetail.TransactionStatusID = 'RE'
										and TransactionDetail.TransactionSign = 1
							 Group by	[Transaction].AccountKey, [Transaction].DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (DepositsRequested) as DepositsRequested
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		DepositsRequested != 0
										 Group by	AccountKey, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != DepositsRequested
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.3', 'DataValid_Fact_KPI DepositsRequested', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.3', 'DataValid_Fact_KPI DepositsRequested', 'Fail', 'Errors in DepositsRequested')
End

--[DepositsCompleted]
If not exists  (
				Select		*
				FROM		(Select		[Transaction].AccountKey
										, [Transaction].DayKey
										, Sum (TransactedAmount) as TransactedAmount
							 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
										inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
							 Where		TransactionDetail.TransactionTypeID = 'DP'
										and TransactionDetail.TransactionStatusID = 'CO'
										and TransactionDetail.TransactionSign = 1
							 Group by	[Transaction].AccountKey, [Transaction].DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (DepositsCompleted) as DepositsCompleted
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		DepositsCompleted != 0
										 Group by	AccountKey, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != DepositsCompleted
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.4', 'DataValid_Fact_KPI DepositsCompleted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.4', 'DataValid_Fact_KPI DepositsCompleted', 'Fail', 'Errors in DepositsCompleted')
End

--DepositRequestedCount
If not exists  (
				Select		*
				FROM		(Select		[Transaction].AccountKey
										, [Transaction].DayKey
										, Count (TransactedAmount) as Transacted
							 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
										inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
							 Where		TransactionDetail.TransactionTypeID = 'DP'
										and TransactionDetail.TransactionStatusID = 'RE'
										and TransactionDetail.TransactionSign = 1
							 Group by	[Transaction].AccountKey, [Transaction].DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Count (Distinct DepositsRequestedCount) as DepositsRequested
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		DepositsRequested != 0
										 Group by	AccountKey, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		Transacted != DepositsRequested
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.4a', 'DataValid_Fact_KPI DepositsRequestedCount', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.4a', 'DataValid_Fact_KPI DepositsRequestedCount', 'Fail', 'Errors in DepositsRequestedCount')
End

--DepositCompletedCount
If not exists  (
				Select		*
				FROM		(Select		[Transaction].AccountKey
										, [Transaction].DayKey
										, Count (TransactedAmount) as Transacted
							 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
										inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
							 Where		TransactionDetail.TransactionTypeID = 'DP'
										and TransactionDetail.TransactionStatusID = 'CO'
										and TransactionDetail.TransactionSign = 1
							 Group by	[Transaction].AccountKey, [Transaction].DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Count (Distinct DepositsCompletedCount) as DepositsCompleted
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		DepositsCompleted != 0
										 Group by	AccountKey, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		Transacted != DepositsCompleted
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.4b', 'DataValid_Fact_KPI DepositsCompletedCount', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.4b', 'DataValid_Fact_KPI DepositsCompletedCount', 'Fail', 'Errors in DepositsCompletedCount')
End

--[Promotions]
If not exists  (
				Select		*
				FROM		(Select		[Transaction].AccountKey
										, [Transaction].DayKey
										, Sum (TransactedAmount) as TransactedAmount
							 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
										inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
							 Where		TransactionDetail.TransactionTypeID = 'BO'
										and TransactionDetail.TransactionStatusID = 'CO'
										and TransactionMethodID not in ('BP', 'RB')
										and TransactionDetail.TransactionSign = 1
										and [Transaction].WalletKey = 1
							 Group by	[Transaction].AccountKey, [Transaction].DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (Promotions) as Promotions
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		Promotions != 0
										 Group by	AccountKey, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != Promotions
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.5', 'DataValid_Fact_KPI Promotions', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.5', 'DataValid_Fact_KPI Promotions', 'Fail', 'Errors in Promotions')
End

--[Transfers]
If not exists  (
				Select		*
				FROM		(Select		[Transaction].AccountKey
										, [Transaction].DayKey
										, Sum (TransactedAmount) as TransactedAmount
							 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
										inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
							 Where		TransactionDetail.TransactionTypeID = 'TR'
										and TransactionDetail.TransactionStatusID = 'CO'
										and TransactionDetail.TransactionSign = 1
							 Group by	[Transaction].AccountKey, [Transaction].DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (Transfers) as Transfers
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		Transfers != 0
										 Group by	AccountKey, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != Transfers
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.6', 'DataValid_Fact_KPI Transfers', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.6', 'DataValid_Fact_KPI Transfers', 'Fail', 'Errors in Transfers')
End

--[BetsPlaced]
If not exists  (
				Select		*
				FROM		(Select		[Transaction].AccountKey
										, [Transaction].DayKey
										, [Transaction].TransactionId
							 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
										inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
										inner join [$(Dimensional)].Dimension.Wallet as Wallet on [Transaction].WalletKey = Wallet.WalletKey
							 Where		TransactionDetail.TransactionTypeID = 'BT'
										and TransactionDetail.TransactionStatusID = 'RE'
										and Wallet.WalletId != 'B'
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, BetsPlaced
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		BetsPlaced != 0
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
														and TData.TransactionId = KPI.BetsPlaced
				Where		KPI.BetsPlaced is null
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.7', 'DataValid_Fact_KPI BetsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.7', 'DataValid_Fact_KPI BetsPlaced', 'Fail', 'Errors in BetsPlaced')
End

--[BonusBetsPlaced]
If not exists  (
				Select		*
				FROM		(Select		[Transaction].AccountKey
										, [Transaction].DayKey
										, [Transaction].TransactionId
							 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
										inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
										inner join [$(Dimensional)].Dimension.Wallet as Wallet on [Transaction].WalletKey = Wallet.WalletKey
							 Where		TransactionDetail.TransactionTypeID = 'BT'
										and TransactionDetail.TransactionStatusID = 'RE'
										and Wallet.WalletId = 'B'
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, BonusBetsPlaced
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		BonusBetsPlaced != 0
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
														and TData.TransactionId = KPI.BonusBetsPlaced
				Where		KPI.BonusBetsPlaced is null
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.7a', 'DataValid_Fact_KPI BonusBetsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.7a', 'DataValid_Fact_KPI BonusBetsPlaced', 'Fail', 'Errors in BonusBetsPlaced')
End
	
--[ContractsPlaced]
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6407'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.8', 'DataValid_Fact_KPI ContractsPlaced', 'Pass', 'CCIA-6407')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.8', 'DataValid_Fact_KPI ContractsPlaced', 'Fail', 'Test Required for ContractsPlaced column')
	End

--[BonusContractsPlaced]
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6407'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.8', 'DataValid_Fact_KPI BonusContractsPlaced', 'Pass', 'CCIA-6407')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.8', 'DataValid_Fact_KPI BonusContractsPlaced', 'Fail', 'Test Required for BonusContractsPlaced column')
	End

--[BettorsPlaced]
If not exists	(
				select		* 
				from		[$(Dimensional)].[Fact].[kpi]
				where		BetsPlaced is not null and BettorsPlaced is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.9a', 'DataValid_Fact_KPI BettorsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.9a', 'DataValid_Fact_KPI BettorsPlaced', 'Fail', 'Errors in the BettorsPlaced column')
End

If not exists	(
				select		* 
				from		[$(Dimensional)].[Fact].[kpi]
							inner join [$(Dimensional)].Dimension.Account as Account on KPI.AccountKey = Account.AccountKey
				where		BettorsPlaced != Account.LedgerID 
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.9b', 'DataValid_Fact_KPI BettorsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.9b', 'DataValid_Fact_KPI BettorsPlaced', 'Fail', 'Errors in the BettorsPlaced column')
End

--[BonusBettorsPlaced]
If not exists	(
				select		* 
				from		[$(Dimensional)].[Fact].[kpi]
				where		BonusBetsPlaced is not null and BonusBettorsPlaced is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.9c', 'DataValid_Fact_KPI BonusBettorsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.9c', 'DataValid_Fact_KPI BonusBettorsPlaced', 'Fail', 'Errors in the BonusBettorsPlaced column')
End

If not exists	(
				select		* 
				from		[$(Dimensional)].[Fact].[kpi]
							inner join [$(Dimensional)].Dimension.Account as Account on KPI.AccountKey = Account.AccountKey
				where		BonusBettorsPlaced != Account.LedgerID 
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.9d', 'DataValid_Fact_KPI BonusBettorsPlaced', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.9d', 'DataValid_Fact_KPI BonusBettorsPlaced', 'Fail', 'Errors in the BonusBettorsPlaced column')
End

--[PlayDaysPlaced]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI]
				where	PlayDaysPlaced is not null
						and Cast (PlayDaysPlaced as varchar(100)) != Cast (BettorsPlaced as varchar(100)) + Cast (Left (DayKey, 8) as char(8))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.10', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.10', 'DataValid_Fact_KPI', 'Fail', 'Errors in the PlayDaysPlaced column')
End

--[BonusPlayDaysPlaced]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI]
				where	BonusPlayDaysPlaced is not null
						and Cast (BonusPlayDaysPlaced as varchar(100)) != Cast (BonusBettorsPlaced as varchar(100)) + Cast (Left (DayKey, 8) as char(8))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.10a', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.10a', 'DataValid_Fact_KPI', 'Fail', 'Errors in the BonusPlayDaysPlaced column')
End

--[PlayFiscalWeeksPlaced]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	PlayDaysPlaced is not null
						and Cast (PlayFiscalWeeksPlaced as varchar(100)) != Cast (BettorsPlaced as varchar(100)) + Cast (FiscalWeekKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.11', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6066'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.11', 'DataValid_Fact_KPI', 'Pass', 'CCIA-6066')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.11', 'DataValid_Fact_KPI', 'Fail', 'Errors in the PlayFiscalWeeksPlaced column')
	End
End

--[BonusPlayFiscalWeeksPlaced]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	PlayDaysPlaced is not null
						and Cast (BonusPlayFiscalWeeksPlaced as varchar(100)) != Cast (BonusBettorsPlaced as varchar(100)) + Cast (FiscalWeekKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.11a', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6066'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.11a', 'DataValid_Fact_KPI', 'Pass', 'CCIA-6066')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.11a', 'DataValid_Fact_KPI', 'Fail', 'Errors in the BonusPlayFiscalWeeksPlaced column')
	End
End

--[PlayCalenderWeeksPlaced]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	PlayDaysPlaced is not null
						and Cast ([PlayCalenderWeeksPlaced] as varchar(100)) != Cast (BettorsPlaced as varchar(100)) + Cast (WeekKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.13', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7109'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.13', 'DataValid_Fact_KPI', 'Pass', 'CCIA-7109')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.13', 'DataValid_Fact_KPI', 'Fail', 'Errors in the PlayCalenderWeeksPlaced column')
	End
End

--[BonusPlayCalenderWeeksPlaced]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	BonusPlayDaysPlaced is not null
						and Cast ([BonusPlayCalenderWeeksPlaced] as varchar(100)) != Cast (BonusBettorsPlaced as varchar(100)) + Cast (WeekKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.13a', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5543'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.13a', 'DataValid_Fact_KPI', 'Pass', 'CCIA-5543')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.13a', 'DataValid_Fact_KPI', 'Fail', 'Errors in the BonusPlayCalenderWeeksPlaced column')
	End
End

--[PlayFiscalMonthsPlaced]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	PlayDaysPlaced is not null
						and Cast ([PlayFiscalMonthsPlaced] as varchar(100)) != Cast (BettorsPlaced as varchar(100)) + Cast (FiscalMonthKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.14', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.14', 'DataValid_Fact_KPI', 'Fail', 'Errors in the PlayFiscalMonthsPlaced column')
End

--[BonusPlayFiscalMonthsPlaced]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	BonusPlayDaysPlaced is not null
						and Cast ([BonusPlayFiscalMonthsPlaced] as varchar(100)) != Cast (BonusBettorsPlaced as varchar(100)) + Cast (FiscalMonthKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.14a', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.14a', 'DataValid_Fact_KPI', 'Fail', 'Errors in the BonusPlayFiscalMonthsPlaced column')
End

--[PlayCalenderMonthsPlaced]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	PlayDaysPlaced is not null
						and Cast ([PlayCalenderMonthsPlaced] as varchar(100)) != Cast (BettorsPlaced as varchar(100)) + Cast (MonthKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.15', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6114'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.15', 'DataValid_Fact_KPI', 'Pass', 'CCIA-6114')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.15', 'DataValid_Fact_KPI', 'Fail', 'Errors in the PlayCalenderMonthsPlaced column')
	End
End

--[BonusPlayCalenderMonthsPlaced]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	PlayDaysPlaced is not null
						and Cast ([BonusPlayCalenderMonthsPlaced] as varchar(100)) != Cast (BonusBettorsPlaced as varchar(100)) + Cast (MonthKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.15a', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6114'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.15a', 'DataValid_Fact_KPI', 'Pass', 'CCIA-6114')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.15a', 'DataValid_Fact_KPI', 'Fail', 'Errors in the BonusPlayCalenderMonthsPlaced column')
	End
End

--[Stakes]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Sum(Data.TransactedAmount) as TransactedAmount
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactedAmount
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
													inner join [$(Dimensional)].Dimension.Wallet as Wallet on [Transaction].WalletKey = Wallet.WalletKey
										 Where		TransactionDetail.TransactionTypeID = 'BT'
													and TransactionStatusID in ('RE', 'DE', 'CN')
													and [Transaction].BalanceTypeKey = 2
													and Wallet.WalletId != 'B'
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (Stakes) as Stakes
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		Stakes != 0
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != Stakes			
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.16', 'DataValid_Fact_KPI Stakes', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.16', 'DataValid_Fact_KPI Stakes', 'Fail', 'Errors in the Stakes column')
End

--[BonusStakes]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Sum(Data.TransactedAmount) as TransactedAmount
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactedAmount
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
													inner join [$(Dimensional)].Dimension.Wallet as Wallet on [Transaction].WalletKey = Wallet.WalletKey
										 Where		TransactionDetail.TransactionTypeID = 'BT'
													and TransactionStatusID in ('RE', 'DE', 'CN')
													and [Transaction].BalanceTypeKey = 2
													and WalletId = 'B'
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (BonusStakes) as Stakes
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		BonusStakes != 0
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != Stakes			
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.16a', 'DataValid_Fact_KPI BonusStakes', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7114'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.16a', 'DataValid_Fact_KPI BonusStakes', 'Pass', 'CCIA-7114')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.16a', 'DataValid_Fact_KPI Stakes', 'Fail', 'Errors in the BonusStakes column')
	End
End

--[Winnings]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Sum(Data.TransactedAmount) as TransactedAmount
								From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactedAmount
											From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
											Where		TransactionDetail.TransactionTypeID = 'BT'
													and TransactionStatusID in ('WN', 'UN','CO')
													and [Transaction].PromotionKey in (0, -1)
													and [Transaction].BalanceTypeKey = 2
										) as Data
								Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (Winnings) as Winnings
											From		[$(Dimensional)].[Fact].[KPI]
											Where		Winnings != 0
											Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != Winnings
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.17', 'DataValid_Fact_KPI Winnings', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6404'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.17', 'DataValid_Fact_KPI Winnings', 'Pass', 'CCIA-6404')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.17', 'DataValid_Fact_KPI Winnings', 'Fail', 'Errors in the Winnings column')
	End
End

--[BettorsCashedOut]
If not exists	(
				select		* 
				from		[$(Dimensional)].[Fact].[kpi]
				where		BetsCashedOut is not null and BettorsCashedOut is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.37a', 'DataValid_Fact_KPI BettorsCashedOut', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.37a', 'DataValid_Fact_KPI BettorsCashedOut', 'Fail', 'Errors in the BettorsCashedOut column')
End

If not exists	(
				select		* 
				from		[$(Dimensional)].[Fact].[kpi]
							inner join [$(Dimensional)].Dimension.Account as Account on KPI.AccountKey = Account.AccountKey
				where		BettorsCashedOut != Account.LedgerID 
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.37b', 'DataValid_Fact_KPI BettorsCashedOut', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.37b', 'DataValid_Fact_KPI BettorsCashedOut', 'Fail', 'Errors in the BettorsCashedOut column')
End

--[BetsCashedOut]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Count(Distinct TransactionId) as Settled
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactionId
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
													inner join [$(Dimensional)].Dimension.Wallet as Wallet on [Transaction].WalletKey = Wallet.WalletKey
										 Where		TransactionDetail.TransactionTypeID = 'BT'
													and TransactionStatusID in ('CO')
													And TransactionMethodID = 'SE' 
													and [Transaction].BalanceTypeKey = 4
													and Wallet.WalletID != 'B'
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, count (Distinct BetsCashedOut) as BetsCashedOut
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		BetsCashedOut is not null
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		Settled != BetsCashedOut	
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.38', 'DataValid_Fact_KPI BetsCashedOut', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.38', 'DataValid_Fact_KPI BetsCashedOut', 'Fail', 'Errors in the BetsCashedOut column')
End	

--[Cashout]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Sum(Data.TransactedAmount) as TransactedAmount
								From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactedAmount
											From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
											Where		TransactionDetail.TransactionTypeID = 'BT'
													and TransactionStatusID in ('CO')
													and [Transaction].PromotionKey in (0, -1)
													and [Transaction].BalanceTypeKey = 2
										) as Data
								Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (CashOut) as CashOut
											From		[$(Dimensional)].[Fact].[KPI]
											Where		CashOut != 0
											Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != CashOut
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.39', 'DataValid_Fact_KPI CashOut', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.39', 'DataValid_Fact_KPI CashOut', 'Fail', 'Errors in the CashOut column')
End

--[CashoutDifferential]

--[WithdrawalsRequested]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Sum(Data.TransactedAmount) as TransactedAmount
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactedAmount
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
										 Where		TransactionDetail.TransactionTypeID = 'WI'
													and TransactionStatusID in ('RE', 'CN', 'DE', 'RJ', 'RV') 
													and [Transaction].BalanceTypeKey = 2
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (WithdrawalsRequested) as WithdrawalsRequested
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		WithdrawalsRequested != 0
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != WithdrawalsRequested	
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.18', 'DataValid_Fact_KPI WithdrawalsRequested', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6405'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.18', 'DataValid_Fact_KPI WithdrawalsRequested', 'Pass', 'CCIA-6405')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.18', 'DataValid_Fact_KPI WithdrawalsRequested', 'Fail', 'Errors in the WithdrawalsRequested column')
	End
End

--[WithdrawalsCompleted]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Sum(Data.TransactedAmount) as TransactedAmount
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactedAmount
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
										 Where		TransactionDetail.TransactionTypeID = 'WI'
													and TransactionStatusID = 'CO'
													and [Transaction].BalanceTypeKey = 2
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (WithdrawalsCompleted) as WithdrawalsCompleted
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		WithdrawalsCompleted != 0
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != WithdrawalsCompleted	
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.19', 'DataValid_Fact_KPI WithdrawalsCompleted', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.19', 'DataValid_Fact_KPI WithdrawalsCompleted', 'Fail', 'Errors in the WithdrawalsCompleted column')
End	

--[WithdrawalsRequestedCount]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Count(Data.TransactionId) as TransactedAmount
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactionId
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
										 Where		TransactionDetail.TransactionTypeID = 'WI'
													and TransactionStatusID in ('RE', 'CN', 'DE', 'RJ', 'RV') 
													and [Transaction].BalanceTypeKey = 2
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Count (Distinct WithdrawalsRequestedCount) as WithdrawalsRequested
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		WithdrawalsRequested != 0
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != WithdrawalsRequested	
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.19a', 'DataValid_Fact_KPI WithdrawalsRequestedCount', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7665'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.19a', 'DataValid_Fact_KPI WithdrawalsRequestedCount', 'Pass', 'CCIA-7665')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.19a', 'DataValid_Fact_KPI WithdrawalsRequestedCount', 'Fail', 'Errors in the WithdrawalsRequestedCount column')
	End
End

--[WithdrawalsCompletedCount]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Count(Data.TransactionId) as TransactedAmount
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactionId
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
										 Where		TransactionDetail.TransactionTypeID = 'WI'
													and TransactionStatusID = 'CO'
													and [Transaction].BalanceTypeKey = 2
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Count (Distinct WithdrawalsCompletedCount) as WithdrawalsCompleted
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		WithdrawalsCompleted != 0
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != WithdrawalsCompleted	
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.19b', 'DataValid_Fact_KPI WithdrawalsCompletedCount', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.19b', 'DataValid_Fact_KPI WithdrawalsCompletedCount', 'Fail', 'Errors in the WithdrawalsCompletedCount column')
End	

--[Adjustments]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Sum(Data.TransactedAmount) as TransactedAmount
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactedAmount
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
										 Where		(
														(
															TransactionDetail.TransactionTypeID = 'AD'
															and TransactionStatusID = 'CO'
														)
														or
														(
															TransactionDetail.TransactionTypeID = 'BO'
															and TransactionStatusID = 'CO'
															and TransactionMethodID in ('BP', 'RB') 
														)
													)
													and [Transaction].BalanceTypeKey = 2
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (Adjustments) as Adjustments
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		Adjustments != 0
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != Adjustments	
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.20', 'DataValid_Fact_KPI Adjustments', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.20', 'DataValid_Fact_KPI Adjustments', 'Fail', 'Errors in the Adjustments column')
End	
		
--[BetsResulted]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Count(Distinct TransactionId) as Settled
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactionId
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
													inner join [$(Dimensional)].Dimension.Wallet as Wallet on [Transaction].WalletKey = Wallet.WalletKey
										 Where		TransactionDetail.TransactionTypeID = 'BT'
													and TransactionStatusID in ('WN', 'LO', 'UN','CO')
													And TransactionMethodID = 'SE' 
													and [Transaction].BalanceTypeKey = 4
													and Wallet.WalletID != 'B'
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, count (Distinct BetsResulted) as BetsResulted
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		BetsResulted is not null
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		Settled != BetsResulted	
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.21', 'DataValid_Fact_KPI BetsResulted', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6406'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.21', 'DataValid_Fact_KPI BetsResulted', 'Pass', 'CCIA-6406')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.21', 'DataValid_Fact_KPI BetsResulted', 'Fail', 'Errors in the BetsResulted column')
	End
End	

--[BonusBetsResulted]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Count(Distinct TransactionId) as Settled
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactionId
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
													inner join [$(Dimensional)].Dimension.Wallet as Wallet on [Transaction].WalletKey = Wallet.WalletKey
										 Where		TransactionDetail.TransactionTypeID = 'BT'
													and TransactionStatusID in ('WN', 'LO', 'UN','CO')
													And TransactionMethodID = 'SE' 
													and [Transaction].BalanceTypeKey = 4
													and Wallet.WalletID = 'B'
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, count (Distinct BonusBetsResulted) as BetsResulted
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		BonusBetsResulted is not null
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		Settled != BetsResulted	
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.21a', 'DataValid_Fact_KPI BonusBetsResulted', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7111'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.21a', 'DataValid_Fact_KPI BonusBetsResulted', 'Pass', 'CCIA-7111')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.21a', 'DataValid_Fact_KPI BonusBetsResulted', 'Fail', 'Errors in the BonusBetsResulted column')
	End
End	

--[ContractsResulted]
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6407'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.22', 'DataValid_Fact_KPI ContractsResulted', 'Pass', 'CCIA-6407')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.22', 'DataValid_Fact_KPI ContractsResulted', 'Fail', 'Test Required for ContractsResulted column')
	End

--[BonusContractsResulted]
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6407'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.22a', 'DataValid_Fact_KPI BonusContractsResulted', 'Pass', 'CCIA-6407')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.22a', 'DataValid_Fact_KPI BonusContractsResulted', 'Fail', 'Test Required for BonusContractsResulted column')
	End

--[BettorsResulted]
If not exists	(
				select		* 
				from		[$(Dimensional)].[Fact].[kpi] as KPI
				Where		BetsResulted is not null
							and BettorsResulted is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.23a', 'DataValid_Fact_KPI BettorsResulted', 'Pass', NULL)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.23a', 'DataValid_Fact_KPI BettorsResulted', 'Fail', 'Error in the BettorsResulted column')
End

If not exists	(
				select		* 
				from		[$(Dimensional)].[Fact].[kpi] as KPI
							inner join [$(Dimensional)].Dimension.Account as Account on KPI.AccountKey = Account.AccountKey
				Where		BettorsResulted != Account.LedgerID
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.23b', 'DataValid_Fact_KPI BettorsResulted', 'Pass', NULL)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7092'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.23b', 'DataValid_Fact_KPI BettorsResulted', 'Pass', 'CCIA-7092')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.23b', 'DataValid_Fact_KPI BettorsResulted', 'Fail', 'Error in the BettorsResulted column')
	End
End

--[BonusBettorsResulted]
If not exists	(
				select		* 
				from		[$(Dimensional)].[Fact].[kpi] as KPI
				Where		BonusBetsResulted is not null
							and BonusBettorsResulted is null
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.23c', 'DataValid_Fact_KPI BettorsResulted', 'Pass', NULL)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.23c', 'DataValid_Fact_KPI BettorsResulted', 'Fail', 'Error in the BettorsResulted column')
End

If not exists	(
				select		* 
				from		[$(Dimensional)].[Fact].[kpi] as KPI
							inner join [$(Dimensional)].Dimension.Account as Account on KPI.AccountKey = Account.AccountKey
				Where		BonusBettorsResulted != Account.LedgerID
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.23d', 'DataValid_Fact_KPI BonusBettorsResulted', 'Pass', NULL)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7092'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.23d', 'DataValid_Fact_KPI BonusBettorsResulted', 'Pass', 'CCIA-7092')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.23d', 'DataValid_Fact_KPI BonusBettorsResulted', 'Fail', 'Error in the BonusBettorsResulted column')
	End
End

--[PlayDaysResulted]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
				where	PlayDaysPlaced is not null
						and Cast (PlayDaysResulted as varchar(100)) != Cast (BettorsResulted as varchar(100)) + Cast (Left (DayKey, 8) as char(8))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.25', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.25', 'DataValid_Fact_KPI', 'Fail', 'Errors in the PlayDaysResulted column')
End

--[BonusPlayDaysResulted]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
				where	BonusPlayDaysPlaced is not null
						and Cast (BonusPlayDaysResulted as varchar(100)) != Cast (BonusBettorsResulted as varchar(100)) + Cast (Left (DayKey, 8) as char(8))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.25a', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.25a', 'DataValid_Fact_KPI', 'Fail', 'Errors in the BonusPlayDaysResulted column')
End

--[PlayFiscalWeeksResulted]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	PlayDaysPlaced is not null
						and Cast (PlayFiscalWeeksResulted as varchar(100)) != Cast (BettorsResulted as varchar(100)) + Cast (FiscalWeekKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.26', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.26', 'DataValid_Fact_KPI', 'Fail', 'Errors in the PlayFiscalWeeksResulted column')
End

--[BonusPlayFiscalWeeksResulted]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	BonusPlayDaysPlaced is not null
						and Cast (BonusPlayFiscalWeeksResulted as varchar(100)) != Cast (BonusBettorsResulted as varchar(100)) + Cast (FiscalWeekKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.26a', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.26a', 'DataValid_Fact_KPI', 'Fail', 'Errors in the PlayFiscalWeeksResulted column')
End

--[PlayCalenderWeeksResulted]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	PlayDaysPlaced is not null
						and Cast ([PlayCalenderWeeksResulted] as varchar(100)) != Cast (BettorsResulted as varchar(100)) + Cast (WeekKey as char(6))
				)
Begin
		Insert into test.tblResults Values (@TestRunNo, '139.27', 'DataValid_Fact_KPI', 'Pass', null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5641'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.27', 'DataValid_Fact_KPI', 'Pass', 'CCIA-5641')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.27', 'DataValid_Fact_KPI', 'Fail', 'Errors in the PlayCalenderWeeksResulted column')
	End
End

--[BonusPlayCalenderWeeksResulted]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	BonusPlayDaysPlaced is not null
						and Cast ([BonusPlayCalenderWeeksResulted] as varchar(100)) != Cast (BonusBettorsResulted as varchar(100)) + Cast (WeekKey as char(6))
				)
Begin
		Insert into test.tblResults Values (@TestRunNo, '139.27', 'DataValid_Fact_KPI', 'Pass', null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5641'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.27', 'DataValid_Fact_KPI', 'Pass', 'CCIA-5641')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.27', 'DataValid_Fact_KPI', 'Fail', 'Errors in the BonusPlayCalenderWeeksResulted column')
	End
End

--[PlayFiscalMonthsResulted]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	PlayDaysPlaced is not null
						and Cast ([PlayFiscalMonthsResulted] as varchar(100)) != Cast (BettorsResulted as varchar(100)) + Cast (FiscalMonthKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.28', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.28', 'DataValid_Fact_KPI', 'Fail', 'Errors in the PlayFiscalMonthsResulted column')
End

--[BonusPlayFiscalMonthsResulted]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	PlayDaysPlaced is not null
						and Cast ([BonusPlayFiscalMonthsResulted] as varchar(100)) != Cast (BonusBettorsResulted as varchar(100)) + Cast (FiscalMonthKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.28a', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.28a', 'DataValid_Fact_KPI', 'Fail', 'Errors in the BonusPlayFiscalMonthsResulted column')
End

--[PlayCalenderMonthsResulted]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	PlayDaysPlaced is not null
						and Cast ([PlayCalenderMonthsResulted] as varchar(100)) != Cast (BettorsResulted as varchar(100)) + Cast (MonthKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.29', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.29', 'DataValid_Fact_KPI', 'Fail', 'Errors in the PlayCalenderMonthsResulted column')
End

--[BonusPlayCalenderMonthsResulted]
If not exists	(
				SELECT	*
				FROM	[$(Dimensional)].[Fact].[KPI] as KPI
						inner join [$(Dimensional)].Dimension.[DayZone] as DayZone on KPI.DayKey = DayZone.DayKey
						inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
				where	BonusPlayDaysPlaced is not null
						and Cast ([BonusPlayCalenderMonthsResulted] as varchar(100)) != Cast (BonusBettorsResulted as varchar(100)) + Cast (MonthKey as char(6))
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.29a', 'DataValid_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.29a', 'DataValid_Fact_KPI', 'Fail', 'Errors in the BonusPlayCalenderMonthsResulted column')
End

--[Turnover]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, sum(Data.TransactedAmount) as TransactedAmount
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactedAmount * -1 as TransactedAmount
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
													inner join [$(Dimensional)].Dimension.Wallet as Wallet on [Transaction].WalletKey = Wallet.WalletKey
										 Where		TransactionDetail.TransactionTypeID = 'BT'
													and TransactionMethodID='SE'
													and TransactionStatusID in ('WN', 'LO', 'UN','CO')
													and [Transaction].BalanceTypeKey = 4
													and Wallet.WalletId != 'B'
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, sum (Turnover) as Turnover
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		Turnover != 0
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != Turnover		
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.30', 'DataValid_Fact_KPI Turnover', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7091'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.30', 'DataValid_Fact_KPI Turnover', 'Pass', 'CCIA-7091')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.30', 'DataValid_Fact_KPI Turnover', 'Fail', 'Errors in the Turnover column')
	End
End

--[BonusTurnover]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, sum(Data.TransactedAmount) as TransactedAmount
							 From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactedAmount * -1 as TransactedAmount
										 From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
													inner join [$(Dimensional)].Dimension.Wallet as Wallet on [Transaction].WalletKey = Wallet.WalletKey
										 Where		TransactionDetail.TransactionTypeID = 'BT'
													and TransactionMethodID='SE'
													and TransactionStatusID in ('WN', 'LO', 'UN','CO')
													and [Transaction].BalanceTypeKey = 4
													and Wallet.WalletID = 'B'
										) as Data
							 Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, sum (BonusTurnover) as Turnover
										 From		[$(Dimensional)].[Fact].[KPI]
										 Where		BonusTurnover != 0
										 Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != Turnover		
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.30a', 'DataValid_Fact_KPI BonusTurnover', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-7091'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.30a', 'DataValid_Fact_KPI BonusTurnover', 'Pass', 'CCIA-7091')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.30a', 'DataValid_Fact_KPI BonusTurnover', 'Fail', 'Errors in the BonusTurnover column')
	End
End

--[GrossWin]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, sum(Data.TransactedAmount) as TransactedAmount
								From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactedAmount as TransactedAmount
											From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
											Where		TransactionDetail.TransactionTypeID = 'BT'
													and TransactionStatusID in ('WN', 'LO', 'UN','CO')
													and [Transaction].BalanceTypeKey = 3
													and [Transaction].PromotionKey in (0,-1)
										) as Data
								Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, sum (GrossWin) as GrossWin
											From		[$(Dimensional)].[Fact].[KPI]
											Where		GrossWin != 0
											Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != GrossWin	
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.31', 'DataValid_Fact_KPI GrossWin', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6408'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.31', 'DataValid_Fact_KPI GrossWin', 'Pass', 'CCIA-6408')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.31', 'DataValid_Fact_KPI GrossWin', 'Fail', 'Errors in the GrossWin column')
	End
End

--[NetRevenue]
If not exists	(
				select * from [$(Dimensional)].[Fact].[kpi] as KPI
				where GrossWin - BonusWinnings != NetRevenue
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.32', 'DataValid_Fact_KPI NetRevenue', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6410'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.32', 'DataValid_Fact_KPI NetRevenue', 'Pass', 'CCIA-6410')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.32', 'DataValid_Fact_KPI NetRevenue', 'Fail', 'Errors in the NetRevenue column')
	End
End

--[NetRevenueAdjustment]
Insert into test.tblResults Values (@TestRunNo, '139.33', 'DataValid_Fact_KPI NetRevenueAdjustment', 'Pass', 'CCIA-7093')

--[BonusWinnings]
If not exists	(
				Select		*
				FROM		(Select		Data.AccountKey
										, Data.DayKey
										, Sum(Data.TransactedAmount) as TransactedAmount
								From		(
										Select		[Transaction].AccountKey
													, [Transaction].DayKey
													, TransactedAmount
											From		[$(Dimensional)].[Fact].[Transaction] as [Transaction]
													inner join [$(Dimensional)].Dimension.TransactionDetail as TransactionDetail on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
											Where		TransactionDetail.TransactionTypeID = 'BT'
													and TransactionStatusID in ('WN', 'UN')
													and [Transaction].PromotionKey not in (0, -1)
													and [Transaction].BalanceTypeKey = 2
										) as Data
								Group by	Data.AccountKey
										, Data.DayKey
							) as TData
							left join	(Select		AccountKey
													, DayKey
													, Sum (BonusWinnings) as BonusWinnings
											From		[$(Dimensional)].[Fact].[KPI]
											Where		BonusWinnings != 0
											Group by	AccountKey
													, DayKey
										) as KPI on TData.AccountKey = KPI.AccountKey
														and TData.DayKey = KPI.DayKey
				Where		TransactedAmount != BonusWinnings
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '139.34', 'DataValid_Fact_KPI BonusWinnings', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-6404'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.34', 'DataValid_Fact_KPI BonusWinnings', 'Pass', 'CCIA-6404')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '139.34', 'DataValid_Fact_KPI BonusWinnings', 'Fail', 'Errors in the BonusWinnings column')
	End
End

--[Betback]
Insert into test.tblResults Values (@TestRunNo, '139.35', 'DataValid_Fact_KPI Betback', 'Pass', 'CCIA-7093')

--[GrossWinAdjustment]
Insert into test.tblResults Values (@TestRunNo, '139.36', 'DataValid_Fact_KPI GrossWinAdjustment', 'Pass', 'CCIA-7093')

--StructureKey