﻿--Test Code
--exec Test.[164DataValid_Dimension_Campaign] 'ADHOC'
Create Procedure Test.[164DataValid_Dimension_Campaign] @TestRunNo varchar(23)

as

Select		*
Into		#Data
From		[$(Dimensional)].[Dimension].[Campaign] with (nolock)

--CampaignKey n/a
--CampaignId
If not exists	  (
					Select		*
					From		#Data
					where		CampaignId < 1
								and CampaignKey not in (0,-1)
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '164', 'DataValid_Dimension_Campaign CampaignId', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '164', 'DataValid_Dimension_Campaign CampaignId', 'Fail', 'CampaignId Incorrect')
End

--CampaignName
If not exists	  (
					Select		*
					From		#Data
					where		CampaignName = ''
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.1', 'DataValid_Dimension_Campaign CampaignName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.1', 'DataValid_Dimension_Campaign CampaignName', 'Fail', 'CampaignName Incorrect')
End

--CampaignComment
If not exists	  (
					Select		*
					From		#Data
					where		CampaignComment = ''
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.2', 'DataValid_Dimension_Campaign CampaignComment', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.2', 'DataValid_Dimension_Campaign CampaignComment', 'Fail', 'CampaignComment Incorrect')
End

--CampaignActive
If not exists	  (
					Select		*
					From		#Data
					where		CampaignActive not in ('No', 'Yes')
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.3', 'DataValid_Dimension_Campaign CampaignActive', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.3', 'DataValid_Dimension_Campaign CampaignActive', 'Fail', 'CampaignActive Incorrect')
End

--CreatedDayKey
If not exists	  (
					Select		*
					From		#Data
					where		CreatedDayKey < 0
								and CampaignKey not in (0,-1)
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.4', 'DataValid_Dimension_Campaign CreatedDayKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.4', 'DataValid_Dimension_Campaign CreatedDayKey', 'Fail', 'CreatedDayKey Incorrect')
End

--CampaignTypeKey
If not exists	  (
					Select		*
					From		#Data
					where		CampaignTypeKey < 0
								and CampaignKey not in (0,-1)
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.5', 'DataValid_Dimension_Campaign CampaignTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.5', 'DataValid_Dimension_Campaign CampaignTypeKey', 'Fail', 'CampaignTypeKey Incorrect')
End

--CampaignTypeId
If not exists	  (
					Select		*
					From		#Data
					where		CampaignTypeId < 0
								and CampaignKey not in (0,-1)
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.6', 'DataValid_Dimension_Campaign CampaignTypeId', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.6', 'DataValid_Dimension_Campaign CampaignTypeId', 'Fail', 'CampaignTypeId Incorrect')
End

--CampaignTypeName
If not exists	  (
					Select		*
					From		#Data
					where		CampaignTypeName = ''
								and CampaignKey not in (0,-1)
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.7', 'DataValid_Dimension_Campaign CampaignTypeName', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.7', 'DataValid_Dimension_Campaign CampaignTypeName', 'Fail', 'CampaignTypeName Incorrect')
End

--CampaignTypeActive
If not exists	  (
					Select		*
					From		#Data
					where		CampaignTypeActive not in ('No', 'Yes')
					)
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.8', 'DataValid_Dimension_Campaign CampaignTypeActive', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '164.8', 'DataValid_Dimension_Campaign CampaignTypeActive', 'Fail', 'CampaignTypeActive Incorrect')
End

--Metadata n/a
--FromDate
--ToDate
--FirstDate
--CreatedDate
--CreatedBatchKey
--CreatedBy
--ModifiedDate
--ModifiedBatchKey
--ModifiedBy
