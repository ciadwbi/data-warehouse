﻿--Test Code
--exec Test.[309BVA_Dimension_Communication] 'ADHOC', '2015-04-03'

Create Procedure Test.[309BVA_Dimension_Communication] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

Select		* 
into		#Data
From		[$(Dimensional)].Dimension.Communication
where		[CommunicationKey] not in (0, -1)
			--and ((FromDate > @FromDate)
			--		or (@FromDate is null))
			--and ToDate = '9999-12-31'

--CommunicationKey n/a
--CommunicationId
If not exists(
				Select		*
				From		#Data
				where		CommunicationId <= 0
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '309.0', 'BVA_Dimension_Communication CommunicationId', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '309.0', 'BVA_Dimension_Communication CommunicationId', 'Fail', 'There are records with an invalid CommunicationId' + @ErrorText)
End

--CommunicationSource
If not exists(
				Select		*
				From		#Data
				where		CommunicationSource != 'ET'
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '309.1', 'BVA_Dimension_Communication CommunicationSource', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '309.1', 'BVA_Dimension_Communication CommunicationSource', 'Fail', 'There are records with an invalid CommunicationSource' + @ErrorText)
End

--Title
If not exists(
				Select		*
				From		#Data
				where		Title = ''
							or Title is null
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '309.2', 'BVA_Dimension_Communication Title', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '309.2', 'BVA_Dimension_Communication Title', 'Fail', 'There are records with an invalid Title' + @ErrorText)
End

--Details
If not exists(
				Select		*
				From		#Data
				where		Details = ''
							or Details is null
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '309.3', 'BVA_Dimension_Communication Details', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '309.3', 'BVA_Dimension_Communication Details', 'Fail', 'There are records with an invalid Details' + @ErrorText)
End

--SubDetails n/a any value is acceptable
--CreatedDate n/a
--ModifiedDate n/a
--CreatedBatchKey n/a
--CreatedBy n/a
--ModifiedBatchKey n/a
--ModifiedBy n/a