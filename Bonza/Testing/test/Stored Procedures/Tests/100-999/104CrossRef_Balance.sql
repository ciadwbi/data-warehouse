﻿--Test Code
--exec Test.[104CrossRef_Balance] 'ADHOC'

Create Procedure [Test].[104CrossRef_Balance] @TestRunNo varchar(23)

as
/*
Declare @FromDate date
Set @FromDate = (Select Max (BatchToDate) from [$(Staging)].[Control].[ETLController] where BatchStatus = 1)

Set @FromDate = DateAdd(Day,-1,@FromDate)

If not exists(
				Select		Balance.AccountKey
							, Balance.DayKey
							, Balance.OpeningBalance
							, Balance.ClosingBalance
							, Position.OpeningBalance
							, Position.ClosingBalance
				From		(
							Select		* 
							From		[$(Dimensional)].Fact.Balance
							Where		BalanceTypeKey = 2
										and WalletKey = 1 
										and DayKey in (Select		Distinct DayKey
																	From		(
																				Select		DayZone.DayKey
																				From		[$(Dimensional)].Dimension.[Day] as [Day]
																							inner join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on [Day].DayKey = DayZone.MasterDayKey
																				Where		DayDate = @FromDate
																				union
																				Select		DayZone.DayKey
																				From		[$(Dimensional)].Dimension.[Day] as [Day]
																							inner join [$(Dimensional)].Dimension.[DayZone] as [DayZone] on [Day].DayKey = DayZone.AnalysisDayKey
																				Where		DayDate = @FromDate
																				) as Data
														)
							) as Balance
							left join	(
										Select		DayKey
													, OpeningBalance
													, ClosingBalance
													, AccountKey
										From		[$(Dimensional)].Fact.Position
										Where		BalanceTypeKey = 2
										) as Position on Balance.AccountKey = Position.AccountKey
														and Balance.DayKey = Position.DayKey
				Where		Balance.OpeningBalance != Position.OpeningBalance	
							or Balance.ClosingBalance != Position.ClosingBalance		
			 ) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '104', 'CrossRef_Balance Wallet', 'Pass', Null)
End
Else
Begin
	--CCIA-4085
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-4085'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '104', 'CrossRef_Balance Wallet', 'Pass', 'CCIA-4085')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '104', 'CrossRef_Balance Wallet', 'Fail', 'Mismatches between Balance and Position Stars')
	End
End
*/