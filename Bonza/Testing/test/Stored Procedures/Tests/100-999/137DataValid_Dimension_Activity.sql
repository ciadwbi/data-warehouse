﻿--Test Code
--exec Test.[137DataValid_Dimension_Activity] 'ADHOC'
Create Procedure Test.[137DataValid_Dimension_Activity] @TestRunNo varchar(23)

as

--this test is about confirming that all possible permutations are represented in the table
If not exists  (
			Select		tblBinaryValues.Bin
						, BinaryValues.BinaryValues
			From		test.tblBinaryValues as tblBinaryValues
						Left Join	(
									 Select	HasDeposited + HasWithdrawn + HasBet + HasWon + HasLost as BinaryValues
									 From	(		
											Select		'HasDeposited' = CASE
															When HasDeposited = 'No' Then '0'
															When HasDeposited = 'Yes' Then '1'
															Else 'X'
															End
														, 'HasWithdrawn' = CASE
															When HasWithdrawn = 'No' Then '0'
															When HasWithdrawn = 'Yes' Then '1'
															Else 'X'
															End
														, 'HasBet' = CASE
															When HasBet = 'No' Then '0'
															When HasBet = 'Yes' Then '1'
															Else 'X'
															End
														, 'HasWon' = CASE
															When HasWon = 'No' Then '0'
															When HasWon = 'Yes' Then '1'
															Else 'X'
															End
														, 'HasLost' = CASE
															When HasLost = 'No' Then '0'
															When HasLost = 'Yes' Then '1'
															Else 'X'
															End
											From		[$(Dimensional)].[Dimension].[Activity]
											where		ActivityKey not in (0, -1)
											) as Data
									) as BinaryValues on Right (tblBinaryValues.Bin, 5) = BinaryValues.BinaryValues -- the Right Function is equal to the number of columns
Where		tblBinaryValues.ID <= 32 --the max number of permutations possible
			and BinaryValues.BinaryValues is null
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '137', 'DataValid_Dimension_Activity', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '137', 'DataValid_Dimension_Activity', 'Fail', 'Permutations Missing')
End