﻿--test
--exec [Test].[159DataDuplicates_Dimension_BetStatus] 'ADHOC'

CREATE PROCEDURE [Test].[159DataDuplicates_Dimension_BetStatus] @TestRunNo varchar(23)

as

If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.BetStatus as BetStatus
		Group by	BetStatus.StatusCode
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '159', 'DataDuplicates_Dimension_BetStatus', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '159', 'DataDuplicates_Dimension_BetStatus', 'Fail', 'Duplicates of BetStatus found')
End