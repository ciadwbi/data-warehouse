﻿--test
--exec [Test].[125DataDuplicates_Fact_KPI] 'ADHOC', null

CREATE PROCEDURE [Test].[125DataDuplicates_Fact_KPI] @TestRunNo varchar(23), @FromDate datetime

as


If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Fact.KPI as KPI with (nolock)
					inner join [$(Dimensional)].Dimension.DayZone as DayZone with (nolock) on KPI.DayKey = DayZone.DayKey
					inner join [$(Dimensional)].Dimension.[Day] as [Day] with (nolock) on DayZone.MasterDayKey = [Day].DayKey
		where		[Day].DayDate = Cast (@FromDate as Date)
					or @FromDate is null	
		Group by	[ContractKey], KPI.[DayKey], [AccountKey], [BetTypeKey], [LegTypeKey], [ChannelKey], [CampaignKey], [MarketKey], [ClassKey], [UserKey], [InstrumentKey], [AccountStatusKey], [ActionChannelKey], [InPlayKey]
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '125', 'DataDuplicates_Fact_KPI', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '125', 'DataDuplicates_Fact_KPI', 'Fail', 'Duplicates of KPI found')
End
