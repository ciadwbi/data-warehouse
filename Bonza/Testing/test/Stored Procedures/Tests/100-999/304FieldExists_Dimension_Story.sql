--Test Code
--exec Test.[304FieldExists_Dimension_Story] 'ADHOC'
Create Procedure Test.[304FieldExists_Dimension_Story] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
			 [StoryKey]
			,[Story]
			--, FirstDate CCIA-6649
			--, FromDate CCIA-6649
			--, ToDate CCIA-6649
			--,[CreatedDate] CCIA-6649
			--,[CreatedBatchKey] CCIA-6649
			--,[CreatedBy] CCIA-6649
			--,[ModifiedDate] CCIA-6649
			--,[ModifiedBatchKey] CCIA-6649
			--,[ModifiedBy] CCIA-6649
			from ' + @Database + '.[Dimension].[InteractionType]'

Exec Test.spSelectValid @TestRunNo, '304','Structure - Dimension Story Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Story'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 2
Begin
	Insert into test.tblResults Values (@TestRunNo, '304.1', 'Structure - Dimension Story Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '303.1', 'Structure - Dimension InteractionType Table', 'Fail', 'Wrong number of columns')
End


