﻿--Test Code
--exec Test.[48DataDuplicates_Dimension_Channel] 'ADHOC'

Create Procedure Test.[48DataDuplicates_Dimension_Channel] @TestRunNo varchar(23)

as

If not exists(
		Select		Channel1.ChannelKey
					, Channel1.Channelid
					, Channel2.ChannelKey
					, Channel2.Channelid
		From		[$(Dimensional)].Dimension.Channel as Channel1
		inner join	[$(Dimensional)].Dimension.Channel as Channel2 on Channel1.Channelid = Channel2.Channelid
		Where		Channel1.ChannelKey != Channel2.ChannelKey
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '48', 'DataDuplicates_Dimension_Channel', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '48', 'DataDuplicates_Dimension_Channel', 'Fail', 'Duplicates of ChannelName found')
End

