﻿--Test Code
--exec Test.[31DataDefault_Dimension_Class] 'ADHOC'
Create Procedure Test.[31DataDefault_Dimension_Class] @TestRunNo varchar(23)

as

If exists(
		Select		ClassKey
		From		[$(Dimensional)].[Dimension].[Class]
		where		ClassKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '31', 'DataDefault_Dimension_Class Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '31', 'DataDefault_Dimension_Class Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		ClassKey
		From		[$(Dimensional)].[Dimension].[Class]
		where		ClassKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '31.1', 'DataDefault_Dimension_Class Table ID-1', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5058'
							and [Status] != 'Done'
				) 
	and exists(
		Select		ClassKey
		From		[$(Dimensional)].[Dimension].[Class]
		where		ClassKey = -2
		)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '31.1', 'DataDefault_Dimension_Class Table ID-1', 'Pass', 'CCIA-5058')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '31.1', 'DataDefault_Dimension_Class Table ID-1', 'Fail', 'No ID -1 record found')
	End
End