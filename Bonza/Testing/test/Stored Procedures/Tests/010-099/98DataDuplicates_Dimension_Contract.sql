﻿--Test Code
--exec Test.[98DataDuplicates_Dimension_Contract] 'ADHOC', null

Create Procedure Test.[98DataDuplicates_Dimension_Contract] @TestRunNo varchar(23), @FromDate datetime2

as

If not exists(
		Select		Count(*)
		From		[$(Dimensional)].Dimension.[Contract] as [Contract]
		where		@FromDate is null
					or CreatedDate >= @FromDate
		Group by	[LegId], [LegIdSource]
		Having		Count(*) > 1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '98', 'DataDuplicates_Dimension_Contract', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '98', 'DataDuplicates_Dimension_Contract', 'Fail', 'Duplicates of Contract found')
End





