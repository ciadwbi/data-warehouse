--Test Code
--exec Test.[13FieldExists_Dimension_Balance] 'ADHOC'
Create Procedure Test.[13FieldExists_Dimension_Balance] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'select top 1
[BalanceTypeKey]
      ,[BalanceTypeID]
      ,[BalanceTypeName]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
  FROM ' + @Database + '.[Dimension].[BalanceType]'

Exec Test.spSelectValid @TestRunNo, '13','Structure - Dimension Balance Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'BalanceType'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 12
Begin
	Insert into test.tblResults Values (@TestRunNo, '13.1', 'Structure - Dimension BalanceType Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '13.1', 'Structure - Dimension BalanceType Table', 'Fail', 'Wrong number of columns')
End