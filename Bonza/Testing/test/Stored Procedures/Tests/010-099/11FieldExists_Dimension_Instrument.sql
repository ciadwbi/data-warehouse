--Test Code
--exec Test.[11FieldExists_Dimension_Instrument] 'ADHOC'
Create Procedure Test.[11FieldExists_Dimension_Instrument] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'select top 1 [InstrumentKey]
[InstrumentKey]
      ,[InstrumentID]
      ,[Source]
      ,[AccountNumber]
      ,[SafeAccountNumber]
      ,[AccountName]
      ,[BSB]
      ,[ExpiryDate]
      ,[Verified]
      ,[VerifyAmount]
      ,[InstrumentTypeKey]
      ,[InstrumentType]
      ,[ProviderKey]
      ,[ProviderName]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
  FROM ' + @Database + '.[Dimension].[Instrument]'

Exec Test.spSelectValid @TestRunNo, '11','Structure - Dimension Instrument Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'Instrument'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Dimension'
		) = 23
Begin
	Insert into test.tblResults Values (@TestRunNo, '11.1', 'Structure - Dimension Instrument Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '11.1', 'Structure - Dimension Instrument Table', 'Fail', 'Wrong number of columns')
End
