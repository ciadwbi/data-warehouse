﻿--Test Code
--exec Test.[51DataDuplicates_Dimension_Note] 'ADHOC', null

Create Procedure Test.[51DataDuplicates_Dimension_Note] @TestRunNo varchar(23), @FromDate datetime

as

--CCIA-3315 - columns missing that would normally be in a type 1 table

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

If not exists(
				Select		Count(*)
				From		[$(Dimensional)].Dimension.Note as Note
				where		(
								(Note.FromDate > @FromDate)
								or (@FromDate is null)
							)
				Group by	NoteId
				Having		Count(*) > 1
							
		) 
Begin

	Insert into test.tblResults Values (@TestRunNo, '51', 'DataDuplicates_Dimension_Note', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '51', 'DataDuplicates_Dimension_Note', 'Fail', 'Duplicates of NoteID found' + @ErrorText)
End