﻿--Test Code
--exec Test.[23DataDefault_Dimension_Day] 'ADHOC'
Create Procedure Test.[23DataDefault_Dimension_Day] @TestRunNo varchar(23)

as

If exists(
		Select		DayKey
		From		[$(Dimensional)].[Dimension].[Day]
		where		DayKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '23', 'DataDefault_Dimension_Day ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '23', 'DataDefault_Dimension_Day ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		DayKey
		From		[$(Dimensional)].[Dimension].[Day]
		where		DayKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '23.1', 'DataDefault_Dimension_Day ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '23.1', 'DataDefault_Dimension_Day ID-1', 'Fail', 'No ID -1 record found')
End

If exists(
		Select		DayKey
		From		[$(Dimensional)].[Dimension].[DayZone]
		where		DayKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '23.4', 'DataDefault_Dimension_DayZone ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '23.4', 'DataDefault_Dimension_DayZone ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		DayKey
		From		[$(Dimensional)].[Dimension].[DayZone]
		where		DayKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '23.5', 'DataDefault_Dimension_DayZone ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '23.5', 'DataDefault_Dimension_DayZone ID-1', 'Fail', 'No ID -1 record found')
End