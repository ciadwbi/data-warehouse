﻿--Test Code
--exec Test.[26DataDefault_Dimension_User] 'ADHOC'
Create Procedure Test.[26DataDefault_Dimension_User] @TestRunNo varchar(23)

as

If exists(
		Select		UserKey
		From		[$(Dimensional)].[Dimension].[User]
		where		UserKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '26', 'DataDefault_Dimension_User Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '26', 'DataDefault_Dimension_User Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		UserKey
		From		[$(Dimensional)].[Dimension].[User]
		where		UserKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '26.1', 'DataDefault_Dimension_User Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '26.1', 'DataDefault_Dimension_User Table ID-1', 'Fail', 'No ID -1 record found')
End


