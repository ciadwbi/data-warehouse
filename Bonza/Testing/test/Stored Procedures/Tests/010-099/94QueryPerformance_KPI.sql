--Test Code
--exec Test.[94QueryPerformance_KPI] 'JF_5May2015_21'
Create Procedure [Test].[94QueryPerformance_KPI] @TestRunNo varchar(23)

as

Declare @Time int

--Simple (1 star, 1 day)
exec [Test].[spExecuteMDX] @TestRunNo, 'KPI', 'Simple', 10000,
	' Select * From OpenQuery([ANALYSISSERVICES],''
		SELECT 
		NON EMPTY
		{[Measures].[Turnover],[Measures].[GrossWin]} on Columns,
		[Company].[CompanyName].[CompanyName] on rows
		FROM [Bonza]
		WHERE 
		([Day].[DayDate].&[2015-03-03T00:00:00])
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'KPI'
							and [TestName] = 'Simple'
				)
IF @Time <= 10000
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '94', 'QueryPerformance_KPI_Simple', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '94', 'QueryPerformance_KPI_Simple', 'Fail', 'Time taken is ' + cast (@Time as varchar(100)) + ' msec and should be less than 10000')
End

--Medium (1 star, 2 months)
exec [Test].[spExecuteMDX] @TestRunNo, 'KPI', 'Medium', 60000,
	' Select * From OpenQuery([ANALYSISSERVICES],''
		WITH MEMBER [Measures].[NetValue] AS [Measures].[Turnover] - [Measures].[GrossWin]
		SELECT 
		NON EMPTY
		{[Measures].[TurnoverPlaced],[Measures].[GrossWin],[Measures].[NetRevenue],[Measures].[NetValue]} on Columns,
		CROSSJOIN([Company].[BusinessUnitName].[BusinessUnitName],[Day].[FiscalWeekStartDate].MEMBERS) on rows
		FROM [Bonza]
		WHERE 
		{([Day].[FiscalMonthText].&[Mar-2015],[Company].[CompanyName].&[William Hill Australia])
		, ([Day].[FiscalMonthText].&[Apr-2015],[Company].[CompanyName].&[William Hill Australia])}
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'KPI'
							and [TestName] = 'Medium'
				)
IF @Time <= 60000
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '94.1', 'QueryPerformance_KPI_Medium', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '94.1', 'QueryPerformance_KPI_Medium', 'Fail', 'Time taken is ' + cast (@Time as varchar(100)) + ' msec and should be less than 60000')
End

--Complex (2 stars, 2 months)
exec [Test].[spExecuteMDX] @TestRunNo, 'KPI', 'Complex', 1200000,
	' Select * From OpenQuery([ANALYSISSERVICES],''
		WITH MEMBER [Measures].[NetValue] AS [Measures].[Turnover] - [Measures].[GrossWin]
		SELECT 
		NON EMPTY
		{[Measures].[TurnoverPlaced],[Measures].[GrossWin],[Measures].[NetRevenue],[Measures].[NetValue],[Measures].[TransactedAmount]} on Columns,
		CROSSJOIN([Company].[BusinessUnitName].[BusinessUnitName],[Day].[FiscalWeekStartDate].MEMBERS) on rows
		FROM [Bonza]
		WHERE 
		{([Day].[FiscalMonthText].&[Mar-2015],[Company].[CompanyName].&[William Hill Australia])
		, ([Day].[FiscalMonthText].&[Apr-2015],[Company].[CompanyName].&[William Hill Australia])}
	'')'


Set @Time = 	(
				Select		MSeconds
				From		[Test].[tblQueryPerformance]
				where		[RunID] = @TestRunNo
							and [TestID] = 'KPI'
							and [TestName] = 'Complex'
				)
IF @Time <= 1200000
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '94.2', 'QueryPerformance_KPI_Complex', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults ([RunID], [TestID], [TestName], [Status], [Notes]) Values (@TestRunNo, '94.2', 'QueryPerformance_KPI_Complex', 'Fail', 'Time taken is ' + cast (@Time as varchar(100)) + ' msec and should be less than 1200000')
End

GO


