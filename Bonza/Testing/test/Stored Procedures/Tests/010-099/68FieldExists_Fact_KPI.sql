--Test Code
--exec Test.[68FieldExists_Fact_KPI] 'ADHOC'
Create Procedure Test.[68FieldExists_Fact_KPI] @TestRunNo varchar(23)

as

Declare @Database varchar(100)
Set @Database = '[$(Dimensional)]'

Declare @SQL varchar(max)
Set @SQL = 'Select top 1 
[ContractKey]
      ,[DayKey]
      ,[AccountKey]
      ,[AccountStatusKey]
      ,[BetTypeKey]
      ,[LegTypeKey]
      ,[ChannelKey]
      ,[CampaignKey]
      ,[MarketKey]
      ,[EventKey]
      ,[ClassKey]
      ,[UserKey]
      ,[InstrumentKey]
      ,[InPlayKey]
      ,[AccountOpened]
      ,[FirstDeposit]
      ,[FirstBet]
      ,[DepositsRequested]
      ,[DepositsCompleted]
      ,[DepositsRequestedCount]
      ,[DepositsCompletedCount]
      ,[Promotions]
      ,[Transfers]
      ,[BetsPlaced]
      ,[BonusBetsPlaced]
      ,[ContractsPlaced]
      ,[BonusContractsPlaced]
      ,[BettorsPlaced]
      ,[BonusBettorsPlaced]
      ,[PlayDaysPlaced]
      ,[BonusPlayDaysPlaced]
      ,[PlayFiscalWeeksPlaced]
      ,[BonusPlayFiscalWeeksPlaced]
      ,[PlayCalenderWeeksPlaced]
      ,[BonusPlayCalenderWeeksPlaced]
      ,[PlayFiscalMonthsPlaced]
      ,[BonusPlayFiscalMonthsPlaced]
      ,[PlayCalenderMonthsPlaced]
      ,[BonusPlayCalenderMonthsPlaced]
      ,[Stakes]
      ,[BonusStakes]
      ,[Winnings]
      ,[WithdrawalsRequested]
      ,[WithdrawalsCompleted]
      ,[WithdrawalsRequestedCount]
      ,[WithdrawalsCompletedCount]
      ,[Adjustments]
      ,[BetsResulted]
      ,[BonusBetsResulted]
      ,[ContractsResulted]
      ,[BonusContractsResulted]
      ,[BettorsResulted]
      ,[BonusBettorsResulted]
      ,[PlayDaysResulted]
      ,[BonusPlayDaysResulted]
      ,[PlayFiscalWeeksResulted]
      ,[BonusPlayFiscalWeeksResulted]
      ,[PlayCalenderWeeksResulted]
      ,[BonusPlayCalenderWeeksResulted]
      ,[PlayFiscalMonthsResulted]
      ,[BonusPlayFiscalMonthsResulted]
      ,[PlayCalenderMonthsResulted]
      ,[BonusPlayCalenderMonthsResulted]
      ,[Turnover]
      ,[BonusTurnover]
      ,[GrossWin]
      ,[NetRevenue]
      ,[NetRevenueAdjustment]
      ,[BonusWinnings]
      ,[Betback]
      ,[GrossWinAdjustment]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
      ,[StructureKey]
  FROM ' + @Database + '.[Fact].[KPI]'

Exec Test.spSelectValid @TestRunNo, '68','Structure - Fact KPI Table', @SQL

If		(
		Select		Count(*)
		From		[$(Dimensional)].sys.all_objects as [Objects]
					inner join [$(Dimensional)].sys.all_columns as [Columns] on [Objects].[object_id] = [Columns].[object_id]
					inner join [$(Dimensional)].sys.schemas as [schemas] on [Objects].[schema_id] = [schemas].[schema_id]
		where		[Objects].name = 'KPI'
					and [Objects].[type] = 'U'
					and [schemas].Name = 'Fact'
		) = 78
Begin
	Insert into test.tblResults Values (@TestRunNo, '68.1', 'Structure - Fact KPI Table', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '68.1', 'Structure - Fact KPI Table', 'Fail', 'Wrong number of columns')
End