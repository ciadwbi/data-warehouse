﻿--Test Code
--exec Test.[28DataDefault_Dimension_Note] 'ADHOC'
Create Procedure Test.[28DataDefault_Dimension_Note] @TestRunNo varchar(23)

as


If exists(
		Select		NoteKey
		From		[$(Dimensional)].[Dimension].[Note]
		where		NoteKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '28', 'DataDefault_Dimension_Note Table ID0', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5057'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '28', 'DataDefault_Dimension_Note Table ID0', 'Pass', 'CCIA-5057')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '28', 'DataDefault_Dimension_Note Table ID0', 'Fail', 'No ID 0 record found')
	End
End

If exists(
		Select		NoteKey
		From		[$(Dimensional)].[Dimension].[Note]
		where		NoteKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '28.1', 'DataDefault_Dimension_Note Table ID-1', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5057'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '28.1', 'DataDefault_Dimension_Note Table ID-1', 'Pass', 'CCIA-5057')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '28.1', 'DataDefault_Dimension_Note Table ID-1', 'Fail', 'No ID -1 record found')
	End
End
