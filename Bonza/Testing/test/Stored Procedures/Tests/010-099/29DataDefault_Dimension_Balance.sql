﻿--Test Code
--exec Test.[29DataDefault_Dimension_Balance] 'ADHOC'
Create Procedure Test.[29DataDefault_Dimension_Balance] @TestRunNo varchar(23)

as

If exists(
		Select		BalanceTypeKey
		From		[$(Dimensional)].Dimension.[BalanceType]
		where		BalanceTypeKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '29', 'DataDefault_Dimension_BalanceType Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '29', 'DataDefault_Dimension_BalanceType Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		BalanceTypeKey
		From		[$(Dimensional)].Dimension.[BalanceType]
		where		BalanceTypeKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '29.1', 'DataDefault_Dimension_BalanceType Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '29.1', 'DataDefault_Dimension_BalanceType Table ID-1', 'Fail', 'No ID -1 record found')
End


