﻿--Test Code
--exec Test.[38DataCount_Dimension_Day] 'ADHOC'
Create Procedure Test.[38DataCount_Dimension_Day] @TestRunNo varchar(23)

as

If not exists(
		Select		*
		From		(		
					Select		count(*) as CountAll
					From		[$(Dimensional)].[Dimension].[Day]
					where		DayKey not in (0, -1)
					) as Counters
					Cross Join	(
								Select		Max (DayDate) as MaxDate
											, min (DayDate) as MinDate
											, (DateDiff (Day,min (DayDate), Max (DayDate)) + 1) as Correction
								From		[$(Dimensional)].[Dimension].[Day]
								where		DayKey not in (0, -1)
											and DayDate != '9999-12-31'
								) as Data
		Where		CountAll != Correction
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '38', 'DataCount_Dimension_Day', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '38', 'DataCount_Dimension_Day', 'Fail', 'The number of records is incorrect')
End

Declare @MinDayzone date
Declare @MaxDayzone date
Declare @Counter int

Set @MinDayzone =	(Select		Min (MasterDayText) as MinDate
					 From		[$(Dimensional)].[Dimension].[DayZone]
					 where		DayKey not in (0, -1)
								and Right (DayKey,2) = 00
					)

Set @MaxDayzone =	DateAdd( Year, 2, getdate())

Set @Counter =	(
				Select		Count(*)
				From		[$(Dimensional)].[Dimension].[DayZone]
				)

IF @Counter = (((DATEDIFF (Day, @MinDayzone, @MaxDayzone))+1) * 4) + 2
	Begin
		Insert into test.tblResults Values (@TestRunNo, '38.2', 'DataCount_Dimension_DayZone', 'Pass', Null)
	End
	Else
	Begin
		--CCIA-5037 creating an additional day of data
		If exists		(Select		*
						 From		test.tblDefects
						 Where		DefectID = 'CCIA-5037'
									and [Status] != 'Done'
				) and @Counter - 4 = (((DATEDIFF (Day, @MinDayzone, @MaxDayzone))+1) * 4) + 2
		Begin
			Insert into test.tblResults Values (@TestRunNo, '38.2', 'DataCount_Dimension_DayZone', 'Pass', 'CCIA-5037')
		End
		Else
		Begin
			Insert into test.tblResults Values (@TestRunNo, '38.2', 'DataCount_Dimension_DayZone', 'Fail', 'The number of records is incorrect')
		End
	End



