﻿--Test Code
--exec Test.[44DataCurrent_Dimension_Time] 'ADHOC'
Create Procedure Test.[44DataCurrent_Dimension_Time] @TestRunNo varchar(23)

as

Select	[TimeKey]
      ,[TimeID]
      ,[TimeText]
      ,[Time]
      ,[TimeStartTime]
      ,[TimeEndTime]
      ,[TimeStartTimestamp]
      ,[TimeEndTimestamp]
      ,[TimeOfHourNumber]
      ,[TimeOfDayNumber]
      ,[HourKey]
      ,[HourID]
      ,[HourText]
      ,[HourStartTime]
      ,[HourEndTime]
      ,[HourOfDayNumber]
      ,[PeriodKey]
      ,[PeriodText]
      ,[FromDate]
      ,[ToDate]
      ,[FirstDate]
      ,[CreatedDate]
      ,[CreatedBatchKey]
      ,[CreatedBy]
      ,[ModifiedDate]
      ,[ModifiedBatchKey]
      ,[ModifiedBy]
into #DimensionTime
From [$(Dimensional)].[Dimension].[Time]

Declare @NumberOfRecords int
Set @NumberOfRecords = 
					(
					Select		count(*) as NumberOfRecords
					From		#DimensionTime
					)
If @NumberOfRecords = 1442
Begin
	Insert into test.tblResults Values (@TestRunNo, '44', 'DataCurrent_Dimension_Time Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '44', 'DataCurrent_Dimension_Time Record Count', 'Fail', 'Expected 1442 records, ' + Cast (@NumberOfRecords as varchar(5)) + ' found.')
End

IF 					(
					Select		Min (TimeKey) as Record
					From		#DimensionTime
					where		TimeKey not in (0 ,-1)
					)
					= 10000
Begin
	Insert into test.tblResults Values (@TestRunNo, '44.1', 'DataCurrent_Dimension_Time Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '44.1', 'DataCurrent_Dimension_Time Record Count', 'Fail', 'The lowest record key is not 10000')
End

IF 					(
					Select		Max (TimeKey) as Record
					From		#DimensionTime
					where		TimeKey not in (0 ,-1)
					)
					= 12359
Begin
	Insert into test.tblResults Values (@TestRunNo, '44.2', 'DataCurrent_Dimension_Time Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '44.2', 'DataCurrent_Dimension_Time Record Count', 'Fail', 'The highest record key is not 12359')
End

