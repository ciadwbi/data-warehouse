﻿--Test Code
--exec Test.[49DataDuplicates_Dimension_User] 'ADHOC'

Create Procedure Test.[49DataDuplicates_Dimension_User] @TestRunNo varchar(23)

as

If not exists(
		Select		User1.UserKey
					, User1.EmployeeKey
					, User1.FromDate
					, User1.ToDate
					, User2.UserKey
					, User2.EmployeeKey
					, User2.FromDate
					, User2.ToDate
		From		[$(Dimensional)].Dimension.[User] as User1
		inner join	[$(Dimensional)].Dimension.[User] as User2 on User1.UserID = User2.UserID
																and User1.SystemName = User2.SystemName
		Where		User1.UserKey != User2.UserKey
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '49', 'DataDuplicates_Dimension_User', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-4477'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '49', 'DataDuplicates_Dimension_User', 'Pass', 'CCIA-4477')
	End
	Else
	Begin	
		Insert into test.tblResults Values (@TestRunNo, '49', 'DataDuplicates_Dimension_User', 'Fail', 'Duplicates of EmployeeKey found')
	End
End

--CCIA-4477
If not exists(
		Select		User1.UserKey
					, User1.PayrollNumber
					, User1.FromDate
					, User1.ToDate
					, User2.UserKey
					, User2.PayrollNumber
					, User2.FromDate
					, User2.ToDate
		From		[$(Dimensional)].Dimension.[User] as User1
		inner join	[$(Dimensional)].Dimension.[User] as User2 on User1.PayrollNumber = User2.PayrollNumber
		Where		User1.UserKey != User2.UserKey
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '49.1', 'DataDuplicates_Dimension_User', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-4477'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '49.1', 'DataDuplicates_Dimension_User', 'Pass', 'CCIA-4477')
	End
	Else
	Begin	
		Insert into test.tblResults Values (@TestRunNo, '49.1', 'DataDuplicates_Dimension_User', 'Fail', 'Duplicates of PayrollNumber found')
	End
End

