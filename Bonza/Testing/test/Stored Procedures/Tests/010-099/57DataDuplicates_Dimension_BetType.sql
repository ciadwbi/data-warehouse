﻿--Test Code
--exec Test.[57DataDuplicates_Dimension_BetType] 'ADHOC'

Create Procedure Test.[57DataDuplicates_Dimension_BetType] @TestRunNo varchar(23)

as


If not exists(
		Select		BetType1.BetTypeKey
					, BetType1.BetGroupType
					, BetType1.BetType
					, BetType1.BetSubType
					, BetType1.GroupingType
					, BetType1.LegType
					, BetType2.BetTypeKey
					, BetType2.BetGroupType
					, BetType2.BetType
					, BetType2.BetSubType
					, BetType2.GroupingType
					, BetType2.LegType
		From		[$(Dimensional)].Dimension.BetType as BetType1
		inner join	[$(Dimensional)].Dimension.BetType as BetType2 on BetType1.BetType = BetType2.BetType
													and BetType1.BetGroupType = BetType2.BetGroupType
													and BetType1.BetSubType = BetType2.BetSubType
													and BetType1.GroupingType = BetType2.GroupingType
													and BetType1.LegType = BetType2.LegType
		Where		BetType1.BetTypeKey != BetType2.BetTypeKey
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '57', 'DataDuplicates_Dimension_BetType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '57', 'DataDuplicates_Dimension_BetType', 'Fail', 'Duplicates of BetTypeID found')
End

