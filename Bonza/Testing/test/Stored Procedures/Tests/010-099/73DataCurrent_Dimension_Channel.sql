﻿--Test Code
--exec Test.[73DataCurrent_Dimension_Channel] 'ADHOC'
Create Procedure Test.[73DataCurrent_Dimension_Channel] @TestRunNo varchar(23)

as

Select	*
into #DimensionChannel
From [$(Dimensional)].[Dimension].[Channel]

Declare @NumberOfRecords int
Set @NumberOfRecords = 
					(
					Select		count(*) as NumberOfRecords
					From		#DimensionChannel
					)
If @NumberOfRecords = 64
Begin
	Insert into test.tblResults Values (@TestRunNo, '73', 'DataCurrent_Dimension_Channel Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '73', 'DataCurrent_Dimension_Channel Record Count', 'Fail', 'Expected 64 records, ' + Cast (@NumberOfRecords as varchar(5)) + ' found.')
End

IF 					(
					Select		Min (ChannelKey) as Record
					From		#DimensionChannel
					where		ChannelKey not in (0 ,-1)
					)
					= 1
Begin
	Insert into test.tblResults Values (@TestRunNo, '73.1', 'DataCurrent_Dimension_Channel Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '73.1', 'DataCurrent_Dimension_Channel Record Count', 'Fail', 'The lowest record key is not 1')
End

IF 					(
					Select		Max (ChannelKey) as Record
					From		#DimensionChannel
					where		ChannelKey not in (0 ,-1)
					)
					= 9999
Begin
	Insert into test.tblResults Values (@TestRunNo, '73.2', 'DataCurrent_Dimension_Channel Record Count', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '73.2', 'DataCurrent_Dimension_Channel Record Count', 'Fail', 'The highest record key is not 9999')
End

