﻿--Test Code
--exec Test.[95DataCount_Dimension_Balance] 'ADHOC'
Create Procedure Test.[95DataCount_Dimension_Balance] @TestRunNo varchar(23)

as


If		(
		Select		count (*)
		From		[$(Dimensional)].Dimension.BalanceType
		) = 10
Begin
	Insert into Test.tblResults Values (@TestRunNo, '95', 'DataCount_Dimension_BalanceType', 'Pass', Null)
End
Else
Begin
	Insert into Test.tblResults Values (@TestRunNo, '95', 'DataCount_Dimension_BalanceType', 'Fail', 'The number of records is incorrect')
End


