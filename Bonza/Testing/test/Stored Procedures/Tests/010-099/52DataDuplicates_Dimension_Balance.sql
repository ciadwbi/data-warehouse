﻿--Test Code
--exec Test.[52DataDuplicates_Dimension_Balance] 'ADHOC'

Create Procedure Test.[52DataDuplicates_Dimension_Balance] @TestRunNo varchar(23)

as


If not exists (
				Select		Count(*)
				From		[$(Dimensional)].Dimension.BalanceType as BalanceType
				Group by	BalanceTypeName
				Having		Count(*) > 1
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '52', 'DataDuplicates_Dimension_BalanceType', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '52', 'DataDuplicates_Dimension_BalanceType', 'Fail', 'Duplicates of BalanceID found')
End

