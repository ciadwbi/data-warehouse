﻿--Test Code
--exec Test.[42DataFK_Star_Transaction] 'Adhoc', '2016-02-02'

Create Procedure Test.[42DataFK_Star_Transaction] @TestRunNo varchar(23), @FromDate datetime2

as

Select		[Transaction].*
into		#Transaction
From		[$(Dimensional)].Fact.[Transaction] as [Transaction] with (nolock)
			inner join [$(Dimensional)].Dimension.DayZone as DayZone on [Transaction].DayKey = DayZone.DayKey
			inner join [$(Dimensional)].Dimension.[Day] as [Day] on DayZone.MasterDayKey = [Day].DayKey
where		[Day].DayDate = Cast (@FromDate as Date)
			or @FromDate is null

--TransactionId n/a
--TransactionIdSource n/a

--TransactionDetailKey
If not exists(
			Select		distinct [Transaction].LegTypeKey
			From		#Transaction as [Transaction]
						left join	[$(Dimensional)].Dimension.LegType as LegType with (nolock) on [Transaction].LegTypeKey = LegType.LegTypeKey
			Where		LegType.LegTypeKey is null
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.11', '42DataFK_Star_Transaction TransactionDetailKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.11', '42DataFK_Star_Transaction TransactionDetailKey', 'Fail', 'Missing TransactionDetailKey')
End

--BalanceTypeKey
If not exists(
			Select		distinct [Transaction].BalanceTypeKey
			From		#Transaction as [Transaction]
						left join	[$(Dimensional)].Dimension.BalanceType as Balance with (nolock) on [Transaction].BalanceTypeKey = Balance.BalanceTypeKey
			Where		Balance.BalanceTypeKey is null				
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.10', '42DataFK_Star_Transaction BalanceTypeKeys', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.10', '42DataFK_Star_Transaction BalanceTypeKeys', 'Fail', 'Missing BalanceKeys')
End

--ContractKey
If not exists(
			Select		distinct [Transaction].ContractKey
			From		#Transaction as [Transaction]
						left join	[$(Dimensional)].Dimension.[Contract] as [Contract] with (nolock) on [Transaction].ContractKey = [Contract].ContractKey
			Where		[Contract].ContractKey is null				
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.2', '42DataFK_Star_Transaction Contract', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.2', '42DataFK_Star_Transaction Contract', 'Fail', 'Missing ContractKeys')
End

--AccountKey
--AccountStatusKey
--WalletKey
If not exists(
		Select		distinct [Transaction].WalletKey
		From		#Transaction as [Transaction]
					left join	[$(Dimensional)].Dimension.Wallet as Wallet with (nolock) on [Transaction].WalletKey = Wallet.WalletKey
		Where		Wallet.WalletKey is null			
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42', '42DataFK_Star_Transaction Wallet', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42', '42DataFK_Star_Transaction Wallet', 'Fail', 'Missing WalletKeys')
End

--PromotionKey
If not exists(
		Select		distinct [Transaction].PromotionKey
		From		#Transaction as [Transaction]
					left join	[$(Dimensional)].Dimension.Promotion as Promotion with (nolock) on [Transaction].PromotionKey = Promotion.PromotionKey
		Where		Promotion.PromotionKey is null				
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.1', '42DataFK_Star_Transaction Promotion', 'Pass', Null)
End
Else
Begin
	--CCIA-3254 no table
	IF	exists	(	Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-3254'
								and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '42.1', '42DataFK_Star_Transaction Promotion', 'Pass', 'CCIA-3254')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '42.1', '42DataFK_Star_Transaction Promotion', 'Fail', 'Test needed for Promotion, CCIA-3254 has been fixed')
	End
End

--MarketKey
--EventKey
--DayKey
If not exists(
			Select		distinct [Transaction].DayKey
			From		#Transaction as [Transaction]
						left join	[$(Dimensional)].Dimension.[DayZone] as [DayZone] with (nolock) on [Transaction].DayKey = [DayZone].DayKey
						left join	[$(Dimensional)].Dimension.[Day] as [Day] with (nolock) on [DayZone].MasterDayKey = [Day].DayKey
			Where		[Day].DayKey is null	
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.3', '42DataFK_Star_Transaction Day', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.3', '42DataFK_Star_Transaction Day', 'Fail', 'Missing DayKeys')
End

--MasterTransactionTimestamp n/a
--MasterTimeKey
If not exists(
			Select		distinct [Transaction].MasterTimeKey
			From		#Transaction as [Transaction]
						left join	[$(Dimensional)].Dimension.[Time] as [Time] with (nolock) on [Transaction].MasterTimeKey = [Time].TimeKey
			Where		[Time].TimeKey is null					
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.4', '42DataFK_Star_Transaction Time (MasterTimeKey)', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.4', '42DataFK_Star_Transaction Time (MasterTimeKey)', 'Fail', 'Missing TimeKey (MasterTimeKey)')
End

--AnalysisTransactionTimestamp n/a
--AnalysisTimeKey
If not exists(
			Select		distinct [Transaction].AnalysisTimeKey
			From		#Transaction as [Transaction]
						left join	[$(Dimensional)].Dimension.[Time] as [Time] with (nolock) on [Transaction].AnalysisTimeKey = [Time].TimeKey
			Where		[Time].TimeKey is null					
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.5', '42DataFK_Star_Transaction Time (AnalysisTimeKey)', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.5', '42DataFK_Star_Transaction Time (AnalysisTimeKey)', 'Fail', 'Missing TimeKey (AnalysisTimeKey)')
End

--CampaignKey
--ChannelKey
If not exists(
			Select		distinct [Transaction].ChannelKey
			From		#Transaction as [Transaction]
						left join	[$(Dimensional)].Dimension.Channel as Channel with (nolock) on [Transaction].ChannelKey = Channel.ChannelKey
			Where		Channel.ChannelKey is null					
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.6', '42DataFK_Star_Transaction ChannelKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.6', '42DataFK_Star_Transaction ChannelKey', 'Fail', 'Missing ChannelKey')
End

--ActionChannelKey
If not exists(
			Select		distinct [Transaction].ActionChannelKey
			From		#Transaction as [Transaction]
						left join	[$(Dimensional)].Dimension.Channel as Channel with (nolock) on [Transaction].ActionChannelKey = Channel.ChannelKey
			Where		Channel.ChannelKey is null					
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.13', '42DataFK_Star_Transaction ActionChannelKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.13', '42DataFK_Star_Transaction ActionChannelKey', 'Fail', 'Missing ActionChannelKey')
End

--LedgerID n/a
--TransactedAmount n/a
--PriceTypeKey
--BetTypeKey
--LegTypeKey
If not exists(
			Select		distinct [Transaction].TransactionDetailKey
			From		#Transaction as [Transaction]
						left join	[$(Dimensional)].Dimension.TransactionDetail as TransactionDetail with (nolock) on [Transaction].TransactionDetailKey = TransactionDetail.TransactionDetailKey
			Where		TransactionDetail.TransactionDetailKey is null
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.11', '42DataFK_Star_Transaction LegTypeKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.11', '42DataFK_Star_Transaction LegTypeKey', 'Fail', 'Missing LegTypeKey')
End

--ClassKey

--UserKey
If not exists(
			Select		distinct [Transaction].UserKey
			From		#Transaction as [Transaction]
						left join	[$(Dimensional)].Dimension.[User] as [User] with (nolock) on [Transaction].UserKey = [User].UserKey
			Where		[User].UserKey is null					
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.7', '42DataFK_Star_Transaction UserKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.7', '42DataFK_Star_Transaction UserKey', 'Fail', 'Missing UserKey')
End

--InstrumentKey
If not exists	(
				Select		distinct [Transaction].InstrumentKey
				From		#Transaction as [Transaction]
							left join	[$(Dimensional)].Dimension.Instrument as Instrument with (nolock) on [Transaction].InstrumentKey = Instrument.InstrumentKey
				Where		Instrument.InstrumentKey is null
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.8', '42DataFK_Star_Transaction InstrumentKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.8', '42DataFK_Star_Transaction InstrumentKey', 'Fail', 'Missing InstrumentKey field')
End

--NoteKey
If not exists(
				Select		distinct Note.NoteKey
				From		#Transaction as [Transaction]
							left join [$(Dimensional)].Dimension.Note as Note with (nolock) on Note.NoteId = [Transaction].TransactionId -- Currently hacked to use NoteId to TransactionId to bypass corrupted NoteKey (same logic reflected in Warehouse view)
				Where		Note.NoteKey is null
							and [Transaction].NoteKey > 0					
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.9', '42DataFK_Star_Transaction NoteKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.9', '42DataFK_Star_Transaction NoteKey', 'Fail', 'Missing NoteKey field')
End

--InPlayKey
If not exists	(
				Select		distinct [Transaction].InPlayKey
				From		#Transaction as [Transaction]
							left join	[$(Dimensional)].Dimension.InPlay as InPlay with (nolock) on [Transaction].InPlayKey = InPlay.InPlayKey
				Where		InPlay.InPlayKey is null
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.10', '42DataFK_Star_Transaction StructureKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.10', '42DataFK_Star_Transaction StructureKey', 'Fail', 'Missing StructureKey field')
End

--StructureKey
If not exists	(
				Select		distinct [Transaction].StructureKey
				From		#Transaction as [Transaction]
							left join	[$(Dimensional)].Dimension.Structure as Structure with (nolock) on [Transaction].StructureKey = Structure.StructureKey
				Where		Structure.StructureKey is null
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.12', '42DataFK_Star_Transaction StructureKey', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '42.12', '42DataFK_Star_Transaction StructureKey', 'Fail', 'Missing StructureKey field')
End

--FromDate n/a
--CreatedDate n/a
--CreatedBatchKey n/a
--CreatedBy n/a
