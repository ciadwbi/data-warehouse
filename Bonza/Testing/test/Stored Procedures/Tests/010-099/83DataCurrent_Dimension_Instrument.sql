﻿--Test Code
--exec Test.[83DataCurrent_Dimension_Instrument] 'ADHOC'
Create Procedure Test.[83DataCurrent_Dimension_Instrument] @TestRunNo varchar(23)

as

Declare	@BatchDate datetime2
Set	@BatchDate = (Select Max(BatchFromDate) From [$(Staging)].[Control].ETLController Where DimensionStatus = 1)

Declare @LastDate varchar(10)
Set		@LastDate = 
					(Select		max (CreatedDate)
					From		[$(Dimensional)].[Dimension].[Instrument]
					)

If @LastDate >= @BatchDate or @LastDate >= DateAdd (Day,-1,getdate())
Begin
	Insert into test.tblResults Values (@TestRunNo, '83', 'DataCurrent_Dimension_Instrument Table', 'Pass', Null)
End
Else
Begin
	--CCIA-3314 only has default records
	IF		(Select		count(*)
			 From		[$(Dimensional)].[Dimension].[Instrument]
			 where		InstrumentKey not in (-1,0)
			) = 0
			and	exists	(	Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-3314'
								and Status != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '83', 'DataCurrent_Dimension_Instrument Table', 'Pass', 'Defect CCIA-3314')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '83', 'DataCurrent_Dimension_Instrument Table', 'Fail', 'The last update in the dimension is ' + cast (@LastDate as varchar(10)) + ' expect an update each day')
	End
End

