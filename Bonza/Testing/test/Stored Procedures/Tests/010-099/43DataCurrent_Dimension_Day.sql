﻿--Test Code
--exec Test.[43DataCurrent_Dimension_Day] 'ADHOC'
Create Procedure Test.[43DataCurrent_Dimension_Day] @TestRunNo varchar(23)

as

Declare @LastDate varchar(10)
Declare @LastDateKey varchar(10)

Set		@LastDate = 
					(Select		Top 1
								DayDate
					From		[$(Dimensional)].[Dimension].[Day]
					Where		DayDate != '9999-12-31'
					Order by	DayDate desc
					)

--This table now has future dates, 2 years in advance
If @LastDate = DateAdd (Year, 2, cast (getdate() as date))
Begin
	Insert into test.tblResults Values (@TestRunNo, '43', 'DataCurrent_Dimension_Day Table', 'Pass', Null)
End
Else
Begin
	
	Insert into test.tblResults Values (@TestRunNo, '43', 'DataCurrent_Dimension_Day Table', 'Fail', 'The last day in the dimension is ' + cast (@LastDate as varchar(10)))
End

Set		@LastDateKey = 
					(Select		Top 1
								DayKey
					From		[$(Dimensional)].[Dimension].[DayZone]
					Where		Right (DayKey, 1) = 0
					Order by	DayKey desc
					)

If @LastDateKey = Convert(varchar(8),DateAdd (year, 2, getdate()), 112) + '00'
Begin
	Insert into test.tblResults Values (@TestRunNo, '43.1', 'DataCurrent_Dimension_DayZone Table ID0', 'Pass', Null)
End
Else
Begin
	
	Insert into test.tblResults Values (@TestRunNo, '43.1', 'DataCurrent_Dimension_Day TableZone ID0', 'Fail', 'The last day in the dimension is ' + @LastDateKey)
End

Set		@LastDateKey = 
					(Select		Top 1
								DayKey
					From		[$(Dimensional)].[Dimension].[DayZone]
					Where		Right (DayKey, 1) = 1
					Order by	DayKey desc
					)

If @LastDateKey = Convert(varchar(8),DateAdd (year, 2, getdate()), 112) + '01'
Begin
	Insert into test.tblResults Values (@TestRunNo, '43.2', 'DataCurrent_Dimension_DayZone Table ID1', 'Pass', Null)
End
Else
Begin
	
	Insert into test.tblResults Values (@TestRunNo, '43.2', 'DataCurrent_Dimension_Day TableZone ID1', 'Fail', 'The last day in the dimension is ' + @LastDateKey)
End

Set		@LastDateKey = 
					(Select		Top 1
								DayKey
					From		[$(Dimensional)].[Dimension].[DayZone]
					Where		Right (DayKey, 1) = 2
					Order by	DayKey desc
					)

If @LastDateKey = Convert(varchar(8),DateAdd (year, 2, getdate()), 112) + '02'
Begin
	Insert into test.tblResults Values (@TestRunNo, '43.3', 'DataCurrent_Dimension_DayZone Table ID2', 'Pass', Null)
End
Else
Begin
	
	Insert into test.tblResults Values (@TestRunNo, '43.3', 'DataCurrent_Dimension_Day TableZone ID2', 'Fail', 'The last day in the dimension is ' + @LastDateKey)
End

Set		@LastDateKey = 
					(Select		Top 1
								DayKey
					From		[$(Dimensional)].[Dimension].[DayZone]
					Where		Right (DayKey, 1) = 3
					Order by	DayKey desc
					)

If @LastDateKey = Convert(varchar(8),DateAdd (year, 2, getdate()), 112) + '03'
Begin
	Insert into test.tblResults Values (@TestRunNo, '43.4', 'DataCurrent_Dimension_DayZone Table ID3', 'Pass', Null)
End
Else
Begin
	
	Insert into test.tblResults Values (@TestRunNo, '43.4', 'DataCurrent_Dimension_Day TableZone ID3', 'Fail', 'The last day in the dimension is ' + @LastDateKey)
End