﻿--Test Code
--exec Test.[25DataDefault_Dimension_Channel] 'ADHOC'
Create Procedure Test.[25DataDefault_Dimension_Channel] @TestRunNo varchar(23)

as

If exists(
		Select		ChannelKey
		From		[$(Dimensional)].[Dimension].[Channel]
		where		ChannelKey = 0
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '25', 'DataDefault_Dimension_Channel Table ID0', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '25', 'DataDefault_Dimension_Channel Table ID0', 'Fail', 'No ID 0 record found')
End

If exists(
		Select		ChannelKey
		From		[$(Dimensional)].[Dimension].[Channel]
		where		ChannelKey = -1
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '25.1', 'DataDefault_Dimension_Channel Table ID-1', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '25.1', 'DataDefault_Dimension_Channel Table ID-1', 'Fail', 'No ID -1 record found')
End


