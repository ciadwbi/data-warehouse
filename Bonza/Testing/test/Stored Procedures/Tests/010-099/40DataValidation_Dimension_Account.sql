﻿--Test Code
--exec Test.[40DataValidation_Dimension_Account] 'ADHOC', null

Create Procedure Test.[40DataValidation_Dimension_Account] @TestRunNo varchar(23), @FromDate datetime

as



Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End


If not exists(
				Select		AccountKey
				From		[$(Dimensional)].Dimension.Account
				where		AccountId = 0
							and AccountKey not in (0,-1)
							and ((FromDate > @FromDate)
									or (@FromDate is null))
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '40', 'DataValidation_Dimension_Account', 'Pass', Null)
End
Else
Begin
	--Defect CCIA-3299 There are 3146 records with an AccountId of 0
	IF		(	Select		Count (*)
				From		[$(Dimensional)].Dimension.Account
				where		AccountId = 0
							and ((FromDate > @FromDate)
									or (@FromDate is null))
			) = 3146
	and	exists	(	Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-3299'
								and Status != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40', 'DataValidation_Dimension_Account', 'Pass', 'Defect CCIA-3299 has already been raised')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40', 'DataValidation_Dimension_Account', 'Fail', 'There are records with an accountid of 0' + @ErrorText)
	End
End


If not exists	(
				select		Account1.AccountId
							, Account1.AccountSource
							, Account1.FromDate
							, Account1.ToDate
				from		[$(Dimensional)].Dimension.Account as Account1
							left join [$(Dimensional)].Dimension.Account as Account2 on Account1.AccountSource = Account2.AccountSource
																	and Account1.ToDate = Account2.FromDate
				where		Account2.AccountSource is null
							and Account1.ToDate != '9999-12-31'
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.1', 'DataValidation_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.1', 'DataValidation_Dimension_Account', 'Fail', 'There are accounts which do not have a continuous history to 9999')
End

--Type 1 dimensions update the fromdate each time there is a change, the firstdate remains the same
If not exists	(
				select		AccountSource
							, AccountID
							, Convert(date,AccountOpenedDate) as OpenedDate
							, Convert(date,min (FromDate)) as FromDateTime
				from		[$(Dimensional)].Dimension.Account as Account1
				Group by	AccountSource, AccountID
							, Convert(date,AccountOpenedDate)
				Having		 Convert(date,min (FirstDate)) != Convert(date,AccountOpenedDate)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.2', 'DataValidation_Dimension_Account', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5362'
							and [Status] != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40.2', 'DataValidation_Dimension_Account', 'Pass', 'CCIA-5362')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40.2', 'DataValidation_Dimension_Account', 'Fail', 'There are accounts which do not have a history from start date')
	End 
End

