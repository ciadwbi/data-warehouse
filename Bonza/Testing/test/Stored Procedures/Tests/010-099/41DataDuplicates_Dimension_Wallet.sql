﻿--Test Code
--exec Test.[41DataDuplicates_Dimension_Wallet] 'ADHOC', null
--exec Test.[41DataDuplicates_Dimension_Wallet] 'ADHOC', '2014-07-11'

Create Procedure Test.[41DataDuplicates_Dimension_Wallet] @TestRunNo varchar(23), @FromDate datetime

as

--41) Defect CCIA-3300 WalletID is duplicated in the table

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

If not exists (
				Select		Count(*)
				From		[$(Dimensional)].Dimension.Wallet as Wallet
				where		@FromDate is null
							or CreatedDate >= @FromDate
				Group by	WalletId
				Having		Count(*) > 1
			)
Begin
	Insert into test.tblResults Values (@TestRunNo, '41', 'DataDuplicates_Dimension_Wallet', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '41', 'DataDuplicates_Dimension_Wallet', 'Fail', 'Duplicates of WalletID found' + @ErrorText)
End

