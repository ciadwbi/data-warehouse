﻿--Test Code
--exec Test.[40BVA_Dimension_Account] 'ADHOC', '2015-04-03'

Create Procedure Test.[40BVA_Dimension_Account] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

Select		* 
into		#Account
From		[$(Dimensional)].Dimension.Account
where		AccountKey not in (0,-1)
			and accountid != -1 --CCIA-5942
			and ((FromDate > @FromDate)
					or (@FromDate is null))
					 
--Keys are Metadata are not tested
--[AccountID]
If not exists(
				Select		AccountKey
				From		#Account
				where		AccountId in (0, -1)
			) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '40', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	IF exists	(	Select		*
					From		test.tblDefects
					Where		DefectID = 'CCIA-5942'
								and Status != 'Done'
				)
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40', 'BVA_Dimension_Account', 'Pass', 'CCIA-5942')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40', 'BVA_Dimension_Account', 'Fail', 'There are records with an accountid of 0 or -1' + @ErrorText)
	End
End

--[AccountSource]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.AccountSource is null or #Account.AccountSource = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.1', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.1', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid AccountSource' + @ErrorText)
End

--[PIN]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.PIN < 1
							--looks like some older accounts do not have a PIN
							and accountopeneddate >= '2005-01-01'
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.2', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5943'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40.2', 'BVA_Dimension_Account', 'Pass', 'CCIA-5943')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40.2', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid PIN' + @ErrorText)
	End
End

----[CustomerKey] --Removed in v3_AccountDimension Branch
--If not exists	(
--				select		#Account.AccountId
--							, #Account.AccountSource
--							, #Account.FromDate
--							, #Account.ToDate
--				from		#Account
--				where		#Account.CustomerKey < 1
--				)

--Begin
--	Insert into test.tblResults Values (@TestRunNo, '40.3', 'BVA_Dimension_Account', 'Pass', Null)
--End
--Else
--Begin
--	Insert into test.tblResults Values (@TestRunNo, '40.3', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid CustomerKey' + @ErrorText)
--End

--[CustomerID]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.CustomerID < 1
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.4', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.4', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid CustomerID' + @ErrorText)
End

--[CustomerSource]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.CustomerSource in ('Unk', 'N/A')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.5', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.5', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid CustomerSource' + @ErrorText)
End

--[LedgerID]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.LedgerID < 1
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.6', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.6', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid LedgerID' + @ErrorText)
End

--[LedgerSource]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.LedgerSource is null or #Account.LedgerSource = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.7', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.7', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid LedgerSource' + @ErrorText)
End

--[LegacyLedgerID]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.LegacyLedgerID < 1
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.8', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.8', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid LegacyLedgerID' + @ErrorText)
End

--[LedgerTypeKey] N/A

--[LedgerTypeID]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.LedgerTypeID is null
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.9', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.9', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid LedgerTypeID' + @ErrorText)
End

--[LedgerTypeSource]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.LedgerTypeSource is null or #Account.LedgerTypeSource = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.10', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.10', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid LedgerTypeSource' + @ErrorText)
End

--[LedgerTypeName]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.LedgerTypeName is null or #Account.LedgerTypeName = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.11', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.11', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid LedgerTypeName' + @ErrorText)
End

--[LedgerGroupingName]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.LedgerGroupingName is null or #Account.LedgerGroupingName = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.12', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.12', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid LedgerGroupingName' + @ErrorText)
End

--[AccountSequenceNumber]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.AccountSequenceNumber < 1
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.13', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.13', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid AccountSequenceNumber' + @ErrorText)
End

--[SourceTitle] n/a
--[SourceSurname] n/a
--[SourceMiddleName] n/a
--[SourceFirstName] n/a
--[SourceGender] n/a
--[Title] n/a
--[Surname] n/a
--[MiddleName] n/a
--[FirstName] n/a
--[DOB] n/a

--[Gender]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.Gender not in ('M','F','U')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.14', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5643'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40.14', 'BVA_Dimension_Account', 'Pass', 'CCIA-5643')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40.14', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid Gender' + @ErrorText)
	End
End

--[CompanyKey] N/A
--[AccountOpenedDayKey] N/A

--[AccountOpenedDate]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.AccountOpenedDate < '1990-01-01'
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.15', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.15', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid AccountOpenedDate' + @ErrorText)
End

If not exists	(
				select		AccountSource
							, AccountID
							, Convert(date,AccountOpenedDate) as OpenedDate
							, Convert(date,min (FromDate)) as FromDateTime
				from		#Account
				Where		CreatedBatchKey = ModifiedBatchKey -- prevents the errors being reported more than once
				Group by	AccountSource, AccountID
							, Convert(date,AccountOpenedDate)
				Having		 Convert(date,min (FirstDate)) != Convert(date,AccountOpenedDate)
				)
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.15a', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.15a', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid AccountOpenedDate' + @ErrorText)
End

--[AccountOpenedChannel]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.AccountOpenedChannel is null or #Account.AccountOpenedChannel = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.16', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.16', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid AccountOpenedChannel' + @ErrorText)
End

--[AddressType]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.AddressType not in ('Home', 'Postal', 'Unknown', 'N/A')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.17', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.17', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid AddressType' + @ErrorText)
End

--[SourceAddress1] N/A
--[SourceAddress2] N/A
--[SourceSuburb] N/A
--[SourceStateCode] N/A
--[SourceStateName] N/A
--[SourcePostCode] N/A
--[SourceCountryCode] N/A
--[SourceCountryName] N/A
--[Address1] N/A
--[Address2] N/A
--[Suburb] N/A
--[StateCode] N/A
--[StateName] N/A
--[PostCode] N/A
--[CountryCode] N/A
--[CountryName] N/A
--[GeographicalLocation] N/A

--[AustralianClient]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.AustralianClient not in ('Yes', 'No', 'Unk', 'N/A')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.18', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.18', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid AustralianClient' + @ErrorText)
End
--[SourcePhone] N/A
--[SourceMobile] N/A
--[SourceFax] N/A
--[SourceEmail] N/A
--[SourceAlternateEmail] N/A

--[Phone]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.Phone is null or #Account.Phone = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.19', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.19', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid Phone' + @ErrorText)
End

--[Mobile]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.Mobile is null or #Account.Mobile = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.19a', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.19a', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid Mobile' + @ErrorText)
End

--[Fax] 
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.Fax is null or #Account.Fax = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.20', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.20', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid Fax' + @ErrorText)
End

--[Email]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.Email is null or #Account.Email = '' or Email not like '%@%')
							and #Account.Email != 'Unknown'
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.21', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.21', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid Email' + @ErrorText)
End

--[AlternateEmail]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.AlternateEmail is null or #Account.AlternateEmail = '' or AlternateEmail not like '%@%')
							and #Account.AlternateEmail != 'Unknown'
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.22', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.22', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid AlternateEmail' + @ErrorText)
End

--[StatementMethod]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.StatementMethod not in ('Unk', 'N/A', 'Mail', 'Email', 'Never', 'Fax')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.23', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.23', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid StatementMethod' + @ErrorText)
End

--[StatementFrequency]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		#Account.StatementFrequency < 0
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.24', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5644'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40.24', 'BVA_Dimension_Account', 'Pass', 'CCIA-5644')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40.24', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid StatementFrequency' + @ErrorText)
	End
End

--[BTag2]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.BTag2 is null or #Account.BTag2 = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.25', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.25', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid BTag2' + @ErrorText)
End

--[AffiliateID]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.AffiliateID = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.25a', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.25a', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid AffiliateID' + @ErrorText)
End

--[AffiliateName] N/A

--[SiteID]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.SiteID is null)
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.26', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.26', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid SiteID' + @ErrorText)
End

--[TrafficSource]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.TrafficSource is null or #Account.TrafficSource = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.27', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	If exists	(Select		*
				 From		test.tblDefects
				 Where		DefectID = 'CCIA-5545'
							and [Status] != 'Done'
				) 
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40.27', 'BVA_Dimension_Account', 'Pass', 'CCIA-5545')
	End
	Else
	Begin
		Insert into test.tblResults Values (@TestRunNo, '40.27', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid TrafficSource' + @ErrorText)
	End
End
--[RefURL]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.RefURL is null or #Account.RefURL = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.28', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.28', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid RefURL' + @ErrorText)
End

--[CampaignID]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.CampaignID is null or #Account.CampaignID = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.29', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.29', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid CampaignID' + @ErrorText)
End

--[Keywords]
If not exists	(
				select		#Account.AccountId
							, #Account.AccountSource
							, #Account.FromDate
							, #Account.ToDate
				from		#Account
				where		(#Account.Keywords is null or #Account.Keywords = '')
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '40.30', 'BVA_Dimension_Account', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '40.30', 'BVA_Dimension_Account', 'Fail', 'There are records with an invalid Keywords' + @ErrorText)
End