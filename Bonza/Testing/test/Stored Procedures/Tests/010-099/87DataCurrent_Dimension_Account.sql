﻿--Test Code
--exec Test.[87DataCurrent_Dimension_Account] 'ADHOC'
Create Procedure Test.[87DataCurrent_Dimension_Account] @TestRunNo varchar(23)

as

Declare	@BatchDate datetime2
Set	@BatchDate = (Select Max(BatchFromDate) From [$(Staging)].[Control].ETLController Where DimensionStatus = 1)

Declare @LastDate varchar(10)
Set		@LastDate = 
					(Select		max (ModifiedDate)
					From		[$(Dimensional)].[Dimension].[Account]
					)

If @LastDate >= @BatchDate or @LastDate >= DateAdd (Day,-1,getdate())
Begin
	Insert into test.tblResults Values (@TestRunNo, '87', 'DataCurrent_Dimension_Account Table', 'Pass', Null)
End
Else
Begin
	
	Insert into test.tblResults Values (@TestRunNo, '87', 'DataCurrent_Dimension_Account Table', 'Fail', 'The last update in the dimension is ' + cast (@LastDate as varchar(10)) + ' expect an update each day')
End


