﻿--Test Code
--exec Test.[47BVA_Dimension_Class] 'ADHOC', null
--exec Test.[47BVA_Dimension_Class] 'ADHOC', '2015-03-26'

Create Procedure Test.[47BVA_Dimension_Class] @TestRunNo varchar(23), @FromDate datetime

as

Declare @ErrorText varchar(100)
If @FromDate is not null
Begin
Set @ErrorText = ' since ' + Convert (varchar(20), @FromDate, 103)
End
else
Begin
Set @ErrorText = '' 
End

--ClassKey n/a as sequential key
--ClassId
If not exists(
		Select		ClassKey
		From		[$(Dimensional)].Dimension.Class
		where		ClassID = 0
					and ClassKey Not in (0, -1)
					and (
							(FromDate > @FromDate)
							or (@FromDate is null)
						 )
		) 
Begin
	Insert into test.tblResults Values (@TestRunNo, '47', 'BVA_Dimension_Class', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '47', 'BVA_Dimension_Class', 'Fail', 'There are records with an ClassID of 0' + @ErrorText)
End

--Source
--SourceClassName
--ClassCode
--ClassName
--ClassIcon
--SuperclassKey
--SuperclassName

--FromDate & ToDate
If not exists	(
				select		Class1.ClassId
							, Class1.FromDate
							, Class1.ToDate
				from		[$(Dimensional)].Dimension.Class as Class1
							left join [$(Dimensional)].Dimension.Class as Class2 on Class1.ClassId = Class2.ClassId
																	and Class1.ToDate = Class2.FromDate
				where		Class2.ClassId is null
							and Class1.ToDate != '9999-12-31'
				)

Begin
	Insert into test.tblResults Values (@TestRunNo, '47.1', 'BVA_Dimension_Class', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, '47.1', 'BVA_Dimension_Class', 'Fail', 'There are records which do not have a continuous history to 9999')
End

--Metadata n/a
--CreatedDate
--CreatedBatchKey
--CreatedBy
--ModifiedDate
--ModifiedBatchKey
--ModifiedBy





