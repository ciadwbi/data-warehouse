﻿--Test Code
--exec Test.[CCIA-4050] 'ADHOC'
Create Procedure Test.[CCIA-4050] @TestRunNo varchar(23)

as


If  exists		(
				SELECT	*
				FROM	[$(Dimensional)].[Dimension].[Channel]
				where	ChannelTypeName = 'IOS'
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4050a', 'CCIA-4050', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4050a', 'CCIA-4050', 'Fail', 'IOS is not available')
End

If  exists		(
				SELECT	*
				FROM	[$(Dimensional)].[Dimension].[Channel]
				where	ChannelTypeName = 'Android'
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4050b', 'CCIA-4050', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4050b', 'CCIA-4050', 'Fail', 'Android is not available')
End

If  exists		(
				SELECT	*
				FROM	[$(Dimensional)].[Dimension].[Channel]
				where	ChannelTypeName = 'Other Mobile'
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4050c', 'CCIA-4050', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4050c', 'CCIA-4050', 'Fail', 'Other Mobile is not available')
End

If  exists		(
				SELECT	*
				FROM	[$(Dimensional)].[Dimension].[Channel]
				where	ChannelID = 822
				) 
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4050d', 'CCIA-4050', 'Pass', Null)
End
Else
Begin
	Insert into test.tblResults Values (@TestRunNo, 'CCIA-4050d', 'CCIA-4050', 'Fail', 'ChannelID 822 is not available')
End