﻿-- =============================================
-- Author:		Ricardo Sekermestrovic
-- Create date:	16/12/2016
-- Description:	CCIA-9928
-- =============================================


CREATE PROCEDURE [Test].Dup_FTB_PositionFact @TestRunNo VARCHAR(23)
AS
	   BEGIN
		  DECLARE @lastBatchKey int

		  SELECT @lastBatchKey = max(batchKey)
		  FROM   [$(Staging)].[Control].ETLController

		  SELECT [AccountKey], COUNT(DISTINCT LEFT(FirstBetDayKey, 8)) AS TotalFirstDay
		  FROM   [$(Dimensional)].[Fact].[Position]
		  WHERE FirstBetDayKey <>0
		  AND CreatedBatchKey = @lastBatchKey
		  GROUP BY [AccountKey]
		  HAVING COUNT(DISTINCT LEFT(FirstBetDayKey, 8)) > 1


		    IF(@@ROWCOUNT > 0)
		  BEGIN
			 INSERT INTO test.tblResults
			 VALUES      (@TestRunNo, 'CCIA-9930', 'Position Fact: Fisrt Time Bet', 'Fail', 'There are Duplicate First Time Bets for a client(s) on Position Fact')
		  END
		  ELSE
		  BEGIN
			 INSERT INTO test.tblResults
			 VALUES      (@TestRunNo, 'CCIA-9930', 'Position Fact:  Fisrt Time Bet', 'Pass', NULL)
		  END

		   
	   END