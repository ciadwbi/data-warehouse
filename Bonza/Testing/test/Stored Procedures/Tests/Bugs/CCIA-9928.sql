﻿
-- =============================================
-- Author:		Ricardo Sekermestrovic
-- Create date:	16/12/2016
-- Description:	CCIA-9928
-- =============================================

CREATE PROCEDURE [Test].[InstrumentID_MNBK]  @TestRunNo varchar(23)
AS
BEGIN
DECLARE @TotalDIff int


SELECT @TotalDIff = TotalCount - TotalInstrumentIDGroup 
FROM
(
SELECT IntrumentIDGroup, count(InstrumentID) AS TotalInstrumentIDGroup, SUM(count(InstrumentID)) OVER() AS TotalCount
FROM 
(
SELECT InstrumentID,
CASE  WHEN InstrumentID = 'N/A' THEN 'N/A'
	 WHEN InstrumentID = 'Unknown' THEN 'Unknown'
	 ELSE 'Email' END AS IntrumentIDGroup 
FROM  [$(Dimensional)].Dimension.Instrument I
WHERE I.InstrumentType = 'MoneyBookers'
) IData
GROUP BY IntrumentIDGroup
) UNK
WHERE IntrumentIDGroup ='Unknown'


IF (@@ROWCOUNT <> 0) 
BEGIN
	 IF (@TotalDIff = 0) 
	 BEGIN
	    Insert into test.tblResults Values (@TestRunNo, 'CCIA-9928', 'Instrument Dimension MoneyBookers ', 'Fail', 'All InstrumentIDs for MoneyBookers are Unknown')
	 END
	 ELSE 
	 BEGIN
		 Insert into test.tblResults Values (@TestRunNo, 'CCIA-9928', 'Instrument Dimension MoneyBookers ', 'Pass', Null)
	 END

	   
END
ELSE
BEGIN
	   Insert into test.tblResults Values (@TestRunNo, 'CCIA-9928', 'Instrument Dimension MoneyBookers ', 'Pass', NULL)
END

END