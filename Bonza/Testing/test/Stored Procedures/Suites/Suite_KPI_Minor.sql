﻿--Test Code
--exec Test.Suite_KPI_Minor

Create Procedure Test.Suite_KPI_Minor

as

Declare @TestRunNo varchar(23)
Set @TestRunNo = convert (varchar(23),getdate(),126)

Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController])

Select	*
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

Declare @BatchFromDate datetime
Set @BatchFromDate = (Select BatchFromDate From #BatchDetails)

Declare @BatchToDate datetime
Set @BatchToDate = (Select @BatchToDate From #BatchDetails)

Insert into [Test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_KPI_Minor', @TestRunNo, '00', 'Start', getdate())

--Tests 1.1 Tables Exist
exec Test.[67TableExists_Star_KPI] @TestRunNo

--Tests 1.2 Fields Exist
exec Test.[68FieldExists_Fact_KPI] @TestRunNo
exec Test.[3FieldExists_Dimension_Account] @TestRunNo
exec Test.[115FieldExists_Dimension_AccountStatus] @TestRunNo
exec Test.[4FieldExists_Dimension_Wallet] @TestRunNo
exec Test.[8FieldExists_Dimension_Channel] @TestRunNo
exec Test.[15FieldExists_Dimension_Class] @TestRunNo
exec Test.[9FieldExists_Dimension_User] @TestRunNo
exec Test.[11FieldExists_Dimension_Instrument] @TestRunNo
exec Test.[18FieldExists_Dimension_BetType] @TestRunNo
exec Test.[17FieldExists_Dimension_Market] @TestRunNo
exec Test.[6FieldExists_Dimension_Day] @TestRunNo
exec Test.[5FieldExists_Dimension_Contract] @TestRunNo
exec Test.[116FieldExist_Dimension_LegType] @TestRunNo
exec Test.[117FieldExist_Dimension_Campaign] @TestRunNo

--Tests 2.1.2 Timely Update - Daily Changing
exec Test.[87DataCurrent_Dimension_Account] @TestRunNo
exec Test.[118DataCurrent_Dimension_AccountStatus] @TestRunNo
exec Test.[85DataCurrent_Dimension_Class] @TestRunNo
exec Test.[82DataCurrent_Dimension_User] @TestRunNo
exec Test.[83DataCurrent_Dimension_Instrument] @TestRunNo
exec Test.[86DataCurrent_Dimension_Market] @TestRunNo

--Tests 2.1.3 Timely Update - Constant Changing
exec Test.[90DataCurrent_Fact_KPI] @TestRunNo
exec test.[45DataCurrent_Dimension_Contract] @TestRunNo

--Tests 2.3.1 Record Count - Volatile
--kpi
--Account Status
--contract

--Tests 2.4.1 Record Checksum - Volatile
--kpi
--Account Status
--contract

--Tests 4.1 Cross Referencing Stars
--kpi

--5.1.1 Defects Regression - S1/2


Insert into [test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_KPI_Minor', @TestRunNo, '00', 'End', getdate())

Select		*
into		#Temp
From		[test].[tblResults]
where		RunID = @TestRunNo
			and [Status] != 'Pass'

Declare @Counter int
Set @Counter = (
				Select		count (*)
				From		#Temp
				)
				 
Declare @SubjectLine varchar(1000)
Set @SubjectLine = test.fnGetServer() + ' Suite_KPI_Minor - ' + @TestRunNo

IF @Counter = 0
	Begin
		Print 'No Tests Failed'
	End
	Else
	Begin
		DECLARE @xml NVARCHAR(MAX)
		DECLARE @body NVARCHAR(MAX)

		SET @xml = CAST(
						( SELECT	[ID] AS 'td'
									,''
									, RunID AS 'td'
									,''
									, TestID AS 'td'
									,''
									, TestName AS 'td'
									,''
									, Notes AS 'td'
						  FROM		#Temp 
						  ORDER BY TestID 
						  FOR XML PATH('tr'), ELEMENTS 
						  ) AS NVARCHAR(MAX)
						)

		SET @body =	'<html>
						<body>
							<H3>Failed Tests</H3>
							<table border = 1> 
							<tr>
								<th> ID </th>
								<th> RunID </th>
								<th> TestID </th>
								<th> TestName </th>
								<th> Notes </th>
							</tr>'    

		SET @body = @body + @xml +'</table></body></html>'

		EXEC msdb.dbo.sp_send_dbmail
		@profile_name = 'DataWarehouse_Auto_Mails',
		@body = @body,
		@body_format ='HTML',
		@recipients = 'james.foley@williamhill.com.au', 
		@subject = @SubjectLine;
	End



GO
