﻿--Test Code
--exec Test.Suite_Performance

CREATE Procedure [Test].[Suite_Performance]

as

Declare @TestRunNo varchar(23)
Set @TestRunNo = convert (varchar(23),getdate(),126)

Insert into [control].[Runs] (RunType, RunID, Start) values ('Suite_Performance', @TestRunNo, getdate())

exec Test.[99QueryPerformance_Transaction] @TestRunNo
exec Test.[94QueryPerformance_KPI] @TestRunNo
exec Test.[100QueryPerformance_Position] @TestRunNo

Update [control].[Runs] set [End] = getdate() where RunID = @TestRunNo

Select		*
into		#Temp
From		[test].[tblResults]
where		RunID = @TestRunNo
			and [Status] != 'Pass'

Declare @Counter int
Set @Counter = (
				Select		count (*)
				From		#Temp
				)
				 


IF @Counter = 0
	Begin
		Print 'No Tests Failed'
	End
	Else
	Begin
		Declare @SubjectLine varchar(1000)
		Set @SubjectLine = test.fnGetServer() + ' Suite_Performance ' + @TestRunNo
		DECLARE @xml NVARCHAR(MAX)
		DECLARE @body NVARCHAR(MAX)

		SET @xml = CAST(
						( SELECT	[ID] AS 'td'
									,''
									, RunID AS 'td'
									,''
									, TestID AS 'td'
									,''
									, TestName AS 'td'
									,''
									, Notes AS 'td'
						  FROM		#Temp 
						  ORDER BY TestID 
						  FOR XML PATH('tr'), ELEMENTS 
						  ) AS NVARCHAR(MAX)
						)

		SET @body =	'<html>
						<body>
							<H3>Failed Tests</H3>
							<table border = 1> 
							<tr>
								<th> ID </th>
								<th> RunID </th>
								<th> TestID </th>
								<th> TestName </th>
								<th> Notes </th>
							</tr>'    

		SET @body = @body + @xml +'</table></body></html>'

		EXEC msdb.dbo.sp_send_dbmail
		@profile_name = 'DataWarehouse_Auto_Mails',
		@body = @body,
		@body_format ='HTML',
		@recipients = 'bi.Service@williamhill.com.au', 
		@subject = @SubjectLine;
	End


GO