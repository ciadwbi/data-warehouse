﻿--Test Code
--exec Test.Suite_KPI_Repopulated

Create Procedure Test.Suite_KPI_Repopulated

as

Declare @TestRunNo varchar(23)
Set @TestRunNo = convert (varchar(23),getdate(),126)

Declare @LastBatch int
Set @LastBatch = (Select Max([BatchKey]) From [$(Staging)].[Control].[ETLController])

Select	*
into	#BatchDetails
From	[$(Staging)].[Control].[ETLController]
Where	[BatchKey] = @LastBatch

Declare @BatchFromDate datetime
Set @BatchFromDate = (Select BatchFromDate From #BatchDetails)

Declare @BatchToDate datetime
Set @BatchToDate = (Select @BatchToDate From #BatchDetails)

Insert into [test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_KPI_Repopulated', @TestRunNo, '00', 'Start', getdate())

--Tests 1.1 Tables Exist
exec Test.[67TableExists_Star_KPI] @TestRunNo

--Tests 1.2 Fields Exist
exec Test.[68FieldExists_Fact_KPI] @TestRunNo
exec Test.[3FieldExists_Dimension_Account] @TestRunNo
exec Test.[115FieldExists_Dimension_AccountStatus] @TestRunNo
exec Test.[4FieldExists_Dimension_Wallet] @TestRunNo
exec Test.[8FieldExists_Dimension_Channel] @TestRunNo
exec Test.[15FieldExists_Dimension_Class] @TestRunNo
exec Test.[9FieldExists_Dimension_User] @TestRunNo
exec Test.[11FieldExists_Dimension_Instrument] @TestRunNo
exec Test.[18FieldExists_Dimension_BetType] @TestRunNo
exec Test.[17FieldExists_Dimension_Market] @TestRunNo
exec Test.[6FieldExists_Dimension_Day] @TestRunNo
exec Test.[5FieldExists_Dimension_Contract] @TestRunNo
exec Test.[116FieldExist_Dimension_LegType] @TestRunNo
exec Test.[117FieldExist_Dimension_Campaign] @TestRunNo

--Tests 2.1.1 Timely Update - Slowly Changing
exec Test.[80DataCurrent_Dimension_Wallet] @TestRunNo
exec Test.[73DataCurrent_Dimension_Channel] @TestRunNo
exec Test.[72DataCurrent_Dimension_BalanceType] @TestRunNo
exec Test.[43DataCurrent_Dimension_Day] @TestRunNo

--Tests 2.1.2 Timely Update - Daily Changing
exec Test.[87DataCurrent_Dimension_Account] @TestRunNo
exec Test.[118DataCurrent_Dimension_AccountStatus] @TestRunNo
exec Test.[85DataCurrent_Dimension_Class] @TestRunNo
exec Test.[82DataCurrent_Dimension_User] @TestRunNo
exec Test.[83DataCurrent_Dimension_Instrument] @TestRunNo
exec Test.[86DataCurrent_Dimension_Market] @TestRunNo

--Tests 2.1.3 Timely Update - Constant Changing
exec Test.[90DataCurrent_Fact_KPI] @TestRunNo
exec test.[45DataCurrent_Dimension_Contract] @TestRunNo

--Tests 2.2 Default Records
exec Test.[37DataDefault_Dimension_Account] @TestRunNo
exec [Test].[123DataDefault_Dimension_AccountStatus] @TestRunNo
exec Test.[20DataDefault_Dimension_Wallet] @TestRunNo
exec Test.[25DataDefault_Dimension_Channel] @TestRunNo
exec Test.[31DataDefault_Dimension_Class] @TestRunNo
exec Test.[26DataDefault_Dimension_User] @TestRunNo
exec Test.[27DataDefault_Dimension_Instrument] @TestRunNo
exec Test.[34DataDefault_Dimension_BetType] @TestRunNo
exec Test.[33DataDefault_Dimension_Market] @TestRunNo
exec Test.[23DataDefault_Dimension_Day] @TestRunNo
exec Test.[22DataDefault_Dimension_Contract] @TestRunNo
exec [Test].[127DataDefault_Dimension_LegType] @TestRunNo
exec [Test].[128DefaultData_Dimension_Campaign] @TestRunNo

--Tests 2.3.1 Record Count - Volatile
--kpi
--Account Status
--contract

--Tests 2.3.2 Record Count - Non-Volatile
exec test.[96DataCount_Dimension_Account] @TestRunNo
exec test.[113DataCount_Dimension_Wallet] @TestRunNo
--Channel
--Class
--User
--Instrument
--Bet Type
--Market
exec test.[38DataCount_Dimension_Day] @TestRunNo
--LegType
--Campaign

--Tests 2.4.1 Record Checksum - Volatile
--kpi
--Account Status
--contract

--Tests 2.4.2 Record Checksum - Non-Volatile
--account
exec Test.[114DataCheckSum_Dimension_Wallet] @TestRunNo
--Channel
--Class
--User
--Instrument
--Bet Type
--Market
--LegType
--Campaign

--Tests 3.1 Validation of Records
exec test.[119DataValidation_Fact_KPI] @TestRunNo
exec test.[40BVA_Dimension_Account] @TestRunNo, null
exec Test.[141BVA_Dimension_AccountStatus] @TestRunNo, null
exec Test.[138DataValid_Dimension_Wallet] @TestRunNo
exec test.[111BVA_Dimension_Channel] @TestRunNo, null
exec test.[47BVA_Dimension_Class] @TestRunNo, null
--User
--Instrument
exec Test.[142BVA_Dimension_BetType] @TestRunNo, null
--Market
exec test.[97BVA_Dimension_Day] @TestRunNo, null
--Contract
--LegType
--Campaign

--Tests 3.2 No Duplicates 
exec [Test].[125DataDuplicates_Fact_KPI] @TestRunNo, null
exec test.[58DataDuplicates_Dimension_Account] @TestRunNo, null
exec test.[131DataDuplicates_Dimension_AccountStatus] @TestRunNo, null
exec test.[41DataDuplicates_Dimension_Wallet] @TestRunNo, null
exec test.[48DataDuplicates_Dimension_Channel] @TestRunNo
exec test.[54DataDuplicates_Dimension_Class] @TestRunNo, null
exec test.[49DataDuplicates_Dimension_User] @TestRunNo
exec test.[50DataDuplicates_Dimension_Instrument] @TestRunNo, null
exec test.[57DataDuplicates_Dimension_BetType] @TestRunNo
exec test.[56DataDuplicates_Dimension_Market] @TestRunNo, null
exec test.[59DataDuplicates_Dimension_Day] @TestRunNo
exec test.[98DataDuplicates_Dimension_Contract] @TestRunNo, null
exec [Test].[133DataDuplicates_Dimension_LegType] @TestRunNo, null
exec [Test].[134DataDuplicates_Dimension_Campaign] @TestRunNo, null

--Tests 3.3 Field Mappings
exec Test.[139DataValid_Fact_KPI] @TestRunNo
exec test.[46DataValid_Dimension_Account] @TestRunNo
--Account Status
exec Test.[138DataValid_Dimension_Wallet] @TestRunNo
--Channel
exec Test.[149DataValid_Dimension_Class] @TestRunNo
--User
--Instrument
--Bet Type
--Market
exec test.[36DataValid_Dimension_Day] @TestRunNo
--Contract
--LegType
--Campaign

--Tests 3.4 Foreign Key Contraints
exec Test.[121DataFK_Fact_KPI] @TestRunNo, null

--Tests 3.5 Truncated Values
--KPI
--Account
--Account Status
--Wallet
--Channel
--Class
--User
--Instrument
--Bet Type
--Market
--Day
--Contract
--LegType
--Campaign

--Test 3.6 Destructive Injection
--KPI
--Account
--Account Status
--Wallet
--Channel
--Class
--User
--Instrument
--Bet Type
--Market
--Day
--Contract
--LegType
--Campai

--Tests 4.1 Cross Referencing Stars
--kpi

--Tests 5.1.1 Defect - S1/2

--Tests 5.1.2 Defect - S3/4
exec Test.[CCIA-3708] @TestRunNo

--Tests 5.1.3 Defect - S5
exec [Test].[CCIA-5072] @TestRunNo

Insert into [test].[tblRuns] (RunType, RunID, [TestID], [State], [Time]) values ('Suite_KPI_Repopulated', @TestRunNo, '00', 'End', getdate())
