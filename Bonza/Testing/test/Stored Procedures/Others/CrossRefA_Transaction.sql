﻿--Test Code
--exec Test.[CrossRefA_Transaction] 'AdHoc'
Create Procedure [Test].[CrossRefA_Transaction] @TestRunNo varchar(23), @FromDate datetime2

as

select	*,
		-- Given this procedure enforces a single day window, the following values are direct derivations and do not require the ovewrhead of the windowed functions below
		NetClosingBalance - NextSourceOpeningBalance DayClosingDiscrepancy,
		NetClosingBalance - NextSourceOpeningBalance OngoingClosingDiscrepancy,
		NetClosingBalance - NextSourceOpeningBalance TotalDiscrepancy
FROM	(	SELECT	Balance,
					WalletName,
					DayDate, 
					BrandCode,
					AccountID,
					LedgerID,
					SourceOpeningBalance,
					TransactedAmount, 
					TransactionCount,
					SourceOpeningBalance + TransactedAmount NetClosingBalance,
					LEAD(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, WalletName, LedgerID ORDER BY DayDate) NextSourceOpeningBalance--,
--					SourceOpeningBalance + TransactedAmount - LEAD(SourceOpeningBalance,1,0) OVER (PARTITION BY BrandCode, Balance, WalletName, LedgerID ORDER BY DayDate) DayClosingDiscrepancy,
--					FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, WalletName, LedgerID ORDER BY DayDate) + SUM(TransactedAmount) OVER (PARTITION BY BrandCode, Balance, WalletName, LedgerID ORDER BY DayDate RANGE BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) - LEAD(SourceOpeningBalance,1,0) OVER (PARTITION BY BrandCode, Balance, WalletName, LedgerID ORDER BY DayDate) OngoingClosingDiscrepancy,
--					FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, WalletName, LedgerID ORDER BY DayDate) + SUM(TransactedAmount) OVER (PARTITION BY BrandCode, Balance, WalletName, LedgerID) - FIRST_VALUE(SourceOpeningBalance) OVER (PARTITION BY BrandCode, Balance, WalletName, LedgerID ORDER BY DayDate DESC) TotalDiscrepancy
			FROM	(	SELECT BrandCode,
								Balance,
								WalletName,
								LedgerID, 
								AccountID,
								DayDate,
								ISNULL(SUM(SourceOpeningBalance) ,0) SourceOpeningBalance, 
								ISNULL(SUM(TransactedAmount),0) TransactedAmount,
								SUM(CASE WHEN TransactedAmount <> 0 THEN 1 ELSE 0 END) TransactionCount
						FROM	(	SELECT	CASE WHEN BrandCode = 'Unk' AND a.LedgerID NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'RWWA' ELSE BrandCode END BrandCode, 
											BalanceTypeName Balance,
											WalletName,
											a.LedgerID,
											AccountID, 
											DayDate,
											NULL SourceOpeningBalance, 
											TransactedAmount
									FROM   [$(Dimensional)].Dimension.day d with (nolock)
											inner join [$(Dimensional)].Dimension.dayzone z with (nolock) on (z.AnalysisDayKey=d.DayKey)
											inner join [$(Dimensional)].Fact.[Transaction] t with (nolock) on (t.DayKey=z.DayKey)
											inner join [$(Dimensional)].Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = t.BalanceTypeKey)
											inner join [$(Dimensional)].Dimension.wallet w with (nolock) on (w.WalletKey = t.WalletKey)
											inner join [$(Dimensional)].Dimension.account a with (nolock) on (a.AccountKey = t.AccountKey)
											inner join [$(Dimensional)].Dimension.accountStatus as accountStatus with (nolock) on (t.AccountStatusKey = accountStatus.AccountStatusKey)
											inner join [$(Dimensional)].Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey)
									where DayDate BETWEEN @FromDate AND @FromDate
											and BalanceTypeName IN ('Client')--,'Hold')
											and a.LedgerType NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
											and accountStatus.AccountType = 'Client'
											and w.WalletName = 'Cash'
									UNION ALL
									select	CASE WHEN BrandCode = 'Unk' AND a.LedgerID NOT IN (777768,779522,783893,817235,825388,1187572,1250958) THEN 'RWWA' ELSE BrandCode END BrandCode, 
											BalanceTypeName Balance,
											'Cash' WalletClassName,
											a.LedgerID,
											AccountID, 
											DayDate,
											OpeningBalance, 
											NULL TransactedAmount
									from   [$(Dimensional)].Dimension.day d with (nolock)
											inner join [$(Dimensional)].Dimension.dayzone z with (nolock) on (z.AnalysisDayKey=d.DayKey)
											inner join [$(Dimensional)].Fact.[Position] t with (nolock) on (t.DayKey=z.DayKey)
											inner join [$(Dimensional)].Dimension.BalanceType b with (nolock) on (b.BalanceTypeKey = t.BalanceTypeKey)
											inner join [$(Dimensional)].Dimension.account a with (nolock) on (a.AccountKey = t.AccountKey)
											inner join [$(Dimensional)].Dimension.accountStatus as accountStatus with (nolock) on (t.AccountstatusKey = accountStatus.AccountstatusKey)
											inner join [$(Dimensional)].Dimension.company c with (nolock) on (c.CompanyKey = a.CompanyKey)
									where DayDate BETWEEN @FromDate AND DateAdd(Day,1,@FromDate)
											and BalanceTypeName IN ('Client')--,'Hold')
											and a.LedgerType NOT IN ('Write Offs','Vendor Splits','Vendor W.Offs/Other','Vendor Arrangements','Vendor 3rd Party Collection','Test','Betback') -- Added Test
											and accountStatus.AccountType = 'Client'
						) z 
					group by BrandCode, Balance, WalletName, DayDate, LedgerID, AccountID
				) y
		) x
WHERE NextSourceOpeningBalance <> NetClosingBalance
-- and (DayClosingDiscrepancy <> 0 /*OR TotalClosingDiscrepancy <> 0*/)
-- and TotalDiscrepancy <> 0
--GROUP BY BrandCode, Balance, WalletClassName, DayDate
order by Balance, WalletName, DayDate, BrandCode, AccountID