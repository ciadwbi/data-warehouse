--Test Code
/*
exec [Test].[spExecuteMDX] 'AdHoc', 'Balance', 'Simple', 10000,
'
Select * From OpenQuery([EQ3DBS05\TABULAR_],''WITH MEMBER [Measures].[NetValue] AS [Measures].[Turnover] - [Measures].[GrossWin]
SELECT 
NON EMPTY
{[Measures].[Turnover],[Measures].[GrossWin],[Measures].[NetRevenue],[Measures].[NetValue]} on Columns,
CROSSJOIN([Company].[BusinessUnitName].[BusinessUnitName],[Day].[FiscalWeekName].MEMBERS) on rows
FROM [DataWareHouseWH]
WHERE 
([Day].[MasterFiscalCalendarHierarchy].[FiscalYearName].&[2015],
[Company].[CompanyName].&[William Hill Australia]
)'')
'
*/

Create Procedure [Test].[spExecuteMDX] (@RunID varchar(23), @TestID varchar(100), @TestName varchar(1000), @TargetMSeconds int, @MDX varchar(max))

as

Declare @Start as datetime
Declare @End as datetime
Declare @Time as int
set @Start = getdate()

exec (@MDX)

set @End = getdate()
set @Time = DATEDIFF(Ms,@Start,@End)

Insert into [Test].[tblQueryPerformance] (RunID, TestID, TestName, Start, Ends, MSeconds, TargetMSeconds) values (@RunID, @TestID, @TestName, @Start, @End, @Time, @TargetMSeconds)

Go


